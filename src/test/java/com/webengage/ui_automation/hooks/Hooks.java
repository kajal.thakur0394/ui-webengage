package com.webengage.ui_automation.hooks;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.stream.Collectors;
import javax.imageio.ImageIO;
import com.webengage.ui_automation.pages.CampaignsPage;
import com.webengage.ui_automation.pages.CommonPage;
import com.webengage.ui_automation.pages.JourneysPage;
import com.webengage.ui_automation.pages.SegmentsPage;
import com.webengage.ui_automation.runners.BeforeTest;
import com.webengage.ui_automation.runners.SequentialRunner;

import org.apache.commons.io.FilenameUtils;
import org.junit.Assume;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import com.webengage.ui_automation.driver.DriverFactory;
import com.webengage.ui_automation.utils.Log;
import com.webengage.ui_automation.utils.SetupUtility;
import com.webengage.ui_automation_dao.DatabaseTransaction;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;

public class Hooks {
	public static final String directoryLoc = System.getProperty("user.dir") + "/src/test/resources/downloads";
	public static String uniqueTag;
	public static String scenarioName;
	public static boolean attachJourney = false;
	DatabaseTransaction dbTransaction = DatabaseTransaction.getInstance();
	SetupUtility setupUtility;

	public String getScenarioTagID(Collection<String> collection) {
		try {
			return collection.stream().filter(s -> s.contains("issue")).collect(Collectors.toList()).get(0)
					.split(":")[1];
		} catch (Exception e) {
			return null;
		}
	}

	@Before(order = 0, value = "@skip")
	public void skipScenario(Scenario scenario) {
		Log.info("Skipped: " + scenario.getName());
		Assume.assumeTrue(false);
		logScenarioDetails(scenario);
	}

	@Before("@Distributed")
	public void resetId(Scenario scenario) {
		CampaignsPage.setCampaignID(null);
		SegmentsPage.setSegmentId(null);
		JourneysPage.setJourneyId(null);
		JourneysPage.setJourneyExpectedPayload(null);
	}

	@Before("@Staging")
	public void skipStageScenario(Scenario scenario) {
		if (CampaignsPage.getPrimaryNamespace() == null && CampaignsPage.getSecondaryNamespace() == null) {
			Log.info("Skipped: " + scenario.getName());
			Assume.assumeTrue(false);
			logScenarioDetails(scenario);
		}
	}

	@Before("@Regression")
	public static void getFeatureFileName(Scenario scenario) {
		String s = scenario.getUri().toString().split("modules")[1];
		String featureName = s.substring(1, s.length()).split("\\.")[0];
		System.setProperty("featureFileName", featureName);
	}

	@After
	public void afterScenario(Scenario scenario) throws IOException {
		if (scenario.isFailed()
				&& DriverFactory.getInstance().getDriver().getCurrentUrl().contains(CommonPage.SDKDynamicWebpage)) {
			final byte[] screenShot = ((TakesScreenshot) DriverFactory.getInstance().getDriver())
					.getScreenshotAs(OutputType.BYTES);
			scenario.attach(screenShot, "image/png", scenario.getName());
			ArrayList<String> windowList = new ArrayList<String>(
					DriverFactory.getInstance().getDriver().getWindowHandles());
			Log.debug("closing the child window...");
			DriverFactory.getInstance().getDriver().close();
			Log.debug("Successfully closed the child window.");
			Log.debug("switching to parent window...");
			DriverFactory.getInstance().getDriver().switchTo().window(windowList.get(0));
			Log.debug("Successfully switched to parent window.");
			DriverFactory.getInstance().getDriver().get(BeforeTest.landingPageURL);
		} else if (scenario.isFailed()) {
			final byte[] screenShot = ((TakesScreenshot) DriverFactory.getInstance().getDriver())
					.getScreenshotAs(OutputType.BYTES);
			scenario.attach(screenShot, "image/png", scenario.getName());
			if (CampaignsPage.getPrimaryNamespace() != null && CampaignsPage.getSecondaryNamespace() != null) {
				String Staging_URL = SetupUtility.configProp.getProperty("Staging_URL");
				Staging_URL = Staging_URL.replace("namespace", CampaignsPage.getPrimaryNamespace());
				DriverFactory.getInstance().getDriver().get(Staging_URL);
			} else
				DriverFactory.getInstance().getDriver().get(BeforeTest.landingPageURL);
		}
		logScenarioDetails(scenario);
	}

	@After("@Journeys")
	public void attachJourneyCanvas(Scenario scenario) throws IOException {
		if (attachJourney) {
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			File imageFile = new File(directoryLoc);
			imageFile = imageFile.listFiles()[0];
			BufferedImage buffImg = ImageIO.read(imageFile);
			ImageIO.write(buffImg, "jpg", output);
			final byte[] screenShot = output.toByteArray();
			scenario.attach(screenShot, "image/png", "Implemeneted Journey details");
			imageFile.delete();
			attachJourney = false;
		}
	}

	private void logScenarioDetails(Scenario scenario) {
		String tags = "";
		for (String s : scenario.getSourceTagNames()) {
			tags = tags.concat(s.replace("@", "") + ",");
		}
		LinkedHashMap<String, String> scenarioInfo = new LinkedHashMap<>();
		String[] idString = scenario.getId().split("/");
		scenarioInfo.put("Feature", idString[scenario.getId().split("/").length - 1].split("\\.")[0]);
		scenarioInfo.put("Scenario Name", scenario.getName());
		scenarioInfo.put("Status", scenario.getStatus().name());
		scenarioInfo.put("Tags", tags.substring(0, tags.length() - 1));
		SequentialRunner.scenarioDetails.add(scenarioInfo);
	}

	@After("@Distributed")
	public void writeDataInDB(Scenario s) {
		if (!s.isFailed()) {
			String featureName = FilenameUtils.getBaseName(s.getUri().toString()).replaceAll("[^a-zA-Z]", "");
			String scenarioName = s.getName();
			DatabaseTransaction.getInstance().selectTable(featureName, scenarioName);
		}
	}

	@After("@OnsiteNotification or @OnsiteSurveys or @InLineContentWebP")
	public void deleteWebPCampaigns(Scenario s) throws IOException {
		String featureName = FilenameUtils.getBaseName(s.getUri().toString()).replaceAll("[^a-zA-Z]", "");
		String campaignId = CampaignsPage.getCampaignID();
		if (s.isFailed() && (campaignId != null)) {
			CampaignsPage campaignsPage = new CampaignsPage();
			if (featureName.toLowerCase().contains("notification"))
				campaignsPage.deleteCampaignsWithIdViaApi(campaignId, "deleteOnsiteNotification");
			else if (featureName.toLowerCase().contains("contentwebp"))
				campaignsPage.deleteCampaignsWithIdViaApi(campaignId, "deleteInlineContentWebPCampaigns");
			else
				campaignsPage.deleteSurveyViaApi(campaignId, "deleteSurvey");
			CampaignsPage.setCampaignID(null);
		}
	}
	
	@After("@InLineContentAppP")
	public void deleteAppinlineCampaignWithTemplates(Scenario s) throws IOException {
		if (s.isFailed()) {
			CampaignsPage campaignsPage = new CampaignsPage();
			String templateId = CampaignsPage.getTemplateId();
			if (Hooks.scenarioName.contains("integration page"))
				campaignsPage.deleteCampaignsWithIdViaApi(templateId, "deleteAppinlineCustomTemplates");
			else if (templateId != null) {
				campaignsPage.deleteCampaignsWithIdViaApi(CampaignsPage.getCampaignID(),
						"deleteInLineContentAppCampaigns");
				campaignsPage.deleteCampaignsWithIdViaApi(templateId, "deleteAppinlineCustomTemplates");
				CampaignsPage.setTemplateId(null);
			}
		}
	}

	@Before
	public void getScenarioName(Scenario s) {
		scenarioName = s.getName();
	}
}
