package com.webengage.ui_automation.stepDefinitions;

import java.util.List;
import java.util.Map;

import com.webengage.ui_automation.pages.AnalyticsPage;
import com.webengage.ui_automation.pages.CommonPage;
import com.webengage.ui_automation.utils.NotificationToastMessages;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;

public class AnalyticsStepDef {
	AnalyticsPage analyticsPage = new AnalyticsPage();
	CommonPage commonPage = new CommonPage();

	@Then("set event section filter as")
	public void set_event_section_filter_as(DataTable dataTable) {
		List<List<String>> filterOptions = dataTable.asLists(String.class);
		analyticsPage.eventFilterSelection(filterOptions);
	}

	@Then("verify following things get reflected in populated table")
	public void verify_following_things_get_reflected_in_populated_table(DataTable dataTable) {
		List<List<String>> cellValues = dataTable.asLists(String.class);
		analyticsPage.validateTableContent(cellValues);
	}

	@Then("fill the Path filter using below values")
	public void fill_the_path_filter_using_below_values(DataTable dataTable) {
		List<List<String>> pathFilters = dataTable.asLists(String.class);
		analyticsPage.pathFilterSelection(pathFilters);
	}

	@Then("validate whether event {string} gets reflected in Step {string}")
	public void validate_whether_event_gets_reflected_in_step(String eventName, String stepNumber) {
		analyticsPage.validateStepinHighChart(eventName, stepNumber);
	}

	@Then("create new Funnel with name as {string}")
	public void create_new_funnel_with_name_as(String funnelName) {
		commonPage.createIcon();
		analyticsPage.enterFunnelName(funnelName);
	}

	@Then("fill the Steps detail using below values")
	public void fill_the_steps_detail_using_below_values(DataTable dataTable) {
		List<List<String>> funnelStepFilters = dataTable.asLists(String.class);
		analyticsPage.addFunnelSteps(funnelStepFilters);
	}

	@Then("delete the funnel with name as {string}")
	public void delete_the_funnel_with_name_as(String funnelName) {
		commonPage.searchModuleNameHavingUnicode(funnelName);
		commonPage.deleteModule(funnelName);
		commonPage.verifyToastMessage(NotificationToastMessages.DELETE_FUNNEL.message());
	}

	@Then("Verify presence of Events")
	public void verify_presence_of_events(DataTable dataTable) {
		List<String> eventList = dataTable.asList(String.class);
		analyticsPage.isEventPresent(eventList);
	}

	@Then("I filter for {string} from dayType dropdown")
	public void i_filter_for_custom_number_days(String dayType) {
		analyticsPage.filterCustomDays(dayType);
	}

	@Then("I validate the actual value to be {string} expected value having {string} for {string} and {string} via {string} graph")
	public void i_verify_the_presence(String operator, String total, String showType, String dayType, String graphType,
			DataTable dataTable) {
		List<Map<String, String>> elementList = dataTable.asMaps(String.class, String.class);
		if (!dayType.equalsIgnoreCase("Hours of Day"))
			analyticsPage.verifyGraphOrLineData(operator, total, showType, dayType, graphType, elementList);
	}

	@Then("I validate the actual value for {string} to be {string} expected value in table displayed")
	public void i_verify_the_data_in_table(String showType, String operator, DataTable dataTable) {
		List<Map<String, String>> expectedValues = dataTable.asMaps(String.class, String.class);
		analyticsPage.verifyTableDataElements(showType, operator, expectedValues);
	}

	@Then("I apply filter for events")
	public void i_apply_filter_for_events(DataTable dataTable) {
		List<List<String>> choices = dataTable.asLists(String.class);
		analyticsPage.addFilter(choices);
	}

	@Then("I Reset filter for {string} tab")
	public void i_reset_filter(String tabName) {
		analyticsPage.resetFilter(tabName);
	}

	@Then("I check common filter does not have common event")
	public void i_check_common_filter(DataTable dataTable) {
		List<List<String>> choices = dataTable.asLists(String.class);
		analyticsPage.commonEventValidation(choices);
	}

	@Then("I verify split by option and custom alert should be greyed out for multi events")
	public void i_verify_split_by_and_custom_alert() {
		analyticsPage.alertAndSplitByDisableCheck();
	}

	@Then("I verify bell icon should be greyed out")
	public void i_verify_bell_icon() {
		analyticsPage.bellIconGreyOutCheck(
				"Alert cannot be created since the Segment /User attribute filter is in use.", "filter");
	}

	@Then("I create custom alert with name {string}")
	public void i_create_custom_alert(String alertName, DataTable dataTable) {
		analyticsPage.createAlert(alertName, dataTable);
	}

	@Then("I verify the selected events get reflected in graph")
	public void i_verify_the_selected_events() {
		analyticsPage.verifySelectedEvents();
	}

	@Then("I remove event {string} and validate common filter {string} is retained for existing events")
	public void i_remove_event(String eventName, String attribute) {
		analyticsPage.removeEvent(eventName);
		analyticsPage.commonFilterRetention(eventName, attribute);
	}

	@Then("I delete the created custom alert with name {string}")
	public void i_delete_custom_alert(String alertName) {
		analyticsPage.deleteAlert(alertName, NotificationToastMessages.DELETE_ALERT.message());
	}

	@Then("I pin in the {string} dashboard with card name {string} and dashboard name {string}")
	public void i_pin_in_the_dashbaord(String dashboardStatus, String cardName, String dashboardName) {
		analyticsPage.pinDashboard(dashboardStatus, cardName, dashboardName);
	}

	@Then("I click on {string} and verify it re-directs to events page")
	public void i_click_on_cardName_and_verify(String cardName) {
		analyticsPage.cardOpenEvent(cardName);
	}

	@Then("I click on {string} option")
	public void i_click_on_option(String option) {
		analyticsPage.clickOption(option);
	}

	@Then("I fill {string} list modal page with time period {string} and name {string}")
	public void i_create_list(String listType, String timePeriod, String listName, DataTable dataTable) {
		Map<String, String> expectedValues = dataTable.asMap(String.class, String.class);
		analyticsPage.listModalPageDetails(listType, timePeriod, listName, expectedValues);
	}
	
	@Then("I verify {string} condition for {string}")
	public void time_period_last_x_days(String period, String listName) {
		analyticsPage.verifyMaxOrMinTimePeriod(period, listName);
	}

	@Then("I compose a string having length as {string} and compare it with limit of {string}")
	public void i_verify_character_limit(String listNameLength, String maxListNameLength) {
		analyticsPage.validateListNameCharacterLimit(listNameLength,maxListNameLength);
	}

	@Then("I verify static list has time period label with To and From date for {string}")
	public void i_verify_to_and_from_date(String dateType) {
		analyticsPage.validateToAndFromDate(dateType);
	}

	@Then("I click on {string} button")
	public void i_click_on_buttone(String button) {
		analyticsPage.clickButton(button);
	}

	@Then("I validate the actual value for events to be {string} expected value in table displayed")
	public void i_validate_the_actual_data_in_table(String operator, DataTable dataTable) {
		List<Map<String, String>> expectedValues = dataTable.asMaps(String.class, String.class);
		analyticsPage.verifyTableDataForExportLists(operator, expectedValues);
	}

	@Then("I validate the presence of all data points of {string}")
	public void i_validate_the_presence_of_all_data_points(String event) {
		analyticsPage.validateDataPoints(event);
	}
	
	@Then("I search and verify presence of data point {string} in the table")
	public void i_search_data_point(String eventName) {
		analyticsPage.searchDataPoint(eventName);
	}

	@Then("I select and de-select the {string} option and verify options are selected properly")
	public void i_select_option(String option) {
		analyticsPage.verifyDataPointsCheckbox(option);
	}
	
	@Then("I select {string} data points")
	public void i_select_multiple_data_points(String count) {
		analyticsPage.selectDataPoints(count);
	}
	
	@Then("I verify all the events of the chart with {string} attribute value")
	public void i_verify_series(String value) {
		analyticsPage.verifySeriesPresence(value);
	}
	
	@Then("I verify segment present in {string} is not present in {string}")
	public void i_verify_segment_presence(String present, String absent, DataTable dataTable) {
		List<List<String>> choices = dataTable.asLists(String.class);
		analyticsPage.verifySegmentPresence(choices.get(0));
	}
}