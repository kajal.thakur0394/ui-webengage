package com.webengage.ui_automation.stepDefinitions;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.webengage.ui_automation.pages.APIOperationsPage;
import com.webengage.ui_automation.pages.CampaignsPage;
import com.webengage.ui_automation.pages.CommonPage;
import com.webengage.ui_automation.pages.SMSCampaignsPage;
import com.webengage.ui_automation.pages.SegmentsPage;
import com.webengage.ui_automation.utils.APIUtility;
import com.webengage.ui_automation.utils.ExcelReader;
import com.webengage.ui_automation.utils.RuntimeUtility;
import com.webengage.ui_automation.utils.SetupUtility;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class SMSCampaignsStepDef {
	SMSCampaignsPage smsCampPage = new SMSCampaignsPage();
	CampaignsPage campPage = new CampaignsPage();
	SegmentsPage segPage = new SegmentsPage();
	SetupUtility setupUtility = new SetupUtility();
	ExcelReader excelReader = new ExcelReader();
	APIUtility apiUtility = new APIUtility();
	RuntimeUtility runTimeUtility = new RuntimeUtility();
	APIOperationsPage apiOperations = new APIOperationsPage();
	CommonPage comPage = new CommonPage();

	@Then("I add {string} to searchbox")
	public void i_add_to_searchbox(String campaignName) {
		smsCampPage.searchBar(campaignName);
	}

	@And("I verify that {string} and {string} exist")
	public void i_verify_that_exist(String campaignName, String campaignType) {
		smsCampPage.verifyCampaignNameandType(campaignName, campaignType);
	}

	@When("I select the SSP with name {string} and I enter the following details in the message SSP")
	public void i_select_the_ssp_with_name_and_i_enter_the_following_details_in_the_message_ssp(String selectSSP,
			DataTable dataTable) {
		campPage.selectSMSSSPName(selectSSP);
		List<List<String>> choices = dataTable.asLists(String.class);
		campPage.enterSMSSSPMessageDetails(choices);
		smsCampPage.saveAndContinueButton();
	}

	@Then("I verify that {string} segment is present inside recently created {string} Campaign")
	public void i_verify_that_segment_is_present_inside_recently_created_campaign(String segmentName,
			String campaignName) {
		smsCampPage.verifySegmentCreated(segmentName, campaignName);
	}

	@Then("I delete the Campaign with Name {string}")
	public void i_delete_the_campaign_with_name(String name) {
		smsCampPage.searchBar(name);
		smsCampPage.deleteCampaign(name);
	}

	@Then("I verify {string} segment is present")
	public void i_verify_segment_is_present(String Name) {
		smsCampPage.verifySegment(Name);
	}

	@Then("I fill the details in Audience tab as below")
	public void i_fill_the_details_in_audience_tab_as_below(List<List<String>> choices) {
		smsCampPage.fillAudienceDetails(choices);
	}

	@Then("I fill the details in Conversion Tracking tab as below")
	public void i_fill_the_details_in_conversion_tracking_tab_as_below(List<List<String>> choices) {
		smsCampPage.checkUnicodeCharacPopUp();
		smsCampPage.fillConversionDetails(choices);
	}

	@Then("I skip the Campaign Test")
	public void i_skip_the_campaign_test() {
		campPage.saveCampaignID(true);
		smsCampPage.skipTestButton();
	}

	@Then("I verify that static segment with Name {string} is present")
	public void i_verify_that_static_segment_with_name_is_present(String name) {
		segPage.setSegmentName(name);
		segPage.searchBar(name);
		segPage.verifyStaticSegments();
	}

	@Then("I save the Audience details")
	public void i_save_the_audience_details() {
		smsCampPage.saveAndContinueButton();
	}

	@Then("I verify following Segments are present")
	public void i_verify_following_segments_are_present(DataTable dataTable) {
		List<String> segmentList = dataTable.asList(String.class);
		smsCampPage.verifySegmentList(segmentList);
	}

	@Then("I verify following details when {string} Campaign having {string} Campaign Name and {string} Channel is selected")
	public void i_verify_following_details_when_campaign_having_campaign_name_and_channel_is_selected(
			String campaignType, String campaignName, String channelType, List<String> assertChoices) {
		smsCampPage.verifyCampaignStatus(campaignType, channelType, assertChoices, campaignName);
	}

	@When("I hit the POST method using static body for {string} api from {string} sheet with ref id {string}")
	public void i_hit_the_post_method_using_static_body_for_api_from_sheet_with_ref_id(String apiName, String sheetName,
			String refId) throws InterruptedException {
		if (apiName.contains("transactionalCampaign"))
			TimeUnit.MINUTES.sleep(1);
		campPage.fetchDynamicAPI(apiName, CampaignsPage.getCampaignID());
		try {
			apiOperations.dynamic_post_method(apiName, sheetName, refId);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Then("I fetch {string} api and add dynamic CampaignId")
	public void i_fetch_api_and_add_dynamic_campaign_id(String apiName) {
		campPage.fetchDynamicAPI(apiName, CampaignsPage.getCampaignID());
	}

	@Then("I open {string} campaign")
	public void i_open_campaign(String campaignName) {
		comPage.openCampaign(campaignName);
	}

	@Then("hit {string} API from {string} sheet to trigger Event with reference id {string}")
	public void hit_api_from_sheet_to_trigger_event_with_reference_id(String apiName, String sheetName, String refId)
			throws InterruptedException {
		if (sheetName.equalsIgnoreCase("PerformEvents"))
			TimeUnit.MINUTES.sleep(1);
		smsCampPage.triggerEventViaAPI(apiName, sheetName.replace("Inject", ""), refId);
	}

	@Then("wait {string} minutes for {string} api to return response for Listpath {string} as below")
	public void wait_for_api_to_return_response_for_listpath_as_below(String duration, String apiName, String jsonPath,
			DataTable dataTable) {
		try {
			apiOperations.appendFromAndToDate(true);
			runTimeUtility.setDynamicRequest("from", "Query");
			runTimeUtility.setDynamicRequest("to", "Query");
			apiOperations.dynamic_get_method(apiName, "SMSCampaigns", "fetchStats");
			smsCampPage.fetchStatsList(duration, apiName, "SMSCampaigns", "fetchStats", jsonPath, dataTable);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Then("I adjust the time for Recurring Campaign by checking system time")
	public void i_adjust_the_time_for_recurring_campaign_by_checking_system_time() {
		campPage.selectTab("When");
		campPage.adjustRecurringTime();
		smsCampPage.saveAndContinueButton();
	}

	@Then("I fill the details in WHEN tab as below when {string} Campaign and {string} Channel is selected")
	public void i_fill_the_details_in_when_tab_as_below_when_campaign_and_channel_is_selected(String campaignType,
			String channelType, List<List<String>> choices) {
		smsCampPage.fillWhenChannelDetails(campaignType, channelType, choices);
		smsCampPage.saveAndContinueButton();
	}

}