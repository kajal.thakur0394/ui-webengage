package com.webengage.ui_automation.stepDefinitions;

import java.util.LinkedList;
import java.util.List;

import com.webengage.ui_automation.pages.CampaignsPage;
import com.webengage.ui_automation.pages.CommonPage;
import com.webengage.ui_automation.pages.PushCampaignsPage;
import com.webengage.ui_automation.utils.ReflectionsUtility;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class PushCampaignsStepDef {
	PushCampaignsPage pushCampPage = new PushCampaignsPage();
	CampaignsPage campPage = new CampaignsPage();
	ReflectionsUtility rel = new ReflectionsUtility();
	CommonPage commonPage=new CommonPage();

	@Given("I select target devices as {string}")
	public void i_select_target_devices_as(String device) {
		pushCampPage.selectTargetDevices(device);
	}
	
	@Given("select specific {string} apps")
	public void select_specific_apps(String platform, DataTable dataTable) {
		campPage.selectPlatformForAppSelection(platform);
		campPage.selectPackageAndSave(new LinkedList<String>(dataTable.asList()));
	}

	@Then("I select template as {string}")
	public void i_select_template_as(String template) {
		pushCampPage.selectTemplate(template);
	}

	@Then("I add following details in {string} Message Template")
	public void i_add_following_details_in_message_template(String label, List<List<String>> choice) {
		pushCampPage.enterMessageDetails(label, choice);
	}

	@Then("I Save all the Message Details")
	public void i_save_all_the_message_details() {
		pushCampPage.saveDetails();
	}

	@Then("I add Carousel Images layout of type {string}")
	public void i_add_carousel_images_layout_of_type(String layout) {
		commonPage.clickOnGenericLabel(layout);
	}

	@Then("I add following details in {string} Message having {string} Template")
	public void i_add_following_details_in_message_having_template(String label, String template,
			List<List<String>> choice) {
		pushCampPage.enterMessageDetails(label, template, choice);

	}

	@Then("I add personalisation details in {string} Template")
	public void i_add_personalisation_details_in_template(String layout) {
		pushCampPage.enterPersonalisationDetails(layout);
	}

}