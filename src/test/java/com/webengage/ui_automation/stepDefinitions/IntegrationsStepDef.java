package com.webengage.ui_automation.stepDefinitions;

import java.util.List;
import java.util.Map;
import com.webengage.ui_automation.pages.IntegrationsPage;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class IntegrationsStepDef{
    IntegrationsPage intPage = new IntegrationsPage();

    @Then("Channel Integration status for {string} set as {string}")
    public void channel_integration_status_for_set_as(String channel, String status) {
        intPage.checkSpecificStatusAs(channel, status);
    }

    @Given("I click on Configure {string}")
    public void i_click_on_configure_email_setup(String channel) {
        intPage.clickOnConfigure(channel);
    }

    @Given("I select {string} ESP to create with values with name set as {string}")
    public void i_select_esp_to_create_with_values(String esp, String configName, DataTable dataTable) {
        intPage.selectServiceProviderandSetName(esp, configName, false);
        List<List<String>> choices = dataTable.asLists(String.class);
        intPage.fillFields(esp, choices, true);
        intPage.makeESP();
    }

    @Then("I verify SP config has been created")
    public void i_verify_sp_config_has_been_created() {
        intPage.checkList();
    }

    @And("I delete the {string} ESP setup")
    public void i_delete_the_esp_setup(String configName) {
        intPage.deleteSP(configName, "ESP");
    }

    @Given("I click on View REST API page")
    public void i_click_on_view_rest_api_page() {
        intPage.openRestApiPage();
    }

    @Then("I verify whether the license code and key is displayed")
    public void i_verify_whether_the_license_code_and_key_is_displayed() {
        intPage.verifyApiPage();
    }

    @Then("I verify the status of each of this Channels")
    public void i_verify_the_status_of_each_of_this_channels(DataTable dataTable) {
        List<List<String>> choices = dataTable.asLists(String.class);
        intPage.verifyChannelStatus(choices.get(0));
    }

    @Then("I switch {string} Web Personalization")
    public void i_switch_web_personalization(String switchStatus) {
        intPage.webPersonalizationToggle(switchStatus);
    }

    @Given("I select {string} SSP to create with values with name set as {string}")
    public void i_select_ssp_to_create_with_values_with_name_set_as(String ssp, String configName,
            DataTable dataTable) {
        intPage.selectServiceProviderandSetName(ssp, configName, false);
        List<List<String>> choices = dataTable.asLists(String.class);
        intPage.fillFields(ssp, choices, true);
        intPage.makeSSP();
    }

    @Then("I edit the {string} SP with name {string} with following configs")
    public void i_edit_the_sp_with_name_with_following_configs(String channel, String sspName, DataTable dataTable) {
        List<List<String>> choices = dataTable.asLists(String.class);
        intPage.editConfig(sspName, choices, channel.charAt(0) + "SP", null);
    }

    @Then("I edit {string} template with WSP as {string} with following configs")
    public void i_edit_whatsapp_template_with_following_configs(String templateName, String wspName,
            DataTable dataTable) {
        List<List<String>> choices = dataTable.asLists(String.class);
        intPage.editWhatsappTemplate(templateName, wspName, choices);
    }

    @Then("I delete the {string} SSP setup")
    public void i_delete_the_ssp_setup(String configName) {
        intPage.deleteSP(configName, "SSP");
    }

    @Given("I select {string} WSP to create with values with name set as {string}")
    public void i_select_wsp_to_create_with_values_with_name_set_as(String wsp, String configName,
            DataTable dataTable) {
        intPage.checkForDuplicate(configName);
        intPage.selectServiceProviderandSetName(wsp, configName, true);
        List<List<String>> choices = dataTable.asLists(String.class);
        intPage.fillFields(wsp, choices, true);
        intPage.makeWSP();
        intPage.WSPConfirmation();
    }

    @And("I delete the {string} WSP setup")
    public void i_delete_the_wsp_setup(String configName) {
        intPage.deleteSP(configName, "WSP");
    }

    @And("I delete the WA Template with name as {string} and WSP as {string}")
    public void i_delete_WA_template(String templateName, String wspName) {
        intPage.deleteWhatsappTemplate(templateName, wspName);
    }
    
	@And("I delete the Custom Template with name as {string}")
	public void i_delete_AppInline_template(String templateName) {
			intPage.deleteCustomTemplate(templateName);
	}

    @Then("I make WA Template for {string} with data as")
    public void i_make_wa_template_for_with_data_as(String wspName, DataTable dataTable) {
        intPage.makeWATemplate(wspName);
        List<List<String>> choices = dataTable.asLists(String.class);
        intPage.fillFields(wspName, choices, true);
        intPage.addTemplate();
    }
    
    @Then("I create AppInline Custom Template with data as")
    public void createAppInlineTemplate(DataTable dataTable) {
    	List<List<String>> choices = dataTable.asLists(String.class);
    		intPage.createCustomTemplate();
    		intPage.fillFields("", choices, false);
        	intPage.saveTemplate("Create");
    }
    
	@Then("I {string} AppInline Custom Template with name {string} with data as")
	public void viewEditDuplicateAppInlineTemplates(String templateAction, String templateName, DataTable dataTable)
			throws InterruptedException {
		List<List<String>> choices = dataTable.asLists(String.class);
		intPage.viewEditDuplicateCustomTemplate(templateAction, templateName, choices);
		
	}

    @Given("I check SDK Integration block")
    public void i_check_sdk_integration_block() {
        intPage.verifySDKBlock();
    }

    @Given("I click on Configure for SDK integration block")
    public void i_click_on_configure_for_sdk_integration_block() {
        intPage.clickOnConfigure("SDK Integration Status");
    }

    @Then("I check Integration Status for {string}")
    public void i_check_integration_status_for(String tab) {
        intPage.openTab(tab);
    }

    @Then("I switch {string} Web Push")
    public void i_switch_web_push(String switchStatus) {
        intPage.webPushToggle(switchStatus);
    }

    @Given("I fill in following details for Android Credentials")
    public void i_fill_in_following_details_for_android_credentials(DataTable dataTable) {
        List<List<String>> choices = dataTable.asLists(String.class);
        intPage.addCredsFor("Android");
        intPage.fillFields(null, choices, false);
    }

    @Then("save and verify Android Credentials")
    public void save_and_verify_android_credentials() {
        intPage.savePushCreds("Android");
    }

    @Then("save and verify iOS Credentials")
    public void save_and_verify_iOS_credentials() {
        intPage.savePushCreds("iOS");
    }

    @Then("I fill in following details for iOS crdentials and upload {string} AuthKey")
    public void i_fill_in_following_details_for_i_os_crdentials_and_upload_auth_key(String authKey,
            DataTable dataTable) {
        List<List<String>> choices = dataTable.asLists(String.class);
        intPage.addCredsFor("iOS");
        intPage.fillFields(null, choices, false);
        intPage.uploadAuthKey(authKey);
    }

    @Then("I delete Android Cred having package name as {string}")
    public void i_delete_android_cred_having_package_name_as(String packageName) {
        intPage.verifyPushConfigDelete(packageName, "Android");
    }

    @Then("I delete iOS Cred having package name as {string}")
    public void i_delete_i_os_cred_having_package_name_as(String bundleName) {
        intPage.verifyPushConfigDelete(bundleName, "iOS");
    }

    @Then("I upload custom Font {string}")
    public void i_upload_custom_font(String fileName) {
        intPage.setFontName(fileName);
        intPage.uploadFont(fileName);
    }

    @Then("I verify uploaded font has been listed")
    public void i_verify_uploaded_font_has_been_listed() {
        intPage.verifyFontInListing(true);
    }

    @Then("I delete the font {string}")
    public void i_delete_the_font(String fontName) {
        intPage.deleteFont(fontName);
    }

    @Then("verify deleted font is not listed")
    public void verify_deleted_font_is_not_listed() {
        intPage.verifyFontInListing(false);
    }

    @Given("I click on Configure for Webhooks block")
    public void i_click_on_configure_for_webhooks_block() {
        intPage.clickOnConfigure("Webhooks");
    }

    @Then("I configure {string} with data as and hit Save")
    public void i_configure_with_data_as_and_hit(String webhookName, DataTable dataTable) {
        Map<String, String> choices = dataTable.asMap(String.class, String.class);
        intPage.checkAddWebhook(webhookName);
        intPage.addWebhook(webhookName, choices.get("URL"), choices.get("Format"));
        intPage.checkConfiguredWebhook(webhookName, choices.get("URL"), choices.get("Format"));
    }

    @Then("I configure {string} with data as and hit Delete")
    public void i_configure_with_data_as_and_hit(String webhookName) {
        intPage.deleteWebhook(webhookName);
    }

    @Then("I edit the {string} Push Cred config having name {string} with following configs")
    public void i_edit_the_push_cred_config_having_name_with_following_configs(String osType, String configname,
            DataTable dataTable) {
        List<List<String>> choices = dataTable.asLists(String.class);
        intPage.editPushConfig(osType, configname, choices);
    }

    @Given("I go to SDK integration page")
    public void i_go_to_sdk_integration_page() {
        intPage.openSdkIntegrationPage();
    }

    @Then("verify the links for {string} OS")
    public void verify_the_links_for_os(String osType) {
        intPage.openLinks(osType);
        intPage.verifyPushPages(osType);
    }

    @Then("verify the package for {string} OS and {string}")
    public void verify_the_package_for_os_and(String osType, String packageName) {
        intPage.openLinks(osType);
        intPage.verifyValidPackages(osType, packageName);
    }

    @Then("verify the {string} Text for Invalid package for following {string}  OS and {string}")
    public void verify_the_text_for_invalid_package_for_following_os_and(String message, String osType,
            String packageName) {
        intPage.openLinks(osType);
        intPage.verifyInvalidPackages(osType, packageName, message);
    }

    @Then("I upload the file for {string}")
    public void i_upload_the_user_data_file(String dataType) throws InterruptedException {
        intPage.uploadData(dataType);
    }

    @Then("I verify upload status as {string}")
    public void i_verify_upload_status_as(String expectedStatus) throws InterruptedException {
        intPage.uploadStatus(expectedStatus);
    }

    @And("I verify the file upload status via API")
    public void i_verify_the_stats() {
        intPage.verifyStatusViaAPI();

    }

    @Then("I add the following Push Service details")
    public void i_add_the_following_push_service_details(DataTable dataTable) {
        List<List<String>> choices = dataTable.asLists(String.class);
        intPage.addCreds(choices);
    }

    @Then("I verify following Push Services are present")
    public void i_verify_following_push_services_are_addded_or_not(DataTable dataTable) {
        List<List<String>> choices = dataTable.asLists(String.class);
        intPage.verifyPushServices(choices);
    }

    @Then("I click on remove icon for following Push Services")
    public void i_click_on_remove_icon_for_following_push_services(DataTable dataTable) {
        List<List<String>> choices = dataTable.asLists(String.class);
        intPage.removePushServices(choices);
    }

    @Then("I verify following Push Services are deleted")
    public void i_verify_following_push_services_are_deleted(DataTable dataTable) {
        List<List<String>> choices = dataTable.asLists(String.class);
        intPage.verifyDeletedPushServices(choices);
    }

    @Then("I edit the {string} Push Cred config having name {string}")
    public void i_edit_the_push_cred_config_having_name(String osType, String configname) {
        intPage.clickOnEditPackages(osType, configname);
    }

    @Then("I select {string} template  with SP name as {string}")
    public void i_select_template_with_sp_name_as(String templateType, String SSPName) {
        intPage.addTemplateType(templateType, SSPName);
    }

    @Then("I search and delete whatsapp template {string}")
    public void i_search_and_delete_whatsapp_template(String templateName) {
        intPage.searchAndDeleteTemplate(templateName);
    }

    @And("I verify toast notification after saving Web Push Configuration")
    public void i_verify_toast_notification_after_saving_Web_Push_Configuration(){
    	intPage.verifyWebPushToastMessage();
    }
    
    @And("I reset the WebPush Config Text Message back to original")
    public void i_reset_the_webpush_config_text_message_back_to_original(){
        intPage.resetWebPushConfigMessage();
    }
}
