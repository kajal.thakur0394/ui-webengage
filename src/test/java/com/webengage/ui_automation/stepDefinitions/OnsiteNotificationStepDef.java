package com.webengage.ui_automation.stepDefinitions;

import java.util.List;

import com.webengage.ui_automation.pages.CommonPage;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import org.json.simple.JSONObject;
import com.webengage.ui_automation.hooks.Hooks;
import com.webengage.ui_automation.pages.CampaignsPage;
import com.webengage.ui_automation.pages.OnsiteNotificationPage;
import com.webengage.ui_automation.utils.ReflectionsUtility;

import io.cucumber.java.en.Then;

public class OnsiteNotificationStepDef {
	CampaignsPage campPage = new CampaignsPage();
	OnsiteNotificationPage onsitePage = new OnsiteNotificationPage();
	CommonPage commonPage = new CommonPage();
	ReflectionsUtility refl = new ReflectionsUtility();

	@Then("I add {string} to the searchbox")
	public void i_add_to_the_searchbox(String campaignName) {
		onsitePage.searchBar(campaignName);
	}

	@Then("I verify that {string} and the {string} exist")
	public void i_verify_that_and_the_exist(String campaignName, String layoutType) {
		onsitePage.verifyCampaignNameandType(campaignName, layoutType);
	}

	@Then("create a {string} Notification")
	public void create_a_notification(String name) {
		onsitePage.createNotification(name);
	}

	@Then("create a {string} Notification and select {string} template")
	public void create_a_notification_and_select_template(String notificationType, String templateType) {
		onsitePage.createNotification(notificationType);
		onsitePage.templateName(templateType, notificationType);
	}

	@Then("use the personalization template {string} for {string} web-p")
	public void use_the_personalization_template_for_web_p(String fileName, String campaignType) {
		JSONObject template = refl.fetchTemplate(Hooks.scenarioName, "PersonalizationTemplates", fileName);
		CampaignsPage.setJsonTemplate(template.toString());
		onsitePage.checkCampaignType(campaignType, template);
	}

	@Then("verify notification on a web page using template")
	public void verify_notification_on_a_web_page_using_template() {
		onsitePage.verifyOnsiteNotificationDetails(refl.getJSONTemplate());
	}

	@Then("delete the notification using template {string}")
	public void delete_the_notification_using_template(String fileName) {
		JSONObject template = refl.fetchTemplate(Hooks.scenarioName, "PersonalizationTemplates", fileName);
		onsitePage.deleteOnsiteNotification(template);
	}

	@And("I enter the notification campaign name as {string} and activate the campaign")
	public void iEnterTheNotificationCampaignNameAsAndActivateTheCampaign(String notificationCampaignName) {
		onsitePage.enterNotificationCampaignName(notificationCampaignName);
		onsitePage.activateNotificationCampaign();
	}

	@And("Verify Campaign tag name as {string} for {string} notification campaign")
	public void VerifyCampaignTagNameAsForNotificationCampaign(String expectedNotificationTagName,
			String notificationName) {
		onsitePage.verifyTagAttachedToOnsiteNotificationCampaign(expectedNotificationTagName, notificationName);
	}

	@And("I create new tag as {string} and attached to the notification campaign {string}")
	public void iCreateNewTagAsAndAttachedToTheNotificationCampaign(String notificationTagName,
			String notificationCampaignName) {
		onsitePage.setNotificationCampaignTag(notificationCampaignName, notificationTagName);
	}

	@And("I delete the notification campaign with Name {string}")
	public void deleteTheNotificationCampaignWithName(String campaignName) {
		onsitePage.deleteNotificationCampaign(campaignName);
	}

	@And("I set the campaign name as {string} and proceed ahead")
	public void enterTheNotificationCampaignNameAsAndClickOn(String notificationName) {
		onsitePage.enterNotificationCampaignName(notificationName);
	}

	@And("I create a new segment by using Traffic Segment as {string}")
	public void createANewSegmentByUsingTrafficSegmentWithTheUserCondition(String segmentName, DataTable dataTable) {
		List<List<String>> choices = dataTable.asLists(String.class);
		onsitePage.createNewSegment(segmentName, choices);
	}

	@And("I activate the notification campaign")
	public void activateTheNotificationCampaign() {
		onsitePage.activateNotificationCampaign();
	}

	@And("I select a existing segment from traffic segment as user condition {string}")
	public void selectAExistingSegmentFromTrafficSegmentAsUserCondition(String userCondition) {
		onsitePage.selectExistingSegment(userCondition);
	}

	@Then("Verify the duplicate campaign is created as {string}")
	public void VerifyTheDuplicateCampaignIsCreatedAs(String notificationCampaignName) {
		onsitePage.checkDuplicateCampaign(notificationCampaignName);
	}

	@Then("Verify the status of campaign {string} as {string}")
	public void VerifyTheStatusOfCampaignAs(String campaignName, String campaignStatus) {
		onsitePage.checkStatusOfCampaign(campaignName, campaignStatus);
	}

	@And("I {string} the {string} campaign")
	public void theCampaign(String campaignStatus, String campaignName) {
		onsitePage.changeOnsiteCampaignStatus(campaignName, campaignStatus);
	}

	@And("I update the campaign name from {string} to {string}")
	public void updatedTheCampaignNameFromTo(String oldNotificationCampaignName, String newNotificationCampaignName) {
		onsitePage.changeOnsiteCampaignName(oldNotificationCampaignName, newNotificationCampaignName);
	}

	@And("I check the {string} segment is attached to the {string} notification campaign")
	public void checkTheSegmentIsAttachedToTheNotificationCampaign(String segmentName,
			String notificationCampaignName) {
		onsitePage.verifySegmentAndOnsiteCampaignId(segmentName, notificationCampaignName);
	}

	@And("I open the {string} notification campaign")
	public void openTheNotificationCampaign(String notificationCampaignName) {
		onsitePage.editOnsiteCampaign(notificationCampaignName);
		campPage.saveCampaignID(false);
	}

	@Then("Verify the updated on-site campaign ID")
	public void verifyTheUpdatedOnSiteCampaignID() {
		onsitePage.verifyUpdateOnsiteCampaignId();
	}

	@And("I create a duplicate campaign of {string}")
	public void iCreateADuplicateCampaignOf(String onSiteCampaignName) {
		onsitePage.createDuplicateOnSiteCampaign(onSiteCampaignName);
	}
}
