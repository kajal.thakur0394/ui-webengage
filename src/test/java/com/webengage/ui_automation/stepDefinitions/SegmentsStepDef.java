package com.webengage.ui_automation.stepDefinitions;

import java.util.List;
import java.util.Map;

import com.webengage.ui_automation.pages.CampaignsPage;
import com.webengage.ui_automation.pages.CommonPage;
import com.webengage.ui_automation.pages.SegmentsPage;
import com.webengage.ui_automation.utils.NotificationToastMessages;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class SegmentsStepDef {
	SegmentsPage segPage = new SegmentsPage();
	CampaignsPage campPage = new CampaignsPage();
	CommonPage commPage = new CommonPage();

	@Then("I create a Live segment with name as {string}")
	public void i_create_a_live_segment_with(String segName) {
		segPage.newSegment(segName);
	}

	@And("Pass following details in user card")
	public void pass_following_details_in_user_card(DataTable dataTable) {
		List<List<String>> choices = dataTable.asLists(String.class);
		segPage.fillChoicesUserCard(choices);
	}

	@Then("I click on refresh button and check {string} count as {int}")
	public void i_click_on_refresh_button_and_check_count_as(String type, Integer count) {
		segPage.checkSegementDetails(type, count);
	}

	@Then("Save the Segment")
	public void save_the_segemnt() {
		segPage.saveSegment();
	}

	@Then("I delete segment with Name {string}")
	public void i_delete_segment_with_name(String segName) {
		segPage.deleteSegment(segName, "Delete Segment", NotificationToastMessages.DELETE_SEGMENT.message());
	}

	@Then("I delete segment from tooltip")
	public void i_delete_segment_from_tooltip() {
		segPage.deleteSegmentFromTooltip("Delete Segment", NotificationToastMessages.DELETE_SEGMENT.message());
	}

	@Then("I verify whether the segement has been deleted")
	public void i_verify_whether_the_segement_has_been_deleted() {
		segPage.checkIfNotInList();
	}

	@Given("perform {string} action for {string} segment on the listing page")
	public void perform_action_for_segment_on_the_listing_page(String action, String segment) {
		segPage.performActionforSegment(action, segment);
	}

	@Then("verify the popup for Segment Duplication and duplicate it in same project itself")
	public void verify_the_popup_for_segment_duplication_and_duplicate_it_in_project() {
		segPage.duplicateContainer();
	}

	@Then("verify that the segment {string} gets Listed on the page")
	public void verify_that_the_segment_gets_listed_on_the_page(String segName) {
		segPage.checkList(segName);
	}

	@Then("I delete the following mentioned {string}")
	public void i_delete_the_following_mentioned_segments(String segmentType, DataTable dataTable) {
		String type = segmentType.equalsIgnoreCase("lists") ? "Delete List" : "Delete Segment";
		String toastMessage = segmentType.equalsIgnoreCase("lists")
				? NotificationToastMessages.DELETE_STATIC_SEGMENT.message()
				: NotificationToastMessages.DELETE_SEGMENT.message();
		List<String> segmentList = dataTable.asList(String.class);
		for (String segment : segmentList) {
			segPage.deleteSegment(segment, type, toastMessage);
		}
	}

	@Given("I Open the Segment {string}")
	public void i_open_the_segment(String segName) {
		segPage.openSegment(segName);
		segPage.getSegmentID();
	}

	@Then("I click on the {string} tool-tip")
	public void i_click_on_the_tool_tip(String button) {
		segPage.clickOnToolTipButton(button);
	}

	@Then("Pass following details in Behavioural card")
	public void pass_following_details_in_bhea_card(DataTable dataTable) throws InterruptedException {
		List<List<String>> choices = dataTable.asLists(String.class);
		segPage.fillChoicesBehCard(choices);
	}

	@Then("I Edit the Segment")
	public void i_edit_the_segment() {
		commPage.clickButton("Edit");
	}

	@Then("check whether the segment has been {string} and gets listed on the page.")
	public void check_whether_the_segment_has_been_and_gets_listed_on_the_page(String type) {
		segPage.checkList(type, true);
	}

	@Then("check whether the static segment has been {string} and gets listed on the page.")
	public void check_whether_the_static_segment_has_been_and_gets_listed_on_the_page(String type) {
		segPage.checkList(type, false);
	}

	@Then("I verify {string} count as {int}")
	public void i_verify_count_as(String type, Integer count) {
		segPage.verifyUsersCount(type.toUpperCase(), count);
	}

	@Then("Pass following details for {string} in Technology card")
	public void pass_following_details_in_tech_card(String type, DataTable dataTable) {
		List<List<String>> choices = dataTable.asLists(String.class);
		segPage.fillChoicesTechCard(type, choices);
	}

	@Given("I navigate to {string} tab on Users Page")
	public void i_navigate_tab_on_users_page(String tab) {
		segPage.openUserTab(tab);
	}

	@Then("I add {string} to search")
	public void i_add_to_search(String keyword) {
		segPage.searchBar(keyword);
	}

	@Then("verify the count of row as {int} and User Id as {string}")
	public void verify_the_count_of_row_as_and_user_id_as(Integer count, String userID) {
		segPage.verifyUserRow(count, userID);
	}

	@Then("I apply filter and select following columns")
	public void i_apply_filter_and_select_following_columns(DataTable dataTable) {
		List<List<String>> choices = dataTable.asLists(String.class);
		segPage.selectFilters(choices.get(0));
	}

	@Then("check for presence of columns on listing page")
	public void check_for_presence_of_columns_on_listing_page(DataTable dataTable) {
		List<List<String>> choices = dataTable.asLists(String.class);
		segPage.verifyColumns(choices.get(0));
	}

	@Then("I create a Static Segment using {string} with name {string}")
	public void i_create_a_static_segment_using_segment_editor_with_name(String type, String name) {
		segPage.newStaticSegment(type, name);
	}

	@Then("I verify that the Static segment is displayed on Listing Page")
	public void i_verify_that_the_static_segment_is_displayed_on_listing_page() {
		segPage.verifyStaticSegments();
	}

	@Then("I delete static segment with Name {string}")
	public void i_delete_static_segment_with_name(String name) {
		segPage.deleteSegment(name, "Delete List", NotificationToastMessages.DELETE_STATIC_SEGMENT.message());
	}

	@And("I apply the filter for {string} as")
	public void i_apply_the_filter_for_as(String type, DataTable dataTable) {
		List<List<String>> choices = dataTable.asLists(String.class);
		segPage.addFilter(type, choices.get(0));
	}

	@Then("I enter segment name {string} and select file from test data to upload")
	public void i_enter_segment_name_and_select_file_from_test_data_to_upload(String segName) {
		segPage.uploadFile(segName);
	}

	@Then("I download the list and verify it")
	public void i_download_the_list_and_verify_it() {
		commPage.downloadListOfUsers();
	}

	@Given("I search for {string} segment")
	public void i_search_for_segment(String segName) {
		segPage.setSegmentName(segName);
		segPage.verifySegment(segName);
	}

	@Then("I verify {string} tag is attached to the {string} segment")
	public void i_verify_tag_is_attached_to_the_segment(String segmentTag, String segmentName) {
		campPage.verifyTagsInBothPages(segmentTag, null, segmentName);
	}

	@Then("create new tag with name as {string} for segment {string}")
	public void create_new_tag_with_name_as_for_segment(String tagName, String segName) {
		segPage.openMeatBallTagMenu(segName);
		campPage.createNewTag(tagName);
		segPage.saveButton();
	}

	@Then("create new tag with name as {string} for segment {string} from overview page")
	public void create_new_tag_with_name_as_for_segment_from_overview_page(String tagName, String segName) {
		campPage.createNewTag(tagName);
		segPage.saveButton();
	}

	@Then("delete the tag with name as {string} from segment {string}")
	public void delete_the_tag_with_name_as_from_segment(String tagName, String segName) {
		segPage.openMeatBallTagMenu(segName);
		campPage.deleteNewTag(tagName);
		// segPage.verifyTagIsDeletedFromList();
		segPage.verifyTagIsDeletedFromList(segName);
	}

	@Given("I search for {string} static segment")
	public void i_search_for_static_segment(String segName) {
		segPage.searchBar(segName);
		segPage.verifyStaticSegments();
	}

	@Then("I verify {string} tag is attached to the static segment")
	public void i_verify_tag_is_attached_to_the_static_segment(String tagName) {
		segPage.verifySegment(segPage.getSegmentName());
		campPage.assertTag(tagName);
	}

	@Then("create new tag from show details with name as {string} for segment {string}")
	public void create_new_tag_from_show_details_with_name_as_for_segment(String tagName, String segName) {
		segPage.openShowDetailsPage(segName);
		campPage.createNewTag(tagName);
		segPage.saveButton();
	}

	@Then("I did these events for following event type")
	public void i_did_these_events(DataTable dataTable) throws InterruptedException {
		List<List<String>> choices = dataTable.asLists(String.class);
		segPage.selectEventType(choices.get(0));
	}

	@Then("I verify {string} dropdown with all the values")
	public void i_verify_dropdown_with_all_the_values(String label) {
		segPage.verifyDropDownValues(label);
	}

	@Then("I refresh static segment with Name {string}")
	public void i_refresh_static_segment_with_name(String segName) {
	    segPage.searchBar(segName);
	    segPage.refreshSegment(segName);
	}

	@Then("verify {string} as {string} for {string} static List")
	public void verify_status_as_for_static_list(String category, String expectedValue, String segName) {
	    segPage.searchBar(segName);
		segPage.verifyRefreshingListCategory(category, expectedValue, segName);
	}
	
	@Then("I verify {string} action is present for {string}")
	public void i_verify_action_presence(String action, String segName) {
		segPage.searchBar(segName);
		segPage.verifyActionPresence(action, segName);
	}

	@Then("Pass following details in Refresh frequency")
	public void I_create_refresh_frequency(DataTable dataTable) {
	    Map<String, String> map = dataTable.asMap(String.class, String.class);
		segPage.refreshFrequency(map);
	}
	@Then("I save the SegmentId of {string} segment")
	public void i_save_the_segment_id_of_segment(String segName) {
	   segPage.saveSegmentID(segName);
	}

	@Then("check {string} popup item is {string} for {string}")
	public void check_popup_item_is_for(String popupItem, String visiblity, String name) {
	    segPage.searchBar(name);
	    segPage.verifyPopOverItemVisiblity(popupItem,visiblity,name);
	}
	
	@Then("verify segment is {string} with name {string}")
	public void _verify_segment_is_with_name(String type, String segmentName) {
		segPage.verifyauditLogValues(type, segmentName);
	}

	@Then("I update Live segment with name as {string}")
	public void i_update_a_live_segment_with(String segName) {
		segPage.updateSegmentName(segName);
	}
}
