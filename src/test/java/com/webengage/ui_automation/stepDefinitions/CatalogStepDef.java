package com.webengage.ui_automation.stepDefinitions;

import java.util.List;
import com.webengage.ui_automation.pages.CatalogsPage;
import com.webengage.ui_automation.pages.CommonPage;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;

public class CatalogStepDef {
	CatalogsPage catPage = new CatalogsPage();
	CommonPage comPage = new CommonPage();

	@Then("fill the Configuration details using below values")
	public void fill_the_configuration_details_using_below_values(DataTable dataTable) {
		List<List<String>> configData = dataTable.asLists(String.class);
		comPage.createIcon();
		catPage.fillConfigurationDetails(configData);
	}

	@Then("fill the Mapping details using below values")
	public void fill_the_mapping_details_using_below_values(DataTable dataTable) {
		List<List<String>> mappingData = dataTable.asLists(String.class);
		catPage.fillMappingDetails(mappingData);
	}

	@Then("fill the Preview details using below values")
	public void fill_the_preview_details_using_below_values(DataTable dataTable) {
		List<List<String>> previewData = dataTable.asLists(String.class);
		catPage.fillPreviewDetails(previewData);
	}

	@Then("map events to catalogs using below details and save the mapping for {string} catalog")
	public void map_events_to_catalogs_using_below_details_and_save_the_mapping_for_catalog(String name,
			DataTable dataTable) {
		List<List<String>> mapEventsToCatalogs = dataTable.asLists(String.class);
		comPage.searchModuleNameHavingUnicode(name);
		catPage.openMapEventsMenu(name);
		catPage.fillMapEventsToCatalogsDetails(mapEventsToCatalogs);
	}

}
