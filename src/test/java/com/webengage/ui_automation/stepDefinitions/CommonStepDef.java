package com.webengage.ui_automation.stepDefinitions;

import com.webengage.ui_automation.pages.SMSCampaignsPage;
import org.json.simple.JSONObject;

import com.webengage.ui_automation.pages.CampaignsPage;
import com.webengage.ui_automation.pages.CommonPage;
import com.webengage.ui_automation.pages.IntegrationsPage;
import com.webengage.ui_automation.utils.Log;
import com.webengage.ui_automation.utils.ReflectionsUtility;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

import java.io.IOException;
import java.util.List;

public class CommonStepDef {

	static JSONObject jsonObj;

	CommonPage commonPage = new CommonPage();
	IntegrationsPage intPage = new IntegrationsPage();
	SMSCampaignsPage smsCampPage = new SMSCampaignsPage();
	ReflectionsUtility refl = new ReflectionsUtility();
	CampaignsPage campPage = new CampaignsPage();

	@Then("{string} provider for {string} by adding details via Template and also validating the same")
	public void provider_for_by_adding_details_via_template_and_also_validating_the_same(String type, String channel) {
		commonPage.createOrUpdateProvider(type.contains("create"), "Onboarding SP Test", channel,
				channel.charAt(0) + "SP");
	}

	@Then("delete {string} config if it exists in {string}")
	public void delete_config_if_it_exists(String configName, String channel) {
		intPage.deleteSP(configName, channel.charAt(0) + "SP");
	}

	@And("check if SP gets created using invalid principle ID")
	public void check_if_sp_gets_created_using_invalid_principle_id() {
		commonPage.createOrUpdateProvider(true, "Onboarding SP Test", "SMS", "SSP_invalid");
	}

	@Then("{string} provider for {string} by adding invalid details from template {string} and also validating the same")
	public void provider_for_by_adding_invalid_details_from_template_and_also_validating_the_same(String type,
			String channel, String tempName) {
		if (refl.fetchTemplate(null, "IntegrationPayloads/" + channel.replace("Invalid", ""), tempName) != null) {
			Log.info("Validating creds creation via Invalid Attributes");
			commonPage.createOrUpdateProvider(type.contains("create"), "Onboarding SP Test", channel, tempName);
		}
	}

	@And("I download the report")
	public void i_download_the_report(List<List<String>> choices) throws InterruptedException, IOException {
		commonPage.downloadFile(choices);
	}

	@Then("I click on {string} tab")
	public void i_click_on_tab(String tabName) {
		commonPage.clickTab(tabName);
	}
	
	@Then("I click button {string}")
    public void i_click_button(String button) {
        commonPage.clickButton(button);
    }

	@Then("check overview page is loaded for {string} campaign")
	public void check_overview_page_is_loaded_for_campaign(String campaignName) {
		campPage.searchBar(campaignName);
		commonPage.verifyOverviewPageLoads(campaignName);
	}

	@Then("I pause the campaign with {string} api")
	public void i_pause_the_campaign_with_api(String apiName) {
		commonPage.pauseCampaignViaAPI(apiName);
	}

	@And("open the dynamic webpage and login")
	public void open_the_dynamic_webpage_and_login() throws InterruptedException {
		commonPage.openDynamicWebPageAndLogin(false);
	}

	@Then("verify emojis are present in the following fields")
	public void verify_emojis_are_present_in_the_following_fields(DataTable dataTable) throws InterruptedException {
		List<String> fieldList = dataTable.asList();
		commonPage.enterDetailsAndValidateEmoji(fieldList);
	}

	@Given("I navigate to {string} Page")
	public void i_navigate_to_page(String moduleName) {
		commonPage.openModule(moduleName);
	}
	
	@Then("verify {string} name which has unicode characters on Overview page for {string}")
	public void verify_name_which_has_unicode_characters_on_overview_page_for(String moduleName, String moduleType) {
		commonPage.searchModuleNameHavingUnicode(moduleName);
		commonPage.verifyModuleNameHavingUnicodeOnOverviewPage(moduleName,moduleType);
	}

	@Then("delete the module with Name {string}")
	public void delete_the_module_with_name(String name) {
		commonPage.searchModuleNameHavingUnicode(name);
		commonPage.deleteModule(name);
	}

	@And("I verify {string} Campaign name with unicode characters on overview page")
	public void verify_campaign_name_with_unicode_characters_on_overview_page(String campaignName) {
		campPage.searchCampaignWithUnicode(campaignName);
		campPage.verifyCampaignWithUnicode(campaignName);
	}

	@Then("I delete the Campaign with unicode characters {string}")
	public void i_delete_the_campaign_with_unicode_characters(String campaignName) {
		campPage.searchCampaignWithUnicode(campaignName);
		smsCampPage.deleteCampaign(campaignName);
	}
}
