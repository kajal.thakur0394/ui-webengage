package com.webengage.ui_automation.stepDefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import com.webengage.ui_automation.pages.ShopifyPage;

public class ShopifyStepDef {
    ShopifyPage shopify = new ShopifyPage();

    @Given("I add product {string} to cart")
    public void i_add_product_to_cart(String product) {
        shopify.addToCart(product);
    }

    @Then("I enter email {string} and fill information section")
    public void i_enter_email_and_fill_information_section(String email) {
        shopify.enterCheckoutInformation(email);
    }

    @Then("I fill bogus credit card details and complete the purchase")
    public void i_fill_bogus_credit_card_details_and_complete_the_purchase() {
        shopify.shopifyPayment();
    }

}
