package com.webengage.ui_automation.stepDefinitions;

import com.webengage.ui_automation.pages.TestDataCreation;

import io.cucumber.java.en.Given;

public class TestDataStepDef {
	
	TestDataCreation testDataCreation=new TestDataCreation();

	@Given("I select the function {string} for building the test data")
	public void i_select_the_function_for_building_the_test_data(String function) {
		testDataCreation.selectFunction(function);
	}
	
	@Given("I create all the users reuqired for test suite")
	public void i_create_all_the_users_reuqired_for_test_suite() {
		testDataCreation.createUsers();
	}

	@Given("I select the channel {string} for campaign creation")
	public void i_select_the_channel_for_campaign_creation(String channel) {
		testDataCreation.createCampaigns(channel);
	}
}
