package com.webengage.ui_automation.stepDefinitions;

import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.webengage.ui_automation.hooks.Hooks;
import com.webengage.ui_automation.pages.CampaignsPage;
import com.webengage.ui_automation.pages.CommonPage;
import com.webengage.ui_automation.pages.InLineContentWebPersPage;
import com.webengage.ui_automation.utils.ReflectionsUtility;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class InLineContentWebPerStepDef {
	InLineContentWebPersPage inlineWeb = new InLineContentWebPersPage();
	ReflectionsUtility refl = new ReflectionsUtility();
	CampaignsPage campPage = new CampaignsPage();
	CommonPage commonPage = new CommonPage();

	@Then("save the campaign")
	public void save_the_campaign() {
		inlineWeb.saveAndContinueButton();
	}

	@Then("select property {string} and {string} as property placement")
	public void select_property_and_as_property_placement(String propName, String propPlacement) {
		inlineWeb.selectPropertyDetails(propName, propPlacement);
	}

	@Then("select page to show as {string}")
	public void select_page_to_show_as(String pageToShow) {
		commonPage.clickOnGenericLabel(pageToShow);
	}
	
	@Then("I screen data as datatype using table")
	public void selectScreenData(DataTable dataTable) {
		Map<String, String> map = dataTable.asMap(String.class, String.class);
		inlineWeb.setScreenData(map);
	}

	@Then("use the in-line WebP template {string} to fill message details")
	public void use_the_in_line_web_p_template_to_fill_message_details(String fileName) throws ClassNotFoundException {
		JSONObject template = refl.fetchTemplate(Hooks.scenarioName, "PersonalizationTemplates", fileName);
		refl.readAndFillData((JSONArray) template.get("Basic"));
		inlineWeb.saveAndContinueButton();
	}

	@Then("verify in-line content on a web page using template {string} with {string} api")
	public void verify_in_line_content_on_a_web_page_using_template_with_api(String fileName, String apiName) {
		inlineWeb.verifyInlineContentDetails(refl.getJSONTemplate(), apiName);
	}

	@Then("check whether campaign creation is disabled for WebP and enable it again")
	public void check_whether_campaign_creation_is_disabled_for_web_p_and_enable_it_again() {
		inlineWeb.verifyCampaignCreationisDisabled();
	}

	@Given("verify select property is enabled and able to fill following details")
	public void verify_select_property_is_enabled_and_able_to_fill_following_details(DataTable dataTable) {
		List<List<String>> choices = dataTable.asLists(String.class);
		inlineWeb.fillAndVerifyPropertyDetails(choices);
	}

	@Then("I verify property Placement on a web page with {string} api")
	public void i_verify_property_placement_on_a_web_page_with_api(String apiName) {
		inlineWeb.verifyPropertyPlacement(apiName);
	}
	
	@Then("I verify personalised message on dynamic webpage")
	public void verify_personalised_message() throws InterruptedException{
		inlineWeb.verifyPersonalizedMessage();
	}
	
	@Then("I switch back to default window")
	public void switch_default_window() throws InterruptedException{
		inlineWeb.switchToDefaultWindow();
	}
}