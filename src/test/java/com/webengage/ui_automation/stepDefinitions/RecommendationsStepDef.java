package com.webengage.ui_automation.stepDefinitions;

import java.util.List;

import com.webengage.ui_automation.pages.CommonPage;
import com.webengage.ui_automation.pages.CustomDashboardPage;
import com.webengage.ui_automation.pages.RecommendationsPage;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class RecommendationsStepDef {
	RecommendationsPage recPage = new RecommendationsPage();
	CustomDashboardPage custDashPage = new CustomDashboardPage();
	CommonPage comPage = new CommonPage();

	@Given("create recommendations using details below")
	public void create_recommendations_using_details_below(DataTable dataTable) {
		List<List<String>> recDetails = dataTable.asLists(String.class);
		comPage.createIcon();
		recPage.fillRecommendationsDetails(recDetails);
	}

	@Then("verify {string} recommendations is created with {string} catalog name")
	public void verify_recommendations_is_created_with_catalog_name(String recName, String catalogName) {
		comPage.searchModuleNameHavingUnicode(recName);
		recPage.validateRecommendationPresent(recName,catalogName);
	}

}
