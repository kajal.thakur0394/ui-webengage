package com.webengage.ui_automation.stepDefinitions;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.json.simple.JSONObject;

import com.webengage.ui_automation.hooks.Hooks;
import com.webengage.ui_automation.pages.APIOperationsPage;
import com.webengage.ui_automation.pages.CommonPage;
import com.webengage.ui_automation.pages.JourneysPage;
import com.webengage.ui_automation.pages.SMSCampaignsPage;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class JourneysStepDef {
	JourneysPage journeyPage = new JourneysPage();
	CommonPage comPage = new CommonPage();
	APIOperationsPage apiOperations = new APIOperationsPage();
	SMSCampaignsPage smsPage = new SMSCampaignsPage();
	HashMap<String, String> journeyDetails = new HashMap<>();

	@Given("I create a new Journey\\/Relay with name as {string}")
	public void i_create_a_new_journey_with_name_as(String journeyName) throws InterruptedException {
		journeyPage.createJourney();
		journeyPage.setJourneyNameAs(journeyName);
		this.journeyDetails = journeyPage.setJourneyDetails();
	}

	@Then("I navigate back to {string} Page")
	public void i_navigate_back_to_Journeys_page(String page) throws InterruptedException {
		journeyPage.navigateBack(page);
	}

	@Then("I use the Journey\\/Relay Template {string}")
	public void i_use_the_journey_template(String fileName) throws InterruptedException {
		JSONObject template = journeyPage.buildJourney(Hooks.scenarioName, fileName);
		journeyPage.zoomOut();
		journeyPage.transform(template, true);
		boolean isSplitThere = journeyPage.connectPaths(template, true);
		journeyPage.zoomOut();
		journeyPage.saveJourney();
		TimeUnit.SECONDS.sleep(3);
		journeyPage.saveStateIDs(template);
		if (isSplitThere)
			journeyPage.configureSplitBlock(template);
		journeyPage.addBlockData(template);
		TimeUnit.SECONDS.sleep(3);
	}

	@And("I save the Journey as draft")
	public void i_save_journey_as_draft() throws InterruptedException {
		journeyPage.saveJourney();
	}

	@Then("I publish the {string}")
	public void i_publish_journey_or_relay(String type) {
		// Hooks.attachJourney=true;
		// journeyPage.captureJourney();
		journeyPage.setJourneyID();
		journeyPage.publishJourney(type, journeyDetails);
		apiOperations.addURLParams("journeyEId", journeyPage.getJourneyID());
	}

	@Given("verify status as {string} for {string} Journey")
	public void verify_status_as_for_journey(String status, String journeyName) {
		smsPage.searchBar(journeyName);
		journeyPage.validateStatus(status, journeyName);
	}

	@Then("{string} the {string} with name as {string}")
	public void action_on_the_journey_with_name_as(String action, String page, String journeyName) {
		smsPage.searchBar(journeyName);
		journeyPage.popOverAction(journeyName, action);
		if (action.equals("Sunset") || action.equals("Stop"))
			journeyPage.validateActionMessages(action, page);
	}

	@Then("I activate conversion tracking for event {string} with data as")
	public void i_activate_conversion_tracking_for_event_with_data_as(String event, DataTable dataTable) {
		List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
		journeyPage.setConversionTracking(event, data.get(0));
	}

	@Given("set exit trigger when {string}")
	public void set_exit_trigger_when_users_attribute_changes(String exitTriggerType, DataTable dataTable)
			throws InterruptedException {
		List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
		journeyPage.setExitTrigger(data.get(0), exitTriggerType);
	}

	@Then("hit API for Event trigger with reference id {string}")
	public void hit_api_for_event_trigger_with_reference_id(String refId) {
		journeyPage.triggerEventviaAPI(refId);
	}

	@Then("wait for {string} api to return response as {string} for path as {string}")
	public void wait_for_api_to_return_response_as_for_path_as(String api, String expectedResponse, String jsonPath) {
		try {
			apiOperations.waitForAPIResponse("default", api, jsonPath, expectedResponse);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Then("verify the Start Date on Journey Listing page for {string}")
	public void verify_the_start_date_on_journey_listing_page_for_as(String journeyName) {
		journeyPage.validateStartDate(journeyName);
	}

	@Then("verify the End Date on Journey Listing page for {string}")
	public void verify_the_end_date_on_journey_listing_page_for_as(String journeyName) {
		journeyPage.validateEndDate(journeyName);
	}

	@Then("open then Journey {string} and {string}")
	public void open_then_journey_and(String journeyName, String action) {
		smsPage.searchBar(journeyName);
		journeyPage.clickOnToolTip(journeyName, action);
	}

	@Then("I validate the Publish Later option of Journeys")
	public void i_validate_the_publish_later_option_of_journeys() {
		journeyPage.verifyPublishLater();
	}

	@Then("open the Live View of Journey {string}")
	public void open_the_live_view_of_journey(String journeyName) {
		smsPage.searchBar(journeyName);
		journeyPage.openLiveView(journeyName);
	}

	@Then("I publish the Journey for later by adding time in the format dd:hh:mm as")
	public void i_publish_the_journey_for_later_by_adding_time_in_the_format_dd_hh_mm_as(DataTable dataTable) {
		Map<String, String> dates = dataTable.asMap(String.class, String.class);
		journeyPage.publishJourneyLater(dates.get("Start Date"), dates.get("End Date"), journeyDetails);
		journeyPage.setJourneyID();
		apiOperations.addURLParams("journeyEId", journeyPage.getJourneyID());
	}

	@And("verify the dates in Schedule card for journey {string}")
	public void verify_the_dates_in_schedule_card_for_journey(String journeyName) {
		journeyPage.verifySchedeuledCard(journeyName);
	}

	@Given("modify the scheduled dates for journey {string} to be")
	public void modify_the_scheduled_dates_to_be(String journeyName, DataTable dataTable) {
		Map<String, String> dates = dataTable.asMap(String.class, String.class);
		journeyPage.modifyScheduleDates(journeyName, dates.get("Start Date"), dates.get("End Date"));
	}

	@Then("verify the dates in Audit Log section for the Journey {string}")
	public void verify_the_dates_in_audit_log_scection_for_the_journey(String journeyName) {
		smsPage.searchBar(journeyName);
		journeyPage.verifyAuditLogSection(journeyName);
	}

	@Steps
	APIOperationsPage apiOperationsStep;

	@Then("verify the stats of Journey by checking overall stats in api response")
	public void verify_the_stats_of_journey_by_checking_stats_api_response() {
		String api = "journeyStats";
		String enteredCount = ((JSONObject) journeyPage.getjourneyTemplate().get("stats")).get("entered").toString();
		String exitedCount = ((JSONObject) journeyPage.getjourneyTemplate().get("stats")).get("exited").toString();
		try {
			apiOperationsStep.waitForAPIResponse("8", api, "response.data.contents[0].stats.entered", enteredCount);
			apiOperationsStep.waitForAPIResponse("8", api, "response.data.contents[0].stats.exited", exitedCount);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Then("perform Journey validation via {string} mechanism")
	public void validate_the_user_count_on_individual_block_level(String type) {
		try {
			apiOperationsStep.getMethod("journey");
			HashMap<String, String> blockIds = journeyPage.fetchBlockIds();
			journeyPage.validateEachBlock(blockIds, type);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Then("I verify following details in {string} Journeys")
	public void i_verify_following_details_in_journeys(String journeyName, DataTable dataTable) {
		List<List<String>> choices = dataTable.asLists(String.class);
		smsPage.searchBar(journeyName);
		comPage.openCampaign(journeyName);
		journeyPage.verifyJourneyNameinOverViewPage(journeyName);
		journeyPage.verifyMultiLingualCampaigns(choices);

	}

	@Then("I click on journey settings by selecting {string} with name as {string}")
	public void i_click_on_journey_settings(String segmentType, String segmentName) {
		journeyPage.openJourneySettings(segmentType, segmentName);
	}

	@Then("I Publish {string} Relay with name {string}")
	public void i_Publish_Relay_with_name(String status, String journeyName) {
		smsPage.searchBar(journeyName);
		journeyPage.changeState(status);
	}

	@Then("verify status as {string} for {string} of {string}")
	public void verify_status_as_for_of(String status, String journeyName, String page) {
		if (page.equalsIgnoreCase("Journeys")) {
			smsPage.searchBar(journeyName);
			journeyPage.validateStatus(status, journeyName);
		}
	}

	@Then("I delete the Journey via API Delete method")
	public void deleteJourneyWithApi() throws IOException {
		apiOperations.addURLParams("journeyId", journeyPage.getJourneyID());
		apiOperations.deleteMethod("deleteJourney");
	}
}