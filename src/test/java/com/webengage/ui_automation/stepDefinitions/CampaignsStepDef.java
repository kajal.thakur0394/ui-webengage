package com.webengage.ui_automation.stepDefinitions;

import java.io.IOException;
import java.util.List;

import com.webengage.ui_automation.hooks.Hooks;
import com.webengage.ui_automation.pages.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import com.webengage.ui_automation.utils.ReflectionsUtility;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class CampaignsStepDef {
	CampaignsPage campPage = new CampaignsPage();
	SegmentsPage segPage = new SegmentsPage();
	SMSCampaignsPage smsCampPage = new SMSCampaignsPage();
	ReflectionsUtility refl = new ReflectionsUtility();
	WhatsAppCampaignPage whatsappCampPage = new WhatsAppCampaignPage();

	@Given("I select New Segment Creation icon")
	public void i_select_new_segment_creation_icon() {
		campPage.selectSegmentCreationButton();
	}

	@Given("I navigate to {string} page via {string}")
	public void i_navigate_to_the_page(String subSection, String section)
			throws IOException, InterruptedException, ParseException {
		campPage.openPageOnDashboard(subSection, section);
	}

	@Then("I add {string} as {string}")
	public void i_add_segment_name_as(String segmentType, String segmentName) {
		campPage.addSegmentName(segmentType, segmentName);
	}

	@Then("I Save and verify the created {string}")
	public void i_save_and_verify_the_created_segment(String segmentType) {
		campPage.saveButton();
		campPage.verifySegment(segmentType);
	}

	@Then("Pass following details in user card And I Save the {string}")
	public void pass_following_details_in_user_card_and_i_save_the_segment(String segmentType, DataTable dataTable) {
		List<List<String>> choices = dataTable.asLists(String.class);
		segPage.fillChoicesUserCard(choices);
		segPage.saveSegment();
		campPage.verifySegment(segmentType.toLowerCase());
	}

	@Then("I select {string} as {string} Inclusion Segment")
	public void i_select_as_inclusion_segment(String name, String type) {
		campPage.selectSegmentUsers(name, type);
		campPage.selectSegmentCreationButton();
	}
	
	@Then("I click on Segment creation button")
	public void i_click_on_segment_creation() {
		campPage.selectSegmentCreationButton();
	}

	@Then("I select {string} as {string} Exclusion Segment")
	public void i_select_as_exclusion_segment(String name, String type) {
		campPage.selectCriteria(type.toLowerCase().contains("exclude"));
		campPage.selectSegmentUsers(name, type);
		campPage.selectExcludeSegmentCreationButton();
	}

	@Then("put in the message details as")
	public void put_in_the_message_details_as(DataTable dataTable) {
		List<List<String>> choices = dataTable.asLists(String.class);
		campPage.fillMessage(choices);
	}

	@Then("I delete campaign with name {string}")
	public void i_delete_campaign_with_name(String campaignName) {
		campPage.deleteCampaign(campaignName);
	}

	@Then("I select WA Template having name {string} with WSP name {string} and fill the Message Content")
	public void i_select_wa_template_having_name_and_fill_message_content(String templateName, String wspName,
			DataTable dataTable) throws InterruptedException {
		campPage.selectWATemplate(templateName, wspName);
		List<List<String>> choices = dataTable.asLists(String.class);
		whatsappCampPage.fillMessageTab(choices);
		campPage.setConversionTracking();
	}

	@Then("I select WA Template having name {string} with WSP name {string}")
	public void i_select_wa_template_having_name_with_wsp_name(String templateName, String wspName)
			throws InterruptedException {
		campPage.selectWATemplate(templateName, wspName);
	}

	@Then("I select Target Device as {string} and also verify {string} as disabled")
	public void i_select_target_device_as_and_also_verify_as_disabled(String platform, String disabledPlatforms) {
		campPage.verifyTargetDevices(platform, disabledPlatforms);
	}

	@Then("verify via {string} API whether following {string} have been attached to campaign just created")
	public void verify_via_api_whether_following_segments_have_been_attached_to_campaign_just_created(String apiName,
			String segmentType, DataTable dataTable) {
		campPage.verifyViaApiAttachedSegments(apiName, segmentType, dataTable);
	}

	@Given("I search for {string} campaign")
	public void i_search_for_campaign(String campaignName) {
		campPage.searchBar(campaignName);
	}

	@Then("I verify {string} tag is attached to the {string} campaign")
	public void i_verify_tag_is_attached_to_the_campaign(String tagName, String campaignName) {
		String status = smsCampPage.getCampaignStatus();
		campPage.verifyTagsInBothPages(tagName, status, campaignName);
	}

	@Given("create new tag from with name as {string} for campaign")
	public void create_new_tag_from_with_name_as_for_campaign(String tagName) {
		campPage.createNewTag(tagName);
	}

	@Then("delete the tag with name as {string} from campaign {string}")
	public void delete_the_tag_with_name_as_from_campaign(String tagName, String campaignName) {
		campPage.openMeatBallTagMenuForCampaign(campaignName);
		campPage.deleteNewTag(tagName);
		segPage.verifyTagIsDeletedFromList(campaignName);
	}

	@Then("select Segment having name as {string}")
	public void select_segment_having_name_as(String segName) {
		campPage.selectSingleSegment(segName);
	}

	@Then("fill the page via {string} template from {string} folder using {string} section")
	public void fill_the_page_via_template_from_folder_using_section(String fileName, String location, String section)
			throws ClassNotFoundException {
		JSONObject template = refl.fetchTemplate(Hooks.scenarioName, location, fileName);
		refl.readAndFillData((JSONArray) template.get(section));
	}

	@Then("I select {string} Campaign Type and enters {string} Campaign Name")
	public void i_select_campaign_type_and_enters_campaign_name(String campaignType, String campaignName) {
		campPage.createCampaignWithType(campaignType, campaignName);
	}

	@Then("I launch the Campaign")
	public void i_launch_the_email_campaign() throws InterruptedException {
		campPage.saveCampaignID(true);
		campPage.launchCampaignButton();
	}

	@And("export the template for Validation on Mobile Device")
	public void export_the_template_for_validation_on_mobile_device() {
		refl.writeExpectedintoFile();
	}

	@Then("I refresh the page")
	public void i_refresh_the_page() {
		campPage.refresh();
	}

	@Then("I validate following things in Preview section")
	public void i_validate_following_things_in_preview_section(DataTable dataTable) {
		List<String> assertChoices = dataTable.asList(String.class);
		smsCampPage.previewCampaign(assertChoices);
	}

	@Then("I verify following details for {string} campaign")
	public void i_verify_following_details_for_campaign(String campaignName, DataTable dataTable) {
		List<String> assertChoices = dataTable.asList(String.class);
		smsCampPage.verifyCampaignStatus(assertChoices, campaignName);
	}

	@Then("I fill the details in WHEN tab as below")
	public void i_fill_the_details_in_when_tab_as_below(List<List<String>> choices) throws InterruptedException {
		smsCampPage.fillWhenDetails(choices);
		String choicesString = choices.toString();
		if (choicesString.contains("FC") || choicesString.contains("QUEUEING") || choicesString.contains("DND")
				|| choicesString.contains("THROTTLING"))
			smsCampPage.fillFCQueueingDetails(choices);
		smsCampPage.saveAndContinueButton();
	}

	@And("test the {string} Campaign for segment {string} and expect the status to be {string}")
	public void test_the_campaign_for_segment_and_expect_the_status_to_be(String channel, String segmentName,
			String expectedStatus) {
		campPage.saveCampaignID(true);
		campPage.initiateCampaignTest(campPage.selectTestSegment(segmentName));
		try {
			campPage.validateTestCampaignOutput(campPage.fetchVariationID(channel),
					campPage.fetchTestSegmentId(segmentName), expectedStatus);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	@Then("{string} test campaign with following parameters")
	public void create_test_campaign_with_following_parameters(String createOrEdit, DataTable dataTable) {
		campPage.createOrUpdateTestSegment(createOrEdit, dataTable.asMap(String.class, String.class));
	}

	@Then("delete Test Segment with name as {string}")
	public void delete_test_segment_with_name_as(String testSegName) {
		campPage.openBreadCrumb("Test Campaign");
		campPage.selectTestSegment(testSegName);
		campPage.deleteTestSegment(testSegName);
		campPage.saveAndContinueButton();
	}

	@Then("I verify {string} toggle button is {string}")
	public void i_verify_toggle_button_is(String button, String visiblity) {
		campPage.verifyToggleButton(button, visiblity);
	}

	@Then("I select tab {string} in campaign crud page")
	public void i_select_tab_in_campaign_crud_page(String tab) {
		campPage.openBreadCrumb(tab);
	}

	@Then("I verify reachable user count as {string}")
	public void i_verify_reachable_user_count_as(String reachableUser) {
		campPage.verifyReachableUser(reachableUser);
	}

	@Then("I switch the namespace")
	public void i_switch_the_namespace() {
		campPage.switchNamespace();
	}

	@Then("I select {string} as {string} Segment for search")
	public void i_select_as_inclusion_segment_for_search(String name, String type) {
		campPage.selectCriteria(type.toLowerCase().contains("exclude"));
		campPage.selectSegmentUsers(name, type);
	}

	@Then("select Segment having name as {string} for {string}")
	public void select_segment_having_name_for_exclude(String segName, String includeExclude) {
		campPage.selectSpecificSegmentIncludeExclude(segName);
	}

	@Then("select multiple segments for {string} section")
	public void select_multiple_segments_for_Include_or_Exclude_section(String includeExclude, DataTable dataTable) {
		List<String> choices = dataTable.asList(String.class);
		campPage.selectMultipleSegment(includeExclude.toLowerCase().contains("include"), choices);
	}

	@Then("verify difference between all user and included usercount {int}")
	public void difference_between_all_user_and_included_usercount(int count) {
		campPage.validateReachableUserCount(count);
	}

	@Then("fetch the count of reachable users")
	public void verify_count_of_reachable_user() {
		campPage.fetchAndSetReachableUsers();
	}

	@Then("I select {string} Segment with name {string}")
	public void i_verify_save_and_continue_button_is_disabled(String type, String segmentName) {
		campPage.selectCriteria(type.toLowerCase().contains("exclude"));
		campPage.selectSpecificSegmentIncludeExclude(segmentName);
	}

	@Then("I remove {string} segment")
	public void i_remove_inlcusion_segment(String type) {
		campPage.removeInclusionExclusionSegment(type);
	}

	@Then("I verify {string} button is {string}")
	public void i_verify_button_is_enabled_or_disabled(String button, String type) {
		campPage.isButtonDisabledOrEnabled(button, type);
	}

	@Then("I save TemplateID with name {string}")
	public void saveTemplateId(String templateName) throws IOException {
		String templatId = campPage.getTemplateIdFromTemplateName(templateName);
		CampaignsPage.setTemplateId(templatId);
	}
}