package com.webengage.ui_automation.stepDefinitions;
import com.webengage.ui_automation.pages.InLineContentAppPersPage;
import io.cucumber.java.en.Then;

public class InLineContentAppPerStepDef {
	InLineContentAppPersPage inlineApp = new InLineContentAppPersPage();

	@Then("select {string} as property")
	public void select_as_property(String property) {
		inlineApp.selectProperty(property);
	}

	@Then("I select {string} layout")
	public void I_select_layout(String layout) {
		inlineApp.selectLayout(layout);
	}
}
