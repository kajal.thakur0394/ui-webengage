package com.webengage.ui_automation.stepDefinitions;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.parser.ParseException;

import com.webengage.ui_automation.pages.AnalyticsPage;
import com.webengage.ui_automation.pages.CommonPage;
import com.webengage.ui_automation.pages.UserPage;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class UserStepDef {

	UserPage userPage = new UserPage();
	CommonPage commonPage = new CommonPage();
	AnalyticsPage analyticsPage = new AnalyticsPage();

	@Given("I open user profile of a user having userId as {string}")
	public void i_open_user_profile_of_a_user_having_user_id_as(String userId) {
		userPage.selectUser(userId);
	}

	@Given("verify the following labels from {string} section")
	public void verify_the_following_labels_from_section(String section, DataTable dataTable) {
		List<List<String>> labels = dataTable.asLists(String.class);
		userPage.openSection(section);
		if (!section.toLowerCase().contains("channels"))
			userPage.verifyDetails(labels);
		else
			userPage.verifyRechability(labels);
	}

	@Given("verify the following labels from {string} section for type {string} and device as {string}")
	public void verify_the_following_labels_from_section_for_type_and(String section, String deviceType,
			String deviceNo, DataTable dataTable) {
		List<List<String>> labels = dataTable.asLists(String.class);
		userPage.openSection(section);
		userPage.openSection(deviceType);
		userPage.openDevice(deviceNo);
		userPage.verifyDetails(labels);
	}

	@Then("I navigate to events section and select period as {string}")
	public void i_navigate_to_events_section_and_select_period_as(String period) {
		userPage.openSection("Events");
		userPage.checkEvents(period);
	}

	@Given("check the count of following parameters in indiv cards")
	public void check_the_count(DataTable dataTable) {
		List<List<String>> cellValues = dataTable.asLists(String.class);
		userPage.verifyTotalUsers(cellValues);
	}

	@Given("open {string} view for the section with header name as {string}")
	public void open_table_view_for_the_section_with_header_name_as(String viewType, String sectionName) {
		commonPage.switchTheViewAs(viewType, sectionName);
	}

	@Then("I download the report for list of users")
	public void i_download_the_report_for_list_of_users() {
		userPage.downloadListOfUsers();
	}
	
    @Then("I verify user {string} and event {string}")
    public void i_verify_user_and_event(String userName, String eventName, io.cucumber.datatable.DataTable dataTable)
            throws ParseException, IOException {
        List<Map<String, String>> eventMap = dataTable.asMaps(String.class, String.class);
        HashMap<String, String> events = new HashMap<>(eventMap.get(0));
        userPage.verifyUserEventStreamData(userName, eventName, events);
    }
}
