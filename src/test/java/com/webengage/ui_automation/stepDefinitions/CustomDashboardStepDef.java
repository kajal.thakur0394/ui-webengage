package com.webengage.ui_automation.stepDefinitions;

import java.util.Map;

import com.webengage.ui_automation.elements.CommonPageElements;
import com.webengage.ui_automation.pages.CommonPage;
import com.webengage.ui_automation.pages.CustomDashboardPage;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class CustomDashboardStepDef {
	CustomDashboardPage custDashPage = new CustomDashboardPage();
	CommonPage comPage = new CommonPage();

    @Then("create new Dashboard with name as {string}")
    public void create_new_dashboard_with_name_as(String dashboardName) {
        comPage.createIcon();
        custDashPage.enterDashboardName(dashboardName);
    }

	@Then("I edit dashboard {string} name as {string} and select dashboard visibility as {string}")
	public void i_edit_dashbaord_name_as(String dashboardName, String editedDashboardName, String visibilityType) {
		custDashPage.editDashboardName(dashboardName, editedDashboardName);
		comPage.selectFromDropDown("Visibility", visibilityType);
		comPage.click(comPage.dynamicXpathLocator(CommonPageElements.BUTTON, "ok"));
		comPage.clickButton("update dashboard");
	}

	@Then("I verify {string} dashboard name which has visibility set as {string}")
	public void i_verify_name_whcich_has_visibility_set_as(String editedDashboardName, String visibilityType) {
		custDashPage.verifyEditedNameAndVisibility(editedDashboardName, visibilityType);
	}

	@Given("select dashboard visibility as {string}")
	public void select_dashboard_visibility_as(String visibilityType) {
		comPage.selectFromDropDown("Visibility", visibilityType);
	}

	@Then("I add the following Users for different AccessMode")
	public void i_add_the_following_users_for_differect_access_mode(io.cucumber.datatable.DataTable dataTable)
			throws InterruptedException {
		Map<String, String> userChoices = dataTable.asMap(String.class, String.class);
		custDashPage.getUsers(userChoices);
	}

	@Then("I open {string} dashboard")
	public void i_click_on_dashboard(String dashBoardName) {
		custDashPage.searchAndOpenDashBoard(dashBoardName);
	}

	@Then("I verify the user {string} for following AccessMode")
	public void i_verify_the_user_for_following_access_mode(String user, io.cucumber.datatable.DataTable dataTable) {
		Map<String, String> expectedAccess = dataTable.asMap(String.class, String.class);
		custDashPage.verifyAccessMode(user, expectedAccess);
	}

	@Then("I {string} the dashboard {string} via overview page")
	public void i_delete_the_dasboard_via_dashboard_page(String action, String dashboardName) throws InterruptedException {
		custDashPage.actionOverviewPage(action, dashboardName);
	}

	@Then("select dashboard visibility as {string} and create a dashboard")
	public void select_dashboard_visibility_as_and_create_a_dashboard(String visibilityType) {
		comPage.selectFromDropDown("Visibility", visibilityType);
		comPage.clickButton("create Dashboard");
		comPage.waitForElementToBeInvisible(
				comPage.dynamicXpathLocator(CommonPageElements.GENERIC_CLASS_CONTAINS, "modal--size-medium"));
	}

	@Then("I redirect to {string} page form Dashboard {string}")
	public void i_redirect_to_page_form_dashboard(String page, String dashBoardName) {
		custDashPage.redirectToUserPage(page, dashBoardName);
	}

	@Then("Verify {string} user card stats of dashboard {string}")
	public void verify_user_card_stats_of_dashboard(String pinCard, String dashboardName) {
		custDashPage.searchAndOpenDashBoard(dashboardName);
		custDashPage.verifyUserCardStats(pinCard);
	}

	@Then("I {string} the card {string}")
	public void i_the_card(String option, String cardName) {
		custDashPage.cardOption(option, cardName);
	}

	@Then("I pin {string} and enter card name {string} and select dashboard {string}")
	public void i_pin_and_enter_card_name_and_select_dashboard(String pinCard, String cardName, String dashboardName) {
		custDashPage.pinCard(pinCard, cardName, dashboardName);
	}

}