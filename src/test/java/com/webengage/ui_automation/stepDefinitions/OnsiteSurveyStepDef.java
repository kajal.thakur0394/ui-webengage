package com.webengage.ui_automation.stepDefinitions;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import com.webengage.ui_automation.pages.CampaignsPage;
import com.webengage.ui_automation.pages.OnsiteNotificationPage;
import com.webengage.ui_automation.pages.OnsiteSurveyPage;
import com.webengage.ui_automation.utils.APIUtility;
import com.webengage.ui_automation.utils.ReflectionsUtility;

import io.cucumber.java.en.Then;

public class OnsiteSurveyStepDef {
	OnsiteNotificationPage onsiteNotificationPage = new OnsiteNotificationPage();
	ReflectionsUtility reflectionUtility = new ReflectionsUtility();
	OnsiteSurveyPage onsiteSurveyPage = new OnsiteSurveyPage();
	OnsiteNotificationStepDef onsiteNotificationStepDef = new OnsiteNotificationStepDef();
	APIUtility apiUtility = new APIUtility();

	@Then("create a {string} Survey and select {string} template")
	public void create_a_survey_using_a_template(String surveyType, String templateType) {
		onsiteNotificationPage.createNotification(surveyType);
		onsiteNotificationPage.templateName(templateType, surveyType);
	}

	@Then("verify survey on a web page using template")
	public void verify_survey_on_a_web_page_using_template() throws ParseException {
		JSONObject template = apiUtility.getJsonObject(CampaignsPage.getJsonTemplate());
		onsiteSurveyPage.verifyOnsiteSurveyData(template);
	}

	@Then("delete survey using template")
	public void delete_the_survey_using_template() throws ParseException {
		JSONObject template = apiUtility.getJsonObject(CampaignsPage.getJsonTemplate());
		onsiteNotificationPage.deleteOnsiteNotification(template);
	}

}
