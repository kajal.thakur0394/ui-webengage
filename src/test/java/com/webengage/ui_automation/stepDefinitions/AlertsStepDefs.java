package com.webengage.ui_automation.stepDefinitions;

import java.util.Map;

import org.json.simple.JSONObject;

import com.webengage.ui_automation.hooks.Hooks;
import com.webengage.ui_automation.pages.AlertsPage;
import com.webengage.ui_automation.pages.CommonPage;
import com.webengage.ui_automation.utils.NotificationToastMessages;
import com.webengage.ui_automation.utils.ReflectionsUtility;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;

public class AlertsStepDefs {
	AlertsPage alertsPage = new AlertsPage();
	ReflectionsUtility refl = new ReflectionsUtility();
	CommonPage commonPage = new CommonPage();

	@Then("I {string} custom alert with name {string} and verify the same")
	public void custom_alert_CRUD(String crud, String alertName) {
		alertsPage.customAlertCRUDS(crud, alertName);
	}

	@Then("I verify Details in Show Details block using template {string}")
	public void verifyShowDetailblock(String templateName) {
		ReflectionsUtility refl = new ReflectionsUtility();
		JSONObject template = refl.fetchTemplate(Hooks.scenarioName, "PersonalizationTemplates", templateName);
		alertsPage.verifyAlertShowDetailsBlock(template);
	}

	@Then("I verify the alert {string} gets listed in alerts list")
	public void iVerifyAlertExist(String alertName) {
		commonPage.verifyToastMessage(NotificationToastMessages.ALERT_CREATED.message());
		alertsPage.selectAlert(alertName);
	}

	@Then("I apply filters and verify the list for the filters")
	public void iVerifyAlertListFilters(DataTable dataTable) {
		Map<String, String> filterOptions = dataTable.asMap();
		alertsPage.verifyAlertList(filterOptions);

	}
}
