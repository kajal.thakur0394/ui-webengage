package com.webengage.ui_automation.stepDefinitions;

import java.util.List;


import com.webengage.ui_automation.pages.CampaignsPage;
import com.webengage.ui_automation.pages.EmailCampaignsPage;
import com.webengage.ui_automation.pages.SMSCampaignsPage;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class EmailCampaignsStepDef {
	SMSCampaignsPage smsCampPage = new SMSCampaignsPage();
	EmailCampaignsPage emailCampPage = new EmailCampaignsPage();
	CampaignsPage campPage = new CampaignsPage();

	@Then("I select the ESP with name {string} and I enter the following details in the message ESP")
	public void i_select_the_esp_with_name_and_i_enter_the_following_details_in_the_message_esp(String selectESP,
			List<List<String>> choices) throws InterruptedException {
		campPage.selectServiceProvider("ESP",selectESP);
		emailCampPage.fillEmailDetails(choices);

	}
	
	@Then("I verify template is attached for status as {string} having {string} as campaign name")
	public void i_verify_template_is_attached_for_status_as_having_as_campaign_name(String status, String campaignName) {
		emailCampPage.verifyAttachedTemplate(status,campaignName);
	}


	@Given("verify status as {string} and attached Start Date from Campaign {string} attached to Journey {string}")
	public void verify_status_as_and_attached_start_date_fro_email_campaign(String status, String campaignName, String journeyName) {
		smsCampPage.searchBar(campaignName);
		emailCampPage.verifyAttachedJourneyCampaign(status,journeyName);
	}
	
	@Then("I select the  Service provider {string} with name {string}")
	public void i_select_the_service_provider_with_name(String selectSP, String valueToChoose) {
	    campPage.selectServiceProvider(selectSP,valueToChoose);
	}
	
}
