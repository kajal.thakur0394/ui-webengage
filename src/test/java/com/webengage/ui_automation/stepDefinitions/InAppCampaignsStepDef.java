package com.webengage.ui_automation.stepDefinitions;

import com.webengage.ui_automation.pages.InAppCampaignsPage;
import com.webengage.ui_automation.pages.SegmentsPage;

import io.cucumber.java.en.Then;

public class InAppCampaignsStepDef {
    InAppCampaignsPage inAppCampPage = new InAppCampaignsPage();
    SegmentsPage segPage= new SegmentsPage();

    @Then("I click {string} and verify PopUp Warning message")
    public void i_click_and_verify_pop_up_warning_message(String template) {
        inAppCampPage.verifyChangeTemplateWarning(template);
    }

    @Then("I verify character limit error")
    public void i_verify_character_limit_error() {
        inAppCampPage.verifyCharacterLimitError();
    }

    @Then("I switch the Layout to {string}")
    public void i_switch_the_layout_to(String layout) {
        inAppCampPage.switchLayout(layout);
    }
    
    @Then("I verify Image hieght has numeric value")
    public void i_verify_image_hieght_has_numeric_value() {
        inAppCampPage.verifyImageHeightNumeric();
    }
   
}
