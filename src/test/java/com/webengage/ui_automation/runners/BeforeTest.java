package com.webengage.ui_automation.runners;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import com.webengage.ui_automation.pages.CampaignsPage;
import com.webengage.ui_automation.pages.CommonPage;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.webengage.ui_automation.elements.CommonPageElements;
import com.webengage.ui_automation.elements.LoginPageElements;
import com.webengage.ui_automation.initialize.RemoteConnection;
import com.webengage.ui_automation.pages.IntegrationsPage;
import com.webengage.ui_automation.pages.JourneysPage;
import com.webengage.ui_automation.pages.ShopifyPage;
import com.webengage.ui_automation.pages.TestDataCreation;
import com.webengage.ui_automation.utils.APIUtility;
import com.webengage.ui_automation.utils.Log;
import com.webengage.ui_automation.utils.RestAssuredBuilder;
import com.webengage.ui_automation.utils.RuntimeUtility;
import com.webengage.ui_automation.utils.SeleniumExtendedUtility;
import com.webengage.ui_automation.utils.SetupUtility;

public class BeforeTest extends SeleniumExtendedUtility {
	public static String landingPageURL;

	public static String username = System.getProperty("set.CredentialsUN");
	public static String password = System.getProperty("set.CredentialsPW");
	public static String environment = System.getProperty("set.Environment");
	public static String namespace = System.getProperty("set.Namespace");
	SetupUtility setupUtility;
	ShopifyPage shopify;
	TestDataCreation testDataCreation;
	CommonPage commonPage;

	public BeforeTest() throws IOException {
		setupUtility = new SetupUtility();
		setupUtility.loadConfigProp();
		testDataCreation = new TestDataCreation();
		commonPage = new CommonPage();
		shopify = new ShopifyPage();
		try {
			writeBuildProperties();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void writeBuildProperties() throws IOException {
		Properties buildProps = new Properties();
		buildProps.put("browserName", ((RemoteWebDriver) driver).getCapabilities().getBrowserName().toUpperCase());
		buildProps.put("browserVersion", ((RemoteWebDriver) driver).getCapabilities().getBrowserVersion());
		buildProps.put("osName", System.getProperty("os.name"));
		buildProps.put("osVersion", System.getProperty("os.version"));
		buildProps.put("osArch", System.getProperty("os.arch"));
		buildProps.put("accountLicenseCode", "-");
		buildProps.put("accountName", "-");
		FileOutputStream outputStream = new FileOutputStream(System.getProperty("user.dir") + "/build.properties");
		buildProps.store(outputStream, "Build Properties for current build");
	}

	private void writeAccountDetailsinBuildProperies() throws IOException {
		FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + "/build.properties");
		Properties buildProps = new Properties();
		buildProps.load(fis);
		buildProps.put("accountLicenseCode", System.getProperty("accountLicenseCode"));
		buildProps.put("accountName", System.getProperty("accountName"));
		FileOutputStream outputStream = new FileOutputStream(System.getProperty("user.dir") + "/build.properties");
		buildProps.store(outputStream, "Build Properties for current build");
	}

	public void login() throws InterruptedException, IOException {
		String Staging_URL = SetupUtility.configProp.getProperty("Staging_URL");
		if (environment.toLowerCase().contains("staging")) {
			openURL(SetupUtility.configProp.getProperty("Staging_Login_URL"));
			TimeUnit.SECONDS.sleep(2);
			waitForElementToBeVisible(LoginPageElements.SIGN_IN.locate());
		} else if (environment.toLowerCase().contains("qa")) {
			driver.get(SetupUtility.configProp.getProperty("qa_Login_URL"));
			waitForElementToBeVisible(LoginPageElements.SIGN_IN.locate());
		} else if (environment.toLowerCase().contains("ksa")) {
			driver.get(SetupUtility.configProp.getProperty("ksa_Login_URL"));
			waitForElementToBeVisible(LoginPageElements.SIGN_IN.locate());
		} else {
			openURL(SetupUtility.configProp.getProperty("URL"));
			TimeUnit.SECONDS.sleep(2);
			waitForElementToBeVisible(LoginPageElements.SIGN_IN.locate());
			if (environment.contains("IN"))
				click(LoginPageElements.REGION_INDIA.locate());
			else
				click(LoginPageElements.REGION_US.locate());
		}
		click(LoginPageElements.LOGIN_USERNAME.locate());
		sendKeys(LoginPageElements.LOGIN_USERNAME.locate(), username);
		sendKeys(LoginPageElements.LOGIN_PASSWORD.locate(), password);
		click(LoginPageElements.SIGN_IN.locate());
		Staging_URL = System.getProperty("set.LicenseCode").equals("null") ? Staging_URL
				: Staging_URL.replace("stg~~2024c06d", System.getProperty("set.LicenseCode"));
		switch (environment.toLowerCase()) {
		case "prod beta":
			openURL(getCurrentURL().replace("dashboard", "beta-dashboard"));
			break;
		case "prod unl":
			openURL(getCurrentURL().replace("dashboard", "dashboard.unl"));
			break;
		case "staging":
			namespace = getStagingNamespace(namespace);
			openURL(Staging_URL.replace("namespace", namespace));
			break;
		case "staging dev":
			openURL(Staging_URL.replace("-namespace", ""));
			break;
		default:
			break;
		}
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Dashboards"));
	}

	public void chooseProj() throws InterruptedException, IOException, ParseException {
		String licCode = System.getProperty("set.LicenseCode");
		String accountName = null;
		IntegrationsPage intPage = new IntegrationsPage();
		if (!licCode.equals("null")) {
			waitForElementToBeVisible(LoginPageElements.CHOOSE_PROJECT.locate());
			openURL(getCurrentURL().replace(getCurrentURL().split("/")[4], licCode));
			System.setProperty("accountName", fetchText(LoginPageElements.CHOOSE_PROJECT.locate()));
			intPage.openIntegrationPage();
			intPage.openRestApiPage();
			testDataCreation.fetchAPIKeys();
		} else {
			accountName = setAccountName(accountName);
			TimeUnit.SECONDS.sleep(2);
			try {
				click(LoginPageElements.CHOOSE_PROJECT.locate());
				click(dynamicXpathLocator(CommonPageElements.ANCHOR_TAG_GENERIC_TEXT, accountName));
			} catch (Exception e) {
				e.printStackTrace();
				click(LoginPageElements.CHOOSE_PROJECT.locate());
				click(dynamicXpathLocator(CommonPageElements.ANCHOR_TAG_GENERIC_TEXT, accountName));
			}
			try {
				setupUtility.loadCredentials(accountName);
			} catch (Exception e) {
				Log.warn("Credentials for account you have chosen are not known.");
				intPage.openIntegrationPage();
				intPage.openRestApiPage();
				testDataCreation.fetchAPIKeys();
			}
			System.setProperty("accountName", accountName);
		}
		System.setProperty("accountLicenseCode", getCurrentURL().split("/")[4]);
		writeAccountDetailsinBuildProperies();
	}

	private String setAccountName(String accountName) {
		if (System.getProperty("set.Suite").equals("Shopify")) {
			switch (environment.toLowerCase()) {
			case "prod us":
				accountName = "ShopifyTestAutomation";
				break;
			default:
				break;
			}
		} else {
			switch (environment.toLowerCase()) {
			case "staging dev":
			case "staging":
				accountName = "UIAutomationStaging";
				break;
			case "prod beta":
			case "prod us":
			case "prod unl":
				accountName = "UIAutomation";
				break;
			case "prod ksa":
				accountName = "UIAutomationKSA";
				break;
			case "prod in":
				accountName = "UIAutomationIndia";
				break;
			case "qa":
				accountName = "UIAutomationQA";
				break;
			default:
				break;
			}
		}
		return accountName;
	}

	public void beforeTest() throws InterruptedException, IOException, ParseException {
		initializeDataVariables();
		login();
		chooseProj();
		flushDirectoryContents();
		landingPageURL = getCurrentURL();
		Log.info("LPU: " + landingPageURL);
		String suite = System.getProperty("set.Suite");
		String className = (System.getProperty("className") == null) ? "" : System.getProperty("className");
		try {
			if (suite.equalsIgnoreCase("WebSDKSuite") || suite.equalsIgnoreCase("Test Data Creation")) {
				webSdkSetup();
				commonPage.openDynamicWebPageAndLogin(true);
				Log.error("Configured SDK");
			} else if (suite.equalsIgnoreCase("End-to-End Distributed") && className.contains("Journeys")) {
				webSdkSetup();
				Log.error("Configured SDK");
			}
		} catch (Exception e) {
			Log.warn("WebSDK configuration skipped");
			e.printStackTrace();
		}
		if (suite.equals("Shopify")) {
			setupUtility.loadStoreUrl(System.getProperty("accountName"));
			driver.switchTo().newWindow(WindowType.TAB);
			openURL(SetupUtility.getShopifyStoreUrl());
			sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_CLASS, "form-input "), "TestAutomation");
			click(dynamicXpathLocator(CommonPageElements.BUTTON, "Enter"));
		}
	}

	private void webSdkSetup() throws InterruptedException {
		editHTML();
		setDynamicSDKUrl();
		setSDKUser();
	}

	private void editHTML() {
		String licenseCode = SetupUtility.licenseCode;
		Log.info("License Code :" + licenseCode);
		Log.info("SDK User is : " + CommonPage.SDKUser);
		Log.info("API Key" + SetupUtility.getAuthApiKey());
		String directoryLocation = setFilePath();
		String pages[] = new String[] { "index.html", "login.html", "home.html" };
		for (String page : pages) {
			try {
				File file = new File(directoryLocation + page);
				BufferedReader br = new BufferedReader(new FileReader(file));
				String line;
				StringBuilder sb = new StringBuilder("");
				while ((line = br.readLine()) != null) {
					if (line.contains("id='licenseCode'>Configured LC")) {
						line = "<p id='licenseCode'>Configured LC : " + licenseCode + "</p>";
					}
					sb.append(line).append("\n");
				}
				FileWriter fw = new FileWriter(file);
				fw.write(sb.toString());
				fw.close();
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private String setFilePath() {
		String path = null;
		switch (System.getProperty("set.Environment")) {
		case "Prod US":
			path = "C:\\Apache\\Apache24\\htdocs\\us\\";
			break;
		case "Prod KSA":
			path = "C:\\Apache\\Apache24\\htdocs\\ksa\\";
			break;
		case "Prod IN":
			path = "C:\\Apache\\Apache24\\htdocs\\in\\";
			break;
		case "Staging":
			path = "C:\\Apache\\Apache24\\htdocs\\stg\\";
			break;
		}
		return path;
	}

	private void initializeDataVariables() {
		APIUtility.initializeVariables();
		RuntimeUtility.initializeVariables();
		RestAssuredBuilder.initializeVariables();
		CampaignsPage.initializeVariables();
		JourneysPage.initializeVariables();
	}

	private void flushDirectoryContents() {
		Log.info("Deleting stuff from Expected, Runtime Payloads, and CampaignStats");
		File fileLocations[] = { new File(SetupUtility.runtimePayloadsDirectory),
				new File(SetupUtility.expectedPayloadsDirectory),
				new File(SetupUtility.runtimeCampaginStatsDirectory) };
		try {
			for (File file : fileLocations) {
				for (File dirFiles : file.listFiles()) {
					if (!dirFiles.getName().contains("gitkeep"))
						dirFiles.delete();
				}
			}
			RemoteConnection.readFilefromRemote("TestData", "testData.xlsx");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void setDynamicSDKUrl() {
		CommonPage.SDKDynamicWebpage = SetupUtility.configProp.getProperty("SDKDynamicWebpage");
		switch (environment) {
		case "Prod US":
			CommonPage.SDKDynamicWebpage = CommonPage.SDKDynamicWebpage.replace("{environment}", "us");
			break;
		case "Prod KSA":
			CommonPage.SDKDynamicWebpage = CommonPage.SDKDynamicWebpage.replace("{environment}", "ksa");
			break;
		case "Prod IN":
			CommonPage.SDKDynamicWebpage = CommonPage.SDKDynamicWebpage.replace("{environment}", "in");
			break;
		case "Staging":
			CommonPage.SDKDynamicWebpage = CommonPage.SDKDynamicWebpage.replace("{environment}", "stg");
			break;
		}
	}

	private void setSDKUser() {
		CommonPage.SDKUser = SetupUtility.configProp.getProperty("SDK_User");
	}

	private String getStagingNamespace(String namespace) {
		if (namespace.contains(",")) {
			CampaignsPage.setPrimaryNamespace(namespace.split(",")[0]);
			CampaignsPage.setSecondaryNamespace(namespace.split(",")[1]);
		}
		return namespace.split(",")[0];
	}
}