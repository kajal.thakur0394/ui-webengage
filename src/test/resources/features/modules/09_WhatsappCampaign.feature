@Whatsapp @Regression
Feature: Whatsapp Campaign Feature

  Scenario Outline: To verify previously created WhatsApp campaigns are available in WhatsApp campaign > List of campaigns page
    Given I navigate to "WhatsApp" page via "Channels"
    Then I add "<CampaignName>" to searchbox
    And I verify that "<CampaignName>" and "<CampaignType>" exist

    Examples: 
      | CampaignName                               | CampaignType  |
      | Test One-time WhatsApp Campaign exist      | One-time      |
      | Test Triggered WhatsApp Campaign exist     | Triggered     |
      | Test Recurring WhatsApp Campaign exist     | Recurring     |
      | Test Transactional WhatsApp Campaign exist | Transactional |

  Scenario: To verify Live Segment via Segment Editor is getting created when One-time Whatsapp Campaign type is selected
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "One-time" Campaign Type and enters "LiveSegment via Segment Editor-One-time Whatsapp Campaign" Campaign Name
    And I select New Segment Creation icon
    And I add "Segment Name" as "Whatsapp: User Condition > User IDs"
    And Pass following details in user card
      | Visitor Type | All Users   |
      | User IDs     | ends with>J |
    And I Save and verify the created "Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then I select WA Template having name "Test Template" with WSP name "Journey WSP" and fill the Message Content
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    And verify via "fetchWACampaign" API whether following "Segment" have been attached to campaign just created
      | Whatsapp: User Condition > User IDs |
    Then I navigate to "Live Segments" page via "Segments"
    And I verify "Whatsapp: User Condition > User IDs" segment is present
    Given I navigate to "WhatsApp" page via "Channels"
    Then I delete the Campaign with Name "LiveSegment via Segment Editor-One-time Whatsapp Campaign"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "Whatsapp: User Condition > User IDs"

  Scenario: To verify Live Segment is getting created when Triggered Whatsapp Campaign type is selected
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "Triggered" Campaign Type and enters "LiveSegment via Segment Editor-Triggered WhatsApp Campaign" Campaign Name
    And I select New Segment Creation icon
    And I add "Segment Name" as "Whatsapp: Behavorial Condition > more than 1 condition of Users who did these Events"
    And Pass following details in Behavioural card
      |     | Users who DID these events : | System>User Login    |
      | And | Users who DID these events : | System>App Installed |
    And I Save and verify the created "Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | UPON OCCURRENCE OF | TriggerWhatsAppEvent |    |       |
      | QUEUEING           | true                 | 12 | Hours |
    Then I select WA Template having name "Test Template" with WSP name "Journey WSP" and fill the Message Content
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    And verify via "fetchWACampaign" API whether following "Segment" have been attached to campaign just created
      | Whatsapp: Behavorial Condition > more than 1 condition of Users who did these Events |
    Then I navigate to "Live Segments" page via "Segments"
    And I verify "Whatsapp: Behavorial Condition > more than 1 condition of Users who did these Events" segment is present
    Given I navigate to "WhatsApp" page via "Channels"
    Then check overview page is loaded for "LiveSegment via Segment Editor-Triggered WhatsApp Campaign" campaign
    Given I navigate to "WhatsApp" page via "Channels"
    Then I delete the Campaign with Name "LiveSegment via Segment Editor-Triggered WhatsApp Campaign"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "Whatsapp: Behavorial Condition > more than 1 condition of Users who did these Events"

  @Smoke
  Scenario: To verify Live Segment via Segment Editor is getting created when Recurring WhatsApp Campaign type is selected
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "Recurring" Campaign Type and enters "LiveSegment via Segment Editor-Recurring  Campaign" Campaign Name
    And I select New Segment Creation icon
    And I add "Segment Name" as "Whatsapp: Technology Condition > Android"
    And Pass following details for "Android" in Technology card
      | Total Time Spent (in seconds) | greater than>100 |
    And I Save and verify the created "Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then I select WA Template having name "Test Template" with WSP name "Journey WSP" and fill the Message Content
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    And verify via "fetchWACampaign" API whether following "Segment" have been attached to campaign just created
      | Whatsapp: Technology Condition > Android |
    And I navigate to "Live Segments" page via "Segments"
    And I verify "Whatsapp: Technology Condition > Android" segment is present
    Given I navigate to "WhatsApp" page via "Channels"
    Then check overview page is loaded for "LiveSegment via Segment Editor-Recurring  Campaign" campaign
    Given I navigate to "WhatsApp" page via "Channels"
    Then I delete the Campaign with Name "LiveSegment via Segment Editor-Recurring  Campaign"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "Whatsapp: Technology Condition > Android"

  @issue:WP-8556
  Scenario: To Verify Static List segment via CSV when 'One-time' Whatsapp Campaign type is selected
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "One-time" Campaign Type and enters "Static Segment via CSV One-time WhatsApp Campaign" Campaign Name
    Then select Segment having name as "StaticListCSV"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then I select WA Template having name "Test Template" with WSP name "Journey WSP" and fill the Message Content
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Given I navigate to "WhatsApp" page via "Channels"
    Then I delete the Campaign with Name "Static Segment via CSV One-time WhatsApp Campaign"

  @Smoke
  Scenario: To verify WhatsApp campaign creation using Inclusion/Exclusion of segment feature
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "One-time" Campaign Type and enters "LiveSegment with Inclusion/Exclusion of segment WhatsApp Campaign" Campaign Name
    Then I fill the details in Audience tab as below
      | Audience Type | multiple segments |
    Then I select "Users in ANY of these segments" as "Send To" Inclusion Segment
    And I add "Segment Name" as "User Condition > User ID ends with Z"
    And Pass following details in user card
      | Visitor Type | All Users   |
      | User IDs     | ends with>Z |
    And Save the Segment
    Then I select "Users in ANY of these segments" as "EXCLUDE" Exclusion Segment
    And I add "Segment Name" as "User Condition > Reachability on Whatsapp"
    And Pass following details in user card
      | Visitor Type | All Users             |
      | Reachability | Reachable on-WhatsApp |
    And Save the Segment
    And I save the Audience details
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then I select WA Template having name "Test Template" with WSP name "Journey WSP" and fill the Message Content
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    And verify via "fetchWACampaign" API whether following "Segment" have been attached to campaign just created
      | User Condition > User ID ends with Z      |
      | User Condition > Reachability on Whatsapp |
    Then I navigate to "Live Segments" page via "Segments"
    And I verify following Segments are present
      | User Condition > User ID ends with Z      |
      | User Condition > Reachability on Whatsapp |
    Given I navigate to "WhatsApp" page via "Channels"
    Then check overview page is loaded for "LiveSegment with Inclusion/Exclusion of segment WhatsApp Campaign" campaign
    Given I navigate to "WhatsApp" page via "Channels"
    Then I delete the Campaign with Name "LiveSegment with Inclusion/Exclusion of segment WhatsApp Campaign"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete the following mentioned "segments"
      | User Condition > User ID ends with Z      |
      | User Condition > Reachability on Whatsapp |

  @Smoke
  Scenario: To verify Send Now Campaign CRUD - WhatsApp
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "One-time" Campaign Type and enters "Verify WhatsApp campaign CRUD" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then I select WA Template having name "Test Template" with WSP name "Journey WSP" and fill the Message Content
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Verify WhatsApp campaign CRUD" campaign
    Given I navigate to "WhatsApp" page via "Channels"
    Then I delete campaign with name "Verify WhatsApp campaign CRUD"

  @Smoke
  Scenario: To verify Triggered WhatsApp campaign when Start Date: Now and End Date: Till is selected
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "Triggered" Campaign Type and enters "Triggered WhatsApp Campaign with Start Date:Now and End Date:Till" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | UPON OCCURRENCE OF | TriggerWhatsAppEvent |  |  |
      | START DATE         | now                  |  |  |
      | END DATE           | till                 |  |  |
    Then I select WA Template having name "Test Template" with WSP name "Journey WSP" and fill the Message Content
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Triggered WhatsApp Campaign with Start Date:Now and End Date:Till" campaign
    Given I navigate to "WhatsApp" page via "Channels"
    Then I add "Triggered WhatsApp Campaign with Start Date:Now and End Date:Till" to searchbox
    And I verify following details for "Triggered WhatsApp Campaign with Start Date:Now and End Date:Till" campaign
      | Start date: |
      | End date:   |
    Given I navigate to "WhatsApp" page via "Channels"
    Then I delete the Campaign with Name "Triggered WhatsApp Campaign with Start Date:Now and End Date:Till"

  Scenario: To verify Triggered WhatsApp campaign when Start Date: Later and End Date: Till is selected
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "Triggered" Campaign Type and enters "Triggered WhatsApp Campaign with Start Date:Later and End Date:Till" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | UPON OCCURRENCE OF | TriggerWhatsAppEvent |  |  |
      | START DATE         | later                |  |  |
      | END DATE           | till                 |  |  |
    Then I select WA Template having name "Test Template" with WSP name "Journey WSP" and fill the Message Content
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Triggered WhatsApp Campaign with Start Date:Later and End Date:Till" campaign
    Given I navigate to "WhatsApp" page via "Channels"
    Then I add "Triggered WhatsApp Campaign with Start Date:Later and End Date:Till" to searchbox
    And I verify following details for "Triggered WhatsApp Campaign with Start Date:Later and End Date:Till" campaign
      | Start date: |
      | End date:   |
    Given I navigate to "WhatsApp" page via "Channels"
    Then I delete the Campaign with Name "Triggered WhatsApp Campaign with Start Date:Later and End Date:Till"

  Scenario: To verify Triggered WhatsApp campaign when Start Date: Later and End Date: Never is selected
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "Triggered" Campaign Type and enters "Triggered WhatsApp Campaign with Start Date:Later and End Date:Never" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | UPON OCCURRENCE OF | TriggerWhatsAppEvent |  |  |
      | START DATE         | later                |  |  |
      | END DATE           | never                |  |  |
    Then I select WA Template having name "Test Template" with WSP name "Journey WSP" and fill the Message Content
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Triggered WhatsApp Campaign with Start Date:Later and End Date:Never" campaign
    Given I navigate to "WhatsApp" page via "Channels"
    Then I add "Triggered WhatsApp Campaign with Start Date:Later and End Date:Never" to searchbox
    And I verify following details for "Triggered WhatsApp Campaign with Start Date:Later and End Date:Never" campaign
      | Start date: |
      | End date:   |
    Given I navigate to "WhatsApp" page via "Channels"
    Then I delete the Campaign with Name "Triggered WhatsApp Campaign with Start Date:Later and End Date:Never"

  Scenario: To verify Triggered WhatsApp campaign when Start Date: Now and End Date: Never is selected
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "Triggered" Campaign Type and enters "Triggered WhatsApp Campaign with Start Date:Now and End Date:Never" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | UPON OCCURRENCE OF | TriggerWhatsAppEvent |  |  |
      | START DATE         | now                  |  |  |
      | END DATE           | never                |  |  |
    Then I select WA Template having name "Test Template" with WSP name "Journey WSP" and fill the Message Content
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Triggered WhatsApp Campaign with Start Date:Now and End Date:Never" campaign
    Given I navigate to "WhatsApp" page via "Channels"
    Then I add "Triggered WhatsApp Campaign with Start Date:Now and End Date:Never" to searchbox
    And I verify following details for "Triggered WhatsApp Campaign with Start Date:Now and End Date:Never" campaign
      | Start date: |
      | End date:   |
    Given I navigate to "WhatsApp" page via "Channels"
    Then I delete the Campaign with Name "Triggered WhatsApp Campaign with Start Date:Now and End Date:Never"

  @Smoke
  Scenario: To verify Recurring WhatsApp campaign when Delivery Schedule: 1/Day and End Date: Never is selected
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "Recurring" Campaign Type and enters "Recurring WhatsApp Campaign with Delivery Schedule:1/Day and End Date:Never" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | DELIVERY SCHEDULE | Day   |  |  |
      | END DATE          | never |  |  |
    Then I select WA Template having name "Test Template" with WSP name "Journey WSP" and fill the Message Content
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Recurring WhatsApp Campaign with Delivery Schedule:1/Day and End Date:Never" campaign
    Given I navigate to "WhatsApp" page via "Channels"
    Then I add "Recurring WhatsApp Campaign with Delivery Schedule:1/Day and End Date:Never" to searchbox
    And I verify following details for "Recurring WhatsApp Campaign with Delivery Schedule:1/Day and End Date:Never" campaign
      | Delivery Schedule |
      | End date:         |
    Given I navigate to "WhatsApp" page via "Channels"
    Then I delete the Campaign with Name "Recurring WhatsApp Campaign with Delivery Schedule:1/Day and End Date:Never"

  Scenario: To verify Recurring WhatsApp campaign when Delivery Schedule: 1/Day and End Date: Till is selected
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "Recurring" Campaign Type and enters "Recurring WhatsApp Campaign with Delivery Schedule:1/Day and End Date:Till" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | DELIVERY SCHEDULE | Day  |  |  |
      | END DATE          | till |  |  |
    Then I select WA Template having name "Test Template" with WSP name "Journey WSP" and fill the Message Content
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Recurring WhatsApp Campaign with Delivery Schedule:1/Day and End Date:Till" campaign
    Given I navigate to "WhatsApp" page via "Channels"
    Then I add "Recurring WhatsApp Campaign with Delivery Schedule:1/Day and End Date:Till" to searchbox
    And I verify following details for "Recurring WhatsApp Campaign with Delivery Schedule:1/Day and End Date:Till" campaign
      | End date:         |
      | Delivery Schedule |
    Given I navigate to "WhatsApp" page via "Channels"
    Then I delete the Campaign with Name "Recurring WhatsApp Campaign with Delivery Schedule:1/Day and End Date:Till"

  Scenario: To verify Recurring WhatsApp campaign when Delivery Schedule: 1/Week and End Date: Never is selected
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "Recurring" Campaign Type and enters "Recurring WhatsApp Campaign with Delivery Schedule:1/Week and End Date:Never" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | DELIVERY SCHEDULE | Week  |  | M W F |
      | END DATE          | never |  |       |
    Then I select WA Template having name "Test Template" with WSP name "Journey WSP" and fill the Message Content
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Recurring WhatsApp Campaign with Delivery Schedule:1/Week and End Date:Never" campaign
    Given I navigate to "WhatsApp" page via "Channels"
    Then I add "Recurring WhatsApp Campaign with Delivery Schedule:1/Week and End Date:Never" to searchbox
    And I verify following details for "Recurring WhatsApp Campaign with Delivery Schedule:1/Week and End Date:Never" campaign
      | End date:         |
      | Delivery Schedule |
    Given I navigate to "WhatsApp" page via "Channels"
    Then I delete the Campaign with Name "Recurring WhatsApp Campaign with Delivery Schedule:1/Week and End Date:Never"

  Scenario: To verify Recurring WhatsApp campaign when Delivery Schedule: 1/Month and End Date: Till is selected
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "Recurring" Campaign Type and enters "Recurring WhatsApp Campaign with Delivery Schedule:1/Month and End Date:Till" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | DELIVERY SCHEDULE | Month | Specific Dates | 1st 2nd 4th 25th |
      | END DATE          | till  |                |                  |
    Then I select WA Template having name "Test Template" with WSP name "Journey WSP" and fill the Message Content
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Recurring WhatsApp Campaign with Delivery Schedule:1/Month and End Date:Till" campaign
    Given I navigate to "WhatsApp" page via "Channels"
    Then I add "Recurring WhatsApp Campaign with Delivery Schedule:1/Month and End Date:Till" to searchbox
    And I verify following details for "Recurring WhatsApp Campaign with Delivery Schedule:1/Month and End Date:Till" campaign
      | End date:         |
      | Delivery Schedule |
    Given I navigate to "WhatsApp" page via "Channels"
    Then I delete the Campaign with Name "Recurring WhatsApp Campaign with Delivery Schedule:1/Month and End Date:Till"

  Scenario: To verify Recurring WhatsApp campaign when Delivery Schedule: 1/Month and End Date: Never is selected
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "Recurring" Campaign Type and enters "Recurring WhatsApp Campaign with Delivery Schedule:1/Month and End Date:Never" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | DELIVERY SCHEDULE | Month | Specific Days | M W F |
      | END DATE          | never |               |       |
    Then I select WA Template having name "Test Template" with WSP name "Journey WSP" and fill the Message Content
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Recurring WhatsApp Campaign with Delivery Schedule:1/Month and End Date:Never" campaign
    Given I navigate to "WhatsApp" page via "Channels"
    Then I add "Recurring WhatsApp Campaign with Delivery Schedule:1/Month and End Date:Never" to searchbox
    And I verify following details for "Recurring WhatsApp Campaign with Delivery Schedule:1/Month and End Date:Never" campaign
      | End date:         |
      | Delivery Schedule |
    Given I navigate to "WhatsApp" page via "Channels"
    Then I delete the Campaign with Name "Recurring WhatsApp Campaign with Delivery Schedule:1/Month and End Date:Never"

  Scenario: Verify existing tag is attached to Whatsapp campaigns
    Given I navigate to "WhatsApp" page via "Channels"
    And I search for "WhatsApp Campaign: Tag" campaign
    Then I verify "campaigntag" tag is attached to the "WhatsApp Campaign: Tag" campaign

  Scenario: Verify new tag is created and attached to Whatsapp campaign
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "One-time" Campaign Type and enters "Tag: WhatsApp Campaign" Campaign Name
    And create new tag from with name as "whatsapp-tag" for campaign
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then I select WA Template having name "Test Template" with WSP name "Journey WSP" and fill the Message Content
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Tag: WhatsApp Campaign" campaign
    Given I navigate to "WhatsApp" page via "Channels"
    Then I verify "whatsapp-tag" tag is attached to the "Tag: WhatsApp Campaign" campaign
    Given I navigate to "WhatsApp" page via "Channels"
    And I search for "Tag: WhatsApp Campaign" campaign
    Then delete the tag with name as "whatsapp-tag" from campaign "Tag: WhatsApp Campaign"
    Then I delete the Campaign with Name "Tag: WhatsApp Campaign"

  @End-to-End @Distributed
  Scenario: To verify Stats are showing properly when One Time whatsapp Campaign type is selected
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "One-time" Campaign Type and enters "Sent/Delivered Status-One-time WhatsApp Campaign" Campaign Name
    Then select Segment having name as "StatsValidationUser Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | QUEUEING   | false |  |  |
      | FC         | false |  |  |
      | DND        | false |  |  |
      | THROTTLING | false |  |  |
    Then I select WA Template having name "welcome_lead" with WSP name "Interakt WSP" and fill the Message Content
      | Variable 1 | Text Test |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Sent/Delivered Status-One-time WhatsApp Campaign" campaign

  @End-to-End @Distributed
  Scenario: To verify Stats are showing properly when Triggered WhatsApp Campaign type is selected
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "Triggered" Campaign Type and enters "Sent/Delivered Status-Triggered WhatsApp Campaign" Campaign Name
    Then select Segment having name as "StatsValidationUser Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | UPON OCCURRENCE OF | TriggerWhatsAppEvent |  |  |
      | QUEUEING           | false                |  |  |
      | FC                 | false                |  |  |
      | DND                | false                |  |  |
      | THROTTLING         | false                |  |  |
      | END DATE           | after 3 hours        |  |  |
    Then I select WA Template having name "welcome_lead" with WSP name "Interakt WSP" and fill the Message Content
      | Variable 1 | Text Test |
    And I skip the Campaign Test
    And I launch the Campaign
    Then wait for "fetchWACampaign" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    Then check overview page is loaded for "Sent/Delivered Status-Triggered WhatsApp Campaign" campaign
    Given I navigate to "WhatsApp" page via "Channels"
    And hit "performEvent" API from "PerformEvents" sheet to trigger Event with reference id "triggerWhatsAppEvent"

  @End-to-End @Distributed
  Scenario: To verify Stats are showing properly when Recurring WhatsApp Campaign type is selected
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "Recurring" Campaign Type and enters "Sent/Delivered Status-Recurring WhatsApp Campaign" Campaign Name
    Then select Segment having name as "StatsValidationUser Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | DELIVERY SCHEDULE | Day           |  |  |
      | QUEUEING          | false         |  |  |
      | FC                | false         |  |  |
      | DND               | false         |  |  |
      | THROTTLING        | false         |  |  |
      | END DATE          | after 3 hours |  |  |
    Then I adjust the time for Recurring Campaign by checking system time
    Then I select WA Template having name "welcome_lead" with WSP name "Interakt WSP" and fill the Message Content
      | Variable 1 | Text Test |
    And I skip the Campaign Test
    And I launch the Campaign

  @End-to-End @Distributed
  Scenario: To verify Stats are showing properly when Transactional whatsapp Campaign type is selected
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "Transactional" Campaign Type and enters "Transactional WhatsApp Campaign" Campaign Name
    Then I fill the details in Audience tab as below
      |  |  |
    Then I select WA Template having name "welcome_lead" with WSP name "Interakt WSP" and fill the Message Content
      | Variable 1 | Text Test |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Transactional WhatsApp Campaign" campaign
    Given I navigate to "WhatsApp" page via "Channels"
    When I hit the POST method using static body for "transactionalCampaign" api from "WhatsAppCampaign" sheet with ref id "transactionalWhatsApp"

  @End-to-End @skip
  Scenario: Verify on sending Invalid template failure is shown
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "One-time" Campaign Type and enters "Failure Status-One-time WhatsApp Campaign" Campaign Name
    Then select Segment having name as "StatsValidationUser Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | QUEUEING | false |  |  |
    Then I select WA Template having name "Test Template" with WSP name "Journey WSP" and fill the Message Content
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then wait for "fetchWACampaign" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    Then check overview page is loaded for "Failure Status-One-time WhatsApp Campaign" campaign
    Given I navigate to "WhatsApp" page via "Channels"
    Then I fetch "whatsappAggregates" api and add dynamic CampaignId
    Then wait "10" minutes for "whatsappAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | failures | 1 |
    Given I navigate to "WhatsApp" page via "Channels"
    Then I delete the Campaign with Name "Failure Status-One-time WhatsApp Campaign"

  Scenario: Verify that user can add Call to Action button in WA template
    Given I navigate to "Integrations" page via "Data Platform"
    And I click on Configure "WhatsApp Setup"
    Then I make WA Template for "WA Cred (Verloop)" with data as
      | Template Name  | Call to action Test |
      | Template Text  | Just check this out |
      | Type Of Action | Call To Action      |
      | Button 1 Text  | Test                |
    And I delete the WA Template with name as "Call to action Test" and WSP as "WA Cred"

  Scenario: Verify that user can add footer in WA template
    Given I navigate to "Integrations" page via "Data Platform"
    And I click on Configure "WhatsApp Setup"
    Then I make WA Template for "WA Cred (Verloop)" with data as
      | Template Name | Footer Test         |
      | Template Text | Just check this out |
      | Footer        | Test                |
    And I delete the WA Template with name as "Footer Test" and WSP as "WA Cred"

  Scenario: Verify that user can add Quick Reply button in WA template
    Given I navigate to "Integrations" page via "Data Platform"
    And I click on Configure "WhatsApp Setup"
    Then I make WA Template for "WA Cred (Verloop)" with data as
      | Template Name  | Quick reply Test    |
      | Template Text  | Just check this out |
      | Type Of Action | Quick Reply         |
      | Button 1 Text  | Test                |
    And I delete the WA Template with name as "Quick reply Test" and WSP as "WA Cred"

  Scenario: Verify that user is able to edit the WA template
    Given I navigate to "Integrations" page via "Data Platform"
    And I click on Configure "WhatsApp Setup"
    Then I make WA Template for "WA Cred (Verloop)" with data as
      | Template Name | Edit template Test  |
      | Template Text | Just check this out |
    And I edit "Edit template Test" template with WSP as "WA Cred" with following configs
      | Template Name | Edit template Test edited |
    And I delete the WA Template with name as "Edit template Test edited" and WSP as "WA Cred"

  @End-to-End @Distributed
  Scenario Outline: To verify on message tab, for <Campaign> template selected template name, Template Text, Image URL text field and variables text field are shown
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "One-time" Campaign Type and enters "Sent/Delivered Status-One-time WhatsApp Campaign - Image Template" Campaign Name
    Then select Segment having name as "StatsValidationUser Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | QUEUEING | false |  |  |
    Then I select WA Template having name "order_shipped_final101" with WSP name "Interakt WSP" and fill the Message Content
      | Variable 1 | Image Test |
      | Media File | <Media>    |
    And I skip the Campaign Test
    And I launch the Campaign
    Then wait for "fetchWACampaign" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    Then I fetch "whatsappAggregates" api and add dynamic CampaignId

    Examples: 
      | Media                                                                                                     | Campaign     |
      | Upload Image                                                                                              | Upload Image |
      | sendImageURL>https://file-examples.com/storage/fe19e1a6e563854389e633c/2017/10/file_example_JPG_100kB.jpg | SendImageURL |

  @End-to-End @Distributed
  Scenario Outline: To verify on message tab, for <Campaign> template selected template name, Template Text, Image URL text field and variables text field are shown
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "One-time" Campaign Type and enters "Sent/Delivered Status-One-time WhatsApp Campaign - Video Template" Campaign Name
    Then select Segment having name as "StatsValidationUser Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | QUEUEING | false |  |  |
    Then I select WA Template having name "order_shipped_final102" with WSP name "Interakt WSP" and fill the Message Content
      | Variable 1 | Video Test |
      | Media File | <Media>    |
    And I skip the Campaign Test
    And I launch the Campaign
    Then wait for "fetchWACampaign" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    Then I fetch "whatsappAggregates" api and add dynamic CampaignId

    Examples: 
      | Media                                                                                            | Campaign     |
      | Upload Video                                                                                     | Upload Video |
      | sendVideoURL>https://jsoncompare.org/LearningContainer/SampleFiles/Video/MP4/sample-mp4-file.mp4 | SendVideoUrl |

  @Smoke
  Scenario: To verify overview page is loaded when campaign status is Paused for Recurring Whatsapp
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "Recurring" Campaign Type and enters "Whatsapp-Recurring-Overview Page Validation for Paused status" Campaign Name
    Then select Segment having name as "StatsValidationUser Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | DELIVERY SCHEDULE | Day |  |  |
    Then I adjust the time for Recurring Campaign by checking system time
    Then I select WA Template having name "otp__dde07788-16f3-4dc2-8eca-a61905124198" with WSP name "WA Cred" and fill the Message Content
      | Variable 1 | Aditya |
      | Variable 2 |   1234 |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I pause the campaign with "whatsapp-deactivate" api
    Then check overview page is loaded for "Whatsapp-Recurring-Overview Page Validation for Paused status" campaign
    Given I navigate to "WhatsApp" page via "Channels"
    Then I delete the Campaign with Name "Whatsapp-Recurring-Overview Page Validation for Paused status"

  Scenario: To verify overview page is loaded when campaign status is in draft state for Triggered Whatsapp
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "Triggered" Campaign Type and enters "Whatsapp-Triggered-Overview Page Validation for draft status" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | UPON OCCURRENCE OF | TriggerWhatsAppEvent |  |  |
    Then I select WA Template having name "Test Template" with WSP name "Journey WSP" and fill the Message Content
      |  |  |
    And I skip the Campaign Test
    Given I navigate to "WhatsApp" page via "Channels"
    Then check overview page is loaded for "Whatsapp-Triggered-Overview Page Validation for draft status" campaign
    Given I navigate to "WhatsApp" page via "Channels"
    Then I delete the Campaign with Name "Whatsapp-Triggered-Overview Page Validation for draft status"

  Scenario: To verify unicode characterset contents are showing properly for 'Recurring' Whatsapp Campaign for 'Document' Template
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "Recurring" Campaign Type and enters "Unicode: Recurring WhatsApp Campaign Document template ℗ ℘ ℙ ℚ ℛ ℜ ℝ ℞ ℟ ℠ ℡" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | DELIVERY SCHEDULE | Day   |  |  |
      | QUEUEING          | false |  |  |
    Then I adjust the time for Recurring Campaign by checking system time
    Then I select WA Template having name "Unicode Testing" with WSP name "Journey WSP"
    Then fill the page via "Unicode-WhatsAppDocument" template from "MultiLingualTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    And I verify "Unicode: Recurring WhatsApp Campaign Document template ℗ ℘ ℙ ℚ ℛ ℜ ℝ ℞ ℟ ℠ ℡" Campaign name with unicode characters on overview page
    Given I navigate to "WhatsApp" page via "Channels"
    Then I delete the Campaign with unicode characters "Unicode: Recurring WhatsApp Campaign Document template ℗ ℘ ℙ ℚ ℛ ℜ ℝ ℞ ℟ ℠ ℡"
