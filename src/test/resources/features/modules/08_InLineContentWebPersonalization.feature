@InLineContentWebP @Regression
Feature: In-line Content Web Personalization Feature

  Scenario Outline: To verify previously created in-line webP campaigns are available in in-line content > List of campaigns page
    Given I navigate to "In-line Content" page via "Web Personalization"
    Then I add "<CampaignName>" to searchbox
    And I verify that "<CampaignName>" and "<CampaignType>" exist

    Examples: 
      | CampaignName                                   | CampaignType |
      | Test Triggered in-line banner web-P exist      | Triggered    |
      | Test Triggered in-line custom code web-P exist | Triggered    |

  @Smoke
  Scenario: To verify segment creation when Banner layout is selected
    Given I navigate to "In-line Content" page via "Web Personalization"
    And I select "Banner" Campaign Type and enters "Banner: in-line webP campaign-Segment validation" Campaign Name
    And I select New Segment Creation icon
    And I add "Segment Name" as "in-line webP: User Condition > User IDs"
    And Pass following details in user card
      | Visitor Type | All Users   |
      | User IDs     | ends with>P |
    And I Save and verify the created "Segment"
    And save the campaign
    Then select property "button_Test (body > button)" and "Add after the property" as property placement
    Then select page to show as "All Pages"
    And save the campaign
    Then use the in-line WebP template "in-linewebPBanner" to fill message details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    And verify via "fetchInlineContentWebP" API whether following "Segment" have been attached to campaign just created
      | in-line webP: User Condition > User IDs |
    Then I navigate to "Live Segments" page via "Segments"
    And I verify "in-line webP: User Condition > User IDs" segment is present
    Given I navigate to "In-line Content" page via "Web Personalization"
    Then check overview page is loaded for "Banner: in-line webP campaign-Segment validation" campaign
    Given I navigate to "In-line Content" page via "Web Personalization"
    Then I delete the Campaign with Name "Banner: in-line webP campaign-Segment validation"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "in-line webP: User Condition > User IDs"

  @Smoke
  Scenario: To verify segment creation when Custom Code layout is selected
    Given I navigate to "In-line Content" page via "Web Personalization"
    And I select "Custom Code" Campaign Type and enters "Custom: in-line webP campaign-Segment validation" Campaign Name
    And I select New Segment Creation icon
    And I add "Segment Name" as "in-line webP: User Condition > User IDs"
    And Pass following details in user card
      | Visitor Type | All Users   |
      | User IDs     | ends with>P |
    And I Save and verify the created "Segment"
    And save the campaign
    Then select property "button_Test (body > button)" and "Add after the property" as property placement
    Then select page to show as "All Pages"
    And save the campaign
    Then use the in-line WebP template "in-linewebPCustom" to fill message details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    And verify via "fetchInlineContentWebP" API whether following "Segment" have been attached to campaign just created
      | in-line webP: User Condition > User IDs |
    Then I navigate to "Live Segments" page via "Segments"
    And I verify "in-line webP: User Condition > User IDs" segment is present
    Given I navigate to "In-line Content" page via "Web Personalization"
    Then check overview page is loaded for "Custom: in-line webP campaign-Segment validation" campaign
    Given I navigate to "In-line Content" page via "Web Personalization"
    Then I delete the Campaign with Name "Custom: in-line webP campaign-Segment validation"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "in-line webP: User Condition > User IDs"

  Scenario: Verify existing tags is attached to in-line web-P content
    Given I navigate to "In-line Content" page via "Web Personalization"
    And I search for "In-line ContentWeb: Tag" campaign
    Then I verify "campaigntag" tag is attached to the "In-line ContentWeb: Tag" campaign

  Scenario: Verify new tag is created from WebP Channel and gets attached to newly created In-Line Content
    Given I navigate to "In-line Content" page via "Web Personalization"
    And I select "Banner" Campaign Type and enters "InLineWebP-Banner: Tag" Campaign Name
    And create new tag from with name as "inline-tag" for campaign
    Then select Segment having name as "User Condition > User IDs> ends with J"
    And save the campaign
    Then select property "button_Test (body > button)" and "Add after the property" as property placement
    Then select page to show as "All Pages"
    And save the campaign
    Then use the in-line WebP template "in-linewebPBanner" to fill message details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then wait for "fetchInlineContentWebP" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    Then I refresh the page
    Then check overview page is loaded for "InLineWebP-Banner: Tag" campaign
    Given I navigate to "In-line Content" page via "Web Personalization"
    And I search for "InLineWebP-Banner: Tag" campaign
    Then I verify "inline-tag" tag is attached to the "InLineWebP-Banner: Tag" campaign
    Given I navigate to "In-line Content" page via "Web Personalization"
    And I search for "InLineWebP-Banner: Tag" campaign
    Then delete the tag with name as "inline-tag" from campaign "InLineWebP-Banner: Tag"
    Then I delete the Campaign with Name "InLineWebP-Banner: Tag"

  @Smoke
  Scenario: To verify property creation tab is enabled when Banner layout is selected
    Given I navigate to "In-line Content" page via "Web Personalization"
    And I select "Banner" Campaign Type and enters "Banner: in-line webP campaign-Property" Campaign Name
    And I select New Segment Creation icon
    And I add "Segment Name" as "in-line webP: User Condition > User IDs"
    And Pass following details in user card
      | Visitor Type | All Users   |
      | User IDs     | ends with>P |
    And I Save and verify the created "Segment"
    And save the campaign
    And verify select property is enabled and able to fill following details
      | Property Name | Test1 |
      | CSS Selector  | Test2 |
    Then select property "button_Test (body > button)" and "Add after the property" as property placement
    Then select page to show as "All Pages"
    And save the campaign
    Then use the in-line WebP template "in-linewebPBanner" to fill message details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    And verify via "fetchInlineContentWebP" API whether following "Segment" have been attached to campaign just created
      | in-line webP: User Condition > User IDs |
    Then I navigate to "Live Segments" page via "Segments"
    And I verify "in-line webP: User Condition > User IDs" segment is present
    Given I navigate to "In-line Content" page via "Web Personalization"
    Then check overview page is loaded for "Banner: in-line webP campaign-Property" campaign
    Given I navigate to "In-line Content" page via "Web Personalization"
    Then I delete the Campaign with Name "Banner: in-line webP campaign-Property"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "in-line webP: User Condition > User IDs"

  Scenario: Verify inline content when Start Date: Now and End Date: Never is selected
    Given I navigate to "In-line Content" page via "Web Personalization"
    And I select "Banner" Campaign Type and enters "InLineWebP-Banner with Start Date:Now and End Date:Never" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    And save the campaign
    Then select property "button_Test (body > button)" and "Add after the property" as property placement
    Then select page to show as "All Pages"
    Then I fill the details in WHEN tab as below
      | START DATE | now   |  |  |
      | END DATE   | never |  |  |
    Then use the in-line WebP template "in-linewebPBanner" to fill message details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then check overview page is loaded for "InLineWebP-Banner with Start Date:Now and End Date:Never" campaign
    Given I navigate to "In-line Content" page via "Web Personalization"
    Then I add "InLineWebP-Banner with Start Date:Now and End Date:Never" to searchbox
    And I verify following details for "InLineWebP-Banner with Start Date:Now and End Date:Never" campaign
      | Start date: |
      | End date:   |
    Given I navigate to "In-line Content" page via "Web Personalization"
    Then I delete the Campaign with Name "InLineWebP-Banner with Start Date:Now and End Date:Never"

  @Smoke
  Scenario: Verify inline content when Start Date: Now and End Date: Till is selected
    Given I navigate to "In-line Content" page via "Web Personalization"
    And I select "Custom Code" Campaign Type and enters "InLineWebP-Custom Code with Start Date:Now and End Date:Till" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    And save the campaign
    Then select property "button_Test (body > button)" and "Add after the property" as property placement
    Then select page to show as "All Pages"
    Then I fill the details in WHEN tab as below
      | START DATE | now  |  |  |
      | END DATE   | till |  |  |
    Then use the in-line WebP template "in-linewebPCustom" to fill message details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then check overview page is loaded for "InLineWebP-Custom Code with Start Date:Now and End Date:Till" campaign
    Given I navigate to "In-line Content" page via "Web Personalization"
    Then I add "InLineWebP-Custom Code with Start Date:Now and End Date:Till" to searchbox
    And I verify following details for "InLineWebP-Custom Code with Start Date:Now and End Date:Till" campaign
      | Start date: |
      | End date:   |
    Given I navigate to "In-line Content" page via "Web Personalization"
    Then I delete the Campaign with Name "InLineWebP-Custom Code with Start Date:Now and End Date:Till"

  Scenario: Verify inline content when Start Date: Later and End Date: Never is selected
    Given I navigate to "In-line Content" page via "Web Personalization"
    And I select "Banner" Campaign Type and enters "InLineWebP-Banner with Start Date:Later and End Date:Never" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    And save the campaign
    Then select property "button_Test (body > button)" and "Add after the property" as property placement
    Then select page to show as "All Pages"
    Then I fill the details in WHEN tab as below
      | START DATE | later |  |  |
      | END DATE   | never |  |  |
    Then use the in-line WebP template "in-linewebPBanner" to fill message details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then check overview page is loaded for "InLineWebP-Banner with Start Date:Later and End Date:Never" campaign
    Given I navigate to "In-line Content" page via "Web Personalization"
    Then I add "InLineWebP-Banner with Start Date:Later and End Date:Never" to searchbox
    And I verify following details for "InLineWebP-Banner with Start Date:Later and End Date:Never" campaign
      | Start date: |
      | End date:   |
    Given I navigate to "In-line Content" page via "Web Personalization"
    Then I delete the Campaign with Name "InLineWebP-Banner with Start Date:Later and End Date:Never"

  Scenario: Verify inline content when Start Date: Later and End Date: Till is selected
    Given I navigate to "In-line Content" page via "Web Personalization"
    And I select "Custom Code" Campaign Type and enters "InLineWebP-Custom Code with Start Date:Later and End Date:Till" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    And save the campaign
    Then select property "button_Test (body > button)" and "Add after the property" as property placement
    Then select page to show as "All Pages"
    Then I fill the details in WHEN tab as below
      | START DATE | later |  |  |
      | END DATE   | till  |  |  |
    Then use the in-line WebP template "in-linewebPCustom" to fill message details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then check overview page is loaded for "InLineWebP-Custom Code with Start Date:Later and End Date:Till" campaign
    Given I navigate to "In-line Content" page via "Web Personalization"
    Then I add "InLineWebP-Custom Code with Start Date:Later and End Date:Till" to searchbox
    And I verify following details for "InLineWebP-Custom Code with Start Date:Later and End Date:Till" campaign
      | Start date: |
      | End date:   |
    Given I navigate to "In-line Content" page via "Web Personalization"
    Then I delete the Campaign with Name "InLineWebP-Custom Code with Start Date:Later and End Date:Till"

  @Smoke
  Scenario: Verify inline content when Specific days and hours is selected
    Given I navigate to "In-line Content" page via "Web Personalization"
    And I select "Banner" Campaign Type and enters "InLineWebP-Banner having specific days and hours" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    And save the campaign
    Then select property "button_Test (body > button)" and "Add after the property" as property placement
    Then select page to show as "All Pages"
    Then I fill the details in WHEN tab as below
      | START DATE | now   |  |  |
      | END DATE   | never |  |  |
      | SCHEDULE   | true  |  |  |
    Then use the in-line WebP template "in-linewebPBanner" to fill message details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then check overview page is loaded for "InLineWebP-Banner having specific days and hours" campaign
    Given I navigate to "In-line Content" page via "Web Personalization"
    Then I add "InLineWebP-Banner having specific days and hours" to searchbox
    And I verify following details for "InLineWebP-Banner having specific days and hours" campaign
      | From date: |
      | To date:   |
    Given I navigate to "In-line Content" page via "Web Personalization"
    Then I delete the Campaign with Name "InLineWebP-Banner having specific days and hours"

  @WebSDKSuite
  Scenario: Verify inline content when property placement is: Replace on a web page for Banner Layout
    Given I navigate to "In-line Content" page via "Web Personalization"
    And I select "Banner" Campaign Type and enters "InLineWebP-Banner-Replace Property" Campaign Name
    Then select Segment having name as "WebSDKUser"
    And save the campaign
    Then select property "test_login (#loginBtn)" and "Replace the property" as property placement
    Then select page to show as "All Pages"
    And save the campaign
    Then use the in-line WebP template "in-linewebPBanner" to fill message details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then I fetch "fetchInlineContentWebP" api and add dynamic CampaignId
    Then wait for "fetchInlineContentWebP" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    Then check overview page is loaded for "InLineWebP-Banner-Replace Property" campaign
    Given I navigate to "In-line Content" page via "Web Personalization"
    And open the dynamic webpage and login
    Then I verify property Placement on a web page with "inlineContent-webP-deactivate" api
    Given I navigate to "In-line Content" page via "Web Personalization"
    Then I delete the Campaign with Name "InLineWebP-Banner-Replace Property"

  @WebSDKSuite
  Scenario: Verify inline content when property placement is: AddBefore on a web page for Banner Layout
    Given I navigate to "In-line Content" page via "Web Personalization"
    And I select "Banner" Campaign Type and enters "InLineWebP-Banner-AddBefore Property" Campaign Name
    Then select Segment having name as "WebSDKUser"
    And save the campaign
    Then select property "test_login (#loginBtn)" and "Add before the property" as property placement
    Then select page to show as "All Pages"
    And save the campaign
    Then use the in-line WebP template "in-linewebPBanner" to fill message details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then I fetch "fetchInlineContentWebP" api and add dynamic CampaignId
    Then wait for "fetchInlineContentWebP" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    Then check overview page is loaded for "InLineWebP-Banner-AddBefore Property" campaign
    Given I navigate to "In-line Content" page via "Web Personalization"
    And open the dynamic webpage and login
    Then I verify property Placement on a web page with "inlineContent-webP-deactivate" api
    Given I navigate to "In-line Content" page via "Web Personalization"
    Then I delete the Campaign with Name "InLineWebP-Banner-AddBefore Property"

  @WebSDKSuite
  Scenario: Verify inline content when property placement is: AddAfter on a web page for Custom Code Layout
    Given I navigate to "In-line Content" page via "Web Personalization"
    And I select "Custom Code" Campaign Type and enters "InLineWebP-Custom Code-AddAfter Property" Campaign Name
    Then select Segment having name as "WebSDKUser"
    And save the campaign
    Then select property "test_login (#loginBtn)" and "Add after the property" as property placement
    Then select page to show as "All Pages"
    And save the campaign
    Then use the in-line WebP template "in-linewebPCustom" to fill message details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then I fetch "fetchInlineContentWebP" api and add dynamic CampaignId
    Then wait for "fetchInlineContentWebP" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    Then check overview page is loaded for "InLineWebP-Custom Code-AddAfter Property" campaign
    Given I navigate to "In-line Content" page via "Web Personalization"
    And open the dynamic webpage and login
    Then I verify property Placement on a web page with "inlineContent-webP-deactivate" api
    Given I navigate to "In-line Content" page via "Web Personalization"
    Then I delete the Campaign with Name "InLineWebP-Custom Code-AddAfter Property"

  @WebSDKSuite
  Scenario: Verify the custom width on a webpage for Banner Layout
    Given I navigate to "In-line Content" page via "Web Personalization"
    And I select "Banner" Campaign Type and enters "InLineWebP-Banner-Width" Campaign Name
    Then select Segment having name as "WebSDKUser"
    And save the campaign
    Then select property "test_login (#loginBtn)" and "Add after the property" as property placement
    Then select page to show as "All Pages"
    And save the campaign
    Then use the in-line WebP template "in-linewebPWidth" to fill message details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then I fetch "fetchInlineContentWebP" api and add dynamic CampaignId
    Then wait for "fetchInlineContentWebP" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    Then check overview page is loaded for "InLineWebP-Banner-Width" campaign
    Given I navigate to "In-line Content" page via "Web Personalization"
    And open the dynamic webpage and login
    Then verify in-line content on a web page using template "in-linewebPWidth" with "inlineContent-webP-deactivate" api
    Given I navigate to "In-line Content" page via "Web Personalization"
    Then I delete the Campaign with Name "InLineWebP-Banner-Width"

  @WebSDKSuite
  Scenario: Verify on webpage that in-line content is delivered for Banner Layout
    Given I navigate to "In-line Content" page via "Web Personalization"
    And I select "Banner" Campaign Type and enters "InLineWebP-Banner" Campaign Name
    Then select Segment having name as "WebSDKUser"
    And save the campaign
    Then select property "test_login (#loginBtn)" and "Add after the property" as property placement
    Then select page to show as "All Pages"
    And save the campaign
    Then use the in-line WebP template "in-linewebPBanner" to fill message details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then I fetch "fetchInlineContentWebP" api and add dynamic CampaignId
    Then wait for "fetchInlineContentWebP" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    Then check overview page is loaded for "InLineWebP-Banner" campaign
    Given I navigate to "In-line Content" page via "Web Personalization"
    And open the dynamic webpage and login
    Then verify in-line content on a web page using template "in-linewebPBanner" with "inlineContent-webP-deactivate" api
    Then I navigate to "Live Segments" page via "Segments"
    And I verify "WebSDKUser" segment is present
    Given I navigate to "In-line Content" page via "Web Personalization"
    Then I delete the Campaign with Name "InLineWebP-Banner"

  @WebSDKSuite
  Scenario: Verify on webpage that in-line content is delivered when Custom HTMLandCSS is added in Banner Layout
    Given I navigate to "In-line Content" page via "Web Personalization"
    And I select "Banner" Campaign Type and enters "InLineWebP-BannerCustom" Campaign Name
    Then select Segment having name as "WebSDKUser"
    And save the campaign
    Then select property "test_login (#loginBtn)" and "Add after the property" as property placement
    Then select page to show as "All Pages"
    And save the campaign
    Then use the in-line WebP template "in-linewebPBannerCustom" to fill message details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then I fetch "fetchInlineContentWebP" api and add dynamic CampaignId
    Then wait for "fetchInlineContentWebP" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    Then check overview page is loaded for "InLineWebP-BannerCustom" campaign
    Given I navigate to "In-line Content" page via "Web Personalization"
    And open the dynamic webpage and login
    Then verify in-line content on a web page using template "in-linewebPBannerCustom" with "inlineContent-webP-deactivate" api
    Given I navigate to "In-line Content" page via "Web Personalization"
    Then I delete the Campaign with Name "InLineWebP-BannerCustom"

  @WebSDKSuite
  Scenario: Verify on webpage that in-line content is delivered for Custom Code Layout
    Given I navigate to "In-line Content" page via "Web Personalization"
    And I select "Custom Code" Campaign Type and enters "InLineWebP-Custom Code" Campaign Name
    Then select Segment having name as "WebSDKUser"
    And save the campaign
    Then select property "test_login (#loginBtn)" and "Add after the property" as property placement
    Then select page to show as "All Pages"
    And save the campaign
    Then use the in-line WebP template "in-linewebPCustom" to fill message details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then I fetch "fetchInlineContentWebP" api and add dynamic CampaignId
    Then wait for "fetchInlineContentWebP" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    Then check overview page is loaded for "InLineWebP-Custom Code" campaign
    Given I navigate to "In-line Content" page via "Web Personalization"
    And open the dynamic webpage and login
    Then verify in-line content on a web page using template "in-linewebPCustom" with "inlineContent-webP-deactivate" api
    Given I navigate to "In-line Content" page via "Web Personalization"
    Then I delete the Campaign with Name "InLineWebP-Custom Code"

  Scenario: To verify overview page is loaded when campaign status is paused for Banner InlineContent WebP
    Given I navigate to "In-line Content" page via "Web Personalization"
    And I select "Banner" Campaign Type and enters "InlineContentWeb-Banner-Overview Page Validation for Paused status" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    And save the campaign
    Then select property "button_Test (body > button)" and "Add after the property" as property placement
    Then select page to show as "All Pages"
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then use the in-line WebP template "in-linewebPBanner" to fill message details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then I pause the campaign with "inlineContent-webP-deactivate" api
    Then check overview page is loaded for "InlineContentWeb-Banner-Overview Page Validation for Paused status" campaign
    Given I navigate to "In-line Content" page via "Web Personalization"
    Then I delete the Campaign with Name "InlineContentWeb-Banner-Overview Page Validation for Paused status"

  Scenario: To verify overview page is loaded when campaign status is paused for CustomCode InlineContent WebP
    Given I navigate to "In-line Content" page via "Web Personalization"
    And I select "Custom Code" Campaign Type and enters "InlineContentWeb-CustomCode-Overview Page Validation for Paused status" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    And save the campaign
    Then select property "button_Test (body > button)" and "Add after the property" as property placement
    Then select page to show as "All Pages"
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then use the in-line WebP template "in-linewebPCustom" to fill message details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then I pause the campaign with "inlineContent-webP-deactivate" api
    Then check overview page is loaded for "InlineContentWeb-CustomCode-Overview Page Validation for Paused status" campaign
    Given I navigate to "In-line Content" page via "Web Personalization"
    Then I delete the Campaign with Name "InlineContentWeb-CustomCode-Overview Page Validation for Paused status"

  Scenario: To verify overview page is loaded when campaign status is in draft state for Banner InlineContent WebP
    Given I navigate to "In-line Content" page via "Web Personalization"
    And I select "Banner" Campaign Type and enters "InlineContentWeb-Banner-Overview Page Validation for draft status" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    And save the campaign
    Then select property "button_Test (body > button)" and "Add after the property" as property placement
    Then select page to show as "All Pages"
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then use the in-line WebP template "in-linewebPBanner" to fill message details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    Given I navigate to "In-line Content" page via "Web Personalization"
    Then check overview page is loaded for "InlineContentWeb-Banner-Overview Page Validation for draft status" campaign
    Given I navigate to "In-line Content" page via "Web Personalization"
    Then I delete the Campaign with Name "InlineContentWeb-Banner-Overview Page Validation for draft status"

  Scenario: To verify overview page is loaded when campaign status is is in draft state for CustomCode InlineContent WebP
    Given I navigate to "In-line Content" page via "Web Personalization"
    And I select "Custom Code" Campaign Type and enters "InlineContentWeb-CustomCode-Overview Page Validation for draft status" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    And save the campaign
    Then select property "button_Test (body > button)" and "Add after the property" as property placement
    Then select page to show as "All Pages"
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then use the in-line WebP template "in-linewebPCustom" to fill message details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    Given I navigate to "In-line Content" page via "Web Personalization"
    Then check overview page is loaded for "InlineContentWeb-CustomCode-Overview Page Validation for draft status" campaign
    Given I navigate to "In-line Content" page via "Web Personalization"
    Then I delete the Campaign with Name "InlineContentWeb-CustomCode-Overview Page Validation for draft status"

  @WebSDKSuite
  Scenario: To verify unicode characterset contents are showing properly for in-line web for "CustomCode" layout
    Given I navigate to "In-line Content" page via "Web Personalization"
    And I select "Custom Code" Campaign Type and enters "Unicode: InLineWebP-Custom Code ™ ℣ ℤ ℥ Ω ℧ ℨ ℩ K Å ℬ ℭ" Campaign Name
    Then select Segment having name as "WebSDKUser"
    And save the campaign
    Then select property "test_login (#loginBtn)" and "Add after the property" as property placement
    Then select page to show as "All Pages"
    And save the campaign
    Then use the in-line WebP template "Unicode-InlinewebPCustom" to fill message details
    Then fill the page via "Unicode-InlinewebPCustom" template from "MultiLingualTemplates" folder using "Basic" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then I fetch "fetchInlineContentWebP" api and add dynamic CampaignId
    Then wait for "fetchInlineContentWebP" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    And I verify "Unicode: InLineWebP-Custom Code ™ ℣ ℤ ℥ Ω ℧ ℨ ℩ K Å ℬ ℭ" Campaign name with unicode characters on overview page
    Given I navigate to "In-line Content" page via "Web Personalization"
    And open the dynamic webpage and login
    Then verify in-line content on a web page using template "Unicode-InlinewebPCustom" with "inlineContent-webP-deactivate" api
    Given I navigate to "In-line Content" page via "Web Personalization"
    Then I delete the Campaign with unicode characters "Unicode: InLineWebP-Custom Code ™ ℣ ℤ ℥ Ω ℧ ℨ ℩ K Å ℬ ℭ"

  @WebSDKSuite
  Scenario Outline: To verify campaign showing properly on targeting webpage with screen data with <datatype> datatype with <bannerplacement>
    Given I navigate to "In-line Content" page via "Web Personalization"
    And I select "Banner" Campaign Type and enters "Screen Variable <datatype>" Campaign Name
    Then select Segment having name as "WebSDKUser"
    And save the campaign
    And select property "test_login (#loginBtn)" and "<bannerplacement>" as property placement
    And select page to show as "All Pages"
    Then I screen data as datatype using table
      | DataType       | <datatype>  |
      | AttributeName  | <attribute> |
      | Operator       | <operator>  |
      | AttributeValue | <value>     |
    And save the campaign
    Then use the in-line WebP template "<template>" to fill message details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then I fetch "fetchInlineContentWebP" api and add dynamic CampaignId
    Then wait for "fetchInlineContentWebP" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    And open the dynamic webpage and login
    Then verify in-line content on a web page using template "<template>" with "inlineContent-webP-deactivate" api
    And open the dynamic webpage and login
    Then I verify property Placement on a web page with "inlineContent-webP-deactivate" api
    Given I navigate to "In-line Content" page via "Web Personalization"
    Then I delete the Campaign with unicode characters "Screen Variable <datatype>"

    Examples: 
      | datatype | attribute     | operator | value          | template    | bannerplacement         |
      | String   | productname   | equal to | Rolex Watch x2 | WebPString  | Add after the property  |
      | Number   | quantity      | equal to |              4 | WebPNumber  | Add before the property |
      | Boolean  | promocodeused | equal to | true           | WebPBoolean | Replace the property    |
