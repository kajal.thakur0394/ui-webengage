@EventAnalyticsExportList @Regression
Feature: Event Analytics export to List Feature

  Scenario Outline: To verify the user is able to click on zoom in option for Custom/System event
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType> |
      | OF       | <event>    |
      | OVER     | Days       |
      | SPLIT BY | <splitby>  |
    Then open "Line" view for the section with header name as "SHOW"
    Then I click on "zoom" option
    Then I validate the actual value to be "<operator>" expected value having "<Total>" for "<showType>" and "last 7 days" via "Line" graph
      | Parameter | Value   |
      | <event>   | <Total> |
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType> |
      | OF       | <event>    |
      | OVER     | Days       |
      | SPLIT BY | <splitby>  |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I click on "zoom" option
    Then I validate the actual value to be "<operator>" expected value having "<Total>" for "<showType>" and "last 7 days" via "Bar" graph
      | Parameter | Value   |
      | <event>   | <Total> |

    Examples: 
      | showType    | Total | event           | operator     | splitby      |
      | Occurrences |    50 | multi_event_A   | equal to     | E_1          |
      | Uniques     |    50 | multi_event_A   | equal to     | E_1          |
      | Occurrences |     1 | Session Started | greater than | Browser Name |
      | Uniques     |     1 | Session Started | greater than | Browser Name |

  Scenario Outline: To verify that static list is getting created with Data Point option using line and bar graph for Single/Multiple system/custom event
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType> |
      | OF       | <event>    |
      | OVER     | Days       |
      | SPLIT BY | -          |
    Then open "<graphType>" view for the section with header name as "SHOW"
    Then I click on "Data Point" option
    Then I fill "Static" list modal page with time period " " and name "<listName>"
      | Static | List |
    Then I click on "Create List" button
    Then check whether the static segment has been "Created" and gets listed on the page.
    Given I navigate to "Lists" page via "Segments"
    Then verify "Type" as "Static" for "<listName>" static List
    Then I verify "Edit" action is present for "<listName>"
    Then I delete static segment with Name "<listName>"

    Examples: 
      | showType | graphType | event                                                              | listName                                                        |
      | Uniques  | Line      | multi_event_A                                                      | Single custom event Static list using data point and Line graph |
      | Uniques  | Bar       | multi_event_A                                                      | Single custom event Static list using data point and Bar graph  |
      | Uniques  | Line      | Session Started                                                    | Single system event Static list using data point and Line graph |
      | Uniques  | Bar       | Session Started                                                    | Single system event Static list using data point and Bar graph  |
      | Uniques  | Line      | multi_event_B,multi_event_C,multi_event_D,multi_event_E            | Multi custom events Static list using data point and Line graph |
      | Uniques  | Bar       | multi_event_B,multi_event_C,multi_event_D,multi_event_E            | Multi custom events Static list using data point and Bar graph  |
      | Uniques  | Line      | Session Started,Web Personalization Impression,Campaign Conversion | Multi system events Static list using data point and Line graph |
      | Uniques  | Bar       | Session Started,Web Personalization Impression,Campaign Conversion | Multi system events Static list using data point and Bar graph  |

  Scenario Outline: To verify that refreshing list is getting created with Data Point option using line and bar graph for Single/Multiple system/custom event
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType> |
      | OF       | <event>    |
      | OVER     | Days       |
      | SPLIT BY | -          |
    Then open "<graphType>" view for the section with header name as "SHOW"
    Then I click on "Data Point" option
    Then I fill "Refreshing" list modal page with time period "Last 30 days" and name "<listName>"
      | when | Monthly>First day of Month |
      | Time | 3:30:am                    |
    Then I click on "Create List" button
    Then check whether the static segment has been "Created" and gets listed on the page.
    Given I navigate to "Lists" page via "Segments"
    Then verify "Type" as "Refreshing" for "<listName>" static List
    Then I delete static segment with Name "<listName>"

    Examples: 
      | showType | graphType | event                                                              | listName                                                            |
      | Uniques  | Line      | multi_event_A                                                      | Single custom event Refreshing list using Data Point and Line graph |
      | Uniques  | Bar       | multi_event_A                                                      | Single custom event Refreshing list using Data Point and Bar graph  |
      | Uniques  | Line      | Session Started                                                    | Single system event Refreshing list using Data Point and Line graph |
      | Uniques  | Bar       | Session Started                                                    | Single system event Refreshing list using Data Point and Bar graph  |
      | Uniques  | Line      | multi_event_B,multi_event_C,multi_event_D,multi_event_E            | Multi custom events Refreshing list using Data Point and Line graph |
      | Uniques  | Bar       | multi_event_B,multi_event_C,multi_event_D,multi_event_E            | Multi custom events Refreshing list using Data Point and Bar graph  |
      | Uniques  | Line      | Session Started,Web Personalization Impression,Campaign Conversion | Multi system events Refreshing list using Data Point and Line graph |
      | Uniques  | Bar       | Session Started,Web Personalization Impression,Campaign Conversion | Multi system events Refreshing list using Data Point and Bar graph  |

  Scenario Outline: To verify that static list is getting created with Series option using line and bar graph for Single/Multiple system/custom event
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType> |
      | OF       | <event>    |
      | OVER     | Days       |
      | SPLIT BY | -          |
    Then open "<graphType>" view for the section with header name as "SHOW"
    Then I click on "Series" option
    Then I fill "Static" list modal page with time period " " and name "<listName>"
      | Static | List |
    Then I click on "Create List" button
    Then check whether the static segment has been "Created" and gets listed on the page.
    Given I navigate to "Lists" page via "Segments"
    Then verify "Type" as "Static" for "<listName>" static List
    Then I verify "Edit" action is present for "<listName>"
    Then I delete static segment with Name "<listName>"

    Examples: 
      | showType | graphType | event                                                              | listName                                                    |
      | Uniques  | Line      | multi_event_A                                                      | Single custom event Static list using Series and Line graph |
      | Uniques  | Bar       | multi_event_A                                                      | Single custom event Static list using Series and Bar graph  |
      | Uniques  | Line      | Session Started                                                    | Single system event Static list using Series and Line graph |
      | Uniques  | Bar       | Session Started                                                    | Single system event Static list using Series and Bar graph  |
      | Uniques  | Line      | multi_event_B,multi_event_C,multi_event_D,multi_event_E            | Multi custom events Static list using Series and Line graph |
      | Uniques  | Bar       | multi_event_B,multi_event_C,multi_event_D,multi_event_E            | Multi custom events Static list using Series and Bar graph  |
      | Uniques  | Line      | Session Started,Web Personalization Impression,Campaign Conversion | Multi system events Static list using Series and Line graph |
      | Uniques  | Bar       | Session Started,Web Personalization Impression,Campaign Conversion | Multi system events Static list using Series and Bar graph  |

  Scenario Outline: To verify that refreshing list is getting created with Series option using line and bar graph for Single/Multiple system/custom event
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType> |
      | OF       | <event>    |
      | OVER     | Days       |
      | SPLIT BY | -          |
    Then open "<graphType>" view for the section with header name as "SHOW"
    Then I click on "Series" option
    Then I fill "Refreshing" list modal page with time period "Last 30 days" and name "<listName>"
      | when | Monthly>First day of Month |
      | Time | 3:30:am                    |
    Then I click on "Create List" button
    Then check whether the static segment has been "Created" and gets listed on the page.
    Given I navigate to "Lists" page via "Segments"
    Then verify "Type" as "Refreshing" for "<listName>" static List
    Then I delete static segment with Name "<listName>"

    Examples: 
      | showType | graphType | event                                                              | listName                                                        |
      | Uniques  | Line      | multi_event_A                                                      | Single custom event Refreshing list using Series and Line graph |
      | Uniques  | Bar       | multi_event_A                                                      | Single custom event Refreshing list using Series and Bar graph  |
      | Uniques  | Line      | Session Started                                                    | Single system event Refreshing list using Series and Line graph |
      | Uniques  | Bar       | Session Started                                                    | Single system event Refreshing list using Series and Bar graph  |
      | Uniques  | Line      | multi_event_B,multi_event_C,multi_event_D,multi_event_E            | Multi custom events Refreshing list using Series and Line graph |
      | Uniques  | Bar       | multi_event_B,multi_event_C,multi_event_D,multi_event_E            | Multi custom events Refreshing list using Series and Bar graph  |
      | Uniques  | Line      | Session Started,Web Personalization Impression,Campaign Conversion | Multi system events Refreshing list using Series and Line graph |
      | Uniques  | Bar       | Session Started,Web Personalization Impression,Campaign Conversion | Multi system events Refreshing list using Series and Bar graph  |

  Scenario Outline: To verify the character limit for List name: Name of the segment
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType> |
      | OF       | <event>    |
      | OVER     | Days       |
      | SPLIT BY | -          |
    Then open "<graphType>" view for the section with header name as "SHOW"
    Then I click on "Series" option
    Then I compose a string having length as "266" and compare it with limit of "255"

    Examples: 
      | showType | graphType | event         |
      | Uniques  | Line      | multi_event_A |

  Scenario Outline: To verify the static list has time period label with To and From date
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "<dateType>" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType> |
      | OF       | <event>    |
      | OVER     | Days       |
      | SPLIT BY | -          |
    Then open "<graphType>" view for the section with header name as "SHOW"
    Then I click on "Series" option
    Then I verify static list has time period label with To and From date for "<dateType>"

    Examples: 
      | showType | graphType | event         | dateType     |
      | Uniques  | Line      | multi_event_A | Last 7 days  |
      | Uniques  | Line      | multi_event_A | Today        |
      | Uniques  | Line      | multi_event_A | Yesterday    |
      | Uniques  | Line      | multi_event_A | Last 30 days |

  Scenario Outline: To verify the refreshing list time period with last X days is allowing to create the list for line and bar graph for Single/Multiple system/custom event
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType> |
      | OF       | <event>    |
      | OVER     | Days       |
      | SPLIT BY | -          |
    Then open "<graphType>" view for the section with header name as "SHOW"
    Then I click on "<DataShowType>" option
    Then I fill "Refreshing" list modal page with time period "Last 30 days" and name "Single/Multi event Refreshing list test using series/data point"
      | when | Monthly>First day of Month |
      | Time | 3:30:am                    |
    Then I click on "Create List" button
    Then check whether the static segment has been "Created" and gets listed on the page.
    Given I navigate to "Lists" page via "Segments"
    Then verify "Type" as "Refreshing" for "Single/Multi event Refreshing list test using series/data point" static List
    Then I delete static segment with Name "Single/Multi event Refreshing list test using series/data point"

    Examples: 
      | showType | graphType | event                                                              | DataShowType |
      | Uniques  | Line      | multi_event_A                                                      | Data Point   |
      | Uniques  | Bar       | Session Started                                                    | Series       |
      | Uniques  | Bar       | multi_event_B,multi_event_C,multi_event_D,multi_event_E            | Series       |
      | Uniques  | Line      | Session Started,Web Personalization Impression,Campaign Conversion | Data Point   |

  Scenario Outline: To verify table data for refreshing list with Series option using line and bar graph for Single custom event
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType> |
      | OF       | <event>    |
      | OVER     | Days       |
      | SPLIT BY | -          |
    Then open "<graphType>" view for the section with header name as "SHOW"
    Then I click on "Series" option
    Then I fill "Refreshing" list modal page with time period "Last 30 days" and name "Single/Multi event Refreshing list test using series"
      | when | Monthly>First day of Month |
      | Time | 3:30:am                    |
    Then I validate the actual value for events to be "equal to" expected value in table displayed
      | Parameter     | Value |
      | multi_event_A |   350 |

    Examples: 
      | showType | graphType | event         |
      | Uniques  | Line      | multi_event_A |
      | Uniques  | Bar       | multi_event_A |

  Scenario Outline: To verify table data for static list with Series option using line and bar graph for Multiple custom events
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType> |
      | OF       | <event>    |
      | OVER     | Days       |
      | SPLIT BY | -          |
    Then open "<graphType>" view for the section with header name as "SHOW"
    Then I click on "Series" option
    Then I fill "Static" list modal page with time period " " and name "Multi event Static list using series"
      | Static | List |
    Then I validate the actual value for events to be "equal to" expected value in table displayed
      | Parameter     | Value |
      | multi_event_A |   350 |
      | multi_event_C |   210 |

    Examples: 
      | showType | graphType | event                       |
      | Uniques  | Line      | multi_event_A,multi_event_C |
      | Uniques  | Bar       | multi_event_A,multi_event_C |

  Scenario Outline: To verify that it is showing all the data points for the selected series
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType> |
      | OF       | <event>    |
      | OVER     | Days       |
      | SPLIT BY | -          |
    Then open "<graphType>" view for the section with header name as "SHOW"
    Then I click on "Data Point" option
    Then I validate the presence of all data points of "<event>"

    Examples: 
      | showType | graphType | event           |
      | Uniques  | Line      | multi_event_A   |
      | Uniques  | Bar       | multi_event_A   |
      | Uniques  | Line      | Session Started |
      | Uniques  | Bar       | Session Started |

  Scenario Outline: To verify on create list page it is allowing users to search for data points by typing the labels of the data point
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType> |
      | OF       | <event>    |
      | OVER     | Days       |
      | SPLIT BY | -          |
    Then open "<graphType>" view for the section with header name as "SHOW"
    Then I click on "Data Point" option
    Then I search and verify presence of data point "<event>" in the table

    Examples: 
      | showType | graphType | event           |
      | Uniques  | Line      | multi_event_A   |
      | Uniques  | Bar       | multi_event_A   |
      | Uniques  | Line      | Session Started |
      | Uniques  | Bar       | Session Started |

  Scenario Outline: To verify the selection and de-selection of Select All/View Selected
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType> |
      | OF       | <event>    |
      | OVER     | Days       |
      | SPLIT BY | -          |
    Then open "<graphType>" view for the section with header name as "SHOW"
    Then I click on "Data Point" option
    Then I select and de-select the "<selectionType>" option and verify options are selected properly

    Examples: 
      | showType | graphType | event           | selectionType |
      | Uniques  | Line      | multi_event_A   | Select All    |
      | Uniques  | Bar       | multi_event_A   | Select All    |
      | Uniques  | Line      | Session Started | Select All    |
      | Uniques  | Bar       | Session Started | Select All    |
      | Uniques  | Line      | multi_event_A   | View Selected |
      | Uniques  | Bar       | multi_event_A   | View Selected |
      | Uniques  | Line      | Session Started | View Selected |
      | Uniques  | Bar       | Session Started | View Selected |

  Scenario Outline: To verify it is allowing to select multiple data points
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType> |
      | OF       | <event>    |
      | OVER     | Days       |
      | SPLIT BY | -          |
    Then open "<graphType>" view for the section with header name as "SHOW"
    Then I click on "<option>" option
    Then I select "<numberOFDataPoints>" data points

    Examples: 
      | showType | graphType | event                                                              | option     | numberOFDataPoints |
      | Uniques  | Line      | multi_event_A                                                      | Data Point |                  6 |
      | Uniques  | Bar       | multi_event_A                                                      | Data Point |                  3 |
      | Uniques  | Line      | Session Started                                                    | Data Point |                  2 |
      | Uniques  | Bar       | Session Started                                                    | Data Point |                  4 |
      | Uniques  | Bar       | multi_event_B,multi_event_C,multi_event_D,multi_event_E            | Series     |                  3 |
      | Uniques  | Line      | Session Started,Web Personalization Impression,Campaign Conversion | Series     |                  2 |
      | Uniques  | Line      | multi_event_B,multi_event_C,multi_event_D,multi_event_E            | Series     |                  3 |
      | Uniques  | Bar       | Session Started,Web Personalization Impression,Campaign Conversion | Series     |                  2 |

  Scenario Outline: To verify that it is showing all the series that belong to the chart with/without Split by for Single/Multiple System/Custom events
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>         |
      | OF       | <event>            |
      | OVER     | Days               |
      | SPLIT BY | <splitByAttribute> |
    Then open "<graphType>" view for the section with header name as "SHOW"
    Then I click on "Series" option
    Then I verify all the events of the chart with "<attributeValue>" attribute value

    Examples: 
      | showType | graphType | event                                                              | splitByAttribute | attributeValue |
      | Uniques  | Line      | multi_event_A                                                      | E_1              | Testing        |
      | Uniques  | Bar       | multi_event_A                                                      | E_1              | Testing        |
      | Uniques  | Line      | Session Started                                                    | Browser Name     | Chrome         |
      | Uniques  | Bar       | Session Started                                                    | Browser Name     | Chrome         |
      | Uniques  | Line      | multi_event_A                                                      | -                | -              |
      | Uniques  | Bar       | multi_event_A                                                      | -                | -              |
      | Uniques  | Line      | Session Started                                                    | -                | -              |
      | Uniques  | Bar       | Session Started                                                    | -                | -              |
      | Uniques  | Bar       | multi_event_B,multi_event_C,multi_event_D,multi_event_E            | -                | -              |
      | Uniques  | Line      | Session Started,Web Personalization Impression,Campaign Conversion | -                | -              |
      | Uniques  | Line      | multi_event_B,multi_event_C,multi_event_D,multi_event_E            | -                | -              |
      | Uniques  | Bar       | Session Started,Web Personalization Impression,Campaign Conversion | -                | -              |

  Scenario Outline: To verify refreshing list last X days min and max limit for <eventType> event using <dataType> and <graphType> graph and <timePeriod> days  
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType> |
      | OF       | <event>    |
      | OVER     | Days       |
      | SPLIT BY | -          |
    Then open "<graphType>" view for the section with header name as "SHOW"
    Then I click on "<dataType>" option
    Then I verify "<timePeriod>" condition for "<listName>"

    Examples: 
      | showType | graphType | event                                     | listName                  | timePeriod | dataType   | eventType       |
      | Uniques  | Line      | multi_event_A                             | Max limit for last X days |        367 | Data Point | Single custom   |
      | Uniques  | Bar       | multi_event_A                             | Max limit for last X days |        367 | Data Point | Single custom   |
      | Uniques  | Line      | multi_event_A                             | Min limit for last X days |          0 | Data Point | Single custom   |
      | Uniques  | Bar       | multi_event_A                             | Min limit for last X days |          0 | Data Point | Single custom   |
      | Uniques  | Line      | multi_event_A                             | Max limit for last X days |        367 | Series     | Single custom   |
      | Uniques  | Bar       | multi_event_A                             | Max limit for last X days |        367 | Series     | Single custom   |
      | Uniques  | Line      | multi_event_A                             | Min limit for last X days |          0 | Series     | Single custom   |
      | Uniques  | Bar       | multi_event_A                             | Min limit for last X days |          0 | Series     | Single custom   |
      | Uniques  | Line      | Session Started                           | Max limit for last X days |        367 | Data Point | Single System   |
      | Uniques  | Bar       | Session Started                           | Max limit for last X days |        367 | Data Point | Single System   |
      | Uniques  | Line      | Session Started                           | Min limit for last X days |          0 | Data Point | Single System   |
      | Uniques  | Bar       | Session Started                           | Min limit for last X days |          0 | Data Point | Single System   |
      | Uniques  | Line      | Session Started                           | Max limit for last X days |        367 | Series     | Single System   |
      | Uniques  | Bar       | Session Started                           | Max limit for last X days |        367 | Series     | Single System   |
      | Uniques  | Line      | Session Started                           | Min limit for last X days |          0 | Series     | Single System   |
      | Uniques  | Bar       | Session Started                           | Min limit for last X days |          0 | Series     | Single System   |
      | Uniques  | Line      | multi_event_A,multi_event_B,multi_event_C | Max limit for last X days |        367 | Series     | Multiple custom |
      | Uniques  | Bar       | multi_event_A,multi_event_B,multi_event_C | Max limit for last X days |        367 | Series     | Multiple custom |
      | Uniques  | Line      | multi_event_A,multi_event_B,multi_event_C | Min limit for last X days |          0 | Series     | Multiple custom |
      | Uniques  | Bar       | multi_event_A,multi_event_B,multi_event_C | Min limit for last X days |          0 | Series     | Multiple custom |
      | Uniques  | Line      | Session Started,Campaign Conversion       | Max limit for last X days |        367 | Series     | Multiple system |
      | Uniques  | Bar       | Session Started,Campaign Conversion       | Max limit for last X days |        367 | Series     | Multiple system |
      | Uniques  | Line      | Session Started,Campaign Conversion       | Min limit for last X days |          0 | Series     | Multiple system |
      | Uniques  | Bar       | Session Started,Campaign Conversion       | Min limit for last X days |          0 | Series     | Multiple system |