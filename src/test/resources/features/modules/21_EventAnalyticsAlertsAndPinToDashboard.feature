@EventAnalyticsAlertsAndPinToDashboard @Regression
Feature: Event Analytics Alerts and Pin to Dashobard TCs

  Scenario Outline: To verify that user can select upto 10 events at a time with the combination of system events and /or custom events to compare
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>                                                                                                                                                                     |
      | OF       | multi_event,multi_event_A,multi_event_B,multi_event_C,multi_event_D,TriggerEmailEvent,TriggerPushEvent,TriggerSMSEvent,TriggerWebPushEvent,TriggerWhatsAppEvent,Email Accepted |
      | OVER     | -                                                                                                                                                                              |
      | SPLIT BY | -                                                                                                                                                                              |

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify on clicking reset on event filter page only event page details to should reset for Multi Events
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>                  |
      | OF       | multi_event_A,multi_event_B |
      | OVER     | Days                        |
      | SPLIT BY | -                           |
    Then I apply filter for events
      | Event   | multi_event_A    | E_1                           | equal to | Testing |
      | Event   | multi_event_B    | E_2                           | equal to |     123 |
      | Segment | Include Segments | IncludeList                   |          |         |
      | Segment | Include Segments | Include3UsersList             |          |         |
      | Segment | Exclude Segments | ExcludeList                   |          |         |
      | Segment | Exclude Segments | StaticListStatsValidationUser |          |         |
    Then I Reset filter for "Event" tab

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify on clicking reset on event filter page only event page details to should reset for Single Event
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>    |
      | OF       | multi_event_A |
      | OVER     | Days          |
      | SPLIT BY | -             |
    Then I apply filter for events
      | Event   | multi_event_A    | E_1                           | equal to | Testing |
      | Segment | Include Segments | IncludeList                   |          |         |
      | Segment | Include Segments | Include3UsersList             |          |         |
      | Segment | Exclude Segments | ExcludeList                   |          |         |
      | Segment | Exclude Segments | StaticListStatsValidationUser |          |         |
    Then I Reset filter for "Event" tab

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify on clicking reset on segment filter page only segment page details for Multi Events
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>                  |
      | OF       | multi_event_A,multi_event_B |
      | OVER     | Days                        |
      | SPLIT BY | -                           |
    Then I apply filter for events
      | Event   | multi_event_A    | E_1                           | equal to | Testing |
      | Event   | multi_event_B    | E_2                           | equal to |     123 |
      | Segment | Include Segments | IncludeList                   |          |         |
      | Segment | Include Segments | Include3UsersList             |          |         |
      | Segment | Exclude Segments | ExcludeList                   |          |         |
      | Segment | Exclude Segments | StaticListStatsValidationUser |          |         |
    Then I Reset filter for "Segment" tab

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify on clicking reset on segment filter page only segment page details for Single Event
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>    |
      | OF       | multi_event_A |
      | OVER     | Days          |
      | SPLIT BY | -             |
    Then I apply filter for events
      | Event   | multi_event_A    | E_1                           | equal to | Testing |
      | Segment | Include Segments | IncludeList                   |          |         |
      | Segment | Include Segments | Include3UsersList             |          |         |
      | Segment | Exclude Segments | ExcludeList                   |          |         |
      | Segment | Exclude Segments | StaticListStatsValidationUser |          |         |
    Then I Reset filter for "Segment" tab

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify the user filter without split by and then try to click on bell icon it would be greyed out
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>    |
      | OF       | multi_event_A |
      | OVER     | Days          |
      | SPLIT BY | -             |
    Then I apply filter for events
      | Event | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_testing_1_test_value |
    Then I verify bell icon should be greyed out

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify segment filter without split by and then try to click on bell icon it would be greyed out
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>    |
      | OF       | multi_event_A |
      | OVER     | Days          |
      | SPLIT BY | -             |
    Then I apply filter for events
      | Segment | Include Segments | IncludeList |
    Then I verify bell icon should be greyed out

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify the user filter with split by and then try to click on bell icon it would be greyed out
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>  |
      | OF       | multi_event |
      | OVER     | Days        |
      | SPLIT BY | new_attr_2  |
    Then I apply filter for events
      | Event | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_testing_1_test_value |
    Then I verify bell icon should be greyed out

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify segment filter with split by and then try to click on bell icon it would be greyed out
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>  |
      | OF       | multi_event |
      | OVER     | Days        |
      | SPLIT BY | new_attr_2  |
    Then I apply filter for events
      | Segment | Include Segments | IncludeList |
    Then I verify bell icon should be greyed out

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify segment filter and user filter with split by and then try to click on bell icon it would be greyed out
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>  |
      | OF       | multi_event |
      | OVER     | Days        |
      | SPLIT BY | new_attr_2  |
    Then I apply filter for events
      | Event   | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_testing_1_test_value |
      | Segment | Include Segments      | IncludeList         |          |                                |
    Then I verify bell icon should be greyed out

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify segment filter and user filter without split by and then try to click on bell icon it would be greyed out
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>  |
      | OF       | multi_event |
      | OVER     | Days        |
      | SPLIT BY | -           |
    Then I apply filter for events
      | Event   | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_testing_1_test_value |
      | Segment | Include Segments      | IncludeList         |          |                                |
    Then I verify bell icon should be greyed out

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify if 1 out of 3 events is removed and a common filter [common event attribute] had been applied. Then the common filter [common event attribute] will be retained for the existing events
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>                                |
      | OF       | multi_event_A,multi_event_B,multi_event_C |
      | OVER     | Days                                      |
      | SPLIT BY | -                                         |
    Then I apply filter for events
      | Event | Common Filter | E_1 | equal to | Testing |
    Then I remove event "multi_event_A" and validate common filter "E_1" is retained for existing events

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify the system event and system event filter is selected with split by and then create a alert where all the selected attribute is displaying on the alert creation page
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>      |
      | OF       | Session Started |
      | OVER     | Days            |
      | SPLIT BY | Country         |
    Then I apply filter for events
      | Event | Session Started | Browser Name | equal to | Chrome |
    Then I create custom alert with name "Custom alert Creation"
      | Send Alert   | Above    | specific value |
      | Browser Name | equal to | Chrome         |
    Given I navigate to "Alerts" page via "Data Platform"
    Then I delete the created custom alert with name "Custom alert Creation"

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify the user filter without split by and then try to click on bell icon it would be greyed out for System event
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>      |
      | OF       | Session Started |
      | OVER     | Days            |
      | SPLIT BY | -               |
    Then I apply filter for events
      | Event | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_value_1 |
    Then I verify bell icon should be greyed out

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify the user filter with split by and then try to click on bell icon it would be greyed out for System event
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>      |
      | OF       | Session Started |
      | OVER     | Days            |
      | SPLIT BY | Browser Name    |
    Then I apply filter for events
      | Event | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_value_1 |
    Then I verify bell icon should be greyed out

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify segment filter without split by and then try to click on bell icon it would be greyed out for System event
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>      |
      | OF       | Session Started |
      | OVER     | Days            |
      | SPLIT BY | -               |
    Then I apply filter for events
      | Segment | Include Segments | IncludeList |
    Then I verify bell icon should be greyed out

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify segment filter with split by and then try to click on bell icon it would be greyed out for System event
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>      |
      | OF       | Session Started |
      | OVER     | Days            |
      | SPLIT BY | Browser Name    |
    Then I apply filter for events
      | Segment | Include Segments | IncludeList |
    Then I verify bell icon should be greyed out

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify segment filter and user filter with split by and then try to click on bell icon it would be greyed out for System event
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>      |
      | OF       | Session Started |
      | OVER     | Days            |
      | SPLIT BY | Browser Name    |
    Then I apply filter for events
      | Event   | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_value_1 |
      | Segment | Include Segments      | IncludeList         |          |                   |
    Then I verify bell icon should be greyed out

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify segment filter and user filter without split by and then try to click on bell icon it would be greyed out for System event
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>      |
      | OF       | Session Started |
      | OVER     | Days            |
      | SPLIT BY | -               |
    Then I apply filter for events
      | Event   | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_value_1 |
      | Segment | Include Segments      | IncludeList         |          |                   |
    Then I verify bell icon should be greyed out

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify the user attribute filter is getting applied along with individual event block [system attribute] for a Single System event
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>      |
      | OF       | Session Started |
      | OVER     | <DayType>       |
      | SPLIT BY | -               |
    Then I apply filter for events
      | Event | User Attribute Filter | Age          | equal to |     42 |
      | Event | Session Started       | Browser Name | equal to | Chrome |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter  | Value           |
      | Session Started | <Count_event_1> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter  | Value           |
      | Session Started | <Count_event_1> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "greater than" expected value in table displayed
      | Parameter  | Value           |
      | <DayType>  |                 |
      | <showType> | <Count_event_1> |

    Examples: 
      | showType    | Count_event_1 | DayType | Total |
      | Occurrences |             1 | Days    |     1 |
      | Uniques     |             1 | Days    |     1 |

  Scenario Outline: To verify the user attribute filter is getting applied along with individual event block [event attribute] for Multiple System events
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>                          |
      | OF       | Session Started,Campaign Conversion |
      | OVER     | <DayType>                           |
      | SPLIT BY | -                                   |
    Then I apply filter for events
      | Event | User Attribute Filter | Age            | equal to |   42 |
      | Event | Campaign Conversion   | revenue_amount | equal to | 39.8 |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter           | Value           |
      | Session Started     | <Count_event_1> |
      | Campaign Conversion | <Count_event_2> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter           | Value           |
      | Session Started     | <Count_event_1> |
      | Campaign Conversion | <Count_event_2> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "greater than" expected value in table displayed
      | Parameter           | Value           |
      | <DayType>           |                 |
      | Total               | <Total>         |
      | Session Started     | <Count_event_1> |
      | Campaign Conversion | <Count_event_2> |

    Examples: 
      | showType    | Count_event_1 | Count_event_2 | DayType | Total |
      | Occurrences |             1 |             1 | Days    |     2 |
      | Uniques     |             1 |             1 | Days    |     1 |

  Scenario Outline: To verify the user attribute filter is getting applied along with individual event block [system attribute] for Multiple System events
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>                          |
      | OF       | Session Started,Campaign Conversion |
      | OVER     | <DayType>                           |
      | SPLIT BY | -                                   |
    Then I apply filter for events
      | Event | User Attribute Filter | Age          | equal to |      42 |
      | Event | Session Started       | Browser Name | equal to | Chrome  |
      | Event | Campaign Conversion   | OS Name      | equal to | Windows |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter           | Value           |
      | Session Started     | <Count_event_1> |
      | Campaign Conversion | <Count_event_2> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter           | Value           |
      | Session Started     | <Count_event_1> |
      | Campaign Conversion | <Count_event_2> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "greater than" expected value in table displayed
      | Parameter           | Value           |
      | <DayType>           |                 |
      | Total               | <Total>         |
      | Session Started     | <Count_event_1> |
      | Campaign Conversion | <Count_event_2> |

    Examples: 
      | showType    | Count_event_1 | Count_event_2 | DayType | Total |
      | Occurrences |             1 |             1 | Days    |     2 |
      | Uniques     |             1 |             1 | Days    |     1 |

  Scenario Outline: To verify the user attribute filter with the split by system attribute for System event
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>      |
      | OF       | Session Started |
      | OVER     | <DayType>       |
      | SPLIT BY | Browser Name    |
    Then I apply filter for events
      | Event | User Attribute Filter | Age | equal to | 42 |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter | Value           |
      | Chrome    | <Count_event_1> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter | Value           |
      | Chrome    | <Count_event_1> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "greater than" expected value in table displayed
      | Parameter | Value           |
      | <DayType> |                 |
      | Chrome    | <Count_event_1> |

    Examples: 
      | showType    | Count_event_1 | DayType | Total |
      | Occurrences |             1 | Days    |     1 |
      | Uniques     |             1 | Days    |     1 |

  Scenario Outline: To verify the user attribute filter with the split by user custom attribute for System event
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>      |
      | OF       | Session Started |
      | OVER     | <DayType>       |
      | SPLIT BY | Age             |
    Then I apply filter for events
      | Event | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_value_1 |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter | Value           |
      |        42 | <Count_event_1> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter | Value           |
      |        42 | <Count_event_1> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "greater than" expected value in table displayed
      | Parameter | Value           |
      | <DayType> |                 |
      |        42 | <Count_event_1> |

    Examples: 
      | showType    | Count_event_1 | DayType | Total |
      | Occurrences |             1 | Days    |     1 |
      | Uniques     |             1 | Days    |     1 |

  Scenario Outline: To verify the user attribute filter with the split by custom attribute for System event
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>          |
      | OF       | Campaign Conversion |
      | OVER     | <DayType>           |
      | SPLIT BY | revenue_amount      |
    Then I apply filter for events
      | Event | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_value_1 |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter | Value           |
      |      39.8 | <Count_event_1> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter | Value           |
      |      39.8 | <Count_event_1> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "greater than" expected value in table displayed
      | Parameter | Value           |
      | <DayType> |                 |
      |      39.8 | <Count_event_1> |

    Examples: 
      | showType    | Count_event_1 | DayType | Total |
      | Occurrences |             1 | Days    |     1 |
      | Uniques     |             1 | Days    |     1 |

  Scenario Outline: To verify the user attribute filter with over by system attribute for Single system Event
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>      |
      | OF       | Session Started |
      | OVER     | Browser Name    |
      | SPLIT BY | -               |
    Then I apply filter for events
      | Event | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_value_1 |
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter  | Value          |
      | <showType> | <Count_attr_1> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter  | Value          |
      | <showType> | <Count_attr_1> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "greater than" expected value in table displayed
      | Parameter    | Value          |
      | Browser Name | <DayType>      |
      | <showType>   | <Count_attr_1> |

    Examples: 
      | showType    | Count_attr_1 | DayType | Total |
      | Occurrences |            1 | Chrome  |     1 |
      | Uniques     |            1 | Chrome  |     1 |

  Scenario Outline: To verify the user attribute filter with over by user custom attribute for Single system Event
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>      |
      | OF       | Session Started |
      | OVER     | Age             |
      | SPLIT BY | -               |
    Then I apply filter for events
      | Event | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_value_1 |
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter  | Value          |
      | <showType> | <Count_attr_1> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter  | Value          |
      | <showType> | <Count_attr_1> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "greater than" expected value in table displayed
      | Parameter  | Value          |
      | Age        | <DayType>      |
      | <showType> | <Count_attr_1> |

    Examples: 
      | showType    | Count_attr_1 | DayType | Total |
      | Occurrences |            1 |      42 |     1 |
      | Uniques     |            1 |      42 |     1 |

  Scenario Outline: To verify the user attribute filter with over by custom attribute for Single system Event
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>          |
      | OF       | Campaign Conversion |
      | OVER     | revenue_amount      |
      | SPLIT BY | -                   |
    Then I apply filter for events
      | Event | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_value_1 |
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter  | Value          |
      | <showType> | <Count_attr_1> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter  | Value          |
      | <showType> | <Count_attr_1> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "greater than" expected value in table displayed
      | Parameter      | Value          |
      | Revenue amount | <DayType>      |
      | <showType>     | <Count_attr_1> |

    Examples: 
      | showType    | Count_attr_1 | DayType | Total |
      | Occurrences |            1 |    39.8 |     1 |
      | Uniques     |            1 |    39.8 |     1 |

  Scenario Outline: To verify the user attribute filter with over by system attribute for Multi system Events
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>                          |
      | OF       | Session Started,Campaign Conversion |
      | OVER     | Browser Name                        |
      | SPLIT BY | -                                   |
    Then I apply filter for events
      | Event | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_value_1 |
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter           | Value           |
      | Session Started     | <Count_event_1> |
      | Campaign Conversion | <Count_event_2> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter           | Value           |
      | Session Started     | <Count_event_1> |
      | Campaign Conversion | <Count_event_2> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "greater than" expected value in table displayed
      | Parameter           | Value           |
      | Browser Name        | <DayType>       |
      | Total               | <Total>         |
      | Session Started     | <Count_event_1> |
      | Campaign Conversion | <Count_event_2> |

    Examples: 
      | showType    | Count_event_1 | Count_event_2 | DayType | Total |
      | Occurrences |             1 |             1 | Chrome  |     2 |
      | Uniques     |             1 |             1 | Chrome  |     1 |

  Scenario Outline: To verify the user attribute filter with over by user custom attribute for Multi system Events
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>                          |
      | OF       | Session Started,Campaign Conversion |
      | OVER     | Age                                 |
      | SPLIT BY | -                                   |
    Then I apply filter for events
      | Event | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_value_1 |
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter           | Value           |
      | Session Started     | <Count_event_1> |
      | Campaign Conversion | <Count_event_2> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter           | Value           |
      | Session Started     | <Count_event_1> |
      | Campaign Conversion | <Count_event_2> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "greater than" expected value in table displayed
      | Parameter           | Value           |
      | Age                 | <DayType>       |
      | Total               | <Total>         |
      | Session Started     | <Count_event_1> |
      | Campaign Conversion | <Count_event_2> |

    Examples: 
      | showType    | Count_event_1 | Count_event_2 | DayType | Total |
      | Occurrences |             1 |             1 |      42 |     2 |
      | Uniques     |             1 |             1 |      42 |     1 |

  Scenario Outline: To verify the common attribute filter [system attribute] is getting  applied post clicking apply button for System events
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>                          |
      | OF       | Session Started,Campaign Conversion |
      | OVER     | <DayType>                           |
      | SPLIT BY | -                                   |
    Then I apply filter for events
      | Event | Common Filter | Browser Name | equal to | Chrome |
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter           | Value           |
      | Session Started     | <Count_event_1> |
      | Campaign Conversion | <Count_event_2> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter           | Value           |
      | Session Started     | <Count_event_1> |
      | Campaign Conversion | <Count_event_2> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "greater than" expected value in table displayed
      | Parameter           | Value           |
      | <DayType>           |                 |
      | Total               | <Total>         |
      | Session Started     | <Count_event_1> |
      | Campaign Conversion | <Count_event_2> |

    Examples: 
      | showType    | Count_event_1 | Count_event_2 | DayType | Total |
      | Occurrences |             1 |             1 | Days    |     2 |
      | Uniques     |             1 |             1 | Days    |     1 |

  Scenario Outline: To verify the user attribute filter with segment filter, individaul filter [event attribute/system attribute] and common filter [system attribute] for System events
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>                          |
      | OF       | Session Started,Campaign Conversion |
      | OVER     | <DayType>                           |
      | SPLIT BY | -                                   |
    Then I apply filter for events
      | Event   | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_value_1 |
      | Event   | Common Filter         | Country             | equal to | United States     |
      | Event   | Session Started       | Browser Name        | equal to | Chrome            |
      | Event   | Campaign Conversion   | revenue_amount      | equal to |              39.8 |
      | Segment | Include Segments      | WebSDKUserList      |          |                   |
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter           | Value           |
      | Session Started     | <Count_event_1> |
      | Campaign Conversion | <Count_event_2> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter           | Value           |
      | Session Started     | <Count_event_1> |
      | Campaign Conversion | <Count_event_2> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "greater than" expected value in table displayed
      | Parameter           | Value           |
      | <DayType>           |                 |
      | Total               | <Total>         |
      | Session Started     | <Count_event_1> |
      | Campaign Conversion | <Count_event_2> |

    Examples: 
      | showType    | Count_event_1 | Count_event_2 | DayType | Total |
      | Occurrences |             1 |             1 | Days    |     2 |
      | Uniques     |             1 |             1 | Days    |     1 |

  Scenario Outline: To verify the user attribute filter along with segment filter for Single System Event
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>      |
      | OF       | Session Started |
      | OVER     | <DayType>       |
      | SPLIT BY | -               |
    Then I apply filter for events
      | Event   | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_value_1 |
      | Segment | Include Segments      | WebSDKUserList      |          |                   |
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter  | Value           |
      | Session Started | <Count_event_1> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter  | Value           |
      | Session Started | <Count_event_1> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "greater than" expected value in table displayed
      | Parameter  | Value           |
      | <DayType>  |                 |
      | <showType> | <Count_event_1> |

    Examples: 
      | showType    | Count_event_1 | DayType | Total |
      | Occurrences |             1 | Days    |     1 |
      | Uniques     |             1 | Days    |     1 |

  Scenario Outline: To verify the user attribute filter along with segment filter for Multi System Events
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>                          |
      | OF       | Session Started,Campaign Conversion |
      | OVER     | <DayType>                           |
      | SPLIT BY | -                                   |
    Then I apply filter for events
      | Event   | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_value_1 |
      | Segment | Include Segments      | WebSDKUserList      |          |                   |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter           | Value           |
      | Session Started     | <Count_event_1> |
      | Campaign Conversion | <Count_event_2> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter           | Value           |
      | Session Started     | <Count_event_1> |
      | Campaign Conversion | <Count_event_2> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "greater than" expected value in table displayed
      | Parameter           | Value           |
      | <DayType>           |                 |
      | Total               | <Total>         |
      | Session Started     | <Count_event_1> |
      | Campaign Conversion | <Count_event_2> |

    Examples: 
      | showType    | Count_event_1 | Count_event_2 | DayType | Total |
      | Occurrences |             1 |             1 | Days    |     2 |
      | Uniques     |             1 |             1 | Days    |     1 |

  Scenario Outline: To verify the user attribute is getting pin in the new dashboard for Single custom event
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>  |
      | OF       | multi_event |
      | OVER     | Days        |
      | SPLIT BY | -           |
    Then I apply filter for events
      | Event | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_testing_1_test_value |
    Then I pin in the "new" dashboard with card name "New custom card" and dashboard name "Single custom Event Dashboard"
    Given I navigate to "Dashboards" Page
    Then I open "Single custom Event Dashboard" dashboard
    Then I click on "New custom card" and verify it re-directs to events page
    Given I navigate to "Dashboards" Page
    Then delete the module with Name "Single custom Event Dashboard"

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify the user attribute is getting pin in the new dashboard for Multiple custom events
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>                |
      | OF       | multi_event,multi_event_A |
      | OVER     | Days                      |
      | SPLIT BY | -                         |
    Then I apply filter for events
      | Event | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_testing_1_test_value |
    Then I pin in the "new" dashboard with card name "New multiple card" and dashboard name "Multi custom Events Dashboard"
    Given I navigate to "Dashboards" Page
    Then I open "Multi custom Events Dashboard" dashboard
    Then I click on "New multiple card" and verify it re-directs to events page
    Given I navigate to "Dashboards" Page
    Then delete the module with Name "Multi custom Events Dashboard"

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify the user attribute is getting pin in the existing dashboard for Single custom event
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>  |
      | OF       | multi_event |
      | OVER     | Days        |
      | SPLIT BY | -           |
    Then I apply filter for events
      | Event | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_testing_1_test_value |
    Then I pin in the "existing" dashboard with card name "Existing single custom card" and dashboard name "Event Data Dashboard"
    Given I navigate to "Dashboards" Page
    Then I open "Event Data Dashboard" dashboard
    Then I click on "Existing single custom card" and verify it re-directs to events page
    Given I navigate to "Dashboards" Page
    Then I open "Event Data Dashboard" dashboard
    Then I "Delete" the card "Existing single custom card"

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify the user attribute is getting pin in the existing dashboard for Multiple custom events
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>                |
      | OF       | multi_event,multi_event_A |
      | OVER     | Days                      |
      | SPLIT BY | -                         |
    Then I apply filter for events
      | Event | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_testing_1_test_value |
    Then I pin in the "existing" dashboard with card name "Multi custom events card" and dashboard name "Event Data Dashboard"
    Given I navigate to "Dashboards" Page
    Then I open "Event Data Dashboard" dashboard
    Then I click on "Multi custom events card" and verify it re-directs to events page
    Given I navigate to "Dashboards" Page
    Then I open "Event Data Dashboard" dashboard
    Then I "Delete" the card "Multi custom events card"

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify the segment filter is getting pin in the new dashboard for Single custom event
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>  |
      | OF       | multi_event |
      | OVER     | Days        |
      | SPLIT BY | -           |
    Then I apply filter for events
      | Segment | Include Segments | UsersList |
    Then I pin in the "new" dashboard with card name "Segment card" and dashboard name "Single Segment Test Dashboard"
    Given I navigate to "Dashboards" Page
    Then I open "Single Segment Test Dashboard" dashboard
    Then I click on "Segment card" and verify it re-directs to events page
    Given I navigate to "Dashboards" Page
    Then delete the module with Name "Single Segment Test Dashboard"

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify the segment filter is getting pin in the new dashboard for Multiple custom events
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>                |
      | OF       | multi_event,multi_event_A |
      | OVER     | Days                      |
      | SPLIT BY | -                         |
    Then I apply filter for events
      | Segment | Include Segments | UsersList |
    Then I pin in the "new" dashboard with card name "Segment card" and dashboard name "Multi Segment Test Dashboard"
    Given I navigate to "Dashboards" Page
    Then I open "Multi Segment Test Dashboard" dashboard
    Then I click on "Segment card" and verify it re-directs to events page
    Given I navigate to "Dashboards" Page
    Then delete the module with Name "Multi Segment Test Dashboard"

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify the segment filter is getting pin in the existing dashboard for Single custom event
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>  |
      | OF       | multi_event |
      | OVER     | Days        |
      | SPLIT BY | -           |
    Then I apply filter for events
      | Segment | Include Segments | UsersList |
    Then I pin in the "existing" dashboard with card name "Single Segment card" and dashboard name "Event Data Dashboard"
    Given I navigate to "Dashboards" Page
    Then I open "Event Data Dashboard" dashboard
    Then I click on "Single Segment card" and verify it re-directs to events page
    Given I navigate to "Dashboards" Page
    Then I open "Event Data Dashboard" dashboard
    Then I "Delete" the card "Single Segment card"

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify the segment filter is getting pin in the existing dashboard for Multiple custom events
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>                |
      | OF       | multi_event,multi_event_A |
      | OVER     | Days                      |
      | SPLIT BY | -                         |
    Then I apply filter for events
      | Segment | Include Segments | UsersList |
    Then I pin in the "existing" dashboard with card name "Multiple Segment card" and dashboard name "Event Data Dashboard"
    Given I navigate to "Dashboards" Page
    Then I open "Event Data Dashboard" dashboard
    Then I click on "Multiple Segment card" and verify it re-directs to events page
    Given I navigate to "Dashboards" Page
    Then I open "Event Data Dashboard" dashboard
    Then I "Delete" the card "Multiple Segment card"

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify the user attribute is getting pin in the new dashboard for Single system event
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>      |
      | OF       | Session Started |
      | OVER     | Days            |
      | SPLIT BY | -               |
    Then I apply filter for events
      | Event | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_value_1 |
    Then I pin in the "new" dashboard with card name "New card" and dashboard name "Single system Event Dashboard"
    Given I navigate to "Dashboards" Page
    Then I open "Single system Event Dashboard" dashboard
    Then I click on "New card" and verify it re-directs to events page
    Given I navigate to "Dashboards" Page
    Then delete the module with Name "Single system Event Dashboard"

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify the user attribute is getting pin in the new dashboard for Multiple system events
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>                          |
      | OF       | Session Started,Campaign Conversion |
      | OVER     | Days                                |
      | SPLIT BY | -                                   |
    Then I apply filter for events
      | Event | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_value_1 |
    Then I pin in the "new" dashboard with card name "New card" and dashboard name "Multi system Events Dashboard"
    Given I navigate to "Dashboards" Page
    Then I open "Multi system Events Dashboard" dashboard
    Then I click on "New card" and verify it re-directs to events page
    Given I navigate to "Dashboards" Page
    Then delete the module with Name "Multi system Events Dashboard"

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify the user attribute is getting pin in the existing dashboard for Single system event
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>      |
      | OF       | Session Started |
      | OVER     | Days            |
      | SPLIT BY | -               |
    Then I apply filter for events
      | Event | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_value_1 |
    Then I pin in the "existing" dashboard with card name "Single system card" and dashboard name "Event Data Dashboard"
    Given I navigate to "Dashboards" Page
    Then I open "Event Data Dashboard" dashboard
    Then I click on "Single system card" and verify it re-directs to events page
    Given I navigate to "Dashboards" Page
    Then I open "Event Data Dashboard" dashboard
    Then I "Delete" the card "Single system card"

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify the user attribute is getting pin in the existing dashboard for Multiple system events
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>                          |
      | OF       | Session Started,Campaign Conversion |
      | OVER     | Days                                |
      | SPLIT BY | -                                   |
    Then I apply filter for events
      | Event | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_value_1 |
    Then I pin in the "existing" dashboard with card name "Multi system card" and dashboard name "Event Data Dashboard"
    Given I navigate to "Dashboards" Page
    Then I open "Event Data Dashboard" dashboard
    Then I click on "Multi system card" and verify it re-directs to events page
    Given I navigate to "Dashboards" Page
    Then I open "Event Data Dashboard" dashboard
    Then I "Delete" the card "Multi system card"

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify the segment filter is getting pin in the new dashboard for Single system event
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>      |
      | OF       | Session Started |
      | OVER     | Days            |
      | SPLIT BY | -               |
    Then I apply filter for events
      | Segment | Include Segments | WebSDKUserList |
    Then I pin in the "new" dashboard with card name "Segment card" and dashboard name "Single system Segment Test Dashboard"
    Given I navigate to "Dashboards" Page
    Then I open "Single system Segment Test Dashboard" dashboard
    Then I click on "Segment card" and verify it re-directs to events page
    Given I navigate to "Dashboards" Page
    Then delete the module with Name "Single system Segment Test Dashboard"

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify the segment filter is getting pin in the new dashboard for Multiple system events
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>                          |
      | OF       | Session Started,Campaign Conversion |
      | OVER     | Days                                |
      | SPLIT BY | -                                   |
    Then I apply filter for events
      | Segment | Include Segments | WebSDKUserList |
    Then I pin in the "new" dashboard with card name "Segment card" and dashboard name "Multi system Segment Test Dashboard"
    Given I navigate to "Dashboards" Page
    Then I open "Multi system Segment Test Dashboard" dashboard
    Then I click on "Segment card" and verify it re-directs to events page
    Given I navigate to "Dashboards" Page
    Then delete the module with Name "Multi system Segment Test Dashboard"

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify the segment filter is getting pin in the existing dashboard for Single system event
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>      |
      | OF       | Session Started |
      | OVER     | Days            |
      | SPLIT BY | -               |
    Then I apply filter for events
      | Segment | Include Segments | WebSDKUserList |
    Then I pin in the "existing" dashboard with card name "Single system Segment card" and dashboard name "Event Data Dashboard"
    Given I navigate to "Dashboards" Page
    Then I open "Event Data Dashboard" dashboard
    Then I click on "Single system Segment card" and verify it re-directs to events page
    Given I navigate to "Dashboards" Page
    Then I open "Event Data Dashboard" dashboard
    Then I "Delete" the card "Single system Segment card"

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify the segment filter is getting pin in the existing dashboard for Multiple system events
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>                          |
      | OF       | Session Started,Campaign Conversion |
      | OVER     | Days                                |
      | SPLIT BY | -                                   |
    Then I apply filter for events
      | Segment | Include Segments | WebSDKUserList |
    Then I pin in the "existing" dashboard with card name "Multiple system Segment card" and dashboard name "Event Data Dashboard"
    Given I navigate to "Dashboards" Page
    Then I open "Event Data Dashboard" dashboard
    Then I click on "Multiple system Segment card" and verify it re-directs to events page
    Given I navigate to "Dashboards" Page
    Then I open "Event Data Dashboard" dashboard
    Then I "Delete" the card "Multiple system Segment card"

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |