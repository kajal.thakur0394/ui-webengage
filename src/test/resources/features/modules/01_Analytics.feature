@Analytics @Regression
Feature: Analytics Feature

  @Smoke
  Scenario: To verify that Paths shows the result for the selected values in Paths fields once clicked on "Show Path" Starting with
    Given I navigate to "Paths" page via "Analytics"
    Then fill the Path filter using below values
      | SHOW PATHS               | Starting with>Starting with | ->TriggerEmailEvent |
      | LOOKAHEAD WINDOW         |                          99 | hours>minutes       |
      | COLLAPSE REPEATED EVENTS | false                       |                     |
    Then validate whether event "TriggerEmailEvent" gets reflected in Step "1"

  @Smoke
  Scenario: To verify that Paths shows the result for the selected values in Paths fields once clicked on "Show Path" Ending with
    Given I navigate to "Funnels" page via "Analytics"
    Given I navigate to "Paths" page via "Analytics"
    Then fill the Path filter using below values
      | SHOW PATHS               | Starting with>Ending with | ->TriggerSMSEvent |
      | LOOKAHEAD WINDOW         |                        72 | hours>hours       |
      | COLLAPSE REPEATED EVENTS | false                     |                   |
    Then validate whether event "TriggerSMSEvent" gets reflected in Step "1"

  Scenario: To verify unicode characterset contents are showing properly when new funnel is created
    Given I navigate to "Funnels" page via "Analytics"
    And create new Funnel with name as "UTF: { } ~ ¢ £ ¤ ڝ ڞ ڟ ڠ ڡ अ आ इ ई ਆ ਇ ਈ ஆ இ"
    Then fill the Steps detail using below values
      | Step 1 | TriggerEmailEvent |
      | Step 2 | TriggerSMSEvent   |
    Given I navigate to "Funnels" page via "Analytics"
    Then verify "UTF: { } ~ ¢ £ ¤ ڝ ڞ ڟ ڠ ڡ अ आ इ ई ਆ ਇ ਈ ஆ இ" name which has unicode characters on Overview page for "Funnels"
    Given I navigate to "Funnels" page via "Analytics"
    Then delete the funnel with name as "UTF: { } ~ ¢ £ ¤ ڝ ڞ ڟ ڠ ڡ अ आ इ ई ਆ ਇ ਈ ஆ இ"