@LiveSegments @Regression
Feature: Live Segments Feature

  @Smoke
  Scenario: Duplicate a newly created or existing segment
    Given I navigate to "Live Segments" page via "Segments"
    And perform "Duplicate" action for "Duplicate this Segment" segment on the listing page
    Then verify the popup for Segment Duplication and duplicate it in same project itself
    Then verify that the segment "Copy of Duplicate this Segment" gets Listed on the page
    Then I create a Live segment with name as "Runtime Segment Duplication"
    And Pass following details in user card
      | Visitor Type | All Users  |
      | User IDs     | contains>4 |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    And perform "Duplicate" action for "Runtime Segment Duplication" segment on the listing page
    Then verify the popup for Segment Duplication and duplicate it in same project itself
    Then verify that the segment "Copy of Runtime Segment Duplication" gets Listed on the page
    Then I delete the following mentioned "segments"
      | Copy of Duplicate this Segment      |
      | Copy of Runtime Segment Duplication |

  @Smoke
  Scenario: Edit an existing Segment by changing User Condition and save it.
    Given I navigate to "Live Segments" page via "Segments"
    Given perform "Edit" action for "Runtime Segment Duplication" segment on the listing page
    Then I Edit the Segment
    And Pass following details in user card
      | Visitor Type | All Users                |
      | User IDs     | does not contain>AutoID1 |
    And Save the Segment
    Then check whether the segment has been "Updated" and gets listed on the page.

  @Smoke
  Scenario: Edit an existing Segment by changing Behavioural Condition and save it.
    Given I navigate to "Live Segments" page via "Segments"
    Then I Open the Segment "Runtime Segment Duplication"
    Then I click on the "Edit" tool-tip
    Then I Edit the Segment
    And Pass following details in Behavioural card
      |  | Users who DID these events : | System>App Installed |
    And Save the Segment
    Then check whether the segment has been "Updated" and gets listed on the page.
    Then I delete segment with Name "Runtime Segment Duplication"

  Scenario: Check the count of users by refreshing Segment Details and on listing page
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "Count the Users"
    And Pass following details in user card
      | Visitor Type | All Users        |
      | User IDs     | equal to>AutoID1 |
    Then I click on refresh button and check "Total Users" count as 1
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "Count the Users"
    Then I verify "Total Users" count as 1
    And I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "Count the Users"

  Scenario: Create a segment by using Technology Condition > Android
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "Technology Condition > Android"
    And Pass following details for "Android" in Technology card
      | Total Time Spent (in seconds) | greater than>100 |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "Technology Condition > Android"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "Technology Condition > Android"

  Scenario: Create a segment by using both User + Behavioural Condition
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "User + Behavioural Condition"
    And Pass following details in user card
      | Visitor Type | All Users             |
      | Reachability | Reachable on-WhatsApp |
    And Pass following details in Behavioural card
      |  | Users who DID NOT do these events : | System>App Installed |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "User + Behavioural Condition"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "User + Behavioural Condition"

  Scenario: Create a segment by using User Condition > Reachability
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "User Condition > Reachability"
    And Pass following details in user card
      | Visitor Type | All Users             |
      | Reachability | Reachable on-WhatsApp |
      | Reachability | Reachable on-Web Push |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "User Condition > Reachability"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "User Condition > Reachability"

  Scenario: Create a segment by using User Condition > User IDs
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "User Condition > User IDs"
    And Pass following details in user card
      | Visitor Type | All Users     |
      | User IDs     | starts with>W |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "User Condition > User IDs"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "User Condition > User IDs"

  Scenario: Create a segment by using User Condition > Created On
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "User Condition > Created On"
    And Pass following details in user card
      | Created On | before-date |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "User Condition > Created On"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "User Condition > Created On"

  Scenario: Create a segment by using User Condition and adding more than 2 User Attribute condition
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "2 User Attribute condition"
    And Pass following details in user card
      | Geo Filtering | India        |
      | User IDs      | is not empty |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "2 User Attribute condition"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "2 User Attribute condition"

  Scenario: Create a segment by using User Condition > User Attribute > Custom Attribute
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "User Attribute > Custom Attribute"
    And Pass following details in user card
      | User Attribute>Select an Option | AutomationUser |
      | User Attribute>Select...        | equal to       |
      | User Attribute>Select...        | True           |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "User Attribute > Custom Attribute"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "User Attribute > Custom Attribute"

  Scenario: Create a segment by using User Condition > User Attribute > System Attribute
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "User Attribute > System Attribute"
    And Pass following details in user card
      | User Attribute>Select an Option | Country  |
      | User Attribute>Select...        | equal to |
      | User Attribute>Enter a value    | India    |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "User Attribute > System Attribute"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "User Attribute > System Attribute"

  Scenario: Create a segment by using Behavioural Condition and adding more than 1 condition of Users who did not do these Events.
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "more than 1 condition of Users who did not do these Events"
    And Pass following details in Behavioural card
      |    | Users who DID NOT do these events : | System>App Installed |
      | Or | Users who DID NOT do these events : | System>User Login    |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "more than 1 condition of Users who did not do these Events"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "more than 1 condition of Users who did not do these Events"

  Scenario: Create a segment by using Behavioural Condition and adding more than 1 condition of Users who did these Events.
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "more than 1 condition of Users who did these Events"
    And Pass following details in Behavioural card
      |     | Users who DID these events : | System>User Login    |
      | And | Users who DID these events : | System>App Installed |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "more than 1 condition of Users who did these Events"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "more than 1 condition of Users who did these Events"

  Scenario: Create a segment by using Behavioural Condition > Users who did not do these Events and add an Event Time Filter.
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "Users who did not do these Events and add an Event Time Filter."
    And Pass following details in Behavioural card
      |  | Users who DID NOT do these events : | System>User Login |
    And I apply the filter for "Users who DID NOT do these events :" as
      | Device | equal to | Huawei |  |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "Users who did not do these Events and add an Event Time Filter."
    And I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "Users who did not do these Events and add an Event Time Filter."

  Scenario: Create a segment by using Behavioural Condition > Users who did these Events and add an Event Time Filter.
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "Users who did these Events and add an Event Time Filter"
    And Pass following details in Behavioural card
      |  | Users who DID these events : | System>App Installed |
    And I apply the filter for "Users who DID these events :" as
      | City | not equal to | Mumbai |  |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "Users who did these Events and add an Event Time Filter"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "Users who did these Events and add an Event Time Filter"

  Scenario: Verify that user is able to create segments from the bulk imported users or events.
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "segments from the bulk imported users"
    And Pass following details in user card
      | Visitor Type | All Users        |
      | User IDs     | equal to>csvUser |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "segments from the bulk imported users"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "segments from the bulk imported users"

  Scenario: Verify existing tags is attached to Live Segments
    Given I navigate to "Live Segments" page via "Segments"
    And I search for "LiveSegments Tag" segment
    Then I verify "livesegmentstag" tag is attached to the "LiveSegments Tag" segment

  Scenario: Verify new tag is created from Menu and gets attached to newly created Live Segment
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "Tags:User Condition > User IDs"
    And Pass following details for "Android" in Technology card
      | Total Time Spent (in seconds) | greater than>100 |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    And I search for "Tags:User Condition > User IDs" segment
    And create new tag with name as "segmenttag" for segment "Tags:User Condition > User IDs"
    And I search for "Tags:User Condition > User IDs" segment
    Then I verify "segmenttag1" tag is attached to the "Tags:User Condition > User IDs" segment
    Then I navigate to "Live Segments" page via "Segments"
    And I search for "Tags:User Condition > User IDs" segment
    Then delete the tag with name as "segmenttag" from segment "Tags:User Condition > User IDs"
    Then I delete segment with Name "Tags:User Condition > User IDs"

  Scenario: Verify new tag is created from Show Details and gets attached to newly created Live Segment
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "Tags:User Condition > User IDs"
    And Pass following details in user card
      | Visitor Type | All Users   |
      | User IDs     | ends with>P |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    And I search for "Tags:User Condition > User IDs" segment
    And create new tag from show details with name as "segmenttag" for segment "Tags:User Condition > User IDs"
    Then I navigate to "Live Segments" page via "Segments"
    And I search for "Tags:User Condition > User IDs" segment
    Then I verify "segmenttag2" tag is attached to the "Tags:User Condition > User IDs" segment
    Then I navigate to "Live Segments" page via "Segments"
    And I search for "Tags:User Condition > User IDs" segment
    Then delete the tag with name as "segmenttag" from segment "Tags:User Condition > User IDs"
    Then I delete segment with Name "Tags:User Condition > User IDs"

  @Smoke
  Scenario: To verify List of users download for a live segment
    Given I navigate to "Live Segments" page via "Segments"
    And I Open the Segment "Duplicate this Segment"
    And I click on the "Download List of Users" tool-tip
    Then I download the list and verify it

  Scenario: To verify creation of segment with unicode characterset for Behavioural Condition > Users who did these Events
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "Unicode Behavioral:$ ! # % &༂ ༃ ༄ ༅ ခ ဂ अ आ इ ई ਆ ਇ"
    And Pass following details in Behavioural card
      |     | Users who DID these events : | System>User Login    |
      | And | Users who DID these events : | System>App Installed |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "Unicode Behavioral:$ ! # % &༂ ༃ ༄ ༅ ခ ဂ अ आ इ ई ਆ ਇ"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "Unicode Behavioral:$ ! # % &༂ ༃ ༄ ༅ ခ ဂ अ आ इ ई ਆ ਇ"

  Scenario: To verify unicode characterset contents are updated properly on editing segments for Technological condition
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "Unicode Technological:ℙ ℚ ℛ ℜ ℝ ℞ ℟ℾ ℿ ⅀ ⅁ ⅂ ⅃ ⅄ ⅅ ⅆ ⅇ ⅈ ⅉ ℐ ℑ ℒ ℓ ℔ ℕ"
    And Pass following details for "Android" in Technology card
      | Total Time Spent (in seconds) | greater than>100 |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "Unicode Technological:ℙ ℚ ℛ ℜ ℝ ℞ ℟ℾ ℿ ⅀ ⅁ ⅂ ⅃ ⅄ ⅅ ⅆ ⅇ ⅈ ⅉ ℐ ℑ ℒ ℓ ℔ ℕ"
    Then I click on the "Edit" tool-tip
    Then I Edit the Segment
    And Pass following details for "iOS" in Technology card
      | Total Time Spent (in seconds) | less than>100 |
    And Save the Segment
    Then check whether the segment has been "Updated" and gets listed on the page.
    Then I Open the Segment "Unicode Technological:ℙ ℚ ℛ ℜ ℝ ℞ ℟ℾ ℿ ⅀ ⅁ ⅂ ⅃ ⅄ ⅅ ⅆ ⅇ ⅈ ⅉ ℐ ℑ ℒ ℓ ℔ ℕ"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "Unicode Technological:ℙ ℚ ℛ ℜ ℝ ℞ ℟ℾ ℿ ⅀ ⅁ ⅂ ⅃ ⅄ ⅅ ⅆ ⅇ ⅈ ⅉ ℐ ℑ ℒ ℓ ℔ ℕ"

  Scenario: Create a  segment with first acquisition source and verify all the  dropdown values
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "First Acquisition Source test"
    And Pass following details in user card
      |  |  |
    Then I verify "First Acquisition Source" dropdown with all the values
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I delete segment with Name "First Acquisition Source test"

  @End-to-End @Distributed
  Scenario: To verify the segment creation with User who did this event AND User who did not do this event
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "Users who did these Events AND User who did not do this event"
    And Pass following details in Behavioural card
      |  | Users who DID these events :        | Application>E1 |
      |  | Users who DID NOT do these events : | Application>E2 |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "Users who did these Events AND User who did not do this event"
    And I navigate to "Live Segments" page via "Segments"

  #@End-to-End @Distributed
  #Scenario: To verify the segment creation with visitor type and created on is On selected date
  #Given I navigate to "Live Segments" page via "Segments"
  #Then I create a Live segment with name as "User with visitor type and created on selected date"
  #And Pass following details in user card
  #| Created On  | on     |
  #| Set Date as | 19 Jan 2021 |
  #And Save the Segment
  #Then check whether the segment has been "Created" and gets listed on the page.
  #Then I Open the Segment "User with visitor type and created on selected date"
  #And I navigate to "Live Segments" page via "Segments"
  @End-to-End @Distributed
  Scenario: To verify the segment creation with User who did this event with Custom Event applying custom attribute filter
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "custom event creation for event E1 and price filter"
    And Pass following details in Behavioural card
      |  | Users who DID these events : | Application>E1 |
    And I apply the filter for "Users who DID these events :" as
      | Price | equal to | 1000 |  |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "custom event creation for event E1 and price filter"
    And I navigate to "Live Segments" page via "Segments"

  @End-to-End @Distributed
  Scenario: To verify the segment creation with User who did this event with System Event applying custom attribute filter
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "system event creation for event email open and email id filter"
    And Pass following details in Behavioural card
      |  | Users who DID these events : | System>Email Open |
    And I apply the filter for "Users who DID these events :" as
      | email_id | one of | aditya.dhanve@webengage.com |  |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "system event creation for event email open and email id filter"
    And I navigate to "Live Segments" page via "Segments"

  @End-to-End @Distributed
  Scenario: To verify the segment creation with User System Attribute First Name does not start with selected dropdown value
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "User Attribute > first name"
    And Pass following details in user card
      | Visitor Type                    | All Users           |
      | User Attribute>Select an Option | First Name          |
      | User Attribute>Select...        | does not start with |
      | User Attribute>Enter a value    | Test                |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "User Attribute > first name"
    And I navigate to "Live Segments" page via "Segments"

  @End-to-End @Distributed
  Scenario: To verify the segment creation with User who did this event with Custom Event applying system attribute filter
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "custom event creation for event E1 and browser name filter"
    And Pass following details in Behavioural card
      |  | Users who DID these events : | Application>E1 |
    And I apply the filter for "Users who DID these events :" as
      | Browser Name | contains | Chrome |  |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "custom event creation for event E1 and browser name filter"
    And I navigate to "Live Segments" page via "Segments"

  @End-to-End @Distributed
  Scenario: To verify the segment creation with User Id equal to entered value
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "All user with User Ids equals to LS1"
    And Pass following details in user card
      | Visitor Type | All Users    |
      | User IDs     | equal to>LS1 |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "All user with User Ids equals to LS1"
    And I navigate to "Live Segments" page via "Segments"

  @End-to-End @Distributed
  Scenario: To verify the segment creation with User Custom Attribute Age equal to selected value 29
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "User Attribute age equals to 29"
    And Pass following details in user card
      | Visitor Type                    | All Users |
      | User Attribute>Select an Option | Age       |
      | User Attribute>Select...        | equal to  |
      | User Attribute>Enter a value    |        29 |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "User Attribute age equals to 29"
    And I navigate to "Live Segments" page via "Segments"

  @End-to-End @Distributed
  Scenario: To verify the segment creation with User Id starts with entered value
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "All user with User starts with LS"
    And Pass following details in user card
      | Visitor Type | All Users      |
      | User IDs     | starts with>LS |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "All user with User starts with LS"
    And I navigate to "Live Segments" page via "Segments"

  @End-to-End @Distributed @skip @issue:WP-17536
  Scenario: To verify the segment creation with User who did this event OR User who did not do these event.
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "Application event creation for Users who DID these events E1 or Users who did not E2 "
    And Pass following details in user card
      | Visitor Type | All Users    |
      | User IDs     | contains>LS1 |
    And Pass following details in Behavioural card
      |    | Users who DID these events :        | Application>E1 |
      | Or | Users who DID NOT do these events : | Application>E2 |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "Application event creation for Users who DID these events E1 or Users who did not E2 "
    And I navigate to "Live Segments" page via "Segments"

  @End-to-End @Distributed
  Scenario: To verify the segment creation from Journey settings with user id equal to entered user
    Given I navigate to "Journeys" Page
    Then I create a new Journey/Relay with name as "Journey settings"
    Then I click on journey settings by selecting "Create new segment" with name as "journey segment creation"
    Then fill the page via "journeySettings" template from "JourneyTemplates" folder using "Basic" section
    Then I click button "SAVE"
    Then I navigate back to "Journeys" Page
    And I navigate to "Live Segments" page via "Segments"
    Then verify that the segment "journey segment creation" gets Listed on the page
    Then I Open the Segment "journey segment creation"

  @End-to-End @Distributed
  Scenario: To verify the segment creation from Journey Is in Segment block with user id equal to entered user
    Given I navigate to "Journeys" Page
    Then I create a new Journey/Relay with name as "Journey is in segment"
    Then I use the Journey/Relay Template "journeySegment"
    Then I click button "SAVE"
    Then I navigate back to "Journeys" Page
    And I navigate to "Live Segments" page via "Segments"
    Then verify that the segment "Create segment through IsInSegment condition" gets Listed on the page
    Then I Open the Segment "Create segment through IsInSegment condition"

  @End-to-End @Distributed
  Scenario: Create a segment by using Users condition > Last seen is after/before and add date.
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "segment for last seen usecase"
    And Pass following details in user card
      | Visitor Type | All Users  |
      | Last Seen    | after-date |
      | Set Date as  | 1 Jan 2024 |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "segment for last seen usecase"

  @End-to-End @Distributed
  Scenario: Create a segment by using Behavioural condition > Users who did these Events and add two events > Users who did not do these Events and add two events
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "segment with two events of both cases"
    And Pass following details in Behavioural card
      |     | Users who DID these events :        | Application>E1                |
      | And | Users who DID these events :        | Application>E3                |
      |     | Users who DID NOT do these events : | Application>TriggerSMSEvent   |
      | And | Users who DID NOT do these events : | Application>TriggerEmailEvent |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "segment with two events of both cases"

  @End-to-End @Distributed
  Scenario: To verify the segment creation with Android App ID equal to entered value
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "Technology Condition >Android"
    And Pass following details for "Android" in Technology card
      | App ID | equal to>com.webengage.production |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "Technology Condition >Android"

  @End-to-End @Distributed
  Scenario: To verify the segment creation with visitor type and created on is after selected date.
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "User Condition > Created On>after selected date"
    And Pass following details in user card
      | Visitor Type | All Users  |
      | Created On   | after-date |
      | Set Date as  | 1 Jan 2022 |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "User Condition > Created On>after selected date"

  @End-to-End @Distributed
  Scenario: To verify the segment creation with Geo Filtering any selected country, region and city
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "Geo Filtering>any selected country, region and city"
    And Pass following details in user card
      | Visitor Type                | All Users   |
      | Set Geo filtering type      | Any         |
      | Geo Filtering               | India       |
      | Geo Filtering>All Regions   | Maharashtra |
      | Geo Filtering>Select Cities | Mumbai      |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "Geo Filtering>any selected country, region and city"

  @End-to-End @Distributed
  Scenario: To verify the segment creation with Geo Filtering None selected country, region and city
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "Geo Filtering>None selected country, region and city"
    And Pass following details in user card
      | Visitor Type                | All Users     |
      | Set Geo filtering type      | None          |
      | Geo Filtering               | United States |
      | Geo Filtering>All Regions   | New Jersey    |
      | Geo Filtering>Select Cities | Belford       |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "Geo Filtering>None selected country, region and city"

  @End-to-End @Distributed
  Scenario: To verify the segment creation with User who did this event at least once
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "User Condition>who did these Events>at least once"
    And Pass following details in Behavioural card
      |  | Users who DID these events : | Application>E1 |
    And I apply the filter for "Users who DID these events :" as
      | Event Time | after | date | 1 Jan 2024 |
    And I did these events for following event type
      | at least once |  |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "User Condition>who did these Events>at least once"

  @End-to-End @Distributed
  Scenario: To verify the segment creation with User who did this event Only once
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "User Condition>who did these Events>only once"
    And Pass following details in Behavioural card
      |  | Users who DID these events : | Application>FireEvent_OnlyOnce |
    And I apply the filter for "Users who DID these events :" as
      | Event Time | after | date | 1 Jan 2024 |
    And I did these events for following event type
      | only once |  |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "User Condition>who did these Events>only once"

  @End-to-End @Distributed
  Scenario: To verify the segment creation with User who did this event with no of occurence less than or equal to
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "User Condition>who did these Events>no. of occurrence"
    And Pass following details in Behavioural card
      |  | Users who DID these events : | Application>FireEvent_OnlyOnce |
    And I apply the filter for "Users who DID these events :" as
      | Event Time | after | date | 1 Jan 2024 |
    And I did these events for following event type
      | no. of occurences | is less than or equal to | 1 |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "User Condition>who did these Events>no. of occurrence"

  @End-to-End @Distributed
  Scenario: Create a segment by using Behavioural condition > Users who did these Events and add most recent event time
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "User Condition>who did these Events>most recent event time"
    And Pass following details in Behavioural card
      |  | Users who DID these events : | Application>E1 |
    And I apply the filter for "Users who DID these events :" as
      | Event Time | after | date | 1 Jan 2024 |
    And I did these events for following event type
      | most recent ⚙ Event Time | before | date |  |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "User Condition>who did these Events>most recent event time"

  @End-to-End @Distributed
  Scenario: Create a segment by using Behavioural condition > Users who did these Events and add least recent event time
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "User Condition>who did these Events>least recent event time"
    And Pass following details in Behavioural card
      |  | Users who DID these events : | Application>E1 |
    And I apply the filter for "Users who DID these events :" as
      | Event Time | after | date | 1 Jan 2024 |
    And I did these events for following event type
      | least recent ⚙ Event Time | before | date |  |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "User Condition>who did these Events>least recent event time"

  @End-to-End @Distributed
  Scenario: To verify Edit option by editing existing segment and updating the new user in it and verfiying new user is added in segment successfully
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "User updation usecase"
    And Pass following details in user card
      | Visitor Type | All Users  |
      | User IDs     | one of>LS1 |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Given I navigate to "Live Segments" page via "Segments"
    Given perform "Edit" action for "User updation usecase" segment on the listing page
    Then I Edit the Segment
    And Pass following details in user card
      | Visitor Type | All Users      |
      | User IDs     | one of>LS1,LS2 |
    And Save the Segment
    Then check whether the segment has been "Updated" and gets listed on the page.
    Then I Open the Segment "User updation usecase"

  @End-to-End @Distributed
  Scenario: To verify the segment creation with Web Device one of Desktop
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "Technology Condition >Website >add device>Desktop"
    And Pass following details for "Web" in Technology card
      | Devices | is one of>Desktop |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "Technology Condition >Website >add device>Desktop"

  @End-to-End @Distributed
  Scenario: To verify the segment creation with Web Device neither of Tablet
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "Technology Condition >Website>neither>Tablet"
    And Pass following details for "Web" in Technology card
      | Devices | is neither of>Tablet |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "Technology Condition >Website>neither>Tablet"

  @End-to-End @Distributed
  Scenario: To verify the segment creation with Web Operating System one of Mac OS
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "Technology Condition >Operatibg system>one of>Mac OS"
    And Pass following details for "Web" in Technology card
      | Operating System | is one of>Mac OS |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "Technology Condition >Operatibg system>one of>Mac OS"

  @End-to-End @Distributed
  Scenario: To verify the edit functionality by updating the existing segment from details page and adding new user
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "Segment updation from overview page"
    And Pass following details in user card
      | Visitor Type | All Users    |
      | User IDs     | equal to>LS1 |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Given I navigate to "Live Segments" page via "Segments"
    Then I Open the Segment "Segment updation from overview page"
    And I click on the "Edit" tool-tip
    Then I Edit the Segment
    And Pass following details in user card
      | Visitor Type | All Users    |
      | User IDs     | equal to>LS2 |
    And Save the Segment
    Then check whether the segment has been "Updated" and gets listed on the page.
    Then I Open the Segment "Segment updation from overview page"

  @End-to-End @Distributed
  Scenario: To verify Duplicate functionality by duplicating the existing segment and verifiying duplicated segment is reflecting in listing page with valid users.
    Given I navigate to "Live Segments" page via "Segments"
    Then I Open the Segment "Segment duplication from overview page"
    And I click on the "Duplicate" tool-tip
    Then verify the popup for Segment Duplication and duplicate it in same project itself
    Given I navigate to "Live Segments" page via "Segments"
    Then I Open the Segment "Copy of Segment duplication from overview page"

  Scenario: To verify Delete functionality by deleting the existing segment and verifiying deleted segment is not reflecting in listing page once deleted.
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "Segment deletion from overview page"
    And Pass following details in user card
      | Visitor Type | All Users    |
      | User IDs     | equal to>LS1 |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Given I navigate to "Live Segments" page via "Segments"
    Then I Open the Segment "Segment deletion from overview page"
    Then I delete segment from tooltip

  Scenario: To verify Tag functionality by adding the Tag in existing segment and verifiying Tag is reflecting for added segment.
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "Segment tag addition from overview page"
    And Pass following details in user card
      | Visitor Type | All Users    |
      | User IDs     | equal to>LS1 |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Given I navigate to "Live Segments" page via "Segments"
    Then I Open the Segment "Segment tag addition from overview page"
    And I click on the "Tag" tool-tip
    And create new tag with name as "segmenttagcheck" for segment "Segment tag addition from overview page" from overview page
    Given I navigate to "Live Segments" page via "Segments"
    And I search for "Segment tag addition from overview page" segment
    Then I verify "segmenttagcheck" tag is attached to the "Segment tag addition from overview page" segment

  Scenario: To verify Download List of users from segment listing page by verifying users in segment are reflecting in csv file.
    Given I navigate to "Live Segments" page via "Segments"
    And perform "Download List of Users" action for "Duplicate this Segment" segment on the listing page
    Then I download the list and verify it

  @End-to-End @Distributed
  Scenario: To verify the segment creation with Android Advertising ID contains entered value
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "Technology Condition >Android>Advertising ID"
    And Pass following details for "Android" in Technology card
      | Advertising ID | contains>b34d7f33-144d-49ac-acb7-b558cab44598 |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "Technology Condition >Android>Advertising ID"

  @End-to-End @Distributed
  Scenario: To verify the segment creation with iOS Vendor ID one of selected value
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "Technology Condition >IOS>Vendor ID"
    And Pass following details for "iOS" in Technology card
      | Vendor ID | one of>43603158-C58B-442E-B074-54AAEE3F8655 |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "Technology Condition >IOS>Vendor ID"

  @End-to-End @Distributed
  Scenario: To verify the segment creation with User System Attribute Email address matches the regex from selected dropdown value
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "User System Attribute Email address matches the regex from selected dropdown value"
    And Pass following details in user card
      | Visitor Type                    | All Users                   |
      | User Attribute>Select an Option | Email Address               |
      | User Attribute>Select...        | matches regex               |
      | User Attribute>Enter a value    | aditya.dhanve@webengage.com |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "User System Attribute Email address matches the regex from selected dropdown value"

  @End-to-End @Distributed
  Scenario: To verify the segment creation with Reachability User Reachable on Email
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "User Condition > Reachability on Email"
    And Pass following details in user card
      | Visitor Type | All Users          |
      | Reachability | Reachable on-Email |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "User Condition > Reachability on Email"

  @End-to-End @Distributed
  Scenario: To verify the segment creation with Reachability User Not Reachable on Email
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "User Condition > Not Reachability on Email"
    And Pass following details in user card
      | Visitor Type | All Users              |
      | Reachability | Not reachable on-Email |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "User Condition > Not Reachability on Email"

  @End-to-End @Distributed
  Scenario: To verify the segment creation with Intelligent Attribute Best Channel is not in Email
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "Intelligent Attributes >is not in>Mail"
    And Pass following details in user card
      | Intelligent Attributes | Best Channel-is not in |
      | Intelligent Attributes | Best Channel-Email     |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "Intelligent Attributes >is not in>Mail"

  @End-to-End @Distributed
  Scenario: To verify the Audit log View option it should display the segment creation name in Audit details pop up page.
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "audit log view segment"
    And Pass following details in user card
      | Visitor Type | All Users    |
      | User IDs     | equal to>LS1 |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "audit log view segment"
    Then verify segment is "created" with name "audit log view segment"

  @End-to-End @Distributed
  Scenario: To verify the Audit log View option it should display the segment updation name in Audit details pop up page.
    Given I navigate to "Live Segments" page via "Segments"
    Given perform "Edit" action for "audit log view segment" segment on the listing page
    Then I Edit the Segment
    Then I update Live segment with name as "audit log view segment edited"
    And Save the Segment
    Then I Open the Segment "audit log view segment edited"
    Then verify segment is "edited" with name "audit log view segment edited"

  @Staging
  Scenario: To create segment on production/master branch namespace and verify the campaign on feature branch namespace
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "Master - Feature live segment"
    And Pass following details in user card
      | Visitor Type | All Users   |
      | User IDs     | ends with>j |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I switch the namespace
    Given I navigate to "Live Segments" page via "Segments"
    Then I Open the Segment "Master - Feature live segment"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "Master - Feature live segment"
    Then I switch the namespace

  @End-to-End @Distributed
  Scenario: Create a segment by using technology condtion > Android/IOS > Last seen after/before last/next x-days/hours/months etc
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "Technology Condition >Android>Last Seen"
    And Pass following details for "Android" in Technology card
      | Last Seen   | after-date |
      | Set Date as | 1 Jan 2024 |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "Technology Condition >Android>Last Seen"

  @End-to-End @Distributed
  Scenario: Create a segment by using Users condition > User IDS is one of / none of and add multiple users comma separated
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "segment for multiple users usecase"
    And Pass following details in user card
      | Visitor Type | All Users                  |
      | User IDs     | one of>AutoID1,AutoID2,LS1 |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "segment for multiple users usecase"

  @End-to-End @Distributed
  Scenario: Create a segment by using technology condtion > Android/IOS > Last seen is within last x-days/hours/months etc to next x days/hours/months etc
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "Technology Condition >Android>Last Seen>within"
    And Pass following details for "Android" in Technology card
      | Last Seen   | within-date |
      | Set Date as | 1 Jan 2024  |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "Technology Condition >Android>Last Seen>within"

  Scenario: To create any campaign, use a single static list, and verify the count on audience page.
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "SMS:Single static list" Campaign Name
    Then I fill the details in Audience tab as below
      | Audience Type | Send to users in single segment |  |
    Then select Segment having name as "StaticListStatsValidationUser"
    Then I verify reachable user count as "1"

  Scenario: To create any campaign, use a single live Segment, and verify the count on audience page. page.
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "SMS:Single live Segment" Campaign Name
    Then I fill the details in Audience tab as below
      | Audience Type | Send to users in single segment |  |
    Then select Segment having name as "StatsValidationUser Segment"
    Then I verify reachable user count as "1"

  Scenario: To create any campaign using users in ANY of these segments inclusion-exclusion (Include live exclude live) and verify the count audience page
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "SMS:Users any-Include live exclude live" Campaign Name
    Then I fill the details in Audience tab as below
      | Audience Type | multiple segments |  |
    Then I select "Users in ANY of these segments" as "Send To" Segment for search
    Then select Segment having name as "IncludeLive" for "include"
    Then I select "Users in ANY of these segments" as "EXCLUDE" Segment for search
    Then select Segment having name as "ExcludeLive" for "exclude"
    Then I verify reachable user count as "4"

  Scenario: To create any campaign using users in ANY of these segments inclusion-exclusion (Include list exclude list) and verify the count audience page
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "SMS:Users any-Include list exclude list" Campaign Name
    Then I fill the details in Audience tab as below
      | Audience Type | multiple segments |  |
    Then I select "Users in ANY of these segments" as "Send To" Segment for search
    Then select Segment having name as "IncludeList" for "include"
    Then I select "Users in ANY of these segments" as "EXCLUDE" Segment for search
    Then select Segment having name as "ExcludeList" for "exclude"
    Then I verify reachable user count as "4"

  Scenario: To create any campaign using users in ANY of these segments inclusion-exclusion (Include live exclude list) and verify the count audience page
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "SMS:Users any- Include live exclude list" Campaign Name
    Then I fill the details in Audience tab as below
      | Audience Type | multiple segments |  |
    Then I select "Users in ANY of these segments" as "Send To" Segment for search
    Then select Segment having name as "IncludeLive" for "include"
    Then I select "Users in ANY of these segments" as "EXCLUDE" Segment for search
    Then select Segment having name as "ExcludeList" for "exclude"
    Then I verify reachable user count as "4"

  Scenario: To create any campaign using users in ANY of these segments inclusion-exclusion (Include list exclude live) and verify the count audience page
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "SMS:Users any-Include list exclude live" Campaign Name
    Then I fill the details in Audience tab as below
      | Audience Type | multiple segments |  |
    Then I select "Users in ANY of these segments" as "Send To" Segment for search
    Then select Segment having name as "IncludeList" for "include"
    Then I select "Users in ANY of these segments" as "EXCLUDE" Segment for search
    Then select Segment having name as "ExcludeLive" for "exclude"
    Then I verify reachable user count as "4"

  Scenario: To create any campaign using users in ANY of these segments inclusion-exclusion (Include all user exclude live) and verify the count audience page
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "SMS:Users any-Include all user exclude live" Campaign Name
    Then I fill the details in Audience tab as below
      | Audience Type | multiple segments |  |
    Then I select "Users in ANY of these segments" as "Send To" Segment for search
    Then I click on Segment creation button
    And I add "Segment Name" as "SMS: User Condition > Reachability on SMS"
    And Pass following details in user card
      | Visitor Type | All Users        |
      | Reachability | Reachable on-SMS |
    And Save the Segment
    Then fetch the count of reachable users
    Then I select "Users in ANY of these segments" as "EXCLUDE" Segment for search
    Then select Segment having name as "ExcludeLive" for "exclude"
    Then verify difference between all user and included usercount 1
    Then I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "SMS: User Condition > Reachability on SMS"

  Scenario: To create any campaign using users in ANY of these segments inclusion-exclusion (Include all user exclude list) and verify the count audience page
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "SMS:Users any-Include all user exclude list" Campaign Name
    Then I fill the details in Audience tab as below
      | Audience Type | multiple segments |  |
    Then I select "Users in ANY of these segments" as "Send To" Segment for search
    Then I click on Segment creation button
    And I add "Segment Name" as "SMS: User Condition > Reachability on SMS"
    And Pass following details in user card
      | Visitor Type | All Users        |
      | Reachability | Reachable on-SMS |
    And Save the Segment
    Then fetch the count of reachable users
    Then I select "Users in ANY of these segments" as "EXCLUDE" Segment for search
    Then select Segment having name as "ExcludeList" for "exclude"
    Then verify difference between all user and included usercount 1
    Then I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "SMS: User Condition > Reachability on SMS"

  Scenario: To create any campaign using users in ALL of these segments inclusion-exclusion (Include live exclude live) and verify the count audience page
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "SMS:Users all-Include live exclude live" Campaign Name
    Then I fill the details in Audience tab as below
      | Audience Type | multiple segments |  |
    Then I select "Users in ALL of these segments" as "Send To" Segment for search
    Then select multiple segments for "include" section
      | IncludeLive       |
      | Include3UsersLive |
    Then I select "Users in ALL of these segments" as "EXCLUDE" Segment for search
    Then select Segment having name as "ExcludeLive" for "exclude"
    Then I verify reachable user count as "2"

  Scenario: To create any campaign using users in ALL of these segments inclusion-exclusion (Include list exclude list) and verify the count audience page
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "SMS:Users all-Include list exclude list" Campaign Name
    Then I fill the details in Audience tab as below
      | Audience Type | multiple segments |  |
    Then I select "Users in ALL of these segments" as "Send To" Segment for search
    Then select multiple segments for "include" section
      | IncludeList       |
      | Include3UsersList |
    Then I select "Users in ALL of these segments" as "EXCLUDE" Segment for search
    Then select Segment having name as "ExcludeList" for "exclude"
    Then I verify reachable user count as "2"

  Scenario: To create any campaign using users in ALL of these segments inclusion-exclusion (Include live exclude list) and verify the count audience page
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "SMS:Users all-Include live exclude list" Campaign Name
    Then I fill the details in Audience tab as below
      | Audience Type | multiple segments |  |
    Then I select "Users in ALL of these segments" as "Send To" Segment for search
    Then select Segment having name as "IncludeLive" for "include"
    Then I select "Users in ALL of these segments" as "EXCLUDE" Segment for search
    Then select multiple segments for "exclude" section
      | ExcludeList       |
      | Include3UsersList |
    Then I verify reachable user count as "4"

  Scenario: To Create any campaign using users in ALL of these segments inclusion-exclusion (Include list exclude live) and verify the count audience page
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "SMS:Users all-Include list exclude live" Campaign Name
    Then I fill the details in Audience tab as below
      | Audience Type | multiple segments |  |
    Then I select "Users in ALL of these segments" as "Send To" Segment for search
    Then select Segment having name as "IncludeList" for "include"
    Then I select "Users in ALL of these segments" as "EXCLUDE" Segment for search
    Then select multiple segments for "exclude" section
      | ExcludeLive       |
      | Include3UsersLive |
    Then I verify reachable user count as "4"

  Scenario: Create any campaign using users in ALL of these segments inclusion-exclusion (Include all user exclude live) and verify the count audience page
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "SMS:Users all-Include all user exclude live" Campaign Name
    Then I fill the details in Audience tab as below
      | Audience Type | multiple segments |  |
    Then I select "Users in ALL of these segments" as "Send To" Segment for search
    Then select multiple segments for "include" section
      | All Users         |
      | Include3UsersLive |
    Then I select "Users in ALL of these segments" as "EXCLUDE" Segment for search
    Then select Segment having name as "ExcludeLive" for "exclude"
    Then I verify reachable user count as "2"

  Scenario: Create any campaign using users in ALL of these segments inclusion-exclusion (Include all user exclude list) and verify the count audience page
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "SMS:Users all-Include all user exclude list" Campaign Name
    Then I fill the details in Audience tab as below
      | Audience Type | multiple segments |  |
    Then I select "Users in ALL of these segments" as "Send To" Inclusion Segment
    And I add "Segment Name" as "SMS: User Condition > Reachability on SMS"
    And Pass following details in user card
      | Visitor Type | All Users        |
      | Reachability | Reachable on-SMS |
    And Save the Segment
    Then fetch the count of reachable users
    Then I select "Users in ALL of these segments" as "EXCLUDE" Segment for search
    Then select multiple segments for "exclude" section
      | IncludeList       |
      | Include3UsersList |
    Then verify difference between all user and included usercount 3
    Then I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "SMS: User Condition > Reachability on SMS"

  @End-to-End @Distributed
  Scenario Outline: To verify the segment creation and count of users who performed a custom event <eventCount>
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "Users who did this Event <eventCount> without any filter"
    And Pass following details in Behavioural card
      |  | Users who DID these events : | Application>count_check_event |
    And I did these events for following event type
      | <eventCount> |  |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "Users who did this Event <eventCount> without any filter"
    And I navigate to "Live Segments" page via "Segments"

    Examples: 
      | eventCount    |
      | only once     |
      | at least once |

  @End-to-End @Distributed
  Scenario Outline: To verify the segment creation and count of users who performed a custom event with no. of occurences <operator> condition
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "Users with no. of occurences <operator> without any filter"
    And Pass following details in Behavioural card
      |  | Users who DID these events : | Application>count_check_event |
    And I did these events for following event type
      | no. of occurences | <operator> | <value> |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "Users with no. of occurences <operator> without any filter"
    And I navigate to "Live Segments" page via "Segments"

    Examples: 
      | operator                    | value  |
      | greater than                |      3 |
      | less than                   |      3 |
      | is greater than or equal to |      3 |
      | is less than or equal to    |      3 |
      | is equal to                 |      6 |
      | between                     | 3 to 7 |

  @End-to-End @Distributed
  Scenario Outline: To verify the segment creation and count of users who performed a custom event with <case> of integer <operator> condition
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "Users with <case> of integer <operator> without any filter"
    And Pass following details in Behavioural card
      |  | Users who DID these events : | Application>max_min_avg_event |
    And I did these events for following event type
      | <case> of count_number | <operator> | <value> |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "Users with <case> of integer <operator> without any filter"
    And I navigate to "Live Segments" page via "Segments"

    Examples: 
      | operator                    | value        | case    |
      | greater than                |          600 | maximum |
      | less than                   |          600 | maximum |
      | is greater than or equal to |          600 | maximum |
      | is less than or equal to    |          600 | maximum |
      | is equal to                 |          600 | maximum |
      | between                     | 600 to 1200  | maximum |
      | greater than                |          200 | minimum |
      | less than                   |          200 | minimum |
      | is greater than or equal to |          200 | minimum |
      | is less than or equal to    |          200 | minimum |
      | is equal to                 |          200 | minimum |
      | between                     | 200 to 500   | minimum |
      | greater than                |         1200 | total   |
      | less than                   |         1200 | total   |
      | is greater than or equal to |         1200 | total   |
      | is less than or equal to    |         1200 | total   |
      | is equal to                 |         1200 | total   |
      | between                     | 1200 to 2100 | total   |
      | greater than                |          700 | average |
      | less than                   |          700 | average |
      | is greater than or equal to |          700 | average |
      | is less than or equal to    |          700 | average |
      | is equal to                 |          700 | average |
      | between                     | 400 to 700   | average |

  @End-to-End @Distributed
  Scenario Outline: To verify the segment creation and count of users who performed a custom event whose <case> <dateType> date is <operator> date
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "Users <operator> <case> <dateType> date without any filter"
    And Pass following details in Behavioural card
      |  | Users who DID these events : | Application><eventName> |
    And I did these events for following event type
      | <case> <eventAttribute> | <operator> | date | <date> |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "Users <operator> <case> <dateType> date without any filter"

    Examples: 
      | operator | case         | eventAttribute | date                       | dateType | eventName         |
      | before   | most recent  | count_Date     | 15 Feb 2024                | custom   | date_test_event   |
      | after    | most recent  | count_Date     | 15 Feb 2024                | custom   | date_test_event   |
      | on       | most recent  | count_Date     | 15 Feb 2024                | custom   | date_test_event   |
      | present  | most recent  | count_Date     |                            | custom   | date_test_event   |
      | within   | most recent  | count_Date     | 25 Jan 2024 to 10 Mar 2024 | custom   | date_test_event   |
      | before   | least recent | count_Date     | 28 Jan 2024                | custom   | date_test_event   |
      | after    | least recent | count_Date     | 28 Jan 2024                | custom   | date_test_event   |
      | on       | least recent | count_Date     | 28 Jan 2024                | custom   | date_test_event   |
      | present  | least recent | count_Date     |                            | custom   | date_test_event   |
      | within   | least recent | count_Date     | 10 Jan 2024 to 20 Feb 2024 | custom   | date_test_event   |
      | before   | most recent  | ⚙ Event Time   | 24 Jan 2024                | system   | system_date_event |
      | after    | most recent  | ⚙ Event Time   | 24 Jan 2024                | system   | system_date_event |
      | on       | most recent  | ⚙ Event Time   | 24 Jan 2024                | system   | system_date_event |
      | present  | most recent  | ⚙ Event Time   |                            | system   | system_date_event |
      | within   | most recent  | ⚙ Event Time   | 14 Jan 2024 to 5 Feb 2024  | system   | system_date_event |
      | before   | least recent | ⚙ Event Time   | 28 Jan 2024                | system   | system_date_event |
      | after    | least recent | ⚙ Event Time   | 28 Jan 2024                | system   | system_date_event |
      | on       | least recent | ⚙ Event Time   | 28 Jan 2024                | system   | system_date_event |
      | present  | least recent | ⚙ Event Time   |                            | system   | system_date_event |
      | within   | least recent | ⚙ Event Time   | 20 Jan 2024 to 7 Feb 2024  | system   | system_date_event |

  @End-to-End @Distributed
  Scenario Outline: To verify the segment creation and count of users who did custom event <Occurences> with <DataType> attribute and <operator> operator in event filter
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "User who did custom Event <Occurences> with <DataType> attribute <operator> filter"
    And Pass following details in Behavioural card
      |  | Users who DID these events : | Application>filter_count_event |
    And I apply the filter for "Users who DID these events :" as
      | <eventAttribute> | <operator> | <eventAttributeValue> | <date> |
    And I did these events for following event type
      | <Occurences> |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "User who did custom Event <Occurences> with <DataType> attribute <operator> filter"

    Examples: 
      | Occurences    | DataType | eventAttribute | operator                 | eventAttributeValue                                                                       | date                      |
      | at least once | boolean  | count_boolean  | equal to                 | True                                                                                      |                           |
      | at least once | boolean  | count_boolean  | not equal to             | True                                                                                      |                           |
      | at least once | boolean  | count_boolean  | is empty                 |                                                                                           |                           |
      | at least once | boolean  | count_boolean  | is not empty             |                                                                                           |                           |
      | only once     | boolean  | count_boolean  | equal to                 | True                                                                                      |                           |
      | only once     | boolean  | count_boolean  | not equal to             | True                                                                                      |                           |
      | only once     | boolean  | count_boolean  | is empty                 |                                                                                           |                           |
      | only once     | boolean  | count_boolean  | is not empty             |                                                                                           |                           |
      | at least once | number   | count_number   | equal to                 |                                                                                       600 |                           |
      | at least once | number   | count_number   | not equal to             |                                                                                       600 |                           |
      | at least once | number   | count_number   | less than                |                                                                                       600 |                           |
      | at least once | number   | count_number   | greater than             |                                                                                       600 |                           |
      | at least once | number   | count_number   | less than or equal to    |                                                                                       600 |                           |
      | at least once | number   | count_number   | greater than or equal to |                                                                                       600 |                           |
      | at least once | number   | count_number   | between                  | 600 to 900                                                                                |                           |
      | at least once | number   | count_number   | not between              | 600 to 900                                                                                |                           |
      | at least once | number   | count_number   | is not empty             |                                                                                           |                           |
      | at least once | number   | count_number   | is empty                 |                                                                                           |                           |
      | at least once | number   | count_number   | one of                   |                                                                                   600,700 |                           |
      | at least once | number   | count_number   | none of                  |                                                                                   600,700 |                           |
      | at least once | string   | count_string   | equal to                 | TestingForString                                                                          |                           |
      | at least once | string   | count_string   | not equal to             | TestingForString                                                                          |                           |
      | at least once | string   | count_string   | one of                   | TestingForString,TestingString                                                            |                           |
      | at least once | string   | count_string   | none of                  | TestingForString,TestingString,TestingForStringWithoutBooelan,TestingForStringWithoutDate |                           |
      | at least once | string   | count_string   | starts with              | filterTest                                                                                |                           |
      | at least once | string   | count_string   | does not start with      | Testing                                                                                   |                           |
      | at least once | string   | count_string   | ends with                | Date                                                                                      |                           |
      | at least once | string   | count_string   | does not end with        | String                                                                                    |                           |
      | at least once | string   | count_string   | contains                 | For                                                                                       |                           |
      | at least once | string   | count_string   | does not contain         | For                                                                                       |                           |
      | at least once | string   | count_string   | is not empty             |                                                                                           |                           |
      | at least once | string   | count_string   | is empty                 |                                                                                           |                           |
      | at least once | Date     | count_Date     | after                    | date                                                                                      | 15 Jan 2024               |
      | at least once | Date     | count_Date     | before                   | date                                                                                      | 15 Jan 2024               |
      | at least once | Date     | count_Date     | on                       | date                                                                                      | 15 Jan 2024               |
      | at least once | Date     | count_Date     | present                  |                                                                                           |                           |
      | at least once | Date     | count_Date     | not present              |                                                                                           |                           |
      | at least once | Date     | count_Date     | within                   | date                                                                                      | 15 Jan 2024 to 5 Feb 2024 |

  @End-to-End @Distributed
  Scenario Outline: To verify the segment creation and count of users who did custom event with no. of occurences <occurenceOperator> condition and event filter having <DataType> attribute and <operator> operator
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "User who did custom Event>no. of occurences <occurenceOperator> with <DataType> attribute <operator> filter"
    And Pass following details in Behavioural card
      |  | Users who DID these events : | Application>filter_count_event |
    And I apply the filter for "Users who DID these events :" as
      | <eventAttribute> | <operator> | <eventAttributeValue> |  |
    And I did these events for following event type
      | <Occurences> | <occurenceOperator> | <value> |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "User who did custom Event>no. of occurences <occurenceOperator> with <DataType> attribute <operator> filter"

    Examples: 
      | Occurences        | DataType | eventAttribute | operator     | eventAttributeValue | occurenceOperator           | value   |
      | no. of occurences | boolean  | count_boolean  | equal to     | True                | greater than                |       3 |
      | no. of occurences | boolean  | count_boolean  | equal to     | True                | less than                   |       3 |
      | no. of occurences | boolean  | count_boolean  | equal to     | True                | is greater than or equal to |       3 |
      | no. of occurences | boolean  | count_boolean  | equal to     | True                | is less than or equal to    |       3 |
      | no. of occurences | boolean  | count_boolean  | equal to     | True                | is equal to                 |       3 |
      | no. of occurences | boolean  | count_boolean  | equal to     | True                | between                     | 3 to 5  |
      | no. of occurences | boolean  | count_boolean  | not equal to | True                | greater than                |       3 |
      | no. of occurences | boolean  | count_boolean  | not equal to | True                | less than                   |       3 |
      | no. of occurences | boolean  | count_boolean  | not equal to | True                | is greater than or equal to |       3 |
      | no. of occurences | boolean  | count_boolean  | not equal to | True                | is less than or equal to    |       3 |
      | no. of occurences | boolean  | count_boolean  | not equal to | True                | is equal to                 |       4 |
      | no. of occurences | boolean  | count_boolean  | not equal to | True                | between                     | 2 to 4  |
      | no. of occurences | boolean  | count_boolean  | is empty     |                     | greater than                |       5 |
      | no. of occurences | boolean  | count_boolean  | is empty     |                     | less than                   |       5 |
      | no. of occurences | boolean  | count_boolean  | is empty     |                     | is greater than or equal to |       1 |
      | no. of occurences | boolean  | count_boolean  | is empty     |                     | is less than or equal to    |       5 |
      | no. of occurences | boolean  | count_boolean  | is empty     |                     | is equal to                 |      11 |
      | no. of occurences | boolean  | count_boolean  | is empty     |                     | between                     | 1 to 11 |
      | no. of occurences | boolean  | count_boolean  | is not empty |                     | greater than                |       3 |
      | no. of occurences | boolean  | count_boolean  | is not empty |                     | less than                   |       3 |
      | no. of occurences | boolean  | count_boolean  | is not empty |                     | is greater than or equal to |       3 |
      | no. of occurences | boolean  | count_boolean  | is not empty |                     | is less than or equal to    |       3 |
      | no. of occurences | boolean  | count_boolean  | is not empty |                     | is equal to                 |       3 |
      | no. of occurences | boolean  | count_boolean  | is not empty |                     | between                     | 3 to 5  |

  Scenario: To verify duplication of liveSegment using behavioural card
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "Runtime Duplication of BehaviouralCondition segment"
    And Pass following details in Behavioural card
      |  | Users who DID these events :        | Application>E1 |
      |  | Users who DID NOT do these events : | Application>E2 |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    And perform "Duplicate" action for "Runtime Duplication of BehaviouralCondition segment" segment on the listing page
    Then verify the popup for Segment Duplication and duplicate it in same project itself
    Then verify that the segment "Copy of Runtime Duplication of BehaviouralCondition segment" gets Listed on the page
    Then I delete the following mentioned "segments"
      | Runtime Duplication of BehaviouralCondition segment         |
      | Copy of Runtime Duplication of BehaviouralCondition segment |

  Scenario: To verify duplication of liveSegment using technology card
    Given I navigate to "Live Segments" page via "Segments"
    Then I create a Live segment with name as "Runtime Duplication of TechnologyCondition segment"
    And Pass following details for "Android" in Technology card
      | App ID | equal to>com.webengage.production |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    And perform "Duplicate" action for "Runtime Duplication of TechnologyCondition segment" segment on the listing page
    Then verify the popup for Segment Duplication and duplicate it in same project itself
    Then verify that the segment "Copy of Runtime Duplication of TechnologyCondition segment" gets Listed on the page
    Then I delete the following mentioned "segments"
      | Runtime Duplication of TechnologyCondition segment         |
      | Copy of Runtime Duplication of TechnologyCondition segment |
