@EventAnalyticsCore @Regression
Feature: Event Analytics Core Functionality

  @Smoke
  Scenario: To verify that Events page is loading and displaying the stats on all the required sections
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | Occurrences       |
      | OF       | All Custom Events |
      | OVER     | -                 |
      | SPLIT BY | -                 |
    Given open "Table" view for the section with header name as "SHOW"
    And verify following things get reflected in populated table
      | TriggerSMSEvent   | >2 |
      | TriggerEmailEvent | >2 |

  @Smoke
  Scenario: To verify that in Events page it allows downloading the report successfully
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | Occurrences     |
      | OF       | TriggerSMSEvent |
      | OVER     | Hours of Day    |
      | SPLIT BY | Country         |
    Then I download the report
      |  | Show Event |

  @Smoke
  Scenario Outline: To verify if All system event are displaying
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>        |
      | OF       | All System Events |
      | OVER     | -                 |
      | SPLIT BY | -                 |
    Then Verify presence of Events
      | Email Rejected  |
      | Journey Started |

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  @Smoke
  Scenario Outline: To verify if All custom(application) event are displaying
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>        |
      | OF       | All Custom Events |
      | OVER     | -                 |
      | SPLIT BY | -                 |
    Then Verify presence of Events
      | multi_event       |
      | TriggerSMSEvent   |
      | TriggerEmailEvent |

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify the custom event over DAYS and split by custom attribute
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>  |
      | OF       | multi_event |
      | OVER     | <DayType>   |
      | SPLIT BY | new_attr_2  |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter | Value         |
      |     111.0 | <Count_attr1> |
      |     222.0 | <Count_attr2> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter | Value         |
      |     111.0 | <Count_attr1> |
      |     222.0 | <Count_attr2> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "equal to" expected value in table displayed
      | Parameter | Value         |
      | <DayType> |               |
      | Total     | <Total>       |
      |     111.0 | <Count_attr1> |
      |     222.0 | <Count_attr2> |

    Examples: 
      | showType    | Count_attr1 | Count_attr2 | DayType | Total |
      | Occurrences |         110 |         100 | Days    |   210 |
      | Uniques     |         100 |         100 | Days    |   200 |

  Scenario Outline: To verify the <showType> of <eventType> event over <DayType> and split by user custom attribute
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType> |
      | OF       | <event>    |
      | OVER     | <DayType>  |
      | SPLIT BY | <splitBy>  |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "<operator>" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter   | Value        |
      | <attribute> | <count_attr> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "<operator>" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter   | Value        |
      | <attribute> | <count_attr> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "<operator>" expected value in table displayed
      | Parameter   | Value        |
      | <DayType>   | <value>      |
      | <attribute> | <count_attr> |

    Examples: 
      | showType    | count_attr | DayType        | Total | value      | event           | splitBy             | attribute                      | operator     | eventType |
      | Occurrences |        100 | Days           |   100 |            | multi_event_B   | user_attr_testing_1 | user_attr_testing_1_test_value | equal to     | custom    |
      | Uniques     |         50 | Days           |    50 |            | multi_event_B   | user_attr_testing_1 | user_attr_testing_1_test_value | equal to     | custom    |
      | Occurrences |        100 | Weeks          |   100 |            | multi_event_B   | user_attr_testing_1 | user_attr_testing_1_test_value | equal to     | custom    |
      | Uniques     |         50 | Weeks          |    50 |            | multi_event_B   | user_attr_testing_1 | user_attr_testing_1_test_value | equal to     | custom    |
      | Occurrences |        100 | Months         |   100 |            | multi_event_B   | user_attr_testing_1 | user_attr_testing_1_test_value | equal to     | custom    |
      | Uniques     |         50 | Months         |    50 |            | multi_event_B   | user_attr_testing_1 | user_attr_testing_1_test_value | equal to     | custom    |
      | Occurrences |        100 | Months of Year |   100 |            | multi_event_B   | user_attr_testing_1 | user_attr_testing_1_test_value | equal to     | custom    |
      | Uniques     |         50 | Months of Year |    50 |            | multi_event_B   | user_attr_testing_1 | user_attr_testing_1_test_value | equal to     | custom    |
      | Occurrences |        100 | Hours of Day   |   100 | 12am - 1am | multi_event_B   | user_attr_testing_1 | user_attr_testing_1_test_value | equal to     | custom    |
      | Uniques     |         50 | Hours of Day   |    50 | 12am - 1am | multi_event_B   | user_attr_testing_1 | user_attr_testing_1_test_value | equal to     | custom    |
      | Occurrences |          1 | Days           |     1 |            | Session Started | user_attr_testing_1 | user_attr_value_1              | greater than | system    |
      | Uniques     |          1 | Days           |     1 |            | Session Started | user_attr_testing_1 | user_attr_value_1              | greater than | system    |
      | Occurrences |          1 | Weeks          |     1 |            | Session Started | user_attr_testing_1 | user_attr_value_1              | greater than | system    |
      | Uniques     |          1 | Weeks          |     1 |            | Session Started | user_attr_testing_1 | user_attr_value_1              | greater than | system    |
      | Occurrences |          1 | Months         |     1 |            | Session Started | user_attr_testing_1 | user_attr_value_1              | greater than | system    |
      | Uniques     |          1 | Months         |     1 |            | Session Started | user_attr_testing_1 | user_attr_value_1              | greater than | system    |
      | Occurrences |          1 | Months of Year |     1 |            | Session Started | user_attr_testing_1 | user_attr_value_1              | greater than | system    |
      | Uniques     |          1 | Months of Year |     1 |            | Session Started | user_attr_testing_1 | user_attr_value_1              | greater than | system    |
      | Occurrences |          1 | Hours of Day   |     1 | 12am - 1am | Session Started | user_attr_testing_1 | user_attr_value_1              | greater than | system    |
      | Uniques     |          1 | Hours of Day   |     1 | 12am - 1am | Session Started | user_attr_testing_1 | user_attr_value_1              | greater than | system    |

  Scenario Outline: To verify the <showType> of <eventType> event with custom attribute event filter and split by user custom attribute
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType> |
      | OF       | <event>    |
      | OVER     | <DayType>  |
      | SPLIT BY | <splitBy>  |
    Then I apply filter for events
      | Event | <event> | <eventAttrbute> | equal to | <eventAttrValue> |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "<operator>" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter   | Value            |
      | <parameter> | <parameterValue> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "<operator>" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter   | Value            |
      | <parameter> | <parameterValue> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "<operator>" expected value in table displayed
      | Parameter   | Value            |
      | <DayType>   |                  |
      | <parameter> | <parameterValue> |

    Examples: 
      | showType    | event               | DayType | splitBy             | eventAttrbute  | eventAttrValue | operator     | Total | parameter                      | parameterValue | eventType |
      | Occurrences | multi_event_B       | Days    | user_attr_testing_1 | E_1            | Testing        | equal to     |   100 | user_attr_testing_1_test_value |            100 | custom    |
      | Uniques     | multi_event_B       | Days    | user_attr_testing_1 | E_1            | Testing        | equal to     |    50 | user_attr_testing_1_test_value |             50 | custom    |
      | Occurrences | Campaign Conversion | Days    | user_attr_testing_1 | revenue_amount |           39.8 | greater than |     1 | user_attr_value_1              |              1 | system    |
      | Uniques     | Campaign Conversion | Days    | user_attr_testing_1 | revenue_amount |           39.8 | greater than |     1 | user_attr_value_1              |              1 | system    |

  Scenario Outline: To verify the custom event over WEEKS and split by custom attribute
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>  |
      | OF       | multi_event |
      | OVER     | <DayType>   |
      | SPLIT BY | new_attr_2  |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "equal to" expected value in table displayed
      | Parameter | Value         |
      | <DayType> |               |
      | Total     | <Total>       |
      |     111.0 | <Count_attr1> |
      |     222.0 | <Count_attr2> |

    Examples: 
      | showType    | Count_attr1 | Count_attr2 | DayType | Total |
      | Occurrences |         110 |         100 | Weeks   |   210 |
      | Uniques     |         100 |         100 | Weeks   |   200 |

  Scenario Outline: To verify the custom event over MONTHS and split by custom attribute
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>  |
      | OF       | multi_event |
      | OVER     | <DayType>   |
      | SPLIT BY | new_attr_2  |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter | Value         |
      |     111.0 | <Count_attr1> |
      |     222.0 | <Count_attr2> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter | Value         |
      |     111.0 | <Count_attr1> |
      |     222.0 | <Count_attr2> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "equal to" expected value in table displayed
      | Parameter | Value         |
      | <DayType> |               |
      | Total     | <Total>       |
      |     111.0 | <Count_attr1> |
      |     222.0 | <Count_attr2> |

    Examples: 
      | showType    | Count_attr1 | Count_attr2 | DayType | Total |
      | Uniques     |         100 |         100 | Months  |   200 |
      | Occurrences |         110 |         100 | Months  |   210 |

  Scenario Outline: To verify the custom event over MONTH OF YEAR and split by custom attribute
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>  |
      | OF       | multi_event |
      | OVER     | <DayType>   |
      | SPLIT BY | new_attr_2  |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "equal to" expected value in table displayed
      | Parameter | Value         |
      | <DayType> |               |
      | Total     | <Total>       |
      |     111.0 | <Count_attr1> |
      |     222.0 | <Count_attr2> |

    Examples: 
      | showType    | Count_attr1 | Count_attr2 | DayType        | Total |
      | Uniques     |         100 |         100 | Months of Year |   200 |
      | Occurrences |         110 |         100 | Months of Year |   210 |

  Scenario Outline: To verify the custom event over HOURS OF DAY and split by custom attribute
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>  |
      | OF       | multi_event |
      | OVER     | <DayType>   |
      | SPLIT BY | new_attr_2  |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "equal to" expected value in table displayed
      | Parameter | Value         |
      | <DayType> | 12am - 1am    |
      | Total     | <Total>       |
      |     111.0 | <Count_attr1> |
      |     222.0 | <Count_attr2> |

    Examples: 
      | showType    | Count_attr1 | Count_attr2 | DayType      | Total |
      | Occurrences |         110 |         100 | Hours of Day |   210 |
      | Uniques     |         100 |         100 | Hours of Day |   200 |

  Scenario Outline: To verify the custom event with custom attribute event filter and split by custom attribute
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>  |
      | OF       | multi_event |
      | OVER     | <DayType>   |
      | SPLIT BY | new_attr_2  |
    Then I apply filter for events
      | Event | multi_event | new_attr_2 | equal to | 111 |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "equal to" expected value in table displayed
      | Parameter | Value         |
      | <DayType> |               |
      |     111.0 | <Count_attr1> |

    Examples: 
      | showType    | Count_attr1 | DayType | Total |
      | Occurrences |         110 | Days    |   110 |
      | Uniques     |         100 | Days    |   100 |

  Scenario Outline: To verify the common attribute filter [common event attribute/system attribute] is getting  applied post clicking apply button
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>                                              |
      | OF       | multi_event_A,multi_event_B,multi_event_C,multi_event_D |
      | OVER     | Days                                                    |
      | SPLIT BY | -                                                       |
    Then I apply filter for events
      | Event | Common Filter | E_1 | equal to | Testing |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "equal to" expected value in table displayed
      | Parameter     | Value         |
      | Days          |               |
      | Total         | <Total>       |
      | multi_event_A | <Count_attr1> |
      | multi_event_B | <Count_attr2> |
      | multi_event_C | <Count_attr3> |
      | multi_event_D | <Count_attr4> |

    Examples: 
      | showType    | Count_attr1 | Count_attr2 | Count_attr3 | Count_attr4 | Total |
      | Occurrences |          50 |         100 |          50 |          25 |   225 |
      | Uniques     |          50 |          50 |          30 |          15 |   145 |

  Scenario Outline: To verify the event and event filter is selected with split by and then create a alert where all the selected attribute is displaying on the alert creation page
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>    |
      | OF       | multi_event_A |
      | OVER     | Days          |
      | SPLIT BY | E_2           |
    Then I apply filter for events
      | Event | multi_event_A | E_1 | equal to | Testing |
    Then I create custom alert with name "Custom alert Creation"
      | Send Alert | Above    | specific value |
      | E_1        | equal to | Testing        |
    Given I navigate to "Alerts" page via "Data Platform"
    Then I delete the created custom alert with name "Custom alert Creation"

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify the system event over DAYS and split by system attribute
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>      |
      | OF       | Session Started |
      | OVER     | <DayType>       |
      | SPLIT BY | Browser Name    |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter | Value         |
      | Chrome    | <Count_attr1> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "greater than" expected value in table displayed
      | Parameter | Value         |
      | <DayType> |               |
      | Chrome    | <Count_attr1> |

    Examples: 
      | showType    | Count_attr1 | DayType | Total |
      | Occurrences |           1 | Days    |     1 |
      | Uniques     |           1 | Days    |     1 |

  Scenario Outline: To verify the system event over WEEKS and split by system attribute
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>      |
      | OF       | Session Started |
      | OVER     | <DayType>       |
      | SPLIT BY | Browser Name    |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "greater than" expected value in table displayed
      | Parameter | Value         |
      | <DayType> |               |
      | Chrome    | <Count_attr1> |

    Examples: 
      | showType    | Count_attr1 | DayType | Total |
      | Occurrences |           1 | Weeks   |     1 |
      | Uniques     |           1 | Weeks   |     1 |

  Scenario Outline: To verify the system event over MONTHS and split by system attribute
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>      |
      | OF       | Session Started |
      | OVER     | <DayType>       |
      | SPLIT BY | Browser Name    |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "greater than" expected value in table displayed
      | Parameter | Value         |
      | <DayType> |               |
      | Chrome    | <Count_attr1> |

    Examples: 
      | showType    | Count_attr1 | DayType | Total |
      | Occurrences |           1 | Months  |     1 |
      | Uniques     |           1 | Months  |     1 |

  Scenario Outline: To verify the system event over MONTHS OF YEAR and split by system attribute
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>      |
      | OF       | Session Started |
      | OVER     | <DayType>       |
      | SPLIT BY | Browser Name    |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "greater than" expected value in table displayed
      | Parameter | Value         |
      | <DayType> |               |
      | Chrome    | <Count_attr1> |

    Examples: 
      | showType    | Count_attr1 | DayType        | Total |
      | Occurrences |           1 | Months of Year |     1 |
      | Uniques     |           1 | Months of Year |     1 |

  Scenario Outline: To verify the system event over HOURS OF DAY and split by system attribute
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>      |
      | OF       | Session Started |
      | OVER     | <DayType>       |
      | SPLIT BY | Browser Name    |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "greater than" expected value in table displayed
      | Parameter | Value         |
      | <DayType> | 12am - 1am    |
      | Chrome    | <Count_attr1> |

    Examples: 
      | showType    | Count_attr1 | DayType      | Total |
      | Occurrences |           1 | Hours of Day |     1 |
      | Uniques     |           1 | Hours of Day |     1 |

  Scenario Outline: To verify the custom event over DAYS and split by system attribute
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>    |
      | OF       | Added To Cart |
      | OVER     | <DayType>     |
      | SPLIT BY | Browser Name  |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter | Value         |
      | Chrome    | <Count_attr1> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter | Value         |
      | Chrome    | <Count_attr1> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "greater than" expected value in table displayed
      | Parameter | Value         |
      | <DayType> |               |
      | Chrome    | <Count_attr1> |

    Examples: 
      | showType    | Count_attr1 | DayType | Total |
      | Occurrences |           1 | Days    |     1 |
      | Uniques     |           1 | Days    |     1 |

  Scenario Outline: To verify the custom event over WEEKS and split by system attribute
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>    |
      | OF       | Added To Cart |
      | OVER     | <DayType>     |
      | SPLIT BY | Browser Name  |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "greater than" expected value in table displayed
      | Parameter | Value         |
      | <DayType> |               |
      | Chrome    | <Count_attr1> |

    Examples: 
      | showType    | Count_attr1 | DayType | Total |
      | Occurrences |           1 | Weeks   |     1 |
      | Uniques     |           1 | Weeks   |     1 |

  Scenario Outline: To verify the custom event over MONTHS and split by system attribute
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>    |
      | OF       | Added To Cart |
      | OVER     | <DayType>     |
      | SPLIT BY | Browser Name  |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter | Value         |
      | Chrome    | <Count_attr1> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter | Value         |
      | Chrome    | <Count_attr1> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "greater than" expected value in table displayed
      | Parameter | Value         |
      | <DayType> |               |
      | Chrome    | <Count_attr1> |

    Examples: 
      | showType    | Count_attr1 | DayType | Total |
      | Occurrences |           1 | Months  |     1 |
      | Uniques     |           1 | Months  |     1 |

  Scenario Outline: To verify the custom event over MONTHS OF YEAR and split by system attribute
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>    |
      | OF       | Added To Cart |
      | OVER     | <DayType>     |
      | SPLIT BY | Browser Name  |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter | Value         |
      | Chrome    | <Count_attr1> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter | Value         |
      | Chrome    | <Count_attr1> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "greater than" expected value in table displayed
      | Parameter | Value         |
      | <DayType> |               |
      | Chrome    | <Count_attr1> |

    Examples: 
      | showType    | Count_attr1 | DayType        | Total |
      | Occurrences |           1 | Months of Year |     1 |
      | Uniques     |           1 | Months of Year |     1 |

  Scenario Outline: To verify the custom event over HOURS OF DAY and split by system attribute
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>    |
      | OF       | Added To Cart |
      | OVER     | <DayType>     |
      | SPLIT BY | Browser Name  |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "greater than" expected value in table displayed
      | Parameter | Value         |
      | <DayType> | 12am - 1am    |
      | Chrome    | <Count_attr1> |

    Examples: 
      | showType    | Count_attr1 | DayType      | Total |
      | Occurrences |           1 | Hours of Day |     1 |
      | Uniques     |           1 | Hours of Day |     1 |

  Scenario Outline: To verify the custom event with custom attribute event filter and split by system attribute
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>    |
      | OF       | Added To Cart |
      | OVER     | <DayType>     |
      | SPLIT BY | Browser Name  |
    Then I apply filter for events
      | Event | Added To Cart | Price | equal to | 39.8 |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter | Value         |
      | Chrome    | <Count_attr1> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter | Value         |
      | Chrome    | <Count_attr1> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "greater than" expected value in table displayed
      | Parameter | Value         |
      | <DayType> |               |
      | Chrome    | <Count_attr1> |

    Examples: 
      | showType    | Count_attr1 | DayType | Total |
      | Occurrences |           1 | Days    |     1 |
      | Uniques     |           1 | Days    |     1 |

  Scenario Outline: To verify the user attribute filter is getting applied post clicking apply button for Single event
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>    |
      | OF       | multi_event_A |
      | OVER     | <DayType>     |
      | SPLIT BY | -             |
    Then I apply filter for events
      | Event | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_testing_1_test_value |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter     | Value           |
      | multi_event_A | <Count_event_1> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter     | Value           |
      | multi_event_A | <Count_event_1> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "equal to" expected value in table displayed
      | Parameter  | Value           |
      | <DayType>  |                 |
      | <showType> | <Count_event_1> |

    Examples: 
      | showType    | Count_event_1 | DayType | Total |
      | Occurrences |            50 | Days    |    50 |
      | Uniques     |            50 | Days    |    50 |

  Scenario Outline: To verify the user attribute filter is getting applied post clicking apply button for Multi events
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>                  |
      | OF       | multi_event_A,multi_event_B |
      | OVER     | <DayType>                   |
      | SPLIT BY | -                           |
    Then I apply filter for events
      | Event | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_testing_1_test_value |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter     | Value           |
      | multi_event_A | <Count_event_1> |
      | multi_event_B | <Count_event_2> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter     | Value           |
      | multi_event_A | <Count_event_1> |
      | multi_event_B | <Count_event_2> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "equal to" expected value in table displayed
      | Parameter     | Value           |
      | <DayType>     |                 |
      | Total         | <Total>         |
      | multi_event_A | <Count_event_1> |
      | multi_event_B | <Count_event_2> |

    Examples: 
      | showType    | Count_event_1 | Count_event_2 | DayType | Total |
      | Occurrences |            50 |           100 | Days    |   150 |
      | Uniques     |            50 |            50 | Days    |   100 |

  Scenario Outline: To verify the user attribute filter is getting applied along with common filter [common event attribute/system attribute]
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>                  |
      | OF       | multi_event_A,multi_event_B |
      | OVER     | <DayType>                   |
      | SPLIT BY | -                           |
    Then I apply filter for events
      | Event | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_testing_1_test_value |
      | Event | Common Filter         | E_1                 | equal to | Testing                        |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter     | Value           |
      | multi_event_A | <Count_event_1> |
      | multi_event_B | <Count_event_2> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter     | Value           |
      | multi_event_A | <Count_event_1> |
      | multi_event_B | <Count_event_2> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "equal to" expected value in table displayed
      | Parameter     | Value           |
      | <DayType>     |                 |
      | Total         | <Total>         |
      | multi_event_A | <Count_event_1> |
      | multi_event_B | <Count_event_2> |

    Examples: 
      | showType    | Count_event_1 | Count_event_2 | DayType | Total |
      | Occurrences |            50 |           100 | Days    |   150 |
      | Uniques     |            50 |            50 | Days    |   100 |

  Scenario Outline: To verify the user attribute filter is getting applied along with common filter  [common event attribute/system attribute] and individual event block  [event attribute/system attribute]
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>                  |
      | OF       | multi_event_A,multi_event_B |
      | OVER     | <DayType>                   |
      | SPLIT BY | -                           |
    Then I apply filter for events
      | Event | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_testing_1_test_value |
      | Event | Common Filter         | E_1                 | equal to | Testing                        |
      | Event | multi_event_A         | E_2                 | equal to |                            123 |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter     | Value           |
      | multi_event_A | <Count_event_1> |
      | multi_event_B | <Count_event_2> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter     | Value           |
      | multi_event_A | <Count_event_1> |
      | multi_event_B | <Count_event_2> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "equal to" expected value in table displayed
      | Parameter     | Value           |
      | <DayType>     |                 |
      | Total         | <Total>         |
      | multi_event_A | <Count_event_1> |
      | multi_event_B | <Count_event_2> |

    Examples: 
      | showType    | Count_event_1 | Count_event_2 | DayType | Total |
      | Occurrences |            50 |           100 | Days    |   150 |
      | Uniques     |            50 |            50 | Days    |   100 |

  Scenario Outline: To verify the user attribute filter is getting applied along with individual event block [event attribute/system attribute] for Multi events
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>                  |
      | OF       | multi_event_A,multi_event_B |
      | OVER     | <DayType>                   |
      | SPLIT BY | -                           |
    Then I apply filter for events
      | Event | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_testing_1_test_value |
      | Event | multi_event_A         | E_2                 | equal to |                            123 |
      | Event | multi_event_B         | E_1                 | equal to | Testing                        |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter     | Value           |
      | multi_event_A | <Count_event_1> |
      | multi_event_B | <Count_event_2> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter     | Value           |
      | multi_event_A | <Count_event_1> |
      | multi_event_B | <Count_event_2> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "equal to" expected value in table displayed
      | Parameter     | Value           |
      | <DayType>     |                 |
      | Total         | <Total>         |
      | multi_event_A | <Count_event_1> |
      | multi_event_B | <Count_event_2> |

    Examples: 
      | showType    | Count_event_1 | Count_event_2 | DayType | Total |
      | Occurrences |            50 |           100 | Days    |   150 |
      | Uniques     |            50 |            50 | Days    |   100 |

  Scenario Outline: To verify the user attribute filter is getting applied along with individual event block [event attribute/system attribute] for Single event
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>    |
      | OF       | multi_event_A |
      | OVER     | <DayType>     |
      | SPLIT BY | -             |
    Then I apply filter for events
      | Event | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_testing_1_test_value |
      | Event | multi_event_A         | E_2                 | equal to |                            123 |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter     | Value           |
      | multi_event_A | <Count_event_1> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter     | Value           |
      | multi_event_A | <Count_event_1> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "equal to" expected value in table displayed
      | Parameter  | Value           |
      | <DayType>  |                 |
      | <showType> | <Count_event_1> |

    Examples: 
      | showType    | Count_event_1 | DayType | Total |
      | Occurrences |            50 | Days    |    50 |
      | Uniques     |            50 | Days    |    50 |

  Scenario Outline: To verify the user attribute filter along with segment filter for Single Event
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>    |
      | OF       | multi_event_A |
      | OVER     | <DayType>     |
      | SPLIT BY | -             |
    Then I apply filter for events
      | Event   | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_testing_1_test_value |
      | Segment | Include Segments      | UsersList           |          |                                |
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter     | Value           |
      | multi_event_A | <Count_event_1> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter     | Value           |
      | multi_event_A | <Count_event_1> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "equal to" expected value in table displayed
      | Parameter  | Value           |
      | <DayType>  |                 |
      | <showType> | <Count_event_1> |

    Examples: 
      | showType    | Count_event_1 | DayType | Total |
      | Occurrences |            50 | Days    |    50 |
      | Uniques     |            50 | Days    |    50 |

  Scenario Outline: To verify the user attribute filter along with segment filter for Multi Events
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>                  |
      | OF       | multi_event_A,multi_event_B |
      | OVER     | <DayType>                   |
      | SPLIT BY | -                           |
    Then I apply filter for events
      | Event   | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_testing_1_test_value |
      | Segment | Include Segments      | UsersList           |          |                                |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter     | Value           |
      | multi_event_A | <Count_event_1> |
      | multi_event_B | <Count_event_2> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter     | Value           |
      | multi_event_A | <Count_event_1> |
      | multi_event_B | <Count_event_2> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "equal to" expected value in table displayed
      | Parameter     | Value           |
      | <DayType>     |                 |
      | Total         | <Total>         |
      | multi_event_A | <Count_event_1> |
      | multi_event_B | <Count_event_2> |

    Examples: 
      | showType    | Count_event_1 | Count_event_2 | DayType | Total |
      | Occurrences |            50 |           100 | Days    |   150 |
      | Uniques     |            50 |            50 | Days    |   100 |

  Scenario Outline: To verify the user attribute filter with the split by system/custom attribute
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>  |
      | OF       | <eventName> |
      | OVER     | <DayType>   |
      | SPLIT BY | <splitBy>   |
    Then I apply filter for events
      | Event | User Attribute Filter | <userAttr> | equal to | <userAttrValue> |
    Then I validate the actual value to be "<operator>" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter   | Value   |
      | <parameter> | <value> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "<operator>" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter   | Value   |
      | <parameter> | <value> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "<operator>" expected value in table displayed
      | Parameter   | Value   |
      | <DayType>   |         |
      | <parameter> | <value> |

    Examples: 
      | showType    | value | DayType | Total | eventName     | splitBy      | userAttr            | userAttrValue                  | operator     | parameter |
      | Occurrences |   100 | Days    |   100 | multi_event_B | E_1          | user_attr_testing_1 | user_attr_testing_1_test_value | equal to     | Testing   |
      | Uniques     |    50 | Days    |    50 | multi_event_B | E_1          | user_attr_testing_1 | user_attr_testing_1_test_value | equal to     | Testing   |
      | Occurrences |     1 | Days    |     1 | Added To Cart | Browser Name | Age                 |                             42 | greater than | Chrome    |
      | Uniques     |     1 | Days    |     1 | Added To Cart | City         | user_attr_testing_1 | user_attr_value_1              | greater than | Ashburn   |

  Scenario Outline: To verify the user attribute filter with the split by user custom attribute
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>    |
      | OF       | multi_event_B |
      | OVER     | <DayType>     |
      | SPLIT BY | Age           |
    Then I apply filter for events
      | Event | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_testing_1_test_value |
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter | Value          |
      |        32 | <Count_attr_1> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter | Value          |
      |        32 | <Count_attr_1> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "equal to" expected value in table displayed
      | Parameter | Value          |
      | <DayType> |                |
      |        32 | <Count_attr_1> |

    Examples: 
      | showType    | Count_attr_1 | DayType | Total |
      | Occurrences |          100 | Days    |   100 |
      | Uniques     |           50 | Days    |    50 |

  Scenario Outline: To verify the user attribute filter with over by system/custom attribute for Single Event
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>  |
      | OF       | <eventName> |
      | OVER     | <overBy>    |
      | SPLIT BY | -           |
    Then I apply filter for events
      | Event | User Attribute Filter | <userAttr> | equal to | <userAttrValue> |
    Then I validate the actual value to be "<operator>" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter  | Value          |
      | <showType> | <Count_attr_1> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "<operator>" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter  | Value          |
      | <showType> | <Count_attr_1> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "<operator>" expected value in table displayed
      | Parameter   | Value          |
      | <parameter> | <DayType>      |
      | <showType>  | <Count_attr_1> |

    Examples: 
      | showType    | Count_attr_1 | DayType | Total | eventName     | overBy       | userAttr            | userAttrValue                  | operator     | parameter    |
      | Occurrences |           50 | Testing |    50 | multi_event_A | E_1          | user_attr_testing_1 | user_attr_testing_1_test_value | equal to     | E 1          |
      | Uniques     |           50 | Testing |    50 | multi_event_A | E_1          | user_attr_testing_1 | user_attr_testing_1_test_value | equal to     | E 1          |
      | Occurrences |            1 | Chrome  |     1 | Added To Cart | Browser Name | Age                 |                             42 | greater than | Browser Name |
      | Uniques     |            1 | Chrome  |     1 | Added To Cart | Browser Name | Age                 |                             42 | greater than | Browser Name |

  Scenario Outline: To verify the user attribute filter with over by user custom attribute for Single Event
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>    |
      | OF       | multi_event_B |
      | OVER     | Age           |
      | SPLIT BY | -             |
    Then I apply filter for events
      | Event | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_testing_1_test_value |
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter  | Value          |
      | <showType> | <Count_attr_1> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter  | Value          |
      | <showType> | <Count_attr_1> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "equal to" expected value in table displayed
      | Parameter  | Value          |
      | Age        | <DayType>      |
      | <showType> | <Count_attr_1> |

    Examples: 
      | showType    | Count_attr_1 | DayType | Total |
      | Occurrences |          100 |      32 |   100 |
      | Uniques     |           50 |      32 |    50 |

  Scenario Outline: To verify the user attribute filter with over by system/custom attribute for Multiple Events
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>                  |
      | OF       | multi_event_A,multi_event_B |
      | OVER     | E_1                         |
      | SPLIT BY | -                           |
    Then I apply filter for events
      | Event | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_testing_1_test_value |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter     | Value           |
      | multi_event_A | <Count_event_1> |
      | multi_event_B | <Count_event_2> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter     | Value           |
      | multi_event_A | <Count_event_1> |
      | multi_event_B | <Count_event_2> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "equal to" expected value in table displayed
      | Parameter     | Value           |
      | E 1           | <DayType>       |
      | Total         | <Total>         |
      | multi_event_A | <Count_event_1> |
      | multi_event_B | <Count_event_2> |

    Examples: 
      | showType    | Count_event_1 | Count_event_2 | DayType | Total |
      | Occurrences |            50 |           100 | Testing |   150 |
      | Uniques     |            50 |            50 | Testing |   100 |

  Scenario Outline: To verify the user attribute filter with over by user custom attribute for Multiple Events
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>                  |
      | OF       | multi_event_A,multi_event_B |
      | OVER     | Age                         |
      | SPLIT BY | -                           |
    Then I apply filter for events
      | Event | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_testing_1_test_value |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter     | Value           |
      | multi_event_A | <Count_event_1> |
      | multi_event_B | <Count_event_2> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter     | Value           |
      | multi_event_A | <Count_event_1> |
      | multi_event_B | <Count_event_2> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "equal to" expected value in table displayed
      | Parameter     | Value           |
      | Age           | <DayType>       |
      | Total         | <Total>         |
      | multi_event_A | <Count_event_1> |
      | multi_event_B | <Count_event_2> |

    Examples: 
      | showType    | Count_event_1 | Count_event_2 | DayType | Total |
      | Occurrences |            50 |           100 |      32 |   150 |
      | Uniques     |            50 |            50 |      32 |   100 |

  Scenario Outline: To verify the user attribute filter with segment filter, individaul filter [event attribute] and common filter [common event attribute]
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>                  |
      | OF       | multi_event_A,multi_event_B |
      | OVER     | <DayType>                   |
      | SPLIT BY | -                           |
    Then I apply filter for events
      | Event   | User Attribute Filter | user_attr_testing_1 | equal to | user_attr_testing_1_test_value |
      | Event   | Common Filter         | E_1                 | equal to | Testing                        |
      | Event   | multi_event_A         | E_2                 | equal to |                            123 |
      | Segment | Include Segments      | UsersList           |          |                                |
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter     | Value          |
      | multi_event_A | <Count_attr_1> |
      | multi_event_B | <Count_attr_2> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter     | Value          |
      | multi_event_A | <Count_attr_1> |
      | multi_event_B | <Count_attr_2> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "equal to" expected value in table displayed
      | Parameter     | Value          |
      | <DayType>     |                |
      | Total         | <Total>        |
      | multi_event_A | <Count_attr_1> |
      | multi_event_B | <Count_attr_2> |

    Examples: 
      | showType    | Count_attr_1 | Count_attr_2 | DayType | Total |
      | Occurrences |           50 |          100 | Days    |   150 |
      | Uniques     |           50 |           50 | Days    |   100 |

  Scenario Outline: To verify the user attribute filter is getting applied post clicking apply button for Single system event
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>      |
      | OF       | Session Started |
      | OVER     | <DayType>       |
      | SPLIT BY | -               |
    Then I apply filter for events
      | Event | User Attribute Filter | Age | equal to | 42 |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter       | Value          |
      | Session Started | <Count_attr_1> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter       | Value          |
      | Session Started | <Count_attr_1> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "greater than" expected value in table displayed
      | Parameter  | Value          |
      | <DayType>  |                |
      | <showType> | <Count_attr_1> |

    Examples: 
      | showType    | Count_attr_1 | DayType | Total |
      | Occurrences |            1 | Days    |     1 |
      | Uniques     |            1 | Days    |     1 |

  Scenario Outline: To verify the user attribute filter is getting applied post clicking apply button for Multiple system events
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>                          |
      | OF       | Session Started,Campaign Conversion |
      | OVER     | <DayType>                           |
      | SPLIT BY | -                                   |
    Then I apply filter for events
      | Event | User Attribute Filter | Age | equal to | 42 |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter           | Value           |
      | Session Started     | <Count_event_1> |
      | Campaign Conversion | <Count_event_2> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter           | Value           |
      | Session Started     | <Count_event_1> |
      | Campaign Conversion | <Count_event_2> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "greater than" expected value in table displayed
      | Parameter           | Value           |
      | <DayType>           |                 |
      | Total               | <Total>         |
      | Session Started     | <Count_event_1> |
      | Campaign Conversion | <Count_event_2> |

    Examples: 
      | showType    | Count_event_1 | Count_event_2 | DayType | Total |
      | Occurrences |             1 |             1 | Days    |     2 |
      | Uniques     |             1 |             1 | Days    |     1 |

  Scenario Outline: To verify the user attribute filter is getting applied along with common filter [system attribute] for System events
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>                          |
      | OF       | Session Started,Campaign Conversion |
      | OVER     | <DayType>                           |
      | SPLIT BY | -                                   |
    Then I apply filter for events
      | Event | User Attribute Filter | Age     | equal to |            42 |
      | Event | Common Filter         | Country | equal to | United States |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter           | Value           |
      | Session Started     | <Count_event_1> |
      | Campaign Conversion | <Count_event_2> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter           | Value           |
      | Session Started     | <Count_event_1> |
      | Campaign Conversion | <Count_event_2> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "greater than" expected value in table displayed
      | Parameter           | Value           |
      | <DayType>           |                 |
      | Total               | <Total>         |
      | Session Started     | <Count_event_1> |
      | Campaign Conversion | <Count_event_2> |

    Examples: 
      | showType    | Count_event_1 | Count_event_2 | DayType | Total |
      | Occurrences |             1 |             1 | Days    |     2 |
      | Uniques     |             1 |             1 | Days    |     1 |

  Scenario Outline: To verify the user attribute filter is getting applied along with common filter  [system attribute] and individual event block  [system attribute] for System events
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>                          |
      | OF       | Session Started,Campaign Conversion |
      | OVER     | <DayType>                           |
      | SPLIT BY | -                                   |
    Then I apply filter for events
      | Event | User Attribute Filter | Age          | equal to |            42 |
      | Event | Common Filter         | Country      | equal to | United States |
      | Event | Session Started       | Browser Name | equal to | Chrome        |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter           | Value           |
      | Session Started     | <Count_event_1> |
      | Campaign Conversion | <Count_event_2> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter           | Value           |
      | Session Started     | <Count_event_1> |
      | Campaign Conversion | <Count_event_2> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "greater than" expected value in table displayed
      | Parameter           | Value           |
      | <DayType>           |                 |
      | Total               | <Total>         |
      | Session Started     | <Count_event_1> |
      | Campaign Conversion | <Count_event_2> |

    Examples: 
      | showType    | Count_event_1 | Count_event_2 | DayType | Total |
      | Occurrences |             1 |             1 | Days    |     2 |
      | Uniques     |             1 |             1 | Days    |     1 |

  Scenario Outline: To verify the user attribute filter is getting applied along with common filter  [system attribute] and individual event block  [event attribute] for System events
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>                          |
      | OF       | Session Started,Campaign Conversion |
      | OVER     | <DayType>                           |
      | SPLIT BY | -                                   |
    Then I apply filter for events
      | Event | User Attribute Filter | Age            | equal to |            42 |
      | Event | Common Filter         | Country        | equal to | United States |
      | Event | Campaign Conversion   | revenue_amount | equal to |          39.8 |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter           | Value           |
      | Session Started     | <Count_event_1> |
      | Campaign Conversion | <Count_event_2> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter           | Value           |
      | Session Started     | <Count_event_1> |
      | Campaign Conversion | <Count_event_2> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "greater than" expected value in table displayed
      | Parameter           | Value           |
      | <DayType>           |                 |
      | Total               | <Total>         |
      | Session Started     | <Count_event_1> |
      | Campaign Conversion | <Count_event_2> |

    Examples: 
      | showType    | Count_event_1 | Count_event_2 | DayType | Total |
      | Occurrences |             1 |             1 | Days    |     2 |
      | Uniques     |             1 |             1 | Days    |     1 |

  Scenario Outline: To verify the user attribute filter is getting applied along with individual event block [event attribute] for a Single System event
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>          |
      | OF       | Campaign Conversion |
      | OVER     | <DayType>           |
      | SPLIT BY | -                   |
    Then I apply filter for events
      | Event | User Attribute Filter | Age            | equal to |   42 |
      | Event | Campaign Conversion   | revenue_amount | equal to | 39.8 |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter           | Value           |
      | Campaign Conversion | <Count_event_1> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter           | Value           |
      | Campaign Conversion | <Count_event_1> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "greater than" expected value in table displayed
      | Parameter  | Value           |
      | <DayType>  |                 |
      | <showType> | <Count_event_1> |

    Examples: 
      | showType    | Count_event_1 | DayType | Total |
      | Occurrences |             1 | Days    |     1 |
      | Uniques     |             1 | Days    |     1 |

  Scenario Outline: To verify that if multiple events are selected (i:e More than 1) then the split by option and custom alert should be greyed out
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>                                              |
      | OF       | multi_event_A,multi_event_B,multi_event_C,multi_event_D |
      | OVER     | Days                                                    |
      | SPLIT BY | -                                                       |
    Then I verify the selected events get reflected in graph
    Then I verify split by option and custom alert should be greyed out for multi events

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify that segment filter will be visible in filter icon and verify there is option to include multiple lists and exclude multiple lists .
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>  |
      | OF       | multi_event |
      | OVER     | Days        |
      | SPLIT BY | -           |
    Then I apply filter for events
      | Segment | Include Segments | IncludeList                   |
      | Segment | Include Segments | Include3UsersList             |
      | Segment | Exclude Segments | ExcludeList                   |
      | Segment | Exclude Segments | StaticListStatsValidationUser |

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify if any new event is added, and that does not have the common filter [common event attribute] then the common filter will be removed.
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>                                                            |
      | OF       | multi_event_A,multi_event_B,multi_event_C,multi_event_D,multi_event_E |
      | OVER     | Days                                                                  |
      | SPLIT BY | -                                                                     |
    Then I check common filter does not have common event
      | Common Filter | E_1 |

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify there is AND operator between common filter block.
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>                                |
      | OF       | multi_event_A,multi_event_B,multi_event_C |
      | OVER     | Days                                      |
      | SPLIT BY | -                                         |
    Then I apply filter for events
      | Event | Common Filter | E_1        | equal to       | Testing |
      | Event | Common Filter | Add Filter | with condition | AND     |
      | Event | Common Filter | E_2        | equal to       |     123 |

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify there is AND operator between event attribute block.
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>                                |
      | OF       | multi_event_A,multi_event_B,multi_event_C |
      | OVER     | Days                                      |
      | SPLIT BY | -                                         |
    Then I apply filter for events
      | Event | multi_event_A | E_1        | equal to       | Testing |
      | Event | multi_event_A | Add Filter | with condition | AND     |
      | Event | multi_event_A | E_2        | equal to       |     123 |

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify there is OR operator between common filter block.
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>                                |
      | OF       | multi_event_A,multi_event_B,multi_event_C |
      | OVER     | Days                                      |
      | SPLIT BY | -                                         |
    Then I apply filter for events
      | Event | Common Filter | E_1        | equal to       | Testing |
      | Event | Common Filter | Add Filter | with condition | OR      |
      | Event | Common Filter | E_2        | equal to       |     123 |

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify there is OR operator between event attribute block.
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>                                |
      | OF       | multi_event_A,multi_event_B,multi_event_C |
      | OVER     | Days                                      |
      | SPLIT BY | -                                         |
    Then I apply filter for events
      | Event | multi_event_A | E_1        | equal to       | Testing |
      | Event | multi_event_A | Add Filter | with condition | OR      |
      | Event | multi_event_A | E_2        | equal to       |     123 |

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |

  Scenario Outline: To verify the <showType> of <eventType> event with system attribute event filter and split by <attrType> attribute
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>  |
      | OF       | <eventName> |
      | OVER     | <DayType>   |
      | SPLIT BY | <splitBy>   |
    Then I apply filter for events
      | Event | <eventName> | <sysAttr> | <operator> | <sysAttrCondition> |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter   | Value   |
      | <parameter> | <value> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "greater than" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter   | Value   |
      | <parameter> | <value> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "greater than" expected value in table displayed
      | Parameter   | Value   |
      | <DayType>   |         |
      | <parameter> | <value> |

    Examples: 
      | showType    | value | DayType | Total | eventName           | sysAttr      | operator    | sysAttrCondition | splitBy             | parameter         | eventType | attrType    |
      | Occurrences |     1 | Days    |     1 | Session Started     | Event Time   | within      | date             | Browser Name        | Chrome            | system    | system      |
      | Uniques     |     1 | Days    |     1 | Session Started     | City         | equal to    | Ashburn          | Browser Name        | Chrome            | system    | system      |
      | Occurrences |     1 | Days    |     1 | Added To Cart       | Event Time   | within      | date             | Browser Name        | Chrome            | custom    | system      |
      | Uniques     |     1 | Days    |     1 | Added To Cart       | Country      | equal to    | United States    | Browser Name        | Chrome            | custom    | system      |
      | Occurrences |     1 | Days    |     1 | Added To Cart       | Browser Name | equal to    | Chrome           | Price               |              39.8 | custom    | custom      |
      | Uniques     |     1 | Days    |     1 | Added To Cart       | Browser Name | equal to    | Chrome           | Product ID          | AXK1337           | custom    | custom      |
      | Occurrences |     1 | Days    |     1 | Campaign Conversion | City         | ends with   | Ashburn          | revenue_amount      |              39.8 | system    | custom      |
      | Uniques     |     1 | Days    |     1 | Campaign Conversion | OS Name      | starts with | Windows          | amplified           | false             | system    | custom      |
      | Occurrences |     1 | Days    |     1 | Added To Cart       | Channel      | equal to    | direct           | Age                 |                42 | custom    | user custom |
      | Uniques     |     1 | Days    |     1 | Added To Cart       | Platform     | equal to    |                1 | user_attr_testing_1 | user_attr_value_1 | custom    | user custom |
      | Occurrences |     1 | Days    |     1 | Session Started     | Country      | equal to    | United States    | Age                 |                42 | system    | user custom |
      | Uniques     |     1 | Days    |     1 | Session Started     | Event Time   | within      | date             | Age                 |                42 | system    | user custom |