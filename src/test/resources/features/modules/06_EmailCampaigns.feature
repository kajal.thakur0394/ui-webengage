@EmailCampaigns @Regression
Feature: Email Campaign Feature

  Scenario Outline: To verify previously created Email campaigns are available in Email campaign > List of campaigns page
    Given I navigate to "Email" page via "Channels"
    Then I add "<CampaignName>" to searchbox
    And I verify that "<CampaignName>" and "<CampaignType>" exist

    Examples: 
      | CampaignName                            | CampaignType  |
      | Test One-time Email Campaign exist      | One-time      |
      | Test Triggered Email Campaign exist     | Triggered     |
      | Test Recurring Email Campaign exist     | Recurring     |
      | Test Transactional Email Campaign exist | Transactional |

  @Smoke
  Scenario: To verify Live Segment via Segment Editor is getting created when One-time Email Campaign type is selected
    Given I navigate to "Email" page via "Channels"
    And I select "One-time" Campaign Type and enters "LiveSegment via Segment Editor-One-time Email Campaign" Campaign Name
    And I select New Segment Creation icon
    And I add "Segment Name" as "Email: User Condition > User IDs"
    And Pass following details in user card
      | Visitor Type | All Users   |
      | User IDs     | ends with>P |
    And I Save and verify the created "Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select the ESP with name "Dummy ESP" and I enter the following details in the message ESP
      | From name  | OneTime Automation                           |
      | From email | info@email.dvmitsolutions.com                |
      | Subject    | Subject: OneTime Email Campaign              |
      | Body       | Rich Text>Hi, I am a OneTime Email Campaign. |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    And verify via "fetchEmailCampaign" API whether following "Segments" have been attached to campaign just created
      | Email: User Condition > User IDs |
    Then I navigate to "Live Segments" page via "Segments"
    And I verify "Email: User Condition > User IDs" segment is present
    Given I navigate to "Email" page via "Channels"
    Then check overview page is loaded for "LiveSegment via Segment Editor-One-time Email Campaign" campaign
    Given I navigate to "Email" page via "Channels"
    Then I delete the Campaign with Name "LiveSegment via Segment Editor-One-time Email Campaign"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "Email: User Condition > User IDs"

  Scenario: To verify Live Segment is getting created when Triggered Email Campaign type is selected
    Given I navigate to "Email" page via "Channels"
    And I select "Triggered" Campaign Type and enters "LiveSegment via Segment Editor-Triggered Email Campaign" Campaign Name
    And I select New Segment Creation icon
    And I add "Segment Name" as "Email: Behavorial Condition > more than 1 condition of Users who did these Events"
    And Pass following details in Behavioural card
      |     | Users who DID these events : | System>User Login    |
      | And | Users who DID these events : | System>App Installed |
    And I Save and verify the created "Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | UPON OCCURRENCE OF | TriggerEmailEvent |  |  |
    And I select the ESP with name "Dummy ESP" and I enter the following details in the message ESP
      | From name  | Trigerred Automation                           |
      | From email | info@email.dvmitsolutions.com                  |
      | Subject    | Subject: Trigerred Email Campaign              |
      | Body       | Rich Text>Hi, I am a Trigerred Email Campaign. |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    And verify via "fetchEmailCampaign" API whether following "Segments" have been attached to campaign just created
      | Email: Behavorial Condition > more than 1 condition of Users who did these Events |
    Then I navigate to "Live Segments" page via "Segments"
    And I verify "Email: Behavorial Condition > more than 1 condition of Users who did these Events" segment is present
    Given I navigate to "Email" page via "Channels"
    Then check overview page is loaded for "LiveSegment via Segment Editor-Triggered Email Campaign" campaign
    Given I navigate to "Email" page via "Channels"
    Then I delete the Campaign with Name "LiveSegment via Segment Editor-Triggered Email Campaign"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "Email: Behavorial Condition > more than 1 condition of Users who did these Events"

  Scenario: To verify Live Segment via Segment Editor is getting created when Recurring Email Campaign type is selected
    Given I navigate to "Email" page via "Channels"
    And I select "Recurring" Campaign Type and enters "LiveSegment via Segment Editor-Recurring Email Campaign" Campaign Name
    And I select New Segment Creation icon
    And I add "Segment Name" as "Email: Technology Condition > iOS"
    And Pass following details for "iOS" in Technology card
      | Total Time Spent (in seconds) | greater than>100 |
    And I Save and verify the created "Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select the ESP with name "Dummy ESP" and I enter the following details in the message ESP
      | From name  | Recurring Automation                           |
      | From email | info@email.dvmitsolutions.com                  |
      | Subject    | Subject: Recurring Email Campaign              |
      | Body       | Rich Text>Hi, I am a Recurring Email Campaign. |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    And verify via "fetchEmailCampaign" API whether following "Segments" have been attached to campaign just created
      | Email: Technology Condition > iOS |
    Then I navigate to "Live Segments" page via "Segments"
    And I verify "Email: Technology Condition > iOS" segment is present
    Given I navigate to "Email" page via "Channels"
    Then check overview page is loaded for "LiveSegment via Segment Editor-Recurring Email Campaign" campaign
    Given I navigate to "Email" page via "Channels"
    Then I delete the Campaign with Name "LiveSegment via Segment Editor-Recurring Email Campaign"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "Email: Technology Condition > iOS"

  @Smoke
  Scenario: To verify Email campaign creation using Inclusion/Exclusion of segment feature
    Given I navigate to "Email" page via "Channels"
    And I select "One-time" Campaign Type and enters "LiveSegment with Inclusion/Exclusion of segment Email Campaign" Campaign Name
    Then I fill the details in Audience tab as below
      | Audience Type | multiple segments |
    Then I select "Users in ANY of these segments" as "Send To" Inclusion Segment
    And I add "Segment Name" as "Email: User Condition > User ID ends with P"
    And Pass following details in user card
      | Visitor Type | All Users   |
      | User IDs     | ends with>P |
    And Save the Segment
    Then I select "Users in ANY of these segments" as "EXCLUDE" Exclusion Segment
    And I add "Segment Name" as "Email: User Condition > Reachability on Whatsapp"
    And Pass following details in user card And I Save the "segment"
      | Visitor Type | All Users             |
      | Reachability | Reachable on-WhatsApp |
    And I save the Audience details
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select the ESP with name "Dummy ESP" and I enter the following details in the message ESP
      | From name  | OneTime Automation                           |
      | From email | info@email.dvmitsolutions.com                |
      | Subject    | Subject: OneTime Email Campaign              |
      | Body       | Rich Text>Hi, I am a OneTime Email Campaign. |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    And verify via "fetchEmailCampaign" API whether following "Segment" have been attached to campaign just created
      | Email: User Condition > User ID ends with P      |
      | Email: User Condition > Reachability on Whatsapp |
    Then I navigate to "Live Segments" page via "Segments"
    And I verify following Segments are present
      | Email: User Condition > User ID ends with P      |
      | Email: User Condition > Reachability on Whatsapp |
    Given I navigate to "Email" page via "Channels"
    Then check overview page is loaded for "LiveSegment with Inclusion/Exclusion of segment Email Campaign" campaign
    Given I navigate to "Email" page via "Channels"
    Then I delete the Campaign with Name "LiveSegment with Inclusion/Exclusion of segment Email Campaign"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete the following mentioned "segments"
      | Email: User Condition > User ID ends with P      |
      | Email: User Condition > Reachability on Whatsapp |

  @Smoke
  Scenario: To verify One-time Email campaign when Delivery Time:Later and Intelligently is selected
    Given I navigate to "Email" page via "Channels"
    And I select "One-time" Campaign Type and enters "One-time Email Campaign with Intelligently Delivery Time" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below when "One-time" Campaign and "Email" Channel is selected
      | DELIVERY SCHEDULE | later             |  |  |
      | DELIVERY TIME     | sendIntelligently |  |  |
    And I select the ESP with name "Dummy ESP" and I enter the following details in the message ESP
      | From name  | OneTime Automation                           |
      | From email | info@email.dvmitsolutions.com                |
      | Subject    | Subject: OneTime Email Campaign              |
      | Body       | Rich Text>Hi, I am a OneTime Email Campaign. |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "One-time Email Campaign with Intelligently Delivery Time" campaign
    Given I navigate to "Email" page via "Channels"
    Then I add "One-time Email Campaign with Intelligently Delivery Time" to searchbox
    And I verify following details when "One-time" Campaign having "One-time Email Campaign with Intelligently Delivery Time" Campaign Name and "Email" Channel is selected
      | Delivery Time: Send Intelligently |
    Given I navigate to "Email" page via "Channels"
    Then I delete the Campaign with Name "One-time Email Campaign with Intelligently Delivery Time"

  @Smoke
  Scenario: To verify One-time Email campaign when Delivery Time:Later and Specific Time is selected
    Given I navigate to "Email" page via "Channels"
    And I select "One-time" Campaign Type and enters "One-time Email Campaign with Specific Time Delivery" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below when "One-time" Campaign and "Email" Channel is selected
      | DELIVERY SCHEDULE | later              |  |  |
      | DELIVERY TIME     | sendAtSpecificTime |  |  |
    And I select the ESP with name "Dummy ESP" and I enter the following details in the message ESP
      | From name  | OneTime Automation                           |
      | From email | info@email.dvmitsolutions.com                |
      | Subject    | Subject: OneTime Email Campaign              |
      | Body       | Rich Text>Hi, I am a OneTime Email Campaign. |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "One-time Email Campaign with Specific Time Delivery" campaign
    Given I navigate to "Email" page via "Channels"
    Then I add "One-time Email Campaign with Specific Time Delivery" to searchbox
    And I verify following details when "One-time" Campaign having "One-time Email Campaign with Specific Time Delivery" Campaign Name and "Email" Channel is selected
      | Delivery Time: |
    Given I navigate to "Email" page via "Channels"
    Then I delete the Campaign with Name "One-time Email Campaign with Specific Time Delivery"

  Scenario: To verify Triggered Email campaign when Start Date: Now and End Date: Till is selected
    Given I navigate to "Email" page via "Channels"
    And I select "Triggered" Campaign Type and enters "Triggered Email Campaign with Start Date:Now and End Date:Till" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | UPON OCCURRENCE OF | TriggerEmailEvent |  |  |
      | START DATE         | now               |  |  |
      | END DATE           | till              |  |  |
    And I select the ESP with name "Dummy ESP" and I enter the following details in the message ESP
      | From name  | Trigerred Automation                           |
      | From email | info@email.dvmitsolutions.com                  |
      | Subject    | Subject: Trigerred Email Campaign              |
      | Body       | Rich Text>Hi, I am a Trigerred Email Campaign. |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Triggered Email Campaign with Start Date:Now and End Date:Till" campaign
    Given I navigate to "Email" page via "Channels"
    Then I add "Triggered Email Campaign with Start Date:Now and End Date:Till" to searchbox
    And I verify following details for "Triggered Email Campaign with Start Date:Now and End Date:Till" campaign
      | Start date: |
      | End date:   |
    Given I navigate to "Email" page via "Channels"
    Then I delete the Campaign with Name "Triggered Email Campaign with Start Date:Now and End Date:Till"

  Scenario: To verify Triggered Email campaign when Start Date: Later and End Date: Till is selected
    Given I navigate to "Email" page via "Channels"
    And I select "Triggered" Campaign Type and enters "Triggered Email Campaign with Start Date:Later and End Date:Till" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | UPON OCCURRENCE OF | TriggerEmailEvent |  |  |
      | START DATE         | later             |  |  |
      | END DATE           | till              |  |  |
    And I select the ESP with name "Dummy ESP" and I enter the following details in the message ESP
      | From name  | Trigerred Automation                           |
      | From email | info@email.dvmitsolutions.com                  |
      | Subject    | Subject: Trigerred Email Campaign              |
      | Body       | Rich Text>Hi, I am a Trigerred Email Campaign. |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Triggered Email Campaign with Start Date:Later and End Date:Till" campaign
    Given I navigate to "Email" page via "Channels"
    Then I add "Triggered Email Campaign with Start Date:Later and End Date:Till" to searchbox
    And I verify following details for "Triggered Email Campaign with Start Date:Later and End Date:Till" campaign
      | Start date: |
      | End date:   |
    Given I navigate to "Email" page via "Channels"
    Then I delete the Campaign with Name "Triggered Email Campaign with Start Date:Later and End Date:Till"

  Scenario: To verify Triggered Email campaign when Start Date: Later and End Date: Never is selected
    Given I navigate to "Email" page via "Channels"
    And I select "Triggered" Campaign Type and enters "Triggered Email Campaign with Start Date:Later and End Date:Never" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | UPON OCCURRENCE OF | TriggerEmailEvent |  |  |
      | START DATE         | later             |  |  |
      | END DATE           | never             |  |  |
    And I select the ESP with name "Dummy ESP" and I enter the following details in the message ESP
      | From name  | Trigerred Automation                           |
      | From email | info@email.dvmitsolutions.com                  |
      | Subject    | Subject: Trigerred Email Campaign              |
      | Body       | Rich Text>Hi, I am a Trigerred Email Campaign. |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Triggered Email Campaign with Start Date:Later and End Date:Never" campaign
    Given I navigate to "Email" page via "Channels"
    Then I add "Triggered Email Campaign with Start Date:Later and End Date:Never" to searchbox
    And I verify following details for "Triggered Email Campaign with Start Date:Later and End Date:Never" campaign
      | Start date: |
      | End date:   |
    Given I navigate to "Email" page via "Channels"
    Then I delete the Campaign with Name "Triggered Email Campaign with Start Date:Later and End Date:Never"

  Scenario: To verify Recurring Email campaign when Delivery Schedule: 1/Day and End Date: Never is selected
    Given I navigate to "Email" page via "Channels"
    And I select "Recurring" Campaign Type and enters "Recurring Email Campaign with Delivery Schedule:1/Day and End Date:Never" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | DELIVERY SCHEDULE | Day   |  |  |
      | END DATE          | never |  |  |
    And I select the ESP with name "Dummy ESP" and I enter the following details in the message ESP
      | From name  | Recurring Automation                           |
      | From email | info@email.dvmitsolutions.com                  |
      | Subject    | Subject: Recurring Email Campaign              |
      | Body       | Rich Text>Hi, I am a Recurring Email Campaign. |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Recurring Email Campaign with Delivery Schedule:1/Day and End Date:Never" campaign
    Given I navigate to "Email" page via "Channels"
    Then I add "Recurring Email Campaign with Delivery Schedule:1/Day and End Date:Never" to searchbox
    And I verify following details for "Recurring Email Campaign with Delivery Schedule:1/Day and End Date:Never" campaign
      | Delivery Schedule |
      | End date:         |
    Given I navigate to "Email" page via "Channels"
    Then I delete the Campaign with Name "Recurring Email Campaign with Delivery Schedule:1/Day and End Date:Never"

  @Smoke
  Scenario: To verify Recurring Email campaign when Delivery Schedule: 1/Day and End Date: Till is selected
    Given I navigate to "Email" page via "Channels"
    And I select "Recurring" Campaign Type and enters "Recurring Email Campaign with Delivery Schedule:1/Day and End Date:Till" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | DELIVERY SCHEDULE | Day  |  |  |
      | END DATE          | till |  |  |
    And I select the ESP with name "Dummy ESP" and I enter the following details in the message ESP
      | From name  | Recurring Automation                           |
      | From email | info@email.dvmitsolutions.com                  |
      | Subject    | Subject: Recurring Email Campaign              |
      | Body       | Rich Text>Hi, I am a Recurring Email Campaign. |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Recurring Email Campaign with Delivery Schedule:1/Day and End Date:Till" campaign
    Given I navigate to "Email" page via "Channels"
    Then I add "Recurring Email Campaign with Delivery Schedule:1/Day and End Date:Till" to searchbox
    And I verify following details for "Recurring Email Campaign with Delivery Schedule:1/Day and End Date:Till" campaign
      | End date:         |
      | Delivery Schedule |
    Given I navigate to "Email" page via "Channels"
    Then I delete the Campaign with Name "Recurring Email Campaign with Delivery Schedule:1/Day and End Date:Till"

  @End-to-End @Distributed
  Scenario: To verify Email is send to users and stats are showing properly when Transactional Email Campaign type is selected
    Given I navigate to "Email" page via "Channels"
    And I select "Transactional" Campaign Type and enters "Transactional Email Campaign" Campaign Name
    Then I fill the details in Audience tab as below
      |  |  |
    And I select the ESP with name "Email Campaign SP" and I enter the following details in the message ESP
      | From name  | Transactional Automation                           |
      | From email | info@email.dvmitsolutions.com                      |
      | Subject    | Subject: Transactional Email Campaign              |
      | Body       | Rich Text>Hi, I am a Transactional Email Campaign. |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then wait for "fetchEmailCampaign" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    Then check overview page is loaded for "Transactional Email Campaign" campaign
    Given I navigate to "Email" page via "Channels"
    When I hit the POST method using static body for "transactionalCampaign" api from "EmailCampaigns" sheet with ref id "transactionalEmail"

  Scenario: To verify on triggering conversion event, conversion is recorded for Email campaign
    Given I navigate to "Email" page via "Channels"
    And I select "One-time" Campaign Type and enters "Conversion Tracking-One-time Email Campaign" Campaign Name
    Then select Segment having name as "StatsValidationUser Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select the ESP with name "Email Campaign SP" and I enter the following details in the message ESP
      | From name  | OneTimeCT Automation                           |
      | From email | info@email.dvmitsolutions.com                  |
      | Subject    | Subject: OneTimeCT Email Campaign              |
      | Body       | Rich Text>Hi, I am a OneTimeCT Email Campaign. |
    Then I fill the details in Conversion Tracking tab as below
      | Conversion Tracking | On                |
      | CONVERSION EVENT    | TriggerEmailEvent |
      | CONVERSION DEADLINE | 7-Days            |
    And I skip the Campaign Test
    And I launch the Campaign
    Then wait for "fetchEmailCampaign" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    Then check overview page is loaded for "Conversion Tracking-One-time Email Campaign" campaign
    Given I navigate to "Email" page via "Channels"
    Then I delete the Campaign with Name "Conversion Tracking-One-time Email Campaign"

  @End-to-End @Distributed
  Scenario: To verify Stats are showing properly when One-time Email Campaign type is selected
    Given I navigate to "Email" page via "Channels"
    And I select "One-time" Campaign Type and enters "Sent/Delivered Status-One-time Email Campaign" Campaign Name
    Then select Segment having name as "StatsValidationUser Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | QUEUEING   | false |  |  |
      | FC         | false |  |  |
      | DND        | false |  |  |
      | THROTTLING | false |  |  |
    And I select the ESP with name "Email Campaign SP" and I enter the following details in the message ESP
      | From name  | OneTimeStats Automation                           |
      | From email | info@email.dvmitsolutions.com                     |
      | Subject    | Subject: OneTimeStats Email Campaign              |
      | Body       | Rich Text>Hi, I am a OneTimeStats Email Campaign. |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Sent/Delivered Status-One-time Email Campaign" campaign

  @End-to-End @Distributed
  Scenario: To verify Stats are showing properly when Triggered Email Campaign type is selected
    Given I navigate to "Email" page via "Channels"
    And I select "Triggered" Campaign Type and enters "Sent/Delivered Status-Triggered Email Campaign" Campaign Name
    Then select Segment having name as "StatsValidationUser Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | UPON OCCURRENCE OF | TriggerEmailEvent |  |  |
      | QUEUEING           | false             |  |  |
      | FC                 | false             |  |  |
      | DND                | false             |  |  |
      | THROTTLING         | false             |  |  |
      | END DATE           | after 3 hours     |  |  |
    And I select the ESP with name "Email Campaign SP" and I enter the following details in the message ESP
      | From name  | TrigerredStats Automation                           |
      | From email | info@email.dvmitsolutions.com                       |
      | Subject    | Subject: TrigerredStats Email Campaign              |
      | Body       | Rich Text>Hi, I am a TrigerredStats Email Campaign. |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then wait for "fetchEmailCampaign" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    Then check overview page is loaded for "Sent/Delivered Status-Triggered Email Campaign" campaign
    Given I navigate to "Email" page via "Channels"
    And hit "performEvent" API from "PerformEvents" sheet to trigger Event with reference id "triggerEmailEvent"

  @End-to-End @Distributed
  Scenario: To verify Stats are showing properly when Recurring Email Campaign type is selected
    Given I navigate to "Email" page via "Channels"
    And I select "Recurring" Campaign Type and enters "Sent/Delivered Status-Recurring Email Campaign" Campaign Name
    Then select Segment having name as "StatsValidationUser Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | DELIVERY SCHEDULE | Day           |  |  |
      | QUEUEING          | false         |  |  |
      | FC                | false         |  |  |
      | DND               | false         |  |  |
      | THROTTLING        | false         |  |  |
      | END DATE          | after 3 hours |  |  |
    Then I adjust the time for Recurring Campaign by checking system time
    And I select the ESP with name "Email Campaign SP" and I enter the following details in the message ESP
      | From name  | RecurringStats Automation                           |
      | From email | info@email.dvmitsolutions.com                       |
      | Subject    | Subject: RecurringStats Email Campaign              |
      | Body       | Rich Text>Hi, I am a RecurringStats Email Campaign. |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Sent/Delivered Status-Recurring Email Campaign" campaign

  @Smoke
  Scenario: To verify One-time Email Campaign is sent when Drag and Drop editor is selected
    Given I navigate to "Email" page via "Channels"
    And I select "One-time" Campaign Type and enters "One-time Email Campaign with Drag and Drop body" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select the ESP with name "Dummy ESP" and I enter the following details in the message ESP
      | From name  | OneTime Automation                                          |
      | From email | info@email.dvmitsolutions.com                               |
      | Subject    | Subject: OneTime Email Campaign                             |
      | Body       | Drag & Drop Editor >Hi, I am a OneTime Drag Email Campaign. |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "One-time Email Campaign with Drag and Drop body" campaign
    Given I navigate to "Email" page via "Channels"
    Then I add "One-time Email Campaign with Drag and Drop body" to searchbox
    Then I refresh the page
    And I verify template is attached for status as "Running" having "One-time Email Campaign with Drag and Drop body" as campaign name
    Given I navigate to "Email" page via "Channels"
    Then I delete the Campaign with Name "One-time Email Campaign with Drag and Drop body"

  Scenario: Verify existing tags is attached to Email Campaign
    Given I navigate to "Email" page via "Channels"
    And I search for "Email Campaign: Tag" campaign
    Then I verify "campaigntag" tag is attached to the "Email Campaign: Tag" campaign

  Scenario: Verify new tag is created from Email Channel and gets attached to newly created Email Campaign
    Given I navigate to "Email" page via "Channels"
    And I select "One-time" Campaign Type and enters "Tag: Email Campaign" Campaign Name
    And create new tag from with name as "email-tag" for campaign
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select the ESP with name "Dummy ESP" and I enter the following details in the message ESP
      | From name  | Tagging Automation                           |
      | From email | info@email.dvmitsolutions.com                |
      | Subject    | Subject: Tagging Email Campaign              |
      | Body       | Rich Text>Hi, I am a Tagging Email Campaign. |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Tag: Email Campaign" campaign
    Given I navigate to "Email" page via "Channels"
    Then I verify "email-tag" tag is attached to the "Tag: Email Campaign" campaign
    Given I navigate to "Email" page via "Channels"
    And I search for "Tag: Email Campaign" campaign
    Then delete the tag with name as "email-tag" from campaign "Tag: Email Campaign"
    Then I delete the Campaign with Name "Tag: Email Campaign"

  @Smoke
  Scenario: To verify test variation/campaign is working fine by creating new Test Segment for Email
    Given I navigate to "Email" page via "Channels"
    And I select "One-time" Campaign Type and enters "Test Page Validation for Email" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select the ESP with name "Email Campaign SP" and I enter the following details in the message ESP
      | From name  | OneTimeStats Automation                           |
      | From email | info@email.dvmitsolutions.com                     |
      | Subject    | Subject: OneTimeStats Email Campaign              |
      | Body       | Rich Text>Hi, I am a OneTimeStats Email Campaign. |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    Then "create" test campaign with following parameters
      | Segment Name | StatsValidationUserSegment |
      | Filters      | User ID                    |
      | values       | StatsValidationUser        |
    And test the "Email" Campaign for segment "TEST-StatsValidationUserSegment" and expect the status to be "SENT"
    Then delete Test Segment with name as "TEST-StatsValidationUserSegment"
    And I launch the Campaign
    Then check overview page is loaded for "Test Page Validation for Email" campaign
    Given I navigate to "Email" page via "Channels"
    Then I delete the Campaign with Name "Test Page Validation for Email"

  Scenario: To verify overview page is loaded when campaign status is paused for Triggerred Email
    Given I navigate to "Email" page via "Channels"
    And I select "Triggered" Campaign Type and enters "Email-Triggerred-Overview Page Validation for Paused status" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | UPON OCCURRENCE OF | TriggerEmailEvent |  |  |
      | START DATE         | now               |  |  |
      | END DATE           | till              |  |  |
    And I select the ESP with name "Dummy ESP" and I enter the following details in the message ESP
      | From name  | Trigerred Automation                           |
      | From email | info@email.dvmitsolutions.com                  |
      | Subject    | Subject: Trigerred Email Campaign              |
      | Body       | Rich Text>Hi, I am a Trigerred Email Campaign. |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I pause the campaign with "email-deactivate" api
    Then check overview page is loaded for "Email-Triggerred-Overview Page Validation for Paused status" campaign
    Given I navigate to "Email" page via "Channels"
    Then I delete the Campaign with Name "Email-Triggerred-Overview Page Validation for Paused status"

  Scenario: To verify overview page is loaded when campaign status is in draft state for OneTime Email
    Given I navigate to "Email" page via "Channels"
    And I select "One-time" Campaign Type and enters "Email-OneTime-Overview Page Validation for draft status" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select the ESP with name "Dummy ESP" and I enter the following details in the message ESP
      | From name  | OneTime Automation                                          |
      | From email | info@email.dvmitsolutions.com                               |
      | Subject    | Subject: OneTime Email Campaign                             |
      | Body       | Drag & Drop Editor >Hi, I am a OneTime Drag Email Campaign. |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    Given I navigate to "Email" page via "Channels"
    Then check overview page is loaded for "Email-OneTime-Overview Page Validation for draft status" campaign
    Given I navigate to "Email" page via "Channels"
    Then I add "Email-OneTime-Overview Page Validation for draft status" to searchbox
    And I verify template is attached for status as "Draft" having "Email-OneTime-Overview Page Validation for draft status" as campaign name
    Given I navigate to "Email" page via "Channels"
    Then I delete the Campaign with Name "Email-OneTime-Overview Page Validation for draft status"

  @Smoke
  Scenario: To verify AMP toggle for sparkpost ESP is visible on message tab and for non spark post esp its hidden
    Given I navigate to "Email" page via "Channels"
    And I select "One-time" Campaign Type and enters "Email-OneTime-AMP Toggle" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "AmpVariation" template from "EmailTemplates" folder using "Message" section
    Then I select tab "Message" in campaign crud page
    Then I select the  Service provider "ESP" with name "Journey ESP"
    Then I click button "Confirm"
    Then I verify "AMP Email" toggle button is "not visible"
    Given I navigate to "Email" page via "Channels"
    Then I delete the Campaign with Name "Email-OneTime-AMP Toggle"

  Scenario: To verify emoji is present in fields when One-time Email is selected having Rich Text Layout
    Given I navigate to "Email" page via "Channels"
    And I select "One-time" Campaign Type and enters "Email-OneTime-Emoji" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select the ESP with name "Journey ESP" and I enter the following details in the message ESP
      | From name  | OneTime Automation                           |
      | From email | info@email.dvmitsolutions.com                |
      | Subject    | Subject: OneTime Email Campaign              |
      | Body       | Rich Text>Hi, I am a OneTime Email Campaign. |
    Then I select tab "Message" in campaign crud page
    Then verify emojis are present in the following fields
      | Subject |
      | Body    |
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Email-OneTime-Emoji" campaign
    Given I navigate to "Email" page via "Channels"
    Then I delete the Campaign with Name "Email-OneTime-Emoji"

  Scenario: To verify emoji is present in fields when One-time Email is selected having Drag and Drop Layout
    Given I navigate to "Email" page via "Channels"
    And I select "One-time" Campaign Type and enters "One-time Email emoji Campaign with Drag and Drop" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select the ESP with name "Journey ESP" and I enter the following details in the message ESP
      | From name  | OneTime Automation                                          |
      | From email | info@email.dvmitsolutions.com                               |
      | Subject    | Subject: OneTime Email Campaign                             |
      | Body       | Drag & Drop Editor >Hi, I am a OneTime Drag Email Campaign. |
    Then I select tab "Message" in campaign crud page
    Then verify emojis are present in the following fields
      | Subject            |
      | Drag & Drop Editor |
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "One-time Email emoji Campaign with Drag and Drop" campaign
    Given I navigate to "Email" page via "Channels"
    Then I delete the Campaign with Name "One-time Email emoji Campaign with Drag and Drop"

  Scenario: To verify unicode characterset contents are showing properly for 'One-Time' Email Campaign for 'Rich Text' Layout
    Given I navigate to "Email" page via "Channels"
    And I select "One-time" Campaign Type and enters "Unicode:One Time Rich Text Email Campaign ! # $ % & ( ) * + , - . ‘" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | QUEUEING | false |  |  |
    Then fill the page via "Unicode-EmailOneTimeRichText" template from "MultiLingualTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    And I verify "Unicode:One Time Rich Text Email Campaign ! # $ % & ( ) * + , - . ‘" Campaign name with unicode characters on overview page
    Given I navigate to "Email" page via "Channels"
    Then I delete the Campaign with unicode characters "Unicode:One Time Rich Text Email Campaign ! # $ % & ( ) * + , - . ‘"

  Scenario: To verify unicode characterset contents are showing properly for 'Recurring' Email Campaign for 'Drag & Drop' Layout
    Given I navigate to "Email" page via "Channels"
    And I select "Recurring" Campaign Type and enters "Unicode:Recurring Drag & Drop Email Campaign : ; < = > ? @ [ ] ਇ ਈ" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | QUEUEING | false |  |  |
    Then fill the page via "Unicode-EmailRecurringDrag&Drop" template from "MultiLingualTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    And I verify "Unicode:Recurring Drag & Drop Email Campaign : ; < = > ? @ [ ] ਇ ਈ" Campaign name with unicode characters on overview page
    Given I navigate to "Email" page via "Channels"
    Then I delete the Campaign with unicode characters "Unicode:Recurring Drag & Drop Email Campaign : ; < = > ? @ [ ] ਇ ਈ"

  @Staging
  Scenario: To create campaign on feature branch as draft and then create on master namespace and verify campaign on feature branch
    Then I switch the namespace
    Given I navigate to "Email" page via "Channels"
    And I select "One-time" Campaign Type and enters "Feature - Master draft Email Campaign" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select the ESP with name "Dummy ESP" and I enter the following details in the message ESP
      | From name  | OneTime Automation                           |
      | From email | info@email.dvmitsolutions.com                |
      | Subject    | Subject: OneTime Email Campaign              |
      | Body       | Rich Text>Hi, I am a OneTime Email Campaign. |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    Given I navigate to "Email" page via "Channels"
    Then I switch the namespace
    Then check overview page is loaded for "Feature - Master draft Email Campaign" campaign
    Then I open "Feature - Master draft Email Campaign" campaign
    Then I select tab "Preview & Launch" in campaign crud page
    And I launch the Campaign
    Then I switch the namespace
    Then check overview page is loaded for "Feature - Master draft Email Campaign" campaign
    Given I navigate to "Email" page via "Channels"
    Then I delete the Campaign with Name "Feature - Master draft Email Campaign"
    Then I switch the namespace

  Scenario: To Verify Email Message Validation
    Given I navigate to "Email" page via "Channels"
    And I select "One-time" Campaign Type and enters "Email Campaign Message tab field validation" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select the ESP with name "Dummy ESP" and I enter the following details in the message ESP
      | From name  | OneTime Automation                     |
      | From email | info@email.dvmitsolutions.com,test.com |
      | Reply to   | test@email@webengage.com,test.com      |
      | CC         | test@webengage.com,test.com            |
      | BCC        | test@webengage.com,test1.com           |
    Given I navigate to "Email" page via "Channels"
    Then I delete the Campaign with Name "Email Campaign Message tab field validation"