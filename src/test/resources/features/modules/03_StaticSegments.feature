@StaticSegments @Regression
Feature: Static Segments Feature

  @Smoke
  Scenario: To verify creation of Static Segments using segment editor
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "Static Segments using segment editor"
    And Pass following details in user card
      | Visitor Type | All Users       |
      | User IDs     | equal to>AutoID |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I verify that the Static segment is displayed on Listing Page
    Then I delete static segment with Name "Static Segments using segment editor"

  @Smoke
  Scenario: To verify creation of Static Segments using csv editor
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "CSV Editor" with name "Static Segments using CSV editor"
    And I enter segment name "Static Segments using CSV editor" and select file from test data to upload
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I verify that the Static segment is displayed on Listing Page
    Then I delete static segment with Name "Static Segments using CSV editor"

  Scenario: Verify existing tags is attached to Static Segments
    Given I navigate to "Lists" page via "Segments"
    And I search for "StaticSegments Tag" segment
    Then I verify "staticsegmentstag" tag is attached to the "StaticSegments Tag" segment

  Scenario: Verify new tag is created from Menu and gets attached to newly created Static Segment
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "Tags:User Condition > User IDs"
    And Pass following details in user card
      | Visitor Type | All Users   |
      | User IDs     | ends with>P |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    And I search for "Tags:User Condition > User IDs" static segment
    And create new tag with name as "segmenttag3" for segment "Tags:User Condition > User IDs"
    Then I verify "segmenttag" tag is attached to the static segment
    Then I navigate to "Lists" page via "Segments"
    And I search for "Tags:User Condition > User IDs" static segment
    Then delete the tag with name as "segmenttag" from segment "Tags:User Condition > User IDs"
    Then I delete static segment with Name "Tags:User Condition > User IDs"

  Scenario: To verify creation of Static Segment containing unicode characterset using Segment editor for UserID condition
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "Unicode Segment Editor:# % & ( ) * ༂ ༃ ༄ ༅ ခ  ℚ ℛ ℜ ℝ ℞ ℟ℾ ℿ"
    And Pass following details in user card
      | Visitor Type | All Users       |
      | User IDs     | equal to>AutoID |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I verify that the Static segment is displayed on Listing Page
    Then I delete static segment with Name "Unicode Segment Editor:# % & ( ) * ༂ ༃ ༄ ༅ ခ  ℚ ℛ ℜ ℝ ℞ ℟ℾ ℿ"

  Scenario: To verify creation of Static Segments with unicode characterset using CSV upload
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "CSV Editor" with name "Unicode CSV Editor:⅁ ⅂ ⅃ ⅄ ⅅ ⅆ ⅇ ⅈ ⅉ ℐ ℑ ℒ ℓ ℔ ℕअ आ इ ई ਆ ਇ ਈ"
    And I enter segment name "Unicode CSV Editor:⅁ ⅂ ⅃ ⅄ ⅅ ⅆ ⅇ ⅈ ⅉ ℐ ℑ ℒ ℓ ℔ ℕअ आ इ ई ਆ ਇ ਈ" and select file from test data to upload
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I verify that the Static segment is displayed on Listing Page
    Then I delete static segment with Name "Unicode CSV Editor:⅁ ⅂ ⅃ ⅄ ⅅ ⅆ ⅇ ⅈ ⅉ ℐ ℑ ℒ ℓ ℔ ℕअ आ इ ई ਆ ਇ ਈ"

  Scenario: To verify if you can explicitly refresh an existing List and edit, refresh button gets disabled
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "Explicit Refresh List"
    And Pass following details in user card
      | Visitor Type | All Users   |
      | User IDs     | ends with>P |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I save the SegmentId of "Explicit Refresh List" segment
    Then wait for "fetchStaticSegments" api to return response as "ACTIVE" for path as "response.data.status"
    Then I refresh static segment with Name "Explicit Refresh List"
    Then verify "status" as "REFRESHING" for "Explicit Refresh List" static List
    Then verify "Type" as "Static" for "Explicit Refresh List" static List
    Then I delete static segment with Name "Explicit Refresh List"

  Scenario: To verify that when the user set the refresh schedule then only the refreshing list is created
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "Schedule Refresh List"
    And Pass following details in Refresh frequency
      | when | Monthly>Last day of Month |
      | Time | 3:30:am                   |
    And Pass following details in user card
      | Visitor Type | All Users   |
      | User IDs     | ends with>P |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then verify "Type" as "Refreshing" for "Schedule Refresh List" static List
    Then I delete static segment with Name "Schedule Refresh List"

  Scenario: To verify that manual refresh buttons are present within both the refreshing static list and normal static list
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "Manual Button Non Schedule List"
    And Pass following details in user card
      | Visitor Type | All Users   |
      | User IDs     | ends with>P |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I save the SegmentId of "Manual Button Non Schedule List" segment
    Then wait for "fetchStaticSegments" api to return response as "ACTIVE" for path as "response.data.status"
    Then check "Refresh" popup item is "Enabled" for "Manual Button Non Schedule List"
    Then I delete static segment with Name "Manual Button Non Schedule List"
    Then I create a Static Segment using "Segment Editor" with name "Manual Button Schedule List"
    And Pass following details in Refresh frequency
      | when | Weekly>Mon |
      | Time | 3:30:am    |
    And Pass following details in user card
      | Visitor Type | All Users   |
      | User IDs     | ends with>P |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I save the SegmentId of "Manual Button Schedule List" segment
    Then wait for "fetchStaticSegments" api to return response as "ACTIVE" for path as "response.data.status"
    Then verify "Type" as "Refreshing" for "Manual Button Schedule List" static List
    Then check "Refresh" popup item is "Enabled" for "Manual Button Schedule List"

  Scenario: To verify that when the user selects periodic refresh then the last refresh time is updated in the last refresh column.
    Given I navigate to "Lists" page via "Segments"
    Then I save the SegmentId of "Manual Button Schedule List" segment
    Then I refresh static segment with Name "Manual Button Schedule List"
    Then wait for "fetchStaticSegments" api to return response as "SUCCESS" for path as "response.data.refreshState"
    Then verify "Last Refresh" as "Date" for "Manual Button Schedule List" static List
    Then I delete static segment with Name "Manual Button Schedule List"

  @Staging
  Scenario: To create static segment on feature branch namespace and verify the campaign on production/master branch namespace
    Then I switch the namespace
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "Feature - Master static segment"
    And Pass following details in user card
      | Visitor Type | All Users   |
      | User IDs     | ends with>j |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I switch the namespace
    Given I navigate to "Lists" page via "Segments"
    Then I verify that the Static segment is displayed on Listing Page
    Then I delete static segment with Name "Feature - Master static segment"

  @End-to-End @Distributed
  Scenario: To verify the static segment creation with visitor type and created on is after selected date.
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "User Condition > Created On > after selected date"
    And Pass following details in user card
      | Visitor Type | All Users  |
      | Created On   | after-date |
      | Set Date as  | 1 Jan 2022 |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I save the SegmentId of "User Condition > Created On > after selected date" segment

  @End-to-End @Distributed
  Scenario: To verify the static segment creation with Geo Filtering any selected country, region and city
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "Geo Filtering>any selected country, region and city"
    And Pass following details in user card
      | Visitor Type                | All Users   |
      | Set Geo filtering type      | Any         |
      | Geo Filtering               | India       |
      | Geo Filtering>All Regions   | Maharashtra |
      | Geo Filtering>Select Cities | Mumbai      |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I save the SegmentId of "Geo Filtering>any selected country, region and city" segment

  @End-to-End @Distributed
  Scenario: To verify the static segment creation with Geo Filtering None selected country, region and city
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "Geo Filtering > None selected country, region and city"
    And Pass following details in user card
      | Visitor Type                | All Users     |
      | Set Geo filtering type      | None          |
      | Geo Filtering               | United States |
      | Geo Filtering>All Regions   | New Jersey    |
      | Geo Filtering>Select Cities | Belford       |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I save the SegmentId of "Geo Filtering > None selected country, region and city" segment

  @End-to-End @Distributed
  Scenario: To verify the static segment creation with User who did this event atleast once
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "User Condition>who did these Events>at least once"
    And Pass following details in Behavioural card
      |  | Users who DID these events : | Application>E1 |
    And I apply the filter for "Users who DID these events :" as
      | Event Time | after | date | 1 Jan 2024 |
    And I did these events for following event type
      | at least once |  |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I save the SegmentId of "User Condition>who did these Events>at least once" segment

  @End-to-End @Distributed
  Scenario: To verify the static segment creation with User who did this event Only once
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "User Condition>who did these Events>Only once"
    And Pass following details in Behavioural card
      |  | Users who DID these events : | Application>FireEvent_OnlyOnce |
    And I apply the filter for "Users who DID these events :" as
      | Event Time | after | date | 1 Jan 2024 |
    And I did these events for following event type
      | only once |  |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I save the SegmentId of "User Condition>who did these Events>Only once" segment

  @End-to-End @Distributed
  Scenario: To verify the static segment creation with User who did this event with no of occurence less than or equal to
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "User Condition>who did these Events>no. of occurrence"
    And Pass following details in Behavioural card
      |  | Users who DID these events : | Application>FireEvent_OnlyOnce |
    And I apply the filter for "Users who DID these events :" as
      | Event Time | after | date | 1 Jan 2024 |
    And I did these events for following event type
      | no. of occurences | is less than or equal to | 1 |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I save the SegmentId of "User Condition>who did these Events>no. of occurrence" segment

  @End-to-End @Distributed
  Scenario: To verify the static segment creation with User who did this event AND User who did not do these event.
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "User who did this event AND User who did not do these event"
    And Pass following details in user card
      | Visitor Type | All Users |
    And Pass following details in Behavioural card
      |  | Users who DID these events :        | Application>E1 |
      |  | Users who DID NOT do these events : | Application>E2 |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I save the SegmentId of "User who did this event AND User who did not do these event" segment

  @End-to-End @Distributed
  Scenario: To verify the static segment creation with User who did this event with System Event applying custom attribute filter
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "User who did this event with System Event > One of> email id"
    And Pass following details in Behavioural card
      |  | Users who DID these events : | System>Email Open |
    And I apply the filter for "Users who DID these events :" as
      | email_id | one of | aditya.dhanve@webengage.com |  |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I save the SegmentId of "User who did this event with System Event > One of> email id" segment

  @End-to-End @Distributed
  Scenario: To verify the static segment creation with User who did this event with Custom Event applying custom attribute filter
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "User who did this event with E1 and price filter"
    And Pass following details in Behavioural card
      |  | Users who DID these events : | Application>E1 |
    And I apply the filter for "Users who DID these events :" as
      | Price | equal to | 1000 |  |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I save the SegmentId of "User who did this event with E1 and price filter" segment

  @End-to-End @Distributed
  Scenario: To verify the static segment creation with User who did this event with Custom Event applying system attribute filter
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "custom event creation for event E1 and browser name filter"
    And Pass following details in Behavioural card
      |  | Users who DID these events : | Application>E1 |
    And I apply the filter for "Users who DID these events :" as
      | Browser Name | contains | Chrome |  |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I save the SegmentId of "custom event creation for event E1 and browser name filter" segment

  @End-to-End @Distributed
  Scenario: To verify the static segment creation with Android App ID equal to entered value
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "Technology Condition >Android"
    And Pass following details for "Android" in Technology card
      | App ID | equal to>com.webengage.production |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I save the SegmentId of "Technology Condition >Android" segment

  @End-to-End @Distributed
  Scenario: To verify the static segment creation with iOS Vendor ID one of selected value
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "Technology Condition >IOS>Vendor ID"
    And Pass following details for "iOS" in Technology card
      | Vendor ID | one of>43603158-C58B-442E-B074-54AAEE3F8655 |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I save the SegmentId of "Technology Condition >IOS>Vendor ID" segment

  @End-to-End @Distributed
  Scenario: To verify the static segment creation with Android Advertising ID contains entered value
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "Technology Condition >Android>Advertising ID"
    And Pass following details for "Android" in Technology card
      | Advertising ID | contains>b34d7f33-144d-49ac-acb7-b558cab44598 |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I save the SegmentId of "Technology Condition >Android>Advertising ID" segment

  @End-to-End @Distributed
  Scenario: To verify the static segment creation with Web Device one of Desktop
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "Technology Condition >Website >add device>Desktop"
    And Pass following details for "Web" in Technology card
      | Devices | is one of>Desktop |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I save the SegmentId of "Technology Condition >Website >add device>Desktop" segment

  @End-to-End @Distributed
  Scenario: To verify the static segment creation with Web Device neither of Tablet
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "Technology Condition >Website>neither>Tablet"
    And Pass following details for "Web" in Technology card
      | Devices | is neither of>Tablet |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I save the SegmentId of "Technology Condition >Website>neither>Tablet" segment

  @End-to-End @Distributed
  Scenario: To verify the static segment creation with Web Operating System one of Mac OS
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "Technology Condition >Operating system>one of>Mac OS"
    And Pass following details for "Web" in Technology card
      | Operating System | is one of>Mac OS |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I save the SegmentId of "Technology Condition >Operating system>one of>Mac OS" segment

  @End-to-End @Distributed
  Scenario: To verify the static segment creation with User System Attribute Email address matches the regex from selected dropdown value
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "User System Attribute Email address matches the regex from selected dropdown value"
    And Pass following details in user card
      | Visitor Type                    | All Users                   |
      | User Attribute>Select an Option | Email Address               |
      | User Attribute>Select...        | matches regex               |
      | User Attribute>Enter a value    | aditya.dhanve@webengage.com |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I save the SegmentId of "User System Attribute Email address matches the regex from selected dropdown value" segment

  @End-to-End @Distributed
  Scenario: To verify the static segment creation with User System Attribute First Name does not start with selected dropdown value
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "User Attribute >NOT> first name"
    And Pass following details in user card
      | Visitor Type                    | All Users           |
      | User Attribute>Select an Option | First Name          |
      | User Attribute>Select...        | does not start with |
      | User Attribute>Enter a value    | Test                |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I save the SegmentId of "User Attribute >NOT> first name" segment

  @End-to-End @Distributed
  Scenario: To verify the Static segment creation with User Custom Attribute Age equal to selected value
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "User Attribute age equals to 29"
    And Pass following details in user card
      | Visitor Type                    | All Users |
      | User Attribute>Select an Option | Age       |
      | User Attribute>Select...        | equal to  |
      | User Attribute>Enter a value    |        29 |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I save the SegmentId of "User Attribute age equals to 29" segment

  @End-to-End @Distributed
  Scenario: To verify the static segment creation with User Id equal to entered value
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "All user with User Ids equals to LS1"
    And Pass following details in user card
      | Visitor Type | All Users    |
      | User IDs     | equal to>LS1 |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I save the SegmentId of "All user with User Ids equals to LS1" segment

  @End-to-End @Distributed
  Scenario: To verify the static segment creation with User Id starts with entered value
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "All user with User starts with LS"
    And Pass following details in user card
      | Visitor Type | All Users      |
      | User IDs     | starts with>LS |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I save the SegmentId of "All user with User starts with LS" segment

  @End-to-End @Distributed
  Scenario: To verify the static segment creation with Reachability User Reachable on Email
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "User Condition > Reachability on Email"
    And Pass following details in user card
      | Visitor Type | All Users          |
      | Reachability | Reachable on-Email |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I save the SegmentId of "User Condition > Reachability on Email" segment

  @End-to-End @Distributed
  Scenario: To verify the static segment creation with Reachability User Not Reachable on Email
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "User Condition > Not Reachability on Email"
    And Pass following details in user card
      | Visitor Type | All Users              |
      | Reachability | Not reachable on-Email |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I save the SegmentId of "User Condition > Not Reachability on Email" segment

  @End-to-End @Distributed
  Scenario: To verify the static segment creation with Intelligent Attribute Best Channel is not in Email
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "Intelligent Attributes >is not in>E-mail"
    And Pass following details in user card
      | Intelligent Attributes | Best Channel-is not in |
      | Intelligent Attributes | Best Channel-Email     |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I save the SegmentId of "Intelligent Attributes >is not in>E-mail" segment

  Scenario: To verify duplication of lists using user card
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "Duplication of lists using UserCard"
    And Pass following details in user card
      | Intelligent Attributes | Best Channel-is not in |
      | Intelligent Attributes | Best Channel-Email     |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    And perform "Duplicate" action for "Duplication of lists using UserCard" segment on the listing page
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I delete the following mentioned "lists"
      | Duplication of lists using UserCard         |
      | Copy of Duplication of lists using UserCard |

  Scenario: To verify duplication of lists using behavioural card
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "Duplication of lists using Behavioural card"
    And Pass following details in Behavioural card
      |  | Users who DID these events :        | Application>E1 |
      |  | Users who DID NOT do these events : | Application>E2 |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    And perform "Duplicate" action for "Duplication of lists using Behavioural card" segment on the listing page
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I delete the following mentioned "lists"
      | Duplication of lists using Behavioural card         |
      | Copy of Duplication of lists using Behavioural card |

  Scenario: To verify duplication of lists using technology card
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "Duplication of lists using Technology card"
    And Pass following details for "Android" in Technology card
      | App ID | equal to>com.webengage.production |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    And perform "Duplicate" action for "Duplication of lists using Technology card" segment on the listing page
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I delete the following mentioned "lists"
      | Duplication of lists using Technology card         |
      | Copy of Duplication of lists using Technology card |

  Scenario: To verify duplication of refreshing lists using user card
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "Duplication of refreshing list using user card"
    And Pass following details in Refresh frequency
      | when | Daily   |
      | Time | 7:10:am |
    And Pass following details in user card
      | Visitor Type | All Users   |
      | User IDs     | ends with>P |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then verify "Type" as "Refreshing" for "Duplication of refreshing list using user card" static List
    And perform "Duplicate" action for "Duplication of refreshing list using user card" segment on the listing page
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I delete the following mentioned "lists"
      | Duplication of refreshing list using user card         |
      | Copy of Duplication of refreshing list using user card |

  Scenario: To verify duplication of refreshing lists using Behavioural card
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "Duplication of refreshing list using behavioural card"
    And Pass following details in Refresh frequency
      | when | Monthly>Last day of Month |
      | Time | 7:20:am                   |
    And Pass following details in Behavioural card
      |  | Users who DID these events : | Application>FireEvent_OnlyOnce |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then verify "Type" as "Refreshing" for "Duplication of refreshing list using behavioural card" static List
    And perform "Duplicate" action for "Duplication of refreshing list using behavioural card" segment on the listing page
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I delete the following mentioned "lists"
      | Duplication of refreshing list using behavioural card         |
      | Copy of Duplication of refreshing list using behavioural card |

  Scenario: To verify duplication of refreshing lists using Technology card
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "Segment Editor" with name "Duplication of refreshing list using technology card"
    And Pass following details in Refresh frequency
      | when | Weekly>Mon |
      | Time | 9:00:am    |
    And Pass following details for "Android" in Technology card
      | App ID | equal to>com.webengage.production |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then verify "Type" as "Refreshing" for "Duplication of refreshing list using technology card" static List
    And perform "Duplicate" action for "Duplication of refreshing list using technology card" segment on the listing page
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I delete the following mentioned "lists"
      | Duplication of refreshing list using technology card         |
      | Copy of Duplication of refreshing list using technology card |
