@SMSCampaigns @Regression
Feature: SMS Campaign Feature
  Scenario Outline: To verify previously created SMS campaigns are available in SMS campaign > List of campaigns page
    Given I navigate to "SMS" page via "Channels"
    Then I add "<CampaignName>" to searchbox
    And I verify that "<CampaignName>" and "<CampaignType>" exist

    Examples: 
      | CampaignName                          | CampaignType  |
      | Test One-time SMS Campaign exist      | One-time      |
      | Test Triggered SMS Campaign exist     | Triggered     |
      | Test Recurring SMS Campaign exist     | Recurring     |
      | Test Transactional SMS Campaign exist | Transactional |

  @Smoke
  Scenario: To verify Live Segment via Segment Editor is getting created when One-time SMS Campaign type is selected
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "LiveSegment via Segment Editor-One-time SMS Campaign" Campaign Name
    And I select New Segment Creation icon
    And I add "Segment Name" as "SMS: User Condition > User IDs"
    And Pass following details in user card
      | Visitor Type | All Users   |
      | User IDs     | ends with>P |
    And I Save and verify the created "Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | QUEUEING | false |  |  |
    And I select the SSP with name "Dummy SSP" and I enter the following details in the message SSP
      | Sender  | WBENGE                                                                                 |
      | Message | Hi Automation, text for Internal development and testing of SMS Automation - WebEngage |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    And verify via "fetchSMSCampaign" API whether following "Segment" have been attached to campaign just created
      | SMS: User Condition > User IDs |
    Then I navigate to "Live Segments" page via "Segments"
    And I verify "SMS: User Condition > User IDs" segment is present
    Given I navigate to "SMS" page via "Channels"
    Then check overview page is loaded for "LiveSegment via Segment Editor-One-time SMS Campaign" campaign
    Given I navigate to "SMS" page via "Channels"
    Then I delete the Campaign with Name "LiveSegment via Segment Editor-One-time SMS Campaign"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "SMS: User Condition > User IDs"

  @Smoke
  Scenario: To verify Live Segment is getting created when Triggered SMS Campaign type is selected
    Given I navigate to "SMS" page via "Channels"
    And I select "Triggered" Campaign Type and enters "LiveSegment via Segment Editor-Triggered SMS Campaign" Campaign Name
    And I select New Segment Creation icon
    And I add "Segment Name" as "SMS: Behavorial Condition > more than 1 condition of Users who did these Events"
    And Pass following details in Behavioural card
      |     | Users who DID these events : | System>User Login    |
      | And | Users who DID these events : | System>App Installed |
    And I Save and verify the created "Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | UPON OCCURRENCE OF | TriggerSMSEvent |    |       |
      | QUEUEING           | true            | 12 | Hours |
    And I select the SSP with name "Dummy SSP" and I enter the following details in the message SSP
      | Sender  | WBENGE                                                                                 |
      | Message | Hi Automation, text for Internal development and testing of SMS Automation - WebEngage |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    And verify via "fetchSMSCampaign" API whether following "Segments" have been attached to campaign just created
      | SMS: Behavorial Condition > more than 1 condition of Users who did these Events |
    Then I navigate to "Live Segments" page via "Segments"
    And I verify "SMS: Behavorial Condition > more than 1 condition of Users who did these Events" segment is present
    Given I navigate to "SMS" page via "Channels"
    Then check overview page is loaded for "LiveSegment via Segment Editor-Triggered SMS Campaign" campaign
    Given I navigate to "SMS" page via "Channels"
    Then I delete the Campaign with Name "LiveSegment via Segment Editor-Triggered SMS Campaign"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "SMS: Behavorial Condition > more than 1 condition of Users who did these Events"

  Scenario: To verify Live Segment via Segment Editor is getting created when Recurring SMS Campaign type is selected
    Given I navigate to "SMS" page via "Channels"
    And I select "Recurring" Campaign Type and enters "LiveSegment via Segment Editor-Recurring SMS Campaign" Campaign Name
    And I select New Segment Creation icon
    And I add "Segment Name" as "SMS: Technology Condition > iOS"
    And Pass following details for "iOS" in Technology card
      | Total Time Spent (in seconds) | greater than>100 |
    And I Save and verify the created "Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select the SSP with name "Dummy SSP" and I enter the following details in the message SSP
      | Sender  | WBENGE                                                                                 |
      | Message | Hi Automation, text for Internal development and testing of SMS Automation - WebEngage |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    And verify via "fetchSMSCampaign" API whether following "Segments" have been attached to campaign just created
      | SMS: Technology Condition > iOS |
    Then I navigate to "Live Segments" page via "Segments"
    And I verify "SMS: Technology Condition > iOS" segment is present
    Given I navigate to "SMS" page via "Channels"
    Then check overview page is loaded for "LiveSegment via Segment Editor-Recurring SMS Campaign" campaign
    Given I navigate to "SMS" page via "Channels"
    Then I delete the Campaign with Name "LiveSegment via Segment Editor-Recurring SMS Campaign"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "SMS: Technology Condition > iOS"

  @Smoke
  Scenario: To verify sms campaign creation using Inclusion/Exclusion of segment feature
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "LiveSegment with Inclusion/Exclusion of segment SMS Campaign" Campaign Name
    Then I fill the details in Audience tab as below
      | Audience Type | multiple segments |
    Then I select "Users in ANY of these segments" as "Send To" Inclusion Segment
    And I add "Segment Name" as "SMS: User Condition > User ID ends with P"
    And Pass following details in user card
      | Visitor Type | All Users   |
      | User IDs     | ends with>P |
    And Save the Segment
    Then I select "Users in ANY of these segments" as "EXCLUDE" Exclusion Segment
    And I add "Segment Name" as "SMS: User Condition > Reachability on Whatsapp"
    And Pass following details in user card
      | Visitor Type | All Users             |
      | Reachability | Reachable on-WhatsApp |
    And Save the Segment
    And I save the Audience details
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select the SSP with name "Dummy SSP" and I enter the following details in the message SSP
      | Sender  | WBENGE                                                                                 |
      | Message | Hi Automation, text for Internal development and testing of SMS Automation - WebEngage |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    And verify via "fetchSMSCampaign" API whether following "Segment" have been attached to campaign just created
      | SMS: User Condition > User ID ends with P      |
      | SMS: User Condition > Reachability on Whatsapp |
    Then I navigate to "Live Segments" page via "Segments"
    And I verify following Segments are present
      | SMS: User Condition > User ID ends with P      |
      | SMS: User Condition > Reachability on Whatsapp |
    Given I navigate to "SMS" page via "Channels"
    Then check overview page is loaded for "LiveSegment with Inclusion/Exclusion of segment SMS Campaign" campaign
    Given I navigate to "SMS" page via "Channels"
    Then I delete the Campaign with Name "LiveSegment with Inclusion/Exclusion of segment SMS Campaign"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete the following mentioned "segments"
      | SMS: User Condition > User ID ends with P      |
      | SMS: User Condition > Reachability on Whatsapp |

  @Smoke
  Scenario: To verify One-time SMS campaign when Delivery Time:Later and Intelligently is selected
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "One-time SMS Campaign with Intelligently Delivery Time" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below when "One-time" Campaign and "SMS" Channel is selected
      | DELIVERY SCHEDULE | later             |  |  |
      | DELIVERY TIME     | sendIntelligently |  |  |
    And I select the SSP with name "Dummy SSP" and I enter the following details in the message SSP
      | Sender  | WBENGE                                                                                 |
      | Message | Hi Automation, text for Internal development and testing of SMS Automation - WebEngage |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "One-time SMS Campaign with Intelligently Delivery Time" campaign
    Given I navigate to "SMS" page via "Channels"
    Then I add "One-time SMS Campaign with Intelligently Delivery Time" to searchbox
    And I verify following details when "One-time" Campaign having "One-time SMS Campaign with Intelligently Delivery Time" Campaign Name and "SMS" Channel is selected
      | Delivery Time: Send Intelligently |
    Given I navigate to "SMS" page via "Channels"
    Then I delete the Campaign with Name "One-time SMS Campaign with Intelligently Delivery Time"

  @Smoke
  Scenario: To verify One-time SMS campaign when Delivery Time:Later and Specific Time is selected
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "One-time SMS Campaign with Specific Time Delivery" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below when "One-time" Campaign and "SMS" Channel is selected
      | DELIVERY SCHEDULE | later              |  |  |
      | DELIVERY TIME     | sendAtSpecificTime |  |  |
    And I select the SSP with name "Dummy SSP" and I enter the following details in the message SSP
      | Sender  | WBENGE                                                                                 |
      | Message | Hi Automation, text for Internal development and testing of SMS Automation - WebEngage |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "One-time SMS Campaign with Specific Time Delivery" campaign
    Given I navigate to "SMS" page via "Channels"
    Then I add "One-time SMS Campaign with Specific Time Delivery" to searchbox
    And I verify following details when "One-time" Campaign having "One-time SMS Campaign with Specific Time Delivery" Campaign Name and "SMS" Channel is selected
      | Delivery Time: |
    Given I navigate to "SMS" page via "Channels"
    Then I delete the Campaign with Name "One-time SMS Campaign with Specific Time Delivery"

  Scenario: To verify Triggered SMS campaign when Start Date: Now and End Date: Till is selected
    Given I navigate to "SMS" page via "Channels"
    And I select "Triggered" Campaign Type and enters "Triggered SMS Campaign with Start Date:Now and End Date:Till" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | UPON OCCURRENCE OF | TriggerSMSEvent |  |  |
      | START DATE         | now             |  |  |
      | END DATE           | till            |  |  |
    And I select the SSP with name "Dummy SSP" and I enter the following details in the message SSP
      | Sender  | WBENGE                                                                                 |
      | Message | Hi Automation, text for Internal development and testing of SMS Automation - WebEngage |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Triggered SMS Campaign with Start Date:Now and End Date:Till" campaign
    Given I navigate to "SMS" page via "Channels"
    Then I add "Triggered SMS Campaign with Start Date:Now and End Date:Till" to searchbox
    And I verify following details for "Triggered SMS Campaign with Start Date:Now and End Date:Till" campaign
      | Start date: |
      | End date:   |
    Given I navigate to "SMS" page via "Channels"
    Then I delete the Campaign with Name "Triggered SMS Campaign with Start Date:Now and End Date:Till"

  Scenario: To verify Triggered SMS campaign when Start Date: Later and End Date: Till is selected
    Given I navigate to "SMS" page via "Channels"
    And I select "Triggered" Campaign Type and enters "Triggered SMS Campaign with Start Date:Later and End Date:Till" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | UPON OCCURRENCE OF | TriggerSMSEvent |  |  |
      | START DATE         | later           |  |  |
      | END DATE           | till            |  |  |
    And I select the SSP with name "Dummy SSP" and I enter the following details in the message SSP
      | Sender  | WBENGE                                                                                 |
      | Message | Hi Automation, text for Internal development and testing of SMS Automation - WebEngage |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Triggered SMS Campaign with Start Date:Later and End Date:Till" campaign
    Given I navigate to "SMS" page via "Channels"
    Then I add "Triggered SMS Campaign with Start Date:Later and End Date:Till" to searchbox
    And I verify following details for "Triggered SMS Campaign with Start Date:Later and End Date:Till" campaign
      | Start date: |
      | End date:   |
    Given I navigate to "SMS" page via "Channels"
    Then I delete the Campaign with Name "Triggered SMS Campaign with Start Date:Later and End Date:Till"

  Scenario: To verify Triggered SMS campaign when Start Date: Later and End Date: Never is selected
    Given I navigate to "SMS" page via "Channels"
    And I select "Triggered" Campaign Type and enters "Triggered SMS Campaign with Start Date:Later and End Date:Never" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | UPON OCCURRENCE OF | TriggerSMSEvent |  |  |
      | START DATE         | later           |  |  |
      | END DATE           | never           |  |  |
    And I select the SSP with name "Dummy SSP" and I enter the following details in the message SSP
      | Sender  | WBENGE                                                                                 |
      | Message | Hi Automation, text for Internal development and testing of SMS Automation - WebEngage |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Triggered SMS Campaign with Start Date:Later and End Date:Never" campaign
    Given I navigate to "SMS" page via "Channels"
    Then I add "Triggered SMS Campaign with Start Date:Later and End Date:Never" to searchbox
    And I verify following details for "Triggered SMS Campaign with Start Date:Later and End Date:Never" campaign
      | Start date: |
      | End date:   |
    Given I navigate to "SMS" page via "Channels"
    Then I delete the Campaign with Name "Triggered SMS Campaign with Start Date:Later and End Date:Never"

  Scenario: To verify Recurring SMS campaign when Delivery Schedule: 1/Day and End Date: Never is selected
    Given I navigate to "SMS" page via "Channels"
    And I select "Recurring" Campaign Type and enters "Recurring SMS Campaign with Delivery Schedule:1/Day and End Date:Never" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | DELIVERY SCHEDULE | Day   |  |  |
      | END DATE          | never |  |  |
    And I select the SSP with name "Dummy SSP" and I enter the following details in the message SSP
      | Sender  | WBENGE                                                                                 |
      | Message | Hi Automation, text for Internal development and testing of SMS Automation - WebEngage |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Recurring SMS Campaign with Delivery Schedule:1/Day and End Date:Never" campaign
    Given I navigate to "SMS" page via "Channels"
    Then I add "Recurring SMS Campaign with Delivery Schedule:1/Day and End Date:Never" to searchbox
    And I verify following details for "Recurring SMS Campaign with Delivery Schedule:1/Day and End Date:Never" campaign
      | Delivery Schedule |
      | End date:         |
    Given I navigate to "SMS" page via "Channels"
    Then I delete the Campaign with Name "Recurring SMS Campaign with Delivery Schedule:1/Day and End Date:Never"

  @Smoke
  Scenario: To verify Recurring SMS campaign when Delivery Schedule: 1/Day and End Date: Till is selected
    Given I navigate to "SMS" page via "Channels"
    And I select "Recurring" Campaign Type and enters "Recurring SMS Campaign with Delivery Schedule:1/Day and End Date:Till" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | DELIVERY SCHEDULE | Day  |  |  |
      | END DATE          | till |  |  |
    And I select the SSP with name "Dummy SSP" and I enter the following details in the message SSP
      | Sender  | WBENGE                                                                                 |
      | Message | Hi Automation, text for Internal development and testing of SMS Automation - WebEngage |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Recurring SMS Campaign with Delivery Schedule:1/Day and End Date:Till" campaign
    Given I navigate to "SMS" page via "Channels"
    Then I add "Recurring SMS Campaign with Delivery Schedule:1/Day and End Date:Till" to searchbox
    And I verify following details for "Recurring SMS Campaign with Delivery Schedule:1/Day and End Date:Till" campaign
      | End date:         |
      | Delivery Schedule |
    Given I navigate to "SMS" page via "Channels"
    Then I delete the Campaign with Name "Recurring SMS Campaign with Delivery Schedule:1/Day and End Date:Till"

  @End-to-End @Distributed
  Scenario: To verify SMS is send to users and stats are showing properly when Transactional SMS Campaign type is selected
    Given I navigate to "SMS" page via "Channels"
    And I select "Transactional" Campaign Type and enters "Transactional SMS Campaign" Campaign Name
    Then I fill the details in Audience tab as below
      |  |  |
    And I select the SSP with name "SMS Campaign SP" and I enter the following details in the message SSP
      | Sender          | WBENGE                                                                                      |
      | Message         | Hi TransAutomation, text for Internal development and testing of SMS Automation - WebEngage |
      | DLT Template ID |                                                                         1107161103863779241 |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then wait for "fetchSMSCampaign" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    Then check overview page is loaded for "Transactional SMS Campaign" campaign
    Given I navigate to "SMS" page via "Channels"
    When I hit the POST method using static body for "transactionalCampaign" api from "SMSCampaigns" sheet with ref id "transactionalSMS"

  @End-to-End @Distributed
  Scenario: To verify Stats are showing properly when One-time SMS Campaign type is selected
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "Sent/Delivered Status-One-time SMS Campaign" Campaign Name
    Then select Segment having name as "StatsValidationUser Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | QUEUEING   | false |  |  |
      | FC         | false |  |  |
      | DND        | false |  |  |
      | THROTTLING | false |  |  |
    And I select the SSP with name "SMS Campaign SP" and I enter the following details in the message SSP
      | Sender          | WBENGE                                                                                        |
      | Message         | Hi OneTimeAutomation, text for Internal development and testing of SMS Automation - WebEngage |
      | DLT Template ID |                                                                           1107161103863779241 |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Sent/Delivered Status-One-time SMS Campaign" campaign

  @End-to-End @Distributed
  Scenario: To verify Stats are showing properly when Triggered SMS Campaign type is selected
    Given I navigate to "SMS" page via "Channels"
    And I select "Triggered" Campaign Type and enters "Sent/Delivered Status-Triggered SMS Campaign" Campaign Name
    Then select Segment having name as "StatsValidationUser Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | UPON OCCURRENCE OF | TriggerSMSEvent |  |  |
      | QUEUEING           | false           |  |  |
      | FC                 | false           |  |  |
      | DND                | false           |  |  |
      | THROTTLING         | false           |  |  |
      | END DATE           | after 3 hours   |  |  |
    And I select the SSP with name "SMS Campaign SP" and I enter the following details in the message SSP
      | Sender          | WBENGE                                                                                     |
      | Message         | Hi TrigAutomation, text for Internal development and testing of SMS Automation - WebEngage |
      | DLT Template ID |                                                                        1107161103863779241 |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then wait for "fetchSMSCampaign" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    Then check overview page is loaded for "Sent/Delivered Status-Triggered SMS Campaign" campaign
    Given I navigate to "SMS" page via "Channels"
    And hit "performEvent" API from "PerformEvents" sheet to trigger Event with reference id "triggerSMSEvent"

  @End-to-End @Distributed
  Scenario: To verify Stats are showing properly when Recurring SMS Campaign type is selected
    Given I navigate to "SMS" page via "Channels"
    And I select "Recurring" Campaign Type and enters "Sent/Delivered Status-Recurring SMS Campaign" Campaign Name
    Then select Segment having name as "StatsValidationUser Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | DELIVERY SCHEDULE | Day           |  |  |
      | QUEUEING          | false         |  |  |
      | FC                | false         |  |  |
      | DND               | false         |  |  |
      | THROTTLING        | false         |  |  |
      | END DATE          | after 3 hours |  |  |
    Then I adjust the time for Recurring Campaign by checking system time
    And I select the SSP with name "SMS Campaign SP" and I enter the following details in the message SSP
      | Sender          | WBENGE                                                                                    |
      | Message         | Hi RecAutomation, text for Internal development and testing of SMS Automation - WebEngage |
      | DLT Template ID |                                                                       1107161103863779241 |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Sent/Delivered Status-Recurring SMS Campaign" campaign

  Scenario: To verify on triggering conversion event, conversion is recorded for SMS campaign
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "Conversion Tracking-One-time SMS Campaign" Campaign Name
    Then select Segment having name as "StatsValidationUser Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select the SSP with name "SMS Campaign SP" and I enter the following details in the message SSP
      | Sender          | WBENGE                                                                                   |
      | Message         | Hi CTAutomation, text for Internal development and testing of SMS Automation - WebEngage |
      | DLT Template ID |                                                                      1107161103863779241 |
    Then I fill the details in Conversion Tracking tab as below
      | Conversion Tracking | On              |
      | CONVERSION EVENT    | TriggerSMSEvent |
      | CONVERSION DEADLINE | 7-Days          |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Conversion Tracking-One-time SMS Campaign" campaign
    Given I navigate to "SMS" page via "Channels"
    Then I delete the Campaign with Name "Conversion Tracking-One-time SMS Campaign"

  Scenario: Verify existing tags is attached to SMS Campaign
    Given I navigate to "SMS" page via "Channels"
    And I search for "SMS Campaign: Tag" campaign
    Then I verify "campaigntag" tag is attached to the "SMS Campaign: Tag" campaign

  @Smoke
  Scenario: Verify new tag is created from SMS Channel and gets attached to newly created SMS Campaign
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "Tag: SMS Campaign" Campaign Name
    And create new tag from with name as "sms-tag" for campaign
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select the SSP with name "Dummy SSP" and I enter the following details in the message SSP
      | Sender  | WBENGE                                                                                    |
      | Message | Hi TagAutomation, text for Internal development and testing of SMS Automation - WebEngage |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Tag: SMS Campaign" campaign
    Given I navigate to "SMS" page via "Channels"
    Then I verify "sms-tag" tag is attached to the "Tag: SMS Campaign" campaign
    Given I navigate to "SMS" page via "Channels"
    And I search for "Tag: SMS Campaign" campaign
    Then delete the tag with name as "sms-tag" from campaign "Tag: SMS Campaign"
    Then I delete the Campaign with Name "Tag: SMS Campaign"

  @Smoke
  Scenario: Verify whether Queuing values get set and get reflected on preview and launch page
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "Check queuing option is enabled" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | QUEUEING | true | 12 | Hours |
    And I select the SSP with name "Dummy SSP" and I enter the following details in the message SSP
      | Sender  | WBENGE                                                                                 |
      | Message | Hi Automation, text for Internal development and testing of SMS Automation - WebEngage |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    Then I validate following things in Preview section
      | QUEUING : Queue for 12 Hours |
    And I launch the Campaign
    Then check overview page is loaded for "Check queuing option is enabled" campaign
    Given I navigate to "SMS" page via "Channels"
    Then I add "Check queuing option is enabled" to searchbox
    And I verify following details for "Check queuing option is enabled" campaign
      | QUEUING : Queue for 12 Hours |
    Given I navigate to "SMS" page via "Channels"
    Then I delete the Campaign with Name "Check queuing option is enabled"

  @Smoke
  Scenario: Verify when queuing is disabled it get's displayed correctly on Preview and launch page
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "Check queuing option is disabled" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | QUEUEING | false |  |  |
    And I select the SSP with name "Dummy SSP" and I enter the following details in the message SSP
      | Sender  | WBENGE                                                                                 |
      | Message | Hi Automation, text for Internal development and testing of SMS Automation - WebEngage |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    Then I validate following things in Preview section
      | QUEUING : Disabled |
    And I launch the Campaign
    Then check overview page is loaded for "Check queuing option is disabled" campaign
    Given I navigate to "SMS" page via "Channels"
    Then I add "Check queuing option is disabled" to searchbox
    And I verify following details for "Check queuing option is disabled" campaign
      | QUEUING : Disabled |
    Given I navigate to "SMS" page via "Channels"
    Then I delete the Campaign with Name "Check queuing option is disabled"

  @Smoke
  Scenario: To verify test variation/campaign is working fine by selecting existing Test Segment for SMS
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "Test Page Validation for SMS" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select the SSP with name "Dummy SSP" and I enter the following details in the message SSP
      | Sender  | WBENGE                                                                                 |
      | Message | Hi Automation, text for Internal development and testing of SMS Automation - WebEngage |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And test the "SMS" Campaign for segment "TEST-UserAditya" and expect the status to be "FAILED"
    And I launch the Campaign
    Then check overview page is loaded for "Test Page Validation for SMS" campaign
    Given I navigate to "SMS" page via "Channels"
    Then I delete the Campaign with Name "Test Page Validation for SMS"

  Scenario: To verify overview page is loaded when campaign status is paused for One-time SMS
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "SMS-OneTime-Overview Page Validation for Paused status" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select the SSP with name "Dummy SSP" and I enter the following details in the message SSP
      | Sender  | WBENGE                                                                                 |
      | Message | Hi Automation, text for Internal development and testing of SMS Automation - WebEngage |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I pause the campaign with "sms-deactivate" api
    Then check overview page is loaded for "SMS-OneTime-Overview Page Validation for Paused status" campaign
    Given I navigate to "SMS" page via "Channels"
    Then I delete the Campaign with Name "SMS-OneTime-Overview Page Validation for Paused status"

  Scenario: To verify overview page is loaded when campaign status is in draft state for Triggered SMS
    Given I navigate to "SMS" page via "Channels"
    And I select "Triggered" Campaign Type and enters "SMS-Triggered-Overview Page Validation for draft status" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | UPON OCCURRENCE OF | TriggerSMSEvent |  |  |
      | START DATE         | later           |  |  |
      | END DATE           | never           |  |  |
    And I select the SSP with name "Dummy SSP" and I enter the following details in the message SSP
      | Sender  | WBENGE                                                                                 |
      | Message | Hi Automation, text for Internal development and testing of SMS Automation - WebEngage |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    Given I navigate to "SMS" page via "Channels"
    Then check overview page is loaded for "SMS-Triggered-Overview Page Validation for draft status" campaign
    Given I navigate to "SMS" page via "Channels"
    Then I delete the Campaign with Name "SMS-Triggered-Overview Page Validation for draft status"

  Scenario: To verify emoji is present in fields when One-time SMS is selected
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "SMS-OneTime-emoji" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select the SSP with name "Journey SSP" and I enter the following details in the message SSP
      | Sender  | WBENGE                                                                                 |
      | Message | Hi Automation, text for Internal development and testing of SMS Automation - WebEngage |
    Then I select tab "Message" in campaign crud page
    Then verify emojis are present in the following fields
      | Message |
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "SMS-OneTime-emoji" campaign
    Given I navigate to "SMS" page via "Channels"
    Then I delete the Campaign with Name "SMS-OneTime-emoji"

  Scenario: To verify emoji is present in fields when Transactional SMS is selected
    Given I navigate to "SMS" page via "Channels"
    And I select "Transactional" Campaign Type and enters "SMS-Transactional-emoji" Campaign Name
    Then I fill the details in Audience tab as below
      |  |  |
    And I select the SSP with name "Journey SSP" and I enter the following details in the message SSP
      | Sender  | WBENGE                                                                                 |
      | Message | Hi Automation, text for Internal development and testing of SMS Automation - WebEngage |
    Then I select tab "Message" in campaign crud page
    Then verify emojis are present in the following fields
      | Message |
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "SMS-Transactional-emoji" campaign
    Given I navigate to "SMS" page via "Channels"
    Then I delete the Campaign with Name "SMS-Transactional-emoji"

  Scenario: To verify unicode characterset contents are showing properly for 'One-Time' SMS Campaign
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "Unicode: SMS Campaign One Time ဃ င Ⴁ Ⴂ Ⴃ Ⴄ ἇ Ἀ ἕ Ἐ ℀ ℁" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | QUEUEING | false |  |  |
    Then fill the page via "Unicode-SMSOneTime" template from "MultiLingualTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    And I verify "Unicode: SMS Campaign One Time ဃ င Ⴁ Ⴂ Ⴃ Ⴄ ἇ Ἀ ἕ Ἐ ℀ ℁" Campaign name with unicode characters on overview page
    Given I navigate to "SMS" page via "Channels"
    Then I delete the Campaign with unicode characters "Unicode: SMS Campaign One Time ဃ င Ⴁ Ⴂ Ⴃ Ⴄ ἇ Ἀ ἕ Ἐ ℀ ℁"

  Scenario: To verify unicode characterset contents are showing properly for 'Transactional' SMS Campaign
    Given I navigate to "SMS" page via "Channels"
    And I select "Transactional" Campaign Type and enters "Unicode: SMS-Transactional Campaign ℂ ℃ ℄ ℅ ℆ ℇ ℈ ℉ ℊ ℋ" Campaign Name
    Then I fill the details in Audience tab as below
      |  |  |
    Then fill the page via "Unicode-SMSTransactional" template from "MultiLingualTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Given I navigate to "SMS" page via "Channels"
    Then I delete the Campaign with unicode characters "Unicode: SMS-Transactional Campaign ℂ ℃ ℄ ℅ ℆ ℇ ℈ ℉ ℊ ℋ"

  @Staging
  Scenario: To create campaign on production/master branch namespace and verify the campaign on feature branch namespace
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "Master - Feature One-time SMS Campaign" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select the SSP with name "Dummy SSP" and I enter the following details in the message SSP
      | Sender  | WBENGE                                                                                 |
      | Message | Hi Automation, text for Internal development and testing of SMS Automation - WebEngage |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I switch the namespace
    Then check overview page is loaded for "Master - Feature One-time SMS Campaign" campaign
    Given I navigate to "SMS" page via "Channels"
    Then I delete the Campaign with Name "Master - Feature One-time SMS Campaign"
    Then I switch the namespace

  @Staging
  Scenario: To create campaign on feature branch namespace and verify the campaign on production/master branch namespace
    Then I switch the namespace
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "Feature - Master One-time SMS Campaign" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select the SSP with name "Dummy SSP" and I enter the following details in the message SSP
      | Sender  | WBENGE                                                                                 |
      | Message | Hi Automation, text for Internal development and testing of SMS Automation - WebEngage |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I switch the namespace
    Then check overview page is loaded for "Feature - Master One-time SMS Campaign" campaign
    Given I navigate to "SMS" page via "Channels"
    Then I delete the Campaign with Name "Feature - Master One-time SMS Campaign"

  @Staging
  Scenario: To create campaign on master branch as draft and then create on feature namespace and verify the campaign on master branch
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "Master - Feature draft SMS Campaign" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select the SSP with name "Dummy SSP" and I enter the following details in the message SSP
      | Sender  | WBENGE                                                                                 |
      | Message | Hi Automation, text for Internal development and testing of SMS Automation - WebEngage |
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    Given I navigate to "SMS" page via "Channels"
    Then I switch the namespace
    Then check overview page is loaded for "Master - Feature draft SMS Campaign" campaign
    Then I open "Master - Feature draft SMS Campaign" campaign
    Then I select tab "Preview & Launch" in campaign crud page
    And I launch the Campaign
    Then I switch the namespace
    Then check overview page is loaded for "Master - Feature draft SMS Campaign" campaign
    Given I navigate to "SMS" page via "Channels"
    Then I delete the Campaign with Name "Master - Feature draft SMS Campaign"

  Scenario: To verify Save and continue will be disabled when inclusion section is blank and any segment present in exclusion section.
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "SMS:save and continue disabled" Campaign Name
    Then I fill the details in Audience tab as below
      | Audience Type | multiple segments |  |
    Then I select "EXCLUDE" Segment with name "ExcludeLive"
    Then I verify "SAVE & CONTINUE" button is "disabled"

  Scenario: To verify Save and continue will be disabled when inclusion and exclusion section initially has segment present but inclusion segment is then removed.
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "SMS:save and continue disabled" Campaign Name
    Then I fill the details in Audience tab as below
      | Audience Type | multiple segments |  |
    Then I select "Users in ALL of these segments" as "Send To" Segment for search
    Then select Segment having name as "IncludeList" for "include"
    Then I select "Users in ALL of these segments" as "EXCLUDE" Segment for search
    Then select Segment having name as "ExcludeList" for "exclude"
    Then I remove "inlcusion" segment
    Then I verify "SAVE & CONTINUE" button is "disabled"

  Scenario: To verify Save and continue will be enabled when inclusion and exclusion section initially has segment present but exclusion segment is then removed.
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "SMS:save and continue enabled" Campaign Name
    Then I fill the details in Audience tab as below
      | Audience Type | multiple segments |  |
    Then I select "Users in ALL of these segments" as "Send To" Segment for search
    Then select Segment having name as "IncludeList" for "include"
    Then I select "Users in ALL of these segments" as "EXCLUDE" Segment for search
    Then select Segment having name as "ExcludeList" for "exclude"
    Then I remove "exclusion" segment
    Then I verify "SAVE & CONTINUE" button is "enabled"