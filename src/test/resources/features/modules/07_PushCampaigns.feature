@PushCampaigns @Regression
Feature: Push Campaign Feature

  Scenario Outline: To verify previously created Push campaigns are available in Push Campaigns > List of campaigns page
    Given I navigate to "Push" page via "Channels"
    Then I add "<CampaignName>" to searchbox
    And I verify that "<CampaignName>" and "<CampaignType>" exist

    Examples: 
      | CampaignName                                        | CampaignType  |
      | Test One-time Push Campaign exist: iOS and Android  | One-time      |
      | Test One-time Push Campaign exist: ios              | One-time      |
      | Test One-time Push Campaign exist: Android          | One-time      |
      | Test Triggered Push Campaign exist: iOS and Android | Triggered     |
      | Test Triggered Push Campaign exist: ios             | Triggered     |
      | Test Triggered Push Campaign exist: Android         | Triggered     |
      | Test Recurring Push Campaign exist: iOS and Android | Recurring     |
      | Test Recurring Push Campaign exist: ios             | Recurring     |
      | Test Recurring Push Campaign exist: Android         | Recurring     |
      | Test Transactional Push Campaign exist              | Transactional |

  Scenario: To verify Live Segment via Segment Editor is getting created when One-time Push Campaign type is selected
    Given I navigate to "Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "LiveSegment via Segment Editor-One-time Push Campaign: Text" Campaign Name
    And I select target devices as "android"
    And select specific "Android" apps
      | dummyPushCred |
    And I select New Segment Creation icon
    And I add "Segment Name" as "Push: User Condition > User IDs"
    And Pass following details in user card
      | Visitor Type | All Users   |
      | User IDs     | ends with>P |
    And I Save and verify the created "Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select template as "Text"
    Then I add following details in "Basic" Message Template
      | Title   | Text Template |
      | Message | Text Body     |
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    And verify via "fetchPushCampaign" API whether following "Segments" have been attached to campaign just created
      | Push: User Condition > User IDs |
    Then I navigate to "Live Segments" page via "Segments"
    And I verify "Push: User Condition > User IDs" segment is present
    Given I navigate to "Push" page via "Channels"
    Then check overview page is loaded for "LiveSegment via Segment Editor-One-time Push Campaign: Text" campaign
    Given I navigate to "Push" page via "Channels"
    Then I delete the Campaign with Name "LiveSegment via Segment Editor-One-time Push Campaign: Text"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "Push: User Condition > User IDs"

  @Smoke
  Scenario: To verify Live Segment via Segment Editor is getting created when Triggered Push Campaign type is selected
    Given I navigate to "Push" page via "Channels"
    And I select "Triggered" Campaign Type and enters "LiveSegment via Segment Editor-Triggered Push Campaign: Banner" Campaign Name
    And I select target devices as "ios"
    And I select New Segment Creation icon
    And I add "Segment Name" as "Push: Behavorial Condition > more than 1 condition of Users who did these Events"
    And Pass following details in Behavioural card
      |     | Users who DID these events : | System>User Login    |
      | And | Users who DID these events : | System>App Installed |
    And I Save and verify the created "Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | UPON OCCURRENCE OF | TriggerPushEvent |  |  |
    And I select template as "Banner"
    Then I add following details in "Basic" Message Template
      | Title   | Text Template                                          |
      | Message | Text Body check special characters > ? & * < ! $ - + @ |
    And I add following details in "Image" Message Template
      | Image | https://cdn.pixabay.com/photo/2021/08/25/20/42/field-6574455__480.jpg |
    And I add following details in "Advanced Options" Message Template
      | On-click action: iOS | www.youtube.com |
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    And verify via "fetchPushCampaign" API whether following "Segments" have been attached to campaign just created
      | Push: Behavorial Condition > more than 1 condition of Users who did these Events |
    Then I navigate to "Live Segments" page via "Segments"
    And I verify "Push: Behavorial Condition > more than 1 condition of Users who did these Events" segment is present
    Given I navigate to "Push" page via "Channels"
    Then check overview page is loaded for "LiveSegment via Segment Editor-Triggered Push Campaign: Banner" campaign
    Given I navigate to "Push" page via "Channels"
    Then I delete the Campaign with Name "LiveSegment via Segment Editor-Triggered Push Campaign: Banner"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "Push: Behavorial Condition > more than 1 condition of Users who did these Events"

  Scenario: To verify Live Segment via Segment Editor is getting created when Recurring Push Campaign type is selected
    Given I navigate to "Push" page via "Channels"
    And I select "Recurring" Campaign Type and enters "LiveSegment via Segment Editor-Recurring Push Campaign: Carousel" Campaign Name
    And I select target devices as "both"
    And I select New Segment Creation icon
    And I add "Segment Name" as "Push: Technology Condition > iOS"
    And Pass following details for "iOS" in Technology card
      | Total Time Spent (in seconds) | greater than>100 |
    And I Save and verify the created "Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select template as "Carousel"
    Then I add following details in "Basic" Message Template
      | Title   | Text Template |
      | Message | Text Body     |
    And I add Carousel Images layout of type "LANDSCAPE"
    Then I add following details in "Carousel Images" Message Template
      | Image                     | https://cdn.pixabay.com/photo/2021/08/25/20/42/field-6574455__480.jpg |
      | Label                     | Button                                                                |
      | On-Click Action (Android) | www.youtube.com                                                       |
      | On-Click Action (iOS)     | www.zomato.com                                                        |
    And I add following details in "Advanced Options (Android)" Message having "Carousel" Template
      | On-click action: android | www.youtube2.com |
    And I add following details in "Advanced Options (iOS)" Message having "Carousel" Template
      | On-click action: iOS | www.zomato2.com |
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    And verify via "fetchPushCampaign" API whether following "Segments" have been attached to campaign just created
      | Push: Technology Condition > iOS |
    Then I navigate to "Live Segments" page via "Segments"
    And I verify "Push: Technology Condition > iOS" segment is present
    Given I navigate to "Push" page via "Channels"
    Then check overview page is loaded for "LiveSegment via Segment Editor-Recurring Push Campaign: Carousel" campaign
    Given I navigate to "Push" page via "Channels"
    Then I delete the Campaign with Name "LiveSegment via Segment Editor-Recurring Push Campaign: Carousel"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "Push: Technology Condition > iOS"

  @Smoke
  Scenario: To verify Push campaign creation using Inclusion/Exclusion of segment feature
    Given I navigate to "Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "LiveSegment with Inclusion/Exclusion of segment Push Campaign" Campaign Name
    And I select target devices as "both"
    Then I fill the details in Audience tab as below
      | Audience Type | multiple segments |
    Then I select "Users in ANY of these segments" as "Send To" Inclusion Segment
    And I add "Segment Name" as "Push: User Condition > User ID ends with P"
    And Pass following details in user card
      | Visitor Type | All Users   |
      | User IDs     | ends with>P |
    And Save the Segment
    Then I select "Users in ANY of these segments" as "EXCLUDE" Exclusion Segment
    And I add "Segment Name" as "Push: User Condition > Reachability on Whatsapp"
    And Pass following details in user card
      | Visitor Type | All Users             |
      | Reachability | Reachable on-WhatsApp |
    And Save the Segment
    And I save the Audience details
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select template as "Text"
    Then I add following details in "Basic" Message Template
      | Title   | Text Template |
      | Message | Text Body     |
    And I add following details in "Advanced Options (Android)" Message having "Text" Template
      | On-click action: android | www.youtube2.com     |
      | Label                    | BUTTON               |
      | On-Click Action          | www.insidebutton.com |
    And I add following details in "Advanced Options (iOS)" Message having "Text" Template
      | On-click action: iOS | www.zomato2.com                              |
      | Type of Buttons      | Yes or No (Open app or Dismiss notification) |
      | On-Click Action      | www.insidebutton.com                         |
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    And verify via "fetchPushCampaign" API whether following "Segment" have been attached to campaign just created
      | Push: User Condition > User ID ends with P      |
      | Push: User Condition > Reachability on Whatsapp |
    Then I navigate to "Live Segments" page via "Segments"
    And I verify following Segments are present
      | Push: User Condition > User ID ends with P      |
      | Push: User Condition > Reachability on Whatsapp |
    Given I navigate to "Push" page via "Channels"
    Then check overview page is loaded for "LiveSegment with Inclusion/Exclusion of segment Push Campaign" campaign
    Given I navigate to "Push" page via "Channels"
    Then I delete the Campaign with Name "LiveSegment with Inclusion/Exclusion of segment Push Campaign"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete the following mentioned "segments"
      | Push: User Condition > User ID ends with P      |
      | Push: User Condition > Reachability on Whatsapp |

  @End-to-End @Distributed
  Scenario: To verify Stats are showing properly when One Time Push Campaign type is selected
    Given I navigate to "Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "Sent/Delivered Status-One-time Push Campaign" Campaign Name
    And I select target devices as "both"
    Then select Segment having name as "StatsValidationUser Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select template as "Text"
    Then I add following details in "Basic" Message Template
      | Title   | One Time Push Campaign                                             |
      | Message | Validation of Push Campaign with new Segment as UserID ends with P |
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    And verify via "fetchPushCampaign" API whether following "Segment" have been attached to campaign just created
      | StatsValidationUser Segment |
    Then I navigate to "Live Segments" page via "Segments"
    And I verify "StatsValidationUser Segment" segment is present
    Given I navigate to "Push" page via "Channels"
    Then check overview page is loaded for "Sent/Delivered Status-One-time Push Campaign" campaign

  @End-to-End @Distributed
  Scenario: To verify Stats are showing properly when Triggered Push Campaign type is selected
    Given I navigate to "Push" page via "Channels"
    And I select "Triggered" Campaign Type and enters "Sent/Delivered Status-Triggered Push Campaign" Campaign Name
    And I select target devices as "both"
    Then select Segment having name as "StatsValidationUser Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | UPON OCCURRENCE OF | TriggerPushEvent |  |  |
      | END DATE           | after 3 hours    |  |  |
    And I select template as "Banner"
    Then I add following details in "Basic" Message Template
      | Title   | Triggered Push Campaign                                  |
      | Message | Triggered Campaign for User AutoID1 via triggerPushEvent |
    And I add following details in "Image" Message Template
      | Image | https://cdn.pixabay.com/photo/2021/08/25/20/42/field-6574455__480.jpg |
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then wait for "fetchPushCampaign" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    Then check overview page is loaded for "Sent/Delivered Status-Triggered Push Campaign" campaign
    Given I navigate to "Push" page via "Channels"
    And hit "performEvent" API from "PerformEvents" sheet to trigger Event with reference id "triggerPushEvent"

  @End-to-End @Distributed
  Scenario: To verify Stats are showing properly when Recurring Push Campaign type is selected
    Given I navigate to "Push" page via "Channels"
    And I select "Recurring" Campaign Type and enters "Sent/Delivered Status-Recurring Push Campaign" Campaign Name
    And I select target devices as "both"
    Then select Segment having name as "StatsValidationUser Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | DELIVERY SCHEDULE | Day           |  |  |
      | END DATE          | after 3 hours |  |  |
    Then I adjust the time for Recurring Campaign by checking system time
    And I select template as "Carousel"
    Then I add following details in "Basic" Message Template
      | Title   | Text Template |
      | Message | Text Body     |
    And I add Carousel Images layout of type "LANDSCAPE"
    Then I add following details in "Carousel Images" Message Template
      | Image                     | https://cdn.pixabay.com/photo/2021/08/25/20/42/field-6574455__480.jpg |
      | Label                     | Button                                                                |
      | On-Click Action (Android) | www.youtube.com                                                       |
      | On-Click Action (iOS)     | www.zomato.com                                                        |
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then wait for "fetchPushCampaign" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    Then check overview page is loaded for "Sent/Delivered Status-Recurring Push Campaign" campaign

  @End-to-End @Distributed
  Scenario: To verify Push is send to users and stats are showing properly when Transactional Push Campaign type is selected
    Given I navigate to "Push" page via "Channels"
    And I select "Transactional" Campaign Type and enters "Transactional Push Campaign" Campaign Name
    Then I fill the details in Audience tab as below
      |  |  |
    And I select template as "Rating"
    Then I add following details in "Basic" Message Template
      | Title   | Text Template |
      | Message | Text Body     |
    Then I add following details in "Rating" Message Template
      | Title            | Text Template                                                         |
      | Description      | Text Body                                                             |
      | Background Image | https://cdn.pixabay.com/photo/2021/08/25/20/42/field-6574455__480.jpg |
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Transactional Push Campaign" campaign
    Given I navigate to "Push" page via "Channels"
    When I hit the POST method using static body for "transactionalCampaign" api from "PushCampaigns" sheet with ref id "transactionalPush"

  @disabled
  Scenario: To verify Personalization is shown in Push Campaign when One-Time Push Campaign type is selected
    Given I navigate to "Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "Personalization-One-time Push Campaign" Campaign Name
    And I select target devices as "both"
    And I select New Segment Creation icon
    And I add "Segment Name" as "User Condition > User IDs"
    And Pass following details in user card
      | Visitor Type | All Users   |
      | User IDs     | ends with>P |
    And I Save and verify the created "Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select template as "Text"
    Then I add following details in "Basic" Message Template
      | Title   | Text Template |
      | Message | Text Body     |
    Then I add personalisation details in "Basic" Template
    Then I Save all the Message Details

  @disabled
  Scenario: To verify Personalization is shown in Push Campaign when Triggered Push Campaign type is selected
    Given I navigate to "Push" page via "Channels"
    And I select "Triggered" Campaign Type and enters "Personalisation-Triggered Push Campaign: Banner" Campaign Name
    And I select target devices as "both"
    And I select New Segment Creation icon
    And I add "Segment Name" as "Behavorial Condition > more than 1 condition of Users who did these Events"
    And Pass following details in Behavioural card
      |     | Users who DID these events : | System>User Login    |
      | And | Users who DID these events : | System>App Installed |
    And I Save and verify the created "Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | UPON OCCURRENCE OF | TriggerPushEvent |  |  |
    And I select template as "Banner"
    Then I add following details in "Basic" Message Template
      | Title   | Text Template |
      | Message | Text Body     |
    Then I add personalisation details in "Basic" Template
    And I add following details in "Image" Message Template
      | Image | https://cdn.pixabay.com/photo/2021/08/25/20/42/field-6574455__480.jpg |
    And I add following details in "Advanced Options (Android)" Message having "Banner" Template
      | On-click action: android | www.youtube2.com     |
      | Label                    | BUTTON               |
      | On-Click Action          | www.insidebutton.com |
    Then I add personalisation details in "Advanced Options (Android)" Template
    Then I Save all the Message Details

  @disabled
  Scenario: To verify Personalization is shown in Push Campaign when Recurring Push Campaign type is selected
    Given I navigate to "Push" page via "Channels"
    And I select "Recurring" Campaign Type and enters "Personalisation-Recurring Push Campaign" Campaign Name
    And I select target devices as "both"
    And I select New Segment Creation icon
    And I add "Segment Name" as "Technology Condition > iOS"
    And Pass following details for "iOS" in Technology card
      | Total Time Spent (in seconds) | greater than>100 |
    And I Save and verify the created "Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | DELIVERY SCHEDULE | Day |  |  |
    Then I adjust the time for Recurring Campaign by checking system time
    And I select template as "Carousel"
    Then I add following details in "Basic" Message Template
      | Title   | Text Template |
      | Message | Text Body     |
    Then I add personalisation details in "Basic" Template
    And I add Carousel Images layout of type "LANDSCAPE"
    Then I add following details in "Carousel Images" Message Template
      | Image                     | https://cdn.pixabay.com/photo/2021/08/25/20/42/field-6574455__480.jpg |
      | Label                     | Button                                                                |
      | On-Click Action (Android) | www.youtube.com                                                       |
      | On-Click Action (iOS)     | www.zomato.com                                                        |
    Then I add personalisation details in "Carousel Image" Template

  @Smoke
  Scenario: To verify One-time Push campaign when Delivery Time:Later and Intelligently is selected
    Given I navigate to "Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "One-time Push Campaign with Intelligently Delivery Time" Campaign Name
    And I select target devices as "both"
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below when "One-time" Campaign and "Push" Channel is selected
      | DELIVERY SCHEDULE | later             |  |  |
      | DELIVERY TIME     | sendIntelligently |  |  |
    And I select template as "Text"
    Then I add following details in "Basic" Message Template
      | Title   | Text Template |
      | Message | Text Body     |
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "One-time Push Campaign with Intelligently Delivery Time" campaign
    Given I navigate to "Push" page via "Channels"
    Then I add "One-time Push Campaign with Intelligently Delivery Time" to searchbox
    And I verify following details when "One-time" Campaign having "One-time Push Campaign with Intelligently Delivery Time" Campaign Name and "Push" Channel is selected
      | Delivery Time: Send Intelligently |
    Given I navigate to "Push" page via "Channels"
    Then I delete the Campaign with Name "One-time Push Campaign with Intelligently Delivery Time"

  @Smoke
  Scenario: To verify One-time Push campaign when Delivery Time:Later and Specific Time is selected
    Given I navigate to "Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "One-time Push Campaign with Specific Time Delivery" Campaign Name
    And I select target devices as "both"
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below when "One-time" Campaign and "Push" Channel is selected
      | DELIVERY SCHEDULE | later              |  |  |
      | DELIVERY TIME     | sendAtSpecificTime |  |  |
    And I select template as "Text"
    Then I add following details in "Basic" Message Template
      | Title   | Text Template |
      | Message | Text Body     |
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "One-time Push Campaign with Specific Time Delivery" campaign
    Given I navigate to "Push" page via "Channels"
    Then I add "One-time Push Campaign with Specific Time Delivery" to searchbox
    And I verify following details when "One-time" Campaign having "One-time Push Campaign with Specific Time Delivery" Campaign Name and "Push" Channel is selected
      | Delivery Time: Send at specific time |
    Given I navigate to "Push" page via "Channels"
    Then I delete the Campaign with Name "One-time Push Campaign with Specific Time Delivery"

  Scenario: Verify existing tags is attached to Push Campaign
    Given I navigate to "Push" page via "Channels"
    And I search for "Push Campaign: Tag" campaign
    Then I verify "campaigntag" tag is attached to the "Push Campaign: Tag" campaign

  @Smoke
  Scenario: Verify new tag is created from Push Channel and gets attached to newly created push Campaign
    Given I navigate to "Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "Tag: Push Campaign" Campaign Name
    And create new tag from with name as "push-tag" for campaign
    And I select target devices as "android"
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select template as "Text"
    Then I add following details in "Basic" Message Template
      | Title   | Text Template |
      | Message | Text Body     |
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Tag: Push Campaign" campaign
    Given I navigate to "Push" page via "Channels"
    Then I verify "push-tag" tag is attached to the "Tag: Push Campaign" campaign
    Given I navigate to "Push" page via "Channels"
    And I search for "Tag: Push Campaign" campaign
    Then delete the tag with name as "push-tag" from campaign "Tag: Push Campaign"
    Then I delete the Campaign with Name "Tag: Push Campaign"

  Scenario: To verify data of 1 line in title and 7 line in description is seen on push received on device when multiple line are added in message tab
    Given I navigate to "Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "Message Limit Check campaign" Campaign Name
    And I select target devices as "both"
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select template as "Text"
    Then fill the page via "MultiLinePushTemplate" template from "PushTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Message Limit Check campaign" campaign
    Given I navigate to "Push" page via "Channels"
    Then I add "Message Limit Check campaign" to searchbox
    Given I navigate to "Push" page via "Channels"
    Then I delete the Campaign with Name "Message Limit Check campaign"

  @MobileSDKSuite
  Scenario: SDK Testing: To verify contents are delivered on mobile for Push Text
    Given I navigate to "Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "SDK Delivery One-time Push Campaign type Text" Campaign Name
    And I select target devices as "android"
    Then select Segment having name as "AndroidSDKUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "PushTemplate1" template from "PushTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "SDK Delivery One-time Push Campaign type Text" campaign
    Given I navigate to "Push" page via "Channels"
    Then wait "12" minutes for "pushAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent       | 1 |
      | deliveries | 1 |
    And export the template for Validation on Mobile Device

  @MobileSDKSuite
  Scenario: SDK Testing: To verify contents are delivered on mobile for Push Banner
    Given I navigate to "Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "SDK Delivery One-time Push Campaign type Banner" Campaign Name
    And I select target devices as "android"
    Then select Segment having name as "AndroidSDKUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "PushTemplate2" template from "PushTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "SDK Delivery One-time Push Campaign type Banner" campaign
    Given I navigate to "Push" page via "Channels"
    Then wait "12" minutes for "pushAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent       | 1 |
      | deliveries | 1 |
    And export the template for Validation on Mobile Device

  @MobileSDKSuite
  Scenario: SDK Testing: To verify contents are delivered on mobile for Push Carousel
    Given I navigate to "Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "SDK Delivery One-time Push Campaign type Carousel" Campaign Name
    And I select target devices as "android"
    Then select Segment having name as "AndroidSDKUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "PushTemplate3" template from "PushTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then wait "default" minutes for "pushAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent       | 1 |
      | deliveries | 1 |
    And export the template for Validation on Mobile Device

  @MobileSDKSuite
  Scenario: SDK Testing: To verify contents are delivered on mobile for Push Rating
    Given I navigate to "Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "SDK Delivery One-time Push Campaign type Rating" Campaign Name
    And I select target devices as "android"
    Then select Segment having name as "AndroidSDKUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "PushTemplate4" template from "PushTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then wait "default" minutes for "pushAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent       | 1 |
      | deliveries | 1 |
    And export the template for Validation on Mobile Device

  @Smoke
  Scenario: To verify test variation/campaign is working fine by updating existing Test Segment for PUSH
    Given I navigate to "Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "Test Page Validation for Push" Campaign Name
    And I select target devices as "ios"
    And select specific "iOS" apps
      | com.webEngage.prodSwift |
    Then select Segment having name as "StatsValidationUser Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select template as "Text"
    Then I add following details in "Basic" Message Template
      | Title   | One Time Push Campaign                                             |
      | Message | Validation of Push Campaign with new Segment as UserID ends with J |
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    Then "create" test campaign with following parameters
      | Segment Name | UserAutoID1 |
      | Filters      | User ID     |
      | values       | AutoID1     |
    Then "edit" test campaign with following parameters
      | Segment Name | StatsValidationUserTest |
      | Filters      | User ID                 |
      | values       | StatsValidationUser     |
    And test the "Push" Campaign for segment "TEST-StatsValidationUserTest" and expect the status to be "SENT"
    Then delete Test Segment with name as "TEST-StatsValidationUserTest"
    And I launch the Campaign
    Then check overview page is loaded for "Test Page Validation for Push" campaign
    Given I navigate to "Push" page via "Channels"
    Then I delete the Campaign with Name "Test Page Validation for Push"

  Scenario: To verify overview page is loaded when campaign status is paused for Recurring Push
    Given I navigate to "Push" page via "Channels"
    And I select "Recurring" Campaign Type and enters "Push-Recurring-Overview Page Validation for Paused status" Campaign Name
    And I select target devices as "both"
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | DELIVERY SCHEDULE | Day |  |  |
    Then I adjust the time for Recurring Campaign by checking system time
    And I select template as "Carousel"
    Then I add following details in "Basic" Message Template
      | Title   | Text Template |
      | Message | Text Body     |
    And I add Carousel Images layout of type "LANDSCAPE"
    Then I add following details in "Carousel Images" Message Template
      | Image                     | https://cdn.pixabay.com/photo/2021/08/25/20/42/field-6574455__480.jpg |
      | Label                     | Button                                                                |
      | On-Click Action (Android) | www.youtube.com                                                       |
      | On-Click Action (iOS)     | www.zomato.com                                                        |
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I pause the campaign with "push-deactivate" api
    Then check overview page is loaded for "Push-Recurring-Overview Page Validation for Paused status" campaign
    Given I navigate to "Push" page via "Channels"
    Then I delete the Campaign with Name "Push-Recurring-Overview Page Validation for Paused status"

  Scenario: To verify overview page is loaded when campaign status is in draft state for Transactional Push
    Given I navigate to "Push" page via "Channels"
    And I select "Transactional" Campaign Type and enters "Push-Transactional-Overview Page Validation for draft status" Campaign Name
    Then I fill the details in Audience tab as below
      |  |  |
    And I select template as "Rating"
    Then I add following details in "Basic" Message Template
      | Title   | Text Template |
      | Message | Text Body     |
    Then I add following details in "Rating" Message Template
      | Title            | Text Template                                                         |
      | Description      | Text Body                                                             |
      | Background Image | https://cdn.pixabay.com/photo/2021/08/25/20/42/field-6574455__480.jpg |
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    Given I navigate to "Push" page via "Channels"
    Then check overview page is loaded for "Push-Transactional-Overview Page Validation for draft status" campaign
    Given I navigate to "Push" page via "Channels"
    Then I delete the Campaign with Name "Push-Transactional-Overview Page Validation for draft status"

  @MobileSDKSuite
  Scenario: SDK Testing: To verify unicode characterset contents are delivered on mobile for Push Text
    Given I navigate to "Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "Unicode:SDK Delivery One-time Push Campaign type Text ^ _ ` { | } ~ ¢ £ ¤ \"" Campaign Name
    And I select target devices as "android"
    Then select Segment having name as "AndroidSDKUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "UnicodePushTemplateText" template from "MultiLingualTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    And I verify "Unicode:SDK Delivery One-time Push Campaign type Text ^ _ ` { | } ~ ¢ £ ¤ \"" Campaign name with unicode characters on overview page
    Given I navigate to "Push" page via "Channels"
    Then wait "12" minutes for "pushAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent       | 1 |
      | deliveries | 1 |
    And export the template for Validation on Mobile Device

  @MobileSDKSuite
  Scenario: SDK Testing: To verify unicode characterset contents are delivered on mobile for Push Banner
    Given I navigate to "Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "Unicode:SDK Delivery One-time Push Campaign type Banner ڝ ڞ ڟ ڠ ڡ अ आ इ" Campaign Name
    And I select target devices as "android"
    Then select Segment having name as "AndroidSDKUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "UnicodePushTemplateBanner" template from "MultiLingualTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    And I verify "Unicode:SDK Delivery One-time Push Campaign type Banner ڝ ڞ ڟ ڠ ڡ अ आ इ" Campaign name with unicode characters on overview page
    Given I navigate to "Push" page via "Channels"
    Then wait "12" minutes for "pushAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent       | 1 |
      | deliveries | 1 |
    And export the template for Validation on Mobile Device

  @MobileSDKSuite
  Scenario: SDK Testing: To verify unicode characterset contents are delivered on mobile for Push Carousel
    Given I navigate to "Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "Unicode:SDK Delivery One-time Push Campaign type Carousel ஆ இ ஈ અ આ ઇ అ ఆ ఇ ఈ" Campaign Name
    And I select target devices as "android"
    Then select Segment having name as "AndroidSDKUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "UnicodePushTemplateCarousel" template from "MultiLingualTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    And I verify "Unicode:SDK Delivery One-time Push Campaign type Carousel ஆ இ ஈ અ આ ઇ అ ఆ ఇ ఈ" Campaign name with unicode characters on overview page
    Given I navigate to "Push" page via "Channels"
    Then wait "default" minutes for "pushAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent       | 1 |
      | deliveries | 1 |
    And export the template for Validation on Mobile Device

  @MobileSDKSuite
  Scenario: SDK Testing: To verify unicode characterset contents are delivered on mobile for Push Rating
    Given I navigate to "Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "Unicode:SDK Delivery One-time Push Campaign type Rating ആ ഇ ഈ ฃ ค ฅ ฆ ༂ ༃ ༄ ༅" Campaign Name
    And I select target devices as "android"
    Then select Segment having name as "AndroidSDKUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "UnicodePushTemplateRating" template from "MultiLingualTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    And I verify "Unicode:SDK Delivery One-time Push Campaign type Rating ആ ഇ ഈ ฃ ค ฅ ฆ ༂ ༃ ༄ ༅" Campaign name with unicode characters on overview page
    Given I navigate to "Push" page via "Channels"
    Then wait "default" minutes for "pushAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent       | 1 |
      | deliveries | 1 |
    And export the template for Validation on Mobile Device

  Scenario: To verify on staticListListing page for singlelistcampaigns count is shown as 1 or not
    Given I navigate to "Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "Push-SingleSegment ListCount" Campaign Name
    And I select target devices as "both"
    Then select Segment having name as "WebSDKUserList"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select template as "Text"
    Then I add following details in "Basic" Message Template
      | Title   | One Time Push Campaign |
      | Message | Test single list       |
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Given I navigate to "Lists" page via "Segments"
    And I search for "WebSDKUserList" segment
    Then I save the SegmentId of "WebSDKUserList" segment
    Then wait for "staticListCampaignCount" api to return response as "1" for path as "response.data[0].count"
    Given I navigate to "Push" page via "Channels"
    Then I delete the Campaign with Name "Push-SingleSegment ListCount"

  Scenario: To verify on staticListListing page for Multiplelistcampaigns count is shown as 1 or not for each list
    Given I navigate to "Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "Push-MultipleSegment ListCount" Campaign Name
    And I select target devices as "both"
    Then I fill the details in Audience tab as below
      | Audience Type | multiple segments |
    Then I select "Users in ALL of these segments" as "Send To" Segment for search
    Then select multiple segments for "include" section
      | StaticSegments Tag |
      | UsersList          |
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select template as "Text"
    Then I add following details in "Basic" Message Template
      | Title   | One Time Push Campaign |
      | Message | Test single list       |
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Given I navigate to "Lists" page via "Segments"
    And I search for "StaticSegments Tag" segment
    Then I save the SegmentId of "StaticSegments Tag" segment
    Then wait for "staticListCampaignCount" api to return response as "1" for path as "response.data[0].count"
    And I search for "UsersList" segment
    Then I save the SegmentId of "UsersList" segment
    Then wait for "staticListCampaignCount" api to return response as "1" for path as "response.data[0].count"
    Given I navigate to "Push" page via "Channels"
    Then I delete the Campaign with Name "Push-MultipleSegment ListCount"
