@CustomAlerts @Regression
Feature: Alerts feature

  Scenario: To verify user can cancelAlertCreation, resetFilter, filtercount and create and verify ShowDetails block alert without filter
    Given I navigate to "Alerts" page via "Data Platform"
    Then fill the page via "temporaryAlert" template from "AlertTemplatess" folder using "CreateAlert" section
    Then I verify the alert "Temporary alert" gets listed in alerts list
    Then I verify Details in Show Details block using template "temporaryAlert"

  Scenario: To Verify Edit, subscribe, unsubscribe, pause, resume Alerts using below data
    Given I navigate to "Alerts" page via "Data Platform"
    And I "Copy" custom alert with name "Temporary alert" and verify the same
    Then I "Edit" custom alert with name "Temporary alert" and verify the same
    And I "Subscribe" custom alert with name "Edited Temporary alert" and verify the same
    And I "Unsubscribe" custom alert with name "Edited Temporary alert" and verify the same
    Then I "Pause" custom alert with name "Edited Temporary alert" and verify the same
    Then I "Resume" custom alert with name "Edited Temporary alert" and verify the same
    And I "Delete" custom alert with name "Edited Temporary alert" and verify the same
    And I "Delete" custom alert with name "Duplicate Temporary alert" and verify the same

  Scenario Outline: To verify that user is able create alert with name <AlertName> for multiple filters and with advanced split by options
    Given I navigate to "Alerts" page via "Data Platform"
    Then fill the page via "<AlertTemplate>" template from "AlertTemplates" folder using "CreateAlert" section
    Then I verify the alert "<AlertName>" gets listed in alerts list
    And I "Delete" custom alert with name "<AlertName>" and verify the same

    Examples: 
      | AlertName                           | AlertTemplate   |
      | OccNum&StrEql>DlyAbvSpfcVa          | alertTemplate1  |
      | OccNum&StrNEql>DlyAbvPrevD          | alertTemplate2  |
      | OccNum&Str1of>DlyAbvAvg             | alertTemplate3  |
      | OccNum&StrNof>DlyAbvSame            | alertTemplate4  |
      | OccNumLesser&StrStrts>DlyBlwSpfcVa  | alertTemplate5  |
      | OccNumGreater&StrNStrts>DlyBlwPrevD | alertTemplate6  |
      | OccNumLsEq&StrEnds>DlyBlwAvg        | alertTemplate7  |
      | UnqNumGrtEq&StrNEnds>DlyBlwSame     | alertTemplate8  |
      | OccNumBtw&StrCont>HrlyAbvPrev       | alertTemplate9  |
      | OccNumNtBtw&StrNCont>HrlyAbovAvg    | alertTemplate10 |

  Scenario Outline: To verify that user is able create alert with name <AlertName> for multiple filters with OR logic
    Given I navigate to "Alerts" page via "Data Platform"
    Then fill the page via "<AlertTemplate>" template from "AlertTemplates" folder using "CreateAlert" section
    Then I verify the alert "<AlertName>" gets listed in alerts list
    And I "Delete" custom alert with name "<AlertName>" and verify the same

    Examples: 
      | AlertName                             | AlertTemplate   |
      | UnqNumORStrEmpt>HrlyAbovSameD         | alertTemplate11 |
      | UnqNumORStrNEmpt>HrlyAbovSameHYstr    | alertTemplate12 |
      | UnqNumEqtoORStrRegx>HrlyBloSameHSameD | alertTemplate13 |
      | UnqNumNtEqORStrNRegx>HrlyBloSameHYstr | alertTemplate14 |
      | OccBoolEqORDatAf>WklyAbovprev         | alertTemplate15 |
      | UnqBoolNEqORDatBfr>WklyBloprev        | alertTemplate16 |
      | UnqBoolEmtORDatLstNxt>WklyAboAvg      | alertTemplate17 |
      | OccBoolNEmtORDatNPr>WklybloAvg        | alertTemplate18 |

  Scenario Outline: To verify that user is able create alert with name <AlertName> for DAU,MAU,WAU moniter metrics
    Given I navigate to "Alerts" page via "Data Platform"
    Then fill the page via "<AlertTemplate>" template from "AlertTemplates" folder using "CreateAlert" section
    Then I verify the alert "<AlertName>" gets listed in alerts list
    And I "Delete" custom alert with name "<AlertName>" and verify the same

    Examples: 
      | AlertName         | AlertTemplate   |
      | Mau>DlyAboSpecVal | alertTemplate19 |
      | Mau>DlyBloPrev    | alertTemplate20 |
      | Wau>DlyAboSameDay | alertTemplate21 |
      | Wau>DlyBloSpecVal | alertTemplate22 |
      | Dau>DlyAboPrev    | alertTemplate23 |
      | Dau>DlyBloSameDay | alertTemplate24 |

  Scenario Outline: To verify that Frequency and Status filters show correct Alerts list
    Given I navigate to "Alerts" page via "Data Platform"
    Then I apply filters and verify the list for the filters
      | Frequency | <frequency> |
      | Status    | <status>    |

    Examples: 
      | frequency | status       |
      | Hourly    | All          |
      | Daily     | All          |
      | Weekly    | All          |
      | All       | Subscribed   |
      | All       | Unsubscribed |
      | Hourly    | Subscribed   |
      | Weekly    | Unsubscribed |
      | Daily     | Unsubscribed |
