@CustomDashboard @Regression
Feature: Custom Dashboard Feature

  Scenario: To verify unicode characterset contents are showing properly when new dashboard is created
    Given I navigate to "Dashboards" Page
    And create new Dashboard with name as "Unicode:$ ! # % & ( ) * + , - . ' / : ; < = > ? @ [ ] ^ _ ` { } ~"
    And select dashboard visibility as "All team members can edit" and create a dashboard
    Then verify "Unicode:$ ! # % & ( ) * + , - . ' / : ; < = > ? @ [ ] ^ _ ` { } ~" name which has unicode characters on Overview page for "Dashboards"
    Given I navigate to "Dashboards" Page
    Then delete the module with Name "Unicode:$ ! # % & ( ) * + , - . ' / : ; < = > ? @ [ ] ^ _ ` { } ~"

  Scenario: Verify if user is able to edit an existing Dashboard via listing page
    Given I navigate to "Dashboards" Page
    And create new Dashboard with name as "AutomationCustomDashboard"
    And select dashboard visibility as "All team members can edit" and create a dashboard
    Then I verify "AutomationCustomDashboard" dashboard name which has visibility set as "All team members can edit"
    Given I navigate to "Dashboards" Page
    Then I edit dashboard "AutomationCustomDashboard" name as "EditedAutomationCustomDashboard" and select dashboard visibility as "Just Me"
    Then I verify "EditedAutomationCustomDashboard" dashboard name which has visibility set as "Just me"
    Given I navigate to "Dashboards" Page
    Then delete the module with Name "EditedAutomationCustomDashboard"

  Scenario: Verify if user is able to delete existing dashboard via Dashboard Page
    Given I navigate to "Dashboards" Page
    And create new Dashboard with name as "DeleteDashboard"
    And select dashboard visibility as "All team members can edit" and create a dashboard
    And I navigate to "Dashboards" Page
    Then I open "DeleteDashboard" dashboard
    Then I "Delete" the dashboard "DeleteDashboard" via overview page

  Scenario: Verify if user is able to add team members in view access and edit access
    Given I navigate to "Dashboards" Page
    And create new Dashboard with name as "DashboardMemberAccess"
    And select dashboard visibility as "Specific team members"
    Then I add the following Users for different AccessMode
      | View Access | Rajesh |
      | Edit Access |        |
    Then I open "DashboardMemberAccess" dashboard
    Then I verify the user "Rajesh" for following AccessMode
      | View Access | YES |
      | Edit Access | NO  |
    Then I "Edit" the dashboard "DashboardMemberAccess" via overview page
    Then I add the following Users for different AccessMode
      | View Access |        |
      | Edit Access | Rajesh |
    Given I navigate to "Dashboards" Page
    Then I open "DashboardMemberAccess" dashboard
    Then I verify the user "Rajesh" for following AccessMode
      | View Access | YES |
      | Edit Access | YES |
    Given I navigate to "Dashboards" Page
    Then delete the module with Name "DashboardMemberAccess"

  Scenario: To verify that after clicking on Add Total user Card and known user card option it should add card and show stats as per selected card from dashboard page
    Given I navigate to "Dashboards" Page
    And create new Dashboard with name as "AutomationTestDashboard"
    And select dashboard visibility as "All team members can edit" and create a dashboard
    Then I redirect to "Users" page form Dashboard "AutomationTestDashboard"
    Then I pin "TOTAL USERS" and enter card name "Total Users" and select dashboard "AutomationTestDashboard"
    Given I navigate to "Dashboards" Page
    Then Verify "Total Users" user card stats of dashboard "AutomationTestDashboard"
    Given I navigate to "Users" Page
    Then I pin "KNOWN USERS" and enter card name "Known Users " and select dashboard "AutomationTestDashboard"
    Given I navigate to "Dashboards" Page
    Then Verify "Known Users" user card stats of dashboard "AutomationTestDashboard"
    Given I navigate to "Dashboards" Page
    Then delete the module with Name "AutomationTestDashboard"

  Scenario: To verify the working of Delete and edit icon in Card Action it should allow to delete and edit the selected card
    Given I navigate to "Dashboards" Page
    And create new Dashboard with name as "AutomationEditDashboardCard"
    And select dashboard visibility as "All team members can edit" and create a dashboard
    Then I redirect to "Users" page form Dashboard "AutomationEditDashboardCard"
    Then I pin "TOTAL USERS" and enter card name "Total Users" and select dashboard "AutomationEditDashboardCard"
    Given I navigate to "Dashboards" Page
    Then Verify "Total Users" user card stats of dashboard "AutomationEditDashboardCard"
    Then I "Rename" the card "Total Users"
    Then I "Delete" the card "Renamed Total Users"
    Given I navigate to "Dashboards" Page
    Then delete the module with Name "AutomationEditDashboardCard"
