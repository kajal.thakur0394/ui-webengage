@ViberCampaigns @Regression
Feature: Viber Campaign feature

  @Smoke
  Scenario: To verify Live Segment via Segment Editor is getting created when One-time Viber Campaign type is selected
    Given I navigate to "Viber" page via "Channels"
    And I select "One-time" Campaign Type and enters "Viber Campaign with text template" Campaign Name
    And I select New Segment Creation icon
    And I add "Segment Name" as "Viber: User Condition > User IDs"
    And Pass following details in user card
      | Visitor Type | All Users   |
      | User IDs     | ends with>J |
    And I Save and verify the created "Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "Viber" template from "Viber" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Given I navigate to "Viber" page via "Channels"
    Then I delete the Campaign with Name "Viber Campaign with text template"
    Given I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "Viber: User Condition > User IDs"

  @Smoke
  Scenario: To verify Send Now Campaign CRUD - Viber
    Given I navigate to "Viber" page via "Channels"
    And I select "One-time" Campaign Type and enters "Verify Viber campaign CRUD" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "Viber" template from "Viber" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Verify Viber campaign CRUD" campaign
    Given I navigate to "Viber" page via "Channels"
    Then I delete campaign with name "Verify Viber campaign CRUD"
