@MessagePersonalisation @Regression
Feature: Message Personalisation Feature

  @Smoke
  Scenario: To verify Message Personalisation in SMS Campaign
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "Message Personalised SMS Campaign" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "SMSPersonalisation" template from "MessagePersonalisationTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Given I navigate to "SMS" page via "Channels"
    Then I delete the Campaign with Name "Message Personalised SMS Campaign"

  @Smoke @skip @issue:WP-16124
  Scenario: To verify Message Personalisation including catalogs and recommendations in SMS Campaign
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "Message Personalised CR SMS Campaign" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then I select the  Service provider "SSP" with name "SMS Campaign SP"
    Then fill the page via "SMSCRPersonalisation" template from "MessagePersonalisationTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Given I navigate to "SMS" page via "Channels"
    Then I delete the Campaign with Name "Message Personalised SMS Campaign"

  @Smoke @skip @issue:WP-16124
  Scenario: To verify Message Personalisation in Email Campaign including catalogs and recommendations using rich text
    Given I navigate to "Email" page via "Channels"
    And I select "One-time" Campaign Type and enters "Message Personalised rich text Email Campaign" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "EmailRichTextPersonalisation" template from "MessagePersonalisationTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I delete the Campaign with Name "Message Personalised rich text Email Campaign"

  Scenario: To verify Message Personalisation in Email Campaign using raw HTML
    Given I navigate to "Email" page via "Channels"
    And I select "One-time" Campaign Type and enters "Message Personalised raw HTML Email Campaign" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "EmailRawHTMLPersonalisation" template from "MessagePersonalisationTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I delete the Campaign with Name "Message Personalised raw HTML Email Campaign"

  @Smoke @skip @issue:WP-16124
  Scenario: To verify Message Personalisation including catalogs and recommendations in Whatsapp Campaign using text template
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "One-time" Campaign Type and enters "Message Personalised text template WhatsApp Campaign" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "WhatsappTextTempPersonalisation" template from "MessagePersonalisationTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I delete the Campaign with Name "Message Personalised text template WhatsApp Campaign"

  Scenario: To verify Message Personalisation in Whatsapp Campaign using document template
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "One-time" Campaign Type and enters "Message Personalised document template WhatsApp Campaign" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then I select WA Template having name "user_details__185af7e9-8fef-4abb-9530-37cd037d71d0" with WSP name "WA Cred"
    Then fill the page via "WhatsappDocumentTempPersonalisation" template from "MessagePersonalisationTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I delete the Campaign with Name "Message Personalised document template WhatsApp Campaign"

  Scenario: To verify Message Personalisation in Whatsapp Campaign using image template
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "One-time" Campaign Type and enters "Message Personalised image template WhatsApp Campaign" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then I select WA Template having name "media_template_image__aa207dd8-1718-4ef1-85e6-be1531df9643`" with WSP name "WA Cred"
    Then fill the page via "WhatsappImageTempPersonalisation" template from "MessagePersonalisationTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I delete the Campaign with Name "Message Personalised image template WhatsApp Campaign"

  @Smoke @skip @issue:WP-16124
  Scenario: To verify Message Personalisation including catalogs and recommendations in Push Campaign using text template
    Given I navigate to "Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "Message Personalised text template Push Campaign" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "PushTextTempPersonalisation" template from "MessagePersonalisationTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I delete the Campaign with Name "Message Personalised text template Push Campaign"

  Scenario: To verify Message Personalisation in Push Campaign using banner template
    Given I navigate to "Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "Message Personalised banner template Push Campaign" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "PushBannerTempPersonalisation" template from "MessagePersonalisationTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I delete the Campaign with Name "Message Personalised banner template Push Campaign"

  Scenario: To verify Message Personalisation in Push Campaign using carousel template
    Given I navigate to "Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "Message Personalised carousel template Push Campaign" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "PushCarouselTempPersonalisation" template from "MessagePersonalisationTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I delete the Campaign with Name "Message Personalised carousel template Push Campaign"

  Scenario: To verify Message Personalisation in Push Campaign using rating template
    Given I navigate to "Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "Message Personalised rating template Push Campaign" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "PushRatingTempPersonalisation" template from "MessagePersonalisationTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I delete the Campaign with Name "Message Personalised rating template Push Campaign"

  @Smoke @skip @issue:WP-16124
  Scenario: To verify Message Personalisation including catalogs and recommendations in Inline Campaign using banner template
    Given I navigate to "In-line Content" page via "Web Personalization"
    And I select "Banner" Campaign Type and enters "Message Personalised banner template In-line" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    And save the campaign
    Then select property "button_Test (body > button)" and "Add after the property" as property placement
    Then select page to show as "All Pages"
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "InlineWebPBannerTempPersonalisation" template from "MessagePersonalisationTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then I delete the Campaign with Name "Message Personalised banner template In-line"

  @WebSDKSuite
  Scenario: To verify Message Personalisation in Inline Campaign using customcode template
    Given I navigate to "In-line Content" page via "Web Personalization"
    And I select "Custom Code" Campaign Type and enters "Message Personalised customcode template In-line" Campaign Name
    Then select Segment having name as "WebSDKUser"
    And save the campaign
    Then select property "test_login (#loginBtn)" and "Add after the property" as property placement
    Then select page to show as "All Pages"
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "InlineWebPCustomCodeTempPersonalisation" template from "MessagePersonalisationTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then I fetch "fetchInlineContentWebP" api and add dynamic CampaignId
    Then wait for "fetchInlineContentWebP" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    Then check overview page is loaded for "Message Personalised customcode template In-line" campaign
    Given I navigate to "In-line Content" page via "Web Personalization"
    Then I verify personalised message on dynamic webpage
    Given I navigate to "In-line Content" page via "Web Personalization"
    Then I delete the Campaign with Name "Message Personalised customcode template In-line"

  @Smoke @skip @issue:WP-16124
  Scenario: To verify Message Personalisation including catalogs and recommendations in InApp Campaign using header template
    Given I navigate to "In-app Notifications" page via "App Personalization"
    And I select "" Campaign Type and enters "Message Personalised header template InApp" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "InAppHeaderTempPersonalisation" template from "MessagePersonalisationTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Given I navigate to "In-app Notifications" page via "App Personalization"
    Then I delete the Campaign with Name "Message Personalised header template InApp"

  Scenario: To verify Message Personalisation in InApp Campaign using Footer template
    Given I navigate to "In-app Notifications" page via "App Personalization"
    And I select "" Campaign Type and enters "Message Personalised footer template InApp" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "InAppFooterTempPersonalisation" template from "MessagePersonalisationTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Given I navigate to "In-app Notifications" page via "App Personalization"
    Then I delete the Campaign with Name "Message Personalised footer template InApp"

  Scenario: To verify Message Personalisation in InApp Campaign using PopOut template
    Given I navigate to "In-app Notifications" page via "App Personalization"
    And I select "" Campaign Type and enters "Message Personalised Popout template InApp" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "InAppPopoutTempPersonalisation" template from "MessagePersonalisationTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Given I navigate to "In-app Notifications" page via "App Personalization"
    Then I delete the Campaign with Name "Message Personalised Popout template InApp"

  @Smoke
  Scenario: To verify Message Personalisation in Web Push Campaign using Banner template
    Given I navigate to "Web Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "Message Personalised One-time Web Push Campaign - Banner" Campaign Name
    Then select Segment for Web Push
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "WebPushBannerPersonalisation" template from "MessagePersonalisationTemplates" folder using "Message" section
    Given I navigate to "Web Push" page via "Channels"
    Then I delete the Campaign with Name "Message Personalised One-time Web Push Campaign - Banner"

  Scenario: To verify Message Personalisation in Web Push Campaign using Text template
    Given I navigate to "Web Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "Message Personalised One-time Web Push Campaign - Text" Campaign Name
    Then select Segment for Web Push
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select template as "Text"
    Then fill the page via "WebPushTextPersonalisation" template from "MessagePersonalisationTemplates" folder using "Message" section
    Given I navigate to "Web Push" page via "Channels"
    Then I delete the Campaign with Name "Message Personalised One-time Web Push Campaign - Text"

  @Smoke
  Scenario: To verify Message Personalisation in InlineApp Campaign using Banner template
    Given I navigate to "In-line Content" page via "App Personalization"
    And I select "" Campaign Type and enters "Message Personalised App-in-Line campaign: Banner" Campaign Name
    Then select "Android only (Android: S1P1)" as property
    Then select Segment having name as "AndroidSDKUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | SET TIMEOUT | 5000 |  |  |
    Then I select "Banner View" layout
    Then fill the page via "InlineAppBannerPersonalisation" template from "MessagePersonalisationTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Given I navigate to "In-line Content" page via "App Personalization"
    Then I delete the Campaign with Name "Message Personalised App-in-Line campaign: Banner"

  Scenario: To verify Message Personalisation in InlineApp Campaign using Text template
    Given I navigate to "In-line Content" page via "App Personalization"
    And I select "" Campaign Type and enters "Message Personalised App-in-Line campaign: Text" Campaign Name
    Then select "Android only (Android: S1P1)" as property
    Then select Segment having name as "AndroidSDKUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | SET TIMEOUT | 5000 |  |  |
    Then I select "Text View" layout
    Then fill the page via "InlineAppTextPersonalisation" template from "MessagePersonalisationTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Given I navigate to "In-line Content" page via "App Personalization"
    Then I delete the Campaign with Name "Message Personalised App-in-Line campaign: Text"

  Scenario: To verify Message Personalisation in InlineApp Campaign using Custom template
    Given I navigate to "In-line Content" page via "App Personalization"
    And I select "" Campaign Type and enters "Message Personalised App-in-Line campaign: Custom" Campaign Name
    Then select "Custom View (Android: flPersonalize4)" as property
    Then select Segment having name as "AndroidSDKUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | SET TIMEOUT | 5000 |  |  |
    Then I select "Custom View" layout
    Then fill the page via "InlineAppCustomPersonalisation" template from "MessagePersonalisationTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Given I navigate to "In-line Content" page via "App Personalization"
    Then I delete the Campaign with Name "Message Personalised App-in-Line campaign: Custom"

  @Smoke
  Scenario: To verify Message Personalisation across Journey blocks for SMS, Email, and Whatsapp campaigns
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "Message Personalised 1"
    Then I use the Journey/Relay Template "journey22"
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page

  Scenario: To verify Message Personalisation across Journey blocks for Push, WebPush, and InApp campaigns
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "Message Personalised 2"
    Then I use the Journey/Relay Template "journey23"
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page
