@WebPushCampaign @Regression
Feature: Web Push Campaign feature

  @Smoke
  Scenario: To verify Send Now Campaign CRUD - Web Push
    Given I navigate to "Web Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "Verify Web Push campaign CRUD" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select template as "Text"
    Then fill the page via "WebPush" template from "WebPushTemplates" folder using "Basic" section
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Verify Web Push campaign CRUD" campaign
    Given I navigate to "Web Push" page via "Channels"
    Then I delete the Campaign with Name "Verify Web Push campaign CRUD"

  Scenario: To verify Triggered Web Push campaign when Start Date: Now and End Date: Till is selected
    Given I navigate to "Web Push" page via "Channels"
    And I select "Triggered" Campaign Type and enters "Triggered Web Push Campaign with Start Date:Now and End Date:Till" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | UPON OCCURRENCE OF | TriggerWebPushEvent |  |  |
      | START DATE         | now                 |  |  |
      | END DATE           | till                |  |  |
    And I select template as "Text"
    Then fill the page via "WebPush" template from "WebPushTemplates" folder using "Basic" section
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Triggered Web Push Campaign with Start Date:Now and End Date:Till" campaign
    Given I navigate to "Web Push" page via "Channels"
    Then I add "Triggered Web Push Campaign with Start Date:Now and End Date:Till" to searchbox
    And I verify following details for "Triggered Web Push Campaign with Start Date:Now and End Date:Till" campaign
      | Start date: |
      | End date:   |
    Given I navigate to "Web Push" page via "Channels"
    Then I delete the Campaign with Name "Triggered Web Push Campaign with Start Date:Now and End Date:Till"

  @Smoke
  Scenario: To verify Triggered Web Push campaign when Start Date: Later and End Date: Till is selected
    Given I navigate to "Web Push" page via "Channels"
    And I select "Triggered" Campaign Type and enters "Triggered Web Push Campaign with Start Date:Later and End Date:Till" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | UPON OCCURRENCE OF | TriggerWebPushEvent |  |  |
      | START DATE         | later               |  |  |
      | END DATE           | till                |  |  |
    And I select template as "Text"
    Then fill the page via "WebPush" template from "WebPushTemplates" folder using "Basic" section
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Triggered Web Push Campaign with Start Date:Later and End Date:Till" campaign
    Given I navigate to "Web Push" page via "Channels"
    Then I add "Triggered Web Push Campaign with Start Date:Later and End Date:Till" to searchbox
    And I verify following details for "Triggered Web Push Campaign with Start Date:Later and End Date:Till" campaign
      | Start date: |
      | End date:   |
    Given I navigate to "Web Push" page via "Channels"
    Then I delete the Campaign with Name "Triggered Web Push Campaign with Start Date:Later and End Date:Till"

  Scenario: To verify Triggered Web Push campaign when Start Date: Later and End Date: Never is selected
    Given I navigate to "Web Push" page via "Channels"
    And I select "Triggered" Campaign Type and enters "Triggered Web Push Campaign with Start Date:Later and End Date:Never" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | UPON OCCURRENCE OF | TriggerWebPushEvent |  |  |
      | START DATE         | later               |  |  |
      | END DATE           | never               |  |  |
    And I select template as "Text"
    Then fill the page via "WebPush" template from "WebPushTemplates" folder using "Basic" section
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Triggered Web Push Campaign with Start Date:Later and End Date:Never" campaign
    Given I navigate to "Web Push" page via "Channels"
    Then I add "Triggered Web Push Campaign with Start Date:Later and End Date:Never" to searchbox
    And I verify following details for "Triggered Web Push Campaign with Start Date:Later and End Date:Never" campaign
      | Start date: |
      | End date:   |
    Given I navigate to "Web Push" page via "Channels"
    Then I delete the Campaign with Name "Triggered Web Push Campaign with Start Date:Later and End Date:Never"

  @Smoke
  Scenario: To verify Triggered Web Push campaign when Start Date: Now and End Date: Never is selected
    Given I navigate to "Web Push" page via "Channels"
    And I select "Triggered" Campaign Type and enters "Triggered Web Push Campaign with Start Date:Now and End Date:Never" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | UPON OCCURRENCE OF | TriggerWebPushEvent |  |  |
      | START DATE         | now                 |  |  |
      | END DATE           | never               |  |  |
    And I select template as "Text"
    Then fill the page via "WebPush" template from "WebPushTemplates" folder using "Basic" section
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Triggered Web Push Campaign with Start Date:Now and End Date:Never" campaign
    Given I navigate to "Web Push" page via "Channels"
    Then I add "Triggered Web Push Campaign with Start Date:Now and End Date:Never" to searchbox
    And I verify following details for "Triggered Web Push Campaign with Start Date:Now and End Date:Never" campaign
      | Start date: |
      | End date:   |
    Given I navigate to "Web Push" page via "Channels"
    Then I delete the Campaign with Name "Triggered Web Push Campaign with Start Date:Now and End Date:Never"

  @Smoke
  Scenario: To verify Recurring Web Push campaign when Delivery Schedule: 1/Day and End Date: Never is selected
    Given I navigate to "Web Push" page via "Channels"
    And I select "Recurring" Campaign Type and enters "Recurring Web Push Campaign with Delivery Schedule:1/Day and End Date:Never" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | DELIVERY SCHEDULE | Day   |  |  |
      | END DATE          | never |  |  |
    And I select template as "Text"
    Then fill the page via "WebPush" template from "WebPushTemplates" folder using "Basic" section
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Recurring Web Push Campaign with Delivery Schedule:1/Day and End Date:Never" campaign
    Given I navigate to "Web Push" page via "Channels"
    Then I add "Recurring Web Push Campaign with Delivery Schedule:1/Day and End Date:Never" to searchbox
    And I verify following details for "Recurring Web Push Campaign with Delivery Schedule:1/Day and End Date:Never" campaign
      | Delivery Schedule |
      | End date:         |
    Given I navigate to "Web Push" page via "Channels"
    Then I delete the Campaign with Name "Recurring Web Push Campaign with Delivery Schedule:1/Day and End Date:Never"

  @Smoke
  Scenario: To verify Recurring Web Push campaign when Delivery Schedule: 1/Day and End Date: Till is selected
    Given I navigate to "Web Push" page via "Channels"
    And I select "Recurring" Campaign Type and enters "Recurring Web Push Campaign with Delivery Schedule:1/Day and End Date:Till" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | DELIVERY SCHEDULE | Day  |  |  |
      | END DATE          | till |  |  |
    And I select template as "Text"
    Then fill the page via "WebPush" template from "WebPushTemplates" folder using "Basic" section
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Recurring Web Push Campaign with Delivery Schedule:1/Day and End Date:Till" campaign
    Given I navigate to "Web Push" page via "Channels"
    Then I add "Recurring Web Push Campaign with Delivery Schedule:1/Day and End Date:Till" to searchbox
    And I verify following details for "Recurring Web Push Campaign with Delivery Schedule:1/Day and End Date:Till" campaign
      | End date:         |
      | Delivery Schedule |
    Given I navigate to "Web Push" page via "Channels"
    Then I delete the Campaign with Name "Recurring Web Push Campaign with Delivery Schedule:1/Day and End Date:Till"

  @Smoke
  Scenario: To verify Recurring Web Push campaign when Delivery Schedule: 1/Week and End Date: Never is selected
    Given I navigate to "Web Push" page via "Channels"
    And I select "Recurring" Campaign Type and enters "Recurring Web Push Campaign with Delivery Schedule:1/Week and End Date:Never" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | DELIVERY SCHEDULE | Week  |  | M W F |
      | END DATE          | never |  |       |
    And I select template as "Text"
    Then fill the page via "WebPush" template from "WebPushTemplates" folder using "Basic" section
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Recurring Web Push Campaign with Delivery Schedule:1/Week and End Date:Never" campaign
    Given I navigate to "Web Push" page via "Channels"
    Then I add "Recurring Web Push Campaign with Delivery Schedule:1/Week and End Date:Never" to searchbox
    And I verify following details for "Recurring Web Push Campaign with Delivery Schedule:1/Week and End Date:Never" campaign
      | End date:         |
      | Delivery Schedule |
    Given I navigate to "Web Push" page via "Channels"
    Then I delete the Campaign with Name "Recurring Web Push Campaign with Delivery Schedule:1/Week and End Date:Never"

  Scenario: To verify Recurring Web Push campaign when Delivery Schedule: 1/Month and End Date: Till is selected
    Given I navigate to "Web Push" page via "Channels"
    And I select "Recurring" Campaign Type and enters "Recurring Web Push Campaign with Delivery Schedule:1/Month and End Date:Till" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | DELIVERY SCHEDULE | Month | Specific Dates | 1st 2nd 4th 25th |
      | END DATE          | till  |                |                  |
    And I select template as "Text"
    Then fill the page via "WebPush" template from "WebPushTemplates" folder using "Basic" section
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Recurring Web Push Campaign with Delivery Schedule:1/Month and End Date:Till" campaign
    Given I navigate to "Web Push" page via "Channels"
    Then I add "Recurring Web Push Campaign with Delivery Schedule:1/Month and End Date:Till" to searchbox
    And I verify following details for "Recurring Web Push Campaign with Delivery Schedule:1/Month and End Date:Till" campaign
      | End date:         |
      | Delivery Schedule |
    Given I navigate to "Web Push" page via "Channels"
    Then I delete the Campaign with Name "Recurring Web Push Campaign with Delivery Schedule:1/Month and End Date:Till"

  @Smoke
  Scenario: To verify Recurring Web Push campaign when Delivery Schedule: 1/Month and End Date: Never is selected
    Given I navigate to "Web Push" page via "Channels"
    And I select "Recurring" Campaign Type and enters "Recurring Web Push Campaign with Delivery Schedule:1/Month and End Date:Never" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | DELIVERY SCHEDULE | Month | Specific Days | M W F |
      | END DATE          | never |               |       |
    And I select template as "Text"
    Then fill the page via "WebPush" template from "WebPushTemplates" folder using "Basic" section
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "Recurring Web Push Campaign with Delivery Schedule:1/Month and End Date:Never" campaign
    Given I navigate to "Web Push" page via "Channels"
    Then I add "Recurring Web Push Campaign with Delivery Schedule:1/Month and End Date:Never" to searchbox
    And I verify following details for "Recurring Web Push Campaign with Delivery Schedule:1/Month and End Date:Never" campaign
      | End date:         |
      | Delivery Schedule |
    Given I navigate to "Web Push" page via "Channels"
    Then I delete the Campaign with Name "Recurring Web Push Campaign with Delivery Schedule:1/Month and End Date:Never"

  Scenario: To verify overview page is loaded when campaign status is paused for Triggered Web Push
    Given I navigate to "Web Push" page via "Channels"
    And I select "Triggered" Campaign Type and enters "WebPush-Triggered-Overview Page Validation for Paused status" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | UPON OCCURRENCE OF | TriggerWebPushEvent |  |  |
    And I select template as "Text"
    Then fill the page via "WebPush" template from "WebPushTemplates" folder using "Basic" section
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I pause the campaign with "webpush-deactivate" api
    Then check overview page is loaded for "WebPush-Triggered-Overview Page Validation for Paused status" campaign
    Given I navigate to "Web Push" page via "Channels"
    Then I delete the Campaign with Name "WebPush-Triggered-Overview Page Validation for Paused status"

  Scenario: To verify overview page is loaded when campaign status is in draft state for Recurring Web Push
    Given I navigate to "Web Push" page via "Channels"
    And I select "Recurring" Campaign Type and enters "WebPush-Recurring-Overview Page Validation for draft status" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select template as "Text"
    Then fill the page via "WebPush" template from "WebPushTemplates" folder using "Basic" section
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    Given I navigate to "Web Push" page via "Channels"
    Then check overview page is loaded for "WebPush-Recurring-Overview Page Validation for draft status" campaign
    Given I navigate to "Web Push" page via "Channels"
    Then I delete the Campaign with Name "WebPush-Recurring-Overview Page Validation for draft status"

  @End-to-End @Distributed
  Scenario: To verify Stats are showing properly when One-time Web Push Campaign - Text type is selected
    Given I verify pre-execution checks for Web Push
    Given I navigate to "Web Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "Sent/Delivered Status-One-time Web Push Campaign - Text" Campaign Name
     Then select Segment for Web Push
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | QUEUEING | false |  |  |
    And I select template as "Text"
    Then fill the page via "WebPush1" template from "WebPushTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then wait for "fetchWebPushCampaign" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    Then check overview page is loaded for "Sent/Delivered Status-One-time Web Push Campaign - Text" campaign

  @End-to-End @Distributed
  Scenario: To verify Stats are showing properly when Transactional Web Push Campaign - Text type is selected
    Given I verify pre-execution checks for Web Push
    Given I navigate to "Web Push" page via "Channels"
    And I select "Transactional" Campaign Type and enters "Sent/Delivered Status Transactional Web Push Campaign - Text" Campaign Name
    Then I fill the details in Audience tab as below
      |  |  |
    And I select template as "Text"
    Then fill the page via "WebPush1" template from "WebPushTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then wait for "fetchWebPushCampaign" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    Then check overview page is loaded for "Sent/Delivered Status Transactional Web Push Campaign - Text" campaign
    Given I navigate to "Web Push" page via "Channels"
    When I hit the POST method using static body for "transactionalCampaign" api from "WebPushCampaign" sheet with ref id "transactionalWebPush"

  @End-to-End @Distributed
  Scenario: To verify Stats are showing properly when One-time Web Push Campaign - Banner type is selected
    Given I verify pre-execution checks for Web Push
    Given I navigate to "Web Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "Sent/Delivered Status-One-time Web Push Campaign - Banner" Campaign Name
    Then select Segment for Web Push
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | QUEUEING | false |  |  |
    And I select template as "Banner"
    Then fill the page via "WebPush3" template from "WebPushTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then wait for "fetchWebPushCampaign" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    Then check overview page is loaded for "Sent/Delivered Status-One-time Web Push Campaign - Banner" campaign

  @End-to-End @Distributed
  Scenario: To verify Stats are showing properly when Transactional Web Push Campaign - Banner type is selected
    Given I verify pre-execution checks for Web Push
    Given I navigate to "Web Push" page via "Channels"
    And I select "Transactional" Campaign Type and enters "Sent/Delivered Status Transactional Web Push Campaign - Banner" Campaign Name
    Then I fill the details in Audience tab as below
      |  |  |
    And I select template as "Banner"
    Then fill the page via "WebPush4" template from "WebPushTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then wait for "fetchWebPushCampaign" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    Then check overview page is loaded for "Sent/Delivered Status Transactional Web Push Campaign - Banner" campaign
    Given I navigate to "Web Push" page via "Channels"
    When I hit the POST method using static body for "transactionalCampaign" api from "WebPushCampaign" sheet with ref id "transactionalWebPush"

  Scenario: To verify emoji is present in fields when One-time Web Push is selected for Banner Layout
    Given I navigate to "Web Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "One-Time Emoji Web Push Banner campaign" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select template as "Banner"
    Then fill the page via "WebPushBannerEmoji" template from "WebPushTemplates" folder using "Message" section
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "One-Time Emoji Web Push Banner campaign" campaign
    Given I navigate to "Web Push" page via "Channels"
    Then I delete the Campaign with Name "One-Time Emoji Web Push Banner campaign"

  Scenario: To verify emoji is present in fields when One-time Web Push is selected for Text Layout
    Given I navigate to "Web Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "One-Time Emoji Web Push Text campaign" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select template as "Text"
    Then fill the page via "WebPushTextEmoji" template from "WebPushTemplates" folder using "Message" section
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then check overview page is loaded for "One-Time Emoji Web Push Text campaign" campaign
    Given I navigate to "Web Push" page via "Channels"
    Then I delete the Campaign with Name "One-Time Emoji Web Push Text campaign"

  Scenario: To verify unicode characterset contents are showing properly for 'One-Time' Web Push Campaign for 'Banner' Layout
    Given I navigate to "Web Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "Unicode: One-Time Web Push Banner campaign ℌ ℍ ℎ ℏ ℐ ℑ ℒ ℓ ℔ ℕ №" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    And I select template as "Banner"
    Then fill the page via "UnicodeWebPushBanner" template from "MultiLingualTemplates" folder using "Message" section
    Then I Save all the Message Details
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    And I verify "Unicode: One-Time Web Push Banner campaign ℌ ℍ ℎ ℏ ℐ ℑ ℒ ℓ ℔ ℕ №" Campaign name with unicode characters on overview page
    Given I navigate to "Web Push" page via "Channels"
    Then I delete the Campaign with unicode characters "Unicode: One-Time Web Push Banner campaign ℌ ℍ ℎ ℏ ℐ ℑ ℒ ℓ ℔ ℕ №"
