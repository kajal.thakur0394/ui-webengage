@InAppCampaigns @Regression
Feature: In App Campaigns

  @MobileSDKSuite
  Scenario: To verify contents are showing properly for In App campaign Header layout
    Given I navigate to "In-app Notifications" page via "App Personalization"
    And I select "" Campaign Type and enters "InApp Campaign: Header layout" Campaign Name
    And I select target devices as "android"
    Then select Segment having name as "AndroidSDKUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | SHOW LIMIT | 1 |  |  |
    Then fill the page via "InApp1" template from "InAppTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then wait for "fetchInAppNotif" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    And export the template for Validation on Mobile Device

  @MobileSDKSuite
  Scenario: To verify contents are showing properly for In App campaign Popout Modal layout
    Given I navigate to "In-app Notifications" page via "App Personalization"
    And I select "" Campaign Type and enters "InApp Campaign: Popout Modal" Campaign Name
    And I select target devices as "android"
    Then select Segment having name as "AndroidSDKUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | SHOW LIMIT | 1 |  |  |
    Then fill the page via "InApp16" template from "InAppTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then wait for "fetchInAppNotif" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    And export the template for Validation on Mobile Device

  @Smoke
  Scenario: Verify Campaigns is getting created for future date
    Given I navigate to "In-app Notifications" page via "App Personalization"
    And I select "" Campaign Type and enters "InApp Campaign-Background URL" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | SHOW LIMIT |     1 |  |  |
      | START DATE | later |  |  |
      | END DATE   | never |  |  |
    Then fill the page via "InApp2" template from "InAppTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then I delete the Campaign with Name "InApp Campaign-Background URL"

  @Smoke
  Scenario: Verify campaign gets created when icon and background upload option is selected
    Given I navigate to "In-app Notifications" page via "App Personalization"
    And I select "" Campaign Type and enters "InApp Campaign-Background upload" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | SHOW LIMIT |    1 |  |  |
      | START DATE | now  |  |  |
      | END DATE   | till |  |  |
    Then fill the page via "InApp3" template from "InAppTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then I delete the Campaign with Name "InApp Campaign-Background upload"

  @Smoke
  Scenario: Verify existing tag is attached to InApp campaigns
    Given I navigate to "In-app Notifications" page via "App Personalization"
    And I search for "In-App Campaign: Tag" campaign
    Then I verify "campaigntag" tag is attached to the "In-App Campaign: Tag" campaign

  @Smoke
  Scenario: Verify new tag is created and attached to InApp campaign
    Given I navigate to "In-app Notifications" page via "App Personalization"
    And I select "" Campaign Type and enters "Tag: In-App Campaign" Campaign Name
    And I select target devices as "android"
    And create new tag from with name as "inapp-tag" for campaign
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "InApp1" template from "InAppTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then check overview page is loaded for "Tag: In-App Campaign" campaign
    Given I navigate to "In-app Notifications" page via "App Personalization"
    Then I verify "inapp-tag" tag is attached to the "Tag: In-App Campaign" campaign
    Given I navigate to "In-app Notifications" page via "App Personalization"
    And I search for "Tag: In-App Campaign" campaign
    Then delete the tag with name as "inapp-tag" from campaign "Tag: In-App Campaign"
    Then I delete the Campaign with Name "Tag: In-App Campaign"

  @Smoke
  Scenario: Create Segment via InApp Campaign CRUD page and verify whether it gets attached to the Campaign via API
    Given I navigate to "In-app Notifications" page via "App Personalization"
    And I select "" Campaign Type and enters "Segment via InApp Campaign CRUD page" Campaign Name
    And I select New Segment Creation icon
    And I add "Segment Name" as "In App Segment User Condition > Reachability"
    And Pass following details in user card
      | Visitor Type | All Users             |
      | Reachability | Reachable on-WhatsApp |
    And I Save and verify the created "Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "InApp2" template from "InAppTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    And verify via "fetchInAppNotif" API whether following "Segment" have been attached to campaign just created
      | In App Segment User Condition > Reachability |
    Then I navigate to "Live Segments" page via "Segments"
    And I verify "In App Segment User Condition > Reachability" segment is present
    Given I navigate to "In-app Notifications" page via "App Personalization"
    Then check overview page is loaded for "Segment via InApp Campaign CRUD page" campaign
    Given I navigate to "In-app Notifications" page via "App Personalization"
    Then I delete the Campaign with Name "Segment via InApp Campaign CRUD page"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete segment with Name "In App Segment User Condition > Reachability"

  @Smoke
  Scenario: To verify In App campaign creation using inclusion/exclusion of segment feature
    Given I navigate to "In-app Notifications" page via "App Personalization"
    And I select "" Campaign Type and enters "Segment with Inclusion/Exclusion of segment InApp Campaign" Campaign Name
    Then I fill the details in Audience tab as below
      | Audience Type | multiple segments |
    Then I select "Users in ANY of these segments" as "Send To" Inclusion Segment
    And I add "Segment Name" as "In-app: User Condition > User ID ends with Z"
    And Pass following details in user card And I Save the "segment"
      | Visitor Type | All Users   |
      | User IDs     | ends with>Z |
    Then I select "Users in ANY of these segments" as "EXCLUDE" Exclusion Segment
    And I add "Segment Name" as "In-app: User Condition > Reachability on Whatsapp"
    And Pass following details in user card And I Save the "segment"
      | Visitor Type | All Users             |
      | Reachability | Reachable on-WhatsApp |
    And I save the Audience details
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "InApp2" template from "InAppTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    And verify via "fetchInAppNotif" API whether following "Segment" have been attached to campaign just created
      | In-app: User Condition > User ID ends with Z      |
      | In-app: User Condition > Reachability on Whatsapp |
    Then I navigate to "Live Segments" page via "Segments"
    And I verify following Segments are present
      | In-app: User Condition > User ID ends with Z      |
      | In-app: User Condition > Reachability on Whatsapp |
    Given I navigate to "In-app Notifications" page via "App Personalization"
    Then check overview page is loaded for "Segment with Inclusion/Exclusion of segment InApp Campaign" campaign
    Given I navigate to "In-app Notifications" page via "App Personalization"
    Then I delete the Campaign with Name "Segment with Inclusion/Exclusion of segment InApp Campaign"
    And I navigate to "Live Segments" page via "Segments"
    Then I delete the following mentioned "segments"
      | In-app: User Condition > User ID ends with Z      |
      | In-app: User Condition > Reachability on Whatsapp |

  @Smoke
  Scenario: Verify campaign with layout as footer gets successfully created for Android
    Given I navigate to "In-app Notifications" page via "App Personalization"
    And I select "" Campaign Type and enters "In-App Campaign: Footer" Campaign Name
    And I select target devices as "android"
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "InApp5" template from "InAppTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Given I navigate to "In-app Notifications" page via "App Personalization"
    Then I delete the Campaign with Name "In-App Campaign: Footer"

  @Smoke
  Scenario: Verify campaign with layout as header gets successfully created for Android
    Given I navigate to "In-app Notifications" page via "App Personalization"
    And I select "" Campaign Type and enters "In-App Campaign: Header" Campaign Name
    And I select target devices as "android"
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "InApp6" template from "InAppTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Given I navigate to "In-app Notifications" page via "App Personalization"
    Then I delete the Campaign with Name "In-App Campaign: Header"

  @Smoke
  Scenario: Verify  InApp Campaign using Full Screen Layout in Preset mode
    Given I navigate to "In-app Notifications" page via "App Personalization"
    And I select "" Campaign Type and enters "Full Screen Modal" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "InApp7" template from "InAppTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Given I navigate to "In-app Notifications" page via "App Personalization"
    Then I delete the Campaign with Name "Full Screen Modal"

  @Smoke
  Scenario: Verify  InApp Campaign using Full Screen Layout in Custom HTML
    Given I navigate to "In-app Notifications" page via "App Personalization"
    And I select "" Campaign Type and enters "Full Screen Modal Custom Html" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "InApp8" template from "InAppTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Given I navigate to "In-app Notifications" page via "App Personalization"
    Then I delete the Campaign with Name "Full Screen Modal Custom Html"

  @Smoke
  Scenario: Verify  InApp Campaign using Classic Layout in Preset mode
    Given I navigate to "In-app Notifications" page via "App Personalization"
    And I select "" Campaign Type and enters "Classic Modal Preset Template" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "InApp9" template from "InAppTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Given I navigate to "In-app Notifications" page via "App Personalization"
    Then I delete the Campaign with Name "Classic Modal Preset Template"

  @Smoke
  Scenario: Verify  InApp Campaign using Classic Layout in Custom HTML mode
    Given I navigate to "In-app Notifications" page via "App Personalization"
    And I select "" Campaign Type and enters "Classic Modal Custom Html" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "InApp10" template from "InAppTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Given I navigate to "In-app Notifications" page via "App Personalization"
    Then I delete the Campaign with Name "Classic Modal Custom Html"

  @Smoke
  Scenario: Verification of InApp campaign HtmlBody character limit and ChangeTemplate PopUp
    Given I navigate to "In-app Notifications" page via "App Personalization"
    And I select "" Campaign Type and enters "Full Screen Modal Char limit and PopUp Verification" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "InApp11" template from "InAppTemplates" folder using "Message" section
    Then I verify character limit error
    Then I click "Preset" and verify PopUp Warning message
    Given I navigate to "In-app Notifications" page via "App Personalization"
    Then I delete the Campaign with Name "Full Screen Modal Char limit and PopUp Verification"

  @Smoke
  Scenario: To verify Image Height option is not present in Full-screen layout and has numeric value in Custom layout
    Given I navigate to "In-app Notifications" page via "App Personalization"
    And I select "" Campaign Type and enters "InApp Image Height Verification" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then I switch the Layout to "Full Screen Modal"
    Then I verify "Image Height" toggle button is "not visible"
    Then I switch the Layout to "Classic Modal"
    Then I verify Image hieght has numeric value
    Then fill the page via "InApp13" template from "InAppTemplates" folder using "Message" section
    Given I navigate to "In-app Notifications" page via "App Personalization"
    Then I delete the Campaign with Name "InApp Image Height Verification"

  @Smoke
  Scenario: To verify Description and Title is optional in Classic and Full-screen Modal
    Given I navigate to "In-app Notifications" page via "App Personalization"
    And I select "" Campaign Type and enters "InApp Title and Description Optional Verification" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "InApp12" template from "InAppTemplates" folder using "Message" section
    Then save the campaign
    Then I select tab "Message" in campaign crud page
    Then I switch the Layout to "Classic Modal"
    Then fill the page via "InApp13" template from "InAppTemplates" folder using "Message" section
    Then save the campaign
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Given I navigate to "In-app Notifications" page via "App Personalization"
    Then I delete the Campaign with Name "InApp Title and Description Optional Verification"

  @Smoke
  Scenario: To verify if either Image, Title or Description field is present then it should allow to proceed in Classic Modal & Full screen modal
    Given I navigate to "In-app Notifications" page via "App Personalization"
    And I select "" Campaign Type and enters "InApp Manadatory Field Verification" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "InApp14" template from "InAppTemplates" folder using "Message" section
    Then save the campaign
    Then I select tab "Message" in campaign crud page
    Then I switch the Layout to "Classic Modal"
    Then fill the page via "InApp15" template from "InAppTemplates" folder using "Message" section
    Then save the campaign
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Given I navigate to "In-app Notifications" page via "App Personalization"
    Then I delete the Campaign with Name "InApp Manadatory Field Verification"

  @MobileSDKSuite
  Scenario: To verify unicode characterset contents are showing properly for In App campaign Header layout
    Given I navigate to "In-app Notifications" page via "App Personalization"
    And I select "" Campaign Type and enters "Unicode:InApp Campaign: Header layout ℹ ℺ ℽ ℾ ℿ ⅀ ⅁ ⅂ ⅃" Campaign Name
    And I select target devices as "android"
    Then select Segment having name as "AndroidSDKUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | SHOW LIMIT | 1 |  |  |
    Then fill the page via "UnicodeInAppHeader" template from "MultiLingualTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    And I verify "Unicode:InApp Campaign: Header layout ℹ ℺ ℽ ℾ ℿ ⅀ ⅁ ⅂ ⅃" Campaign name with unicode characters on overview page
    Then wait for "fetchInAppNotif" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    And export the template for Validation on Mobile Device

  @MobileSDKSuite
  Scenario: To verify unicode characterset contents are showing properly for In App campaign Popout Modal layout
    Given I navigate to "In-app Notifications" page via "App Personalization"
    And I select "" Campaign Type and enters "Unicode:InApp Campaign: Popout Modal ⅄ ⅅ ⅆ ⅇ ⅈ ⅉ ⅊ ⅋ ခ ဂ" Campaign Name
    And I select target devices as "android"
    Then select Segment having name as "AndroidSDKUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | SHOW LIMIT | 1 |  |  |
    Then fill the page via "UnicodeInAppPopout" template from "MultiLingualTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    And I verify "Unicode:InApp Campaign: Popout Modal ⅄ ⅅ ⅆ ⅇ ⅈ ⅉ ⅊ ⅋ ခ ဂ" Campaign name with unicode characters on overview page
    Then wait for "fetchInAppNotif" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    And export the template for Validation on Mobile Device
