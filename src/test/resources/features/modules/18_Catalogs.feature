@Catalogs @Regression
Feature: Catalogs feature
  Scenario: To verify unicode characterset contents are showing properly when new Catalog is created
    Given I navigate to "Catalogs" Page
    Then fill the Configuration details using below values
      | CATALOG NAME  | Unicode:⅂ ⅃ ⅄ ⅅ ⅆ ⅇ ⅈ ⅉ ⅊ ⅋ |
      | CATALOG TYPE  | E-Commerce                  |
      | UPLOAD METHOD | DIRECT                      |
    Then fill the Mapping details using below values
      | item id | product_id |
      | title   | title      |
      | link    | web_link   |
    Then fill the Preview details using below values
      |  |  |
    Then map events to catalogs using below details and save the mapping for "Unicode:⅂ ⅃ ⅄ ⅅ ⅆ ⅇ ⅈ ⅉ ⅊ ⅋" catalog
      | Added to Cart | Product ID |
    Then verify "Unicode:⅂ ⅃ ⅄ ⅅ ⅆ ⅇ ⅈ ⅉ ⅊ ⅋" name which has unicode characters on Overview page for "Catalogs"
    Given I navigate to "Catalogs" Page
    Then delete the module with Name "Unicode:⅂ ⅃ ⅄ ⅅ ⅆ ⅇ ⅈ ⅉ ⅊ ⅋"
