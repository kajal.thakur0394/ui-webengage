@OnsiteNotification @Regression
Feature: On-site Notification Feature

  Scenario Outline: To verify previously created onsite-notification campaigns are available in onsite-notificationt > List of campaigns page
    Given I navigate to "On-site Notifications" page via "Web Personalization"
    Then I add "<CampaignName>" to the searchbox
    And I verify that "<CampaignName>" and the "<LayoutType>" exist

    Examples: 
      | CampaignName                                    | LayoutType |
      | Test create your own: Banner layout exist       | Box        |
      | Test choose from gallery: CartAbandonment exist | Callout    |

  @WebSDKSuite
  Scenario: Verify on webpage that onsite-notification is getting delivered when ‘Create your own’ Notification is selected
    Given I navigate to "On-site Notifications" page via "Web Personalization"
    Then create a " Create Your Own " Notification and select "Box" template
    Then use the personalization template "onsite" for "On-site Notifications" web-p
    And open the dynamic webpage and login
    Then verify notification on a web page using template
    Then delete the notification using template "onsite"

  @WebSDKSuite
  Scenario Outline: Verify on webpage that uploadImage onsite-notification is getting delivered with <OnsiteLayout>
    Given I navigate to "On-site Notifications" page via "Web Personalization"
    Then create a " Create Your Own " Notification and select "<OnsiteLayout>" template
    Then use the personalization template "<Template>" for "On-site Notifications" web-p
    And open the dynamic webpage and login
    Then verify notification on a web page using template
    Then delete the notification using template "<Template>"

    Examples: 
      | OnsiteLayout | Template                |
      | Web Header   | OnsiteHeaderUploadImage |
      | Web Footer   | OnsiteFooterUploadImage |
      | Banner       | BannerUploadImage       |

  @WebSDKSuite
  Scenario Outline: Verify on webpage that imageURL onsite-notification is getting delivered with <OnsiteLayout>
    Given I navigate to "On-site Notifications" page via "Web Personalization"
    Then create a " Create Your Own " Notification and select "<OnsiteLayout>" template
    Then use the personalization template "<Template>" for "On-site Notifications" web-p
    And open the dynamic webpage and login
    Then verify notification on a web page using template
    Then delete the notification using template "<Template>"

    Examples: 
      | OnsiteLayout | Template        |
      | Banner       | BannerURL       |
      | Web Header   | OnsiteHeaderURL |
      | Web Footer   | OnsiteFooterURL |

  Scenario: Verify existing tag is successfully present on campaign
    Given I navigate to "On-site Notifications" page via "Web Personalization"
    And I add "On-site Campaign: Tag" to the searchbox
    And I verify that "On-site Campaign: Tag" and the "Box" exist
    And I add "On-site Campaign: Tag" to the searchbox
    And Verify Campaign tag name as "campaigntag" for "On-site Campaign: Tag" notification campaign

  Scenario: Verify new tag is successfully attached
    Given I navigate to "On-site Notifications" page via "Web Personalization"
    When create a " Create Your Own " Notification and select "Web Header" template
    And I enter the notification campaign name as "Test Demo Notification" and activate the campaign
    And I navigate to "On-site Notifications" page via "Web Personalization"
    And I add "Test Demo Notification" to the searchbox
    And I create new tag as "onsite-tag" and attached to the notification campaign "Test Demo Notification"
    Then Verify Campaign tag name as "onsite-tag" for "Test Demo Notification" notification campaign
    And I delete the notification campaign with Name "Test Demo Notification"

  Scenario: Verify new Segment is attached to onsite-campaign when Create your own Notification is selected
    Given I navigate to "On-site Notifications" page via "Web Personalization"
    When create a " Create Your Own " Notification and select "Web Header" template
    And I set the campaign name as "Demo On-site Notifications" and proceed ahead
    And I create a new segment by using Traffic Segment as "Segment Data"
      | Visitor Type | All Users       |
      | User IDs     | contains>QWERTY |
    And Save the Segment
    And I activate the notification campaign
    And I navigate to "On-site Notifications" page via "Web Personalization"
    And I add "Demo On-site Notifications" to the searchbox
    And I open the "Demo On-site Notifications" notification campaign
    Then I check the "Segment Data" segment is attached to the "Demo On-site Notifications" notification campaign
    And I navigate to "On-site Notifications" page via "Web Personalization"
    And I delete the notification campaign with Name "Demo On-site Notifications"
    And I navigate to "Live Segments" page via "Segments"
    And I delete segment with Name "Segment Data"

  Scenario: Verify new Segment is attached to onsite-campaign when Choose from Gallery Notification is selected
    Given I navigate to "On-site Notifications" page via "Web Personalization"
    When create a "Choose from Gallery" Notification and select "Pop your customer care contact details to visitors dropping off" template
    And I set the campaign name as "Gallery On-site Notifications" and proceed ahead
    And I create a new segment by using Traffic Segment as "Segment contains>QWERTY"
      | Visitor Type | All Users       |
      | User IDs     | contains>QWERTY |
    And Save the Segment
    And I activate the notification campaign
    And I navigate to "On-site Notifications" page via "Web Personalization"
    And I add "Gallery On-site Notifications" to the searchbox
    And I open the "Gallery On-site Notifications" notification campaign
    Then I check the "Segment contains>QWERTY" segment is attached to the "Gallery On-site Notifications" notification campaign
    And I navigate to "On-site Notifications" page via "Web Personalization"
    And I delete the notification campaign with Name "Gallery On-site Notifications"
    And I navigate to "Live Segments" page via "Segments"
    And I delete segment with Name "Segment contains>QWERTY"

  Scenario: Verify existing Segment is attached to onsite-campaign when Create your own Notification is selected
    Given I navigate to "On-site Notifications" page via "Web Personalization"
    When create a " Create Your Own " Notification and select "Web Header" template
    And I set the campaign name as "Notifications User IDs ends with J" and proceed ahead
    And I select a existing segment from traffic segment as user condition "User Condition > User IDs> ends with J"
    And I activate the notification campaign
    And I navigate to "On-site Notifications" page via "Web Personalization"
    And I add "Notifications User IDs ends with J" to the searchbox
    And I open the "Notifications User IDs ends with J" notification campaign
    And I check the "User Condition > User IDs> ends with J" segment is attached to the "Notifications User IDs ends with J" notification campaign
    And I navigate to "On-site Notifications" page via "Web Personalization"
    And I delete the notification campaign with Name "Notifications User IDs ends with J"

  Scenario: Verify campaign gets duplicated when Create your own Notification is selected
    Given I navigate to "On-site Notifications" page via "Web Personalization"
    When create a " Create Your Own " Notification and select "Web Header" template
    And I enter the notification campaign name as "Test Demo Notification" and activate the campaign
    And I navigate to "On-site Notifications" page via "Web Personalization"
    And I add "Test Demo Notification" to the searchbox
    And I create a duplicate campaign of "Test Demo Notification"
    And I navigate to "On-site Notifications" page via "Web Personalization"
    Then Verify the duplicate campaign is created as "Test Demo Notification"
    When I update the campaign name from "Test Demo Notification" to "Updated Test Demo Notification"
    And I navigate to "On-site Notifications" page via "Web Personalization"
    Then I delete the notification campaign with Name "Updated Test Demo Notification"
    Then I delete the notification campaign with Name "Test Demo Notification"

  Scenario: Verify campaign gets duplicated when Choose from Gallery Notification is selected
    Given I navigate to "On-site Notifications" page via "Web Personalization"
    When create a "Choose from Gallery" Notification and select "Pop your customer care contact details to visitors dropping off" template
    And I set the campaign name as "Gallery On-site Notifications" and proceed ahead
    And I activate the notification campaign
    And I navigate to "On-site Notifications" page via "Web Personalization"
    And I add "Gallery On-site Notifications" to the searchbox
    And I create a duplicate campaign of "Gallery On-site Notifications"
    And I navigate to "On-site Notifications" page via "Web Personalization"
    Then Verify the duplicate campaign is created as "Gallery On-site Notifications"
    And I add "Gallery On-site Notifications" to the searchbox
    And I update the campaign name from "Gallery On-site Notifications" to "Updated Gallery On-site Notifications"
    And I navigate to "On-site Notifications" page via "Web Personalization"
    And I delete the notification campaign with Name "Updated Gallery On-site Notifications"
    And I delete the notification campaign with Name "Gallery On-site Notifications"

  Scenario: Verify campaign gets deactivated successfully when Create your own Notification is selected
    Given I navigate to "On-site Notifications" page via "Web Personalization"
    When create a " Create Your Own " Notification and select "Web Header" template
    And I enter the notification campaign name as "Deactivate Create Your Own Campaign" and activate the campaign
    And I navigate to "On-site Notifications" page via "Web Personalization"
    And I add "Deactivate Create Your Own Campaign" to the searchbox
    And I "Inactive" the "Deactivate Create Your Own Campaign" campaign
    Then Verify the status of campaign "Deactivate Create Your Own Campaign" as "Inactive"
    And I delete the notification campaign with Name "Deactivate Create Your Own Campaign"

  Scenario: Verify campaign gets deactivated successfully when Choose from Gallery Notification is selected
    Given I navigate to "On-site Notifications" page via "Web Personalization"
    When create a "Choose from Gallery" Notification and select "Pop your customer care contact details to visitors dropping off" template
    And I enter the notification campaign name as "Inactive choose from gallery Campaign" and activate the campaign
    And I navigate to "On-site Notifications" page via "Web Personalization"
    And I add "Inactive choose from gallery Campaign" to the searchbox
    And I "Inactive" the "Inactive choose from gallery Campaign" campaign
    Then Verify the status of campaign "Inactive choose from gallery Campaign" as "Inactive"
    And I delete the notification campaign with Name "Inactive choose from gallery Campaign"

  Scenario: Check if user is able to update the Title of Campaign
    Given I navigate to "On-site Notifications" page via "Web Personalization"
    When create a " Create Your Own " Notification and select "Web Header" template
    And I enter the notification campaign name as "Campaign name" and activate the campaign
    And I navigate to "On-site Notifications" page via "Web Personalization"
    And I update the campaign name from "Campaign name" to "Updated Campaign name"
    And I navigate to "On-site Notifications" page via "Web Personalization"
    And I add "Updated Campaign name" to the searchbox
    And I open the "Updated Campaign name" notification campaign
    Then Verify the updated on-site campaign ID
    And I navigate to "On-site Notifications" page via "Web Personalization"
    And I delete the notification campaign with Name "Updated Campaign name"
