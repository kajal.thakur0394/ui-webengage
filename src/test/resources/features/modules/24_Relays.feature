@Relays @Regression
Feature: Relay Feature

  Scenario: To verify creation of relay entry trigger block mapping with user attribute 
    Given I navigate to "Relays" Page
    And I create a new Journey/Relay with name as "Trigger>Event | Mapping User attribute | Stop Relay"
    Then I use the Journey/Relay Template "relaysUserAtrribute"
    Then I publish the "Relay"
    Then I navigate back to "Relays" Page
    Then "Stop" the "Relay" with name as "Trigger>Event | Mapping User attribute | Stop Relay"
  
  Scenario: To verify creation of relay entry trigger block mapping with Event attribute
    Given I navigate to "Relays" Page
    And I create a new Journey/Relay with name as "Trigger>Event | Mapping Event attribute | Sunset Relay"
    Then I use the Journey/Relay Template "relaysEventAtrribute"
    Then I publish the "Relay"
    Then I navigate back to "Relays" Page
    Then "Sunset" the "Relay" with name as "Trigger>Event | Mapping Event attribute | Sunset Relay"
 
  @End-to-End @Distributed
  Scenario: To verify the relay where conversion is configured with the X days/hours deadline and users are getting converted
    Given I navigate to "Relays" Page
    And I create a new Journey/Relay with name as "Trigger>Event| Action>Send Email | Conversion"
    Then I use the Journey/Relay Template "relaysUserAtrribute"
    Then I activate conversion tracking for event "user_logged_in" with data as
      | CONVERSION EVENT  | number | Timeline |
      | System>User Login |      5 | Hours    |
    Then I publish the "Relay"
    Then I navigate back to "Relays" Page
    Then wait for "journey" api to return response as "ACTIVE" for path as "response.data.status"
    And hit "triggerBusinessEvent" API from "PerformEvents" sheet to trigger Event with reference id "businessEventString2"
    Then perform Journey validation via "Distributed" mechanism

  Scenario: To verify Sunset Relay is restarted
    Given I navigate to "Relays" Page
    Then I Publish "Sunset" Relay with name "Trigger>Event | Mapping Event attribute | Sunset Relay"
    Then I navigate back to "Relays" Page

  Scenario: To verify Stop Relay is restarted
    Given I navigate to "Relays" Page
    Then I Publish "STOPPED" Relay with name "Trigger>Event | Mapping User attribute | Stop Relay"
    Then I navigate back to "Relays" Page

  @End-to-End @Distributed
  Scenario: To verify if user is in reachable block with Email
    Given I navigate to "Relays" Page
    And I create a new Journey/Relay with name as " Relay | Is User Reachable| Action>Send Email"
    Then I use the Journey/Relay Template "relaysUserReachableEmail"
    Then I publish the "Relay"
    Then I navigate back to "Relays" Page
    Then wait for "journey" api to return response as "ACTIVE" for path as "response.data.status"
    And hit "triggerBusinessEvent" API from "PerformEvents" sheet to trigger Event with reference id "businessEventString"
    Then perform Journey validation via "Distributed" mechanism

  @End-to-End @Distributed
  Scenario: To verify if user is in reachable block with Push
    Given I navigate to "Relays" Page
    And I create a new Journey/Relay with name as " Relay | Is User Reachable| Action>Send Push"
    Then I use the Journey/Relay Template "relaysUserReachablePush"
    Then I publish the "Relay"
    Then I navigate back to "Relays" Page
    Then wait for "journey" api to return response as "ACTIVE" for path as "response.data.status"
    And hit "triggerBusinessEvent" API from "PerformEvents" sheet to trigger Event with reference id "businessEventNumber"
    Then perform Journey validation via "Distributed" mechanism

  @End-to-End @Distributed
  Scenario: To verify if user is in reachable block with Whatsapp
    Given I navigate to "Relays" Page
    And I create a new Journey/Relay with name as " Relay | Is User Reachable| Action>Send Whatsapp"
    Then I use the Journey/Relay Template "relaysUserReachableWSP"
    Then I publish the "Relay"
    Then I navigate back to "Relays" Page
    Then wait for "journey" api to return response as "ACTIVE" for path as "response.data.status"
    And hit "triggerBusinessEvent" API from "PerformEvents" sheet to trigger Event with reference id "businessEventBoolean"
    Then perform Journey validation via "Distributed" mechanism
