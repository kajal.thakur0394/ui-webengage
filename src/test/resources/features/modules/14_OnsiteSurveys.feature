@OnsiteSurveys @Regression
Feature: On-site Surveys Feature

  @WebSDKSuite
  Scenario Outline: Verify on webpage that onsite-surveys is getting delivered using <template>
    Given I navigate to "On-site Surveys" page via "Web Personalization"
    And create a " Create Your Own " Survey and select "<SurveyLayout>" template
    And use the personalization template "<template>" for "<SurveyType>" web-p
    And open the dynamic webpage and login
    Then verify survey on a web page using template
    And delete survey using template

    Examples: 
      | SurveyLayout     | template                   | SurveyType       |
      | On-site Classic  | ClassicSurvey<TextBox      | On-site Surveys  |
      | On-site Modal    | ModalSurvey<ChoiceOfOption | On-site Surveys  |
      | Off-site Classic | OffsiteClassic<Dropdown    | Off-site Surveys |

  @Smoke
  Scenario: Verify on webpage that onsite-surveys is getting delivered using Mobile<Ratings
    Given I navigate to "On-site Surveys" page via "Web Personalization"
    And create a " Create Your Own " Survey and select "Mobile Modal" template
    And use the personalization template "Mobile<Ratings" for "On-site Surveys" web-p
    Then delete survey using template
