@Journeys @Regression
Feature: Journey Feature

  @Smoke
  Scenario Outline: Create a journey by adding some blocks in the journey
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "Typical Journey"
    Then I use the Journey/Relay Template "<journeyTemplateFile>"
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page

    Examples: 
      | journeyTemplateFile |
      | journey2            |

  @Smoke
  Scenario: Try to Stop newly created or existing journey
    Given I navigate to "Journeys" Page
    And verify status as "UPCOMING" for "Typical Journey" Journey
    Then "Stop" the "Journey" with name as "Typical Journey"
    And verify status as "STOPPING" for "Typical Journey" Journey
    Then I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "Journey with End"
    Then I use the Journey/Relay Template "journey10"
    Then I publish the Journey for later by adding time in the format dd:hh:mm as
      | Start Date | -        |
      | End Date   | 00:00:05 |
    Then I navigate back to "Journeys" Page

  @Smoke
  Scenario: Try to Sunset newly created or existing journey
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "Sunset Journey"
    Then I use the Journey/Relay Template "journey1"
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page
    And verify status as "RUNNING" for "Sunset Journey" Journey
    Then "Sunset" the "Journey" with name as "Sunset Journey"
    And verify status as "SUNSET" for "Sunset Journey" Journey

  @Smoke
  Scenario: Create a journey with 'Wait for Time Slots' Flow control block along with different blocks.
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "Wait for Time Slots Journey"
    Then I use the Journey/Relay Template "journey3"
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page
    Then "Stop" the "Journey" with name as "Wait for Time Slots Journey"

  @Smoke
  Scenario: Create a journey with any block > Set Conversion Tracking and publish the journey.
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "Set Conversion Tracking Journey"
    Then I use the Journey/Relay Template "journey3"
    Then I activate conversion tracking for event "user_logged_in" with data as
      | CONVERSION EVENT  | number | Timeline |
      | System>User Login |      5 | Hours    |
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page

  Scenario: Create a journey by using Trigger Block 'For Specific Users', 'Send SMS' action block, 'Has done Event' Condition block and one 'End Journey' Flow Control block
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "Specific Users Journey"
    Then I use the Journey/Relay Template "journey4"
    Then I publish the Journey for later by adding time in the format dd:hh:mm as
      | Start Date | -        |
      | End Date   | 01:00:00 |
    Then I navigate back to "Journeys" Page
    And verify status as "UPCOMING" for "Specific Users Journey" Journey

  Scenario: Create a journey by using Trigger Block 'Occurence of Event', any action block, any flow control with Exit Trigger
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "Occurrence of Event Journey with Exit Trigger"
    Then I use the Journey/Relay Template "journey9"
    And set exit trigger when "profile attibute changes"
      | change type | attribute |
      | any         | Company   |
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page

  Scenario: Create a journey by using Trigger Block 'Enter/Exit/Is In Segment', 'Send Email' action block, add two Flow Control 'Wait for Some Time' & 'End Journey'
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "Enter/Exit Segment Journey"
    Then I use the Journey/Relay Template "journey6"
    Then I publish the Journey for later by adding time in the format dd:hh:mm as
      | Start Date | 00:05:00 |
      | End Date   | 01:00:00 |
    Then I navigate back to "Journeys" Page

  Scenario: Create a journey by using Trigger Block 'Occurence of Event', 'Send Push' action block and one 'End Journey' Flow Control block
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "Event Journey"
    Then I use the Journey/Relay Template "journey5"
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page

  Scenario: Create a journey having Call api block in it
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "API block Journey"
    Then I use the Journey/Relay Template "journey7"
    Then I publish the Journey for later by adding time in the format dd:hh:mm as
      | Start Date | 00:15:00 |
      | End Date   | 02:00:00 |
    Then I navigate back to "Journeys" Page

  Scenario: Create a journey having has done event block in it
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "Geofence and Done Event block Journey"
    Then I use the Journey/Relay Template "journey8"
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page

  @End-to-End @Distributed
  Scenario: Journey execution (Trigger Event Block)
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "Trigger>Event | Action>Send Email"
    Then I use the Journey/Relay Template "journeyExecution"
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page
    Then wait for "journey" api to return response as "ACTIVE" for path as "response.data.status"
    And hit API for Event trigger with reference id "Wishlisted"
    Then perform Journey validation via "Distributed" mechanism

  @End-to-End @Distributed
  Scenario: Journey execution (Already in segment Trigger Block)
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "Trigger>Already in Segment | Action>Send Text"
    Then I use the Journey/Relay Template "journeyExecution2"
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page
    Then wait for "journey" api to return response as "ACTIVE" for path as "response.data.status"
    Then perform Journey validation via "Distributed" mechanism

  @Smoke
  Scenario: Verify that start date on journey listing page is coming from first published date.
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "Schedule journey now"
    Then I use the Journey/Relay Template "journey10"
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page
    Then verify the Start Date on Journey Listing page for "Schedule journey now"
    Then verify the End Date on Journey Listing page for "Schedule journey now"

  @Smoke
  Scenario: Verify that when journey canvas is editable in draft/stopped state publish button should be named as Publish now with an dropdown arrow.
    Given I navigate to "Journeys" Page
    And verify status as "STOPPED" for "Typical Journey" Journey
    And verify status as "STOPPED" for "Wait for Time Slots Journey" Journey
    Then "Edit" the "Journey" with name as "Typical Journey"
    Then I publish the Journey for later by adding time in the format dd:hh:mm as
      | Start Date | 00:02:00 |
      | End Date   | -        |
    Then I navigate back to "Journeys" Page
    Then open then Journey "Wait for Time Slots Journey" and "Edit"
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page
    And I create a new Journey/Relay with name as "Draft Journey"
    Then I use the Journey/Relay Template "journey10"
    Then I navigate back to "Journeys" Page
    And verify status as "DRAFT" for "Draft Journey" Journey
    Then "Edit" the "Journey" with name as "Draft Journey"
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page

  Scenario: Verify that in drop down on publish now user should get option to publish later.
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "Verify Schedule Later options"
    Then I use the Journey/Relay Template "journey10"
    Then I validate the Publish Later option of Journeys
    Then I navigate back to "Journeys" Page

  Scenario: Verify that journey is in upcoming/running/sunset state user should get schedule card on left of the journey canvas.
    Given I navigate to "Journeys" Page
    Then open the Live View of Journey "Enter/Exit Segment Journey"
    Then I navigate back to "Journeys" Page
    Then open the Live View of Journey "Event Journey"
    Then I navigate back to "Journeys" Page
    Then open the Live View of Journey "Sunset Journey"
    Then I navigate back to "Journeys" Page

  Scenario: Verify that when journey is in upcoming/running/sunset state and have schedule dates, these dates should reflect on schedule card.
    Given I navigate to "Journeys" Page
    Then open the Live View of Journey "Enter/Exit Segment Journey"
    And verify the dates in Schedule card for journey "Enter/Exit Segment Journey"
    Then I navigate back to "Journeys" Page
    Then open the Live View of Journey "Event Journey"
    And verify the dates in Schedule card for journey "Event Journey"
    Then I navigate back to "Journeys" Page
    Then open the Live View of Journey "Sunset Journey"
    And verify the dates in Schedule card for journey "Sunset Journey"
    Then I navigate back to "Journeys" Page

  Scenario: Verify that in upcoming/running/sunset state the schedule card should be in editable state.
    Given I navigate to "Journeys" Page
    Then open the Live View of Journey "Enter/Exit Segment Journey"
    And modify the scheduled dates for journey "Enter/Exit Segment Journey" to be
      | Start Date | 00:00:15 |
      | End Date   | -        |
    Then I navigate back to "Journeys" Page
    Then open the Live View of Journey "Event Journey"
    And modify the scheduled dates for journey "Event Journey" to be
      | Start Date | -        |
      | End Date   | 01:00:00 |
    Then I navigate back to "Journeys" Page
    Then open the Live View of Journey "Specific Users Journey"
    And modify the scheduled dates for journey "Specific Users Journey" to be
      | Start Date | -        |
      | End Date   | 01:00:00 |
    Then I navigate back to "Journeys" Page
    Then open the Live View of Journey "Sunset Journey"
    And modify the scheduled dates for journey "Sunset Journey" to be
      | Start Date | 00:00:15 |
      | End Date   | -        |
    Then I navigate back to "Journeys" Page

  Scenario: Verify that when 'first published' date is not available show the date specified by user.
    Given I navigate to "Journeys" Page
    Then verify the Start Date on Journey Listing page for "Specific Users Journey"
    Then verify the End Date on Journey Listing page for "Specific Users Journey"

  Scenario: Verify that if journey is in upcoming state the start date and end date would be editable.
    Given I navigate to "Journeys" Page
    Then open the Live View of Journey "Enter/Exit Segment Journey"
    And modify the scheduled dates for journey "Enter/Exit Segment Journey" to be
      | Start Date | 00:00:15 |
      | End Date   | 00:03:00 |
    And modify the scheduled dates for journey "Enter/Exit Segment Journey" to be
      | Start Date | 00:00:15 |
      | End Date   | 00:02:00 |
    Then I navigate back to "Journeys" Page

  @issue:WP-9451
  Scenario: To verify on scheduling a journey on a future date, start date is shown in campaign listing page for the upcoming journey
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "Journey - Campaign date check"
    Then I use the Journey/Relay Template "journey11"
    Then I publish the Journey for later by adding time in the format dd:hh:mm as
      | Start Date | 00:15:00 |
      | End Date   | -        |
    Then I navigate back to "Journeys" Page
    #Given I navigate to "Email" page via "Channels"
    #And verify status as "Upcoming" and attached Start Date from Campaign "Check Campaign Status" attached to Journey "Journey - Campaign date check"
    #And verify status as "Draft" and attached Start Date from Campaign "Check Campaign Status" attached to Journey "Journey - Campaign date check"
  
  Scenario: To verify for upcoming journey, when start date = current date upcoming status should change to running
    Given I navigate to "Journeys" Page
    And verify status as "RUNNING" for "Specific Users Journey" Journey

  Scenario: To verify for upcoming journey, when End date = current date Running status should change to Stopped
    Given I navigate to "Journeys" Page
    And verify status as "STOPPED" for "Journey with End" Journey

  Scenario: Verify that all start and end date should show up in audit logs.
    Given I navigate to "Journeys" Page
    Then verify the dates in Audit Log section for the Journey "Journey with End"

  Scenario: Create a journey having InApp and WebPush blocks in it
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "InApp WebPush Journey"
    Then I use the Journey/Relay Template "journey12"
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page

  Scenario: Create a journey having In Line content block in it
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "Inline Content Journey"
    Then I use the Journey/Relay Template "journey13"
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page

  @issue:WP-12671
  Scenario: Create a journey having On-site Notification block in it
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "On-site Notification Journey"
    Then I use the Journey/Relay Template "journey14"
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page

  Scenario: To verify upload CSV file in Specific Users block
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "Verify upload Users CSV file"
    Then I use the Journey/Relay Template "journey15"
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page

  @End-to-End @Distributed
  Scenario: Verification of Check if User is in Segment Block with Email Delivery
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "Trigger>Manual |Condition>Is in Segment | Action>Send Email"
    Then I use the Journey/Relay Template "journey16"
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page
    Then wait for "journey" api to return response as "ACTIVE" for path as "response.data.status"
    Then perform Journey validation via "Distributed" mechanism

  Scenario: Verification of Best Channel Block
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "Trigger>Attribute Change |Condition>Best Channel | Action>Multiple"
    Then I use the Journey/Relay Template "journey17"
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page

  Scenario: Verification of Wait For Event Block
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "Trigger>Segment |Flow Control>Wait For Event | Action>InApp/SMS"
    Then I use the Journey/Relay Template "journey18"
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page

  Scenario: Verification of Split Block
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "Trigger>Attribute Change |Flow Control>Split | Action>Update Attribute"
    Then I use the Journey/Relay Template "SplitJourney"
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page

  @Smoke
  Scenario: InApp block in Journey with CustomHtml
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "Journey with CustomHtml"
    Then I click on journey settings by selecting "Existing segment" with name as "User Condition > User IDs> ends with J"
    Then I click button "SAVE"
    Then I use the Journey/Relay Template "journey19"
    And set exit trigger when "profile attibute changes"
      | change type | attribute |
      | any         | Company   |
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page

  Scenario: To verify personlisation emojis across Journey blocks for SMS, Email, and Push campaigns
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "Journey Emoticon Personalisation_1"
    Then I use the Journey/Relay Template "journey20"
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page

  @Smoke
  Scenario: To verify personlisation emojis across Journey blocks for WebPush, InApp, App-Inline and InLineWebP campaigns
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "Journey Emoticon Personalisation_2"
    Then I click on journey settings by selecting "Existing segment" with name as "User Condition > User IDs> ends with J"
    Then I click button "SAVE"
    Then I use the Journey/Relay Template "journey21"
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page

  @issue:WP-17838
  Scenario: To verify characterset contents are showing properly across Journey blocks for SMS, Email, and Push campaigns
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "Journey Unicode_1: ! # $ % & ' ( ) * +"
    Then I use the Journey/Relay Template "UnicodeJourney1"
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page
    Then I verify following details in "Journey Unicode_1: ! # $ % & ' ( ) * +" Journeys
      | Channels_Email | Unicode:Email: अ आ इ ई ਆ ਇ ਈ ஆ இ ஈ અ આ       |
      | Channels_SMS   | Unicode:SMS: [ ] ^ _ ` { } ~ ¢ £ ¤ ڝ ڞ ڟ ڠ ڡ |
      | Channels_Push  | Unicode:Push: ઇ అ ఆ ఇ ఈ ആ ഇ ഈ ฃ ค ฅ ฆ        |

  @Smoke
  Scenario: To verify characterset contents are showing properly across Journey blocks for WebPush, InApp, App-Inline and InLineWebP campaigns
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "Journey Unicode_2: ༂ ༃ ༄ ༅ ခ ဂ ဃ င Ⴁ Ⴂ Ⴃ"
    Then I click on journey settings by selecting "Existing segment" with name as "User Condition > User IDs> ends with J"
    Then I click button "SAVE"
    Then I use the Journey/Relay Template "UnicodeJourney2"
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page
    Then I verify following details in "Journey Unicode_2: ༂ ༃ ༄ ༅ ခ ဂ ဃ င Ⴁ Ⴂ Ⴃ" Journeys
      | Web Personalization_In-line Content      | Unicode:WebP: ℊ ℋ ℌ ℍ ℎ ℏ $ ! # % & ' ( )   |
      | App Personalization_In-app Notifications | Unicode:InApp: ℗ ℘ ℙ ℚ ℛ ℜ ℝ ℞ ℟ ℠ ℡ ™      |
      | Channels_Web Push                        | Unicode:WebPush: ἇ Ἀ ἕ Ἐ ℀ ℁ ℂ ℃ ℄ ℅ ℆ ℇ ℈  |
      | App Personalization_In-line Content      | Unicode:App-inline: अ आ इ ई ਆ ਇ ਈ ஆ இ ஈ અ આ |

  Scenario Outline: To verify journeys count in staticlisting page for Enter/Exit/Is In Segment  for static list usage
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "<JourneyName>"
    Then I use the Journey/Relay Template "<Template>"
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page
    Given I navigate to "Lists" page via "Segments"
    And I search for "<Static>" segment
    Then I save the SegmentId of "<Static>" segment
    Then wait for "staticListJourneyCount" api to return response as "1" for path as "response.data[0].count"
    Then I delete the Journey via API Delete method

    Examples: 
      | JourneyName                     | Template  | Static                        |
      | Isalreadyin ListCount           | journey24 | StaticSegments Tag            |
      | Enters ListCount                | journey25 | WebSDKUserList                |
      | Exits ListCount                 | journey26 | StaticListStatsValidationUser |
      | Enters Or isalreadyin ListCount | journey27 | UsersList                     |

  Scenario Outline: To verify journeys count in staticlisting page for Add exitTrigger when user <action> list
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "Exit Trigger > When User <action> list"
    Then I use the Journey/Relay Template "journey24"
    And set exit trigger when "user enters or exits a segment"
      | user action | segment name |
      | <action>    | <segment>    |
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page
    Given I navigate to "Lists" page via "Segments"
    And I search for "<segment>" segment
    Then I save the SegmentId of "<segment>" segment
    Then wait for "staticListJourneyCount" api to return response as "1" for path as "response.data[0].count"
    Then I delete the Journey via API Delete method

    Examples: 
      | action | segment     |
      | Enters | IncludeList |
      | Exits  | ExcludeList |

  @End-to-End @Distributed
  Scenario: User Path Validation through CustomEvent Email EndJourney
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "CustomEvent Email User Validation"
    Then I use the Journey/Relay Template "journey28"
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page
    Then wait for "journey" api to return response as "ACTIVE" for path as "response.data.status"
    And hit "performEvent" API from "PerformEvents" sheet to trigger Event with reference id "OrderPlaced"
    Then perform Journey validation via "Distributed" mechanism

  @End-to-End @Distributed
  Scenario: User Path Validation through SystemEvent Email EndJourney
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "SystemEvent Email User Validation"
    Then I use the Journey/Relay Template "journey29"
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page
    Then wait for "journey" api to return response as "ACTIVE" for path as "response.data.status"
    Then open the dynamic webpage and login
    Then I switch back to default window
    Then perform Journey validation via "Distributed" mechanism

  @End-to-End @Distributed
  Scenario: User Path Validation Through IsInSegment SMS Wait
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "IsInSegment SMS Wait"
    Then I use the Journey/Relay Template "journey30"
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page
    Then wait for "journey" api to return response as "ACTIVE" for path as "response.data.status"
    Then perform Journey validation via "Distributed" mechanism

  @End-to-End @Distributed
  Scenario: User Path Validation Through IsInSegment InApp HasDoneEvent Wait
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "IsInSegment InApp HasDoneEvent"
    Then I use the Journey/Relay Template "journey31"
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page
    Then wait for "journey" api to return response as "ACTIVE" for path as "response.data.status"
    And hit "performEvent" API from "PerformEvents" sheet to trigger Event with reference id "Checkout"
    Then perform Journey validation via "Distributed" mechanism

  @End-to-End @Distributed
  Scenario: User Path Validation Through CsvUpload UserReachable Whatsapp
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "CsvUpload UserReachable Whatsapp"
    Then I use the Journey/Relay Template "journey32"
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page
    Then wait for "journey" api to return response as "ACTIVE" for path as "response.data.status"
    Then perform Journey validation via "Distributed" mechanism

  Scenario: To verify Journeys using Show appinline import and reset custom templates
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "Import Custom Template"
    Then I use the Journey/Relay Template "journeyImportTemplate"
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page
    Then I delete the Journey via API Delete method

  Scenario: To verify Journeys using appinline Save Templates option
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "Save Custom Template"
    Then I use the Journey/Relay Template "JourneySaveTemplate"
    Then I save TemplateID with name "TemplateSavedFromJourney"
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page
    Then I delete the Journey via API Delete method
    Given I navigate to "Integrations" page via "Data Platform"
    And I click on Configure "App In-line Setup"
    And I delete the Custom Template with name as "TemplateSavedFromJourney"
