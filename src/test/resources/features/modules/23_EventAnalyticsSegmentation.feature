@EventAnalyticsSegmentation @Regression
Feature: Event Analytics Live And Predictive Segments feature

  Scenario Outline: To verify the <showType> count post applying the live segment in the segment filter [<Inclusion/Exclusion>] without split by attribute
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>  |
      | OF       | multi_event |
      | OVER     | <DayType>   |
      | SPLIT BY | -           |
    Then I apply filter for events
      | Segment | <Include/Exclude> Segments | multiEventAUsers |  |  |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter   | Value           |
      | multi_event | <Count_event_1> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter   | Value           |
      | multi_event | <Count_event_1> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "equal to" expected value in table displayed
      | Parameter  | Value           |
      | <DayType>  |                 |
      | <showType> | <Count_event_1> |

    Examples: 
      | showType    | Count_event_1 | DayType | Total | Include/Exclude | Inclusion/Exclusion |
      | Occurrences |            60 | Days    |    60 | Include         | Inclusion           |
      | Uniques     |            50 | Days    |    50 | Include         | Inclusion           |
      | Occurrences |           150 | Days    |   150 | Exclude         | Exclusion           |
      | Uniques     |           150 | Days    |   150 | Exclude         | Exclusion           |

  Scenario Outline: To verify the <showType> count when live segment with the list[<listType>] segment in the segment filter [<Inclusion/Exclusion>]  without split by attribute
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>  |
      | OF       | multi_event |
      | OVER     | <DayType>   |
      | SPLIT BY | -           |
    Then I apply filter for events
      | Segment | <Include/Exclude> Segments | multiEventCUsers |  |  |
      | Segment | <Include/Exclude> Segments | <listName>       |  |  |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter   | Value           |
      | multi_event | <Count_event_1> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter   | Value           |
      | multi_event | <Count_event_1> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "equal to" expected value in table displayed
      | Parameter  | Value           |
      | <DayType>  |                 |
      | <showType> | <Count_event_1> |

    Examples: 
      | showType    | Count_event_1 | DayType | Total | Include/Exclude | Inclusion/Exclusion | listType      | listName                |
      | Occurrences |            40 | Days    |    40 | Include         | Inclusion           | criteria      | multiEventDcriteriaList |
      | Uniques     |            30 | Days    |    30 | Include         | Inclusion           | criteria      | multiEventDcriteriaList |
      | Occurrences |           170 | Days    |   170 | Exclude         | Exclusion           | criteria      | multiEventDcriteriaList |
      | Uniques     |           170 | Days    |   170 | Exclude         | Exclusion           | criteria      | multiEventDcriteriaList |
      | Occurrences |            40 | Days    |    40 | Include         | Inclusion           | CSV           | multiEventDCsvList      |
      | Uniques     |            30 | Days    |    30 | Include         | Inclusion           | CSV           | multiEventDCsvList      |
      | Occurrences |           170 | Days    |   170 | Exclude         | Exclusion           | CSV           | multiEventDCsvList      |
      | Uniques     |           170 | Days    |   170 | Exclude         | Exclusion           | CSV           | multiEventDCsvList      |
      | Occurrences |            40 | Days    |    40 | Include         | Inclusion           | exported list | multiEventDExportedList |
      | Uniques     |            30 | Days    |    30 | Include         | Inclusion           | exported list | multiEventDExportedList |
      | Occurrences |           170 | Days    |   170 | Exclude         | Exclusion           | exported list | multiEventDExportedList |
      | Uniques     |           170 | Days    |   170 | Exclude         | Exclusion           | exported list | multiEventDExportedList |

  Scenario Outline: To verify the count when live segment with the <event/User attribute> filter in the segment filter [<Inclusion/Exclusion>]
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>  |
      | OF       | multi_event |
      | OVER     | <DayType>   |
      | SPLIT BY | -           |
    Then I apply filter for events
      | Segment | <Include/Exclude> Segments | multiEventCUsers |          |                  |
      | Event   | <event>                    | <eventAttrbute>  | equal to | <eventAttrValue> |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter   | Value           |
      | multi_event | <Count_event_1> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter   | Value           |
      | multi_event | <Count_event_1> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "equal to" expected value in table displayed
      | Parameter  | Value           |
      | <DayType>  |                 |
      | <showType> | <Count_event_1> |

    Examples: 
      | showType    | Count_event_1 | DayType | Total | Include/Exclude | Inclusion/Exclusion | event                 | eventAttrbute       | eventAttrValue                 | event/User attribute |
      | Occurrences |            40 | Days    |    40 | Include         | Inclusion           | multi_event           | new_attr_2          |                            111 | event                |
      | Uniques     |            30 | Days    |    30 | Include         | Inclusion           | multi_event           | new_attr_2          |                            111 | event                |
      | Occurrences |            70 | Days    |    70 | Exclude         | Exclusion           | multi_event           | new_attr_2          |                            111 | event                |
      | Uniques     |            70 | Days    |    70 | Exclude         | Exclusion           | multi_event           | new_attr_2          |                            111 | event                |
      | Occurrences |            40 | Days    |    40 | Include         | Inclusion           | User Attribute Filter | user_attr_testing_1 | user_attr_testing_1_test_value | User attribute       |
      | Uniques     |            30 | Days    |    30 | Include         | Inclusion           | User Attribute Filter | user_attr_testing_1 | user_attr_testing_1_test_value | User attribute       |
      | Occurrences |           170 | Days    |   170 | Exclude         | Exclusion           | User Attribute Filter | Age                 |                             32 | User attribute       |
      | Uniques     |           170 | Days    |   170 | Exclude         | Exclusion           | User Attribute Filter | Age                 |                             32 | User attribute       |

  Scenario Outline: To verify the <showType> count post applying the live segment in the segment filter [Inclusion] with split by <custom/user custom> attribute
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>  |
      | OF       | multi_event |
      | OVER     | <DayType>   |
      | SPLIT BY | <splitBy>   |
    Then I apply filter for events
      | Segment | Include Segments | <liveSegment> |  |  |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter   | Value        |
      | <attribute> | <Count_attr> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter   | Value        |
      | <attribute> | <Count_attr> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "equal to" expected value in table displayed
      | Parameter   | Value        |
      | <DayType>   |              |
      | <attribute> | <Count_attr> |

    Examples: 
      | showType    | Count_attr | DayType | Total | attribute | liveSegment      | splitBy    | custom/user custom |
      | Occurrences |         60 | Days    |    60 |     111.0 | multiEventAUsers | new_attr_2 | custom             |
      | Uniques     |         50 | Days    |    50 |     111.0 | multiEventAUsers | new_attr_2 | custom             |
      | Occurrences |         40 | Days    |    40 |        32 | multiEventCUsers | Age        | user custom        |
      | Uniques     |         30 | Days    |    30 |        32 | multiEventCUsers | Age        | user custom        |

  Scenario Outline: To verify the <showType> count post applying the live segment in the segment filter [Inclusion] with over by <custom/user custom> attribute
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>  |
      | OF       | multi_event |
      | OVER     | <overBy>    |
      | SPLIT BY | -           |
    Then I apply filter for events
      | Segment | Include Segments | <liveSegment> |  |  |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter  | Value        |
      | <showType> | <Count_attr> |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter  | Value        |
      | <showType> | <Count_attr> |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "equal to" expected value in table displayed
      | Parameter     | Value        |
      | <tableHeader> | <DayType>    |
      | <showType>    | <Count_attr> |

    Examples: 
      | showType    | Count_attr | DayType | Total | liveSegment      | overBy     | custom/user custom | tableHeader |
      | Occurrences |         60 |   111.0 |    60 | multiEventAUsers | new_attr_2 | custom             | New attr 2  |
      | Uniques     |         50 |   111.0 |    50 | multiEventAUsers | new_attr_2 | custom             | New attr 2  |
      | Occurrences |         40 |      32 |    40 | multiEventCUsers | Age        | user custom        | Age         |
      | Uniques     |         30 |      32 |    30 | multiEventCUsers | Age        | user custom        | Age         |

  Scenario Outline: To verify the <showType> count post applying the live segment in the segment filter [Exclusion] split by user custom attribute
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>  |
      | OF       | multi_event |
      | OVER     | <DayType>   |
      | SPLIT BY | Age         |
    Then I apply filter for events
      | Segment | Exclude Segments | multiEventCUsers |  |  |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter | Value |
      |        32 |   170 |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter | Value |
      |        32 |   170 |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "equal to" expected value in table displayed
      | Parameter | Value |
      | <DayType> |       |
      |        32 |   170 |

    Examples: 
      | showType    | DayType | Total |
      | Occurrences | Days    |   170 |
      | Uniques     | Days    |   170 |

  Scenario Outline: To verify the <showType> count post applying the live segment in the segment filter [Exclusion] split by custom attribute
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>  |
      | OF       | multi_event |
      | OVER     | <DayType>   |
      | SPLIT BY | new_attr_2  |
    Then I apply filter for events
      | Segment | Exclude Segments | multiEventCUsers |  |  |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter | Value |
      |     111.0 |    70 |
      |     222.0 |   100 |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter | Value |
      |     111.0 |    70 |
      |     222.0 |   100 |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "equal to" expected value in table displayed
      | Parameter | Value   |
      | <DayType> |         |
      | Total     | <Total> |
      |     111.0 |      70 |
      |     222.0 |     100 |

    Examples: 
      | showType    | DayType | Total |
      | Occurrences | Days    |   170 |
      | Uniques     | Days    |   170 |

  Scenario Outline: To verify the <showType> count post applying the live segment in the segment filter [Exclusion] over by <custom/user custom> attribute
    Given I navigate to "Events" page via "Analytics"
    Then I filter for "Today" from dayType dropdown
    Then set event section filter as
      | SHOW     | <showType>    |
      | OF       | multi_event_A |
      | OVER     | <overBy>      |
      | SPLIT BY | -             |
    Then I apply filter for events
      | Segment | Exclude Segments | multiEventCUsers |  |  |
    Then open "Bar" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Bar" graph
      | Parameter  | Value |
      | <showType> |    20 |
    Then open "Line" view for the section with header name as "SHOW"
    Then I validate the actual value to be "equal to" expected value having "<Total>" for "<showType>" and "<DayType>" via "Line" graph
      | Parameter  | Value |
      | <showType> |    20 |
    Then open "Table" view for the section with header name as "SHOW"
    Then I validate the actual value for "<showType>" to be "equal to" expected value in table displayed
      | Parameter  | Value     |
      | <param>    | <DayType> |
      | <showType> |        20 |

    Examples: 
      | showType    | DayType | Total | overBy | param | custom/user custom |
      | Occurrences | Testing |    20 | E_1    | E 1   | custom             |
      | Uniques     | Testing |    20 | E_1    | E 1   | custom             |
      | Occurrences |      32 |    20 | Age    | Age   | user custom        |
      | Uniques     |      32 |    20 | Age    | Age   | user custom        |

  Scenario Outline: To verify if segment A is select in the inclusion then same segment should not be present in the exclusion and vice versa
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | <showType>  |
      | OF       | multi_event |
      | OVER     | Days        |
      | SPLIT BY | new_attr_2  |
    Then I verify segment present in "Include" is not present in "Exclude"
      | Segment | Include Segments | multiEventCUsers |
    Then I verify segment present in "Exclude" is not present in "Include"
      | Segment | Exclude Segments | multiEventCUsers |

    Examples: 
      | showType    |
      | Occurrences |
      | Uniques     |