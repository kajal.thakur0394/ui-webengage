@Integration @Regression
Feature: Integrations Feature

  @Smoke
  Scenario: Verify that user is able to view rest api configs from integration page.
    Given I navigate to "Integrations" page via "Data Platform"
    And I click on View REST API page
    Then I verify whether the license code and key is displayed

  Scenario: Verify that for the channels on integration proper integration status should be shown(success/pending)
    Given I navigate to "Integrations" page via "Data Platform"
    Then I verify the status of each of this Channels
      | In-app Setup | Push Setup | On-site Setup | Web Push Setup | SMS Setup | Email Setup | WhatsApp Setup | Web Personalization Setup | Google Adwords | Facebook |

  @Smoke
  Scenario: Verify that user is able to turn on/off web personalisation from integration page.
    Given I navigate to "Integrations" page via "Data Platform"
    And I click on Configure "Web Personalization Setup"
    Then I switch "OFF" Web Personalization
    Given I navigate to "Integrations" page via "Data Platform"
    And Channel Integration status for "Web Personalization Setup" set as "PENDING"
    And I click on Configure "Web Personalization Setup"
    Then I switch "ON" Web Personalization
    Given I navigate to "Integrations" page via "Data Platform"
    And Channel Integration status for "Web Personalization Setup" set as "SUCCESS"

  @Smoke
  Scenario: Verify that user is able to add/edit or delete email service provider.
    Given I navigate to "Integrations" page via "Data Platform"
    And I click on Configure "Email Setup"
    And I select "Octane" ESP to create with values with name set as "ESP Config 1"
      | Username | username |
      | API Key  | apiKey   |
    Then I verify SP config has been created
    Given I navigate to "Integrations" page via "Data Platform"
    And Channel Integration status for "Email Setup" set as "SUCCESS"
    Then I click on Configure "Email Setup"
    And I edit the "Email" SP with name "ESP Config 1" with following configs
      | name | ESP Config 1 edited |
    And I delete the "ESP Config 1 edited" ESP setup

  Scenario: Verify that ESP added in integration page are reflection on email campaign creation page
    Given I navigate to "Integrations" page via "Data Platform"
    And I click on Configure "Email Setup"
    And I select "Octane" ESP to create with values with name set as "ESP Config Campaign"
      | Username | username |
      | API Key  | apiKey   |
    Then I verify SP config has been created
    Given I navigate to "Email" page via "Channels"
    And I select "One-time" Campaign Type and enters "CHECK ESP" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then put in the message details as
      | ESP | ESP Config Campaign |
    Given I navigate to "Email" page via "Channels"
    Then I delete campaign with name "CHECK ESP"
    Given I navigate to "Integrations" page via "Data Platform"
    And Channel Integration status for "Email Setup" set as "SUCCESS"
    Then I click on Configure "Email Setup"
    And I delete the "ESP Config Campaign" ESP setup

  @Smoke
  Scenario: Verify that user is able add/edit or delete SSP from integration page.
    Given I navigate to "Integrations" page via "Data Platform"
    And I click on Configure "SMS Setup"
    And I select "2Factor" SSP to create with values with name set as "SSP Config 1"
      | API Key | apiKey |
    Then I verify SP config has been created
    Given I navigate to "Integrations" page via "Data Platform"
    And Channel Integration status for "SMS Setup" set as "SUCCESS"
    Then I click on Configure "SMS Setup"
    And I edit the "SMS" SP with name "SSP Config 1" with following configs
      | name | SSP Config 1 edited |
    And I delete the "SSP Config 1 edited" SSP setup

  Scenario: Verify that ssp added in integration page are reflecting on sms campaign creation page
    Given I navigate to "Integrations" page via "Data Platform"
    And I click on Configure "SMS Setup"
    And I select "2Factor" SSP to create with values with name set as "SSP Config 1"
      | API Key | apiKey |
    Then I verify SP config has been created
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "CHECK SSP" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then put in the message details as
      | SSP | SSP Config 1 |
    Given I navigate to "SMS" page via "Channels"
    Then I delete campaign with name "CHECK SSP"
    Given I navigate to "Integrations" page via "Data Platform"
    Then I click on Configure "SMS Setup"
    And I delete the "SSP Config 1" SSP setup

  @Smoke
  Scenario: Verify that user is able to add/edit or delete the Whatsapp service providers.
    Given I navigate to "Integrations" page via "Data Platform"
    And I click on Configure "WhatsApp Setup"
    And I select "Gupshup" WSP to create with values with name set as "WSP Config 1"
      | Username                 | username   |
      | Whatsapp Business Number | 9876543210 |
      | Password                 | pass@123   |
    Then I verify SP config has been created
    Then I make WA Template for "WSP Config 1 (Gupshup)" with data as
      | Template Name | WA Template 1       |
      | Template Text | Just check this out |
    Given I navigate to "Integrations" page via "Data Platform"
    And Channel Integration status for "WhatsApp Setup" set as "SUCCESS"
    Then I click on Configure "WhatsApp Setup"
    And I edit the "WhatsApp" SP with name "WSP Config 1" with following configs
      | Configuration Name | WSP Config 1 edited |
    And I delete the "WSP Config 1 edited" WSP setup

  Scenario: Verify that WSP added from integration page are reflection Whatsapp campaign audience page
    Given I navigate to "Integrations" page via "Data Platform"
    And I click on Configure "WhatsApp Setup"
    And I select "Gupshup" WSP to create with values with name set as "WSP Config for Campaign"
      | Username                 | username   |
      | Whatsapp Business Number | 9876543210 |
      | Password                 | pass@123   |
    Then I verify SP config has been created
    Then I make WA Template for "WSP Config for Campaign (Gupshup)" with data as
      | Template Name | WA Template Campaign |
      | Template Text | Just check this out  |
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "One-time" Campaign Type and enters "CHECK WSP" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then I select WA Template having name "WA Template Campaign" with WSP name "WSP Config for Campaign" and fill the Message Content
      |  |  |
    Given I navigate to "WhatsApp" page via "Channels"
    Then I delete campaign with name "CHECK WSP"
    Given I navigate to "Integrations" page via "Data Platform"
    And Channel Integration status for "WhatsApp Setup" set as "SUCCESS"
    Then I click on Configure "WhatsApp Setup"
    And I delete the "WSP Config for Campaign" WSP setup

  Scenario: Verify that for new projects user is able to configure sdk integration from Data platform > Integration > Sdk integration page
    Given I navigate to "Integrations" page via "Data Platform"
    And I check SDK Integration block
    And I click on Configure for SDK integration block
    Then I check Integration Status for "iOS"
    Then I check Integration Status for "Website"
    Then I check Integration Status for "Android"

  @Smoke
  Scenario: Verify that user is able to toggle and turn on/off the web push from integration page.
    Given I navigate to "Integrations" page via "Data Platform"
    And I click on Configure "Web Push Setup"
    Then I switch "OFF" Web Push
    Given I navigate to "Integrations" page via "Data Platform"
    And I click on Configure "Web Push Setup"
    Then I switch "ON" Web Push

  @Smoke
  Scenario: Verify that user is able to add/edit or delete Android push creds.
    Given I navigate to "Integrations" page via "Data Platform"
    And I click on Configure "Push Setup"
    And I fill in following details for Android Credentials
      | PACKAGE NAME | com.webengage.production.edit                                                                                                                            |
      | App Secret   | AAAAEemEUmU:APA91bGpkg7bY7L9417MfJJCPAX4BOmu2oP8QkwCHRbdLNgCjJ9Kgalslpz5dUfy0I8_SejVNUoY_2ukyiIUnM6dVmUv29kmTeDu6gYbyEsmqPJnuT3JPHEc0OJWdZUSa4LgOKa0gfEZ |
    Then save and verify Android Credentials
    Then I edit the "Android" Push Cred config having name "com.webengage.production.edit" with following configs
      | App Secret | Random |
    Then I delete Android Cred having package name as "com.webengage.production.edit"

  @Smoke
  Scenario: Verify that user is able to add/edit or delete iOS push creds.
    Then I navigate to "Integrations" page via "Data Platform"
    And I click on Configure "Push Setup"
    And I fill in following details for iOS crdentials and upload "AuthKey_RDZ7UD226N.p8" AuthKey
      | Team ID           | S7X7V9YPX7                   |
      | Bundle Identifier | com.webEngage.prodSwift.test |
    Then save and verify iOS Credentials
    Then I edit the "iOS" Push Cred config having name "com.webEngage.prodSwift.test" with following configs
      | Team ID | KU59F2LKHW |
    Then I delete iOS Cred having package name as "com.webEngage.prodSwift.test"

  @skip @issue:WP-8925
  Scenario: Verify that push creds for an app present in integration are reflecting on push creation page while selecting the audience .
    Given I navigate to "Integrations" page via "Data Platform"
    And I click on Configure "Push Setup"
    And I fill in following details for iOS crdentials and upload "AuthKey_RDZ7UD226N.p8" AuthKey
      | Team ID           | S7X7V9YPX7                   |
      | Bundle Identifier | com.webEngage.prodSwift.test |
    Then save and verify iOS Credentials
    Given I navigate to "Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "CHECK PUSH" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I select Target Device as "iOS only" and also verify "Both Android & iOS,Android only" as disabled
    Given I navigate to "Integrations" page via "Data Platform"
    And I click on Configure "Push Setup"
    Then I delete iOS Cred having package name as "com.webEngage.prodSwift.test"
    And I fill in following details for Android Credentials
      | PACKAGE NAME | com.webengage.production.testv                                                                                                                           |
      | App Secret   | AAAAEemEUmU:APA91bGpkg7bY7L9417MfJJCPAX4BOmu2oP8QkwCHRbdLNgCjJ9Kgalslpz5dUfy0I8_SejVNUoY_2ukyiIUnM6dVmUv29kmTeDu6gYbyEsmqPJnuT3JPHEc0OJWdZUSa4LgOKa0gfEZ |
    Then save and verify Android Credentials
    Given I navigate to "Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "CHECK PUSH" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I select Target Device as "Android only" and also verify "Both Android & iOS,iOS only" as disabled
    Given I navigate to "Integrations" page via "Data Platform"
    And I click on Configure "Push Setup"
    And I fill in following details for iOS crdentials and upload "AuthKey_RDZ7UD226N.p8" AuthKey
      | Team ID           | S7X7V9YPX7                   |
      | Bundle Identifier | com.webEngage.prodSwift.test |
    Then save and verify iOS Credentials
    Given I navigate to "Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "CHECK PUSH" Campaign Name
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I select Target Device as "Both Android & iOS" and also verify "" as disabled
    Given I navigate to "Integrations" page via "Data Platform"
    And I click on Configure "Push Setup"
    Then I delete iOS Cred having package name as "com.webEngage.prodSwift.test"
    Then I delete Android Cred having package name as "com.webengage.production.testv"

  @Smoke
  Scenario: Verify that able to add new fonts for inapps from integration page.
    Given I navigate to "Integrations" page via "Data Platform"
    And I click on Configure "In-app Setup"
    Then I upload custom Font "font.ttf"
    Then I verify uploaded font has been listed
    Then I delete the font "font.ttf"
    And verify deleted font is not listed

  Scenario: Verify that if sdk is configured the user is able to send web push and web personalization
    Given I navigate to "Integrations" page via "Data Platform"
    And I click on Configure "Web Push Setup"
    Then I switch "ON" Web Push
    Given I navigate to "Web Push" page via "Channels"
    And I select "One-time" Campaign Type and enters "CHECK WEBP" Campaign Name
    Given I navigate to "Integrations" page via "Data Platform"
    And I click on Configure "Web Personalization Setup"
    Then I switch "OFF" Web Personalization
    Given I navigate to "In-line Content" page via "Web Personalization"
    And check whether campaign creation is disabled for WebP and enable it again
    Then I switch "ON" Web Personalization
    Given I navigate to "In-line Content" page via "Web Personalization"
    And I select "Banner" Campaign Type and enters "CHECK ESP" Campaign Name

  @Smoke @issue:WP-10701
  Scenario: Verify that if required user is able to configure webhooks as well.
    Given I navigate to "Integrations" page via "Data Platform"
    And I click on Configure for Webhooks block
    Then I configure "Global System Events WebHook" with data as and hit Save
      | URL    | https://www.google.com/ |
      | Format | XML                     |
    #Then I configure "Global System Events WebHook" with data as and hit Delete
    #Then I configure "Global System Events WebHook" with data as and hit Save
    #  | URL    | https://www.google.com/ |
    #  | Format | JSON                    |
    Then I configure "Global Application Events WebHook" with data as and hit Save
      | URL    | https://in.search.yahoo.com/?fr2=inr |
      | Format | JSON                                 |
    Then I configure "Global System Events WebHook" with data as and hit Delete
    Then I configure "Global Application Events WebHook" with data as and hit Delete

  @Smoke
  Scenario Outline: To verify on Data platform, Link to push integration page is shown
    Given I navigate to "Integrations" page via "Data Platform"
    And I go to SDK integration page
    Then verify the links for "<osType>" OS

    Examples: 
      | osType  |
      | Android |
      | iOS     |

  @Smoke
  Scenario Outline: To verify search functionality is working for listed creds with help of Package name
    Given I navigate to "Integrations" page via "Data Platform"
    And I go to SDK integration page
    Then verify the package for "<osType>" OS and "<packageName>"

    Examples: 
      | osType  | packageName              |
      | Android | com.webengage.production |
      | iOS     | com.webEngage.prodSwift  |

  @Smoke
  Scenario Outline: To verify Proper message is shown on searching Invalid keywords
    Given I navigate to "Integrations" page via "Data Platform"
    And I go to SDK integration page
    Then verify the "You have not added any credentials yet" Text for Invalid package for following "<osType>"  OS and "<packageName>"

    Examples: 
      | osType  | packageName |
      | Android | abc         |
      | iOS     | def         |

  @Smoke
  Scenario: To verify user can add MI and Huawei Push creds to existing FCM creds
    Given I navigate to "Integrations" page via "Data Platform"
    And I click on Configure "Push Setup"
    Then I edit the "Android" Push Cred config having name "com.webengage.production"
    And I add the following Push Service details
      | MI Push Service     | App Secret    | WLbeRrvZ6N/eyEb/DmPy2w==                                         |
      | Huawei Push Service | Client ID     |                                                        104734947 |
      | Huawei Push Service | Client Secret | 6129d5270b2e114bcc8592bf96294073dea0011b2b4c8b0c8fa71f9a8e03bfa1 |
    Then I edit the "Android" Push Cred config having name "com.webengage.production"
    And I verify following Push Services are present
      | MI Push Service     |
      | Huawei Push Service |

  @Smoke @issue:WP-15077
  Scenario: To verify user can remove MI and Huawei push creds
    Given I navigate to "Integrations" page via "Data Platform"
    And I click on Configure "Push Setup"
    Then I edit the "Android" Push Cred config having name "com.webengage.production"
    And I click on remove icon for following Push Services
      | MI Push Service     |
      | Huawei Push Service |
    Then I edit the "Android" Push Cred config having name "com.webengage.production"
    And I verify following Push Services are deleted
      | + MI Push Service     |
      | + Huawei Push Service |

  Scenario: To verify that CSV upload User Data section in Data Management allows to upload new users
    Given I navigate to "Upload Data" page via "Data Platform"
    Then I click on "Upload User Data" tab
    Then I upload the file for "User Data"
    Then I verify upload status as "IN PROGRESS"
    And I verify the file upload status via API
    Then I verify upload status as "COMPLETED"

  Scenario: To verify that CSV upload Event Data section in Data Management allows to upload user events
    Given I navigate to "Upload Data" page via "Data Platform"
    Then I click on "Upload Events Data" tab
    Then I upload the file for "Events Data"
    Then I verify upload status as "IN PROGRESS"
    And I verify the file upload status via API
    Then I verify upload status as "COMPLETED"

  Scenario Outline: To verify emoji is present on the fields while adding a whatsapp template
    Given I navigate to "Integrations" page via "Data Platform"
    And I click on Configure "WhatsApp Setup"
    Then I select "<templateType>" template  with SP name as "Journey WSP"
    Then fill the page via "<templateName>" template from "WhatsappTemplates" folder using "Message" section
    And I search and delete whatsapp template "Emoji Template"

    Examples: 
      | templateType | templateName              |
      | TEXT         | WhatsappEmojiTextTemplate |
      | IMAGE        | WhatsappEmojiTemplate     |
      | VIDEO        | WhatsappEmojiTemplate     |
      | DOCUMENT     | WhatsappEmojiTemplate     |
      | LOCATION     | WhatsappEmojiTemplate     |

  Scenario Outline: To verify unicode characterset contents are showing properly WhatsApp 'Text' Template
    Given I navigate to "Integrations" page via "Data Platform"
    And I click on Configure "WhatsApp Setup"
    Then I select "<templateType>" template  with SP name as "Journey WSP"
    Then fill the page via "<templateName>" template from "MultiLingualTemplates" folder using "Message" section
    And I search and delete whatsapp template "Unicode Template"

    Examples: 
      | templateType | templateName                     |
      | TEXT         | Unicode-WhatsAppTextTemplate     |
      | DOCUMENT     | Unicode-WhatsAppDocumentTemplate |

  Scenario: To verify unicode characterset contents are showing properly for WebPush Configuration
    Given I navigate to "Integrations" page via "Data Platform"
    And I click on Configure "Web Push Setup"
    Then fill the page via "Unicode-WebPushConfig" template from "MultiLingualTemplates" folder using "Message" section
    And I verify toast notification after saving Web Push Configuration
    And I reset the WebPush Config Text Message back to original
