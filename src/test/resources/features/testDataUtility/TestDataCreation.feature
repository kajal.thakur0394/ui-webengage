@TestDataCreation
Feature: Create and feed pre-requisite test data in user defined or newly created accounts via API
  I want to use this template for my feature file

  @InjectSegments
  Scenario: Segment Creation
    Given I select the function "Segments" for building the test data
    Given I select the function "StaticSegments" for building the test data
    Given I navigate to "Lists" page via "Segments"
    Then I create a Static Segment using "CSV Editor" with name "StaticListCSV"
    And I enter segment name "StaticListCSV" and select file from test data to upload
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I verify that the Static segment is displayed on Listing Page

  @InjectCredentials
  Scenario: Inject Credentials
    Given I select the function "Integrations" for building the test data
    Given I select the function "SDK" for building the test data

  @InjectUsers
  Scenario: Create required Users
    Given I create all the users reuqired for test suite
    And I select the function "Users" for building the test data
    And hit "performEvent" API from "PerformEventsInject" sheet to trigger Event with reference id "orderPlaced"
    And hit "performEvent" API from "PerformEventsInject" sheet to trigger Event with reference id "triggerSMSEvent"
    And hit "performEvent" API from "PerformEventsInject" sheet to trigger Event with reference id "triggerEmailEvent"
    And hit "performEvent" API from "PerformEventsInject" sheet to trigger Event with reference id "triggerPushEvent"
    And hit "performEvent" API from "PerformEventsInject" sheet to trigger Event with reference id "addedToCart"
    And hit "performEvent" API from "PerformEventsInject" sheet to trigger Event with reference id "triggerWhatsAppEvent"
    And hit "performEvent" API from "PerformEventsInject" sheet to trigger Event with reference id "triggerWebPushEvent"

  @InjectTags
  Scenario: Attach tags to created segments and campaigns
    Given I select the function "Tags" for building the test data

  @InjectProperties
  Scenario: Inject Properties
    Given I select the function "InLineContentWebP" for building the test data
    Given I select the function "InLineContentAppP" for building the test data

  @InjectCampaigns
  Scenario: Create campaigns
    Given I select the channel "SMS" for campaign creation
    Given I select the channel "Email" for campaign creation
    Given I select the channel "Push" for campaign creation
    Given I select the channel "WebP" for campaign creation
    Given I select the channel "WhatsApp" for campaign creation
    Given I select the channel "InLineContentAppP" for campaign creation
    Given I select the channel "InApp" for campaign creation

  @InjectCatalogs
  Scenario: Inject Catalogs
    Given I select the function "Catalogs" for building the test data

  @InjectMultiEvent
  Scenario: Trigger events using API
    And hit "performEvent" API from "PerformEvents" sheet to trigger Event with reference id "triggerSMSEvent"
    And hit "performEvent" API from "PerformEvents" sheet to trigger Event with reference id "triggerEmailEvent"

  @InjectMultiEvent
  Scenario Outline: Inject Single & Multi Event Data
    Given I navigate to "Upload Data" page via "Data Platform"
    Then I click on "Upload Events Data" tab
    Then I upload the file for "<fileName>"
    Then I verify upload status as "IN PROGRESS"
    And I verify the file upload status via API
    Then I verify upload status as "COMPLETED"

    Examples: 
      | fileName                       |
      | multi_event                    |
      | multi_event_A_50_uni_occ       |
      | multi_event_B_50_uni_100_occur |
      | multi_event_C_30_uni_20_occ    |
      | multi_event_D_15_uni_10_occ    |
      | multi_event_E_25_uni_20_occ    |