@WSP_Onboarding
Feature: WSP Onboarding feature

  @Text @Image @Video @Document  
  Scenario: To Verify user can add WSP
    Given I navigate to "Integrations" page via "Data Platform"
    Then I click on Configure "WhatsApp Setup"
    And delete "Onboarding SP Test" config if it exists in "WhatsApp"
    And "create" provider for "WhatsApp" by adding details via Template and also validating the same

  @Text
  Scenario: To create new WP text Template
    Given I navigate to "Integrations" page via "Data Platform"
    Then I click on Configure "WhatsApp Setup"
    Then fill the page via "WhatsappIntTextTemplate" template from "IntegrationPayloads/WhatsApp" folder using "Data" section

  @Image
  Scenario: To create new WP image Template
    Given I navigate to "Integrations" page via "Data Platform"
    Then I click on Configure "WhatsApp Setup"
    Then fill the page via "WhatsappIntImageTemplate" template from "IntegrationPayloads/WhatsApp" folder using "Data" section

  @Image 
  Scenario: To create new WP inavlid image Template
    Given I navigate to "Integrations" page via "Data Platform"
    Then I click on Configure "WhatsApp Setup"
    Then fill the page via "WhatsappIntInvalidImgTextTemplate" template from "IntegrationPayloads/WhatsApp" folder using "Data" section

  @Video 
  Scenario: To create new WP video Template
    Given I navigate to "Integrations" page via "Data Platform"
    Then I click on Configure "WhatsApp Setup"
    Then fill the page via "WhatsappIntVideoTemplate" template from "IntegrationPayloads/WhatsApp" folder using "Data" section

  @Document 
  Scenario: To create new WP document Template
    Given I navigate to "Integrations" page via "Data Platform"
    Then I click on Configure "WhatsApp Setup"
    Then fill the page via "WhatsappIntDocumentTemplate" template from "IntegrationPayloads/WhatsApp" folder using "Data" section

  @Text @Image @Document @Video
  Scenario: Choose user to perform SP Onboarding on
    Given I navigate to "Live Segments" page via "Segments"
    And I search for "OnboardingUser" segment
    Then I delete segment with Name "OnboardingUser"
    Then I create a Live segment with name as "OnboardingUser"
    Then fill the page via "OnboardingUser" template from "TestData" folder using "UserCard" section
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.

  @Text
  Scenario: To verify template type Text should successfully sent with valid number
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "One-time" Campaign Type and enters "Sent/Delivered Status-One-time Text WSP Onboarding" Campaign Name
    Then select Segment having name as "OnboardingUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "WSP_Onboarding_TextMessage" template from "IntegrationPayloads/WhatsApp" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I fetch "whatsappAggregates" api and add dynamic CampaignId
    Then wait "10" minutes for "whatsappAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent       | 1 |
      | deliveries | 1 |

  @Text
  Scenario: To verify template type text should show error message with invalid number
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "One-time" Campaign Type and enters "WSP Onboarding Invalid Number" Campaign Name
    And I select New Segment Creation icon
    And I add "Segment Name" as "User Condition > User IDs"
    And Pass following details in user card
      | Visitor Type | All Users           |
      | User IDs     | equal to>TestUser02 |
    And I Save and verify the created "Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "WSP_Onboarding_TextMessage" template from "IntegrationPayloads/WhatsApp" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I fetch "whatsappAggregates" api and add dynamic CampaignId
    Then wait "10" minutes for "whatsappAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent                    | 1 |
      | failures                | 1 |
      | Invalid WhatsApp Number | 1 |

  @Text
  Scenario: To verify template type text with unreachable whatsapp should not show successfull event
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "One-time" Campaign Type and enters "WSP Onboarding unreachable WhatsApp" Campaign Name
    And I select New Segment Creation icon
    And I add "Segment Name" as "User Condition > User IDs"
    And Pass following details in user card
      | Visitor Type | All Users           |
      | User IDs     | equal to>TestUser03 |
    And I Save and verify the created "Segment"
    Then I verify reachable user count as "0"

  @Image
  Scenario: To verify template type Image with JPG format should successfully sent with valid number
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "One-time" Campaign Type and enters "Sent/Delivered Status-One-time JPG Image WSP Onboarding" Campaign Name
    Then select Segment having name as "OnboardingUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "WSP_Onboarding_ImageJpgMessage" template from "IntegrationPayloads/WhatsApp" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I fetch "whatsappAggregates" api and add dynamic CampaignId
    Then wait "10" minutes for "whatsappAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent       | 1 |
      | deliveries | 1 |

  @Image
  Scenario: To verify template type Image with PNG format should successfully sent with valid number
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "One-time" Campaign Type and enters "Sent/Delivered Status-One-time PNG Image WSP Onboarding" Campaign Name
    Then select Segment having name as "OnboardingUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "WSP_Onboarding_ImagePNGMessage" template from "IntegrationPayloads/WhatsApp" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I fetch "whatsappAggregates" api and add dynamic CampaignId
    Then wait "10" minutes for "whatsappAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent       | 1 |
      | deliveries | 1 |

  @Image 
  Scenario: To verify with template type image should show error message with invalid number
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "One-time" Campaign Type and enters "Sent/Delivered Status-One-time Error Image WSP Onboarding" Campaign Name
    And I select New Segment Creation icon
    And I add "Segment Name" as "User Condition > User IDs"
    And Pass following details in user card
      | Visitor Type | All Users           |
      | User IDs     | equal to>TestUser02 |
    And I Save and verify the created "Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "WSP_Onboarding_ImageJpgMessage" template from "IntegrationPayloads/WhatsApp" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I fetch "whatsappAggregates" api and add dynamic CampaignId
    Then wait "10" minutes for "whatsappAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent                    | 1 |
      | failures                | 1 |
      | Invalid WhatsApp Number | 1 |

  @Image
  Scenario: To verify with template type image should fail with random template name
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "One-time" Campaign Type and enters "Sent/Delivered Status-One-time Invalid Image WSP Onboarding" Campaign Name
    Then select Segment having name as "OnboardingUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "WSP_Onboarding_InvalidImageMessage" template from "IntegrationPayloads/WhatsApp" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I fetch "whatsappAggregates" api and add dynamic CampaignId
    Then wait "10" minutes for "whatsappAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent           | 1 |
      | failures       | 1 |
      | Other Failures | 1 |

  @Video
  Scenario: To verify template type Video should successfully sent with valid number
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "One-time" Campaign Type and enters "Sent/Delivered Status-One-time Mp4 Video WSP Onboarding" Campaign Name
    Then select Segment having name as "OnboardingUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "WSP_Onboarding_VideoMp4Message" template from "IntegrationPayloads/WhatsApp" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I fetch "whatsappAggregates" api and add dynamic CampaignId
    Then wait "10" minutes for "whatsappAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent       | 1 |
      | deliveries | 1 |

  @Video
  Scenario: To verify delivery of whatsapp message with type video other than mp4 format should be succesfull from WSP side but delivery should not happen
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "One-time" Campaign Type and enters "Sent/Delivered Status-One-time Flv Video WSP Onboarding" Campaign Name
    Then select Segment having name as "OnboardingUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "WSP_Onboarding_VideoFlvMessage" template from "IntegrationPayloads/WhatsApp" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I fetch "whatsappAggregates" api and add dynamic CampaignId
    Then wait "10" minutes for "whatsappAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent           | 1 |
      | failures       | 1 |
      | Other Failures | 1 |

  @Document 
  Scenario: To verify delivery of whatsapp message with type document pdf should be successfully sent
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "One-time" Campaign Type and enters "Sent/Delivered Status-One-time PDF WSP Onboarding" Campaign Name
    Then select Segment having name as "OnboardingUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "WSP_Onboarding_DocumentPdfMessage" template from "IntegrationPayloads/WhatsApp" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I fetch "whatsappAggregates" api and add dynamic CampaignId
    Then wait "10" minutes for "whatsappAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent       | 1 |
      | deliveries | 1 |

  @Document
  Scenario: To verify delivery of whatsapp message with type document word should be successfully sent
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "One-time" Campaign Type and enters "Sent/Delivered Status-One-time DOC WSP Onboarding" Campaign Name
    Then select Segment having name as "OnboardingUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "WSP_Onboarding_DocumentDocMessage" template from "IntegrationPayloads/WhatsApp" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I fetch "whatsappAggregates" api and add dynamic CampaignId
    Then wait "10" minutes for "whatsappAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent       | 1 |
      | deliveries | 1 |

  @Document
  Scenario: To verify delivery of whatsapp message with type document word should be successfully sent
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "One-time" Campaign Type and enters "Sent/Delivered Status-One-time XLS WSP Onboarding" Campaign Name
    Then select Segment having name as "OnboardingUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "WSP_Onboarding_DocumentXlsMessage" template from "IntegrationPayloads/WhatsApp" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I fetch "whatsappAggregates" api and add dynamic CampaignId
    Then wait "10" minutes for "whatsappAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent       | 1 |
      | deliveries | 1 |

  @Text
  Scenario Outline: To verify Stats are showing properly when phone number is without country code and plus sign
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "One-time" Campaign Type and enters "<campaignName>" Campaign Name
    And I select New Segment Creation icon
    And I add "Segment Name" as "User Condition > User IDs"
    And Pass following details in user card
      | Visitor Type | All Users         |
      | User IDs     | equal to><userId> |
    And I Save and verify the created "Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "WSP_Onboarding_TextMessage" template from "IntegrationPayloads/WhatsApp" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I fetch "whatsappAggregates" api and add dynamic CampaignId
    Then wait "10" minutes for "whatsappAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent       | 1 |
      | deliveries | 1 |

    Examples: 
      | userId     | campaignName                                    |
      | TestUser04 | SP Onboarding valid number without plus sign    |
      | TestUser05 | SP Onboarding valid number without country code |
      | TestUser06 | SP Onboarding valid number stating with 00      |

   @Text  
  Scenario: To verify template with invalid business number should fail
    Given I navigate to "Integrations" page via "Data Platform"
    Then I click on Configure "WhatsApp Setup"
    And "update" provider for "WhatsApp" by adding invalid details from template "WSP_Invalid BusinessNo" and also validating the same
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "One-time" Campaign Type and enters "Sent/Delivered Status-One-time Text WSP Invalid Business No" Campaign Name
    Then select Segment having name as "OnboardingUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "WSP_Onboarding_TextMessage" template from "IntegrationPayloads/WhatsApp" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I fetch "whatsappAggregates" api and add dynamic CampaignId
    Then wait "10" minutes for "whatsappAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent                    | 1 |
      | failures                | 1 |
      | WSP Configuration Error | 1 |

  @Text 
  Scenario: To verify template with invalid api key should fail
    Given I navigate to "Integrations" page via "Data Platform"
    Then I click on Configure "WhatsApp Setup"
    And "update" provider for "WhatsApp" by adding invalid details from template "WSP_InvalidApiKey" and also validating the same
    Given I navigate to "WhatsApp" page via "Channels"
    And I select "One-time" Campaign Type and enters "Sent/Delivered Status-One-time Text WSP Invalid Api Key" Campaign Name
    Then select Segment having name as "OnboardingUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "WSP_Onboarding_TextMessage" template from "IntegrationPayloads/WhatsApp" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I fetch "whatsappAggregates" api and add dynamic CampaignId
    Then wait "10" minutes for "whatsappAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent                    | 1 |
      | failures                | 1 |
      | WSP Configuration Error | 1 |