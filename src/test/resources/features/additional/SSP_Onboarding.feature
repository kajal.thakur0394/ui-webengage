@SSP_Onboarding
Feature: SSP Onboarding feature

  Scenario: To Verify user can add new SSP with invalid principle ID, Password, etc
    Given I navigate to "Integrations" page via "Data Platform"
    Then I click on Configure "SMS Setup"
    And delete "Onboarding SP Test" config if it exists in "SMS"
    And "create" provider for "SMS" by adding invalid details from template "SSP_invalid" and also validating the same

  Scenario: Choose user to perform SP Onboarding on
    Given I navigate to "Live Segments" page via "Segments"
    And I search for "OnboardingUser" segment
    Then I delete segment with Name "OnboardingUser"
    Then I create a Live segment with name as "OnboardingUser"
    Then fill the page via "OnboardingUser" template from "TestData" folder using "UserCard" section
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.

  Scenario: To Verify on succesfull integration of new SSP, it is listed on listing page and also edit the same
    Given I navigate to "Integrations" page via "Data Platform"
    Then I click on Configure "SMS Setup"
    And delete "Onboarding SP Test" config if it exists in "SMS"
    And "create" provider for "SMS" by adding details via Template and also validating the same
    And "update" provider for "SMS" by adding invalid details from template "SSP_edit" and also validating the same
    And delete "Onboarding SP Test" config if it exists in "SMS"
    And "create" provider for "SMS" by adding details via Template and also validating the same

  Scenario: To verify Stats are showing properly when One-time SMS Campaign type is selected
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "SP Onboarding One-time SMS Campaign" Campaign Name
    Then select Segment having name as "OnboardingUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "SSP_Onboarding_Message" template from "IntegrationPayloads/SMS" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I fetch "smsAggregates" api and add dynamic CampaignId
    Then wait "default" minutes for "smsAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent       | 1 |
      | deliveries | 1 |

  Scenario: To verify SMS is send to users and stats are showing properly when Transactional SMS Campaign type is selected
    Given I navigate to "SMS" page via "Channels"
    And I select "Transactional" Campaign Type and enters "SP Onboarding Transactional SMS Campaign" Campaign Name
    Then I fill the details in Audience tab as below
      |  |  |
    Then fill the page via "SSP_Onboarding_Message" template from "IntegrationPayloads/SMS" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    When I hit the POST method using static body for "transactionalCampaign" api from "SMSCampaigns" sheet with ref id "customTransactionalSMS"
    Then I fetch "smsAggregates" api and add dynamic CampaignId
    Then wait "default" minutes for "smsAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent       | 1 |
      | deliveries | 1 |

  Scenario: To verify Stats are showing properly when Triggered SMS Campaign type is selected
    Given I navigate to "SMS" page via "Channels"
    And I select "Triggered" Campaign Type and enters "SP Onboarding Triggered SMS Campaign" Campaign Name
    Then select Segment having name as "OnboardingUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | UPON OCCURRENCE OF | TriggerSMSEvent |  |  |
    Then fill the page via "SSP_Onboarding_Message" template from "IntegrationPayloads/SMS" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then wait for "fetchSMSCampaign" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    And hit "performEvent" API from "PerformEvents" sheet to trigger Event with reference id "customSMSTrigger"
    Then I fetch "smsAggregates" api and add dynamic CampaignId
    Then wait "10" minutes for "smsAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent       | 1 |
      | deliveries | 1 |

  Scenario: To verify Stats are showing properly when Recurring SMS Campaign type is selected
    Given I navigate to "SMS" page via "Channels"
    And I select "Recurring" Campaign Type and enters "SP Onboarding Recurring SMS Campaign" Campaign Name
    Then select Segment having name as "OnboardingUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | DELIVERY SCHEDULE | Day |  |  |
    Then I adjust the time for Recurring Campaign by checking system time
    Then fill the page via "SSP_Onboarding_Message" template from "IntegrationPayloads/SMS" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then wait for "fetchSMSCampaign" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    Then I fetch "smsAggregates" api and add dynamic CampaignId
    Then wait "10" minutes for "smsAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent       | 1 |
      | deliveries | 1 |

  Scenario: To Verify journey with new SSP campaign is delivered to users device
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "SP Onboarding Journey"
    Then I use the Journey/Relay Template "journeySPOnboarding"
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page
    And hit API for Event trigger with reference id "customSMSTrigger"
    Then verify the stats of Journey by checking overall stats in api response
    Then perform Journey validation via "Polling" mechanism
    And verify status as "RUNNING" for "SP Onboarding Journey" Journey
    Then "Stop" the "Journey" with name as "SP Onboarding Journey"
    And verify status as "STOPPING" for "SP Onboarding Journey" Journey

  Scenario: To Verify campaign with Sender Id invalid in Message tab shows Sender ID Invalid failure
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "SP Onboarding invalid Sender ID" Campaign Name
    Then select Segment having name as "OnboardingUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "SSP_Onboarding_InvalidSender" template from "IntegrationPayloads/SMS" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I fetch "smsAggregates" api and add dynamic CampaignId
    Then wait "default" minutes for "smsAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent              | 1 |
      | failures          | 1 |
      | Sender ID Invalid | 1 |

  Scenario: To verify campaign with invalid message in Message tab, shows failure
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "SP Onboarding invalid Message" Campaign Name
    Then select Segment having name as "OnboardingUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "SSP_Onboarding_InvalidMessage" template from "IntegrationPayloads/SMS" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I fetch "smsAggregates" api and add dynamic CampaignId
    Then wait "default" minutes for "smsAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent     | 1 |
      | failures | 1 |
      | Rejected | 1 |

  Scenario: To Verify Campaign with template ID invalid, shows failure
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "SP Onboarding invalid Template ID" Campaign Name
    Then select Segment having name as "OnboardingUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "SSP_Onboarding_InvalidTemplateID" template from "IntegrationPayloads/SMS" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I fetch "smsAggregates" api and add dynamic CampaignId
    Then wait "default" minutes for "smsAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent      | 1 |
      | failures  | 1 |
      | DLT Error | 1 |

  Scenario: To Verify SMS is not sent to User with valid mobile number but not reachable
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "SP Onboarding valid number non reachable user" Campaign Name
    And I select New Segment Creation icon
    And I add "Segment Name" as "User Condition > User IDs"
    And Pass following details in user card
      | Visitor Type | All Users           |
      | User IDs     | equal to>TestUser01 |
    And I Save and verify the created "Segment"
    Then I verify reachable user count as "0"

  Scenario Outline: To verify Stats are showing properly when phone number is without country code and plus sign
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "<campaignName>" Campaign Name
    And I select New Segment Creation icon
    And I add "Segment Name" as "User Condition > User IDs"
    And Pass following details in user card
      | Visitor Type | All Users         |
      | User IDs     | equal to><userId> |
    And I Save and verify the created "Segment"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "SSP_Onboarding_Message" template from "IntegrationPayloads/SMS" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I fetch "smsAggregates" api and add dynamic CampaignId
    Then wait "default" minutes for "smsAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent       | 1 |
      | deliveries | 1 |
    
    Examples: 
      | userId     | campaignName                                    |
      | TestUser04 | SP Onboarding valid number without plus sign    |
      | TestUser05 | SP Onboarding valid number without country code |
      | TestUser06 | SP Onboarding valid number stating with 00      |
    
   Scenario: To Verify failure on sending SMS with invalid creds but  with valid user and valid message
    Given I navigate to "Integrations" page via "Data Platform"
    Then I click on Configure "SMS Setup"
    And "update" provider for "SMS" by adding invalid details from template "SSP_invalidPass" and also validating the same
    Given I navigate to "SMS" page via "Channels"
    And I select "One-time" Campaign Type and enters "SP Onboarding invalid creds" Campaign Name
    Then select Segment having name as "OnboardingUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "SSP_Onboarding_Message" template from "IntegrationPayloads/SMS" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I fetch "smsAggregates" api and add dynamic CampaignId
    Then wait "default" minutes for "smsAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent                   | 1 |
      | failures               | 1 |
      | Authentication Failure | 1 |