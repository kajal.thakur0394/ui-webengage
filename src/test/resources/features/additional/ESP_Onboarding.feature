@ESP_Onboarding
Feature: ESP Onboarding feature

  Scenario: To Verify user cannot add new ESP with invalid api key
    Given I navigate to "Integrations" page via "Data Platform"
    Then I click on Configure "Email Setup"
    And delete "Onboarding SP Test" config if it exists in "Email"
    And "create" provider for "EmailInvalid" by adding invalid details from template "ESP_invalid" and also validating the same

  Scenario: Choose user to perform SP Onboarding on
    Given I navigate to "Live Segments" page via "Segments"
    And I search for "OnboardingUser" segment
    Then I delete segment with Name "OnboardingUser"
    Then I create a Live segment with name as "OnboardingUser"
    Then fill the page via "OnboardingUser" template from "TestData" folder using "UserCard" section
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.

  Scenario: To Verify on succesfull integration of new ESP, it is listed on listing page
    Given I navigate to "Integrations" page via "Data Platform"
    Then I click on Configure "Email Setup"
    And delete "Onboarding SP Test" config if it exists in "Email"
    And "create" provider for "Email" by adding details via Template and also validating the same

  Scenario: To verify Stats are showing properly when One-time Email Campaign type is selected
    Given I navigate to "Email" page via "Channels"
    And I select "One-time" Campaign Type and enters "SP Onboarding One-time Email Campaign" Campaign Name
    Then select Segment having name as "OnboardingUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "ESP_Onboarding_Message" template from "IntegrationPayloads/Email" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I fetch "emailAggregates" api and add dynamic CampaignId
    Then wait "default" minutes for "emailAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent       | 1 |
      | deliveries | 1 |

  Scenario: To verify Stats are showing properly when Recurring Email Campaign type is selected
    Given I navigate to "Email" page via "Channels"
    And I select "Recurring" Campaign Type and enters "SP Onboarding Recurring Email Campaign" Campaign Name
    Then select Segment having name as "OnboardingUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | DELIVERY SCHEDULE | Day |  |  |
    Then I adjust the time for Recurring Campaign by checking system time
    Then fill the page via "ESP_Onboarding_Message" template from "IntegrationPayloads/Email" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then wait for "fetchEmailCampaign" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    Then check overview page is loaded for "SP Onboarding Recurring Email Campaign" campaign
    Given I navigate to "Email" page via "Channels"
    Then I fetch "emailAggregates" api and add dynamic CampaignId
    Then wait "10" minutes for "emailAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent       | 1 |
      | deliveries | 1 |

  Scenario: To verify Stats are showing properly when Triggered Email Campaign type is selected
    Given I navigate to "Email" page via "Channels"
    And I select "Triggered" Campaign Type and enters "SP Onboarding Triggered Email Campaign" Campaign Name
    Then select Segment having name as "OnboardingUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | UPON OCCURRENCE OF | TriggerEmailEvent |  |  |
    Then fill the page via "ESP_Onboarding_Message" template from "IntegrationPayloads/Email" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then wait for "fetchEmailCampaign" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    And hit "performEvent" API from "PerformEvents" sheet to trigger Event with reference id "customTriggerEmailEvent"
    Then I fetch "emailAggregates" api and add dynamic CampaignId
    Then wait "10" minutes for "emailAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent       | 1 |
      | deliveries | 1 |

  Scenario: To verify Email is send to users and stats are showing properly when Transactional Email Campaign type is selected
    Given I navigate to "Email" page via "Channels"
    And I select "Transactional" Campaign Type and enters "SP Onboarding Transactional Email Campaign" Campaign Name
    Then I fill the details in Audience tab as below
      |  |  |
    Then fill the page via "ESP_Onboarding_TransactionalMessage" template from "IntegrationPayloads/Email" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    When I hit the POST method using static body for "transactionalCampaign" api from "EmailCampaigns" sheet with ref id "customTransactionalEmail"
    Then I fetch "emailAggregates" api and add dynamic CampaignId
    Then wait "default" minutes for "emailAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent       | 1 |
      | deliveries | 1 |

  Scenario: To verify journey with new ESP campaign is delivered to users device
    Given I navigate to "Journeys" Page
    And I create a new Journey/Relay with name as "SP Onboarding Journey"
    Then I use the Journey/Relay Template "journeyESPOnboarding"
    Then I publish the "Journey"
    Then I navigate back to "Journeys" Page
    And hit API for Event trigger with reference id "customTriggerEmailEvent"
    Then verify the stats of Journey by checking overall stats in api response
    Then perform Journey validation via "Polling" mechanism
    And verify status as "RUNNING" for "SP Onboarding Journey" Journey
    Then "Stop" the "Journey" with name as "SP Onboarding Journey"
    And verify status as "STOPPING" for "SP Onboarding Journey" Journey

  Scenario: To Verify campaign with invalid from name in message tab gets sent
    Given I navigate to "Email" page via "Channels"
    And I select "One-time" Campaign Type and enters "SP Onboarding Invalid From Name" Campaign Name
    Then select Segment having name as "OnboardingUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "ESP_Onboarding_InvalidFromNameMessage" template from "IntegrationPayloads/Email" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I fetch "emailAggregates" api and add dynamic CampaignId
    Then wait "default" minutes for "emailAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent       | 1 |
      | deliveries | 1 |

  Scenario: To Verify campaign with invalid email in message tab shows failure
    Given I navigate to "Email" page via "Channels"
    And I select "One-time" Campaign Type and enters "SP Onboarding Invalid Email" Campaign Name
    Then select Segment having name as "OnboardingUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "ESP_Onboarding_InvalidEmailMessage" template from "IntegrationPayloads/Email" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I fetch "emailAggregates" api and add dynamic CampaignId
    Then wait "default" minutes for "emailAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent           | 1 |
      | failures       | 1 |
      | Other Failures | 1 |

  Scenario: To Verify email is not sent to user with invalid emaiid and not reachable
    Given I navigate to "Email" page via "Channels"
    And I select "One-time" Campaign Type and enters "SP Onboarding Invalid Email And Non Reachable User" Campaign Name
    And I select New Segment Creation icon
    And I add "Segment Name" as "User Condition > User IDs"
    And Pass following details in user card
      | Visitor Type | All Users           |
      | User IDs     | equal to>TestUser04 |
    And I Save and verify the created "Segment"
    Then I verify reachable user count as "0"

  Scenario: To verify failure on sending email campaign with invalid creds but with valid user and valid message
    Given I navigate to "Integrations" page via "Data Platform"
    Then I click on Configure "Email Setup"
    And "update" provider for "Email" by adding invalid details from template "ESP_invalidApiKey" and also validating the same
    Given I navigate to "Email" page via "Channels"
    And I select "One-time" Campaign Type and enters "SP Invalid Creds Valid User" Campaign Name
    Then select Segment having name as "OnboardingUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then fill the page via "ESP_Onboarding_Message" template from "IntegrationPayloads/Email" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I skip the Campaign Test
    And I launch the Campaign
    Then I fetch "emailAggregates" api and add dynamic CampaignId
    Then wait "default" minutes for "emailAggregates" api to return response for Listpath "response.data[0].dimensions[0].metrics" as below
      | sent           | 1 |
      | failures       | 1 |
      | Other Failures | 1 |
