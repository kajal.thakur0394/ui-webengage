package com.webengage.ui_automation.pojopackages.user;

public class User {

	private String userId;
	private String firstName;
	private String lastName;
	private String birthDate;
	private String gender;
	private String email;
	private String phone;
	private String company;
	private Boolean smsOptIn;
	private Boolean emailOptIn;
	private Boolean whatsappOptIn;
	private Attributes attributes;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public Boolean getSmsOptIn() {
		return smsOptIn;
	}

	public void setSmsOptIn(Boolean smsOptIn) {
		this.smsOptIn = smsOptIn;
	}

	public Boolean getWhatsappOptIn() {
		return whatsappOptIn;
	}

	public void setWhatsappOptIn(Boolean whatsappOptIn) {
		this.whatsappOptIn = whatsappOptIn;
	}
	
	public void setEmailOptIn(Boolean emailOptIn) {
        this.emailOptIn = emailOptIn;
    }
	
    public Boolean getEmailOptIn() {
        return emailOptIn;
    }

	public Attributes getAttributes() {
		return attributes;
	}

	public void setAttributes(Attributes attributes) {
		this.attributes = attributes;
	}

}
