package com.webengage.ui_automation.elements;

import org.openqa.selenium.By;

public enum InlineContentAppPerPageElements {
	TITLE_IFRAME(getId("app-text-field_ifr")), 
	DESC_IFRAME(getId("app-desc-field_ifr")),
	TITLE_TEXT(getXpath("//*[@data-id='app-text-field']")), 
	DESC_TEXT(getXpath("//*[@data-id='app-desc-field']")),
	BANNER_IFRAME(getId("app-button-title-field_ifr")),
	BANNER_BTN_LABEL(getXpath("//*[@data-id='app-button-title-field']")),
	ANDROID_ONCLICK_BTN_URL(getXpath("//h5[text()='Button']/../following-sibling::*//*[text()='On-click Action (Android)']/../..//input")),
	ICON_TOGGLE(getXpath("//h5[text()='Icon']/following-sibling::div//div[@class='react-toggle']")),
	KEY_VALUE_PAIR_TOGGLE(getXpath("//h5[text()='Key-Value Pairs']/following-sibling::div//div[@class='react-toggle']")),
	ON_CLICK_ACTION_TA(getXpath("//*[text()='Description']/../../following-sibling::*//label[text()='On-click Action (Android)']/../following-sibling::*//input"));

	public static String LAYOUT = "//span[text()='%s']/../..";
	public static String KEY_VALUE = "//div[@class='row form__row'][%s1]//input[@placeholder='%s2']";
	public static String CUSTOM_TEMPLATE_KEY_VALUE = "//div[contains(@class,'row form__row ')][%s1]//input[@placeholder='%s2']";
	public static String CUSTOM_TEMPLATE_IMAGE = "//*[text()='%s']/ancestor::div[contains(@class,'row')]//img";
	public static String TEXT_ACTION_URL="//span[text()='Text & Action']/../../following-sibling::*/*//label[text()='%s']/../following-sibling::*//input";
	By locator;

	InlineContentAppPerPageElements(By locator) {
		this.locator = locator;
	}

	public By locate() {
		return this.locator;
	}

	static By getXpath(String element) {
		return By.xpath(element);
	}

	static By getClassName(String element) {
		return By.className(element);
	}

	static By getId(String element) {
		return By.id(element);
	}
}
