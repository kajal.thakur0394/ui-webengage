package com.webengage.ui_automation.elements;

import org.openqa.selenium.By;

public enum CatalogsPageElements {
	CATALOG_NAME(getXpath("//label[text()='CATALOG NAME']/parent::div//following-sibling::*//input")),
	COLUMN(getXpath("//div[@aria-label='-' and contains(@class,'r-ss-placeholder')]")),
	EVENT_MAP(getXpath("//div[@aria-label='open menu' and contains(@class,'r-ss-placeholder')]")),
	MAP_EVENTS_BTN(getXpath("//span[text()='Map Events']"));
	
	By locator;

	
	public static String UPLOAD_TYPE ="//input[@value='%s']/parent::label";
	public static String CATALOG_NAME_DD ="//label[text()='%s']/parent::div/following-sibling::div//*[@class='Select-placeholder']";
	public static String CATALOG_NAME_ENTRY ="//label[text()='%s']/parent::div/following-sibling::div//*[@class='Select-placeholder']/following-sibling::*//input";
	public static String COLUMN_EVENT="//span[@title='%s']";

	CatalogsPageElements(By locator) {
		this.locator = locator;
	}

	public By locate() {
		return this.locator;
	}

	static By getXpath(String element) {
		return By.xpath(element);
	}

	static By getName(String element) {
		return By.name(element);
	}

	static By getId(String element) {
		return By.id(element);
	}

	static By getCSS(String element) {
		return By.xpath(element);
	}

	static By getClassName(String element) {
		return By.className(element);
	}
}
