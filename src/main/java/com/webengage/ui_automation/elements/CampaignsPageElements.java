package com.webengage.ui_automation.elements;

import org.openqa.selenium.By;

public enum CampaignsPageElements {

	SEGMENT_CREATION_ICON(getXpath("//span[@data-tooltip='Create New Segment']")),
	EXCLUDE_SEGMENT_CREATION_ICON(getXpath("//label[text()='EXCLUDE USERS FROM THESE SEGMENTS']/parent::div/following-sibling::div//span[@data-tooltip='Create New Segment']")),
	CHANNELS_LIST(getXpath("//span[text()='Channels']/ancestor::li")),
	WEB_PERSONALIZATION_LIST(getXpath("//span[text()='Web Personalization']/ancestor::li")),
	SELECT_SEGMENT(getXpath("//*[@aria-label='Select...'] | (//*[text()='Select Segments'])[last()] | (//*[text()='-'])[last()] | //*[@aria-label='open menu']//span[text()='-']")),
	CREATE_BTN(getXpath("//a[contains(@href,'create')]")),
	POP_OVER_DELETE(getXpath("//*[text()='Delete']")),
	EVENT_DROPDOWN(getXpath("//*[@aria-label='open menu'][not(@disabled)]/..")),
	SAVE_CONTINUE_BTN(getXpath("//button[text()='SAVE & CONTINUE']")),
	SKIP_TEST_BTN(getXpath("//button[text()='SKIP TEST']")),
	EMAIL_BODY(getId("tiny-editor_ifr")),	
	SENDER(getXpath("//input[contains(@placeholder,'MY-COMPANY')]")),
	MESSAGE_TEXTAREA(getXpath("//label[text()='Message']/parent::div/following-sibling::div/div/textarea")),
	DLT_TEMPLATE_ID(getXpath("//label[text()='DLT TEMPLATE ID']/parent::div/following-sibling::div//input")),
	SEGMENT_BREW(getXpath("//h5[text()='Important']")),
	EXCLUDE_SEGMENTS_EMPTY_BOX(getXpath("//*[text()='EXCLUDE USERS FROM THESE SEGMENTS']/../following-sibling::div//span[text()='-']")),
	SEGMENT_BREW_OK(getXpath("//button[@class='button']")),
	SPECIFIC_HOUR(getXpath("//input[@name='react-select-hours']")),
	SPECIFIC_MINUTE(getXpath("//input[@name='react-select-minutes']")),
	SPECIFIC_WRAPPER(getXpath("//input[@name='react-select-wrapper']")),
	SPECIFIC_TIMEZONE(getXpath("//input[@name='timezone']/following-sibling::div//span[contains(@id,'react-select')]")),
	TOOLTIP_WEBP(getXpath("//*[contains(@class,'tooltip')]//p")),
	SHOW_LIMIT(getXpath("//*[contains(text(),'viewed upto')]")),
	TYPE_NUMBER(getXpath("//*[contains(text(),'viewed upto')]/ancestor::div[contains(@class,'row align')]//input[@type='text']")),
	QUEUING_PERIOD(getXpath("//*[text()='QUEUEING']/ancestor::*[contains(@class,'form__row')]//*[@class='Select-value']")),
	TEST_SEGMENT_NAME(getXpath("//*[text()='Segment Name']/ancestor::div[contains(@class,'row')]//input")),
	TEST_FILTER_DD(getXpath("//*[text()='Filters']/ancestor::div[contains(@class,'row')]//*[@class='Select-control']")),
	TEST_FILTER_VALUE(getXpath("//*[text()='Filters']/ancestor::div[contains(@class,'row')]//*[@type='text']")),
	PREVIEW_TEXT(getXpath("//div[@class='preview']//div[contains(@class,'preview-message')]")),
	REACHABLE_USER_TEXT(getClassName("card__title__count")),
	AUDIENCE_REACAHBLE_SECTION(getXpath("//*[contains(@class,'segment-view-title ')] | //*[contains(text(),'Reachable Users')]")),
	FETCH_DD_VALUES(getXpath("//*[contains(@class,'r-ss-dropdown-options')]//li"));
	
	public static String SEG_USER_TYPE = "//label[contains(text(),'%s')]/parent::div/following-sibling::div//div[@class='Select-control']";
	public static String SEGMENT_TYPE = "//p[text()='%s1']/parent::div//div/button[text()='%s2']";
	public static String OPEN_DD = "//*[text()='%s']/../following-sibling::div/descendant::div[contains(@class,'col') or contains(@class,'form')]//div[@class='Select-placeholder' or @class='Select-value']";
	public static String ENTER_TEXT_FOR = "//*[text()='%s']/../following-sibling::div//input | //*[text()='%s']/../following-sibling::div//textarea";
	public static String SELECT_TEMPLATE = "//span[text()='%s']/../..";
	public static String TEMPLATE_WITH_ATTRIBUTES = "//span[text()='%s']/..";
	public static String SELECT_FROM_DD = "//*[@aria-label='%s' or @title='%s'][not(@disabled)]";
	public static String OPEN_POP_OVER = "//*[contains(@class,'table__container-fixed')]/descendant::*[@title='%s']/ancestor::tr/descendant::*[@class='pop-over ']";
	public static String OPEN_DD_SELECT = "//*[text()='%s1']/../following-sibling::div/descendant::div[contains(@class,'col') or contains(@class,'form')]/descendant::*[text()='%s2']";
	public static String HOURS_DD="//*[text()='%s']/../..//*[@name='react-select-hours']/..//*[@class='Select-control']";
	public static String MINUTES_DD="//*[text()='%s']/../..//*[@name='react-select-minutes']/..//*[@class='Select-control']";
	public static String AM_PM_DD="//*[text()='%s']/../..//*[@value='am' or @value='pm']/..//*[@class='Select-control']";
	public static String DATE_PICKER="//*[text()='%s']/ancestor::div[contains(@class,'row')][1]//*[@class='SingleDatePickerInput']";
	public static String CAMPAIGN_TYPE = "//li/span[text()='%s']";
	public static String SUB_SECTION= "//span[text()='%s1']/../../following-sibling::ul//span[text()='%s2']";
	public static String INPUT_FIELD="//*[text()='%s']/../following-sibling::*//*[contains(@class,'field')]";
	public static String UPLOAD_FIELD="//*[text()='%s']/../following-sibling::div//*[@type='file']";
	public static String RESET_BTN="//*[text()='%s']/../following-sibling::div//*[text()='Reset']";
	public static String CHOOSE_PLATFORM="//*[contains(text(),'Target %s')]/../..//*[text()='Send to specific apps']";
	public static String PLATFORM_EDIT_BTN="//*[@data-tooltip='Select Target %s Apps']";
	public static String SELECT_PACKAGE="//*[text()='%s']/ancestor::td/following-sibling::td//label";
	public static String QUEUE_THROTTLE_INPUT="//*[text()='%s']/ancestor::*[contains(@class,'form__row')]//*[@type='number']";
	public static String LABEL_VALUE="//*[text()='%s']/../span";
	public static String BREADCRUMB="//*[text()='%s']/ancestor::*[@class='breadcrumbs__element']//a";
	public static String MESSAGEPERSONALISATION= "//*[text()='%s']/../following-sibling::div//*[contains(@id,'email-p')]//i|//*[contains(text(),'{%s}')]/..//div//*[contains(@id,'email-p')]//i";
	public static String PERSONALISATION_LABEL="//*[contains(text(),'%s1')]/following::*[text()='%s2']/parent::*[contains(@class,'list')]";
	public static String EVENT_TYPE="//*[text()='System']/%s1-sibling::*[text()='%s2']";
	public static String INCLUDE_EXCLUDE="//label[text()='%s1']/parent::div/following-sibling::div//span[text()='%s2']";
	public static String REMOVE_INCLUSION_EXCLUSION_SEGMENT="//label[text()='%s']/parent::div/following-sibling::div//span[@data-tooltip='Remove']";
	public static String INCLUDE_EXCLUDE_DROPDOWN="//*[text()='%s']/../following-sibling::div//*[contains(@class,'placeholder')]";
	public static String INCLUDE_EXCLUDE_SEGMENT="//*[contains(text(),'%s')]/ancestor::div[contains(@class,'form__row')]//*[text()='-']";
	
	By locator;

	CampaignsPageElements(By locator) {
		this.locator = locator;
	}

	public By locate() {
		return this.locator;
	}

	static By getXpath(String element) {
		return By.xpath(element);
	}

	static By getName(String element) {
		return By.name(element);
	}

	static By getId(String element) {
		return By.id(element);
	}

	static By getCSS(String element) {
		return By.xpath(element);
	}

	static By getClassName(String element) {
		return By.className(element);
	}
}
