package com.webengage.ui_automation.elements;

import org.openqa.selenium.By;

public enum CommonPageElements {
	SAVE_BUTTON(getXpath("//button[text()='Save'] | //button[text()='save'] | //button[text()='SAVE']")),
	CHECKBOX(getXpath("//*[@type='checkbox']/..")),
	TABLE_CHECKBOX(getXpath("//td[.=*]/following-sibling::td//*[contains(@class,'is-checked')]//*[@type='checkbox']/..")),
	SAVE_AND_CONTINUE_BUTTON(getXpath(
			"//button[text()='SAVE & CONTINUE'] | //button[text()='Save & Continue'] | //button[text()='Save and Continue']")),
	SKIP_TEST_BUTTON(getXpath("//button[text()='SKIP TEST & PROCEED']  | //button[text()='SKIP TEST']")),
	CONFIRM_PROCEED_BUTTON(getXpath("//button[text()='CONFIRM & PROCEED']")),
	LAUNCH_CAMPAIGN_BUTTON(getXpath("//button[text()='Launch Campaign']")),
	LOADER(getClassName("loader")),
	DD_SELECT_VALUE(getClassName("Select-value")),
	ADD_BTN(getXpath("//*[contains(@class,'fl-add')]")),
	EDIT_BTN(getXpath("//*[contains(@class,'fl-edit')]")),
	TRASH_BTN(getXpath("//*[contains(@class,'fl-trash')]")),
	DOWNLOAD_BTN(getXpath("//*[contains(@class,'fl-download')]")),
	SYNC_BTN(getXpath("//*[contains(@class,'fl-sync')]")),
	MORE_BTN(getXpath("//*[contains(@class,'fl-vert-more')]")),
	ADD_CRITERIA_BUTTON(getXpath("//button[text()='Add Criteria']")),
	DROPDOWN(getXpath("//div[@role='combobox']")),
	SAVE_AND_CLOSE_BUTTON(getXpath("//span[text()='Save & Close'] | //button[text()='SAVE & CLOSE']")),
	TOAST(getXpath("//*[@class='toast__body']")),
	CLOSE_TOAST(getXpath("//*[contains(@class,'toast__close')]")),
	LICENSE_CODE(getXpath("//*[text()='License Code:']/../following-sibling::div")),
	API_KEY(getXpath("//*[text()='API KEY:']/../following-sibling::div")),
	PERSONALIZATION_TOAST(getXpath("//div[@id='message']//p")),
	VERIFY_PROPERTYMODAL(getXpath("//a[contains(@class,'modal__close')]/..")),
	SEARCH(getXpath("//input[@placeholder='Search']")),
	TIME_PERIOD(getXpath("//*[@class='pop-over smart-date ']")),
	DOWNLOAD_CHANNEL_OVERVIEW_OR_ANALYZE(getXpath("//i[@title='download data as csv']")),
	ENGAGEMENT_SPLITBY_CHANNEL_DOWNLOAD(getXpath(
			"//div[@class='card__header display-flex justify-content-end']//i[@title='download data as csv']")),
	SELECT_CONTROL_DD(getXpath("//div[@class='Select-control']")),
	CUSTOM_DATE_MONTH(getXpath("//*[@data-visible='true']//*[@id='CalendarMonth__caption']/strong")),
	CUSTOM_DATE_PREVIOUS(getXpath("//button[contains(@aria-label,'previous month')]")),
	NAME_LINK_TEXT(getXpath(
			"//tbody/tr[@class='table__row']//a | //tbody/tr[@class='table__row']//span[contains(@class,'handle-text')]/span")),
	CUSTOM_DATE_NEXT(getXpath("//button[contains(@aria-label,'next month')]")),
	INPUT_SDK_USER(getId("enterUserId")),
	CLICK_LOGIN_BTN_SDK(getXpath("//*[text()='Home'] | //*[text()='Login']")),
	CREATE_ICON(getXpath("//button[@class='button--no-style'] | //i[contains(@class,'subhead__action')]")),
	DELETE_BUTTON(getXpath(
			"//button[text()='delete'] | //button[text()='DELETE'] | //button[text()='Delete'] | //button[contains(text(),'Delete')]")),
	NAME_ON_OVERVIEWPAGE(getXpath("//header//h4")),
	POP_OVER_DELETE(getXpath("//*[text()='DELETE'] | //*[text()='Delete']")),
	FILTER_TEXT(getXpath("//*[text()='OVER']/..//span")),
	TABLE_HEADER(getXpath("//div[@class='table__container ']//*[@class='table__block undefined']//th")),
	SEARCHBOX(getXpath("//input[contains(@placeholder,'Search by')]"));

	public static String MODULE_LIST_UTF = "//*[contains(@class,'container-fixed')]//tbody/tr//a[@title=\"%s\"] | //*[contains(@class,'container-fixed')]//tbody/tr//a[text()=\"%s\"] | //*[contains(@class,'container-fixed')]//tbody/tr//span[contains(@class,'handle-text')]/span[@title=\"%s\"]";
	public static String MODULE_LIST = "//*[contains(@class,'container-fixed')]//tbody/tr//a[@title='%s'] | //*[contains(@class,'container-fixed')]//tbody/tr//a[text()='%s']  | //*[contains(@class,'container-fixed')]//tbody/tr//span[contains(@class,'handle-text')]/span[@title='%s']";
	public static String MODULE_NAME = "//tbody/tr[@class='table__row']//a[@titile='%s']";
	public static String STATUS = "//a[@title='%s']/../../../following-sibling::td//span[contains(@class,'status-label')]";
	public static String STATUS_SINGLE_QUOTE = "//a[@title=\"%s\"]/../../../following-sibling::td//span[contains(@class,'status-label')]";
	public static String BUTTON = "//button[text()='%s']";
	public static String BUTTON_CONTAINS = "//button[contains(text(),'%s')]";
	public static String GENERIC_TEXT = "//*[text()='%s']";
	public static String GENERIC_NORMALIZE_SPACE = "//div[normalize-space()='%s']";
	public static String GENERIC_TEXT_LAST = "(//*[contains(text(),'%s')])[last()]";
	public static String GENERIC_LABEL = "//label[text()='%s']";
	public static String GENERIC_PLACEHOLDER = "//*[@placeholder='%s']";
	public static String GENERIC_PLACEHOLDER_CONTAINS = "//*[contains(@placeholder,'%s')]";
	public static String ANCHOR_TAG_GENERIC_TEXT = "//a[text()='%s']";
	public static String ANCHOR_TAG_GENERIC_TEXT_CONTAINS = "//a[contains(text(),'%s')]";
	public static String SECTION_TAB_LIST = "//span[text()='%s']/ancestor::li";
	public static String GENERIC_TEXT_CONTAINS = "//*[contains(text(),'%s')]";
	public static String DD_NAME = "//*[text()='%s']/../parent::div[contains(@class,'row')]//*[@class='Select-value']";
	public static String GENERIC_DOWNLOAD_BUTTON = "//*[contains(text(),'%s')]/ancestor::div[@class='card__header']/descendant::*[@title='download data as csv']";
	public static String CHECKBOX_INPUT = "//input[@name='%s']/..";
	public static String TABLE_VALUE_CHECK = "//*[@class='table__container ']//*[text()='%s']/../following-sibling::td/span | //*[text()='%s']/following-sibling::td[1]";
	public static String LABEL_TEXT_FIELD = "//label[text()='%s']/../following-sibling::div//input[@type='text']";
	public static String MORE_OPTIONS = "//*[text()='%s']/ancestor::div[@class='card__header']/descendant::i[@class='fl-vert-more fl-dark']";
	public static String CUSTOM_DATE = "//*[@data-visible='true']//*[text()='%s']";
	public static String LABEL_TEXT = "//*[text()='Image ' and text()='%s1']/ancestor::div[contains(@class,'message-container__buttons')]//*[text()='%s2']/../..//*[@type='text']";
	public static String LABEL_CHECKBOX = "//label[text()='%s']/../following-sibling::div//input[@type='checkbox']/..";
	public static String MODULE = "//span[text()='%s']";
	public static String CHECKBOX_VALUE_VERIFY = "//*[contains(@class,'is-checked')]//input[@value='%s']";
	public static String CATALOG_TYPE_VALUE = "//div[@aria-label='%s']";
	public static String OPEN_POP_OVER_MENU = "//*[@title='%s' or text()='%s']/ancestor::tr/descendant::*[@class='pop-over ']//i";
	public static String OPEN_POP_OVER_MENU_UTF = "//*[@title=\"%s\" or text()=\"%s\"]/ancestor::tr/descendant::*[@class='pop-over ']//i";
	public static String GENERIC_LABEL_INPUT = "//label[text()='%s']/../following-sibling::div//input";
	public static String TEXT_CONTAINS_SPAN = "//span[contains(text(),'%s')]";
	public static String TABLE_HEADER_CONTENTS = "//th[text()='%s']";
	public static String INPUT_TEXT_BOX = "//*[text()='%s']/following-sibling::div/input";
	public static String GENERIC_CLASS = "//*[@class='%s']";
	public static String GENERIC_TYPE = "//*[@type='%s']";
	public static String GENERIC_DIVTEXT = "//div[text()='%s']";
	public static String GENERIC_CLASS_CONTAINS = "//*[contains(@class,'%s')]";
	public static String LISTING_COLUMN_VALUE = "//div[@class='table__container ']//*[@title='%s1']/ancestor::tr/td[%s2]";

	By locator;

	CommonPageElements(By locator) {
		this.locator = locator;
	}

	public By locate() {
		return this.locator;
	}

	static By getXpath(String element) {
		return By.xpath(element);
	}

	static By getClassName(String element) {
		return By.className(element);
	}

	static By getId(String element) {
		return By.id(element);
	}
}
