package com.webengage.ui_automation.elements;

import org.openqa.selenium.By;

public enum ShopifyPageElements {
    
    CARDNUMBER_PLACEHOLDER(getXpath("//div[@data-card-field-placeholder='Card number']"));

    public static String DD_OPTIONS = "//*[@name='%s']//option";
    public static String QUANTITY_NUM = "//*[text()=' in cart)']//span[text()='%s']";

    By locator;
    ShopifyPageElements(By locator) {
        this.locator = locator;
    }

    public By locate() {
        return this.locator;
    }
    static By getXpath(String element) {
        return By.xpath(element);
    }

}
