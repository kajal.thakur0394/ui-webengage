package com.webengage.ui_automation.elements;

import org.openqa.selenium.By;

public enum OnsiteNotificationPageElements {

	PERSONALIZATION_TAB_LIST(getXpath("//span[text()='Web Personalization']/ancestor::li")),
	TITLE(getXpath("//input[@name='title']")),
	CREATE_NOTIFICATION_BTN(getXpath("//a[@class='btn-lg btn-primary']")),
	CREATE_OWN(getXpath("//div[contains(@class, 'we-create-header')]/a[1]")),
	CREATE_GALLERY(getXpath("//div[contains(@class, 'we-create-header')]/a[2]")),
	DESC_IFRAME(getXpath("//iframe[@class='redactor_helpTextEnabled']")),
	DESCRIPTION(getXpath("//html//body[@contenteditable='true']")),
	NEW_TAB(getXpath("//div[@id='uniform-atnw']/..")),
	NOTIFICATIONS_LIST(getXpath("//div[contains(@class,'row page-header')]//a[contains(@href,'notifications/all')]")),
	NOTIFICATION_NAME(getXpath("//div[@class='row list-item-header']")),
	ACTUAL_TITLE(getXpath("//div[@class='title-container']/span")),
	ONSITE_NOTIFICATION_CLOSE(getXpath("//*[contains(@class,'we_close')]")),
	ONSITE_NOTIFICATION_IFRAME(getXpath("//iframe[contains(@name,'notification')]")),
	MORE_TAB(getXpath("//i[@class='weicon we_menu']")),
	DELETE_NOTIFICATION(getXpath("//a[contains(@class,'delete')]")),
	DEACTIVATE_NOTIFICATION(getXpath("//a[contains(@class,'change-status active')]")),
	SEARCH_BAR(getXpath("//span[contains(@class,'sb-icon-search')]")),
	NAME_TEXT(getXpath("//div[contains(@class,'list-item-container')][1]//div[contains(@class,'list-item-header-left')]")),
	LAYOUT_TEXT(getXpath("//div[contains(@class,'list-item-container')][1]//*[contains(text(),'Layout')]")),
	ONSITE_JOURNEY_IFRAME(getXpath("//div[contains(@class,'modal--journey')]//following-sibling::iframe")),
	TRAFFIC_SEGMENT_DROPDOWN(getXpath("//option[@value='everyone']/..")),
	SEGMENT_IFRAME(getXpath("//iframe[contains(@id,'fancybox-frame')]")),
	UPLOAD_CENTRE(getXpath("//iframe[@class='fancybox-iframe']"));

	public static String SAVE="//label[text()='%s']";
	public static String PERSONALIZATION_TYPE = "//span[text()='%s']";
	public static String TAB = "//div[@id='%s']/..";
	public static String NEW_TITLE = "//input[@name='%s']";
	public static String LIST_PAGE = "//div[contains(@class,'row page-header')]//a[contains(@href,'%s/all')]";
	public static String ROW_ELEMENTS = "//*[text()='%s1']/ancestor::div[contains(@id,'row')]//%s2";
	public static String ONSITE_IFRAME_LAYOUTID = "//iframe[@data-notification-layout-id='%s']";
	public static String ONSITE_IFRAME_LAYOUTNAME = "//iframe[@data-notification-layout-name='%s']";
	public static String NOTIFICATION_CAMPAIGN_TAG_NAME = "//div[contains(text(),'%s')]/ancestor::div[contains(@class,'list-item-container')]//label[@class='tag']";
	public static String NOTIFICATION_CAMPAIGN_NEW_TAG = "//div[contains(text(),'%s')]/ancestor::div[contains(@class,'list-item-container')]//a[text()='Add Tag']";
	public static String NOTIFICATION_CAMPAIGN_ADD_NEW_TAG = "//div[contains(text(),'%s')]/ancestor::div[contains(@class,'list-item-container')]//input[@placeholder='Press enter to save']";
	public static String NOTIFICATION_CAMPAIGN_MORE_BTN = "//div[contains(text(),'%s')]/ancestor::div[contains(@class,'list-item-container')]//div[contains(@class,'row-more last')]";
	public static String GALLERY_TEMPLATE = "//div[contains(text(),'%s')]/../../../div/button[contains(text(),'Use This')]";
	public static String ONSITE_CAMPAIGN_STATUS = "//div[contains(text(),'%s')]/ancestor::div[contains(@class,'list-item-container')]//div[contains(@class,'notification-status')]";
	public static String ONSITE_CAMPAIGN_STATUS_VALUE = "//div[contains(text(),'%s')]/ancestor::div[contains(@class,'list-item-container')]//div[contains(@class,'notification-status')]/i";
	public static String ONSITE_CAMPAIGN_EDIT_BTN = "//div[contains(text(),'%s')]/ancestor::div[contains(@class,'list-item-container')]//i[@class='weicon we_edit']";
	public static String ONSITE_CAMPAIGN_COPY_BTN = "//div[contains(text(),'%s')]/ancestor::div[contains(@class,'list-item-container')]//a[@class='notification-copy']";
	By locator;

	OnsiteNotificationPageElements(By locator) {
		this.locator = locator;
	}

	public By locate() {
		return this.locator;
	}

	static By getXpath(String element) {
		return By.xpath(element);
	}

	static By getClassName(String element) {
		return By.className(element);
	}
}
