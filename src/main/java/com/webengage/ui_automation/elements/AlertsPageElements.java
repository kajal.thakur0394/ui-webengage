package com.webengage.ui_automation.elements;

import org.openqa.selenium.By;

public enum AlertsPageElements {

	ADVANCED_DROPDOWN(getXpath("(//*[@class='carat down'])[2]")), 
	ALERT_LIST(getXpath("//*[contains(@class,'overflow-hidden')]/preceding::*[contains(@class,'justify-content-between')]"));

	public static String RELATIVE_DROPDOWN = "//*[text()='%s']/../..//*[@class='Select-arrow']";
	public static String ALERT_DROPDOWN = "//*[text()='%s']/ancestor::*[@class='row']//*[text()='-']";
	public static String FILTER_ELEMENTS = "//*[@class='filter']//*[text()='%s']";
	public static String FILTER_ATTRIBUTE_DROPDOWN = "(//*[@class='filter']//*[@class='Select-arrow'])[%s]";
	public static String FILTER_COUNT = "//*[@data-count='%s']";
	public static String GET_SHOW_DETAILS_VALUE = "//*[contains(@class,'modal')]//*[text()='%s']/../span";
	By locator;

	AlertsPageElements(By locator) {
		this.locator = locator;
	}

	public By locate() {
		return this.locator;
	}

	static By getXpath(String element) {
		return By.xpath(element);
	}

	static By getName(String element) {
		return By.name(element);
	}

	static By getId(String element) {
		return By.id(element);
	}

	static By getCSS(String element) {
		return By.xpath(element);
	}

	static By getClassName(String element) {
		return By.className(element);
	}
}
