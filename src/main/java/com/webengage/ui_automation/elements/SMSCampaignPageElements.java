package com.webengage.ui_automation.elements;

import org.openqa.selenium.By;

public enum SMSCampaignPageElements {
	
	SEARCH_INPUT_BAR(getXpath("//div[@class='Select-input']/input")),
	SEARCH_INPUT_BAR_VALUE(getClassName("Select-value")),
	CLEAR_SEARCH(getClassName("Select-clear")),
	TABLE_HEADER(getClassName("table__header")),
	CAMPAIGN_NAME_TEXTFIELD(getXpath("//input[contains(@class,'field--string')]")),
	EXCLUDE_SEGMENT_CREATION_ICON(getXpath("//label[text()='EXCLUDE USERS FROM THESE SEGMENTS']/parent::div/following-sibling::div//span[@data-tooltip='Create New Segment']")),
	VERIFY_SEGMENT_NAME(getXpath("//label[text()='AUDIENCE: ']/following-sibling::a")),
	POP_OVER_MENU(getXpath("//*[contains(@class,'table__container-fixed')]//tr[@class='table__row']//td//div[@class='pop-over__head']")),
	EVENT_DROPDOWN(getXpath("//*[@aria-label='open menu'][not(@disabled)]/..")),
	LATER(getXpath("//input[@value='later']/parent::label")),
	TILL(getXpath("//input[@value='till']/parent::label")),
	TD_LIST(getXpath("//div[@data-visible='true']//td[@class='CalendarDay CalendarDay--valid']")),
	NEXT_MONTH(getXpath("//button[@aria-label='Move forward to switch to the next month']")),
	PREVIEWPAGE(getXpath("//span[text()='Preview & Launch']")),
	ACTUAL_DELIVERY_TIME(getXpath("//label[text()='Delivery Time:']/parent::div")),
	STARTNOW_DATE(getXpath("//p[@id='DateInput__screen-reader-message-date']/following-sibling::div")),
	CAMPAIGN_STATUS(getXpath("//tr[@class='table__row']/td[3]/span")),
	CAMPAIGN_START_DATE(getXpath("//tr[@class='table__row']/td[4]")),
	SHOW_DETAILS(getXpath("//a[@class='details']")),
	CONVERSION_ON(getXpath("//div[@class='react-toggle']//following-sibling::span[2]")),
	PERSONALIZATION_TOGGLE(getXpath("//*[contains(@class,'react-toggle')]")),
	CONVERSION_DEADLINE_FIELD(getXpath("//input[@value='7']")),
	CONVERSION_DEADLINE_TIME(getXpath("//input[@value='d']/parent::div")),
	CAMP_STATUS(getXpath("//span[contains(@class,'status-label')]")),
	SCHEDULING(getXpath("//input[@name='web-per-schedule' and @value='true']/parent::label")),
	DELIVERY_SCHEDULE_ON(getXpath("//div[contains(@class,'col-4')]//div[contains(@class,'Select-value')]")),
	SENDER_PREVIEW(getXpath("//div[@class='sms-preview-header']//div")),
	HOUR_SELECTION(getXpath("//*[text()='END DATE']/../..//*[@name='react-select-hours']/..//span[@class='Select-value-label']")),
	AMPM_SELECTION(getXpath("//*[text()='END DATE']/../..//*[@name='react-select-wrapper']/..//span[@class='Select-value-label']")),
	MESSAGE_PREVIEW(getXpath("//div[@class='sms-preview-message']"));
	
	public static String  POP_OVER_MENU2="//*[contains(@class,'table__container-fixed')]//a[@title='%s']/../following-sibling::*//div[@class='pop-over__head']";
	public static String CAMPAIGNTYPE_TEXT="//tr[@class='table__row']/td[text()='%s']";
	public static String CAMPAIGN_TYPE = "//li/span[text()='%s']";
	public static String BUTTON = "//button[text()='%s']";
	public static String SMS_CAMPAIGN_NAME = "//a[@title='%s']";
	public static String NOTIFICATION_TOAST = "//*[text()='%s']";
	public static String AUDIENCE_OR_CONTENT_TYPE = "//label[contains(text(),'%s')]";
	public static String DATEYPE="//input[@value='%s']/parent::label";
	public static String DATEPICKER = "//label[text()='%s']/parent::div/following-sibling::div//*[@class='SingleDatePicker']";
	public static String DELIVERY_DATE="//div[@class='Select-menu']//div[@aria-label='%s']";
	public static String DATE="//label[text()='%s']/parent::div/following-sibling::div//*[@class='SingleDatePicker']//input[@placeholder='Select Date']";
	public static String ANOTHER_DATE="//label[text()='%s']/parent::div/following-sibling::div//*[@class='SingleDatePicker']//p[@id='DateInput__screen-reader-message-date']/following-sibling::div";
	public static String HOUR="//label[text()='%s']/parent::div/following-sibling::div//input[@name='react-select-hours']";
	public static String MINUTES="//label[text()='%s']/parent::div/following-sibling::div//input[@name='react-select-minutes']";
	public static String WRAPPER="//label[text()='%s']/parent::div/following-sibling::div//input[@name='react-select-wrapper']";
	public static String CAMPAIGNNAMELINK="a[@title='%s']";
	public static String START_OR_END_DATES="//label[text()='%s']//following-sibling::span";
	public static String DETAILS_START_OR_END_DATES="//label[contains(text(),'%s')]//following-sibling::span";
	public static String SELECT_CONVERSION_DEADLINE="//div[@aria-label='%s']";
	public static String SELECT_DATE_OR_DAY="//span[normalize-space()='%s']";
	public static String MONTH_SCHEDULE_DROPDOWN = "//div[@class='Select-menu-outer']//div[normalize-space()='%s']";
	
	
	By locator;

	SMSCampaignPageElements(By locator) {
		this.locator = locator;
	}

	public By locate() {
		return this.locator;
	}

	static By getXpath(String element) {
		return By.xpath(element);
	}

	static By getClassName(String element) {
		return By.className(element);
	}

}
