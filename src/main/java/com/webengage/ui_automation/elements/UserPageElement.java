package com.webengage.ui_automation.elements;

import org.openqa.selenium.By;

public enum UserPageElement {

	REACHABILITY_HEADER(getXpath("//thead/tr/th")),
	USER_EVENT_LIST(getXpath("//div[@class='engagement-block accordion']")),
	TOTALUSER_BY_COUNTRY(getXpath("//td[@class='table__cell table__cell--small table__cell--footer table__cell--content-number']")),
	EVENT_PAGE(getXpath("//div[contains(@class,'card__content user-profile-blocks')]//*[text()='No data available'] |  //div[contains(@class,'card__content user-profile-blocks')]//*[@class='engagement-block accordion']"));
	
	public static String CLICK_USER = "//*[@class='table__container table__container-fixed  undefined']//*[text()='%s']";
	public static String LABEL_VALUE = "//label[contains(text(),'%s')]/following-sibling::span";
	public static String REACHABILITY_VALUE = "//tbody/tr/td[%s]/span";
	By locator;

	UserPageElement(By locator) {
		this.locator = locator;
	}

	public By locate() {
		return this.locator;
	}

	static By getXpath(String element) {
		return By.xpath(element);
	}

	static By getName(String element) {
		return By.name(element);
	}

	static By getId(String element) {
		return By.id(element);
	}

	static By getCSS(String element) {
		return By.xpath(element);
	}

	static By getClassName(String element) {
		return By.className(element);
	}

}
