package com.webengage.ui_automation.elements;

import org.openqa.selenium.By;

public enum OnsiteSurveysPageElements {
	QUESTIONS_LINK(getXpath("//div[contains(@class,'add-questions')]")),
	QUESTIONS_IFRAME(getXpath("//iframe[contains(@id,'fancybox-frame')]")),
	HEADER_IFRAME(getXpath("//iframe[contains(@class,'editor')]")),
	TEXT_QUESTION(getXpath("//textarea")),
	SUBMIT_BTN(getXpath("//button[contains(@class,'submitQuestion')]")),
	DATE_INPUT_SELECT(getXpath("//div[@id='ui-datepicker-div']")),
	START_DATE(getXpath("//input[@id='startOn']")),
	END_DATE(getXpath("//input[@id='endOn']")),
	DATE(getXpath("//table[@class='ui-datepicker-calendar']//tr/td[contains(@class,'ui-datepicker-current-day')]")),
	CONFIRM_DATE(getXpath("//button[text()='Done']")),
	SURVEY_IFRAME(getXpath("//iframe[contains(@title,'survey-frame')]")),
	SURVEY_QUESTION(getXpath("//*[@class='question-label']")),
	WELCOME_MESSAGE(getXpath("//*[@class='form-header-message']"));
	
	public static String QUESTION_TYPE="//label[text()='%s']/..";
	public static String CHOICE_OF_OPTIONS="//*[@name='optionsForm.optionRows[%s].newValue']";
	public static String GET_OFFSITE_URL="//*[contains(@id,'%s')]//*[contains(@class,'survey-url')]";
	
	By locator;

	OnsiteSurveysPageElements(By locator) {
		this.locator = locator;
	}

	public By locate() {
		return this.locator;
	}

	static By getXpath(String element) {
		return By.xpath(element);
	}

	static By getClassName(String element) {
		return By.className(element);
	}
}
