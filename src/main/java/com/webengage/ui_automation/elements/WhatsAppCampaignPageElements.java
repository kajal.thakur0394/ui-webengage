package com.webengage.ui_automation.elements;

import org.openqa.selenium.By;

public enum WhatsAppCampaignPageElements {
    SEARCH_TEMPLATE(getXpath("//input[@placeholder='Search by template name']")),
    INPUT_VARIABLES(getXpath("//*[contains(text(),'{{') and contains(text(),'}}')]/following-sibling::div//*[@placeholder='Value']")),
    PREVIEW_MESSAGE(getXpath("//div[@class='whatsapp-preview-body']//p"));

    public static String VARIABLE_TEXTBOX = "//div[contains(text(),'%s')]/parent::div//input";
    public static String SELECT_WA_TEMPLATE = "//*[text()='%s1']/../parent::tr//*[text()='%s2']/..";
    public static String VIDEO = "//label[text()='%s']/../following-sibling::div//input[@placeholder='https://www.abc.com/video.mp4']";

    By locator;
    WhatsAppCampaignPageElements(By locator) {
        this.locator = locator;
    }

    public By locate() {
        return this.locator;
    }
    static By getXpath(String element) {
        return By.xpath(element);
    }

}
