package com.webengage.ui_automation.elements;

import org.openqa.selenium.By;

public enum PushCampaignPageElements {
	BANNER_IMAGE(getXpath("//label[text()='Image']/../following-sibling::div//input")),
	ANDROID_ADV_OPTION(getXpath("//h5[text()='(Android)']/following-sibling::div//div[@class='react-toggle']")),
	IOS_ADV_OPTION(getXpath("//h5[text()='(iOS)']/following-sibling::div//div[@class='react-toggle']")),
	ADV_OPTION_TOGGLE(getXpath("//h5[text()='Advanced Options ']/following-sibling::div//div[@class='react-toggle']")),
	ANDROID_ONCLICK_URL(getXpath("//*[text()='On-click Action (Android)']/../..//input")),
	IOS_ONCLICK_URL(getXpath("//*[text()='On-click Action (iOS)']/../..//input")),
	RATING_DESCRIPTION(
			getXpath("//*[text()='Rating']/ancestor::*[@class='card']//*[text()='Description']/../..//textarea")),
	RATING_TITLE(getXpath(
			"//h5[text()='Rating']/../../../following-sibling::div//label[text()='Title']/../following-sibling::div//input")),
	BUTTONS(getXpath("//label[text()='Buttons']/../following-sibling::div//div[@class='react-toggle']")),
	OPEN_DD(getXpath("//div[@class='Select-placeholder']")),
	TITLE_IFRAME(getId("push-msg-title_ifr")),
	DESCRIPTION_IFRAME(getId("push-msg-description_ifr")),
	TITLE_IFRAME_BUTTON(getId("push-msg-android-cta-0_ifr")),
	TITLE_SECTION_BUTTON(getXpath("//*[@data-id='push-msg-android-cta-0']")),
	TITLE_SECTION(getXpath("//*[@data-id='push-msg-title']")),
	DESCRIPTION_SECTION(getXpath("//*[@data-id='push-msg-description']")),
	MESSAGE_SUMMARY_SECTION(getXpath("//*[@data-id='push-msg-android-summary']")),
	ATTRIBUTE_TYPE(getXpath("//li[@title='User Attributes']")),
	SELECT_ATTRIBUTE(getXpath("//li[@title='First Name']")),
	ANDROID_DESCRIPTION_IFRAME(getId("push-msg-android-summary_ifr")),
	ANDROID_DESCRIPTION(getXpath("//*[@data-id='push-msg-android-summary']")),
	IOS_DESCRIPTION_IFRAME(getId("push-msg-ios-subtext_ifr")),
	IOS_DESCRIPTION(getXpath("//*[@data-id='push-msg-ios-subtext']")),
	EMOJI(getXpath("//div[@data-name='Smileys & People']/following-sibling::span[1]")),
	ADV_OPTION_TOGGLE_IFRAME(getId("push-msg-android-summary_ifr")),
	BUTTONS_LABEL(getXpath("//*[text()='Label']/parent::div/following-sibling::div//input")),
	ONCLICK_INPUT(getXpath("//*[text()='On-Click Action']/../..//input")),
	PUSH_TITLE_RAWPREVIEW(getXpath("//div[contains(@class,'body-heading-title')]")),
	PUSH_DESC_RAWPREVIEW(getXpath("//div[contains(@class,'body-heading-description')]")),
	PUSH_MSG_SUMMARY_RAWPREVIEW(getXpath("//div[contains(@class,'text-ellipsis')]")),
	PUSH_SUBTITLE_RAWPREVIEW(getXpath("(//p[contains(@class,'__text--title')])[2]")),
	OS_DROPDOWN_RAWPREVIEW(getXpath("//div[contains(@class,'Select field--type')]")),
	SELECT_OS_RAWPREVIEW(getXpath("//div[@aria-label='iOS']")),
	PREVIEW_TEXT(getXpath("//div[contains(@class,'description')]"));
	

	public static String EMOJI_LINK = "//label[text()='%s']/parent::div//following-sibling::div//div[@id='emoji-picker']/i";
	public static String CLOSE_EMOJI_LINK = "(//label[text()='Label']/parent::div//following-sibling::div//div[@id='emoji-picker']/i)[2]";
	public static String DEVICE = "//input[@value='%s']/..";
	public static String TEMPLATE = "//span[text()='%s']/../..";
	public static String TITLE_INPUT = "//label[text()='%s']/../following-sibling::div//input";
	public static String IOS_BTN_CLICK = "//h6[text()='Button 1']/../../following-sibling::div[2]//label[text()='%s']/../following-sibling::div//input";
	public static String SELECT_DD = "//div[@class='Select-menu-outer']//div[@aria-label='%s']";
	public static String SELECT_PERSONALIZATION = "//label[text()='%s']/../following-sibling::div//div[@id='email-p']";
	public static String TEXTAREA_EMOJI = "//*[contains(text(),'%s')]/ancestor::div[contains(@class,'form__lhs')]//following-sibling::div//child::div[@id='emoji-picker']/i";
	
	By locator;

	PushCampaignPageElements(By locator) {
		this.locator = locator;
	}

	public By locate() {
		return this.locator;
	}

	static By getXpath(String element) {
		return By.xpath(element);
	}

	static By getClassName(String element) {
		return By.className(element);
	}

	static By getId(String element) {
		return By.id(element);
	}
}
