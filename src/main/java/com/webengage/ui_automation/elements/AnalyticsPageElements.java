package com.webengage.ui_automation.elements;

import org.openqa.selenium.By;

public enum AnalyticsPageElements {
	INPUT_FUNNEL(getXpath("//input[@type='text']")),
	SELECT_ADD_TAB(getXpath("//*[label='OF']//*[text()='All']")),
	DROPDOWN_ARROW(getXpath("//span[text()='Last 7 days']/following-sibling::i")),
	X_AXIS_DATE(getXpath("//div[@class='highcharts-axis-labels highcharts-xaxis-labels ']/span")),
	CHART_BACKGROUND(getXpath("//*[@class='highcharts-background']")),
	FILTER_EXPANSION(getXpath("//*[contains(@class,'accordion-title')]")),
	CUSTOM_ALERT(getXpath("//div[@class='card__header']//*[contains(@class,'custom-alerts')]")),
	FILTER_ICON(getXpath("(//i[contains(@class,'fl-filter')])[last()]")),
	BAR_ELEMENTS(getXpath("//*[contains(@class,'highcharts-series')]/*[contains(@class,'highcharts-point')]/..")),
	LINE_BAR_POINTS(getXpath("//*[contains(@class,'highcharts-point')]")),
	TOOLTIP_TEXT(getXpath("//*[contains(@class,'highcharts-tooltip') and contains(@style,'visible')]")),
	TOOLTIP_TEXT_OVERBY(getXpath("//*[contains(text(),'●')]/parent::span")),
	EVENT_DROPDOWN(getXpath("//*[contains(text(),'OF')]/../div")),
	CHECKBOX(getXpath("//*[contains(@class,'is-checked')]//span")),
	SPLITBY_DROPDOWN(getXpath("//*[text()='SPLIT BY']/parent::div//*[contains(text(),'-')]")),
	TIMEPERIOD_RADIO_BUTTON(getXpath("//*[text()='Last']/..//*[contains(@class,'radio')]")),
	LAST_INPUT_BOX(getXpath("//*[text()='Last']//*[@type='text']")),
	SEARCH_EVENT(getXpath("//input[@placeholder='Search for Events']")),
	DATA_POINTS_CHECKBOX_SELECTED(
			getXpath("//tr[contains(@style,'height')]//*[contains(@class,'checkbox__label--is-checked')]")),
	DATA_POINTS_CHECKBOX(getXpath("//tr[contains(@style,'height')]//*[contains(@class,'checkbox__label')]")),
	DATA_POINT_POP_OVER(getXpath("//span[@class='align-items-center ']//div[@class='pop-over__head']")),;

	public static String SELECT_DD = "//label[text()='%s']/following-sibling::div";
	public static String PATH_STEP = "//*[contains(@class,'highcharts-point') and contains(@class,'%s1__*we*__%s2')]";
	public static String SELECT_FUNNEL_EVENT = "//div[@class='r-ss-dropdown-options']//div[@title='%s']";
	public static String FUNNEL_STEP_DD = "//label[normalize-space(.)='%s']/parent::div/following-sibling::div//div[@role='combobox']";
	public static String FUNNEL_LIST_UTF = "//*[contains(@class,'table__container')]//tbody/tr//a[text()=\"%s\"]";
	public static String TABLE_BODY_ELEMENT = "(//*[@class='table__container ']//tbody//td[%s1])[%s2]";
	public static String DAY_TYPE_ELEMENTS = "//*[@class='table__container ']//tbody//td[%s]";
	public static String FILTER_DROPDOWN_COL = "//*[text()='%s1']/../../following-sibling::div//*[text()='%s2']";
	public static String SEND_TEXT = "(//*[text()='%s']/../../following-sibling::div//input)[last()]";
	public static String FUNNEL_LIST = "//*[contains(@class,'table__container')]//tbody/tr//a[text()='%s']";
	public static String EVENT_COUNT = "//*[@class='table__container ']//*[text()='%s']/../following-sibling::td";
	public static String PATH_STEP_EVENT = "//*[contains(text(),'%s')]//following-sibling::div//span[text()='100%']";

	By locator;

	AnalyticsPageElements(By locator) {
		this.locator = locator;
	}

	public By locate() {
		return this.locator;
	}

	static By getXpath(String element) {
		return By.xpath(element);
	}

	static By getName(String element) {
		return By.name(element);
	}

	static By getId(String element) {
		return By.id(element);
	}

	static By getCSS(String element) {
		return By.xpath(element);
	}

	static By getClassName(String element) {
		return By.className(element);
	}
}