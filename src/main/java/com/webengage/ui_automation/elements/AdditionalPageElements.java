package com.webengage.ui_automation.elements;

import org.openqa.selenium.By;

public enum AdditionalPageElements {

	SUMMARY_LABEL1(getXpath("//p[contains(@class,'card__title')]/..//label")),
	SUMMARY_ALL_LABELS(getXpath("//label[contains(@class,'card__title')]")),
	SUMMARY_VALUE_COUNT1(getXpath("//label[contains(@class,'card__title')]/parent::div/following-sibling::p")),
	SUMMARY_ALL_SUBTITLES(getXpath("//div[contains(@class,'card__subtitles')]//label")),
	DATE(getXpath("//div[contains(@class,'smart-date')]/div")),
	DATE_LIST(getXpath("//li[contains(@class,'nav-list')]/span")),
	VB1(getXpath("//div[@id='rss_1']")),
	VB2(getXpath("//div[@id='rss_2']")),
	VB1_DOWN(getXpath("//ul[@id='rss_1_list']//span")),
	VB2_DOWN(getXpath("//ul[@id='rss_2_list']//span")),
	COLUMN_LABEL(getXpath("//div[@class='col-4']//div[contains(@class,'form__element')]/label")),
	FILTER(getXpath("//i[contains(@class,'fl-filter')]")),
	TABLE_SHORT_COL(getXpath("//div[@class='container']/div[1]//tr[@class='table__row']/th")),
	TABLE_ROWS(getXpath("//div[@class='container']/div[1]//div[contains(@class,'container-fixed')]/preceding-sibling::div//tbody/tr")),
	CLEAR_SELECTION(getXpath("//button[@aria-label='Clear Selection']")),
	VIEWAS_MENU(getXpath("//div[@class='row align-items-center']//div[@class='pop-over__head']")),
	COUNT_VIEWAS(getXpath("//label[text()='#']")),
	DATATABLE_MENU(getXpath("//div[@class='container']/div[@class='card']//div[contains(@class,'content-end')]//div[@class='pop-over__head']")),
	TABLE_OPTION(getXpath("//label[text()='Table']")),
	DATATABLE_VB1_GETLIST(getXpath("//div[@class='container']/div[@class='card']//div[@class='pop-over__head']")),
	DATATABLE_VB1_LIST(getXpath("//div[@id='popover_body']//li/span")),
	DATATABLE_HEADERS(getXpath("//div[@class='container']/div[@class='card'][1]//table//th")),
	DATATABLE_ROWS(getXpath("//div[@class='container']/div[@class='card']//div[contains(@class,'container-fixed')]/preceding-sibling::div//tbody//tr")),
	PAGINATION_NEXT(getXpath("//li[@class='pagination__next']")),
	CAMPAIGN_NAME(getXpath("//div[@class='align-items-center']/h4")),
	CHANNEL_SELECT(getXpath("//a[contains(@class,'is-active')]//span")),
	NAME_LINK(getXpath("//div[contains(@class,'container-fixed')]//tbody/tr[@class='table__row']//a")),
	CAMPAIGNLIST_HEADER(getXpath("//div[contains(@class,'container-fixed')]/preceding-sibling::div//th")),
	PAGINATION_PREV(getXpath("//li[@class='pagination__previous']"));
	
	public static String NAME="//div[contains(@class,'container-fixed')]/preceding-sibling::div//tbody//tr[1]/td//a";
	public static String CAMPAIGNLIST_ROWDATA="//div[contains(@class,'container-fixed')]/preceding-sibling::div//tbody//tr[%s]/td";
	public static String DATATABLE_TDS="//div[@class='container']/div[@class='card']//div[contains(@class,'container-fixed')]/preceding-sibling::div//tbody//tr[%s]/td";
	public static String VB1ALTERNATETEXT="//div[contains(@class,'container-fixed')]/preceding-sibling::div//tbody//tr[%s]/td[1]/span";
	public static String SUMMARY_SUBTITLE_VALUE = "//label[text()='%s']/following-sibling::span";
	public static String SUMMARY_ALL_VALUE = "//label[text()='%s']/parent::div/following-sibling::h5";
	public static String ROW_TD = "//div[contains(@class,'container-fixed')]/preceding-sibling::div//tbody//tr[%s]/td";
	public static String CURSOR_POINTER = "//div[contains(@class,'container-fixed')]/preceding-sibling::div//tbody//tr[%s]/td[1]//span[@class='cursor-pointer']";
	public static String VB1CURSORTEXT = "//div[contains(@class,'container-fixed')]/preceding-sibling::div//tbody//tr[%s]/td[1]//span[@class='handle-text-overflow']/span";
	public static String VB1TEXT = "//div[contains(@class,'container-fixed')]/preceding-sibling::div//tbody//tr[%s]/td[1]/span/span[1]";
	public static String VB2TEXT = "//div[contains(@class,'container-fixed')]/preceding-sibling::div//tbody//tr[%s]/td[1]//span[contains(@class,'padding-left-xxl')]";
	By locator;

	AdditionalPageElements(By locator) {
		this.locator = locator;
	}

	public By locate() {
		return this.locator;
	}

	static By getXpath(String element) {
		return By.xpath(element);
	}

	static By getName(String element) {
		return By.name(element);
	}

	static By getId(String element) {
		return By.id(element);
	}

	static By getCSS(String element) {
		return By.xpath(element);
	}

	static By getClassName(String element) {
		return By.className(element);
	}
}
