package com.webengage.ui_automation.elements;

import org.openqa.selenium.By;

public enum InAppCampaignPageElements {
	DESCRIPTION_SECTION(getXpath("//*[text()='Description']/../following-sibling::*//*[contains(@class,'field field--textarea  ')]")),
	ICON_LINK(getXpath("//*[text()='Icon']/../following-sibling::*//input[@type='text']")),
	ICON_UPLOAD(getXpath("//*[text()='Icon']/../following-sibling::*//*[text()='Upload']")),
	BACKGROUND_LINK(getXpath("//*[text()='Background Image']/../following-sibling::*//input[@type='text']")),
	CHANGELAYOUT_MESSAGE(getXpath("//div[.='Change Template']/following-sibling::div//div")),
	TOOLTIP_ERROR(getXpath("//span[@class='tooltip-bottom']")),
	CHANGELAYOUT_TOOLTIP(getXpath("//*[@data-tooltip='Change Layout']")),
	PRIMARY_BUTTON(getXpath("//*[text()='Primary Button']/following-sibling::*//*[contains(@class,'react-toggle ')]")),
	PRIMARY_BTN_LABEL(getXpath("//*[text()='Primary Button']/../../following-sibling::div//label[text()='Label']/../following-sibling::div//input")),
	CLOSE_BTN_LABEL(getXpath("//*[text()='Close Button']/../following-sibling::div//label[text()='Label']/../following-sibling::div//input")),
	POPOUT_BTN_LABEL(getXpath("//label[text()='Label']/../following-sibling::*//input")),
	POPOUT_IMG(getXpath("//label[text()='Image']/../following-sibling::div//input[@type='text']")),
	POPOUT_TITLE(getXpath("//label[text()='Title']/../following-sibling::*//input")),
	HEADER_DESC_RAWPREVIEW(getXpath("//div[@class='description']")),
	RAW_PREVIEW_IFRAME(getId("we-in-app-preview-frame"));
    
    public static String ONCLICK_ANDROID_ACTION="//*[text()='%s']/parent::div/..//following-sibling::*//*[text()='On-click Action (Android)']/../following-sibling::div//input";
    public static String LABEL="//*[text()='%s1']/following::*[.='%s2']/following-sibling::*//input";
	public static String RADIO_BUTTON="//div[.='%s1']/following-sibling::div//*[text()='%s2']";
	public static String MESSAGE_BUTTON="//*[.='%s']/following-sibling::div//div[contains(@class,'toggle')]";
	public static String INPUT_VALUE="//div[.='%s1']/following-sibling::div//*[text()='%s2']//input | //div[.='%s1']/following-sibling::div//*[text()='%s2']/preceding-sibling::input";
	public static String DEVICE_ON_CLICK="(//*[text()='Primary Button']/following::*[.='%s']/following-sibling::*//input)[1]";
	
	By locator;
	
	InAppCampaignPageElements(By locator) {
		this.locator = locator;
	}

	public By locate() {
		return this.locator;  
	}

	static By getXpath(String element) {
		return By.xpath(element);
	}

	static By getClassName(String element) {
		return By.className(element);
	}
	
	static By getId(String element) {
		return By.id(element);
	}
}
