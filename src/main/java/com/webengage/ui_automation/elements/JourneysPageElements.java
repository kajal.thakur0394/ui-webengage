package com.webengage.ui_automation.elements;

import org.openqa.selenium.By;

public enum JourneysPageElements {

	CREATE_BTN(getXpath("//a[contains(@href,'create')]")),
	JOURNEY_SIDE_PANEL(getId("joint-stencil")),
	DROP_TO(getXpath("//*[@class='j-working-area theme1 ']")),
	ALIGN_VERTICALLY(getXpath("//span[@data-tooltip='Auto Align Vertically']")),
	BTN_YES(getXpath("//button[text()='Yes']")),
	BTN_NO(getXpath("//button[text()='No']")),
	SAVE_DRAFT(getXpath("//*[text()='Save Draft']")),
	ZOOM_OUT(getXpath("//*[@title='Zoom out']")),
	SEARCH_CONTROLS(getClassName("search")),
	SELECT_VALUE(getXpath(
			"//*[@class='Select-value'] | //*[@class='Select-placeholder'] | //*[@class='Select-value-label']")),
	DOWNSHIFT_DD_OPEN(getXpath("//div[contains(@aria-labelledby,'downshift')]")),
	DOWNSHIFT_DD_ENABLE(getXpath("//*[contains(@class,'r-ss-trigger')]")),
	CLOSE_BTN(getXpath("//*[contains(@class,'fl-close')]")),
	INPUT_NUMBER(getXpath("//*[@type='number']")),
	NAVIGATE_BACK(getXpath("//*[contains(@class,'fl-back')]")),
	JOURNEY_NAME(getId("we-journey-name")),
	SELECT_DAY(getXpath("//*[contains(@class,'col-2')]//*[@class='Select-value-label']")),
	TEXTAREA(getXpath("//textarea[contains(@class,'user')]")),
	TOGGLE(getClassName("react-toggle")),
	CONVERSION_TRACKING_BTN(getXpath("//*[text()='Set Conversion Tracking']/..")),
	EXIT_TRIGGER_BTN(getXpath("//*[contains(@class,'exit-trigger')]")),
	EXIT_TRIGGER_DD(getXpath("//*[text()='End journey when there is ']/..//*[@class='Select-control']")),
	EXIT_CRITERIA(getClassName("event-filter-list-item")),
	SANPSHOT_BTN(getXpath("//button[@title='Download an image of the entire canvas']")),
	JOURNEY_DETAILS(getClassName("details")),
	SEARCH_BY_JOURNEY(getXpath("//*[text()='Search by journey name']")),
	DROPDOWN_TOGGLE_ICON(getClassName("dropdown-toggle-icon")),
	MONTH_YEAR(getXpath("//*[contains(@class,'CalendarMonth') and @data-visible='true']//strong")),
	START_DATE(getXpath("//*[text()='Start DATE']/../following-sibling::div//*[@class='SingleDatePickerInput']")),
	END_DATE(getXpath("//*[text()='END DATE']/../../following-sibling::div//*[@class='SingleDatePickerInput']")),
	START_INPUT_DATE(getXpath("//*[text()='Start DATE']/../following-sibling::div//p[contains(@id,'DateInput')]/following-sibling::div")),
	START_INPUT_HOURS(getXpath("//*[text()='Start DATE']/../following-sibling::div//*[@name='react-select-hours']/..")),
	START_INPUT_MINUTE(getXpath("//*[text()='Start DATE']/../following-sibling::div//*[@name='react-select-minutes']/..")),
	START_INPUT_AM_PM(getXpath("//*[text()='Start DATE']/../following-sibling::div//*[@name='react-select-wrapper']/..")),
	END_INPUT_DATE(getXpath("//*[text()='END DATE']/../../following-sibling::div//p[contains(@id,'DateInput')]/following-sibling::div")),
	END_INPUT_HOURS(getXpath("//*[text()='END DATE']/../../following-sibling::div//*[@name='react-select-hours']/..")),
	END_INPUT_MINUTE(getXpath("//*[text()='END DATE']/../../following-sibling::div//*[@name='react-select-minutes']/..")),
	END_INPUT_AM_PM(getXpath("//*[text()='END DATE']/../../following-sibling::div//*[@name='react-select-wrapper']/..")),
	NEXT_MONTH(getXpath("//button[@aria-label='Move forward to switch to the next month']")),
	SCHEDULE_BTN(getXpath("//*[text()='Schedule']/following-sibling::button")),
	STATUS(getXpath("//*[contains(@class,'status-label')]")),
	AUDIT_LOG_VIEW(getXpath("//label[contains(text(),'AUDIT LOG')]/following-sibling::span//*[text()='View']")),
	AUDIT_LOG_ROWS(getXpath("//*[contains(@class,'engagement-block-title cursor')]")),
	LOG_DATE(getXpath("//*[contains(@class,'date')]")),
	LOG_NAME(getXpath("//*[contains(@class,'name')]")),
	SPECIFIC_USER_TRIGGER(getXpath("//label[@class='radio__label radio__label--is-checked']")),
	SELECT_FILE_BUTTON(getXpath("//*[@type='file']")),
	JOURNEY_BLACKOUT(getClassName("blackout")),
	CAMPAIGNNAMES(getXpath("//div[contains(@class,'table__container-fixed')]//tbody//tr//a")),
	JOURNEY_SETTINGS(getXpath("//i[contains(@class,'fl-cog fl-dark')]")),
	JOURNEY_NAME_OVERVIEW(getXpath("//header[@class='subhead']//h4"));
	
	public static String USER_OPEN_DD = "//*[text()='%s']/../following-sibling::div/descendant::div[@class='Select-control']";
	public static String LOCATE_BLOCK = "//div[@id='paper']//*[@id='%s' or @model-id='%s']";
	public static String GENERIC_VALUE = "//*[@value='%s']";
	public static String GENERIC_TITLE = "//*[@title='%s']";
	public static String SVG = "//*[@id='%s']/descendant::*[@class='icon'][2]";
	public static String BODY_TEXT = "//*[@id='%s']/descendant::*[@class='body-text']";
	public static String HOVER_OVER = "//*[@id='%s1' or @model-id='%s1']/descendant::*[@port='%s2']";
	public static String HOVER_OVER_CONTAINS = "//*[@id='%s1']/descendant::*[contains(@port,'%s2')]";
	public static String OPEN_BLOCK = "//*[@model-id='%s']/descendant::*[@class='v-line'][3]";
	public static String SELECT_OPTION = "//*[@aria-label='%s' or @title='%s']";
	public static String SELECT_FROM_EVENT_DD = "//*[@aria-label='%s1']/following-sibling::*//*[@title='%s2']";
	public static String TRANSFORM_VALUE = "//*[@id='paper']//*[@transform='%s']";
	public static String JOURNEY_STATUS = "//*[@class='table__container ']/descendant::a[text()='%s'][1]/ancestor::td/following-sibling::td//span";
	public static String JOURNEY_START_DATE = "//*[@class='table__container ']/descendant::a[text()='%s']/ancestor::td/following-sibling::td[2]";
	public static String JOURNEY_POPOVER = "//*[@class='table__container table__container-fixed  overflow-visible']/descendant::a[text()='%s']/../..//*[contains(@class,'fl-vert')]";
	public static String TIMESLOT_VALUE = "//*[text()='%s1']/following-sibling::div[1]//*[@class='col-auto'][%s2]//*[@class='Select-value']";
	public static String PLACEHOLDER_VALUE = "//*[@placeholder='%s']";
	public static String FETCH_API_RESULTS = "//*[text()='%s']/../following-sibling::div";
	public static String COL_SEPECIFIC_SELECT_VALUE = "//*[@class='col col-%s']//*[@class='Select-value']";
	public static String SCHEDULE_DATES = "//*[text()='%s']/../span";
	public static String CARD_LABELS="//*[contains(@class,'card__content')]//label[text()='%s']";
	public static String JOURNEY_DETAIL_VALUE = "//*[text()='%s']/following-sibling::span";
	public static String TOOLTIP_BUTTONS = "//*[@data-tooltip='%s']";
	public static String SELECT_CAL_DAY = "//*[contains(@aria-label,'%s,')]";
	public static String GET_BLOCK_ID="//div[@id='paper']/descendant::*[@data-type='%s']";
	public static String BLOCK_TYPE="//div[@class='content']//*[@data-type='%s']";
	public static String BLOCK_TEXT="//*[@id='%s']//*[@class='body-text']";
	public static String OPEN_DD="//*[text()='%s']/../..//div[contains(@aria-labelledby,'downshift')]";

	By locator;

	JourneysPageElements(By locator) {
		this.locator = locator;
	}

	public By locate() {
		return this.locator;
	}

	static By getXpath(String element) {
		return By.xpath(element);
	}

	static By getId(String element) {
		return By.id(element);
	}

	static By getCSS(String element) {
		return By.cssSelector(element);
	}

	static By getClassName(String element) {
		return By.className(element);
	}
	
	static By getName(String element) {
		return By.name(element);
	}
	
	static By getLinkText(String element) {
		return By.linkText(element);
	}

}
