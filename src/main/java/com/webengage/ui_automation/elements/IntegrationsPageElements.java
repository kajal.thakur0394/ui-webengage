package com.webengage.ui_automation.elements;

import org.openqa.selenium.By;

public enum IntegrationsPageElements {

	DATA_PLATFORM_LIST(getXpath("//span[text()='Data Platform']/ancestor::li")),
	ESP_DD(getXpath("//*[@role='combobox']/div/span | //*[@role='option']")), 
	CONFIG_NAME(getName("name")),
	ADD_ESP_BTN(getXpath("//button[text()='Add ESP']")), 
	ADD_SSP_BTN(getXpath("//button[text()='Add SSP']")),
	ADD_WSP_BTN(getXpath("//button[text()='Add WSP']")), 
	POP_OVER_DELETE(getXpath("//*[text()='Delete']")),
	POP_OVER_EDIT(getXpath("//*[text()='Edit']")), 
	DELETE_BTN(getXpath("//*[text()='DELETE' or text()='Delete']")),
	WEB_PUSH_TOGGLE(getXpath("//*[contains(text(),'Web')]/../following-sibling::div/div/div")),
	WEB_PERSONALIZATION_TOGGLE(getXpath("//*[contains(text(),'Web')]/../following-sibling::div/div/div/div")),
	TOGGLE_STATUS(getXpath("//*[contains(text(),'Web')]/../following-sibling::div//*[@class='margin-left-x']")),
	SDK_CARD(getXpath("//*[text()='SDK Integration Status']/ancestor::*[@class='card']")),
	SDK_CARD_HEADERS(getXpath("//*[text()='SDK Integration Status']/ancestor::*[@class='card']/descendant::th")),
	SDK_STATUS(getXpath("//*[text()='SDK Setup']/../following-sibling::div/descendant::*[contains(@class,'status')]")),
	SAVE_BTN(getXpath("//button[text()='Save']")),
	SAVE_IOS(getXpath("//*[text()='iOS']/../../descendant::button[text()='Save']")),
	UPLOAD(getXpath("//*[@type='file']")), 
	ADD_BTN(getXpath("//*[contains(@class,'fl-add-circle')]")),
	CONFIG(getXpath("//div[@class='display-flex align-iterm-center']/label")),
	PUSH_HYPERLINK(getXpath("//a[text()='here']")),
	INVALID_PACKAGE_LABEL(getXpath("//h5[text()='You have not added any credentials yet']")),
	NAME_YOUR_CONFIGURATION(getName("name")),
	ACCOUNT_ID(getName("Account Id")),
	TOKEN(getName("Token")),
	USERNAME(getXpath(
			"//input[translate(@name,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz') = 'username']")),
	PASSWORD(getXpath(
			"//input[translate(@name,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz') = 'password']")),
	PRINCIPAL_ENTITY_ID(getName("Principal Entity Id")),
	MESSAGING_SERVICE_SID(getName("Messaging Service SID")),
	HEADER_KEY(getXpath("//*[@value='' and @name='Header Key']")),
	HEADER_VALUE(getXpath("//*[@value='' and @name='Header Value']")),
	SDK_SETUP_STATUS(getXpath("//*[text()='SDK Setup']/../child::td/descendant::*[contains(@class,'status')]")),
	WHATSAPP_BUTTON_TOGGLE(getXpath("//div[@class='react-toggle']")),
	WHATSAPP_TEMPLATE_ACTIONS(getXpath("//*[text()='WhatsApp Templates']/following::div[@class='pop-over__head']/i")),
	WEB_PUSH_TYPE_OF_PROMPT(getXpath("//span[contains(text(),'Type of Prompt')]/..//following-sibling::i")),
	WEB_PUSH_MESSAGE(getXpath("//textarea[@class='field field--textarea   ']"));

	public static String DELETE_PUSH_SERVICES = "//div[text()='%s']/following::i";
	public static String MI_APP_SECRET = "//div[.='MI Push Service']/following-sibling::div[.='%s']//input";
	public static String CHANNEL_STATUS = "//*[text()='%s']/following-sibling::td/descendant::*[contains(@class,'status')]";
	public static String CHANNEL_CONFIGURE = "//*[text()='%s']/following-sibling::td/descendant::*[text()='Configure'] | //*[text()='%s']/following-sibling::*[text()='Configure'] | //*[text()='%s']/following-sibling::a";
	public static String SELECT_PROVIDER = "//*[@id='available-providers']/..//span[text()='%s']";
	public static String FIELDS = "//*[@name='%s'] | //*[text()='%s']/../../following-sibling::div/descendant::input | //*[text()='%s']/../following-sibling::div/descendant::input | //*[text()='%s']/../../following-sibling::div/descendant::textarea";
	public static String EDIT_FIELD = "//*[text()='%s']/../following-sibling::div/descendant::button";
	public static String DD_FIELD = "//*[text()='%s1']/../../following-sibling::div/descendant::*[text()='%s2']";
	public static String SELECT_FROM_DD = "//*[contains(@aria-label,'%s')]";
	public static String NOTIFICATION_TOAST = "//*[text()='%s']";
	public static String CONFIG_PROVIDER = "//*[text()='%s']";
	public static String LIST_CONFIG_NAME = "//*[text()='%s']/preceding-sibling::td | //*[text()='%s']/../preceding-sibling::td";
	public static String OPEN_POP_OVER = "//*[text()='%s']/following-sibling::td/div[@class='pop-over '] | //*[@title='%s']/../following-sibling::td/div[@class='pop-over ']";
	public static String BUTTON = "//button[text()='%s']";
	public static String PLATFORM_SPECIFIC_DELETE = "//*[text()='%s1']/following-sibling::td[text()='%s2']/following-sibling::td//i";
	public static String ADD_WEBHOOK = "//*[contains(text(),'%s')]/ancestor::div[contains(@class,'webhook')]/descendant::b";
	public static String WEBHOOK_URL = "//*[contains(text(),'%s1')]/../../descendant::*[contains(text(),'%s2')]/following-sibling::div/descendant::*[@name='postUrl']";
	public static String WEBHOOK_FORMAT = "//*[contains(text(),'%s1')]/../../descendant::*[contains(text(),'%s2')]/following-sibling::div/descendant::*[@name='responseFormat']";
	public static String WEBHOOK_SELECT = "//*[contains(text(),'Add New Webhooks')]/../../descendant::*[contains(text(),'%s')]/following-sibling::div/descendant::*[@name='responseFormat']";
	public static String WEBHOOK_SAVE = "//*[contains(text(),'Add New Webhooks')]/../../descendant::*[contains(text(),'%s')]/following-sibling::div/descendant::*[@title='Save/Update']";
	public static String WEBHOOK_DELETE = "//*[contains(text(),'Configured Webhooks')]/../../descendant::*[contains(text(),'%s')]/following-sibling::div/descendant::*[@title='Delete']";
	public static String ADD_PLATFORM = "//*[text()='%s']/parent::div";
	public static String GENERIC_FIELD = "//*[text()='%s']/../following-sibling::div//input";
	public static String OS_TAB = "//a[text()='%s'] ";
	public static String PACKAGE_NAME_RESULT = "//td[text()='%s']";
	public static String WA_TEMPLATE_POP_OVER = "//*[text()='%s1']/../..//*[text()='%s2']/../..//*[@class='pop-over ']";
	public static String CUSTOM_TEMPLATE_POP_OVER = "//*[text()='%s']/ancestor::tr//*[@class='pop-over ']";
	public static String STATUS = "//tbody//tr[1]//following-sibling::td/span[text()='%s']";
	public static String ADD_WHATSAPP_TEMPLATE = "//h5[text()='WhatsApp Templates']/following::label";
	public static String WHATSAPP_TEMPLATE_FIELDS = "//label[text()='%s']/following::div[2]/input";
	public static String WHATSAPP_TEMPLATE_TEXTAREA = "//label[text()='%s']/following::div[2]/textarea";
	public static String WHATSAPP_TEMPLATE_TYPE = "//input[@value='%s']/../.";
	public static String TEMPLATE_EDIT_BUTTON = "//*[.='%s']//i";
	public static String TEMPLATE_DD = "//*[.='%s1']/following-sibling::*//*[contains(@aria-label,'%s2')]";
	public static String GET_PAGES_FROM_CARDNAME = "//*[text()='%s']/ancestor::div[@class='card']//*[@class='pagination__page__link']";

	By locator;

	IntegrationsPageElements(By locator) {
		this.locator = locator;
	}

	public By locate() {
		return this.locator;
	}

	static By getXpath(String element) {
		return By.xpath(element);
	}

	static By getName(String element) {
		return By.name(element);
	}

	static By getId(String element) {
		return By.id(element);
	}

	static By getCSS(String element) {
		return By.xpath(element);
	}

	static By getClassName(String element) {
		return By.className(element);
	}

}
