package com.webengage.ui_automation.elements;

import org.openqa.selenium.By;

public enum CustomDashboardPageElements {
	DASHBOARD_INPUT(getXpath("//label[text()='Dashboard Name']/parent::div//following-sibling::*//input")),
	MODAL_CLOSE(getXpath("//*[contains(@class,'modal__close')]")),
	ACCESS_CELL_HEADERS(getXpath("//*[contains(@class,'table__cell--header')]")),
	CARD_PREVIEW(getXpath("//h5[@class='font-72']")),
	DASBOARDS_DROPDOWN(getXpath("//*[contains(@class,'fl-dark margin-top')]"));

	public static String ACCESS_DD = "//label[text()='%s']/parent::div/following-sibling::div";
	public static String ACCESS_TYPE_CLEAR_BTN="//*[text()='%s']/ancestor::div[@class='row form__row']//*[@class='Select-clear']";
	public static String USER_ACCESS_VALUE = "//td/span[text()='%s1']/following::td[%s2]";
	public static String DASHBOARD_OPERATIONS = "//span[contains(@data-tooltip,'%s')]";
    public static String PINUSER="//div[.='%s']/ancestor::div[contains(@class,'flex')]//i[contains(@class,'pin')]";
    public static String DASHBOARD_USER_STATS="//*[contains(text(),'%s')]/ancestor::div[contains(@class,'card transition')]//h5";
    public static String CARDOPTION="//*[contains(text(),'%s')]/ancestor::div[contains(@class,'card transition')]//i";
	
	By locator;

	CustomDashboardPageElements(By locator) {
		this.locator = locator;
	}

	public By locate() {
		return this.locator;
	}

	static By getXpath(String element) {
		return By.xpath(element);
	}

	static By getClassName(String element) {
		return By.className(element);
	}

	static By getId(String element) {
		return By.id(element);
	}
}
