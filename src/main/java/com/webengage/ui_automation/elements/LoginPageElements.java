package com.webengage.ui_automation.elements;

import org.openqa.selenium.By;

public enum LoginPageElements {

	LOGIN_USERNAME(getId("field_un")),
	LOGIN_PASSWORD(getId("field_pw")),
	REGION_INDIA(getXpath("//input[@value='in']")),
	REGION_US(getXpath("//input[@value='us']")),
	SIGN_IN(getXpath("//*[text()='Sign In']")),
	CHOOSE_PROJECT(getId("we-account-name")),
	BUY_BUTTON(getXpath("//*[text()='Buy Now!']")),
	DOMAIN_SDK(getXpath("//div[contains(@class,'form__element')]//input[@type='text']")),;

	By locator;

	LoginPageElements(By locator) {
		this.locator = locator;
	}

	public By locate() {
		return this.locator;
	}

	static By getXpath(String element) {
		return By.xpath(element);
	}

	static By getName(String element) {
		return By.name(element);
	}

	static By getId(String element) {
		return By.id(element);
	}

	static By getCSS(String element) {
		return By.cssSelector(element);
	}

	static By getClassName(String element) {
		return By.className(element);
	}

}
