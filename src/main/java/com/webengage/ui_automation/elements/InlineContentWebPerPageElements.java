package com.webengage.ui_automation.elements;

import org.openqa.selenium.By;

public enum InlineContentWebPerPageElements {
	CREATE_INLINE(getXpath("//h4//following-sibling::a")),
	CAMPAIGN_NAME(getXpath("//label[text()='CAMPAIGN NAME']/..//following-sibling::div//input")),
	PROP_DROPDOWN(getXpath("//div[starts-with(@id,'rss_')]")),
	REPLACE_PROP_PLACEMENT(getXpath("//body/button")),
	WEBP_IMAGE(getXpath("//div[@style='opacity: 1;']//img")),
	CUSTOMHTML(getXpath("//div[@class=' form__element ']//textarea")),
	PROPERTY_ICON(getXpath("//label[text()='Select Property']/..//following-sibling::div[contains(@class,'align-items-center')]/span")),
	CLOSE_PROPERTY(getXpath("//a[contains(@class,'modal__close')]")),
	WHEN_TAB(getXpath("//a[text()='When']")),
	WIDTH(getXpath("//input[@type='number']")),
	CUSTOMHTMLTOGGLE(getXpath("//span[text()='Custom HTML & CSS']/following-sibling::span//div[@class='react-toggle']")),
	BACKGRND_IMG(getXpath("//div[contains(@class,'custom-code-container')]/following-sibling::img")),
	CUSTOM_IMG(getXpath("//div[contains(@class,'custom-code-container')]//img")),
	CUSTOM_EMOJI(getXpath("//p[@id='emoji']")),
	MULTILINGUAL(getXpath("//p[@id='multilingualDataSet']")),
	PREVIEWDOM(getXpath("//span[text()='Preview']")),
	PREVIEW_TEXT(getXpath("//div[contains(@class,'custom-code-container')]"));
	
	public static String TOGGLE="//span[text()='%s']/following-sibling::span//div[@class='react-toggle']";
	public static String DEL_SCHEDULE="//span[text()='%s']/following-sibling::div[1]//div[@class='Select-value']/span";
	public static String PROPERTY_VALUE="//label[text()='%s']/..//following-sibling::div//input";
	public static String LAYOUT = "//span[text()='%s']";
	public static String IMAGE = "//label[text()='%s']/../following-sibling::div//input[@placeholder='https://www.abc.com/image.png']";
	public static String PROP_PLACEMENT_VALIDATE="//body/button/%s-sibling::div";
	public static String EMOJI_LINK="//*[text()='%s']/following::div[@class='emoji-picker ']/i";
	public static String CAMPAIGN_SRC="//*[@src='%s']";
	
	By locator;

	InlineContentWebPerPageElements(By locator) {
		this.locator = locator;
	}

	public By locate() {
		return this.locator;
	}

	static By getXpath(String element) {
		return By.xpath(element);
	}

	static By getClassName(String element) {
		return By.className(element);
	}
}
