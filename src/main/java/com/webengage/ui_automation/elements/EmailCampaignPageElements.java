package com.webengage.ui_automation.elements;

import org.openqa.selenium.By;

public enum EmailCampaignPageElements {

	MSG(getXpath("//body[@id='tinymce']/p")), 
	IFRAME(getXpath("//iframe[@id='tiny-editor_ifr']")),
	PROCEED_BTN(getXpath("//div[contains(@class,'modal__controls')]/button[@class='button button--secondary']")),
	TEMPLATE(getXpath("//input[@value='template']/parent::label")), 
	BASIC_TEMPLATE(getXpath("//a[text()='Basic']")),
	BLANK_TYPE(getXpath("//span[text()='Blank Template']")), 
	CONTENT(getXpath("//span[text()='Content']")),
	TEMP_IFRAME(getXpath("//div[@id='bEditor']//iframe")), 
	DRAG(getXpath("//span[@class='Text']/div")),
	DROP(getXpath("//div[contains(@class,'drop-target')]")), 
	TEXT(getXpath("//div[@contenteditable='true']")),
	EDIT(getXpath("//a[contains(@href,'edit')]")),
	VERIFY_IFRAME(getXpath("//div[@class='email-preview-message ']/iframe")),
	NEWCLEAR(getXpath("//div[@contenteditable='true']//p/parent::div")),
	AMP_FALLBACK(getXpath("//span[text()='Fallback']")),
	UNSUB_LINK(getXpath("//label[text()='Insert Unsubscribe link']")),
	BODY_IFRAME(getXpath("//*[@data-id='tiny-editor']")), 
	PREVIEW_IFRAME(getXpath("//*[@id='iframe']")),
	PREVIEW_IFRAME_DRAG_DROP(getClassName("amp-iframe")),
	PREVIEW_DRAG_DROP_BODY(getXpath("//td[@class='pad']/div/div/p")),
	PREVIEW_BODY_IFRAME(getXpath("//p/parent::body[not (@id)]/..")),
	PREVIEW_EMAIL_CLOSE(getXpath("//div[@class='modal__head']//a")), 
	PREVIEW_EMAIL_CONTENT(getTagName("tbody")),
	EDITOR(getXpath("//span[text()='Edit in Drag & Drop Editor']/parent::div/..")),
	DRAG_DROP_EMOJI_PICKER(getXpath("//h5[text()='Insert Personalization or Emoji']/following::div[@id='emoji-picker']")),
    DRAG_DROP_EMOJI(getXpath("//div[@data-name='Smileys & People']/following-sibling::span[1]")),
	BODY_TEXT(getXpath("//*[@id='tinymce']"));
	
	public static String EMAIL_INPUT = "//label[text()='%s']/parent::div//following-sibling::div//input[@type='text']";
    public static String ENABLE_TOGGLE_BUTTON="//label[text()='%s']//parent::div//following-sibling::div//div[contains(@class,'toggle-thumb')]";
    
	By locator;

	EmailCampaignPageElements(By locator) {
		this.locator = locator;
	}

	public By locate() {
		return this.locator;
	}

	static By getXpath(String element) {
		return By.xpath(element);
	}

	static By getClassName(String element) {
		return By.className(element);
	}

	static By getId(String element) {
		return By.id(element);
	}

	static By getTagName(String element) {
		return By.tagName(element);
	}
}
