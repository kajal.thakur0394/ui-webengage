package com.webengage.ui_automation.initialize;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class Initializer {
	public static String modulesPath = "src/test/resources/features/modules/";
	public static String modulesFolder = System.getProperty("user.dir") + "/" + modulesPath;
	public static String cucumberPropPath = System.getProperty("user.dir")
			+ "/src/test/resources/buildCucumberOptions.properties";

	public static void main(String[] args) throws IOException {
		try {
			if (System.getProperty("set.executionType").equalsIgnoreCase("Parallel")) {
				TestRunnerActions.generateTestRunners(System.getProperty("set.Suite"),
						System.getProperty("set.Module"));
			}
		} catch (NullPointerException e) {
			buildCucumberProps();
		}
	}

	public static void buildCucumberProps() throws IOException {
		Properties cucumberOptProps = new Properties();
		String suite = System.getProperty("set.Suite");
		String module = System.getProperty("set.Module");
		switch (suite) {
		case "Regression":
			cucumberOptProps = getRegressionProperties(module);
			break;
		case "WebSDKSuite":
		case "MobileSDKSuite":
		case "Shopify":
			cucumberOptProps = getSDKTestingProperties(suite, module);
			break;
		case "Smoke":
			cucumberOptProps = getSmokeProperties(module);
			break;
		case "End-to-End Distributed":
			cucumberOptProps = gete2eDistributedProperties(module);
			break;
		case "Test Data Creation":
			cucumberOptProps = getTestDataProperties(module);
			break;
		case "WSP Onboarding":
			cucumberOptProps = getWspProperties(module);
			break;
		default:
			cucumberOptProps = customTag(suite);
			break;
		}
		FileOutputStream outputStream = new FileOutputStream(cucumberPropPath);
		cucumberOptProps.store(outputStream, "Cucumber Options for current build");
	}

	private static Properties customTag(String suite) {
		Properties regProp = new Properties();
		regProp.put("cucumber.filter.tags", "@" + suite.replaceAll(" ", "_"));
		regProp.put("cucumber.features", "src/test/resources/features");
		return regProp;
	}

	private static Properties getTestDataProperties(String module) {
		Properties regProp = new Properties();
		String dynamic = createDynamicFilterTags(module, "TestDataCreation");
		String tags = (dynamic.substring(0, dynamic.length() - 3).trim());
		regProp.put("cucumber.filter.tags", tags);
		regProp.put("cucumber.features", "src/test/resources/features/testDataUtility");
		return regProp;
	}

	private static Properties gete2eDistributedProperties(String module) {
		Properties regProp = new Properties();
		if (!module.equalsIgnoreCase("All")) {
			String dynamic = createDynamicFilterTags(module, "End-to-End");
			String add = (dynamic.substring(0, dynamic.length() - 3).trim());
			regProp.put("cucumber.filter.tags", "(@Distributed) and (not @disabled) and (" + add + ")");
			regProp.put("cucumber.features", modulesPath);
		} else {
			regProp.put("cucumber.filter.tags", "(not @disabled) and (@End-to-End) and (@Distributed)");
			regProp.put("cucumber.features", modulesPath);
		}
		return regProp;
	}

	private static Properties getSmokeProperties(String module) {
		Properties regProp = new Properties();
		if (!module.equalsIgnoreCase("All")) {
			String dynamic = createDynamicFilterTags(module, "Smoke");
			String add = (dynamic.substring(0, dynamic.length() - 3).trim());
			regProp.put("cucumber.filter.tags", "(not @disabled) and (" + add + ")");
			regProp.put("cucumber.features", modulesPath);
		} else {
			regProp.put("cucumber.filter.tags", "(not @disabled) and (@Smoke)");
			regProp.put("cucumber.features", modulesPath);
		}
		return regProp;
	}

	public static String moduleFeatures(String module) {
		StringBuilder moStringBuilder = new StringBuilder();
		String[] featureNames = new File(modulesFolder).list();
		for (String indivFeatureName : featureNames) {
			if (indivFeatureName.toLowerCase().contains(module.toLowerCase())) {
				moStringBuilder.append(modulesPath + indivFeatureName);
				break;
			}
		}
		return moStringBuilder.toString();
	}

	private static Properties getSDKTestingProperties(String suite, String module) {
		Properties regProp = new Properties();
		if ((!module.equalsIgnoreCase("All")) && module.length() != 0) {
			String dynamic = createDynamicFilterTags(module, suite);
			String add = (dynamic.substring(0, dynamic.length() - 3).trim());
			regProp.put("cucumber.filter.tags", "(not @disabled) and (" + add + ")");
			regProp.put("cucumber.features", modulesPath);
		} else {
			regProp.put("cucumber.filter.tags", "(not @disabled) and (@" + suite + ")");
			regProp.put("cucumber.features", modulesPath);
		}
		return regProp;
	}

	private static String createDynamicFilterTags(String module, String type) {
		StringBuilder moStringBuilder = new StringBuilder();
		String[] moduleArr = module.split(",");
		for (String s : moduleArr) {
			moStringBuilder.append("(@" + type + " and @" + s.trim() + ") or ");
		}
		return moStringBuilder.toString();
	}

	private static Properties getWspProperties(String module) {
		Properties regProp = new Properties();
		String dynamic = createDynamicFilterTags(module, "WSP_Onboarding");
		String add = (dynamic.substring(0, dynamic.length() - 3).trim());
		regProp.put("cucumber.filter.tags", add);
		regProp.put("cucumber.features", "src/test/resources/features");
		return regProp;
	}

	private static Properties getRegressionProperties(String module) {
		Properties regProp = new Properties();
		if (!module.equalsIgnoreCase("All")) {
			String dynamic = createDynamicFilterTags(module, "Regression");
			String add = (dynamic.substring(0, dynamic.length() - 3).trim());
			regProp.put("cucumber.filter.tags",
					"((not @disabled and not @End-to-End and not @WebSDKSuite and not @MobileSDKSuite and not @Shopify)) and ("
							+ add + ")");
			regProp.put("cucumber.features", modulesPath);
		} else {
			regProp.put("cucumber.filter.tags",
					"((not @disabled and not @End-to-End and not @WebSDKSuite and not @MobileSDKSuite and not @Shopify)) and (@Regression)");
			regProp.put("cucumber.features", modulesPath);
		}
		return regProp;
	}

}