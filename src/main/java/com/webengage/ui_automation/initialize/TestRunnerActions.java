package com.webengage.ui_automation.initialize;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import com.webengage.ui_automation.utils.Log;

public class TestRunnerActions {
	private static final String FEATURE_FILE_PATH = "src/test/resources/features/modules/";
	private static final String TESTRUNNER_FILE_PATH = "/src/test/java/com/webengage/ui_automation/runners/";
	private static final String RUNTIME_TESTRUNNER_FILE = "/src/test/resources/TestRunnerTemplate.txt";

	public static void generateTestRunners(String suite, String module) throws IOException {
		deleteTestRunners();
		selectSuiteAndGenerateRunners(suite, module);
	}

	private static void selectSuiteAndGenerateRunners(String suite, String module) throws IOException {
		String tag = "";
		switch (suite) {
		case "Regression":
		case "Regression - Desired Account":
			tag = "((not @disabled and not @End-to-End and not @WebSDKSuite and not @MobileSDKSuite and not @Shopify)) and (@"
					+ suite.split("-")[0].trim() + ")";
			createRunners(findFeatureFiles(module), tag);
			break;
		case "Smoke":
		case "Smoke - Desired Account":
			tag = "(not @disabled) and (@" + suite.split("-")[0].trim() + ")";
			createRunners(findFeatureFiles(module), tag);
			break;
		case "End-to-End Distributed":
			tag = "(@Distributed) and (not @disabled) and (@" + suite.split(" ")[0] + ")";
			createRunners(findFeatureFiles(module), tag);
			break;
		default:
			break;
		}
	}

	private static void createRunners(List<String> featureFiles, String tag) {
		for (String featureFile : featureFiles) {
			writeTestRunner(generateClassName(featureFile), featureFile.replace("\\", "/"), tag);
		}
	}

	private static List<String> findFeatureFiles(String module) throws IOException {
		List<String> featuresPath = new ArrayList<>();
		List<String> actualFeaturePath = findFeatureDirectories(FEATURE_FILE_PATH);
		if (!module.equalsIgnoreCase("All")) {
			String[] moduleArr = module.split(",");
			for (String mod : moduleArr) {
				for (String feature : actualFeaturePath) {
					if (feature.contains(mod)) {
						featuresPath.add(feature);
						break;
					}
				}
			}
			return featuresPath;
		} else {
			return actualFeaturePath;
		}
	}

	private static void deleteTestRunners() {
		File folder = new File(System.getProperty("user.dir") + TESTRUNNER_FILE_PATH);
		File[] files = folder.listFiles(
				(dir, name) -> !(name.endsWith("SequentialRunner.java") || name.endsWith("BeforeTest.java")));
		for (File file : files) {
			file.delete();
		}
	}

	private static List<String> findFeatureDirectories(String featureDirectoryPath) throws IOException {
		List<String> featureFilePaths = new ArrayList<>();
		Path start = Paths.get(featureDirectoryPath);
		int maxDepth = Integer.MAX_VALUE;
		try (Stream<Path> paths = Files.walk(start, maxDepth)) {
			paths.filter(Files::isRegularFile).filter(p -> p.toString().endsWith(".feature"))
					.forEach(p -> featureFilePaths.add(p.toString()));
		}
		return featureFilePaths;
	}

	private static String generateClassName(String featureFile) {
		return featureFile.split("modules")[1].replace(".feature", "").replace("/", "").concat("Runner").split("_")[1];
	}

	private static void writeTestRunner(String className, String featureFile, String tag) {
		String config;
		try {
			config = new String(
					Files.readAllBytes(Paths.get(System.getProperty("user.dir") + RUNTIME_TESTRUNNER_FILE)));
			config = config.replace("<featureFile>", featureFile).replace("<className>", className).replace("<tag>",
					tag);
			writeToFile(className + ".java", config);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void writeToFile(String filename, String content) throws IOException {
		File file = new File(System.getProperty("user.dir") + TESTRUNNER_FILE_PATH + filename);
		file.getParentFile().mkdirs();
		FileWriter fileWriter = new FileWriter(file);
		fileWriter.write(content);
		fileWriter.close();
		Log.info("Successfully wrote to file " + filename);
	}
}
