package com.webengage.ui_automation.pages;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.webengage.ui_automation.elements.CommonPageElements;
import com.webengage.ui_automation.elements.CustomDashboardPageElements;
import com.webengage.ui_automation.elements.IntegrationsPageElements;
import com.webengage.ui_automation.utils.CustomAssertions;
import com.webengage.ui_automation.elements.SMSCampaignPageElements;
import com.webengage.ui_automation.elements.SegmentsPageElements;
import com.webengage.ui_automation.utils.NotificationToastMessages;
import com.webengage.ui_automation.utils.SeleniumExtendedUtility;

public class CustomDashboardPage extends SeleniumExtendedUtility {
	CommonPage commonPage = new CommonPage();
	String count;

	public String getuserCount() {
		return this.count;
	}

	public void setUserCount(String count) {
		this.count = count;
	}

	public void enterDashboardName(String dashboardName) {
		sendKeys(CustomDashboardPageElements.DASHBOARD_INPUT.locate(), dashboardName);
	}

	public void editDashboardName(String dashBoardName, String editedDashboardName) {
		click(dynamicXpathLocator(CommonPageElements.OPEN_POP_OVER_MENU_UTF, dashBoardName));
		click(IntegrationsPageElements.POP_OVER_EDIT.locate());
		clearFieldAndSendkeys(CustomDashboardPageElements.DASHBOARD_INPUT.locate(), editedDashboardName);
	}

	public void addUsers(String accessMode, String User) {
		click(dynamicXpathLocator(CustomDashboardPageElements.ACCESS_DD, accessMode));
		click(dynamicXpathLocator(CommonPageElements.CATALOG_TYPE_VALUE, User));
	}

	public void getUsers(Map<String, String> userChoices) {
		if (checkIfElementisLocated(
				dynamicXpathLocator(CustomDashboardPageElements.ACCESS_TYPE_CLEAR_BTN, "View Access")))
			click(dynamicXpathLocator(CustomDashboardPageElements.ACCESS_TYPE_CLEAR_BTN, "View Access"));
		if (checkIfElementisLocated(
				dynamicXpathLocator(CustomDashboardPageElements.ACCESS_TYPE_CLEAR_BTN, "Edit Access")))
			click(dynamicXpathLocator(CustomDashboardPageElements.ACCESS_TYPE_CLEAR_BTN, "Edit Access"));
		userChoices.entrySet().forEach(accessType -> {
			if (accessType.getValue() != null)
				addUsers(accessType.getKey(), accessType.getValue());
		});
		if (checkIfElementisLocated(dynamicXpathLocator(CommonPageElements.BUTTON, "create Dashboard"))) {
			commonPage.clickButton("create Dashboard");
		} else {
			commonPage.clickButton("update Dashboard");
		}
	}

	public void verifyAccessMode(String user, Map<String, String> expectedAccess) {
		click(dynamicXpathLocator(CommonPageElements.MODULE, "Specific team members"));
		expectedAccess.entrySet().forEach(expectedUserAccess -> {
			boolean validated = false;
			List<WebElement> elements = findElements(CustomDashboardPageElements.ACCESS_CELL_HEADERS.locate());
			int i = 0;
			for (WebElement el : elements) {
				System.out.println(el.getText());
				if (el.getText().equalsIgnoreCase(expectedUserAccess.getKey())) {
					CustomAssertions.assertEquals("Value does not match", expectedUserAccess.getValue(),
							fetchText(dynamicXpathLocator(CustomDashboardPageElements.USER_ACCESS_VALUE, user,
									String.valueOf(i))));
					validated = true;
					break;
				}
				i++;
			}
			CustomAssertions.assertTrue("Validationn failed", validated);
		});
	}

	public void verifyEditedNameAndVisibility(String dashboardName, String visibilityType) {
		searchAndOpenDashBoard(dashboardName);
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, visibilityType));
		CustomAssertions.assertEquals("Visibility does not match", visibilityType,
				fetchText(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, visibilityType)));
	}

	public void actionOverviewPage(String action, String dashboardName) throws InterruptedException {
		click(dynamicXpathLocator(CustomDashboardPageElements.DASHBOARD_OPERATIONS, action));
		switch (action.toLowerCase()) {
		case "delete":
			click(CommonPageElements.DELETE_BUTTON.locate());
			TimeUnit.SECONDS.sleep(3);
			click(CustomDashboardPageElements.DASBOARDS_DROPDOWN.locate());
			click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Manage Dashboard"));
			CustomAssertions.assertFalse(" Dashboard is still visible in listing page",
					checkIfElementisLocated(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, dashboardName)));
			break;
		case "edit":
			waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Edit Dashboard"));
			break;
		default:
			CustomAssertions.assertTrue(action + "action has not yet been addressed ", false);
			break;
		}
	}

	public void redirectToUserPage(String page, String dashboardName) {
		searchAndOpenDashBoard(dashboardName);
		click(dynamicXpathLocator(CommonPageElements.GENERIC_DIVTEXT, page));
	}

	public void verifyUserCardStats(String pinCard) {
		CustomAssertions.assertEquals("User Stats not matched",
				fetchText(dynamicXpathLocator(CustomDashboardPageElements.DASHBOARD_USER_STATS, pinCard)),
				getuserCount());
	}

	public void searchAndOpenDashBoard(String dashboardName) {
		waitForElementToBeVisible(SMSCampaignPageElements.TABLE_HEADER.locate());
		clearFieldAndSendkeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Search by Dashboard Name"),
				dashboardName + Keys.RETURN);
		commonPage.openCampaign(dashboardName);
	}

	public void cardOption(String option, String cardName) {
		try {
			TimeUnit.SECONDS.sleep(3);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		click(dynamicXpathLocator(CustomDashboardPageElements.CARDOPTION, cardName));
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, option));
		if (option.equals("Rename")) {
			clearFieldAndSendkeys(dynamicXpathLocator(IntegrationsPageElements.FIELDS, "card name"),
					"Renamed ".concat(cardName));
			click(dynamicXpathLocator(CommonPageElements.BUTTON, "Rename Card"));
		} else {
			click(CommonPageElements.DELETE_BUTTON.locate());
			waitForElementToBeInvisible(dynamicXpathLocator(CustomDashboardPageElements.CARDOPTION, cardName));
			CustomAssertions.assertFalse("Card is not deleted",
					checkIfElementisLocated(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, cardName)));
		}
	}

	public void pinCard(String pincard, String cardName, String dashboardName) {
		click(dynamicXpathLocator(CustomDashboardPageElements.PINUSER, pincard));
		sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Card Name"), cardName);
		click(dynamicXpathLocator(SegmentsPageElements.OPEN_DD, "Dashboards"));
		click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, dashboardName));
		setUserCount(fetchText(CustomDashboardPageElements.CARD_PREVIEW.locate()));
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "pin to Dashboard"));
		verifyToastMessage(NotificationToastMessages.CARD_PINNED.message());
	}
}
