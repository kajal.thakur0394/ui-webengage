package com.webengage.ui_automation.pages;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.time.temporal.WeekFields;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebElement;
import java.time.LocalDate;
import java.util.Random;
import com.webengage.ui_automation.elements.AdditionalPageElements;
import com.webengage.ui_automation.elements.AnalyticsPageElements;
import com.webengage.ui_automation.elements.CampaignsPageElements;
import com.webengage.ui_automation.elements.CatalogsPageElements;
import com.webengage.ui_automation.elements.CommonPageElements;
import com.webengage.ui_automation.elements.SMSCampaignPageElements;
import com.webengage.ui_automation.elements.SegmentsPageElements;
import com.webengage.ui_automation.elements.UserPageElement;
import com.webengage.ui_automation.utils.CustomAssertions;
import com.webengage.ui_automation.utils.NotificationToastMessages;
import com.webengage.ui_automation.utils.SeleniumExtendedUtility;
import io.cucumber.datatable.DataTable;

public class AnalyticsPage extends SeleniumExtendedUtility {
	CommonPage commPage = new CommonPage();
	SegmentsPage segPage = new SegmentsPage();
	String[] multiEvents;

	public void eventFilterSelection(List<List<String>> filterOptions) {
        waitUntilAttributeDoesNotContain(dynamicXpathLocator(AnalyticsPageElements.SELECT_DD, "SHOW"), "class",
				"disabled");		
		try {
			TimeUnit.SECONDS.sleep(3);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		for (List<String> selectVal : filterOptions) {
			String selectDD = selectVal.get(0);
			String selectDDvalue = selectVal.get(1);
			if (selectDDvalue.equals("-"))
				continue;
			if (selectDD.equals("OF"))
				selectMultiEvents(selectDDvalue);
			else {
				click(dynamicXpathLocator(AnalyticsPageElements.SELECT_DD, selectDD));
				click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, selectDDvalue));
			}
		}
		click(CommonPageElements.SYNC_BTN.locate());
		waitForElementToBeInvisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Event name"));
	}

	private void selectMultiEvents(String selectDDvalue) {
		multiEvents = selectDDvalue.split(",");
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, "All"));
		if (multiEvents.length > 10) {
			for (String eventName : multiEvents) {
				clearFieldAndSendkeys(AnalyticsPageElements.SEARCH_EVENT.locate(), eventName);
				click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, eventName));
			}
			commPage.verifyToastMessage(NotificationToastMessages.EVENTS_LIMIT_EXCEED.message());
		} else if (multiEvents.length == 1) {
			clearFieldAndSendkeys(AnalyticsPageElements.SEARCH_EVENT.locate(), multiEvents[0]);
			click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, multiEvents[0]));
			click(dynamicXpathLocator(CommonPageElements.TEXT_CONTAINS_SPAN, "Selected"));
			CustomAssertions.assertEquals("Size does not match the selection",
					Integer.parseInt(fetchText(dynamicXpathLocator(CommonPageElements.TEXT_CONTAINS_SPAN, "Selected"))
							.replaceAll("[^0-9]", "")),
					multiEvents.length);
		} else {
			for (String eventName : multiEvents) {
				clearFieldAndSendkeys(AnalyticsPageElements.SEARCH_EVENT.locate(), eventName);
				click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, eventName));
				click(dynamicXpathLocator(CommonPageElements.TEXT_CONTAINS_SPAN, "Selected"));
				CustomAssertions.assertTrue(eventName + " has not been selected", checkIfElementisLocated(
						dynamicXpathLocator(CommonPageElements.CHECKBOX_VALUE_VERIFY, eventName)));
				click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, "All"));
				clearFieldAndSendkeys(AnalyticsPageElements.SEARCH_EVENT.locate(), eventName);
				CustomAssertions.assertTrue(eventName + " has not been selected", checkIfElementisLocated(
						dynamicXpathLocator(CommonPageElements.CHECKBOX_VALUE_VERIFY, eventName)));
			}
			click(dynamicXpathLocator(CommonPageElements.TEXT_CONTAINS_SPAN, "Selected"));
			CustomAssertions.assertEquals("Size does not match the selection",
					Integer.parseInt(fetchText(dynamicXpathLocator(CommonPageElements.TEXT_CONTAINS_SPAN, "Selected"))
							.replaceAll("[^0-9]", "")),
					multiEvents.length);
		}
	}

	public void alertAndSplitByDisableCheck() {
		bellIconGreyOutCheck("An alert can only be created for a single event.", "alert");
		CustomAssertions.assertTrue("Split by is not disabled",
				fetchAttributeValue(AnalyticsPageElements.SPLITBY_DROPDOWN.locate(), "class").contains("ss-disabled"));
	}

	public void validateTableContent(List<List<String>> cellValues) {
		for (List<String> cellVal : cellValues) {
			String fieldName = cellVal.get(0);
			if (cellVal.get(0).contains("Date")) {
				fieldName = getPreviousDate();
			}
			int expectedValue = Integer.parseInt(cellVal.get(1).replaceAll("\\D+", ""));
			int actualValue = Integer
					.parseInt(fetchText(dynamicXpathLocator(CommonPageElements.TABLE_VALUE_CHECK, fieldName)));
			if (cellVal.get(1).contains(">"))
				CustomAssertions.assertTrue("Expected value condition not staisfied", actualValue > expectedValue);
		}
	}

	private String getPreviousDate() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		SimpleDateFormat format1 = new SimpleDateFormat("d MMM", Locale.ENGLISH);
		return format1.format(new Date(cal.getTimeInMillis()));
	}

	public void pathFilterSelection(List<List<String>> pathFilters) {
		for (List<String> selectVal : pathFilters) {
			String label = selectVal.get(0);
			for (int i = 1; i < selectVal.size(); i++) {
				try {
					if (selectVal.get(i).contains(">")) {
						String columnName = selectVal.get(i).split(">")[0];
						String columnValue = selectVal.get(i).split(">")[1];
						click(dynamicXpathLocator(SegmentsPageElements.OPEN_DD_SELECT, label, columnName));
						commPage.selectInDropdown(
								dynamicXpathLocator(SegmentsPageElements.OPEN_DD_SELECT, label, columnName),
								columnValue);
					} else if (label.contains("COLLAPSE")) {
						if (selectVal.get(i).contains("true"))
							click(dynamicXpathLocator(CommonPageElements.LABEL_CHECKBOX, label));
					} else
						clearFieldAndSendkeys(dynamicXpathLocator(CommonPageElements.LABEL_TEXT_FIELD, label),
								selectVal.get(i));
				} catch (NullPointerException e) {
					continue;
				}
			}
		}
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "Show path"));
	}

	public void validateStepinHighChart(String eventName, String stepNumber) {
		waitForElementToBeVisible(dynamicXpathLocator(AnalyticsPageElements.PATH_STEP_EVENT, eventName));
		waitForElementToBeVisible(dynamicXpathLocator(AnalyticsPageElements.PATH_STEP, eventName, stepNumber));
	}

	public void enterFunnelName(String funnelName) {
		sendKeys(AnalyticsPageElements.INPUT_FUNNEL.locate(), funnelName);
	}

	public void addFunnelSteps(List<List<String>> funnelStepFilters) {
		addSteps(funnelStepFilters);
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "Create Funnel"));
		verifyToastMessage(NotificationToastMessages.FUNNEL_CREATED.message());
	}

	private void addSteps(List<List<String>> funnelStepFilters) {
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.BUTTON, "Add Step"));
		funnelStepFilters.forEach(s -> {
			String stepNo = s.get(0);
			String eventName = s.get(1);
			if (!checkIfElementisLocated(dynamicXpathLocator(AnalyticsPageElements.FUNNEL_STEP_DD, stepNo))) {
				click(dynamicXpathLocator(CommonPageElements.BUTTON, "Add Step"));
			}
			click(dynamicXpathLocator(AnalyticsPageElements.FUNNEL_STEP_DD, stepNo));
			sendKeys(CommonPageElements.SEARCH.locate(), eventName);
			click(dynamicXpathLocator(AnalyticsPageElements.SELECT_FUNNEL_EVENT, eventName));
		});
	}

	public void isEventPresent(List<String> eventList) {
		eventList.forEach(event -> {
			CustomAssertions.assertTrue("Event '" + event + "' not found.",
					findElements(dynamicXpathLocator(CatalogsPageElements.COLUMN_EVENT, event)).size() > 0);
		});
	}

	public void filterCustomDays(String dayType) {
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, "Total"));
		click(AnalyticsPageElements.DROPDOWN_ARROW.locate());
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, dayType));
	}

	public void verifyGraphOrLineData(String operator, String total, String showType, String dayType, String graphType,
			List<Map<String, String>> data) {
		Set<String> graphElementSetActual = new HashSet<>();
		Set<String> graphElementSetExpected = new HashSet<>();
		boolean isGreaterThanEqual = operator.toLowerCase().contains("greater");
		boolean isUniquesLineGraph = showType.equalsIgnoreCase("Uniques") && graphType.equalsIgnoreCase("Line");
		validateTotalCount(isGreaterThanEqual, total);
		if (!dayType.equalsIgnoreCase("Weeks"))
			validateDay(getDate(dayType));
		List<WebElement> eventElement = findElements(AnalyticsPageElements.BAR_ELEMENTS.locate());
		eventElement.forEach(element -> {
			extractGraphElement(element, graphElementSetActual);
		});
		if (isGreaterThanEqual)
			valueGreaterEqualEventCheck(graphElementSetActual, data);
		processCheckboxData(data, graphElementSetExpected, isGreaterThanEqual);
		if (isGreaterThanEqual)
			valueGreaterEqualEventCheck(graphElementSetExpected, data);
		if (!isGreaterThanEqual && !isUniquesLineGraph)
			CustomAssertions.assertTrue("Element not present", graphElementSetExpected.equals(graphElementSetActual));
	}

	private void validateTotalCount(boolean isGreaterThanEqual, String total) {
		if (multiEvents.length == 1) {
			int dynamicTotal = Integer.parseInt(fetchText(dynamicXpathLocator(UserPageElement.LABEL_VALUE, "Total")));
			if (isGreaterThanEqual) {
				CustomAssertions.assertTrue("Count is not correctly displayed", dynamicTotal > 0);
			} else {
				CustomAssertions.assertEquals("Count is not correctly displayed", Integer.parseInt(total),
						dynamicTotal);
			}
		}
	}

	private void processCheckboxData(List<Map<String, String>> data, Set<String> graphElementSetExpected,
			boolean isGreaterThanEqual) {
		if (checkIfElementisLocated(AnalyticsPageElements.CHECKBOX.locate()) && data.size() > 1) {
			data.forEach(row -> {
				String parameter = row.get("Parameter");
				String value = row.get("Value");
				List<WebElement> elements = findElements(AnalyticsPageElements.CHECKBOX.locate());
				elements.forEach(element -> {
					if (!element.getAttribute("title").equalsIgnoreCase(parameter)) {
						element.click();
					}
				});
				extractGraphElement(findElement(AnalyticsPageElements.BAR_ELEMENTS.locate()), graphElementSetExpected);
				if (!isGreaterThanEqual) {
					CustomAssertions.assertTrue(parameter + " Element not present" + " with value: " + value,
							graphElementSetExpected.contains(parameter + ": " + value));
				} else {
					for (String element : graphElementSetExpected) {
						if (element.contains(row.get("Parameter"))) {
							CustomAssertions.assertTrue("Value is not correct for parameter: " + parameter,
									Integer.parseInt(element.split(": ")[1]) >= Integer.parseInt(value));
							break;
						}
					}
				}
				click(dynamicXpathLocator(CommonPageElements.GENERIC_LABEL, "All"));
			});
		} else {
			data.forEach(row -> {
				String parameter = row.get("Parameter");
				String value = row.get("Value");
				graphElementSetExpected.add(parameter + ": " + value);
			});
		}
	}

	private void valueGreaterEqualEventCheck(Set<String> graphElementSetExpected, List<Map<String, String>> data) {
		data.forEach(row -> {
			for (String element : graphElementSetExpected) {
				if (element.contains(row.get("Parameter"))) {
					CustomAssertions.assertTrue("Value is not correct",
							Integer.parseInt(element.split(": ")[1]) >= Integer.parseInt(row.get("Value")));
					break;
				}
			}
		});
	}

	public void extractGraphElement(WebElement element, Set<String> graphElementSetActual) {
		mouseOverElement(AnalyticsPageElements.CHART_BACKGROUND.locate());
		waitUntilAttributeContains(AnalyticsPageElements.BAR_ELEMENTS.locate(), "transform", "scale(1 1)");
		mouseOverElement(element);
		String tooltipText = checkIfElementisLocated(AnalyticsPageElements.TOOLTIP_TEXT_OVERBY.locate())
				? fetchText(AnalyticsPageElements.TOOLTIP_TEXT_OVERBY.locate())
				: String.join("\n", fetchText(AnalyticsPageElements.TOOLTIP_TEXT.locate()).split("\n"));
		graphElementSetActual.add(tooltipText.contains("●") ? tooltipText.substring(tooltipText.indexOf("●") + 1).trim()
				: tooltipText.split("\n")[0] + ": " + tooltipText.split("\n")[tooltipText.split("\n").length - 1]);
	}

	private void validateDay(String expectedDay) {
		CustomAssertions.assertEquals(expectedDay + "Date is not correctly displayed", expectedDay,
				fetchText(AnalyticsPageElements.X_AXIS_DATE.locate()));
	}

	private String getDate(String value) {
		LocalDate currentDate = LocalDate.now();
		String currentMonth = currentDate.getMonth().getDisplayName(TextStyle.SHORT, Locale.ENGLISH);
		String currentYear = String.valueOf(currentDate.getYear()).substring(2);

		switch (value.toLowerCase()) {
		case "months":
			return String.format("%s '%s", currentMonth, currentYear);
		case "days":
			return String.format("%d %s '%s", currentDate.getDayOfMonth(), currentMonth, currentYear);
		case "weeks":
			LocalDate startOfWeek = currentDate.with(WeekFields.of(Locale.getDefault()).dayOfWeek(), 1);
			LocalDate endOfWeek = startOfWeek.plusDays(6);
			return String.format("%d %s - %d %s", startOfWeek.getDayOfMonth(),
					startOfWeek.getMonth().getDisplayName(TextStyle.SHORT, Locale.ENGLISH), endOfWeek.getDayOfMonth(),
					endOfWeek.getMonth().getDisplayName(TextStyle.SHORT, Locale.ENGLISH));
		case "months of year":
			return currentMonth;
		case "last 7 days":
			LocalDate lastSevenDays = currentDate.minusDays(6);
			return String.format("%d %s '%s", lastSevenDays.getDayOfMonth(),
					lastSevenDays.getMonth().getDisplayName(TextStyle.SHORT, Locale.ENGLISH), currentYear);
		default:
			return value;
		}
	}

	public void verifyTableDataElements(String showType, String operator, List<Map<String, String>> expectedValues) {
		int valueIndex = -1;
		String hourOfDay = null;
		for (Map<String, String> expectedValue : expectedValues) {
			String parameter = expectedValue.get("Parameter");
			String value = expectedValue.get("Value");
			if (parameter.equalsIgnoreCase("months") || parameter.equalsIgnoreCase("days")
					|| parameter.equalsIgnoreCase("weeks") || parameter.equalsIgnoreCase("months of year")) {
				value = getDate(parameter);
			}
			if (parameter.equalsIgnoreCase("Hours of Day")) {
				hourOfDay = parameter;
				List<WebElement> elements = findElements(
						dynamicXpathLocator(AnalyticsPageElements.DAY_TYPE_ELEMENTS, "1"));
				for (int i = 0; i < elements.size(); i++) {
					WebElement element = elements.get(i);
					if (element.getText().equalsIgnoreCase(value)) {
						valueIndex = i + 1;
						break;
					}
				}
			}
			List<WebElement> elements = findElements(CommonPageElements.TABLE_HEADER.locate());
			int headIndex = -1;
			for (int i = 0; i < elements.size(); i++) {
				WebElement element = elements.get(i);
				if (element.getText().equalsIgnoreCase(parameter)) {
					headIndex = i + 1;
					break;
				}
			}
			String actualValue;
			if (headIndex != -1) {
				actualValue = (hourOfDay != null)
						? fetchText(dynamicXpathLocator(AnalyticsPageElements.TABLE_BODY_ELEMENT,
								Integer.toString(headIndex), Integer.toString(valueIndex)))
						: fetchText(dynamicXpathLocator(AnalyticsPageElements.TABLE_BODY_ELEMENT,
								Integer.toString(headIndex), "1"));
				switch (operator) {
				case "equal to":
					CustomAssertions.assertEquals("Expected value condition not satisfied for: " + value, value,
							actualValue);
					break;
				case "greater than":
					if (headIndex == 1)
						CustomAssertions.assertEquals("Expected value condition not satisfied for: " + value, value,
								actualValue);
					else
						CustomAssertions.assertTrue("Expected value condition not satisfied for: " + value,
								Integer.parseInt(actualValue) >= Integer.parseInt(value));
					break;
				}
			} else if (parameter.equalsIgnoreCase("Total") && showType.equalsIgnoreCase("Uniques")) {
				continue;
			} else {
				CustomAssertions.assertTrue("Expected header not present in table: " + parameter, headIndex != -1);
			}
		}
	}

	public void addFilter(List<List<String>> list) {
		waitForElementToBeVisible(AnalyticsPageElements.CHART_BACKGROUND.locate());
		click(AdditionalPageElements.FILTER.locate());
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Event"));
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Segment"));
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "USER ATTRIBUTE FILTERS"));
		for (String eventName : multiEvents) {
			waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, eventName));
		}
		if (multiEvents.length > 1) {
			waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "COMMON FILTER FOR EVENTS"));
			waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "INDIVIDUAL EVENT FILTER"));
		}
		for (List<String> parameter : list) {
			click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, parameter.get(0)));
			if (parameter.get(0).equalsIgnoreCase("Event")) {
				if (!parameter.get(2).equalsIgnoreCase("Add Filter")) {
					click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, parameter.get(1)));
					click(dynamicXpathLocator(AnalyticsPageElements.FILTER_DROPDOWN_COL, parameter.get(1), "-"));
					click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, parameter.get(2)));
					scrollIntoView(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, parameter.get(1)));
					click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Select..."));
					click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, parameter.get(3)));
					click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Select..."));
					click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, parameter.get(4)));
				} else {
					click(dynamicXpathLocator(AnalyticsPageElements.FILTER_DROPDOWN_COL, parameter.get(1),
							parameter.get(2)));
					click(dynamicXpathLocator(AnalyticsPageElements.FILTER_DROPDOWN_COL, parameter.get(1),
							parameter.get(4)));
					click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, parameter.get(1)));
				}
			} else {
				handleIncludeExcludeParameter(parameter);
				if (list.indexOf(parameter) < list.size() - 1) {
					boolean includeCheck = parameter.get(1).contains("Include");
					String includeExcludeLocator = includeCheck ? "Include Segments" : "Exclude Segments";
					click(dynamicXpathLocator(CampaignsPageElements.INCLUDE_EXCLUDE, includeExcludeLocator,
							"Add Segment"));
				}
			}
		}
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "APPLY"));
	}

	private void handleIncludeExcludeParameter(List<String> parameter) {
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Include Segments"));
		scrollIntoView(dynamicXpathLocator(CampaignsPageElements.INCLUDE_EXCLUDE, parameter.get(1), "-"));
		waitUntilAttributeDoesNotContain(
				dynamicXpathLocator(CampaignsPageElements.INCLUDE_EXCLUDE_DROPDOWN, parameter.get(1)), "class",
				"disabled");
		click(dynamicXpathLocator(CampaignsPageElements.INCLUDE_EXCLUDE, parameter.get(1), "-"));
		sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Search"), parameter.get(2));
		click(dynamicXpathLocator(CampaignsPageElements.SELECT_FROM_DD, parameter.get(2)));
	}

	public void resetFilter(String tabName) {
		waitForElementToBeVisible(AnalyticsPageElements.CHART_BACKGROUND.locate());
		click(AdditionalPageElements.FILTER.locate());
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, tabName));
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "RESET"));
		if (tabName.equalsIgnoreCase("Event")) {
			CustomAssertions.assertTrue("Reset is failed for: " + tabName,
					findElements(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "1")).size() == 0);
			click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Segment"));
			CustomAssertions.assertTrue("Reset should not have been done for: Segment",
					findElements(dynamicXpathLocator(CommonPageElements.MODULE, "-")).size() == 0);
		} else {
			CustomAssertions.assertTrue("Reset is failed for: " + tabName,
					findElements(dynamicXpathLocator(CommonPageElements.MODULE, "-")).size() == 2);
			click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Event"));
			CustomAssertions.assertTrue("Reset should not have been done for: Event",
					findElements(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "1")).size() > 0);
		}
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "APPLY"));
	}

	public void commonEventValidation(List<List<String>> list) {
		waitForElementToBeVisible(AnalyticsPageElements.CHART_BACKGROUND.locate());
		click(AdditionalPageElements.FILTER.locate());
		for (List<String> parameter : list) {
			click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, parameter.get(0)));
			click(dynamicXpathLocator(AnalyticsPageElements.FILTER_DROPDOWN_COL, parameter.get(0), "-"));
			sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Search"), parameter.get(1));
			CustomAssertions.assertEquals("Event attribute is present", "No attributes found",
					fetchText(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, "found")));
		}
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "APPLY"));
	}

	public void bellIconGreyOutCheck(String tooltipText, String validationText) {
		waitForElementToBeVisible(AnalyticsPageElements.CHART_BACKGROUND.locate());
		CustomAssertions.assertTrue("Alert option is not greyed out",
				fetchAttributeValue(AnalyticsPageElements.CUSTOM_ALERT.locate(), "class").contains("is-disabled"));
		click(AnalyticsPageElements.CUSTOM_ALERT.locate());
		mouseOverElement(AnalyticsPageElements.CUSTOM_ALERT.locate());
		CustomAssertions.assertEquals("Alert text do not match expected text", tooltipText,
				fetchText(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, validationText)));
	}

	public void createAlert(String alertName, DataTable dataTable) {
		click(AnalyticsPageElements.CUSTOM_ALERT.locate());
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "ALERT NAME"));
		sendKeys(SMSCampaignPageElements.CAMPAIGN_NAME_TEXTFIELD.locate(), alertName);
		List<List<String>> choices = dataTable.asLists(String.class);
		for (List<String> parameter : choices) {
			if (parameter.get(0).equalsIgnoreCase("Send Alert")) {
				click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Select..."));
				click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, parameter.get(1)));
				click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Select..."));
				click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, parameter.get(2)));
			} else {
				click(AnalyticsPageElements.FILTER_ICON.locate());
				for (String value : parameter)
					CustomAssertions.assertEquals("Event filter is not applied", value,
							fetchText(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, value)));
				click(dynamicXpathLocator(CommonPageElements.BUTTON, "APPLY"));
			}
		}
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "Create Alert"));
		commPage.verifyToastMessage(NotificationToastMessages.ALERT_CREATED.message());
	}

	public void verifySelectedEvents() {
		for (String event : multiEvents) {
			clearFieldAndSendkeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Search"), event);
			CustomAssertions.assertTrue(event + " is not present",
					checkIfElementisLocated(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, event)));
		}
	}

	public void removeEvent(String eventName) {
		waitForElementToBeVisible(AnalyticsPageElements.CHART_BACKGROUND.locate());
		click(AnalyticsPageElements.EVENT_DROPDOWN.locate());
		clearFieldAndSendkeys(AnalyticsPageElements.SEARCH_EVENT.locate(), eventName);
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, eventName));
		click(CommonPageElements.SYNC_BTN.locate());
	}

	public void commonFilterRetention(String eventName, String attribute) {
		waitForElementToBeVisible(AnalyticsPageElements.CHART_BACKGROUND.locate());
		click(AdditionalPageElements.FILTER.locate());
		CustomAssertions.assertTrue(eventName + " is present",
				!checkIfElementisLocated(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, eventName)));
		CustomAssertions.assertTrue("Common filter has been reset",
				checkIfElementisLocated(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "1")));
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Common Filter"));
		CustomAssertions.assertTrue(attribute + " is not present",
				checkIfElementisLocated(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, attribute)));
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "APPLY"));
	}

	public void deleteAlert(String alertName, String toastMsg) {
		waitUntilAttributeDoesNotContain(SegmentsPageElements.CARD_CONTENT.locate(), "class", "loader");
		click(dynamicXpathLocator(SegmentsPageElements.OPEN_POP_OVER, alertName));
		click(SegmentsPageElements.POP_OVER_DELETE.locate());
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "Delete"));
		verifyToastMessage(toastMsg);
	}

	public void pinDashboard(String dashboardStatus, String cardName, String dashboardName) {
		waitForElementToBeVisible(AnalyticsPageElements.CHART_BACKGROUND.locate());
		click(dynamicXpathLocator(CommonPageElements.GENERIC_CLASS_CONTAINS, "pin"));
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "card name"));
		sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Card Name"), cardName);
		if (dashboardStatus.equalsIgnoreCase("new")) {
			click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Create a new dashboard"));
			sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Dashboard Name"), dashboardName);
		} else {
			click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Select an Option"));
			sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Search"), dashboardName);
			click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, dashboardName));
		}
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "pin to Dashboard"));
		commPage.verifyToastMessage(NotificationToastMessages.PIN_DASHBOARD.message());
	}

	public void cardOpenEvent(String cardName) {
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, cardName));
		waitForElementToBeVisible(AnalyticsPageElements.CHART_BACKGROUND.locate());
		CustomAssertions.assertTrue("Show filter is not present",
				checkIfElementisLocated(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "SHOW")));
	}

	public void clickOption(String option) {
		waitForElementToBeVisible(AnalyticsPageElements.CHART_BACKGROUND.locate());
		mouseOverElement(AnalyticsPageElements.CHART_BACKGROUND.locate());
		waitUntilAttributeContains(AnalyticsPageElements.BAR_ELEMENTS.locate(), "transform", "scale(1 1)");
		List<WebElement> elements = findElements(AnalyticsPageElements.LINE_BAR_POINTS.locate());
		for (WebElement element : elements) {
			if (element.getAttribute("height") != null && !("0".equals(element.getAttribute("height")))) {
				actions.click(element).build().perform();
				break;
			} else if (element.getAttribute("d") != null
					&& !(element.getAttribute("d").contains("353.99600000066664"))) {
				actions.click(element).build().perform();
				break;
			}
		}
		click(option.contains("zoom") ? dynamicXpathLocator(CommonPageElements.GENERIC_CLASS_CONTAINS, option)
				: dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, option));
	}

	public void listModalPageDetails(String listType, String timePeriod, String listName,
			Map<String, String> expectedValues) {
		CustomAssertions.assertTrue("Counts column is not present",
				checkIfElementisLocated(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "counts")));
		CustomAssertions.assertTrue("Occurences Note is not present at left bottom of list creation page",
				checkIfElementisLocated(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT,
						"NOTE: The count mentioned in the table above are the occurrences of the event.")));
		sendKeys(dynamicXpathLocator(CommonPageElements.LABEL_TEXT_FIELD, "NAME"), listName);
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, listType));
		if (listType.equalsIgnoreCase("Refreshing")) {
			CustomAssertions.assertTrue(" Blank list on next refresh message is not present",
					checkIfElementisLocated(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT,
							"Static data points might give you a blank list on next refresh")));
			waitUntilAttributeDoesNotContain(AnalyticsPageElements.LAST_INPUT_BOX.locate(), "class", "disabled");
			sendKeys(AnalyticsPageElements.LAST_INPUT_BOX.locate(), timePeriod.split(" ")[1]);
			segPage.refreshFrequency(expectedValues);
		}
	}

	public void validateListNameCharacterLimit(String listNameLength, String maxListNameLength) {
		StringBuilder listName = new StringBuilder(Integer.parseInt(listNameLength));
		String characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		Random random = new Random();
		for (int i = 0; i < Integer.parseInt(listNameLength); i++) {
			char randomChar = characters.charAt(random.nextInt(characters.length()));
			listName.append(randomChar);
		}
		sendKeys(dynamicXpathLocator(CommonPageElements.LABEL_TEXT_FIELD, "NAME"), listName.toString());
		CustomAssertions.assertTrue("Character limit is not set for list name length more than 255 characters",
				checkIfElementisLocated(dynamicXpathLocator(SegmentsPageElements.TOOLTIP_BUTTON,
						"List name should only consist 255 characters.")));
	}

	public void validateToAndFromDate(String dateType) {
		String expectedDate = dateType.equalsIgnoreCase("yesterday")
				? String.format("FROM %s TO %s", getDate(dateType, "dd/MMM/yyyy"), getDate(dateType, "dd/MMM/yyyy"))
						.toLowerCase()
				: String.format("FROM %s TO %s", getDate(dateType, "dd/MMM/yyyy"), getDate("days", "dd/MMM/yyyy"))
						.toLowerCase();
		String actualDate = fetchText(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, "FROM"))
				.toLowerCase();
		CustomAssertions.assertEquals("Time format is not correct", expectedDate, actualDate);
	}

	public void clickButton(String button) {
		click(dynamicXpathLocator(CommonPageElements.BUTTON, button));
		waitForElementToBeInvisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "NAME"));
	}

	public void verifyTableDataForExportLists(String operator, List<Map<String, String>> expectedValues) {
		for (Map<String, String> expectedValue : expectedValues) {
			String value = expectedValue.get("Value");
			String actualValue = fetchText(
					dynamicXpathLocator(AnalyticsPageElements.EVENT_COUNT, expectedValue.get("Parameter")));
			switch (operator) {
			case "equal to":
				CustomAssertions.assertEquals("Expected value condition not satisfied for: " + value,
						Integer.parseInt(value), Integer.parseInt(actualValue));
				break;
			case "greater than":
				CustomAssertions.assertTrue("Expected value condition not satisfied for: " + value,
						Integer.parseInt(actualValue) >= Integer.parseInt(value));
				break;
			}
		}
	}

	public void validateDataPoints(String event) {
		CustomAssertions.assertTrue("One Data point is not selected by default",
				checkIfElementisLocated(AnalyticsPageElements.DATA_POINTS_CHECKBOX_SELECTED.locate()));
		List<WebElement> elements = findElements(dynamicXpathLocator(AnalyticsPageElements.DAY_TYPE_ELEMENTS, "1"));
		CustomAssertions.assertEquals("Expected elements size was 7 but found: " + elements.size(), 7, elements.size());
		for (int i = 0; i < elements.size(); i++) {
			String expectedDataPoint = event + "_"
					+ getDate("last " + Integer.toString(elements.size() - i) + " days", "d MMM ''yy");
			CustomAssertions.assertEquals("Expected data point did not match: " + elements.get(i).getText(),
					expectedDataPoint, elements.get(i).getText());
		}
	}

	public void searchDataPoint(String eventName) {
		String expectedEventDataPoint = eventName + "_" + getDate("days", "d MMM ''yy");
		sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Search"), expectedEventDataPoint);
		CustomAssertions.assertEquals("Expected data point did not match: " + expectedEventDataPoint,
				expectedEventDataPoint, findElement(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS,
						expectedEventDataPoint.split("'")[0])).getText());
		CustomAssertions.assertTrue("Expected event data point did not occur: " + expectedEventDataPoint,
				findElements(dynamicXpathLocator(AnalyticsPageElements.DAY_TYPE_ELEMENTS, "1")).size() == 1);
	}

	public void verifyDataPointsCheckbox(String option) {
		CustomAssertions.assertTrue("One Data point is not selected by default",
				findElements(AnalyticsPageElements.DATA_POINTS_CHECKBOX_SELECTED.locate()).size() == 1);
		click(AnalyticsPageElements.DATA_POINT_POP_OVER.locate());
		CustomAssertions.assertTrue("Select All is not present",
				checkIfElementisLocated(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Select All")));
		CustomAssertions.assertTrue("View Selected is not present",
				checkIfElementisLocated(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "View Selected")));
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, option));
		if (option.equalsIgnoreCase("Select All")) {
			CustomAssertions.assertTrue("All data points are not selected",
					findElements(AnalyticsPageElements.DATA_POINTS_CHECKBOX_SELECTED.locate()).size() == 8);
			click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, option));
			CustomAssertions.assertTrue("More than one data point is selected",
					findElements(AnalyticsPageElements.DATA_POINTS_CHECKBOX_SELECTED.locate()).size() == 1);
		} else {
			CustomAssertions.assertTrue("Selected data points are not visible",
					findElements(AnalyticsPageElements.DATA_POINTS_CHECKBOX_SELECTED.locate()).size() == 1);
			click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, option));
			CustomAssertions.assertTrue("All data points are not shown",
					findElements(AnalyticsPageElements.DATA_POINTS_CHECKBOX_SELECTED.locate()).size() == 1);
			CustomAssertions.assertTrue("All data points are not shown",
					findElements(AnalyticsPageElements.DATA_POINTS_CHECKBOX.locate()).size() == 8);
		}
	}

	public void selectDataPoints(String count) {
		List<WebElement> elements = findElements(AnalyticsPageElements.DATA_POINTS_CHECKBOX.locate());
		int totalSelectedPoints = 1;
		for (int i = 1; i <= Integer.parseInt(count); i++) {
			if (!elements.get(i).getAttribute("class").contains("is-checked")) {
				totalSelectedPoints++;
				actions.click(elements.get(i)).build().perform();
			}
		}
		CustomAssertions.assertTrue(count + " data points are not selected",
				findElements(AnalyticsPageElements.DATA_POINTS_CHECKBOX_SELECTED.locate())
						.size() == totalSelectedPoints);
	}

	public void verifySeriesPresence(String value) {
		CustomAssertions.assertTrue("One Data point is not selected by default",
				findElements(AnalyticsPageElements.DATA_POINTS_CHECKBOX_SELECTED.locate()).size() == 1);
		for (String event : multiEvents) {
			String expectedEventName = (multiEvents.length > 1 || value.equalsIgnoreCase("-")) ? event
					: event + "_" + value;
			CustomAssertions.assertEquals(
					"Expected event name: " + expectedEventName + " did not match actual event name", expectedEventName,
					fetchText(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, expectedEventName)));
		}
	}

	public String getDate(String value, String format) {
		LocalDate currentDate = LocalDate.now();
		String currentMonth = currentDate.getMonth().getDisplayName(TextStyle.SHORT, Locale.ENGLISH);
		String currentYear = String.valueOf(currentDate.getYear()).substring(2);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
		String[] parts = value.toLowerCase().split(" ");
		switch (parts[0]) {
		case "months":
			return currentMonth + " " + currentYear;
		case "days":
			return currentDate.format(formatter);
		case "weeks":
			LocalDate startOfWeek = currentDate.with(WeekFields.of(Locale.getDefault()).dayOfWeek(), 1);
			LocalDate endOfWeek = startOfWeek.plusDays(6);
			return startOfWeek.format(formatter) + " - " + endOfWeek.format(formatter);
		case "today":
			return currentDate.format(formatter);
		case "yesterday":
			return currentDate.minusDays(1).format(formatter);
		case "last":
			return currentDate.minusDays(Integer.parseInt(value.split(" ")[1]) - 1).format(formatter);
		case "next":
			return currentDate.plusDays(Integer.parseInt(value.split(" ")[1]) - 1).format(formatter);
		default:
			return value;
		}
	}

	public void verifySegmentPresence(List<String> list) {
		waitForElementToBeVisible(AnalyticsPageElements.CHART_BACKGROUND.locate());
		click(AdditionalPageElements.FILTER.locate());
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Segment"));
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Segment"));
		handleIncludeExcludeParameter(list);
		click(dynamicXpathLocator(CampaignsPageElements.INCLUDE_EXCLUDE,
				list.get(1).contains("Include") ? "Exclude Segments" : "Include Segments", "-"));
		sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Search"), list.get(2));
		CustomAssertions.assertTrue(list.get(2) + " is present in both include and exclude section",
				checkIfElementisLocated(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "No Result Found")));
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "RESET"));
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "APPLY"));
	}

	public void verifyMaxOrMinTimePeriod(String period, String listName) {
		sendKeys(dynamicXpathLocator(CommonPageElements.LABEL_TEXT_FIELD, "NAME"), listName);
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Refreshing"));
		waitUntilAttributeDoesNotContain(AnalyticsPageElements.LAST_INPUT_BOX.locate(), "class", "disabled");
		sendKeys(AnalyticsPageElements.LAST_INPUT_BOX.locate(), period);
		String attrValue = fetchAttributeValue(AnalyticsPageElements.LAST_INPUT_BOX.locate(), "value");
		CustomAssertions.assertTrue(
				period.equalsIgnoreCase("0") ? "Min limit for days is not set" : "Max limit for days is not set",
				period.equalsIgnoreCase("0") ? attrValue.equalsIgnoreCase("") : !attrValue.equalsIgnoreCase(period));
	}
}