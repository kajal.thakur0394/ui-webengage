package com.webengage.ui_automation.pages;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.webengage.ui_automation.utils.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.webengage.ui_automation.elements.CommonPageElements;
import com.webengage.ui_automation.elements.OnsiteNotificationPageElements;
import com.webengage.ui_automation.elements.OnsiteSurveysPageElements;

public class OnsiteNotificationPage extends SeleniumExtendedUtility {

	CommonPage comPage = new CommonPage();
	CampaignsPage campPage = new CampaignsPage();
	SegmentsPage segmentsPage = new SegmentsPage();
	RuntimeUtility runTimeUtility = new RuntimeUtility();
	SetupUtility setupUtility = new SetupUtility();
	APIUtility apiUtility = new APIUtility();
	ReflectionsUtility refl = new ReflectionsUtility();
	public String notificationName = null;
	private String createdSegmentId;
	private String createdSegmentName;
	private String attachedSegmentId;

	/**
	 * @param{campaignName} This method will locate the 'Search input' field and
	 *                      enters the campaign name inside the input field.
	 */
	public void searchBar(String campaignName) {
		clearTextField(CommonPageElements.SEARCH.locate());
		sendKeys(CommonPageElements.SEARCH.locate(), campaignName + Keys.RETURN);
	}

	/**
	 * @param{campaignName},{campaignType} This method will verify that
	 *                                     'CampaignName' and 'CampaignType'
	 *                                     searched matches with the expected
	 *                                     'CampaignName' and 'layoutType'.
	 */
	public void verifyCampaignNameandType(String campaignName, String layoutType) {
		CustomAssertions.assertEquals("CampaignName do not match:", campaignName,
				fetchText(OnsiteNotificationPageElements.NAME_TEXT.locate()));
		CustomAssertions.assertTrue("LayoutType do not match:",
				fetchText(OnsiteNotificationPageElements.LAYOUT_TEXT.locate()).contains(layoutType));
		clearTextField(CommonPageElements.SEARCH.locate());
		click(OnsiteNotificationPageElements.SEARCH_BAR.locate());
	}

	/**
	 * This method will click on 'Create Notification' button and will select the
	 * onsite-notifcation type.
	 */
	public void createNotification(String name) {
		click(OnsiteNotificationPageElements.CREATE_NOTIFICATION_BTN.locate());
		By locator = name.equalsIgnoreCase(" Create Your Own ") ? OnsiteNotificationPageElements.CREATE_OWN.locate()
				: OnsiteNotificationPageElements.CREATE_GALLERY.locate();
		click(locator);
	}

	/**
	 * This method will select the desired template type of onsite-notification.
	 */
	public void templateName(String name, String notificationType) {
		By locator = notificationType.equalsIgnoreCase("Choose from Gallery")
				? dynamicXpathLocator(OnsiteNotificationPageElements.GALLERY_TEMPLATE, name)
				: dynamicXpathLocator(CommonPageElements.GENERIC_DIVTEXT, name);
		click(locator);
	}

	public void verifyOnsiteNotificationDetails(JSONObject template) {
		JSONObject expectedData = (JSONObject) template.get("ExpectedData");
		String title = expectedData.get("Title").toString();
		By iframe = OnsiteNotificationPageElements.ONSITE_NOTIFICATION_IFRAME.locate();
		CampaignsPage.setCampaignName(title);
		Log.debug("Expected title from JSON: " + title);
		try {
			TimeUnit.SECONDS.sleep(5);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Log.debug("Waiting for SDKPageResponse..");
		iframe = expectedData.containsKey("layoutID")
				? dynamicXpathLocator(OnsiteNotificationPageElements.ONSITE_IFRAME_LAYOUTID,
						expectedData.get("layoutID").toString())
				: dynamicXpathLocator(OnsiteNotificationPageElements.ONSITE_IFRAME_LAYOUTNAME,
						expectedData.get("layoutName").toString());
		comPage.waitForSDKPageResponse(iframe, System.currentTimeMillis(), "");
		switchToiFrame(iframe);
		if (title.contains("Box")) {
			Log.debug("Actual Title from DOM: " + fetchText(OnsiteNotificationPageElements.ACTUAL_TITLE.locate()));
			CustomAssertions.assertEquals("Notification title does not match", title,
					fetchText(OnsiteNotificationPageElements.ACTUAL_TITLE.locate()));
			click(OnsiteNotificationPageElements.ONSITE_NOTIFICATION_CLOSE.locate());
		} else if (title.contains("Header") || title.contains("Footer") || title.contains("Banner")) {
			CustomAssertions.assertTrue("description Validation Failed ",
					fetchText(dynamicXpathLocator(CommonPageElements.GENERIC_CLASS, "description"))
							.contains(expectedData.get("description").toString()));
			if (title.contains("Banner")) {
				CustomAssertions
						.assertTrue("Icon Validation Failed",
								fetchAttributeValue(dynamicXpathLocator(CommonPageElements.GENERIC_CLASS_CONTAINS,
										"image-container"), "src")
												.contains(expectedData.get("BannerImage").toString()));
				click(OnsiteNotificationPageElements.ONSITE_NOTIFICATION_CLOSE.locate());
			} else {
				CustomAssertions
						.assertTrue("Icon Validation Failed",
								fetchAttributeValue(
										dynamicXpathLocator(CommonPageElements.GENERIC_CLASS_CONTAINS, "img"), "src")
												.contains(expectedData.get("IconName").toString()));
				WebElement linktext = findElement(
						dynamicXpathLocator(CommonPageElements.GENERIC_CLASS_CONTAINS, "button inline"));
				CustomAssertions.assertEquals("LinkText Validation Failed", linktext.getText(),
						(expectedData.get("linktext").toString()));
				CustomAssertions.assertEquals("LinkText Validation Failed", linktext.getAttribute("data-action-link"),
						(expectedData.get("link").toString()));
				if (title.contains("imageURL")) {
					try {
						JavascriptExecutor js = ((JavascriptExecutor) driver);
						WebElement element = (WebElement) js
								.executeScript("return document.querySelector('[class*=\"wrapper\"]')");
						String backgroundImageValue = (String) js
								.executeScript("return getComputedStyle(arguments[0]).backgroundImage", element);
						CustomAssertions.assertTrue("BackgroundImage Validation Failed",
								backgroundImageValue.contains(expectedData.get("BackgroundImage").toString()));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				click(OnsiteNotificationPageElements.ONSITE_NOTIFICATION_CLOSE.locate());
			}
		}
		Log.debug("validation passed hence switching to parent window..");
		switchToDefaultWindow();
		Log.debug("switched to my parent window successfully");

	}

	public void deleteOnsiteNotification(JSONObject template) {
		JSONObject expectedTitle1 = (JSONObject) template.get("ExpectedData");
		String title = expectedTitle1.get("Title").toString();
		sendKeys(CommonPageElements.SEARCH.locate(), title);
		click(OnsiteNotificationPageElements.MORE_TAB.locate());
		click(OnsiteNotificationPageElements.DELETE_NOTIFICATION.locate());
		acceptAlert();
	}
	
	public void enterNotificationCampaignName(String notificationName) {
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, "Specific Options"));
		campPage.saveCampaignID(false);
		clearTextField(OnsiteNotificationPageElements.TITLE.locate());
		sendKeys(OnsiteNotificationPageElements.TITLE.locate(), notificationName);
		click(dynamicXpathLocator(CommonPageElements.GENERIC_LABEL, "Save and Proceed"));
	}

	public void activateNotificationCampaign() {
		waitForElementToBeVisible(
				dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, "Whom to show to? (Traffic Segment)"));
		campPage.saveCampaignID(false);
		waitForElementToBeInvisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Name your segment"));
		scrollIntoView(dynamicXpathLocator(CommonPageElements.GENERIC_LABEL, "Save and Proceed"));
		click(dynamicXpathLocator(CommonPageElements.GENERIC_LABEL, "Save and Proceed"));
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, "Skip Notification"));
		scrollIntoView(dynamicXpathLocator(CommonPageElements.GENERIC_LABEL, "Save & Activate"));
		click(dynamicXpathLocator(CommonPageElements.GENERIC_LABEL, "Save & Activate"));
	}

	public void saveUpdateCampaignName() {
		waitForElementToBeVisible(
				dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, "Whom to show to? (Traffic Segment)"));
		click(dynamicXpathLocator(CommonPageElements.GENERIC_LABEL, "Save and Proceed"));
		waitForElementToBeVisible(
				dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, "Setup Conversion tracking"));
		if (checkIfElementisLocated(dynamicXpathLocator(CommonPageElements.GENERIC_LABEL, "Save & Activate"))) {
			click(dynamicXpathLocator(CommonPageElements.GENERIC_LABEL, "Save & Activate"));
		}
		click(dynamicXpathLocator(CommonPageElements.GENERIC_LABEL, "Save Changes"));
	}

	public void setNotificationCampaignTag(String notificationCampaignName, String tagName) {
		click(dynamicXpathLocator(OnsiteNotificationPageElements.NOTIFICATION_CAMPAIGN_NEW_TAG,
				notificationCampaignName));
		CampaignsPage.setSystemDateTime(tagName.concat("-".concat(CampaignsPage.systemDateTime())));
		sendKeys(dynamicXpathLocator(OnsiteNotificationPageElements.NOTIFICATION_CAMPAIGN_ADD_NEW_TAG,
				notificationCampaignName), (CampaignsPage.getSystemDateTime()) + Keys.ENTER);
		CampaignsPage.setSystemDateTimeAvailable(true);
	}

	public void deleteNotificationCampaign(String campaignName) {
		click(dynamicXpathLocator(OnsiteNotificationPageElements.NOTIFICATION_CAMPAIGN_MORE_BTN, campaignName));
		click(OnsiteNotificationPageElements.DELETE_NOTIFICATION.locate());
		acceptAlert();
		waitForElementToBeInvisible(dynamicXpathLocator(CommonPageElements.GENERIC_NORMALIZE_SPACE, campaignName));
	}

	public void createNewSegment(String segmentName, List<List<String>> choices) {
		selectDropDown(OnsiteNotificationPageElements.TRAFFIC_SEGMENT_DROPDOWN.locate(), "Create a new segment");
		switchToiFrame(OnsiteNotificationPageElements.SEGMENT_IFRAME.locate());
		sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Segment Name"), segmentName);
		segmentsPage.fillChoicesUserCard(choices);
	}

	public void selectExistingSegment(String userCondition) {
		selectDropDown(OnsiteNotificationPageElements.TRAFFIC_SEGMENT_DROPDOWN.locate(), userCondition);
	}

	public void checkDuplicateCampaign(String onSiteCampaignName) {
		CustomAssertions
				.assertEquals("No duplicate campaign found",
						getSizeOfElements(
								dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, onSiteCampaignName)) > 1,
						true);
	}

	public void createDuplicateOnSiteCampaign(String onSiteCampaignName) {
		click(dynamicXpathLocator(OnsiteNotificationPageElements.NOTIFICATION_CAMPAIGN_MORE_BTN, onSiteCampaignName));
		click(dynamicXpathLocator(OnsiteNotificationPageElements.ONSITE_CAMPAIGN_COPY_BTN, onSiteCampaignName));
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "Paste"));
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, "Notification List"));
	}

	public void checkStatusOfCampaign(String notificationName, String campaignStatus) {
		String notificationStatus = fetchText(
				dynamicXpathLocator(OnsiteNotificationPageElements.ONSITE_CAMPAIGN_STATUS, notificationName))
						.split(":")[1].trim();
		CustomAssertions.assertEquals("status is not matched", notificationStatus, campaignStatus);
	}

	public void changeOnsiteCampaignStatus(String notificationName, String status) {
		click(dynamicXpathLocator(OnsiteNotificationPageElements.NOTIFICATION_CAMPAIGN_MORE_BTN, notificationName));
		click(OnsiteNotificationPageElements.DEACTIVATE_NOTIFICATION.locate());
		waitUntilAttributeContains(
				dynamicXpathLocator(OnsiteNotificationPageElements.ONSITE_CAMPAIGN_STATUS_VALUE, notificationName),
				"title", "Status " + status);
	}

	public void changeOnsiteCampaignName(String oldName, String newName) {
		click(dynamicXpathLocator(OnsiteNotificationPageElements.ONSITE_CAMPAIGN_EDIT_BTN, oldName));
		enterNotificationCampaignName(newName);
		saveUpdateCampaignName();
	}

	public void editOnsiteCampaign(String notificationName) {
		click(dynamicXpathLocator(OnsiteNotificationPageElements.ONSITE_CAMPAIGN_EDIT_BTN, notificationName));
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, " Targeting"));
		waitForElementToBeVisible(
				dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Whom to show to? (Traffic Segment)"));
	}

	public void fetchCreatedSegmentNameIDFromOnSiteCampaign(String apiName) {
		APIUtility.getRuntimeValues().put(ConstantUtils.CAMPAIGNID, CampaignsPage.getCampaignID());
		runTimeUtility.setDynamicRequest(ConstantUtils.CAMPAIGNID, "URL");
		try {
			setupUtility.setApiURI(apiName);
			apiUtility.getRequest(runTimeUtility.modifyAPIURL(setupUtility.getApiURI()));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		this.createdSegmentName = apiUtility.fetchValue("response.data.trafficSegmentDto.name");
		this.createdSegmentId = apiUtility.fetchValue("response.data.trafficSegmentDto.id");
	}

	public void fetchNotificationNameAndAttachedSegmentId(String apiName) {
		APIUtility.getRuntimeValues().put(ConstantUtils.CAMPAIGNNAME, CampaignsPage.getCampaignID());
		runTimeUtility.setDynamicRequest(ConstantUtils.CAMPAIGNNAME, "URL");
		try {
			setupUtility.setApiURI(apiName);
			apiUtility.getRequest(runTimeUtility.modifyAPIURL(setupUtility.getApiURI()));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		this.attachedSegmentId = apiUtility.fetchValue("response.data.segmentId");
		this.notificationName = apiUtility.fetchValue("response.data.title");
	}

	public void verifySegmentAndOnsiteCampaignId(String segmentName, String notificationCampaignName) {
		fetchCreatedSegmentNameIDFromOnSiteCampaign("fetchOnSiteSegment");
		fetchNotificationNameAndAttachedSegmentId("fetchOnsiteNotificationCampaign");
		CustomAssertions.assertEquals("Attached and created segment name does not match", createdSegmentName,
				segmentName);
		CustomAssertions.assertEquals("Attached and created segment Id does not match", attachedSegmentId,
				createdSegmentId);
		CustomAssertions.assertEquals("Notification and segment ID does not match", notificationName,
				notificationCampaignName);
	}

	public void verifyTagAttachedToOnsiteNotificationCampaign(String expectedTagName, String campaignName) {
		if (CampaignsPage.isSystemDateTimeAvailable()) {
			CustomAssertions.assertEquals("Tag is not attached to the onsiteNotification Campaign",
					CampaignsPage.getSystemDateTime(),
					fetchText(dynamicXpathLocator(OnsiteNotificationPageElements.NOTIFICATION_CAMPAIGN_TAG_NAME,
							campaignName)));
			CampaignsPage.setSystemDateTimeAvailable(false);
		} else {
			CustomAssertions.assertEquals("Tag is not attached to the onsiteNotification Campaign",
					expectedTagName,
					fetchText(dynamicXpathLocator(OnsiteNotificationPageElements.NOTIFICATION_CAMPAIGN_TAG_NAME,
							campaignName)));
		}
	}

	public void verifyUpdateOnsiteCampaignId() {
		CustomAssertions.assertEquals("Updated campaign id is not matched",
				getCurrentURL().split("/")[getCurrentURL().split("/").length - 2], CampaignsPage.getCampaignID());
	}

	public void checkCampaignType(String campaignType, JSONObject template) {
		CampaignsPage.setCampaignType(campaignType);
		switch (campaignType) {
		case "On-site Surveys":
		case "Off-site Surveys": {
			refl.readAndFillData((JSONArray) template.get("BasicDetails"));
			String surveyCampaignId = campPage.getCurrentURL().split("/")[campPage.getCurrentURL().split("/").length
					- 2];
			CampaignsPage.setCampaignID(surveyCampaignId);
			refl.readAndFillData((JSONArray) template.get("Questionnaire"));
			refl.readAndFillData((JSONArray) template.get("Targeting"));
			try {
				TimeUnit.SECONDS.sleep(5);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			refl.readAndFillData((JSONArray) template.get("Appearance"));
			refl.readAndFillData((JSONArray) template.get("Activate"));
			click(dynamicXpathLocator(OnsiteNotificationPageElements.LIST_PAGE, "surveys"));
			if (campaignType.contains("Off-site")) {
				LinkedHashMap<String, String> offsiteURL = new LinkedHashMap<String, String>();
				offsiteURL.put("offsiteURL", fetchAttributeValue(
						dynamicXpathLocator(OnsiteSurveysPageElements.GET_OFFSITE_URL, surveyCampaignId), "value"));
				RuntimeUtility.setRuntimeURL(offsiteURL);
			}
			break;
		}
		case "On-site Notifications": {
			refl.readAndFillData((JSONArray) template.get("BasicDetails"));
			String campaignId = campPage.getCurrentURL().split("/")[campPage.getCurrentURL().split("/").length - 2];
			CampaignsPage.setCampaignID(campaignId);
			refl.readAndFillData((JSONArray) template.get("Targeting"));
			refl.readAndFillData((JSONArray) template.get("Activate"));
			click(dynamicXpathLocator(OnsiteNotificationPageElements.LIST_PAGE, "notifications"));
			break;
		}
		case "default": {
			break;
		}
		}
	}
}
