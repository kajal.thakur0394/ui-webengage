package com.webengage.ui_automation.pages;

import java.util.concurrent.TimeUnit;

import org.json.simple.JSONObject;
import org.openqa.selenium.By;

import com.webengage.ui_automation.elements.AnalyticsPageElements;
import com.webengage.ui_automation.elements.CommonPageElements;
import com.webengage.ui_automation.elements.OnsiteNotificationPageElements;
import com.webengage.ui_automation.elements.OnsiteSurveysPageElements;
import com.webengage.ui_automation.utils.CustomAssertions;
import com.webengage.ui_automation.utils.Log;
import com.webengage.ui_automation.utils.RuntimeUtility;
import com.webengage.ui_automation.utils.SeleniumExtendedUtility;

public class OnsiteSurveyPage extends SeleniumExtendedUtility {
	APIOperationsPage apiOperationsPage = new APIOperationsPage();
	CommonPage commonPage = new CommonPage();

	public void verifyOnsiteSurveyData(JSONObject template) {
		JSONObject expectedData = (JSONObject) template.get("ExpectedData");
		String title = expectedData.get("Title").toString();
		String questionType = expectedData.get("questionType").toString();
		By submit = dynamicXpathLocator(CommonPageElements.GENERIC_TYPE, "submit");
		CampaignsPage.setCampaignName(title);
		Log.debug("Expected title from JSON: " + title);
		try {
			TimeUnit.SECONDS.sleep(5);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		if (title.contains("Off-site")) {
			switchToDefaultWindow();
			openURLinNewTab(RuntimeUtility.getRuntimeURL().get("offsiteURL"));
		} else {
			Log.debug("Waiting for SDKPageResponse..");
			By iframe = OnsiteSurveysPageElements.SURVEY_IFRAME.locate();
			commonPage.waitForSDKPageResponse(iframe, System.currentTimeMillis(), "");
			switchToiFrame(iframe);
		}
		if (!questionType.equals("Text box (one line)")) {
			CustomAssertions.assertEquals("Incorrect welcome message for survey",
					expectedData.get("welcomMessage").toString(),
					fetchText(OnsiteSurveysPageElements.WELCOME_MESSAGE.locate()).trim());
			click(submit);
		}
		CustomAssertions.assertEquals("Incorrect Question displayed on survey",
				expectedData.get("surveyQuestion").toString(),
				fetchText(OnsiteSurveysPageElements.SURVEY_QUESTION.locate()).trim());

		switch (questionType) {
		case "Text box (one line)":
			clearFieldAndSendkeys(AnalyticsPageElements.INPUT_FUNNEL.locate(),
					expectedData.get("inputReviews").toString());
			break;
		case "Choice of Options":
			click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS,
					expectedData.get("chooseOption").toString()));
			break;
		case "Dropdown menu of options":
			click(dynamicXpathLocator(CommonPageElements.GENERIC_CLASS, "select medium"));
			click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, expectedData.get("chooseOption").toString()));
		}
		click(submit);
		waitForElementToBeInvisible(submit);
		if (!title.contains("Off-site")) {
			CustomAssertions.assertEquals("Incorrect exit message displayed on survey",
					expectedData.get("exitMessage").toString(),
					fetchText(OnsiteSurveysPageElements.SURVEY_QUESTION.locate()).trim());
			click(OnsiteNotificationPageElements.ONSITE_NOTIFICATION_CLOSE.locate());
		}
		Log.debug("validation passed hence switching to parent window..");
		switchToDefaultWindow();
		Log.debug("switched to my parent window successfully");
	}

}
