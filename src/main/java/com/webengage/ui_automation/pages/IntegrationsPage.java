package com.webengage.ui_automation.pages;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.google.gson.Gson;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvException;
import com.webengage.ui_automation.elements.CommonPageElements;
import com.webengage.ui_automation.elements.InlineContentAppPerPageElements;
import com.webengage.ui_automation.elements.IntegrationsPageElements;
import com.webengage.ui_automation.elements.JourneysPageElements;
import com.webengage.ui_automation.utils.APIUtility;
import com.webengage.ui_automation.utils.CustomAssertions;
import com.webengage.ui_automation.utils.Log;
import com.webengage.ui_automation.utils.NotificationToastMessages;
import com.webengage.ui_automation.utils.ReflectionsUtility;
import com.webengage.ui_automation.utils.SeleniumExtendedUtility;
import com.webengage.ui_automation.utils.SetupUtility;

public class IntegrationsPage extends SeleniumExtendedUtility {
	String providerName;
	String configName;
	String fontName;
	ReflectionsUtility refl = new ReflectionsUtility();
	APIUtility apiUtility = new APIUtility();
	APIOperationsPage apiOperationsPage = new APIOperationsPage();

	public String getConfigName() {
		return configName;
	}

	public void setConfigName(String configName) {
		this.configName = configName;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public String getFontName() {
		return fontName;
	}

	public void setFontName(String fontName) {
		this.fontName = fontName;
	}

	public void openIntegrationPage() {
		String attribute = fetchAttributeValue(IntegrationsPageElements.DATA_PLATFORM_LIST.locate(), "class");
		if (attribute.contains("active")) {
			click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Integrations"));
		} else {
			click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Data Platform"));
			click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Integrations"));
		}
	}

	public void checkSpecificStatusAs(String channel, String expectedStatus) {
		String actualStatus = fetchText(dynamicXpathLocator(IntegrationsPageElements.CHANNEL_STATUS, channel));
		CustomAssertions.assertEquals("Channel Status do not match:", expectedStatus, actualStatus);
	}

	public void clickOnConfigure(String channel) {
		click(dynamicXpathLocator(IntegrationsPageElements.CHANNEL_CONFIGURE, channel));
	}

	public void selectServiceProviderandSetName(String chooseProvider, String configName, boolean wsp) {
		click(dynamicXpathLocator(IntegrationsPageElements.SELECT_PROVIDER, chooseProvider));
		if (wsp)
			sendKeys(dynamicXpathLocator(IntegrationsPageElements.FIELDS, "Configuration Name"), configName);
		else
			sendKeys(IntegrationsPageElements.CONFIG_NAME.locate(), configName);
		setProviderName(chooseProvider);
		setConfigName(configName);
	}

	public void fillFields(String expectedValue, List<List<String>> choices, boolean verify) {
		if (verify) {
			String actualValue = fetchText(IntegrationsPageElements.ESP_DD.locate());
			CustomAssertions.assertEquals("Strings do not match:", expectedValue, actualValue);
		}
		for (List<String> choice : choices) {
			String parameter = choice.get(0);
			String value = choice.get(1);
			switch (parameter) {
			case "Template Name":
			case "Template Text":
			case "Button 1 Text":
			case "Footer":
				clearFieldAndSendkeys(dynamicXpathLocator(IntegrationsPageElements.FIELDS, parameter), value);
				break;
			case "Type Of Action":
				click(dynamicXpathLocator(IntegrationsPageElements.FIELDS, "Buttons"));
				click(dynamicXpathLocator(IntegrationsPageElements.DD_FIELD, parameter, "Call To Action"));
				click(dynamicXpathLocator(IntegrationsPageElements.SELECT_FROM_DD, value));
				break;
			case "Key":
			case "Value":
				String[] splitValues = value.split("-");
				By keyValue = dynamicXpathLocator(InlineContentAppPerPageElements.CUSTOM_TEMPLATE_KEY_VALUE,
						splitValues[0], parameter);
				clearFieldAndSendkeys(keyValue, splitValues[1]);
				break;
			case "Upload":
				By resetBtn = dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Reset");
				if(checkIfElementisLocated(resetBtn)) {
					click(resetBtn);
				}
				By uploadButton = dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, parameter);
				waitForElementToBeVisible(uploadButton);
				uploadFile(value, JourneysPageElements.SELECT_FILE_BUTTON.locate());
				waitForElementToBePresent(resetBtn);
				break;
			default:
				sendKeys(dynamicXpathLocator(IntegrationsPageElements.FIELDS, parameter), value);
			}
		}
	}

	public void verifyFields(List<List<String>> choices) throws InterruptedException {
		
		for (List<String> choice : choices) {
			String parameter = choice.get(0);
			String expectedValue = choice.get(1);
			String actualValue="";
			switch (parameter) {
			case "Template Name":
				actualValue = fetchAttributeValue(
						dynamicXpathLocator(IntegrationsPageElements.FIELDS, parameter), "value");
				CustomAssertions.assertEquals(parameter + "mismatch", expectedValue, actualValue);
				break;
			case "Key":
			case "Value":
				String[] splitValues = expectedValue.split("-");
				By keyValue = dynamicXpathLocator(InlineContentAppPerPageElements.CUSTOM_TEMPLATE_KEY_VALUE,
						splitValues[0], parameter);
				actualValue = fetchAttributeValue(keyValue, "value");
				CustomAssertions.assertEquals(parameter + "mismatch", splitValues[1], actualValue);
				break;
			case "Template Image (Optional)":
				actualValue = fetchAttributeValue(
						dynamicXpathLocator(InlineContentAppPerPageElements.CUSTOM_TEMPLATE_IMAGE, parameter), "class");
				CustomAssertions.assertEquals(parameter + "mismatch", expectedValue, actualValue);
				break;
			}
		}
	}

	public void deleteWhatsappTemplate(String templateName, String wspName) {
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Actions"));
		searchWhatsappTemplate(templateName);
		click(dynamicXpathLocator(IntegrationsPageElements.WA_TEMPLATE_POP_OVER, templateName, wspName));
		click(IntegrationsPageElements.POP_OVER_DELETE.locate());
		click(IntegrationsPageElements.DELETE_BTN.locate());
		verifyToastMessage(NotificationToastMessages.WA_TEMPLATE_DELETE.message());
	}
	
	public void selectTemplateMenuAction(String templateName, String templateAction) throws InterruptedException {
		By popOverMenu = dynamicXpathLocator(IntegrationsPageElements.CUSTOM_TEMPLATE_POP_OVER, templateName);
		List<WebElement> pages = findElements(dynamicXpathLocator(IntegrationsPageElements.GET_PAGES_FROM_CARDNAME,"Saved Custom Templates"));
		for (WebElement page: pages) {
			page.click();
			if (checkIfElementisLocated(popOverMenu))
				break;
		}
		click(popOverMenu);
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, templateAction));
	}

	public void deleteCustomTemplate(String templateName) {
		try {
			waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Saved Custom Templates"));
			selectTemplateMenuAction(templateName,"Delete");
			waitForElementToBeVisible(IntegrationsPageElements.DELETE_BTN.locate());
			click(IntegrationsPageElements.DELETE_BTN.locate());
			verifyToastMessage(NotificationToastMessages.CUSTOM_TEMPLATE_DELETE.message());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void searchWhatsappTemplate(String templateName) {
		clearFieldAndSendkeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Search by template name"),
				templateName + Keys.RETURN);
	}

	public void checkList() {
		waitForElementToBeVisible(dynamicXpathLocator(IntegrationsPageElements.CONFIG_PROVIDER, getConfigName()));
		String actualValue = fetchText(dynamicXpathLocator(IntegrationsPageElements.LIST_CONFIG_NAME, getConfigName()));
		CustomAssertions.assertEquals("Names do not match:", getProviderName(), actualValue);
	}

	public void deleteSP(String configName, String type) {
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Actions"));
		if (checkIfElementisLocated(dynamicXpathLocator(IntegrationsPageElements.CONFIG_PROVIDER, configName))) {
			Log.info(configName + " will be deleted from the list");
			click(dynamicXpathLocator(IntegrationsPageElements.OPEN_POP_OVER, configName));
			click(IntegrationsPageElements.POP_OVER_DELETE.locate());
			try {
				TimeUnit.SECONDS.sleep(2);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if (checkIfElementisLocated(
					dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, "not be able to delete"))) {
				try {
					deleteAttachedCampaigns(configName, type);
					click(JourneysPageElements.CLOSE_BTN.locate());
					click(dynamicXpathLocator(IntegrationsPageElements.OPEN_POP_OVER, configName));
					click(IntegrationsPageElements.POP_OVER_DELETE.locate());
				} catch (IOException | ParseException e) {
					e.printStackTrace();
				}
			}
			click(IntegrationsPageElements.DELETE_BTN.locate());
			verifyToastMessage(type + NotificationToastMessages.DELETE_PROVIDER.message());
		}
	}

	private void deleteAttachedCampaigns(String configName, String type) throws IOException, ParseException {
		CampaignsPage campPage = new CampaignsPage();
		String credId;
		switch (type) {
		case "SSP":
			credId = fetchSPId(configName, "verifySSP");
			campPage.deleteCampaignsviaCampId(apiOperationsPage, fetchCampaignids(credId, "usageSSP"),
					"deleteSMSCampaign");
			break;
		case "WSP":
			credId = fetchSPId(configName, "verifyWSP");
			campPage.deleteCampaignsviaCampId(apiOperationsPage, fetchCampaignids(credId, "usageWSP"),
					"deleteWACampaign");
			break;
		case "ESP":
			credId = fetchSPId(configName, "verifyESP");
			campPage.deleteCampaignsviaCampId(apiOperationsPage, fetchCampaignids(credId, "usageESP"),
					"deleteEmailCampaign");
			break;
		default:
			break;
		}
	}

	private LinkedList<String> fetchCampaignids(String credId, String apiName) throws IOException, ParseException {
		apiOperationsPage.addURLParams("credsId", credId);
		apiOperationsPage.getMethod(apiName);
		List<Object> objList = apiUtility.fetchObjectList("response.data.usage[0].list");
		LinkedList<String> campIds = new LinkedList<>();
		for (Object obj : objList) {
			JSONObject jo = apiUtility.getJsonObject(new Gson().toJson(obj));
			campIds.add((String) jo.get("id"));
		}
		return campIds;
	}

	public String fetchSPId(String configName, String apiName) throws IOException, ParseException {
		String jsonPath = "response.data";
		apiOperationsPage.getMethod(apiName);
		List<Object> objList = apiUtility.fetchObjectList(jsonPath);
		String spID = null;
		for (Object obj : objList) {
			JSONObject jo = apiUtility.getJsonObject(new Gson().toJson(obj));
			if (jo.get("name").equals(configName)) {
				spID = (String) jo.get("id");
				break;
			}
		}
		return spID;
	}

	public void openRestApiPage() {
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Rest API"));
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "View"));
	}

	public void verifyApiPage() {
		waitForElementToBePresent(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "License Code:"));
		waitForElementToBePresent(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "API KEY:"));
		waitForElementToBePresent(
				dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, driver.getCurrentUrl().split("/")[4]));

	}

	public void verifyChannelStatus(List<String> list) {
		for (String channel : list) {
			waitForElementToBePresent(dynamicXpathLocator(IntegrationsPageElements.CHANNEL_CONFIGURE, channel));
			String actualStatus = fetchText(dynamicXpathLocator(IntegrationsPageElements.CHANNEL_STATUS, channel));
			CustomAssertions.assertTrue("Status does not match:",
					actualStatus.contains("PENDING") || actualStatus.contains("SUCCESS"));
		}
	}

	public void webPersonalizationToggle(String switchStatus) {
		waitForElementToBeVisible(IntegrationsPageElements.TOGGLE_STATUS.locate());
		switch (switchStatus) {
		case "ON":
			if (fetchAttributeValue(IntegrationsPageElements.WEB_PERSONALIZATION_TOGGLE.locate(), "class")
					.contains("toggle--checked")) {
				click(IntegrationsPageElements.WEB_PERSONALIZATION_TOGGLE.locate());
				click(dynamicXpathLocator(IntegrationsPageElements.BUTTON, "Yes"));
			}
			waitForTextToBe(IntegrationsPageElements.TOGGLE_STATUS.locate(), "Off");
			click(IntegrationsPageElements.WEB_PERSONALIZATION_TOGGLE.locate());
			waitForTextToBe(IntegrationsPageElements.TOGGLE_STATUS.locate(), "On");
			CustomAssertions.assertTrue("Toggle is OFF:",
					fetchAttributeValue(IntegrationsPageElements.WEB_PERSONALIZATION_TOGGLE.locate(), "class")
							.contains("toggle--checked"));
			break;
		case "OFF":
			if (!fetchAttributeValue(IntegrationsPageElements.WEB_PERSONALIZATION_TOGGLE.locate(), "class")
					.contains("toggle--checked")) {
				click(IntegrationsPageElements.WEB_PERSONALIZATION_TOGGLE.locate());
			}
			waitForTextToBe(IntegrationsPageElements.TOGGLE_STATUS.locate(), "On");
			click(IntegrationsPageElements.WEB_PERSONALIZATION_TOGGLE.locate());
			waitForElementToBePresent(
					dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Turn off Web Personalization"));
			waitForElementToBePresent(
					dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, "will stop showing"));
			click(dynamicXpathLocator(IntegrationsPageElements.BUTTON, "Yes"));
			waitForTextToBe(IntegrationsPageElements.TOGGLE_STATUS.locate(), "Off");
			CustomAssertions.assertFalse("Toggle is ON:",
					fetchAttributeValue(IntegrationsPageElements.WEB_PERSONALIZATION_TOGGLE.locate(), "class")
							.contains("toggle--checked"));
			break;
		default:
			break;
		}
	}

	public void webPushToggle(String switchStatus) {
		waitForElementToBeVisible(IntegrationsPageElements.TOGGLE_STATUS.locate());
		switch (switchStatus) {
		case "ON":
			if (fetchAttributeValue(IntegrationsPageElements.WEB_PUSH_TOGGLE.locate(), "class")
					.contains("toggle--checked")) {
				click(IntegrationsPageElements.WEB_PUSH_TOGGLE.locate());
				verifyToastMessage(NotificationToastMessages.OPTIN_DISABLED.message());
			}
			waitForTextToBe(IntegrationsPageElements.TOGGLE_STATUS.locate(), "Off");
			click(IntegrationsPageElements.WEB_PUSH_TOGGLE.locate());
			verifyToastMessage(NotificationToastMessages.OPTIN_ENABLED.message());
			waitForTextToBe(IntegrationsPageElements.TOGGLE_STATUS.locate(), "On");
			CustomAssertions.assertTrue("Toggle is OFF:",
					fetchAttributeValue(IntegrationsPageElements.WEB_PUSH_TOGGLE.locate(), "class")
							.contains("toggle--checked"));
			break;
		case "OFF":
			if (!fetchAttributeValue(IntegrationsPageElements.WEB_PUSH_TOGGLE.locate(), "class")
					.contains("toggle--checked")) {
				click(IntegrationsPageElements.WEB_PUSH_TOGGLE.locate());
				verifyToastMessage(NotificationToastMessages.OPTIN_ENABLED.message());
			}
			waitForTextToBe(IntegrationsPageElements.TOGGLE_STATUS.locate(), "On");
			click(IntegrationsPageElements.WEB_PUSH_TOGGLE.locate());
			verifyToastMessage(NotificationToastMessages.OPTIN_DISABLED.message());
			waitForTextToBe(IntegrationsPageElements.TOGGLE_STATUS.locate(), "Off");
			CustomAssertions.assertFalse("Toggle is ON:",
					fetchAttributeValue(IntegrationsPageElements.WEB_PUSH_TOGGLE.locate(), "class")
							.contains("toggle--checked"));
			break;
		default:
			break;
		}
	}

	public void editConfig(String configName, List<List<String>> choices, String type, JSONObject jsonObj) {
		click(dynamicXpathLocator(IntegrationsPageElements.OPEN_POP_OVER, configName));
		click(IntegrationsPageElements.POP_OVER_EDIT.locate());
		if (jsonObj != null) {
			refl.readAndFillData((JSONArray) jsonObj.get("Data"));
		} else {
			for (List<String> choice : choices) {
				String parameter = choice.get(0);
				String value = choice.get(1);
				clearFieldAndSendkeys(dynamicXpathLocator(IntegrationsPageElements.FIELDS, parameter), value);
				if (parameter.equals("name"))
					setProviderName(value);
			}
		}
		click(dynamicXpathLocator(IntegrationsPageElements.BUTTON, "Save"));
		String message = type.equals("WSP") ? NotificationToastMessages.WSP_CHANGES.message()
				: type + NotificationToastMessages.SP_UPDATE.message();
		verifyToastMessage(message);
	}

	public void editWhatsappTemplate(String templateName, String wspName, List<List<String>> choices) {
		searchWhatsappTemplate(templateName);
		click(dynamicXpathLocator(IntegrationsPageElements.WA_TEMPLATE_POP_OVER, templateName, wspName));
		click(IntegrationsPageElements.POP_OVER_EDIT.locate());
		for (List<String> choice : choices) {
			String parameter = choice.get(0);
			String value = choice.get(1);
			clearFieldAndSendkeys(dynamicXpathLocator(IntegrationsPageElements.FIELDS, parameter), value);
		}
		click(dynamicXpathLocator(IntegrationsPageElements.BUTTON, "Save"));
		verifyToastMessage(NotificationToastMessages.WA_TEMPLATE_CHANGES.message());

	}

	public void makeESP() {
		click(IntegrationsPageElements.ADD_ESP_BTN.locate());
		verifyToastMessage(NotificationToastMessages.ADD__ESP.message());
	}

	public void makeInvalidESP() {
		click(IntegrationsPageElements.ADD_ESP_BTN.locate());
		verifyToastMessage(NotificationToastMessages.ADD__INVALIDESP.message());
	}

	public void makeSSP() {
		click(IntegrationsPageElements.ADD_SSP_BTN.locate());
		verifyToastMessage(NotificationToastMessages.ADD_SSP.message());
	}

	public void makeWSP() {
		click(IntegrationsPageElements.ADD_WSP_BTN.locate());
		verifyToastMessage(NotificationToastMessages.ADD_WSP.message());
	}

	public void WSPConfirmation() {
		waitForElementToBePresent(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "WSP Added Successfully"));
		click(dynamicXpathLocator(IntegrationsPageElements.BUTTON, "OK"));
	}

	public void makeWATemplate(String wspName) {
		if (findElements(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Add WhatsApp Template")).size() != 0)
			click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Add WhatsApp Template"));
		else
			click(CommonPageElements.ADD_BTN.locate());
		click(dynamicXpathLocator(IntegrationsPageElements.DD_FIELD, "WhatsApp Service Provider (WSP)", "Select WSP"));
		click(dynamicXpathLocator(IntegrationsPageElements.SELECT_FROM_DD, wspName));
	}
	
	public void createCustomTemplate() {
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Saved Custom Templates"));
		click(CommonPageElements.ADD_BTN.locate());
	}
	
	public void viewEditDuplicateCustomTemplate(String templateAction, String templateName, List<List<String>> choices)
			throws InterruptedException {
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Saved Custom Templates"));
		selectTemplateMenuAction(templateName,templateAction);
		switch (templateAction) {
		case "Edit":
		case "Duplicate":
			waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT,  templateAction+" Template"));
			fillFields("", choices, false);
			saveTemplate(templateAction);
			break;
		case "View":
			waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "View Template"));
			verifyFields(choices);
			break;
		}
	}

	public void addTemplate() {
		click(dynamicXpathLocator(IntegrationsPageElements.BUTTON, "Add Template"));
		verifyToastMessage(NotificationToastMessages.WA_TEMPLATE.message());
	}
	
	public void saveTemplate(String toastType) {
		By saveButton = dynamicXpathLocator(IntegrationsPageElements.BUTTON, "Save Template");
		click(saveButton);
		switch (toastType) {
		case "Create":
			verifyToastMessage(NotificationToastMessages.CREATE_CUSTOM_TEMPLATE.message());
			break;
		case "Edit":
			verifyToastMessage(NotificationToastMessages.EDIT_CUSTOM_TEMPLATE.message());
			break;
		case "Duplicate":
			verifyToastMessage(NotificationToastMessages.DUPLICATE_CUSTOM_TEMPLATE.message());
			break;
		}
	}
	
	public void verifySDKBlock() {
		waitForElementToBeVisible(IntegrationsPageElements.SDK_CARD.locate());
		waitForElementToBePresent(IntegrationsPageElements.SDK_CARD_HEADERS.locate());
		List<WebElement> els = findElements(IntegrationsPageElements.SDK_CARD_HEADERS.locate());
		int count = 0;
		for (WebElement el : els) {
			if (el.getText().contains("WEBSITE") || el.getText().contains("ANDROID") || el.getText().contains("IOS")) {
				++count;
			}
		}
		CustomAssertions.assertEquals("Count do not match:", 3, count);
	}

	public void openTab(String tab) {
		click(dynamicXpathLocator(CommonPageElements.ANCHOR_TAG_GENERIC_TEXT, tab));
		waitForElementToBePresent(IntegrationsPageElements.SDK_STATUS.locate());
		String value = fetchText(IntegrationsPageElements.SDK_STATUS.locate());
		CustomAssertions.assertTrue("Status does not match:",
				value.equalsIgnoreCase("Pending") || value.equalsIgnoreCase("Success"));
	}

	public void savePushCreds(String platform) {
		click(IntegrationsPageElements.SAVE_BTN.locate());
		String platformMessage = platform.toLowerCase().contains("android")
				? NotificationToastMessages.SAVE_ANDROID_CREDS.message()
				: NotificationToastMessages.SAVE_IOS_CREDS.message();
		try {
			verifyToastMessage(platformMessage);
		} catch (TimeoutException e) {
			Log.warn("Failed to save creds in default timeout. Extending timeout further by same duration");
			verifyToastMessage(platformMessage);
		}
	}

	public void uploadAuthKey(String authKey) {
		uploadFile(authKey, IntegrationsPageElements.UPLOAD.locate());
		waitForElementToBePresent(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Reset"));
	}

	public void verifyPushConfigDelete(String name, String platform) {
		click(dynamicXpathLocator(IntegrationsPageElements.PLATFORM_SPECIFIC_DELETE, name, platform));
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Delete"));
		waitForElementToBeVisible(IntegrationsPageElements.DELETE_BTN.locate());
		String msg = platform.equalsIgnoreCase("Android") ? "Android Package details" : "iOS Credential";
		waitForElementToBePresent(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Delete " + msg));
		click(IntegrationsPageElements.DELETE_BTN.locate());
		String platformMsg = platform.equalsIgnoreCase("Android") ? NotificationToastMessages.ANDROID_DELETE.message()
				: NotificationToastMessages.IOS_DELETE.message();
		try {
			verifyToastMessage(platformMsg);
		} catch (TimeoutException e) {
			Log.warn("Failed to save creds in default timeout. Extending timeout further by same duration");
			verifyToastMessage(platformMsg);
		}
		waitForElementToBeInvisible(
				dynamicXpathLocator(IntegrationsPageElements.PLATFORM_SPECIFIC_DELETE, name, platform));
	}

	public void uploadFont(String fileName) {
		uploadFile(fileName, IntegrationsPageElements.UPLOAD.locate());
	}

	public void verifyFontInListing(boolean presence) {
		if (presence)
			waitForElementToBeVisible(dynamicXpathLocator(IntegrationsPageElements.OPEN_POP_OVER, getFontName()));
		else
			waitForElementToBeInvisible(dynamicXpathLocator(IntegrationsPageElements.OPEN_POP_OVER, getFontName()));
	}

	public void deleteFont(String fontName) {
		click(dynamicXpathLocator(IntegrationsPageElements.OPEN_POP_OVER, fontName));
		click(IntegrationsPageElements.DELETE_BTN.locate());
		waitForElementToBePresent(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Delete Custom Font"));
		click(IntegrationsPageElements.DELETE_BTN.locate());
		waitForElementToBeInvisible(IntegrationsPageElements.DELETE_BTN.locate());
	}

	public void checkAddWebhook(String webhookName) {
		waitForElementToBePresent(dynamicXpathLocator(IntegrationsPageElements.ADD_WEBHOOK, webhookName));
		String value = fetchText(dynamicXpathLocator(IntegrationsPageElements.ADD_WEBHOOK, webhookName));
		CustomAssertions.assertTrue("String does not match:", value.contains("Add New Webhooks"));
	}

	public void addWebhook(String webhookName, String url, String format) {
		sendKeys(dynamicXpathLocator(IntegrationsPageElements.WEBHOOK_URL, "Add New Webhooks", webhookName), url);
		Select options = new Select(
				findElement(dynamicXpathLocator(IntegrationsPageElements.WEBHOOK_SELECT, webhookName)));
		options.selectByValue(format);
		click(dynamicXpathLocator(IntegrationsPageElements.WEBHOOK_SAVE, webhookName));
	}

	public void checkConfiguredWebhook(String webhookName, String expectedURL, String expectedFormat) {
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, "Configured"));
		String actualURL = fetchAttributeValue(
				dynamicXpathLocator(IntegrationsPageElements.WEBHOOK_URL, "Configured Webhooks", webhookName), "value");
		Select selectedOption = new Select(findElement(
				dynamicXpathLocator(IntegrationsPageElements.WEBHOOK_FORMAT, "Configured Webhooks", webhookName)));
		String actualFormat = selectedOption.getFirstSelectedOption().getAttribute("value");
		CustomAssertions.assertEquals("WebHooks URL does not match:", expectedURL, actualURL);
		CustomAssertions.assertEquals("WebHooks format does not match:", expectedFormat, actualFormat);
	}

	public void deleteWebhook(String webhookName) {
		click(dynamicXpathLocator(IntegrationsPageElements.WEBHOOK_DELETE, webhookName));
		acceptAlert();
		waitForElementToBeInvisible(
				dynamicXpathLocator(IntegrationsPageElements.WEBHOOK_URL, "Configured Webhooks", webhookName));
	}

	public void addCredsFor(String platform) {
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Actions"));
		click(dynamicXpathLocator(IntegrationsPageElements.ADD_PLATFORM, platform));
	}

	public void checkForDuplicate(String configName) {
		waitForElementToBePresent(
				dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, "WhatsApp Service Providers (WSP)"));
		if (findElements(dynamicXpathLocator(IntegrationsPageElements.OPEN_POP_OVER, configName)).size() != 0) {
			deleteSP(configName, "WSP");
		}
	}

	public void editPushConfig(String osType, String configname, List<List<String>> choices) {
		click(dynamicXpathLocator(IntegrationsPageElements.OPEN_POP_OVER, configname));
		click(IntegrationsPageElements.POP_OVER_EDIT.locate());
		if (osType.toLowerCase().equals("android")) {
			String parameter = choices.get(0).get(0);
			String value = choices.get(0).get(1);
			click(dynamicXpathLocator(IntegrationsPageElements.EDIT_FIELD, "App Secret"));
			waitForElementToBeClickable(dynamicXpathLocator(IntegrationsPageElements.BUTTON, "Save"));
			clearFieldAndSendkeys(dynamicXpathLocator(IntegrationsPageElements.FIELDS, parameter), value);
			click(dynamicXpathLocator(IntegrationsPageElements.BUTTON, "Save"));
			verifyToastMessage(NotificationToastMessages.INVALID_ANDROID_CRED.message());
		} else {
			click(dynamicXpathLocator(IntegrationsPageElements.BUTTON, "Reset"));
			fillFields(null, choices, false);
			uploadAuthKey("AuthKey_S4WG995LHR.p8");
			new CommonPage().selectFromDropDown("Default push mode", "Production");
			CustomAssertions.assertFalse("Bundle identifier is expected to be disabled",
					findElement(dynamicXpathLocator(IntegrationsPageElements.FIELDS, "Bundle Identifier")).isEnabled());
			click(dynamicXpathLocator(IntegrationsPageElements.BUTTON, "Save"));
			try {
				verifyToastMessage(NotificationToastMessages.SAVE_IOS_CREDS.message());
			} catch (TimeoutException e) {
				Log.warn("Failed to save creds in default timeout. Extending timeout further by same duration");
				verifyToastMessage(NotificationToastMessages.SAVE_IOS_CREDS.message());
			}
		}
	}

	public void openSdkIntegrationPage() {
		click(IntegrationsPageElements.CONFIG.locate());
	}

	public void openLinks(String osType) {
		click(dynamicXpathLocator(IntegrationsPageElements.OS_TAB, osType));
		click(dynamicXpathLocator(CommonPageElements.MODULE, "Push Setup"));
		click(IntegrationsPageElements.PUSH_HYPERLINK.locate());
	}

	public void verifyPushPages(String osType) {
		String parentWindow = driver.getWindowHandle();
		switchToChildWindow();
		CustomAssertions.assertEquals("Incorrect Page", osType,
				fetchText(dynamicXpathLocator(IntegrationsPageElements.PACKAGE_NAME_RESULT, osType)));
		switchToParentWindow(parentWindow);
	}

	public void verifyValidPackages(String osType, String packageName) {
		String parentWindow = driver.getWindowHandle();
		switchToChildWindow();
		sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Search by package name"),
				packageName + Keys.ENTER);
		CustomAssertions.assertEquals("Invalid Package", packageName,
				fetchText(dynamicXpathLocator(IntegrationsPageElements.PACKAGE_NAME_RESULT, packageName)));
		switchToParentWindow(parentWindow);
	}

	public void verifyInvalidPackages(String osType, String packageName, String message) {
		String parentWindow = driver.getWindowHandle();
		switchToChildWindow();
		sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Search by package name"),
				packageName + Keys.ENTER);
		CustomAssertions.assertEquals("Invalid Package", message,
				fetchText(IntegrationsPageElements.INVALID_PACKAGE_LABEL.locate()));
		switchToParentWindow(parentWindow);
	}

	public void uploadData(String dataType) throws InterruptedException {
		TimeUnit.SECONDS.sleep(3);
		switch (dataType) {
		case "User Data":
			uploadFile("upload_user.csv", IntegrationsPageElements.UPLOAD.locate());
			break;
		case "Events Data":
			uploadFile("events_sample.csv", IntegrationsPageElements.UPLOAD.locate());
			break;
		default:
			uploadFile(modifyEventTime(dataType), IntegrationsPageElements.UPLOAD.locate());
			break;
		}
		TimeUnit.SECONDS.sleep(2);
		waitForElementToBeVisible(CommonPageElements.CONFIRM_PROCEED_BUTTON.locate());
		click(CommonPageElements.CONFIRM_PROCEED_BUTTON.locate());
	}

	private String modifyEventTime(String dataType) {
		File file = new File(SetupUtility.directoryLoc + "AccountData/" + dataType + "_updated.csv");
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		String dateTime = dtf.format(now);
		try {
			FileReader inputFile = new FileReader(SetupUtility.directoryLoc + "AccountData/" + dataType + ".csv");
			FileWriter outputfile = new FileWriter(file);
			CSVWriter writer = new CSVWriter(outputfile);
			List<String[]> data = new ArrayList<String[]>();
			CSVReader reader = new CSVReader(inputFile);
			List<String[]> allRows = reader.readAll();
			for (String[] row : allRows) {
				String toInsert = Arrays.toString(row).replace("[", "").replace("]", "").replace(" ", "")
						.replace("<date>", dateTime);
				data.add(toInsert.split(","));
			}
			writer.writeAll(data);
			reader.close();
			writer.close();
		} catch (IOException | CsvException e) {
			e.printStackTrace();
			CustomAssertions.assertTrue("Multi event CSV Updation failed", false);
		}
		return dataType + "_updated.csv";
	}

	public void verifyStatusViaAPI() {
		String api = "uploadUserOrEventsData";
		try {
			apiOperationsPage.getMethod(api);
			apiOperationsPage.waitForAPIResponse("9", api, "response.data.contents[0].status", "DONE");
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public void uploadStatus(String expectedStatus) throws InterruptedException {
		String actualStatus = "";
		int iteration = 1;
		switch (expectedStatus) {
		case "IN PROGRESS":
			actualStatus = fetchText(dynamicXpathLocator(IntegrationsPageElements.STATUS, expectedStatus));
			CustomAssertions.assertEquals("File upload status mismatch", expectedStatus, actualStatus);
			break;
		case "COMPLETED":
			while (!checkIfElementisLocated(dynamicXpathLocator(IntegrationsPageElements.STATUS, expectedStatus))) {
				click(CommonPageElements.SYNC_BTN.locate());
				iteration++;
				if (checkIfElementisLocated(dynamicXpathLocator(IntegrationsPageElements.STATUS, expectedStatus)))
					break;
				if (iteration == 40) {
					CustomAssertions.assertTrue("File upload status mismatch", false);
					break;
				}
				TimeUnit.SECONDS.sleep(6);
			}
			actualStatus = fetchText(dynamicXpathLocator(IntegrationsPageElements.STATUS, expectedStatus));
			CustomAssertions.assertEquals("File upload status mismatch", expectedStatus, actualStatus);
			break;
		}
	}

	public void clickOnEditPackages(String osType, String configname) {
		click(dynamicXpathLocator(IntegrationsPageElements.OPEN_POP_OVER, configname));
		click(IntegrationsPageElements.POP_OVER_EDIT.locate());
	}

	public void addCreds(List<List<String>> choices) {
		for (List<String> choice : choices) {
			String parameter = choice.get(0);
			String value = choice.get(1);
			String creds = choice.get(2);
			if (("MI PUSH SERVICE").equalsIgnoreCase(parameter)) {
				click(dynamicXpathLocator(CommonPageElements.TEXT_CONTAINS_SPAN, parameter));
				sendKeys(dynamicXpathLocator(IntegrationsPageElements.MI_APP_SECRET, value), creds);
			} else if (("HUAWEI PUSH SERVICE").equalsIgnoreCase(parameter)) {
				boolean visible = checkIfElementisLocated(
						dynamicXpathLocator(CommonPageElements.TEXT_CONTAINS_SPAN, "Huawei"));
				if (visible)
					click(dynamicXpathLocator(CommonPageElements.TEXT_CONTAINS_SPAN, parameter));
				sendKeys(dynamicXpathLocator(IntegrationsPageElements.FIELDS, value), creds);
			}
		}
		click(dynamicXpathLocator(IntegrationsPageElements.BUTTON, "Save"));
		verifyToastMessage(NotificationToastMessages.SAVE_ANDROID_CREDS.message());
	}

	public void verifyPushServices(List<List<String>> choices) {
		for (List<String> choice : choices) {
			String parameter = choice.get(0);
			CustomAssertions.assertEquals("Push Services not added", parameter,
					fetchText(dynamicXpathLocator(CommonPageElements.GENERIC_DIVTEXT, parameter)));
		}
	}

	public void removePushServices(List<List<String>> choices) {
		for (List<String> choice : choices) {
			String parameter = choice.get(0);
			click(dynamicXpathLocator(IntegrationsPageElements.DELETE_PUSH_SERVICES, parameter));
		}
		click(dynamicXpathLocator(IntegrationsPageElements.BUTTON, "Save"));
		try {
			verifyToastMessage(NotificationToastMessages.SAVE_ANDROID_CREDS.message());
		} catch (TimeoutException e) {
			Log.warn("Failed to save creds in default timeout. Extending timeout further by same duration");
			verifyToastMessage(NotificationToastMessages.SAVE_ANDROID_CREDS.message());
		}
	}

	public void verifyDeletedPushServices(List<List<String>> choices) {
		for (List<String> choice : choices) {
			String parameter = choice.get(0);
			CustomAssertions.assertEquals("Push Services not deleted", parameter,
					fetchText(dynamicXpathLocator(CommonPageElements.TEXT_CONTAINS_SPAN, parameter)));
		}
	}

	public void addTemplateType(String template, String wspName) {
		click(dynamicXpathLocator(IntegrationsPageElements.ADD_WHATSAPP_TEMPLATE, "WhatsApp Templates"));
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Select WSP"));
		click(dynamicXpathLocator(IntegrationsPageElements.SELECT_FROM_DD, wspName));
		click(dynamicXpathLocator(IntegrationsPageElements.WHATSAPP_TEMPLATE_TYPE, template));
	}

	public void searchAndDeleteTemplate(String templateName) {
		sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Search by template name"),
				templateName + Keys.ENTER);
		click(IntegrationsPageElements.WHATSAPP_TEMPLATE_ACTIONS.locate());
		click(dynamicXpathLocator(CommonPageElements.MODULE, "Delete"));
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "Delete"));

	}

	public void resetWebPushConfigMessage() {
		click(IntegrationsPageElements.WEB_PUSH_TYPE_OF_PROMPT.locate());
		clearFieldAndSendkeys(IntegrationsPageElements.WEB_PUSH_MESSAGE.locate(),
				"automation-web wants to start sending you push notifications. Click <b>Allow</b> to subscribe.");
		click(CommonPageElements.SAVE_BUTTON.locate());
	}

	public void verifyWebPushToastMessage() {
		verifyToastMessage(NotificationToastMessages.WEBPUSH_CONFIG_UPDATE.message());
	}
}
