package com.webengage.ui_automation.pages;

import com.webengage.ui_automation.elements.CampaignsPageElements;
import com.webengage.ui_automation.elements.CommonPageElements;
import com.webengage.ui_automation.elements.InAppCampaignPageElements;
import com.webengage.ui_automation.elements.PushCampaignPageElements;
import com.webengage.ui_automation.utils.CustomAssertions;
import com.webengage.ui_automation.utils.SeleniumExtendedUtility;

public class InAppCampaignsPage extends SeleniumExtendedUtility {
    
    public void verifyChangeTemplateWarning(String template) {
        click(dynamicXpathLocator(InAppCampaignPageElements.RADIO_BUTTON,"Template",template));
        String popUpText =fetchText(InAppCampaignPageElements.CHANGELAYOUT_MESSAGE.locate());
        CustomAssertions.assertEquals("Pop Up Message not matched","Please note that if you change your layout, your existing message will be lost and you will not be able to recover it. Please confirm if you would like to proceed.",popUpText);
        CustomAssertions.assertTrue("Cancel or Change Layout button not present",checkIfElementisLocated(dynamicXpathLocator(CommonPageElements.BUTTON, "CANCEL")) && checkIfElementisLocated(dynamicXpathLocator(CommonPageElements.BUTTON, "Change Layout")));
        String selectedTemp = fetchAttributeValue(dynamicXpathLocator(InAppCampaignPageElements.INPUT_VALUE,"Template",template),"value");
        CustomAssertions.assertEquals(template + " is selected",selectedTemp,"false");
        click(dynamicXpathLocator(CommonPageElements.BUTTON, "CANCEL"));
        click(dynamicXpathLocator(CommonPageElements.BUTTON, "BACK"));

    }
    
    public void verifyCharacterLimitError() {
        String errMsg = fetchAttributeValue(InAppCampaignPageElements.TOOLTIP_ERROR.locate(),"data-tooltip");
        CustomAssertions.assertEquals("Character limit exceeded error not found","Character limit exceeded. Max character limit: 10000",errMsg);
        boolean checkAttr = verifyElementDisabled(CommonPageElements.SAVE_AND_CONTINUE_BUTTON.locate());
        CustomAssertions.assertTrue("Save and Continue button is not disabled",checkAttr);
    }
 
    public void switchLayout(String layout) {
        waitUntilAttributeContains(dynamicXpathLocator(CampaignsPageElements.BREADCRUMB,"Message"),"aria-current","page");
        boolean changeLayout = checkIfElementisLocated(InAppCampaignPageElements.CHANGELAYOUT_TOOLTIP.locate());
        if(changeLayout) {
            click(InAppCampaignPageElements.CHANGELAYOUT_TOOLTIP.locate());
            click(dynamicXpathLocator(CommonPageElements.BUTTON, "Change Layout"));
        }
        click(dynamicXpathLocator(PushCampaignPageElements.TEMPLATE,layout));
    }
   
    public void verifyImageHeightNumeric() {
        click(dynamicXpathLocator(InAppCampaignPageElements.MESSAGE_BUTTON,"Image Height"));
        CustomAssertions.assertEquals("Image Height doesn't have numeric value","0",fetchAttributeValue(dynamicXpathLocator(CampaignsPageElements.INPUT_FIELD,"Image Height"),"value"));
        click(dynamicXpathLocator(InAppCampaignPageElements.MESSAGE_BUTTON,"Image Height"));
    }
   

    
}
