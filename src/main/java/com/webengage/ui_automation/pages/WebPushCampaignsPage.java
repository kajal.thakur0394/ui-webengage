package com.webengage.ui_automation.pages;

import com.webengage.ui_automation.elements.IntegrationsPageElements;
import com.webengage.ui_automation.elements.LoginPageElements;
import com.webengage.ui_automation.utils.CustomAssertions;
import com.webengage.ui_automation.utils.Log;
import com.webengage.ui_automation.utils.SeleniumExtendedUtility;
import com.webengage.ui_automation.utils.SetupUtility;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WebPushCampaignsPage extends SeleniumExtendedUtility {

    IntegrationsPage intPage = new IntegrationsPage();
    SegmentsPage segmentsPage = new SegmentsPage();
    UserPage userPage = new UserPage();
    CampaignsPage campaignsPage = new CampaignsPage();

    public void verifySDKStatus(){
        intPage.openIntegrationPage();
        waitForElementToBePresent(IntegrationsPageElements.SDK_SETUP_STATUS.locate());
        String value = fetchText(IntegrationsPageElements.SDK_SETUP_STATUS.locate());
        Log.info("SDK Integration status: "+value);
        CustomAssertions.assertTrue("SDK Integration Status mismatch", value.equalsIgnoreCase("Success"));

    }

    public void verifySDKDomain(){
        intPage.openIntegrationPage();
        intPage.clickOnConfigure("SDK Integration Status");
        String configuredDomain = fetchAttributeValue(LoginPageElements.DOMAIN_SDK.locate(), "value");
        Log.info("Existing configured SDK URL: " +configuredDomain);
        CustomAssertions.assertNotNull("No SDK URL configured",configuredDomain);

    }

    public void verifyWebPushStatus(){
        intPage.openIntegrationPage();
        waitForElementToBePresent(dynamicXpathLocator(IntegrationsPageElements.CHANNEL_CONFIGURE, "Web Push Setup"));
        String actualStatus = fetchText(dynamicXpathLocator(IntegrationsPageElements.CHANNEL_STATUS, "Web Push Setup"));
        Log.info("WebPush Integration Status: " +actualStatus);
        CustomAssertions.assertTrue("Web Push Channel Integration Status mismatch", actualStatus.contains("SUCCESS"));
    }

    public void verifyWebPushToggle(){
        intPage.openIntegrationPage();
        intPage.clickOnConfigure("Web Push Setup");
        String toggleStatus = fetchText(IntegrationsPageElements.TOGGLE_STATUS.locate());
        Log.info("Web Push Toggle status is: "+toggleStatus);
        CustomAssertions.assertEquals("Toggle status is off","On",toggleStatus);

    }

    public void verifyUserReachability() throws IOException {
        segmentsPage.openUserTab("List of Users");
        userPage.selectUser(SetupUtility.configProp.getProperty("WebPush"));
        Log.info("Checking Web Push Reachability for user: " +SetupUtility.configProp.getProperty("WebPush"));
        userPage.openSection("Channels");
        List<List<String>> listOfChannels = new ArrayList<>();
        List<String> innerList = new ArrayList<>();
        innerList.add("WEB PUSH");
        innerList.add("green");
        listOfChannels.add(innerList);
        userPage.verifyRechability(listOfChannels);
    }

    public void selectWebPushSDKSegment() throws IOException {
        campaignsPage.selectSingleSegment(SetupUtility.configProp.getProperty("WebPushSegment"));
    }
	
}
