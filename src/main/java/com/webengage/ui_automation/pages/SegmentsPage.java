package com.webengage.ui_automation.pages;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.webengage.ui_automation.utils.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.webengage.ui_automation.driver.DataFactory;
import com.webengage.ui_automation.elements.CampaignsPageElements;
import com.webengage.ui_automation.elements.CommonPageElements;
import com.webengage.ui_automation.elements.EmailCampaignPageElements;
import com.webengage.ui_automation.elements.IntegrationsPageElements;
import com.webengage.ui_automation.elements.JourneysPageElements;
import com.webengage.ui_automation.elements.OnsiteNotificationPageElements;
import com.webengage.ui_automation.elements.SMSCampaignPageElements;
import com.webengage.ui_automation.elements.SegmentsPageElements;
import com.webengage.ui_automation.utils.APIUtility;
import com.webengage.ui_automation.utils.ConstantUtils;
import com.webengage.ui_automation.utils.CustomAssertions;
import com.webengage.ui_automation.utils.NotificationToastMessages;
import com.webengage.ui_automation.utils.RuntimeUtility;
import com.webengage.ui_automation.utils.SeleniumExtendedUtility;

public class SegmentsPage extends SeleniumExtendedUtility {
	CommonPage comPage = new CommonPage();
	String segmentName;
	String userCount;
	RuntimeUtility runTimeUtility = new RuntimeUtility();

	public static void setSegmentId(String segmentID) {
		DataFactory.getInstance().setData(ConstantUtils.SEGMENTID, segmentID);
	}

	public static String getSegmentId() {
		return DataFactory.getInstance().getData(ConstantUtils.SEGMENTID, String.class);
	}

	public String getSegmentName() {
		return segmentName;
	}

	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}

	public String getUserCount() {
		return userCount;
	}

	public void setUserCount(String userCount) {
		this.userCount = userCount;
	}

	public SegmentsPage() {
		super();
	}

	public void openSegmentType(String segType) {

		if (!fetchAttributeValue(SegmentsPageElements.SEGMENTS_TAB_LIST.locate(), "class").contains("active"))
			click(dynamicXpathLocator(CommonPageElements.MODULE, "Segments"));
		click(dynamicXpathLocator(CommonPageElements.MODULE, segType));
	}

	public void newSegment(String segName) {
		click(SegmentsPageElements.CREATE_BTN.locate());
		sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Segment Name"), segName);
		setSegmentName(segName);
	}

	public void fillChoicesUserCard(List<List<String>> choices) {
		boolean collasped = fetchAttributeValue(SegmentsPageElements.USER_CARD_HEADER.locate(), "class")
				.contains("collapsed");
		if (collasped)
			click(SegmentsPageElements.USER_CARD.locate());
		for (List<String> choice : choices) {
			if (choice.get(0) == null && choice.get(1) == null)
				return;
			String parameter = choice.get(0);
			String value = choice.get(1);
			String valueCol2 = "";
			String text = "";
			if (value.contains(">")) {
				text = value.split(">")[1];
				value = value.split(">")[0];
			}
			if (value.contains("-")) {
				valueCol2 = value.split("-")[1];
				value = value.split("-")[0];
			}
			if (parameter.equalsIgnoreCase("Set Date as"))
				setCalendarDate(value);
			else if (parameter.contains("-")) {
				valueCol2 = parameter.split("-")[1];
				parameter = parameter.split("-")[0];
				sendKeys(dynamicXpathLocator(SegmentsPageElements.SEND_VALUE, parameter, valueCol2), value);
			} else if (parameter.equalsIgnoreCase("Set Geo filtering type")) {
				click(SegmentsPageElements.SELECT_OPERATOR_GEO_FILTERING.locate());
				click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, value));
			} else {
				By dd_Type = parameter.contains(">")
						? dynamicXpathLocator(SegmentsPageElements.OPEN_DD_SELECT, parameter.split(">")[0],
								parameter.split(">")[1])
						: dynamicXpathLocator(SegmentsPageElements.OPEN_DD, parameter);
				click(dd_Type);
				click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, value));
				if (!text.isEmpty())
					if (parameter.equalsIgnoreCase("User IDs") && value.contains("one of")) {
						String[] userIdArray = text.split(",");
						for (String userId : userIdArray) {
							click(dynamicXpathLocator(SegmentsPageElements.OPEN_DD_COL2, parameter));
							sendKeys(dynamicXpathLocator(SegmentsPageElements.SEND_INPUT, parameter),
									userId.trim() + Keys.RETURN);
						}
					} else
						sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Enter a value"), text);
				if (!valueCol2.isEmpty()) {
					By openDDLocator = valueCol2.equalsIgnoreCase("is not in")
							? dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "is in")
							: dynamicXpathLocator(SegmentsPageElements.OPEN_DD_COL2, parameter);
					click(openDDLocator);
					click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, valueCol2));
				}
			}
		}
	}

	public void setCalendarDate(String day) {
		String[] dateParts = day.split(" to ");
		datePicker(dateParts[0], false);
		if (dateParts.length > 1)
			datePicker(dateParts[1], true);
	}

	public void datePicker(String date, boolean toDatePresent) {
		By locator = toDatePresent ? SegmentsPageElements.OPEN_CALENDAR_LAST.locate()
				: SegmentsPageElements.OPEN_CALENDAR.locate();
		click(locator);
		String[] dateParts = date.split(" ");
		String day = dateParts[0];
		String month = dateParts[1];
		String year = dateParts[2];
		click(SegmentsPageElements.OPEN_YEAR_LIST.locate());
		click(dynamicXpathLocator(SegmentsPageElements.SELECT_MONTH_OR_YEAR, year));
		click(dynamicXpathLocator(SegmentsPageElements.SELECT_MONTH_OR_YEAR, month));
		click(dynamicXpathLocator(SegmentsPageElements.SELECT_DATE, day, month));
	}

	public void checkSegementDetails(String type, Integer expectedValue) {
		setUserCount(String.valueOf(expectedValue));
		waitForSpinner();
		click(SegmentsPageElements.SEGMENT_DETAILS_BTN.locate());
		String actualValue = fetchText(dynamicXpathLocator(SegmentsPageElements.SEGEMENT_DETAIL_COUNT, type));
		CustomAssertions.assertEquals("Segment count do not match:", Double.valueOf(expectedValue),
				Double.valueOf(actualValue));
	}

	public void saveSegment() {
		click(SegmentsPageElements.SAVE_BTN.locate());
	}

	public void checkList(String type, boolean liveSeg) {
		switch (type) {
		case "Updated":
			verifyToastMessage(NotificationToastMessages.SEGMENT_UPDATED.message());
			break;
		case "Created":
			if (liveSeg)
				verifyToastMessage(NotificationToastMessages.SEGMENT_CREATED.message());
			else
				verifyToastMessage(NotificationToastMessages.LIST_CREATED.message());
			break;
		default:
			break;
		}
		if (liveSeg)
			waitForElementToBeVisible(dynamicXpathLocator(SegmentsPageElements.SEGMENTS_LIST, getSegmentName()));
	}

	public void checkList(String segName) {
		waitForElementToBeVisible(dynamicXpathLocator(SegmentsPageElements.SEGMENTS_LIST, segName));
	}

	public void deleteSegment(String segName, String type, String toastMsg) {
		setSegmentName(segName);
		waitUntilAttributeDoesNotContain(SegmentsPageElements.CARD_CONTENT.locate(), "class", "loader");
		clearFieldAndSendkeys(CommonPageElements.SEARCHBOX.locate(), segmentName + Keys.RETURN);
		click(dynamicXpathLocator(SegmentsPageElements.OPEN_POP_OVER, segName));
		click(SegmentsPageElements.POP_OVER_DELETE.locate());
		click(dynamicXpathLocator(CommonPageElements.BUTTON, type));
		verifyToastMessage(toastMsg);
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		click(JourneysPageElements.CLOSE_BTN.locate());
	}

	public void refreshSegment(String segName) {
		comPage.clickPopUpItem(segName, "Refresh");
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "Confirm Refresh List"));
		verifyToastMessage(NotificationToastMessages.REFRESH_STATIC_SEGMENT.message());
		click(JourneysPageElements.CLOSE_BTN.locate());
	}

	public void refreshFrequency(Map<String, String> map) {
		for (Map.Entry<String, String> entry : map.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			switch (key.toLowerCase()) {
			case "when":
				click(dynamicXpathLocator(SegmentsPageElements.OPEN_DD, "Refresh Frequency"));
				click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, value.split(">")[0]));
				if (value.contains("Weekly")) {
					String[] daysOfWeek = value.split(">")[1].split(",");
					click(SegmentsPageElements.SELECT_DAY.locate());
					List<WebElement> dayEle = findElements(SegmentsPageElements.SELECT_DAY.locate());
					boolean isMondaySelected = false;
					for (String day : daysOfWeek) {
						switch (day.trim()) {
						case "Mon":
							dayEle.get(0).click();
							isMondaySelected = true;
							break;
						case "Tue":
							dayEle.get(1).click();
							break;
						case "Wed":
							dayEle.get(2).click();
							break;
						case "Thu":
							dayEle.get(3).click();
							break;
						case "Fri":
							dayEle.get(4).click();
							break;
						case "Sat":
							dayEle.get(5).click();
							break;
						case "Sun":
							dayEle.get(6).click();
							break;
						default:
							break;
						}
					}
					if (!isMondaySelected)
						dayEle.get(0).click();
				} else if (value.contains("Monthly")) {
					click(SegmentsPageElements.SELECT_DAY_OF_MONTH.locate());
					click((dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, value.split(">")[1])));
				}
				break;
			case "time":
				String[] addtimeSplit = value.split(":");
				click(dynamicXpathLocator(SegmentsPageElements.SELECT_TIME, "hours"));
				click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, addtimeSplit[0].trim()));
				click(dynamicXpathLocator(SegmentsPageElements.SELECT_TIME, "minutes"));
				click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, addtimeSplit[1].trim()));
				click(dynamicXpathLocator(SegmentsPageElements.SELECT_TIME, "wrapper"));
				click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, addtimeSplit[2].trim()));
				break;
			default:
				break;
			}
		}
	}

	public void deleteSegmentFromTooltip(String type, String toastMsg) {
		click(dynamicXpathLocator(SegmentsPageElements.TOOLTIP_BUTTON, "Delete"));
		click(dynamicXpathLocator(CommonPageElements.BUTTON, type));
		verifyToastMessage(toastMsg);
	}

	public void checkIfNotInList() {
		waitForSpinner();
		waitForElementToBeInvisible(dynamicXpathLocator(SegmentsPageElements.SEGMENTS_LIST, getSegmentName()));
	}

	public void performActionforSegment(String action, String segment) {
		setSegmentName(segment);
		waitForElementToBeVisible(SMSCampaignPageElements.TABLE_HEADER.locate());
		clearFieldAndSendkeys(CommonPageElements.SEARCHBOX.locate(), segment + Keys.RETURN);
		click(dynamicXpathLocator(SegmentsPageElements.OPEN_POP_OVER, segment));
		click(dynamicXpathLocator(SegmentsPageElements.PERFORM_ACTION, action));
	}

	public void verifyActionPresence(String action, String segment) {
		click(dynamicXpathLocator(SegmentsPageElements.OPEN_POP_OVER, segment));
		CustomAssertions.assertTrue("Expected" + action + " action to be present",
				checkIfElementisLocated(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, action)));
		CustomAssertions.assertTrue("Expected Four actions to be present",
				findElements(SegmentsPageElements.POPUP_OPTIONS.locate()).size() == 4);
	}

	public void duplicateContainer() {
		CustomAssertions.assertEquals("Duplicate String does not match:", "Duplicate Segment",
				fetchText(SegmentsPageElements.CONTAINER_TITLE.locate()));
		click(dynamicXpathLocator(SegmentsPageElements.OPEN_DD, "Select Project"));
		click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, System.getProperty("accountName")));
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "Duplicate"));
		verifyToastMessage(NotificationToastMessages.DUPLICATE_SEGMENT.message());
	}

	public void openSegment(String segName) {
		setSegmentName(segName);
		waitForElementToBeVisible(SMSCampaignPageElements.TABLE_HEADER.locate());
		clearFieldAndSendkeys(CommonPageElements.SEARCHBOX.locate(), segName + Keys.RETURN);
		clickUsingJSExecutor(dynamicXpathLocator(SegmentsPageElements.OPEN_SEGMENT, segName));
		waitForElementToBePresent(dynamicXpathLocator(SegmentsPageElements.TOOLTIP_BUTTON, "Edit"));
		waitForElementToBePresent(SegmentsPageElements.SEGEMNT_PAGE_HEADER.locate());
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "TOTAL USERS"));
		CustomAssertions.assertEquals("Segments do not match:", segName,
				fetchAttributeValue(SegmentsPageElements.SEGEMNT_PAGE_HEADER.locate(), "title"));
	}

	public void clickOnToolTipButton(String button) {
		click(dynamicXpathLocator(SegmentsPageElements.TOOLTIP_BUTTON, button));
	}

	public void fillChoicesBehCard(List<List<String>> choices) throws InterruptedException {
		boolean collasped = fetchAttributeValue(SegmentsPageElements.BEHAVIORAL_CARD_HEADER.locate(), "class")
				.contains("collapsed");
		if (collasped)
			click(SegmentsPageElements.BEHAVIORAL_CARD.locate());
		for (List<String> choice : choices) {
			String logicalOp = choice.get(0);
			String parameter = choice.get(1);
			String value = choice.get(2);
			if (logicalOp != null) {
				TimeUnit.MILLISECONDS.sleep(3500);
				click(dynamicXpathLocator(SegmentsPageElements.ADD_BUTTON, parameter));
			}
			TimeUnit.MILLISECONDS.sleep(1500);
			click(dynamicXpathLocator(SegmentsPageElements.OPEN_DD_SELECT, parameter, "-"));
			sendKeys(CommonPageElements.SEARCH.locate(), value.split(">")[1]);
			String traverseDirection = "preceding";
			if (value.split(">")[0].toLowerCase().contains("system")) {
				traverseDirection = "following";
				click(dynamicXpathLocator(SegmentsPageElements.SELECT_EVENT, traverseDirection, value.split(">")[1]));
			}
			if (value.split(">")[0].toLowerCase().contains("application")) {
				traverseDirection = "following";
				click(dynamicXpathLocator(SegmentsPageElements.SELECT_APPLICATION_EVENT, traverseDirection,
						value.split(">")[1]));
			}
			if (logicalOp != null) {
				List<WebElement> el = findElements(
						dynamicXpathLocator(SegmentsPageElements.LOGICAL_OP, parameter, logicalOp));
				el.get(el.size() - 1).click();
			}
		}
	}

	public void verifyUsersCount(String value, Integer expectedValue) {
		String actualValue = fetchText(dynamicXpathLocator(SegmentsPageElements.USERS_SEGMENT_PAGE, value));
		CustomAssertions.assertEquals("User count do not match:", Double.valueOf(expectedValue),
				Double.valueOf(actualValue));
	}

	public void fillChoicesTechCard(String type, List<List<String>> choices) {
		boolean collasped = fetchAttributeValue(SegmentsPageElements.TECHNOLOGY_CARD_HEADER.locate(), "class")
				.contains("collapsed");
		if (collasped)
			click(SegmentsPageElements.TECHNOLOGY_CARD.locate());
		fillCardsinTechnology(type, choices);
	}

	private void fillCardsinTechnology(String type, List<List<String>> choices) {
		boolean collasped;
		switch (type) {
		case "Android":
			collasped = fetchAttributeValue(SegmentsPageElements.ANDROID_CARD_HEADER.locate(), "class")
					.contains("collapsed");
			if (collasped)
				click(SegmentsPageElements.ANDROID_CARD.locate());
			break;
		case "iOS":
			collasped = fetchAttributeValue(SegmentsPageElements.IOS_CARD_HEADER.locate(), "class")
					.contains("collapsed");
			if (collasped)
				click(SegmentsPageElements.IOS_CARD.locate());
			break;
		case "Web":
			collasped = fetchAttributeValue(SegmentsPageElements.WEB_CARD_HEADER.locate(), "class")
					.contains("collapsed");
			if (collasped)
				click(SegmentsPageElements.WEB_CARD.locate());
			break;
		default:
			break;
		}
		for (List<String> choice : choices) {
			String parameter = choice.get(0);
			String value = choice.get(1);
			String valueCol2 = "";
			String text = "";
			if (value.contains(">")) {
				text = value.split(">")[1];
				value = value.split(">")[0];
			}
			if (value.contains("-")) {
				valueCol2 = value.split("-")[1];
				value = value.split("-")[0];
			}
			if (parameter.equalsIgnoreCase("Set Date as"))
				setCalendarDate(value);
			else {
				click(dynamicXpathLocator(SegmentsPageElements.SELECT_PLACE_HOLDER, type));
				click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, parameter));
				click(dynamicXpathLocator(SegmentsPageElements.OPEN_DD, parameter));
				click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, value));
				if (!text.isEmpty()) {
					if (value.equalsIgnoreCase("is one of") || value.equalsIgnoreCase("is neither of")) {
						clickUsingJSExecutor(dynamicXpathLocator(OnsiteNotificationPageElements.NEW_TITLE, text));
					} else if (value.equalsIgnoreCase("one of") || value.equalsIgnoreCase("none of"))
						sendKeys(dynamicXpathLocator(CommonPageElements.INPUT_TEXT_BOX, "Select..."),
								text + Keys.RETURN);
					else
						sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Enter a value"), text);
				}
				if (!valueCol2.isEmpty()) {
					click(dynamicXpathLocator(SegmentsPageElements.OPEN_DD_COL2, parameter));
					click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, valueCol2));
				}
			}
		}
	}

	public void openUserTab(String tab) {
		click(dynamicXpathLocator(CommonPageElements.MODULE, "Users"));
		click(dynamicXpathLocator(CommonPageElements.ANCHOR_TAG_GENERIC_TEXT, tab));
		waitForElementToBeVisible(SegmentsPageElements.TABLE_HEADER.locate());
	}

	public void searchBar(String keyword) {
		clearTextField(CommonPageElements.SEARCHBOX.locate());
		findElement(CommonPageElements.SEARCHBOX.locate()).sendKeys(keyword + Keys.RETURN);
		waitForElementToBeVisible(SegmentsPageElements.TABLE_HEADER.locate());
	}

	public void verifyUserRow(int expectedCount, String userID) {
		CustomAssertions.assertEquals("Row count do not match:", expectedCount,
				findElements(SegmentsPageElements.USER_ROWS.locate()).size());
		List<WebElement> userIds = findElements(SegmentsPageElements.USER_ROWS_USER_ID.locate());
		for (WebElement el : userIds) {
			CustomAssertions.assertEquals("ID count does not match:", userID, el.getAttribute("innerHTML"));
		}
	}

	public void selectFilters(List<String> list) {
		click(SegmentsPageElements.FILTER_BTN.locate());
		List<WebElement> selected = findElements(SegmentsPageElements.SELECTED_FILTERS.locate());
		for (WebElement temp : selected) {
			temp.click();
		}
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "APPLY"));
		List<WebElement> headers = findElements(CommonPageElements.TABLE_HEADER.locate());
		CustomAssertions.assertEquals("Size do not match:", 1, headers.size());
		click(SegmentsPageElements.FILTER_BTN.locate());
		for (String s : list) {
			click(dynamicXpathLocator(SegmentsPageElements.CHECKBOX, s));
		}
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "APPLY"));
		headers = findElements(CommonPageElements.TABLE_HEADER.locate());
		CustomAssertions.assertEquals("Size does not match:", list.size() + 1, headers.size());
	}

	public void verifyColumns(List<String> expectedColumns) {
		List<WebElement> headers = findElements(CommonPageElements.TABLE_HEADER.locate());
		List<String> actualColumns = new ArrayList<>();
		for (WebElement el : headers) {
			actualColumns.add(el.getText());
		}
		Collections.sort(actualColumns, String.CASE_INSENSITIVE_ORDER);
		Collections.sort(expectedColumns, String.CASE_INSENSITIVE_ORDER);
		CustomAssertions.assertEquals("Columns do not match:", expectedColumns.toString().toLowerCase(),
				actualColumns.toString().toLowerCase());
	}

	public void newStaticSegment(String type, String name) {
		type = type.contains("CSV") ? "upload CSV file" : " Create with Criteria ";
		click(SegmentsPageElements.CREATE_BTN.locate());
		click(dynamicXpathLocator(CommonPageElements.BUTTON, type));
		if (type.contains("CSV"))
			sendKeys(SegmentsPageElements.CSV_SEGMENT_NAME.locate(), name);
		else
			sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "List Name"), name);
		setSegmentName(name);
	}

	public void verifyStaticSegments() {
		waitForElementToBeVisible(dynamicXpathLocator(SegmentsPageElements.OPEN_SEGMENT, getSegmentName()));
		boolean statusLabel = checkIfElementisLocated(
				dynamicXpathLocator(SegmentsPageElements.STATIC_SEGMENT_TOOL_TIP, getSegmentName()));
		if (statusLabel) {
			String actualValue = fetchText(
					dynamicXpathLocator(SegmentsPageElements.STATIC_SEGMENT_TOOL_TIP, getSegmentName()));
			CustomAssertions.assertEquals("Status do not match:", "IN PROGRESS", actualValue);
		} else
			CustomAssertions.assertTrue("Brewing tool tip not found", checkIfElementisLocated(
					dynamicXpathLocator(SegmentsPageElements.SEGMENT_TOOLTIP, getSegmentName())));
	}

	public void addFilter(String type, List<String> list) {
		List<WebElement> el = findElements(dynamicXpathLocator(SegmentsPageElements.FILTER_BUTTON, type));
		el.get(el.size() - 1).click();
		String expected = type.contains("NOT") ? "Filter for users who DID NOT do events"
				: "Filter for users who DID events";
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, expected));
		try {
			TimeUnit.SECONDS.sleep(1);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		String eventAttribute = list.get(0);
		String operator = list.get(1);
		String attrValue = list.get(2);
		String date = list.get(3);
		click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, "- Select Attribute -"));
		click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, eventAttribute));
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_LAST, "Select"));
		click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, operator));
		boolean isValid = !(operator.contains("empty") || operator.contains("present"));
		if (isValid) {
			boolean operatorType = checkIfElementisLocated(SegmentsPageElements.ENTER_A_VALUE.locate());
			if (operatorType)
				enterTwoValues(attrValue);
			else if (!operator.equalsIgnoreCase("on")) {
				click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_LAST, "Select"));
				click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, attrValue));
			}
			if (!(date == null))
				setCalendarDate(date);
		}
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "APPLY"));
		waitForElementToBeInvisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_LAST, expected));
		el = findElements(dynamicXpathLocator(SegmentsPageElements.FILTER_BUTTON, type));
		CustomAssertions.assertEquals("Size does not match:", "1", el.get(el.size() - 1).getAttribute("data-count"));
	}

	public void uploadFile(String segName) {
		uploadFile("StaticList.csv", IntegrationsPageElements.UPLOAD.locate());
		clearFieldAndSendkeys(dynamicXpathLocator(CampaignsPageElements.INPUT_FIELD, "LIST NAME"), segName);
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "Save & Continue"));
	}

	public void enterTwoValues(String value) {
		String[] values = value.contains("to") ? value.split(" to ") : new String[] { value };
		click(SegmentsPageElements.ENTER_A_VALUE.locate());
		sendKeys(SegmentsPageElements.ENTER_VALUE.locate(), values[0] + Keys.RETURN);
		if (values.length > 1) {
			click(SegmentsPageElements.ENTER_A_VALUE_LAST_BOX.locate());
			sendKeys(SegmentsPageElements.ENTER_VALUE_LAST.locate(), values[1] + Keys.RETURN);
		}
	}

	/*
	 * @param{name} This method will verify that segment name in the list matches
	 * with the expected segment name.
	 */
	public void verifySegment(String name) {
		waitForElementToBeVisible(SMSCampaignPageElements.TABLE_HEADER.locate());
		clearFieldAndSendkeys(CommonPageElements.SEARCHBOX.locate(), name + Keys.RETURN);
		CustomAssertions.assertEquals("Segments do not match:", name,
				fetchAttributeValue(CommonPageElements.NAME_LINK_TEXT.locate(), "title"));
	}

	public void openMeatBallTagMenu(String segmentName) {
		click(dynamicXpathLocator(SegmentsPageElements.OPEN_POP_OVER, segmentName));
		click(SegmentsPageElements.POP_OVER_TAG.locate());
	}

	public void verifyTagIsDeletedFromList(String name) {
		verifyTagIsDeleted(name);
	}

	private void verifyTagIsDeleted(String name) {
		waitForElementToBeInvisible(
				dynamicXpathLocator(SegmentsPageElements.TAG_DATE, name, CampaignsPage.getSystemDateTime()));
		CustomAssertions.assertEquals("Element is not present:", false, checkIfElementisLocated(
				dynamicXpathLocator(SegmentsPageElements.TAG_DATE, name, CampaignsPage.getSystemDateTime())));
	}

	public void openShowDetailsPage(String segName) {
		comPage.openCampaign(segName);
		click(SegmentsPageElements.SHOWDETAILS.locate());
		click(SegmentsPageElements.ADD_TAG_DETAILS.locate());

	}

	public void getSegmentID() {
		String segmentID = getCurrentURL().split("/")[getCurrentURL().split("/").length - 2];
		setSegmentId(segmentID);
		Log.info("SEGMENTID: " + segmentID);
		Log.info(getSegmentId());
	}

	public void selectEventType(List<String> eventType) {
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "at least once"));
		click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, eventType.get(0)));
		if (eventType.get(0).contains("most recent") || eventType.get(0).contains("least recent")) {
			click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Select..."));
			click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, eventType.get(1)));
			if (!(eventType.get(1).contains("present") || eventType.get(1).equalsIgnoreCase("on"))) {
				click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Select..."));
				click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, eventType.get(2)));
			}
			if (!(eventType.get(3) == null))
				setCalendarDate(eventType.get(3));
		}
		if (eventType.get(0).contains("Price") || eventType.get(0).equalsIgnoreCase("no. of occurences")
				|| eventType.get(0).contains("count_number")) {
			click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, "Select Operator"));
			click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, eventType.get(1)));
			enterTwoValues(eventType.get(2));
		}
	}

	public void verifyDropDownValues(String label) {
		List<String> list = new ArrayList<String>();
		By arrow = dynamicXpathLocator(SegmentsPageElements.OPEN_ARROW, label);
		click(arrow);
		List<WebElement> options = findElements(dynamicXpathLocator(SegmentsPageElements.DD_OPTIONS, label));
		for (WebElement ele : options) {
			list.add(ele.getText());
		}
		for (String opt : list) {
			click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, opt));
			waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, label));
			click(arrow);
		}
		click(arrow);
	}

	public void verifyRefreshingListCategory(String category, String expectedValue, String segName) {
		switch (category.toLowerCase()) {
		case "status":
			CustomAssertions.assertEquals("Status doesn't match", expectedValue,
					fetchText(dynamicXpathLocator(SegmentsPageElements.LIST_STATUS, segName)));
			verifyPopOverItemVisiblity("Edit", "Disabled", segName);
			verifyPopOverItemVisiblity("Refresh", "Disabled", segName);
			break;
		case "last refresh":
			CustomAssertions.assertFalse("LastRefresh doesnt have a date",
					getColumnValue(segName, category, expectedValue).equals("-"));
			break;
		default:
			CustomAssertions.assertEquals("Column Value doesn't match", expectedValue,
					getColumnValue(segName, category, expectedValue));
			break;
		}
	}

	public void verifyPopOverItemVisiblity(String popupItem, String visiblity, String name) {
		click(dynamicXpathLocator(SegmentsPageElements.OPEN_POP_OVER, name));
		if (visiblity.equalsIgnoreCase("Enabled")) {
			CustomAssertions.assertFalse(popupItem + " is disabled",
					fetchAttributeValue(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, popupItem), "class")
							.contains("disabled"));
		} else {
			CustomAssertions.assertTrue(popupItem + " is Enabled",
					fetchAttributeValue(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, popupItem), "class")
							.contains("disabled"));
		}
	}

	public String getColumnValue(String name, String columnName, String value) {
		List<WebElement> columns = findElements(CommonPageElements.TABLE_HEADER.locate());
		int index = 0;
		for (int i = 0; i < columns.size(); i++) {
			if (columns.get(i).getText().equalsIgnoreCase(columnName))
				break;

			index++;
		}
		return fetchText(dynamicXpathLocator(SegmentsPageElements.TABLE_VALUE, name, String.valueOf(index)));
	}

	public void saveSegmentID(String segName) {
		waitUntilAttributeDoesNotContain(SegmentsPageElements.CARD_CONTENT.locate(), "class", "loader");
		comPage.clickPopUpItem(segName, "Edit");
		String segmentId = getCurrentURL().split("/")[getCurrentURL().split("/").length - 2];
		setSegmentId(segmentId);
		Log.info("Segment ID: " + getSegmentId());
		APIUtility.getRuntimeValues().put("SegmentId", segmentId);
		runTimeUtility.setDynamicRequest("SegmentId", "URL");
		openSegmentType("Lists");
	}

	public void verifyauditLogValues(String type, String segmentName) {
		click(JourneysPageElements.JOURNEY_DETAILS.locate());
		click(JourneysPageElements.AUDIT_LOG_VIEW.locate());
		String status = findElement((dynamicXpathLocator(CommonPageElements.TEXT_CONTAINS_SPAN, type))).getText();
		CustomAssertions.assertTrue("Element is not present:", status.contains(type));
		CustomAssertions.assertTrue("Segment is not present:", status.contains(segmentName));
		click(EmailCampaignPageElements.PREVIEW_EMAIL_CLOSE.locate());
	}

	public void updateSegmentName(String segName) {
		clearFieldAndSendkeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Segment Name"), segName);
		setSegmentName(segName);
	}

}
