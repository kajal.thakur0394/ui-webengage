package com.webengage.ui_automation.pages;

import com.webengage.ui_automation.elements.*;
import com.webengage.ui_automation.utils.Log;
import com.webengage.ui_automation.utils.SeleniumExtendedUtility;
import org.openqa.selenium.WebElement;

import java.util.List;

public class WhatsAppCampaignPage extends SeleniumExtendedUtility {
    public void fillMessageTab(List<List<String>> choices) {
        boolean variablesFilled = false;
        for (List<String> choice : choices) {
            String parameter = choice.get(0);
            String value = choice.get(1);
            try {
				if (parameter.contains("Variable")) {
                    sendKeys(dynamicXpathLocator(WhatsAppCampaignPageElements.VARIABLE_TEXTBOX, parameter.split(" ")[1]), value);
                    variablesFilled = true;
                } else if (parameter.equalsIgnoreCase("Media File")) {
                    if(value.equalsIgnoreCase("Upload Image"))
                       uploadFile("icon.png",IntegrationsPageElements.UPLOAD.locate());
                    else if (value.equalsIgnoreCase("Upload Video")) {
                        uploadFile("sample_video.mp4",IntegrationsPageElements.UPLOAD.locate());
                    } else if(value.contains("sendImageURL")) {
                        sendKeys(dynamicXpathLocator(InlineContentWebPerPageElements.IMAGE, parameter), value.split(">")[1]);
                    }
                    else if (value.contains("sendVideoURL")) {
                        sendKeys(dynamicXpathLocator(WhatsAppCampaignPageElements.VIDEO,parameter),value.split(">")[1]);
                    }
                }
            }
            catch (NullPointerException e){
                Log.warn("No text field to be filled");
            }
        }
        if(!variablesFilled) {
            fillVariablesInTemplate();
        }
        saveAndContinueButton();
    }

    public void fillVariablesInTemplate(){
       int valueInt = 1;
        if (checkIfElementisLocated(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT,"Variables"))){
           List <WebElement> el = findElements(WhatsAppCampaignPageElements.INPUT_VARIABLES.locate());
           for (WebElement element : el){
               element.sendKeys("Value " +valueInt);
               valueInt++;
           }
       }
    }

}
