package com.webengage.ui_automation.pages;

import java.util.List;
import org.openqa.selenium.WebElement;

import com.webengage.ui_automation.elements.CommonPageElements;
import com.webengage.ui_automation.elements.PushCampaignPageElements;
import com.webengage.ui_automation.utils.SeleniumExtendedUtility;

public class PushCampaignsPage extends SeleniumExtendedUtility {

	/* This method will click on the targeted device send as parameter. */
	public void selectTargetDevices(String device) {
		click(dynamicXpathLocator(PushCampaignPageElements.DEVICE, device));

	}

	/* This method will click on the targeted layout send as parameter. */
	public void selectTemplate(String template) {
		click(dynamicXpathLocator(PushCampaignPageElements.TEMPLATE, template));
	}

	/*
	 * This method will enter the details according to the 'label' passed as an
	 * argument.
	 */
	public void enterMessageDetails(String label, List<List<String>> choice) {
		if (("Basic").equalsIgnoreCase(label))
			enterBasicDetails(choice);
		else if (("Advanced Options").equalsIgnoreCase(label))
			enterAdvanceOptionsDetails(choice);
		else if (("Image").equalsIgnoreCase(label))
			enterImageDetails(choice);
		else if (("Carousel Images").equalsIgnoreCase(label))
			enterCarouselImageDetails(choice);
		else if (("Rating").equalsIgnoreCase(label))
			enterRatingDetails(choice);
	}

	/* This method enters all the details for 'Rating' Layout. */
	private void enterRatingDetails(List<List<String>> choices) {
		for (List<String> choice : choices) {
			String parameter = choice.get(0);
			String value = choice.get(1);
			if (("Title").equalsIgnoreCase(parameter)) {
				sendKeys(PushCampaignPageElements.RATING_TITLE.locate(), value);
			} else if (("Description").equalsIgnoreCase(parameter)) {
				sendKeys(PushCampaignPageElements.RATING_DESCRIPTION.locate(), value);
			} else if (("Background Image").equalsIgnoreCase(parameter)) {
				sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_LABEL_INPUT,"Background Image"), value);
			}
		}

	}

	/*
	 * This method adds the detail in both Image1 and Image2 Container of 'Carousel'
	 * Layout.
	 */
	private void enterCarouselImageDetails(List<List<String>> choices) {
		for (List<String> choice : choices) {
			String parameter = choice.get(0);
			String value = choice.get(1);
			enterDetails(parameter, value);
		}

	}

	private void enterDetails(String parameter, String value) {
		List<WebElement> el = findElements(dynamicXpathLocator(CommonPageElements.LABEL_TEXT_FIELD, parameter));
		for (WebElement element : el) {
			element.sendKeys(value);
		}
	}

	/*
	 * This method enters the detail inside 'on-click action' url' depending on the
	 * type of device(android or iOS) selected.
	 */
	private void enterAdvanceOptionsDetails(List<List<String>> choices) {
		click(PushCampaignPageElements.ADV_OPTION_TOGGLE.locate());
		for (List<String> choice : choices) {
			String parameter = choice.get(0);
			String value = choice.get(1);
			if (("On-click action: android").equalsIgnoreCase(parameter)) {
				sendKeys(PushCampaignPageElements.ANDROID_ONCLICK_URL.locate(), value);
			} else if (("On-click action: iOS").equalsIgnoreCase(parameter)) {
				sendKeys(PushCampaignPageElements.IOS_ONCLICK_URL.locate(), value);
			}
		}
	}

	/* This method enters the image details in 'Banner' Layout. */
	private void enterImageDetails(List<List<String>> choices) {
		for (List<String> choice : choices) {
			sendKeys(PushCampaignPageElements.BANNER_IMAGE.locate(), choice.get(1));
		}

	}

	/* This method enters the detail inside 'Advance Option(Android)' card. */
	private void enterAdvanceOptionsAndroidDetails(String template, List<List<String>> choices) {
		if (!("Rating").equalsIgnoreCase(template))
			click(PushCampaignPageElements.ANDROID_ADV_OPTION.locate());
		for (List<String> choice : choices) {
			String parameter = choice.get(0);
			String value = choice.get(1);
			if (("On-click action: android").equalsIgnoreCase(parameter)) {
				sendKeys(PushCampaignPageElements.ANDROID_ONCLICK_URL.locate(), value);
			} else if (("Label").equalsIgnoreCase(parameter)) {
				click(PushCampaignPageElements.BUTTONS.locate());
				switchToiFrame(PushCampaignPageElements.TITLE_IFRAME_BUTTON.locate());
				sendKeys(PushCampaignPageElements.TITLE_SECTION_BUTTON.locate(), value);
				switchToDefaultContent();
			} else if (("On-Click Action").equalsIgnoreCase(parameter)) {
				sendKeys(dynamicXpathLocator(PushCampaignPageElements.TITLE_INPUT, parameter), value);
			}
		}

	}

	/* This method enters the detail inside 'Basic' card. */
	private void enterBasicDetails(List<List<String>> choices) {
		for (List<String> choice : choices) {
			String parameter = choice.get(0);
			String value = choice.get(1);
			if (("Title").equalsIgnoreCase(parameter)) {
				switchToiFrame(PushCampaignPageElements.TITLE_IFRAME.locate());
				sendKeys(PushCampaignPageElements.TITLE_SECTION.locate(), value);
				switchToDefaultContent();
			} else if (("Message").equalsIgnoreCase(parameter)) {
				switchToiFrame(PushCampaignPageElements.DESCRIPTION_IFRAME.locate());
				sendKeys(PushCampaignPageElements.DESCRIPTION_SECTION.locate(), value);
				switchToDefaultContent();
			}
		}
	}

	public void saveDetails() {
		saveAndContinueButton();
		if (checkIfElementisLocated(dynamicXpathLocator(CommonPageElements.BUTTON,"Proceed")))
			click(dynamicXpathLocator(CommonPageElements.BUTTON,"Proceed"));
	}

	public void enterMessageDetails(String label, String template, List<List<String>> choice) {
		if (("Advanced Options (Android)").equalsIgnoreCase(label))
			enterAdvanceOptionsAndroidDetails(template, choice);
		if (("Advanced Options (iOS)").equalsIgnoreCase(label))
			enterAdvanceOptionsiOSDetails(template, choice);

	}

	/* This method enters the detail inside 'Advance Option(iOS)' card. */
	private void enterAdvanceOptionsiOSDetails(String template, List<List<String>> choices) {
		if (!("Rating").equalsIgnoreCase(template))
			click(PushCampaignPageElements.IOS_ADV_OPTION.locate());
		for (List<String> choice : choices) {
			String parameter = choice.get(0);
			String value = choice.get(1);
			if (("On-click action: iOS").equalsIgnoreCase(parameter)) {
				sendKeys(PushCampaignPageElements.IOS_ONCLICK_URL.locate(), value);
			} else if (("Type of Buttons").equalsIgnoreCase(parameter)) {
				click(PushCampaignPageElements.BUTTONS.locate());
				scrollIntoView(PushCampaignPageElements.OPEN_DD.locate());
				openDropdown(PushCampaignPageElements.OPEN_DD.locate());
				click(dynamicXpathLocator(PushCampaignPageElements.SELECT_DD, value));
			} else if (("On-Click Action").equalsIgnoreCase(parameter)) {
				sendKeys(dynamicXpathLocator(PushCampaignPageElements.IOS_BTN_CLICK, parameter), value);
			}

		}
	}

	public void enterPersonalisationDetails(String card) {
		if (("Basic").equalsIgnoreCase(card)) {
			click(dynamicXpathLocator(PushCampaignPageElements.SELECT_PERSONALIZATION, "Title"));
			selectPersonalizedAttribute();
			click(dynamicXpathLocator(PushCampaignPageElements.SELECT_PERSONALIZATION, "Message"));
			selectPersonalizedAttribute();
		} else if (("Advanced Options (Android)").equalsIgnoreCase(card)) {
			click(dynamicXpathLocator(PushCampaignPageElements.SELECT_PERSONALIZATION, "Label"));
			selectPersonalizedAttribute();
		} else if (("Carousel Image").equalsIgnoreCase(card)) {
			List<WebElement> el = findElements(
					dynamicXpathLocator(PushCampaignPageElements.SELECT_PERSONALIZATION, "Label"));
			for (WebElement element : el) {
				element.click();
				selectPersonalizedAttribute();
			}
		}

	}

	private void selectPersonalizedAttribute() {
		click(PushCampaignPageElements.ATTRIBUTE_TYPE.locate());
		click(PushCampaignPageElements.SELECT_ATTRIBUTE.locate());

	}

}
