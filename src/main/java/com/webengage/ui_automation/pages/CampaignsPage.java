package com.webengage.ui_automation.pages;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.webengage.ui_automation.driver.DataFactory;
import com.webengage.ui_automation.elements.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import com.google.gson.Gson;
import com.webengage.ui_automation.utils.APIUtility;
import com.webengage.ui_automation.utils.ConstantUtils;
import com.webengage.ui_automation.utils.CustomAssertions;
import com.webengage.ui_automation.utils.Log;
import com.webengage.ui_automation.utils.NotificationToastMessages;
import com.webengage.ui_automation.utils.ReflectionsUtility;
import com.webengage.ui_automation.utils.RuntimeUtility;
import com.webengage.ui_automation.utils.SeleniumExtendedUtility;
import com.webengage.ui_automation.utils.SetupUtility;

import io.cucumber.datatable.DataTable;

public class CampaignsPage extends SeleniumExtendedUtility {

	APIUtility apiUtility = new APIUtility();
	APIOperationsPage apiOperations = new APIOperationsPage();
	SetupUtility setupUtility = new SetupUtility();
	RuntimeUtility runTimeUtility = new RuntimeUtility();
	ReflectionsUtility refl = new ReflectionsUtility();
	CommonPage comPage = new CommonPage();

	public static void initializeVariables() {
		setSystemDateTimeAvailable(false);
		setSecondaryNamespace(null);
		setPrimaryNamespace(null);
	}

	public static String getSystemDateTime() {
		return DataFactory.getInstance().getData(ConstantUtils.SYSTEM_DATETIME, String.class);
	}

	public static void setSystemDateTime(String systemDateTime) {
		DataFactory.getInstance().setData(ConstantUtils.SYSTEM_DATETIME, systemDateTime);
	}

	public static boolean isSystemDateTimeAvailable() {
		return DataFactory.getInstance().getData(ConstantUtils.IS_SYSTEM_DATETIME_AVAILABLE, Boolean.class);
	}

	public static void setSystemDateTimeAvailable(boolean isSystemDateTimeAvailable) {
		DataFactory.getInstance().setData(ConstantUtils.IS_SYSTEM_DATETIME_AVAILABLE, isSystemDateTimeAvailable);
	}

	public static String getCampaignName() {
		return DataFactory.getInstance().getData(ConstantUtils.CAMPAIGNNAME, String.class);
	}

	public static void setCampaignName(String campaignName) {
		DataFactory.getInstance().setData(ConstantUtils.CAMPAIGNNAME, campaignName);
	}

	public static String getCampaignType() {
		return DataFactory.getInstance().getData(ConstantUtils.CAMPAIGNTYPE, String.class);
	}

	public static void setCampaignType(String campaignType) {
		DataFactory.getInstance().setData(ConstantUtils.CAMPAIGNTYPE, campaignType);
	}

	public static void setCampaignID(String campaignID) {
		DataFactory.getInstance().setData(ConstantUtils.CAMPAIGNID, campaignID);
	}

	public static void setTemplateId(String templateId) {
		DataFactory.getInstance().setData(ConstantUtils.TEMPLATEID, templateId);
	}

	public static String getPrimaryNamespace() {
		return DataFactory.getInstance().getData(ConstantUtils.PRIMARY_NAMESPACE, String.class);
	}

	public static void setPrimaryNamespace(String primaryNamespace) {
		DataFactory.getInstance().setData(ConstantUtils.PRIMARY_NAMESPACE, primaryNamespace);
	}

	public static String getSecondaryNamespace() {
		return DataFactory.getInstance().getData(ConstantUtils.SECONDARY_NAMESPACE, String.class);
	}

	public static void setSecondaryNamespace(String primaryNamespace) {
		DataFactory.getInstance().setData(ConstantUtils.SECONDARY_NAMESPACE, primaryNamespace);
	}

	public static String getCampaignID() {
		return DataFactory.getInstance().getData(ConstantUtils.CAMPAIGNID, String.class);
	}

	public static String getTemplateId() {
		return DataFactory.getInstance().getData(ConstantUtils.TEMPLATEID, String.class);
	}

	public static void setAllReachableUserCount(String reachableuser) {
		DataFactory.getInstance().setData(ConstantUtils.REACHABLE_USER, reachableuser);
	}

	public static String getAllReachableUserCount() {
		return DataFactory.getInstance().getData(ConstantUtils.REACHABLE_USER, String.class);
	}

	public static String getJsonTemplate() {
		return DataFactory.getInstance().getData(ConstantUtils.JSONTEMPLATE, String.class);
	}

	public static void setJsonTemplate(String jsonTemplate) {
		DataFactory.getInstance().setData(ConstantUtils.JSONTEMPLATE, jsonTemplate);
	}

	/**
	 * 
	 * This method will first check if respective section is expanded. If it is
	 * collapsed, it will click to expand the corresponding list and then click on
	 * tab that has been passed as an argument.
	 * 
	 * @param subSection
	 * @param section
	 */
	public void openPageOnDashboard(String subSection, String section) {
		if (!fetchAttributeValue(dynamicXpathLocator(CommonPageElements.SECTION_TAB_LIST, section), "class")
				.contains("active"))
			click(dynamicXpathLocator(CommonPageElements.SECTION_TAB_LIST, section));
		click(dynamicXpathLocator(CampaignsPageElements.SUB_SECTION, section, subSection));
		checkForUnsavedChanges();
	}

	public void saveCampaignID(boolean backButtonPresent) {
		if (backButtonPresent) {
			waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.BUTTON, "BACK"));
		}
		String campaignId = getCurrentURL().split("/")[getCurrentURL().split("/").length - 2];
		setCampaignID(campaignId);
		Log.info("CAMPAIGNID: " + getCampaignID());
		APIUtility.getRuntimeValues().put(ConstantUtils.CAMPAIGNID, campaignId);
		runTimeUtility.setDynamicRequest(ConstantUtils.CAMPAIGNID, "URL");
	}

	/* This method will click on the Segment Creation button */
	public void selectSegmentCreationButton() {
		click(CampaignsPageElements.SEGMENT_CREATION_ICON.locate());
	}

	/**
	 * @param{segmentName} This method will enter the segment named as passed as an
	 *                     argument.
	 */
	public void addSegmentName(String segmentType, String segmentName) {
		sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, segmentType), segmentName);
	}

	/**
	 * This method will Save the Segment created and it will also verify whether
	 * segment has been successfully created by ToastMessage.
	 */
	public void verifySegment(String type) {
		switch (type.toLowerCase()) {
		case "segment":
			verifyToastMessage(NotificationToastMessages.SEGMENT_CREATED.message());
			break;
		case "list":
			verifyToastMessage(NotificationToastMessages.LIST_CREATED.message());
			break;
		default:
			break;
		}
	}

	/*
	 * 
	 * This method will search for SegmentUsers from drop down and will select the
	 * desired value.
	 */
	public void selectSegmentUsers(String name, String type) {
		scrollIntoView(dynamicXpathLocator(CampaignsPageElements.SEG_USER_TYPE, type));
		openDropdown(dynamicXpathLocator(CampaignsPageElements.SEG_USER_TYPE, type));
		click(dynamicXpathLocator(CampaignsPageElements.SELECT_FROM_DD, name));
		if (type.toLowerCase().equals("exclude")
				&& !checkIfElementisLocated(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "TARGET DEVICES")))
			waitForElementToBeVisible(CampaignsPageElements.AUDIENCE_REACAHBLE_SECTION.locate());
	}

	/* This method will click on 'Add Criteria' button. */
	public void selectCriteria(boolean excludeBoolean) {
		if (excludeBoolean)
			click(CommonPageElements.ADD_CRITERIA_BUTTON.locate());
	}

	public void selectExcludeSegmentCreationButton() {
		click(CampaignsPageElements.EXCLUDE_SEGMENT_CREATION_ICON.locate());
	}

	public void createNew(String type) {
		click(CampaignsPageElements.CREATE_BTN.locate());
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, type));
	}

	public void fillAudience(List<List<String>> choices) {
		for (List<String> choice : choices) {
			String parameter = choice.get(0);
			String value = choice.get(1);
			sendKeys(dynamicXpathLocator(CampaignsPageElements.ENTER_TEXT_FOR, parameter), value);
		}
		saveAndContinueButton();
	}

	public void fillWhen() {
		saveAndContinueButton();
	}

	public void fillMessage(List<List<String>> choices) {
		for (List<String> choice : choices) {
			String parameter = choice.get(0);
			String value = choice.get(1);
			if (parameter.equals("ESP") || parameter.equals("SSP")) {
				click(dynamicXpathLocator(CampaignsPageElements.OPEN_DD_SELECT, parameter, "Select..."));
				click(dynamicXpathLocator(CampaignsPageElements.SELECT_FROM_DD, value));
			}
		}
	}

	public void deleteCampaign(String campaignName) {
		click(dynamicXpathLocator(CampaignsPageElements.OPEN_POP_OVER, campaignName));
		click(CampaignsPageElements.POP_OVER_DELETE.locate());
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "DELETE"));
		verifyToastMessage(NotificationToastMessages.DELETE_CAMPAIGN.message());
	}

	public void selectWATemplate(String templateName, String wspName) throws InterruptedException {
		TimeUnit.SECONDS.sleep(2);
		waitForElementToBePresent(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Select a Template"));
		sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Search by template name"), templateName);
		click(dynamicXpathLocator(WhatsAppCampaignPageElements.SELECT_WA_TEMPLATE, wspName, templateName));
	}

	public void setConversionTracking() {
		saveAndContinueButton();
	}

	public void verifyTargetDevices(String platform, String disabledPlatforms) {
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, platform));
		String checkAttr = fetchAttributeValue(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, platform), "class");
		CustomAssertions.assertFalse("Attributes do not match:", checkAttr.contains("is-disabled"));
		CustomAssertions.assertTrue("Attributes do not match:", checkAttr.contains("is-checked"));
	}

	public void fillSendTextBlock(JSONObject data) {
		fillBasicInfo((JSONObject) data.get("Basic Info"));
		fillSendTextMessageDetails((JSONObject) data.get("Message"));
		click(CampaignsPageElements.SKIP_TEST_BTN.locate());
	}

	private void fillBasicInfo(JSONObject basicInfo) {
		sendKeys(dynamicXpathLocator(CampaignsPageElements.ENTER_TEXT_FOR, "CAMPAIGN NAME"),
				(String) basicInfo.get("CAMPAIGN NAME"));
		click(CampaignsPageElements.SAVE_CONTINUE_BTN.locate());
	}

	private void fillSendTextMessageDetails(JSONObject message) {
		if (message.containsKey("directory")) {
			readFromTemplate(message, "Message");
		} else {
			sendKeys(dynamicXpathLocator(CampaignsPageElements.ENTER_TEXT_FOR, "Sender"),
					(String) message.get("Sender"));
			String spName = (String) message.get("SSP");
			click(dynamicXpathLocator(CampaignsPageElements.OPEN_DD, "SSP"));
			click(dynamicXpathLocator(CampaignsPageElements.SELECT_FROM_DD, spName));
			sendKeys(dynamicXpathLocator(CampaignsPageElements.ENTER_TEXT_FOR, "Message"),
					(String) message.get("Message"));
			if (((String) message.get("SSP")).contains("Infobip")) {
				sendKeys(dynamicXpathLocator(CampaignsPageElements.ENTER_TEXT_FOR, "DLT TEMPLATE ID"),
						(String) message.get("TemplateId"));
			}
			click(CampaignsPageElements.SAVE_CONTINUE_BTN.locate());
		}
	}

	public void selectSMSSSPName(String selectSSP) {
		scrollIntoView(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "SMS Service Provider"));
		click(dynamicXpathLocator(CampaignsPageElements.OPEN_DD, "SSP"));
		try {
			click(dynamicXpathLocator(CampaignsPageElements.SELECT_FROM_DD, selectSSP));
		} catch (TimeoutException e) {
			Log.debug("Failed to interact with SSP dropdown. Retrying . . .");
			click(dynamicXpathLocator(CampaignsPageElements.OPEN_DD, "SSP"));
			click(dynamicXpathLocator(CampaignsPageElements.SELECT_FROM_DD, selectSSP));
		}
	}

	public void selectServiceProvider(String selectSP, String valueToChoose) {
		click(dynamicXpathLocator(CampaignsPageElements.OPEN_DD, selectSP));
		click(dynamicXpathLocator(CampaignsPageElements.SELECT_FROM_DD, valueToChoose));
	}

	public void enterSMSSSPMessageDetails(List<List<String>> choices) {
		for (List<String> choice : choices) {
			String parameter = choice.get(0);
			String value = choice.get(1);
			if (parameter.equalsIgnoreCase("Sender")) {
				sendKeys(CampaignsPageElements.SENDER.locate(), value);

			} else if (parameter.equalsIgnoreCase("Message")) {
				sendKeys(CampaignsPageElements.MESSAGE_TEXTAREA.locate(), value);

			} else if (parameter.equalsIgnoreCase("DLT Template ID")) {
				sendKeys(CampaignsPageElements.DLT_TEMPLATE_ID.locate(), value);
			}
		}
	}

	public void fillSendPushBlock(JSONObject data) {
		fillSendPushBasicInfo((JSONObject) data.get("Basic Info"));
		fillSendPushMessageDetails((JSONObject) data.get("Message"));
		click(CampaignsPageElements.SKIP_TEST_BTN.locate());
	}

	private void fillSendPushBasicInfo(JSONObject basicInfo) {
		sendKeys(dynamicXpathLocator(CampaignsPageElements.ENTER_TEXT_FOR, "CAMPAIGN NAME"),
				(String) basicInfo.get("CAMPAIGN NAME"));
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, (String) basicInfo.get("TARGET DEVICES")));
		click(CampaignsPageElements.SAVE_CONTINUE_BTN.locate());
	}

	private void fillSendPushMessageDetails(JSONObject message) {
		if (message.containsKey("directory")) {
			readFromTemplate(message, "Message");
		} else {
			click(dynamicXpathLocator(CampaignsPageElements.SELECT_TEMPLATE, (String) message.get("Layout")));
			switchToiFrame(PushCampaignPageElements.TITLE_IFRAME.locate());
			sendKeys(PushCampaignPageElements.TITLE_SECTION.locate(), (String) message.get("Title"));
			switchToDefaultContent();
			switchToiFrame(PushCampaignPageElements.DESCRIPTION_IFRAME.locate());
			sendKeys(PushCampaignPageElements.DESCRIPTION_SECTION.locate(), (String) message.get("Message"));
			switchToDefaultContent();
			click(CampaignsPageElements.SAVE_CONTINUE_BTN.locate());
		}
	}

	public void fillSendMailBlock(JSONObject data) {
		fillBasicInfo((JSONObject) data.get("Basic Info"));
		fillSendMailMessageBlock((JSONObject) data.get("Message"));
		click(CampaignsPageElements.SKIP_TEST_BTN.locate());
	}

	private void fillSendMailMessageBlock(JSONObject message) {
		if (message.containsKey("directory")) {
			readFromTemplate(message, "Message");
		} else {
			click(dynamicXpathLocator(CampaignsPageElements.OPEN_DD, "ESP"));
			click(dynamicXpathLocator(CampaignsPageElements.SELECT_FROM_DD, (String) message.get("ESP")));
			sendKeys(dynamicXpathLocator(CampaignsPageElements.ENTER_TEXT_FOR, "From name"),
					(String) message.get("From name"));
			sendKeys(dynamicXpathLocator(CampaignsPageElements.ENTER_TEXT_FOR, "From email"),
					(String) message.get("From email"));
			sendKeys(dynamicXpathLocator(CampaignsPageElements.ENTER_TEXT_FOR, "Subject"),
					(String) message.get("Subject"));
			clearFieldAndSendkeys(CampaignsPageElements.EMAIL_BODY.locate(), (String) message.get("Body"));
			click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Insert Unsubscribe link"));
			click(CampaignsPageElements.SAVE_CONTINUE_BTN.locate());
		}
	}

	public void fillWABlock(JSONObject data) throws InterruptedException {
		fillBasicInfo((JSONObject) data.get("Basic Info"));
		selectTemplate((JSONObject) data.get("Message"));
		click(CampaignsPageElements.SKIP_TEST_BTN.locate());
	}

	private void selectTemplate(JSONObject message) throws InterruptedException {
		if (message.containsKey("directory")) {
			readFromTemplate(message, "Message");
		} else {
			selectWATemplate((String) message.get("Template"), (String) message.get("WSP Name"));
			saveAndContinueButton();
		}
	}

	/*
	 * This method dynamically attach 'CampaignId' in the api URI. It also calls
	 * another method(fetchSegList) in order to return SegmentId List.
	 */
	public List<String> fetchSegmentListfromAPI(String apiName, boolean isList, String id) throws IOException {
		APIUtility.getRuntimeValues().put(ConstantUtils.CAMPAIGNID, getCampaignID());
		runTimeUtility.setDynamicRequest(ConstantUtils.CAMPAIGNID, "URL");
		setupUtility.setApiURI(apiName);
		apiUtility.getRequest(runTimeUtility.modifyAPIURL(setupUtility.getApiURI()));
		return fetchSegList(apiName, isList);
	}

	public void fetchDynamicAPI(String apiName, String id) {
		APIUtility.getRuntimeValues().put(ConstantUtils.CAMPAIGNID, id);
		runTimeUtility.setDynamicRequest(ConstantUtils.CAMPAIGNID, "URL");
		try {
			setupUtility.setApiURI(apiName);
			runTimeUtility.modifyAPIURL(setupUtility.getApiURI());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/*
	 * This method identifies segment type i.e. 'Live/Static' segments on the basis
	 * of 'userlistId' and returns the list accordingly.
	 */
	public List<String> fetchSegList(String apiName, boolean isList) {
		List<String> inclusionList = new ArrayList<String>();
		List<String> exclusionList = new ArrayList<String>();
		String updatedJsonPath = isList ? "userListIds" : "segmentIds";
		if (apiName.equalsIgnoreCase("fetchInlineContentWebP")) {
			String inclusionSegId = apiUtility.fetchValue("response.data.segment.id");
			inclusionList.add(inclusionSegId);
			return inclusionList;
		} else {
			List<String> totalList = new ArrayList<String>();
			inclusionList = apiUtility.fetchList("response.data.experiment.segmentInclusionDto." + updatedJsonPath);
			exclusionList = apiUtility.fetchList("response.data.experiment.segmentExclusionDto." + updatedJsonPath);
			if (inclusionList != null)
				totalList.addAll(inclusionList);
			if (exclusionList != null)
				totalList.addAll(exclusionList);
			return totalList;
		}
	}

	/**
	 * This method extracts the segmentName of every SegmentIds and assert if the
	 * expected segmentName is the same as extracted segmentName
	 * from @param{List<String> segmentIds}. If the assertion is true, it then
	 * removes that segmentName from the list and returns the segment List size.
	 */
	public int validateSegments(List<String> segments, List<String> segmentIds, String apiURI) throws IOException {
		for (String segId : segmentIds) {
			String segmentName = "";
			APIUtility.getRuntimeValues().put("SegmentId", segId);
			runTimeUtility.setDynamicRequest("SegmentId", "URL");
			setupUtility.setApiURI(apiURI);
			apiUtility.getRequest(runTimeUtility.modifyAPIURL(setupUtility.getApiURI()));
			String actualApiURI = setupUtility.getApiURI();
			if (actualApiURI.contains("userlist")) {
				segmentName = apiUtility.fetchValue("response.data.name");
			} else {
				segmentName = apiUtility.fetchValue("response.data.trafficSegmentDto.name");
			}
			for (String expectedSegment : segments) {
				if (expectedSegment.equals(segmentName)) {
					segments.remove(segmentName);
					break;
				}
			}
		}
		return segments.size();
	}

	private String getOrdinalDate(String date) {
		String actualDate = convertDate(date);
		return calculateOrdinalDate(actualDate);
	}

	public static String systemDateTime() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
		Date date = new Date();
		setSystemDateTime(formatter.format(date).toLowerCase());
		return getSystemDateTime();
	}

	private String convertDate(String dateString) {
		String parseDate = "";
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat formatter1 = new SimpleDateFormat("dd MMM yy");
		try {
			parseDate = formatter1.format(formatter.parse(dateString));
		} catch (ParseException e) {
		}
		return parseDate;
	}

	public String calculateOrdinalDate(String date) {
		String[] arr = date.split(" ");
		int day = Integer.parseInt(arr[0]);
		String y = String.valueOf(day) + getDayNumberSuffix(day);
		String newDate = y.concat(" ").concat(arr[1]).concat(" ").concat(arr[2]);
		return newDate;

	}

	public String newFormatDate(String dateString) {
		Pattern p = Pattern.compile("([0-9])");
		Matcher m = p.matcher(dateString);
		if (!m.find()) {
			return dateString;
		}
		String parseDate = "";
		String[] str = dateString.split(" ");
		dateString = str[0].substring(0, str[0].length() - 2).concat(" ").concat(str[1]).concat(" ").concat(str[2]);
		DateFormat formatter = new SimpleDateFormat("d MMM yyyy");
		DateFormat formatter1 = new SimpleDateFormat("dd MMM ''yy");
		try {
			parseDate = formatter1.format(formatter.parse(dateString));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return parseDate;
	}

	private String getDayNumberSuffix(int day) {
		if (day >= 11 && day <= 13) {
			return "th";
		}
		switch (day % 10) {
		case 1:
			return "st";
		case 2:
			return "nd";
		case 3:
			return "rd";
		default:
			return "th";
		}
	}

	private String concatDateTime(String date, String time) {
		String dateTime = date.concat(", ").concat(time);
		return dateTime;
	}

	public String getDateTime(String parameter) {
		// String startDate =
		// fetchAttributeValue(dynamicXpathLocator(SMSCampaignPageElements.DATE,
		// parameter), "value");
		// String newStartDate = getOrdinalDate(startDate);
		String newStartDate = getDate(parameter);
		String newTime = fetchTime(parameter);
		return concatDateTime(newStartDate, newTime);

	}

	public String getDate(String parameter) {
		String startDate = fetchAttributeValue(dynamicXpathLocator(SMSCampaignPageElements.DATE, parameter), "value");
		return getOrdinalDate(startDate);
	}

	public String getNowDateTime(String parameter) {
		String date = fetchText(dynamicXpathLocator(SMSCampaignPageElements.ANOTHER_DATE, parameter));
		return concatDateTime(date, fetchTime(parameter));

	}

	private String fetchTime(String parameter) {
		String hours = fetchAttributeValue(dynamicXpathLocator(SMSCampaignPageElements.HOUR, parameter), "value");
		int hour = Integer.parseInt(hours);
		hours = hour > 0 && hour < 10 ? "0".concat(hours) : hours;
		String minutes = fetchAttributeValue(dynamicXpathLocator(SMSCampaignPageElements.MINUTES, parameter), "value");
		String wrapper = fetchAttributeValue(dynamicXpathLocator(SMSCampaignPageElements.WRAPPER, parameter), "value");
		String time = hours.concat(":").concat(minutes).concat(wrapper);
		return time;

	}

	public String fetchSpecificTime() {
		String hours = fetchAttributeValue(CampaignsPageElements.SPECIFIC_HOUR.locate(), "value");
		int hour = Integer.parseInt(hours);
		hours = hour > 0 && hour < 10 ? "0".concat(hours) : hours;
		String minutes = fetchAttributeValue(CampaignsPageElements.SPECIFIC_MINUTE.locate(), "value");
		String wrapper = fetchAttributeValue(CampaignsPageElements.SPECIFIC_WRAPPER.locate(), "value");
		String time = hours.concat(":").concat(minutes).concat(wrapper);
		return time;

	}

	public void selectTab(String tabName) {
		waitUntilAttributeContains(dynamicXpathLocator(CampaignsPageElements.TEMPLATE_WITH_ATTRIBUTES, tabName),
				"class", "completed");
		click(dynamicXpathLocator(CampaignsPageElements.TEMPLATE_WITH_ATTRIBUTES, tabName));
		waitUntilAttributeContains(dynamicXpathLocator(CampaignsPageElements.TEMPLATE_WITH_ATTRIBUTES, tabName),
				"class", "active");
	}

	public void adjustRecurringTime() {
		DateFormat df = new SimpleDateFormat("MMMM yyyy");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		int minute = calendar.get(Calendar.MINUTE);
		String dateFor = "DELIVERY SCHEDULE";
		if ((minute % 10) % 5 == 0) {
			calendar.set(Calendar.MINUTE, minute + (5 - ((minute % 10) % 5)));
			click(dynamicXpathLocator(CampaignsPageElements.DATE_PICKER, "START DATE"));
			while (!fetchText(JourneysPageElements.MONTH_YEAR.locate()).equals(df.format(calendar.getTime()))) {
				click(JourneysPageElements.NEXT_MONTH.locate());
			}
			click(dynamicXpathLocator(JourneysPageElements.SELECT_CAL_DAY, df.format(calendar.getTime()).split(" ")[0]
					+ " " + String.valueOf(calendar.get(Calendar.DAY_OF_MONTH))));
			click(dynamicXpathLocator(CampaignsPageElements.HOURS_DD, dateFor));
			click(dynamicXpathLocator(CampaignsPageElements.SELECT_FROM_DD,
					String.valueOf(calendar.get(Calendar.HOUR) == 0 ? 12 : calendar.get(Calendar.HOUR))));
			click(dynamicXpathLocator(CampaignsPageElements.MINUTES_DD, dateFor));
			String mins = String.valueOf(calendar.get(Calendar.MINUTE));
			click(dynamicXpathLocator(CampaignsPageElements.SELECT_FROM_DD, mins.length() == 1 ? "0" + mins : mins));
			click(dynamicXpathLocator(CampaignsPageElements.AM_PM_DD, dateFor));
			click(dynamicXpathLocator(CampaignsPageElements.SELECT_FROM_DD,
					calendar.get(Calendar.AM_PM) == 1 ? "pm" : "am"));
			try {
				TimeUnit.MINUTES.sleep(3);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * @param{campaignType},{campaignName} This method will click on
	 *                                     'AnyCampaignCreation' icon and will
	 *                                     select the 'CampaignType'. It will also
	 *                                     enter the CampaignName.
	 */
	public void createCampaignWithType(String campaignType, String campaignName) {
		setCampaignType(campaignType);
		selectCampaignType(campaignType);
		sendKeys(SMSCampaignPageElements.CAMPAIGN_NAME_TEXTFIELD.locate(), campaignName);
	}

	public void launchCampaignButton() throws InterruptedException {
		TimeUnit.SECONDS.sleep(3);
		waitUntilAttributeDoesNotContain(CommonPageElements.LAUNCH_CAMPAIGN_BUTTON.locate(), "class", "disabled");
		clickUsingJSExecutor(CommonPageElements.LAUNCH_CAMPAIGN_BUTTON.locate());
		TimeUnit.SECONDS.sleep(1);
		if (checkIfElementisLocated(CampaignsPageElements.SEGMENT_BREW.locate())) {
			Log.warn("Segment brewing pop up has appeared. Trying to click on OK");
			click(CampaignsPageElements.SEGMENT_BREW_OK.locate());
		}
		verifyToastMessage(NotificationToastMessages.LAUNCH_CAMPAIGN.message());
	}

	public void verifyViaApiAttachedSegments(String apiName, String segmentType, DataTable dataTable) {
		try {
			boolean isList = segmentType.toLowerCase().contains("list");
			List<String> segments = new LinkedList<String>(dataTable.asList());
			List<String> segmentIds = new ArrayList<String>();
			segmentIds = fetchSegmentListfromAPI(apiName, isList, getCampaignID());
			if (isList) {
				CustomAssertions.assertEquals("Segment size do not match:", segmentIds.size(), segments.size());
				CustomAssertions.assertEquals("Segment size do not match:", 0,
						validateSegments(segments, segmentIds, "fetchStaticSegments"));
			} else {
				CustomAssertions.assertEquals("Segment size do not match:", segmentIds.size(), segments.size());
				CustomAssertions.assertEquals("Segment size do not match:", 0,
						validateSegments(segments, segmentIds, "fetchSegments"));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * @param{campaignName} This method will locate the 'Search input' field and
	 *                      enters the campaign name inside the input field.
	 */
	public void searchBar(String campaignName) {
		waitForElementToBeVisible(SMSCampaignPageElements.TABLE_HEADER.locate());
		if (checkIfElementisLocated(SMSCampaignPageElements.SEARCH_INPUT_BAR_VALUE.locate()))
			click(SMSCampaignPageElements.CLEAR_SEARCH.locate());
		sendKeys(SMSCampaignPageElements.SEARCH_INPUT_BAR.locate(), campaignName + Keys.RETURN);
		click(SMSCampaignPageElements.TABLE_HEADER.locate());
	}

	public void assertTag(String tagName) {
		waitForElementToBeVisible(SegmentsPageElements.TAG.locate());
		if (isSystemDateTimeAvailable())
			tagName = getSystemDateTime();
		CustomAssertions.assertEquals("Tags do not match:", tagName,
				fetchAttributeValue(SegmentsPageElements.TAG.locate(), "title"));
		setSystemDateTimeAvailable(false);
	}

	public void verifyTagsInBothPages(String tagName, String status, String campaignName) {
		if (isSystemDateTimeAvailable()) {
			tagName = getSystemDateTime();
		}
		assertTag(tagName);
		comPage.openCampaign(campaignName);
		By pageToOpen = (status == null || status.equalsIgnoreCase("Running") || status.equalsIgnoreCase("Ended")
				|| status.equalsIgnoreCase("Paused")) ? SegmentsPageElements.SHOWDETAILS.locate()
						: SMSCampaignPageElements.PREVIEWPAGE.locate();
		click(pageToOpen);
		assertTag(tagName);
		setSystemDateTimeAvailable(false);
	}

	public void createNewTag(String tagName) {
		setSystemDateTime(tagName.concat("-".concat(systemDateTime())));
		setSystemDateTimeAvailable(true);
		sendKeys(SegmentsPageElements.ENTER_TAG.locate(), getSystemDateTime());
		click(SegmentsPageElements.ADD_TAG.locate());
	}

	public void openMeatBallTagMenuForCampaign(String campaignName) {
		click(dynamicXpathLocator(CampaignsPageElements.OPEN_POP_OVER, campaignName));
		click(SegmentsPageElements.POP_OVER_TAG.locate());
	}

	public void deleteNewTag(String tagName) {
		click(dynamicXpathLocator(SegmentsPageElements.DELETE_TAG, getSystemDateTime()));
		saveButton();
	}

	public void selectCampaignType(String campaignType) {
		click(CampaignsPageElements.CREATE_BTN.locate());
		if (!campaignType.isEmpty())
			click(dynamicXpathLocator(CampaignsPageElements.CAMPAIGN_TYPE, campaignType));
	}

	public void selectSpecificSegmentIncludeExclude(String segName) {
		scrollIntoView(CampaignsPageElements.SELECT_SEGMENT.locate());
		click(CampaignsPageElements.SELECT_SEGMENT.locate());
		clearFieldAndSendkeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER_CONTAINS, "Search"), segName);
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, segName));
		click(CommonPageElements.SAVE_BUTTON.locate());
		waitForElementToBeVisible(CampaignsPageElements.AUDIENCE_REACAHBLE_SECTION.locate());
	}

	public void selectSingleSegment(String segName) {
		scrollIntoView(CampaignsPageElements.SELECT_SEGMENT.locate());
		click(CampaignsPageElements.SELECT_SEGMENT.locate());
		sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER_CONTAINS, "Search"), segName);
		click(dynamicXpathLocator(CampaignsPageElements.SELECT_FROM_DD, segName));
		waitForElementToBeVisible(CampaignsPageElements.AUDIENCE_REACAHBLE_SECTION.locate());
	}

	public void checkForUnsavedChanges() {
		if (checkIfElementisLocated(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Unsaved Changes in Message")))
			click(dynamicXpathLocator(CommonPageElements.BUTTON, "Discard Changes"));
	}

	public void deleteCampaignsviaCampId(APIOperationsPage apiOperationsPage, LinkedList<String> fetchCampaignids,
			String apiName) throws IOException {
		LinkedList<String> deleteCampId = new LinkedList<>();
		for (String campId : fetchCampaignids) {
			apiOperationsPage.addURLParams(ConstantUtils.CAMPAIGNID, campId);
			apiOperationsPage.getMethod(apiName.replace("delete", "fetch"));
			String container = apiUtility.fetchValue("response.data.experiment.container");
			if (container.equals("JOURNEY")) {
				apiOperationsPage.addURLParams("journeyId",
						apiUtility.fetchValue("response.data.experiment.journeyId"));
				apiOperationsPage.deleteMethod("deleteJourney");
				deleteCampId.add(campId);
			}
		}
		deleteCampId.forEach(s -> {
			fetchCampaignids.remove(s);
		});
		fetchCampaignids.forEach(s -> {
			Log.info("Deleting Campaign having campaign ID as [" + s + "]");
			apiOperationsPage.addURLParams(ConstantUtils.CAMPAIGNID, s);
			try {
				apiOperationsPage.deleteMethod(apiName);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});

	}

	public void fillInAppBlock(JSONObject data) {
		fillSendPushBasicInfo((JSONObject) data.get("Basic Info"));
		fillWhereAndWhen((JSONObject) data.get("Where_When"));
		readFromTemplate((JSONObject) data.get("Message"), "Message");
		if ((JSONObject) data.get("Validity") != null) {
			fillValidityTime((JSONObject) data.get("Validity"));
		}
		saveAndCloseButton();
	}

	public void fillValidityTime(JSONObject time) {
		waitForElementToBeClickable(JourneysPageElements.INPUT_NUMBER.locate());
		clearFieldAndSendkeys(JourneysPageElements.INPUT_NUMBER.locate(), (String) time.get("number"));
		click(CommonPageElements.DD_SELECT_VALUE.locate());
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, (String) time.get("timeunit")));
	}

	public void readFromTemplate(JSONObject message, String keyword) {
		String templateFileName = (String) message.get("template");
		Log.info("Sourcing message from Message Template file " + templateFileName);
		JSONObject template = refl.fetchTemplate(null, (String) message.get("directory"), templateFileName);
		refl.readAndFillData((JSONArray) template.get(keyword));
	}

	private void fillWhereAndWhen(JSONObject where_when) {
		click(CampaignsPageElements.SHOW_LIMIT.locate());
		clearFieldAndSendkeys(CampaignsPageElements.TYPE_NUMBER.locate(), (String) where_when.get("SHOW LIMIT"));
		saveAndContinueButton();
	}

	public void fillWebPushBlock(JSONObject data) {
		fillBasicInfo((JSONObject) data.get("Basic Info"));
		readFromTemplate((JSONObject) data.get("Message"), "Message");
		click(CampaignsPageElements.SKIP_TEST_BTN.locate());
	}

	private void fillinLineWhereBlock(String property) {
		openDropdown(InlineContentWebPerPageElements.PROP_DROPDOWN.locate());
		sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Search"), property);
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, property));
		saveAndContinueButton();
	}

	public void fillInlineWebBlock(JSONObject data) {
		click(dynamicXpathLocator(InlineContentWebPerPageElements.LAYOUT, data.get("layout").toString()));
		fillBasicInfo((JSONObject) data.get("Basic Info"));
		fillinLineWhereBlock(data.get("Select Property").toString());
		readFromTemplate((JSONObject) data.get("Message"), "Basic");
		saveAndContinueButton();
		saveAndCloseButton();
	}

	public void fillOnsiteBlock(JSONObject data) {
		switchToiFrame(OnsiteNotificationPageElements.ONSITE_JOURNEY_IFRAME.locate());
		click(dynamicXpathLocator(CommonPageElements.GENERIC_DIVTEXT, data.get("ONSITE_TEMPLATE").toString()));
		// readFromTemplate((JSONObject) data.get("Message"), "BasicDetails");
		// issue:WP-12671
		readFromTemplate((JSONObject) data.get("Message"), "JourneyDetails");
		readFromTemplate((JSONObject) data.get("Message"), "Targeting");
		switchToDefaultContent();
		saveAndCloseButton();
	}

	public void selectPlatformForAppSelection(String platform) {
		click(dynamicXpathLocator(CampaignsPageElements.CHOOSE_PLATFORM, platform));
		click(dynamicXpathLocator(CampaignsPageElements.PLATFORM_EDIT_BTN, platform));
		findElements(CommonPageElements.TABLE_CHECKBOX.locate()).forEach(el -> el.click());
	}

	public void selectPackageAndSave(LinkedList<String> packageNames) {
		packageNames.forEach(s -> {
			click(dynamicXpathLocator(CampaignsPageElements.SELECT_PACKAGE, s));
			CustomAssertions.assertTrue("Checkbox can't be checked->" + s,
					fetchAttributeValue(dynamicXpathLocator(CampaignsPageElements.SELECT_PACKAGE, s), "class")
							.contains("checked"));
		});
		saveButton();
	}

	public String fetchVariationID(String channel) {
		try {
			apiOperations.getMethod("fetch" + channel + "Campaign");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return apiUtility.fetchValue("response.data.experiment.variations[0].id");
	}

	public String fetchTestSegmentId(String segmentName) throws org.json.simple.parser.ParseException {
		String testSegId = null;
		try {
			apiOperations.getMethod("fetchTestSegment");
		} catch (IOException e) {
			e.printStackTrace();
		}
		for (Object obj : apiUtility.fetchObjectList("response.data")) {
			JSONObject jo = apiUtility.getJsonObject(new Gson().toJson(obj));
			if (jo.get("name").toString().equals(segmentName))
				testSegId = jo.get("id").toString();
		}
		return testSegId;
	}

	public void validateTestCampaignOutput(String variationID, String testSegId, String expectedStatus) {
		Log.info("Variation ID is " + variationID);
		Log.info("Test Segment ID is " + testSegId);
		APIUtility.getRuntimeValues().put("VariationId", variationID);
		runTimeUtility.setDynamicRequest("VariationId", "URL");
		APIUtility.getRuntimeValues().put("TestSegId", testSegId);
		runTimeUtility.setDynamicRequest("TestSegId", "URL");
		try {
			apiOperations.waitForAPIResponse("default", "testVariation", "response.data.contents[0].testStatus",
					expectedStatus);
		} catch (IOException e) {
			e.printStackTrace();
		}
		saveAndContinueButton();
	}

	public boolean selectTestSegment(String segmentName) {
		boolean isPresent = false;
		waitUntilAttributeContains(dynamicXpathLocator(CampaignsPageElements.SELECT_FROM_DD, "select a test segment"),
				"aria-disabled", "false");
		waitUntilAttributeDoesNotContain(
				dynamicXpathLocator(CampaignsPageElements.SELECT_FROM_DD, "select a test segment"), "class", "false");
		try {
			TimeUnit.SECONDS.sleep(5);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		click(dynamicXpathLocator(CampaignsPageElements.SELECT_FROM_DD, "select a test segment"));
		for (WebElement el : findElements(CampaignsPageElements.FETCH_DD_VALUES.locate())) {
			if (el.getText().equals(segmentName)) {
				el.click();
				isPresent = true;
				break;
			}
		}
		return isPresent;
	}

	public void initiateCampaignTest(boolean segSelected) {
		CustomAssertions.assertTrue("Desired Test Segment not selected", segSelected);
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "TEST CAMPAIGN"));
		verifyToastMessage(NotificationToastMessages.TEST_CAMPAIGN.message());
	}

	public void createOrUpdateTestSegment(String createOrEdit, Map<String, String> valueMap) {
		if (createOrEdit.equals("create")) {
			if (selectTestSegment("TEST-" + valueMap.get("Segment Name")))
				deleteTestSegment("TEST-" + valueMap.get("Segment Name"));
			click(CommonPageElements.ADD_BTN.locate());
			waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Create Test Segment"));
		} else if (createOrEdit.equals("edit")) {
			selectTestSegment("TEST-" + valueMap.get("Segment Name"));
			click(CommonPageElements.EDIT_BTN.locate());
			waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Edit Test Segment"));
		}
		clearFieldAndSendkeys(CampaignsPageElements.TEST_SEGMENT_NAME.locate(), valueMap.get("Segment Name"));
		click(CampaignsPageElements.TEST_FILTER_DD.locate());
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, valueMap.get("Filters")));
		clearFieldAndSendkeys(CampaignsPageElements.TEST_FILTER_VALUE.locate(), valueMap.get("values"));
		click(dynamicXpathLocator(CommonPageElements.BUTTON_CONTAINS, "test segment"));
	}

	public void openBreadCrumb(String breadCrumb) {
		waitUntilAttributeContains(dynamicXpathLocator(CampaignsPageElements.BREADCRUMB, breadCrumb), "class",
				"completed");
		click(dynamicXpathLocator(CampaignsPageElements.BREADCRUMB, breadCrumb));
	}

	public void deleteTestSegment(String testSegName) {
		CustomAssertions.assertEquals("Selected test segment doesn't match", testSegName,
				fetchText(dynamicXpathLocator(CampaignsPageElements.SELECT_FROM_DD, testSegName)));
		click(CommonPageElements.TRASH_BTN.locate());
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Delete Test Segment"));
		click(SegmentsPageElements.DELETE_BTN.locate());
		verifyToastMessage(NotificationToastMessages.DELETE_TEST_SEGMENT.message().replace("%s", testSegName));
	}

	public void verifyToggleButton(String button, String visiblilty) {
		boolean toggleButton = checkIfElementisLocated(
				dynamicXpathLocator(InAppCampaignPageElements.MESSAGE_BUTTON, button));
		CustomAssertions.assertEquals("Toggle button not verified", visiblilty.equalsIgnoreCase("visible"),
				toggleButton);
	}

	public void personaliseMessage(HashMap<String, String> map) {
		boolean boolBack = false;
		By backBtnEle = dynamicXpathLocator(CampaignsPageElements.PERSONALISATION_LABEL, map.get("Label"), "Back");
		click(dynamicXpathLocator(CampaignsPageElements.MESSAGEPERSONALISATION, map.get("Label")));
		boolBack = checkIfElementisLocated(backBtnEle);
		while (boolBack) {
			click(backBtnEle);
			boolBack = checkIfElementisLocated(backBtnEle);
		}
		String attributes[] = map.get("Attributes").split(">");
		for (String attrib : attributes) {
			click(dynamicXpathLocator(CampaignsPageElements.PERSONALISATION_LABEL, map.get("Label"), attrib));
		}
	}

	public void personaliseCatalogItem(HashMap<String, String> map) {
		personaliseMessage(map);
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, "Select..."));
		click(dynamicXpathLocator(InAppCampaignPageElements.RADIO_BUTTON, "Field", map.get("Field")));
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "Submit"));
	}

	public void verifyUserPreview(HashMap<String, String> map) throws InterruptedException, ClassNotFoundException {
		String userId = map.get("User");
		String channel = map.get("Type");
		JSONObject template = refl.fetchTemplate(null, "AccountData", "UsersData");
		boolean userPrev = checkIfElementisLocated(
				dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, "User Preview"));
		if (!userPrev)
			click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, "Raw Preview"));
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, "User Preview"));
		sendKeys(dynamicXpathLocator(CampaignsPageElements.INPUT_FIELD, "Search User"), userId);
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Search & Populate values"));
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "see preview"));
		waitForElementToBeInvisible(dynamicXpathLocator(CommonPageElements.BUTTON, "see preview"));
		String actualText = verifyPreviewAcrossChannel(channel);
		String expectedText = refl.updateStringWithJson((JSONObject) template.get(userId), map.get("PreviewMessage"));
		CustomAssertions.assertEquals("Preview Message not matched ", expectedText, actualText);

	}

	private String verifyPreviewAcrossChannel(String channel) {
		String actualText = null;
		switch (channel) {
		case "Email":
			switchToiFrame(EmailCampaignPageElements.PREVIEW_IFRAME.locate());
			actualText = fetchText(EmailCampaignPageElements.PREVIEW_BODY_IFRAME.locate());
			switchToDefaultContent();
			break;
		case "SMS":
		case "Whatsapp":
			actualText = fetchText(CampaignsPageElements.PREVIEW_TEXT.locate());
			break;
		case "Push":
		case "Webpush":
			waitForElementToBeVisible(PushCampaignPageElements.PREVIEW_TEXT.locate());
			actualText = fetchText(PushCampaignPageElements.PREVIEW_TEXT.locate());
			break;
		case "InApp":
			switchToiFrame(InAppCampaignPageElements.RAW_PREVIEW_IFRAME.locate());
			actualText = fetchText(InAppCampaignPageElements.HEADER_DESC_RAWPREVIEW.locate());
			switchToDefaultContent();
			break;
		}
		return actualText;
	}

	public void searchCampaignWithUnicode(String campaignName) {
		waitForElementToBeVisible(SMSCampaignPageElements.TABLE_HEADER.locate());
		if (checkIfElementisLocated(SMSCampaignPageElements.SEARCH_INPUT_BAR_VALUE.locate()))
			click(SMSCampaignPageElements.CLEAR_SEARCH.locate());
		sendKeysViaJavaScript(SMSCampaignPageElements.SEARCH_INPUT_BAR.locate(), campaignName);
		sendKeys(SMSCampaignPageElements.SEARCH_INPUT_BAR.locate(), Keys.RETURN);
		click(SMSCampaignPageElements.TABLE_HEADER.locate());
	}

	public void verifyCampaignWithUnicode(String campaignName) {
		String status = "";
		if (campaignName.contains("'"))
			status = fetchText(dynamicXpathLocator(CommonPageElements.STATUS_SINGLE_QUOTE, campaignName));
		else
			status = fetchText(dynamicXpathLocator(CommonPageElements.STATUS, campaignName));
		comPage.overviewPageLoad(status, campaignName);
		if (!(("UPCOMING").equalsIgnoreCase(status) || ("DRAFT").equalsIgnoreCase(status))) {
			CustomAssertions.assertEquals("Campaign name does not match", campaignName,
					fetchText(AdditionalPageElements.CAMPAIGN_NAME.locate()));
		}
	}

	public void verifyCampaignNameOnListPage(String campaignName) {
		CustomAssertions.assertEquals("CampaignName do not match:", campaignName,
				fetchAttributeValue(CommonPageElements.NAME_LINK_TEXT.locate(), "title"));
	}

	public void verifyReachableUser(String reachableUser) {
		CustomAssertions.assertEquals("Reachable user does not match", reachableUser,
				fetchText(CampaignsPageElements.REACHABLE_USER_TEXT.locate()));
	}

	public void fetchAndSetReachableUsers() {
		String actualReachableUser = fetchText(CampaignsPageElements.REACHABLE_USER_TEXT.locate());
		setAllReachableUserCount(actualReachableUser);
	}

	public void validateReachableUserCount(int count) {
		int channelReachableUsers = Integer.parseInt(getAllReachableUserCount());
		int actualReachableUsers = Integer.parseInt(fetchText(CampaignsPageElements.REACHABLE_USER_TEXT.locate()));
		CustomAssertions.assertEquals("Reachable user does not match", channelReachableUsers - count,
				actualReachableUsers);
	}

	public void fillAppInLineBlock(JSONObject message) {
		fillAppInLineBasicInfo((JSONObject) message.get("Basic Info"));
		readFromTemplate((JSONObject) message.get("Message"), "Message");
		saveAndCloseButton();

	}

	public void switchNamespace() {
		String url = getCurrentURL();
		if (getPrimaryNamespace() != null && getSecondaryNamespace() != null) {
			url = url.contains(getPrimaryNamespace()) ? url.replace(getPrimaryNamespace(), getSecondaryNamespace())
					: url.replace(getSecondaryNamespace(), getPrimaryNamespace());
		}
		Log.info("New Url->" + url);
		openURL(url);
	}

	private void fillAppInLineBasicInfo(JSONObject data) {
		sendKeys(dynamicXpathLocator(CampaignsPageElements.ENTER_TEXT_FOR, "CAMPAIGN NAME"),
				(String) data.get("CAMPAIGN NAME"));
		fillinLineWhereBlock(data.get("Select Property").toString());
		saveAndContinueButton();
		click(dynamicXpathLocator(CampaignsPageElements.SELECT_TEMPLATE, (String) data.get("layout")));
	}

	public void selectMultipleSegment(boolean includeCheck, List<String> choices) {
		String includeExcludeSection = includeCheck ? "Send To" : "EXCLUDE";
		click(dynamicXpathLocator(CampaignsPageElements.INCLUDE_EXCLUDE_SEGMENT, includeExcludeSection));
		for (String parameter : choices) {
			clearFieldAndSendkeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER_CONTAINS, "Search"),
					parameter);
			click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, parameter));
		}
		click(CommonPageElements.SAVE_BUTTON.locate());
		waitForElementToBeVisible(CampaignsPageElements.AUDIENCE_REACAHBLE_SECTION.locate());
	}

	public void selectSegment(String segmentName) {
		click(CampaignsPageElements.EXCLUDE_SEGMENTS_EMPTY_BOX.locate());
		sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER_CONTAINS, "Search"), segmentName);
		click(dynamicXpathLocator(CampaignsPageElements.SELECT_FROM_DD, segmentName));
		waitForElementToBeVisible(CampaignsPageElements.AUDIENCE_REACAHBLE_SECTION.locate());
	}

	public void isButtonDisabledOrEnabled(String button, String type) {
		String actualStatus = fetchAttributeValue(dynamicXpathLocator(CommonPageElements.BUTTON, button), "class")
				.contains("disabled") ? "disabled" : "enabled";
		CustomAssertions.assertEquals(button + " button is expected to be " + type, type, actualStatus);
	}

	public void removeInclusionExclusionSegment(String type) {
		String buttonText = type.toLowerCase().equals("exclusion") ? "EXCLUDE USERS FROM THESE SEGMENTS" : "Send To";
		click(dynamicXpathLocator(CampaignsPageElements.REMOVE_INCLUSION_EXCLUSION_SEGMENT, buttonText));
		waitForElementToBeVisible(CampaignsPageElements.AUDIENCE_REACAHBLE_SECTION.locate());
	}

	public void deleteCampaignsWithIdViaApi(String notificationID, String api) throws IOException {
		apiOperations.addURLParams(ConstantUtils.CAMPAIGNID, notificationID);
		apiOperations.deleteMethod(api);
	}

	public void deleteSurveyViaApi(String surveyId, String api) throws IOException {
		apiOperations.addURLParams(ConstantUtils.CAMPAIGNID, surveyId);
		apiOperations.dynamic_post_method(api, "WebPersonalization", "deleteSurveys");
		apiOperations.dynamic_post_method(api, "WebPersonalization", "deleteSurveys");
	}

	public String getTemplateIdFromTemplateName(String templateName) throws IOException {
		setupUtility.setApiURI("fetchCustomTemplates");
		apiUtility.getRequest(setupUtility.getApiURI());
		int totalTemplates = Integer.parseInt(apiUtility.fetchValue("response.data.totalCount"));
		for (int j = 0; j < totalTemplates; j++) {
			if (templateName.equals(apiUtility.fetchValue("response.data.contents[" + j + "].templateName"))) {
				return apiUtility.fetchValue("response.data.contents[" + j + "].templateId");
			}
		}
		return null;
	}
}