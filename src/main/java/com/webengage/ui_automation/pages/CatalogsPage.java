package com.webengage.ui_automation.pages;

import java.util.List;
import java.util.concurrent.TimeUnit;
import com.webengage.ui_automation.elements.CatalogsPageElements;
import com.webengage.ui_automation.elements.CommonPageElements;
import com.webengage.ui_automation.elements.IntegrationsPageElements;
import com.webengage.ui_automation.utils.NotificationToastMessages;
import com.webengage.ui_automation.utils.SeleniumExtendedUtility;

public class CatalogsPage extends SeleniumExtendedUtility {
	SMSCampaignsPage smsCamp=new SMSCampaignsPage();

	public void fillConfigurationDetails(List<List<String>> configData) {
		for (List<String> choice : configData) {
			String parameter = choice.get(0);
			String value = choice.get(1);
			switch (parameter) {
			case "CATALOG NAME":
				sendKeys(CatalogsPageElements.CATALOG_NAME.locate(), value);
				break;
			case "CATALOG TYPE":
				click(CommonPageElements.SELECT_CONTROL_DD.locate());
				click(dynamicXpathLocator(CommonPageElements.CATALOG_TYPE_VALUE, value));
				break;
			case "UPLOAD METHOD":
				click(dynamicXpathLocator(CatalogsPageElements.UPLOAD_TYPE, value));
				uploadFile("upload-catalogs.csv", IntegrationsPageElements.UPLOAD.locate());
				break;
			}
		}
		saveAndContinueButton();
	}

	public void fillMappingDetails(List<List<String>> mappingData) {
		for (List<String> choice : mappingData) {
			String parameter = choice.get(0);
			String value = choice.get(1);
			switch (parameter) {
			case "item id":
			case "title":
			case "link":
				click(dynamicXpathLocator(CatalogsPageElements.CATALOG_NAME_DD, parameter));
				sendKeys(dynamicXpathLocator(CatalogsPageElements.CATALOG_NAME_ENTRY, parameter), value);
				click(dynamicXpathLocator(CommonPageElements.CATALOG_TYPE_VALUE, value));
				break;
			}
		}
		saveAndContinueButton();
	}

	public void fillPreviewDetails(List<List<String>> previewData) {
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "Create Catalog"));
		verifyToastMessage(NotificationToastMessages.CATALOG_CREATION.message());
	}

	public void fillMapEventsToCatalogsDetails(List<List<String>> mapEventsToCatalogs) {
		mapCatalogsEvents(mapEventsToCatalogs);
		saveButton();
		verifyToastMessage(NotificationToastMessages.CATALOG_MAPPING.message());
	}

	private void mapCatalogsEvents(List<List<String>> mapEventsToCatalogs) {
		mapEventsToCatalogs.forEach(s -> {
			String eventName = s.get(0);
			String dataColumn = s.get(1);
			waitForElementToBeVisible(CatalogsPageElements.EVENT_MAP.locate());
			if (!checkIfElementisLocated(CatalogsPageElements.EVENT_MAP.locate())) {
				click(dynamicXpathLocator(CommonPageElements.BUTTON, "Add Event"));
			} 
			smsCamp.selectEvent(eventName);
			try {
				TimeUnit.SECONDS.sleep(2);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			click(CatalogsPageElements.COLUMN.locate());
			sendKeys(CommonPageElements.SEARCH.locate(), dataColumn);
			click(dynamicXpathLocator(CatalogsPageElements.COLUMN_EVENT, dataColumn));
		});
	}

	public void openMapEventsMenu(String name) {
		clickUsingJSExecutor(dynamicXpathLocator(CommonPageElements.OPEN_POP_OVER_MENU, name));
		click(CatalogsPageElements.MAP_EVENTS_BTN.locate());
	}
}
