package com.webengage.ui_automation.pages;

import java.io.FileOutputStream;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import com.webengage.ui_automation.driver.DriverFactory;
import com.webengage.ui_automation.elements.AdditionalPageElements;
import com.webengage.ui_automation.elements.AnalyticsPageElements;
import com.webengage.ui_automation.elements.CampaignsPageElements;
import com.webengage.ui_automation.elements.CommonPageElements;
import com.webengage.ui_automation.elements.CustomDashboardPageElements;
import com.webengage.ui_automation.elements.EmailCampaignPageElements;
import com.webengage.ui_automation.elements.InAppCampaignPageElements;
import com.webengage.ui_automation.elements.InlineContentAppPerPageElements;
import com.webengage.ui_automation.elements.InlineContentWebPerPageElements;
import com.webengage.ui_automation.elements.IntegrationsPageElements;
import com.webengage.ui_automation.elements.OnsiteNotificationPageElements;
import com.webengage.ui_automation.elements.PushCampaignPageElements;
import com.webengage.ui_automation.elements.SMSCampaignPageElements;
import com.webengage.ui_automation.elements.SegmentsPageElements;
import com.webengage.ui_automation.elements.WebPushCampaignsPageElements;
import com.webengage.ui_automation.elements.WhatsAppCampaignPageElements;
import com.webengage.ui_automation.utils.APIUtility;
import com.webengage.ui_automation.utils.ConstantUtils;
import com.webengage.ui_automation.utils.CustomAssertions;
import com.webengage.ui_automation.utils.Log;
import com.webengage.ui_automation.utils.ReflectionsUtility;
import com.webengage.ui_automation.utils.RuntimeUtility;
import com.webengage.ui_automation.utils.SeleniumExtendedUtility;
import com.webengage.ui_automation.utils.SetupUtility;

public class CommonPage extends SeleniumExtendedUtility {

	static JSONObject jsonObj;
	IntegrationsPage intPage = new IntegrationsPage();
	ReflectionsUtility refl = new ReflectionsUtility();
	RuntimeUtility runtimeUtility = new RuntimeUtility();
	SetupUtility setupUtility = new SetupUtility();
	APIUtility apiUtility = new APIUtility();
	public static String SDKDynamicWebpage = "SDKDynamicWebpageURL";
	public static String SDKUser;

	public void openDynamicWebPageAndLogin(boolean returnToDefaultWindow) throws InterruptedException {
		openURLinNewTab(CommonPage.SDKDynamicWebpage);
		Thread.sleep(5000);
		click(returnToDefaultWindow ? dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Home Page")
				: dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Login Page"));
		click(CommonPageElements.INPUT_SDK_USER.locate());
		sendKeys(CommonPageElements.INPUT_SDK_USER.locate(), CommonPage.SDKUser);
		click(CommonPageElements.CLICK_LOGIN_BTN_SDK.locate());
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, CommonPage.SDKUser));
		String screenshotName = "Screenshot_" + System.currentTimeMillis() + ".png";
		byte[] screenshotBytes = ((TakesScreenshot) DriverFactory.getInstance().getDriver())
				.getScreenshotAs(OutputType.BYTES);
		try (FileOutputStream fos = new FileOutputStream(screenshotName)) {
			fos.write(screenshotBytes);
		} catch (IOException e) {
			e.printStackTrace();
		}
		Log.debug("Screenshot: " + screenshotName);
		Log.debug("New tab title: " + driver.getTitle());
		Log.debug("New tab url: " + getCurrentURL());
		if (returnToDefaultWindow)
			switchToDefaultWindow();
	}

	public void createOrUpdateProvider(boolean type, String configName, String channel, String fileName) {
		if (type) {
			jsonObj = refl.fetchTemplate(null, "IntegrationPayloads/" + channel.replace("Invalid", ""), fileName);
			intPage.selectServiceProviderandSetName((String) jsonObj.get("Provider"), configName,
					channel.equalsIgnoreCase("WhatsApp"));
			refl.readAndFillData((JSONArray) jsonObj.get("Data"));
			switch (channel) {
			case "SMS":
				intPage.makeSSP();
				break;
			case "Email":
				intPage.makeESP();
				break;
			case "EmailInvalid":
				intPage.makeInvalidESP();
				return;
			case "WhatsApp":
				intPage.makeWSP();
				intPage.WSPConfirmation();
				break;
			default:
				break;
			}
		} else {
			jsonObj = refl.fetchTemplate(null, "IntegrationPayloads/" + channel, fileName);
			intPage.setProviderName((String) jsonObj.get("Provider"));
			intPage.setConfigName(configName);
			intPage.editConfig(configName, null, channel.charAt(0) + "SP", jsonObj);
		}
		intPage.checkList();
	}

	public void selectFromDropDown(String dropDownName, String valueToChoose) {
		click(dynamicXpathLocator(CommonPageElements.DD_NAME, dropDownName));
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, valueToChoose));
	}

	public void downloadListOfUsers() {
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "Download"));
		waitForElementToBeVisible(
				dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, "file is being prepared for download"));
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "Ok"));
	}

	public void clickTab(String tabName) {
		click(dynamicXpathLocator(CommonPageElements.ANCHOR_TAG_GENERIC_TEXT, tabName));
	}

	public void clickButton(String button) {
		click(dynamicXpathLocator(CommonPageElements.BUTTON, button));
	}

	public void downloadFile(List<List<String>> choices) throws IOException {
		for (List<String> choice : choices) {
			String tab = choice.get(0);
			String reportType = choice.get(1);
			if (tab != null
					&& checkIfElementisLocated(dynamicXpathLocator(CommonPageElements.ANCHOR_TAG_GENERIC_TEXT, tab))) {
				click(dynamicXpathLocator(CommonPageElements.ANCHOR_TAG_GENERIC_TEXT, tab));
			}
			switch (reportType) {
			case "Channel Overview":
			case "Campaign Analyze":
				waitForElementToBePresent(CommonPageElements.DOWNLOAD_CHANNEL_OVERVIEW_OR_ANALYZE.locate());
				CustomAssertions.assertTrue("File Download Unsuccessful - " + reportType, verifyDownloadedFile(
						CommonPageElements.DOWNLOAD_CHANNEL_OVERVIEW_OR_ANALYZE.locate(), reportType));
				break;
			case "Segment Size":
				waitForElementToBePresent(dynamicXpathLocator(CommonPageElements.GENERIC_DOWNLOAD_BUTTON, reportType));
				CustomAssertions.assertTrue("File Download Unsuccessful - " + reportType,
						verifyDownloadedFile(
								dynamicXpathLocator(CommonPageElements.GENERIC_DOWNLOAD_BUTTON, reportType),
								"Activity(MAU,WAU,DAU)"));
				break;
			case "Cohorts":
				waitForElementToBePresent(
						dynamicXpathLocator(CommonPageElements.GENERIC_DOWNLOAD_BUTTON, "FIRST EVENT"));
				CustomAssertions.assertTrue("File Download Unsuccessful - " + reportType, verifyDownloadedFile(
						dynamicXpathLocator(CommonPageElements.GENERIC_DOWNLOAD_BUTTON, "FIRST EVENT"), reportType));
				break;
			case "Activity (MAU, WAU, DAU)":
				waitForElementToBePresent(dynamicXpathLocator(CommonPageElements.GENERIC_DOWNLOAD_BUTTON, reportType));
				CustomAssertions.assertTrue("File Download Unsuccessful - " + reportType,
						verifyDownloadedFile(
								dynamicXpathLocator(CommonPageElements.GENERIC_DOWNLOAD_BUTTON, reportType),
								"Activity(MAU,WAU,DAU)"));
				break;
			case "Channel Reachability":
			case "Analyze":
				try {
					TimeUnit.SECONDS.sleep(3);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				waitForElementToBePresent(dynamicXpathLocator(CommonPageElements.GENERIC_DOWNLOAD_BUTTON, reportType));
				CustomAssertions.assertTrue("File Download Unsuccessful - " + reportType, verifyDownloadedFile(
						dynamicXpathLocator(CommonPageElements.GENERIC_DOWNLOAD_BUTTON, reportType), reportType));
				break;
			case "Show Event":
				waitForElementToBePresent(dynamicXpathLocator(CommonPageElements.GENERIC_DOWNLOAD_BUTTON, "SHOW"));
				CustomAssertions.assertTrue("File Download Unsuccessful - " + reportType, verifyDownloadedFile(
						dynamicXpathLocator(CommonPageElements.GENERIC_DOWNLOAD_BUTTON, "SHOW"), "Event"));
				break;
			case "Engagement - channels : Split By Channels":
				reportType = "Engagement - channels";
				waitForElementToBePresent(CommonPageElements.ENGAGEMENT_SPLITBY_CHANNEL_DOWNLOAD.locate());
				CustomAssertions.assertTrue("File Download Unsuccessful - " + reportType, verifyDownloadedFile(
						(CommonPageElements.ENGAGEMENT_SPLITBY_CHANNEL_DOWNLOAD.locate()), reportType));
				break;
			case "Engagement - channels : Unique Conversions":
				reportType = "Engagement - channels";
				waitForElementToBePresent(
						dynamicXpathLocator(CommonPageElements.GENERIC_DOWNLOAD_BUTTON, "Unique Conversions"));
				CustomAssertions.assertTrue("File Download Unsuccessful - " + reportType,
						verifyDownloadedFile(
								(dynamicXpathLocator(CommonPageElements.GENERIC_DOWNLOAD_BUTTON, "Unique Conversions")),
								reportType));
				break;
			default:
				CustomAssertions.assertFalse("File not found. Please check the file name.", true);
			}
		}

	}

	public void waitForSDKPageResponse(By by, long startTime, String apiName) {
		if ((System.currentTimeMillis() - startTime) < 500000) {
			if (!checkIfElementisLocated(by)) {
				refresh();
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					e.printStackTrace();

				}
				waitForSDKPageResponse(by, startTime, apiName);
			}

		} else {
			try {
				Log.error("Expected response was not obtained");
				CustomAssertions.assertTrue("Response timed out", false);
			} finally {
				Log.debug("Pausing/Deactivating timed out campaign....");
				pauseTimedOutCampaign(CampaignsPage.getCampaignType(), apiName);
			}
		}
	}

	private void pauseTimedOutCampaign(String campaignType, String apiName) {
		if (campaignType.contains("On-site Notifications")) {
			Log.debug("switching to parent window...");
			switchToDefaultWindow();
			Log.debug("Successfully switched to parent window");
			sendKeys(CommonPageElements.SEARCH.locate(), CampaignsPage.getCampaignName());
			click(OnsiteNotificationPageElements.MORE_TAB.locate());
			click(OnsiteNotificationPageElements.DEACTIVATE_NOTIFICATION.locate());
			((JavascriptExecutor) driver).executeScript("window.open('http://qa-automation.webengage.org');");
			System.out.println(getCurrentURL());
		} else {
			pauseCampaignViaAPI(apiName);

		}
		Log.debug("Campaign paused/deactivated successfully...");
	}

	public void pauseCampaignViaAPI(String apiName) {
		try {
			APIUtility.getRuntimeValues().put(ConstantUtils.CAMPAIGNID, CampaignsPage.getCampaignID());
			runtimeUtility.setDynamicRequest(ConstantUtils.CAMPAIGNID, "URL");
			setupUtility.setApiURI(apiName);
			apiUtility.putRequest(runtimeUtility.modifyAPIURL(setupUtility.getApiURI()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void verifyOverviewPageLoads(String campaignName) {
		String status = fetchText(dynamicXpathLocator(CommonPageElements.STATUS, campaignName));
		overviewPageLoad(status, campaignName);
	}

	public void overviewPageLoad(String status, String campaignName) {
		if (!(("UPCOMING").equalsIgnoreCase(status) || ("DRAFT").equalsIgnoreCase(status))) {
			openCampaign(campaignName);
			waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_LABEL, "UNIQUE CLICKS"));
			CustomAssertions.assertTrue("UNIQUE_CLICKS Label is not present",
					checkIfElementisLocated(dynamicXpathLocator(CommonPageElements.GENERIC_LABEL, "UNIQUE CLICKS")));
		}
	}

	public void switchTheViewAs(String viewType, String sectionName) {
		click(dynamicXpathLocator(CommonPageElements.MORE_OPTIONS, sectionName));
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, viewType));
	}

	public void selectCustomTimePeriod(String selectedDay) {
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Custom dates"));
		String startDate = selectedDay.split("-")[0].trim().replace("'", "");
		String endDate = selectedDay.split("-")[1].trim().replace("'", "");
		String currentMonth = fetchText(CommonPageElements.CUSTOM_DATE_MONTH.locate());
		while (!(currentMonth.contains(startDate.replaceAll("\\d", "").trim())
				&& currentMonth.contains(startDate.split(" ")[2]))) {
			System.out.println(startDate.replaceAll("\\d", ""));
			System.out.println(currentMonth.contains(startDate.split(" ")[2]));
			click(CommonPageElements.CUSTOM_DATE_PREVIOUS.locate());
			currentMonth = fetchText(CommonPageElements.CUSTOM_DATE_MONTH.locate());
		}
		click(dynamicXpathLocator(CommonPageElements.CUSTOM_DATE, startDate.split(" ")[0]));
		while (!(currentMonth.contains(endDate.replaceAll("\\d", "").trim())
				&& currentMonth.contains(endDate.split(" ")[2]))) {
			click(CommonPageElements.CUSTOM_DATE_NEXT.locate());
			currentMonth = fetchText(CommonPageElements.CUSTOM_DATE_MONTH.locate());
		}
		click(dynamicXpathLocator(CommonPageElements.CUSTOM_DATE, endDate.split(" ")[0]));
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "APPLY"));
		CustomAssertions.assertEquals("Chosen date and required date doesn't match", selectedDay,
				fetchText(AdditionalPageElements.DATE.locate()));
	}

	public void openModule(String moduleName) {
		click(dynamicXpathLocator(CommonPageElements.MODULE, moduleName));
		if (moduleName.equals("Dashboards")) {
			click(CustomDashboardPageElements.DASBOARDS_DROPDOWN.locate());
			click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Manage Dashboard"));
		}
	}

	public void openCampaign(String campaignName) {
		if (campaignName.contains("'")) {
			clickUsingJSExecutor(dynamicXpathLocator(CommonPageElements.MODULE_LIST_UTF, campaignName));
		} else {
			clickUsingJSExecutor(dynamicXpathLocator(CommonPageElements.MODULE_LIST, campaignName));
		}
	}

	public void openModule(String moduleName, String moduleType) {
		if (moduleType.equalsIgnoreCase("Funnels")) {
			By locator = moduleName.contains("'")
					? dynamicXpathLocator(AnalyticsPageElements.FUNNEL_LIST_UTF, moduleName)
					: dynamicXpathLocator(AnalyticsPageElements.FUNNEL_LIST, moduleName);
			clickUsingJSExecutor(locator);
		} else {
			By locator = moduleName.contains("'") ? dynamicXpathLocator(CommonPageElements.MODULE_LIST_UTF, moduleName)
					: dynamicXpathLocator(CommonPageElements.MODULE_LIST, moduleName);
			clickUsingJSExecutor(locator);
		}
	}

	public void selectInDropdown(By locator, String columnValue) {
		if (fetchAttributeValue(locator, "id").contains("downshift"))
			sendKeys(CommonPageElements.SEARCH.locate(), columnValue);
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, columnValue));
	}

	public void selectSpecificDDValue(String systemValue, String eventName) {
		click(CampaignsPageElements.EVENT_DROPDOWN.locate());
		sendKeys(CommonPageElements.SEARCH.locate(), eventName);
		String traverseDirection = systemValue.contains("System") ? "following" : "preceding";
		click(dynamicXpathLocator(CampaignsPageElements.EVENT_TYPE, traverseDirection, eventName));
	}

	public void validateEmojiViaTemplate(HashMap<String, String> map) throws InterruptedException {
		List<String> fieldList = new ArrayList<>();
		String fields = map.get("Label");
		if (fields.contains(">")) {
			String arr[] = fields.split(">");
			for (int i = 0; i < arr.length; i++) {
				fieldList.add(arr[i]);
			}
		} else {
			fieldList.add(fields);
		}
		enterDetailsAndValidateEmoji(fieldList);
	}

	public void enterDetailsAndValidateEmoji(List<String> choices) {
		String fieldTextValue = null;
		for (String field : choices) {
			switch (field) {
			case "Title":
				openEmojiPickerAndAttachEmoji(field);
				switchToiFrame(PushCampaignPageElements.TITLE_IFRAME.locate());
				fieldTextValue = fetchText((PushCampaignPageElements.TITLE_SECTION.locate()));
				switchToDefaultContent();
				break;
			case "Description":
				openEmojiPickerAndAttachEmoji(field);
				switchToiFrame(PushCampaignPageElements.DESCRIPTION_IFRAME.locate());
				fieldTextValue = fetchText((PushCampaignPageElements.DESCRIPTION_SECTION.locate()));
				switchToDefaultContent();
				break;
			case "Title_AppInLine":
				openEmojiPickerAndAttachEmoji(field.split("_")[0]);
				switchToiFrame(InlineContentAppPerPageElements.TITLE_IFRAME.locate());
				fieldTextValue = fetchText((InlineContentAppPerPageElements.TITLE_TEXT.locate()));
				switchToDefaultContent();
				break;
			case "Description_AppInLine":
				openEmojiPickerAndAttachEmoji(field.split("_")[0]);
				switchToiFrame(InlineContentAppPerPageElements.DESC_IFRAME.locate());
				fieldTextValue = fetchText((InlineContentAppPerPageElements.DESC_TEXT.locate()));
				switchToDefaultContent();
				break;
			case "Label_AppInLine":
				openEmojiPickerAndAttachEmoji(field.split("_")[0]);
				switchToiFrame(InlineContentAppPerPageElements.BANNER_IFRAME.locate());
				fieldTextValue = fetchText((InlineContentAppPerPageElements.BANNER_BTN_LABEL.locate()));
				switchToDefaultContent();
				break;
			case "Message Summary":
				click(dynamicXpathLocator(PushCampaignPageElements.TEXTAREA_EMOJI, field));
				click(PushCampaignPageElements.EMOJI.locate());
				switchToiFrame(PushCampaignPageElements.ANDROID_DESCRIPTION_IFRAME.locate());
				fieldTextValue = fetchText((PushCampaignPageElements.ANDROID_DESCRIPTION.locate()));
				switchToDefaultContent();
				break;
			case "Subtitle":
				click(dynamicXpathLocator(PushCampaignPageElements.TEXTAREA_EMOJI, field));
				click(PushCampaignPageElements.EMOJI.locate());
				switchToiFrame(PushCampaignPageElements.IOS_DESCRIPTION_IFRAME.locate());
				fieldTextValue = fetchText((PushCampaignPageElements.IOS_DESCRIPTION.locate()));
				switchToDefaultContent();
				break;
			case "Custom HTML & CSS":
				click(dynamicXpathLocator(InlineContentWebPerPageElements.EMOJI_LINK, field));
				click(PushCampaignPageElements.EMOJI.locate());
				fieldTextValue = fetchText((InlineContentWebPerPageElements.CUSTOMHTML.locate()));
				break;
			case "Description_InApp":
				openEmojiPickerAndAttachEmoji(field.split("_")[0]);
				fieldTextValue = fetchText((InAppCampaignPageElements.DESCRIPTION_SECTION.locate()));
				break;
			case "Label_PrimaryButton":
				openEmojiPickerAndAttachEmoji(field.split("_")[0]);
				fieldTextValue = fetchAttributeValue(
						dynamicXpathLocator(InAppCampaignPageElements.LABEL, "Primary Button", "Label"), "value");
				break;
			case "Label":
				click(dynamicXpathLocator(PushCampaignPageElements.CLOSE_EMOJI_LINK, field));
				click(PushCampaignPageElements.EMOJI.locate());
				fieldTextValue = fetchAttributeValue(
						dynamicXpathLocator(InAppCampaignPageElements.LABEL, "Close Button", "Label"), "value");
				break;
			case "Title_WebPush":
				openEmojiPickerAndAttachEmoji(field.split("_")[0]);
				fieldTextValue = fetchAttributeValue(WebPushCampaignsPageElements.TITLE.locate(), "value");
				break;
			case "Message_WebPush":
				field = (field.split("_")[0]);
				openEmojiPickerAndAttachEmoji(field);
				fieldTextValue = fetchText(dynamicXpathLocator(CampaignsPageElements.INPUT_FIELD, field));
				break;
			case "Label_WebPush":
				openEmojiPickerAndAttachEmoji(field.split("_")[0]);
				fieldTextValue = fetchAttributeValue(WebPushCampaignsPageElements.BUTTON_LABEL.locate(), "value");
				break;
			case "Body":
				openEmojiPickerAndAttachEmoji(field);
				switchToiFrame(EmailCampaignPageElements.IFRAME.locate());
				fieldTextValue = fetchText(EmailCampaignPageElements.BODY_TEXT.locate());
				break;
			case "Drag & Drop Editor":
				enterEmojiInDragAndDropEditor();
				fieldTextValue = fetchText(EmailCampaignPageElements.TEXT.locate());
				break;
			case "Title_WP":
				openEmojiPickerAndAttachEmoji(field.split("_")[0]);
				fieldTextValue = fetchAttributeValue(WebPushCampaignsPageElements.TITLE.locate(), "value");
				break;
			case "Message_WP":
				openEmojiPickerAndAttachEmoji(field.split("_")[0]);
				fieldTextValue = fetchAttributeValue(WebPushCampaignsPageElements.MESSAGE.locate(), "value");
				break;
			case "Label_WP":
				openEmojiPickerAndAttachEmoji(field.split("_")[0]);
				fieldTextValue = fetchAttributeValue(WebPushCampaignsPageElements.BUTTON_LABEL.locate(), "value");
				break;
			case "Template Name":
			case "Header":
			case "Button 1 Text":
			case "Footer":
				fieldTextValue = fetchAttributeValue(
						dynamicXpathLocator(IntegrationsPageElements.WHATSAPP_TEMPLATE_FIELDS, field), "value");
				break;
			case "Template Text":
				fieldTextValue = fetchAttributeValue(
						dynamicXpathLocator(IntegrationsPageElements.WHATSAPP_TEMPLATE_TEXTAREA, field), "value");
				break;
			default:
				openEmojiPickerAndAttachEmoji(field);
				fieldTextValue = fetchAttributeValue(dynamicXpathLocator(CampaignsPageElements.INPUT_FIELD, field),
						"value");
				break;
			}
			Assert.assertTrue("The field does not contain emoji",
					(fieldTextValue.contains("😀") || fieldTextValue.contains("\ud83d\ude03")));
			if (field.equalsIgnoreCase("Drag & Drop Editor")) {
				switchToDefaultContent();
				click(CommonPageElements.SAVE_AND_CLOSE_BUTTON.locate());
			} else if (field.equalsIgnoreCase("Body")) {
				switchToDefaultContent();
			}
		}
	}

	private void openEmojiPickerAndAttachEmoji(String field) {
		click(dynamicXpathLocator(PushCampaignPageElements.EMOJI_LINK, field));
		click(PushCampaignPageElements.EMOJI.locate());
	}

	private void enterEmojiInDragAndDropEditor() {
		scrollIntoView(EmailCampaignPageElements.EDITOR.locate());
		mouseOverElement(EmailCampaignPageElements.EDITOR.locate());
		click(dynamicXpathLocator(CommonPageElements.MODULE, "Edit in Drag & Drop Editor"));
		waitForElementToBeClickable(CommonPageElements.SAVE_AND_CLOSE_BUTTON.locate());
		switchToiFrame(EmailCampaignPageElements.TEMP_IFRAME.locate());
		click(EmailCampaignPageElements.TEXT.locate());
		click(dynamicXpathLocator(CommonPageElements.MODULE, "Personalization & Emoji"));
		switchToDefaultContent();
		click(EmailCampaignPageElements.DRAG_DROP_EMOJI_PICKER.locate());
		click((EmailCampaignPageElements.DRAG_DROP_EMOJI.locate()));
		switchToiFrame(EmailCampaignPageElements.TEMP_IFRAME.locate());
	}

	public void validateMultiLingualDataSet(HashMap<String, String> map) {
		String campaignType = map.get("CampaignType");
		switch (campaignType) {
		case "SMS":
			validateSMSMultilingualDataSet(map);
			break;
		case "Email":
			validateEmailMultilingualDataSet(map);
			break;
		case "WebPush":
			validateWebPushMultilingualDataSet(map);
			break;
		case "WhatsApp":
			validateWhatsAppMultilingualDataSet(map);
			break;
		case "Push":
			validatePushMultilingualDataSet(map);
			break;
		case "InApp":
			validateInAppMultilingualDataSet(map);
			break;
		case "App-InLine":
			validateAppInLineMultilingualDataSet(map);
			break;
		case "WebInline":
			validateWebInlineMultilingualDataSet(map);
			break;
		default:
			Log.error("Keyword is not present in the channel type");
		}
	}

	private void validateWebInlineMultilingualDataSet(HashMap<String, String> map) {
		CustomAssertions.assertEquals("WebInline CustomHTML text does not match", map.get("ExpectedTitle"),
				fetchText(InlineContentWebPerPageElements.PREVIEW_TEXT.locate()));

	}

	private void validateInAppMultilingualDataSet(HashMap<String, String> map) {
		switchToiFrame(InAppCampaignPageElements.RAW_PREVIEW_IFRAME.locate());
		CustomAssertions.assertEquals("InApp Header Description does not match", map.get("ExpectedDescription"),
				fetchText(InAppCampaignPageElements.HEADER_DESC_RAWPREVIEW.locate()));
		switchToDefaultContent();
	}

	private void validatePushMultilingualDataSet(HashMap<String, String> map) {
		CustomAssertions.assertEquals("Push Title does not match", map.get("ExpectedTitle"),
				fetchText(PushCampaignPageElements.PUSH_TITLE_RAWPREVIEW.locate()));
		CustomAssertions.assertEquals("Push description does not match", map.get("ExpectedDescription"),
				fetchText(PushCampaignPageElements.PUSH_DESC_RAWPREVIEW.locate()));
		CustomAssertions.assertEquals("Push Message summary does not match", map.get("ExpectedMesageSummary"),
				fetchText(PushCampaignPageElements.PUSH_MSG_SUMMARY_RAWPREVIEW.locate()));
		click(PushCampaignPageElements.OS_DROPDOWN_RAWPREVIEW.locate());
		click(PushCampaignPageElements.SELECT_OS_RAWPREVIEW.locate());
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		CustomAssertions.assertEquals("Push subtitle does not match", map.get("ExpectedSubtitle"),
				fetchText(PushCampaignPageElements.PUSH_SUBTITLE_RAWPREVIEW.locate()));
	}

	public void validateSMSMultilingualDataSet(Map<String, String> map) {
		CustomAssertions.assertEquals("Sender Text does not match", map.get("ExpectedSenderText"),
				fetchText(SMSCampaignPageElements.SENDER_PREVIEW.locate()));
		CustomAssertions.assertEquals("Message Text does not match", map.get("ExpectedMessageText"),
				fetchText(SMSCampaignPageElements.MESSAGE_PREVIEW.locate()));

	}

	public void validateEmailMultilingualDataSet(Map<String, String> map) {

		String layoutType = map.get("layoutType");
		switch (layoutType) {
		case "Rich Text":
			boolean bodyIframe = checkIfElementisLocated(EmailCampaignPageElements.PREVIEW_IFRAME.locate());
			if (bodyIframe) {
				switchToiFrame(EmailCampaignPageElements.PREVIEW_IFRAME.locate());
				CustomAssertions.assertEquals("Body Text does not match", map.get("ExpectedBodyText"),
						fetchText(EmailCampaignPageElements.PREVIEW_BODY_IFRAME.locate()));
				switchToDefaultContent();
				break;
			}
		case "Drag & Drop":
			boolean iframeDragDrop = checkIfElementisLocated(
					EmailCampaignPageElements.PREVIEW_IFRAME_DRAG_DROP.locate());
			if (iframeDragDrop) {
				switchToiFrame(EmailCampaignPageElements.PREVIEW_IFRAME_DRAG_DROP.locate());
				CustomAssertions.assertEquals("Body Text does not match", map.get("ExpectedBodyText"),
						fetchText(EmailCampaignPageElements.PREVIEW_DRAG_DROP_BODY.locate()));
				switchToDefaultContent();
				break;
			}
		default:
			Log.error("Email layout type is not present");
		}
	}

	public void validateWebPushMultilingualDataSet(Map<String, String> map) {
		CustomAssertions.assertEquals("Title Text does not match", map.get("ExpectedTitleText"),
				fetchText(WebPushCampaignsPageElements.PREVIEW_TITLE.locate()));
		CustomAssertions.assertEquals("Message Text does not match", map.get("ExpectedMessageText"),
				fetchText(WebPushCampaignsPageElements.PREVIEW_MESSAGE.locate()));
		CustomAssertions.assertEquals("Button 1 Text does not match", map.get("ExpectedButton1Text"),
				fetchText(WebPushCampaignsPageElements.PREVIEW_BUTTON_1.locate()));
	}

	public void validateAppInLineMultilingualDataSet(Map<String, String> map) {
		switchToiFrame(InlineContentAppPerPageElements.TITLE_IFRAME.locate());
		CustomAssertions.assertEquals("Title Text does not match", map.get("ExpectedTitleText"),
				fetchText(InlineContentAppPerPageElements.TITLE_TEXT.locate()));
		switchToDefaultContent();
		switchToiFrame(InlineContentAppPerPageElements.DESC_IFRAME.locate());
		CustomAssertions.assertEquals("Discription Text does not match", map.get("ExpectedDescriptionText"),
				fetchText(InlineContentAppPerPageElements.DESC_TEXT.locate()));
		switchToDefaultContent();
		switchToiFrame(InlineContentAppPerPageElements.BANNER_IFRAME.locate());
		CustomAssertions.assertEquals("Button Text does not match", map.get("ExpectedButtonText"),
				fetchText(InlineContentAppPerPageElements.BANNER_BTN_LABEL.locate()));
		switchToDefaultContent();
	}

	public void validateWhatsAppMultilingualDataSet(Map<String, String> map) {
		CustomAssertions.assertEquals("Message Text does not match", map.get("ExpectedMessageText"),
				fetchText(WhatsAppCampaignPageElements.PREVIEW_MESSAGE.locate()));
	}

	public void createIcon() {
		waitUntilAttributeDoesNotContain(CommonPageElements.CREATE_ICON.locate(), "class", "disabled");
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		click(CommonPageElements.CREATE_ICON.locate());
	}

	public void searchModuleNameHavingUnicode(String moduleName) {
		refresh();
		waitForElementToBeVisible(SegmentsPageElements.USER_ROWS.locate());
		clearTextField(CommonPageElements.SEARCHBOX.locate());
		sendKeysViaJavaScript(CommonPageElements.SEARCHBOX.locate(), moduleName);
		sendKeys(CommonPageElements.SEARCHBOX.locate(), Keys.ENTER);
	}

	public void deleteModule(String moduleName) {
		if (moduleName.contains("'"))
			clickUsingJSExecutor(dynamicXpathLocator(CommonPageElements.OPEN_POP_OVER_MENU_UTF, moduleName));
		else
			clickUsingJSExecutor(dynamicXpathLocator(CommonPageElements.OPEN_POP_OVER_MENU, moduleName));
		click(CommonPageElements.POP_OVER_DELETE.locate());
		deleteButton();
	}

	public void verifyModuleNameHavingUnicodeOnOverviewPage(String moduleName, String moduleType) {
		openModule(moduleName, moduleType);
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		CustomAssertions.assertEquals("Custom Dashboard name does not match", moduleName,
				fetchText(CommonPageElements.NAME_ON_OVERVIEWPAGE.locate()));
	}

	public void clickPopUpItem(String name, String action) {
		click(dynamicXpathLocator(SegmentsPageElements.OPEN_POP_OVER, name));
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, action));
	}

	public long convertDateToMilliseconds(String date) {
		Instant instant = Instant.parse(date);
		long milliseconds = instant.toEpochMilli();
		return milliseconds;
	}

	public void clickOnGenericLabel(String genericText) {
		click(dynamicXpathLocator(CommonPageElements.GENERIC_LABEL, genericText));
	}

}
