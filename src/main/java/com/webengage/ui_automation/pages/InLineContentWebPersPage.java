package com.webengage.ui_automation.pages;

import java.util.List;
import java.util.Map;
import org.json.simple.JSONObject;
import org.openqa.selenium.StaleElementReferenceException;

import com.webengage.ui_automation.elements.AnalyticsPageElements;
import com.webengage.ui_automation.elements.CampaignsPageElements;
import com.webengage.ui_automation.elements.CommonPageElements;
import com.webengage.ui_automation.elements.InlineContentWebPerPageElements;
import com.webengage.ui_automation.utils.CustomAssertions;
import com.webengage.ui_automation.utils.Log;
import com.webengage.ui_automation.utils.ReflectionsUtility;
import com.webengage.ui_automation.utils.SeleniumExtendedUtility;

public class InLineContentWebPersPage extends SeleniumExtendedUtility {
	SMSCampaignsPage smscamp = new SMSCampaignsPage();
	ReflectionsUtility rel = new ReflectionsUtility();
	OnsiteNotificationPage onspage = new OnsiteNotificationPage();
	CommonPage comPage = new CommonPage();

	String propPlacement = null;

	public String getPropPlacement() {
		return propPlacement;
	}

	public void setPropPlacement(String propPlacement) {
		this.propPlacement = propPlacement;
	}

	public void selectLayout(String layout) {
		click(InlineContentWebPerPageElements.CREATE_INLINE.locate());
		click(dynamicXpathLocator(InlineContentWebPerPageElements.LAYOUT, layout));
	}

	public void enterCampaignName(String name) {
		sendKeys(InlineContentWebPerPageElements.CAMPAIGN_NAME.locate(), name);
	}

	public void selectPropertyDetails(String propName, String propPlacement) {
		setPropPlacement(propPlacement);
		try {
			checkIfElementisLocated(InlineContentWebPerPageElements.PROP_DROPDOWN.locate());
			openDropdown(InlineContentWebPerPageElements.PROP_DROPDOWN.locate());
		} catch (StaleElementReferenceException e) {
			Log.error("Trying to locate the element again");
			openDropdown(InlineContentWebPerPageElements.PROP_DROPDOWN.locate());
			e.printStackTrace();
		}
		sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Search"), propName);
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, propName));
		click(CommonPageElements.SELECT_CONTROL_DD.locate());
		click(dynamicXpathLocator(CommonPageElements.GENERIC_DIVTEXT, propPlacement));
	}

	public void setScreenData(Map<String, String> map) {
		comPage.selectFromDropDown("Screen Data", map.get("DataType"));
		sendKeys(AnalyticsPageElements.INPUT_FUNNEL.locate(), map.get("AttributeName").toString());
		click(addIndexToLocator(dynamicXpathLocator(CommonPageElements.DD_NAME, "Screen Data"),"2"));
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, map.get("Operator")));
		sendKeys(addIndexToLocator(AnalyticsPageElements.INPUT_FUNNEL.locate(), "2"),
				map.get("AttributeValue").toString());
	}

	public void verifyPropertyPlacement(String apiName) {
		switch (getPropPlacement()) {
		case "Replace the property":
			comPage.waitForSDKPageResponse(InlineContentWebPerPageElements.WEBP_IMAGE.locate(),
					System.currentTimeMillis(), apiName);
			CustomAssertions.assertFalse("Replace-Property Placement exist",
					checkIfElementisLocated(InlineContentWebPerPageElements.REPLACE_PROP_PLACEMENT.locate()));
			break;
		case "Add before the property":
			comPage.waitForSDKPageResponse(
					dynamicXpathLocator(InlineContentWebPerPageElements.PROP_PLACEMENT_VALIDATE, "preceding"),
					System.currentTimeMillis(), apiName);
			CustomAssertions.assertTrue("Add before-property placement does not exist", checkIfElementisLocated(
					dynamicXpathLocator(InlineContentWebPerPageElements.PROP_PLACEMENT_VALIDATE, "preceding")));
			break;
		case "Add after the property":
			comPage.waitForSDKPageResponse(
					dynamicXpathLocator(InlineContentWebPerPageElements.PROP_PLACEMENT_VALIDATE, "following"),
					System.currentTimeMillis(), apiName);
			CustomAssertions.assertTrue("Add after-property placement does not exist", checkIfElementisLocated(
					dynamicXpathLocator(InlineContentWebPerPageElements.PROP_PLACEMENT_VALIDATE, "following")));
			break;
		default:
			Log.error("Property placement is not present");
		}
		onspage.switchToDefaultWindow();
	}

	private void verifyExpectedJSONData(Map<String, Object> map, String keyName, String apiName) {

		switch (keyName) {
		case "Image":
			comPage.waitForSDKPageResponse(InlineContentWebPerPageElements.WEBP_IMAGE.locate(),
					System.currentTimeMillis(), apiName);
			comPage.waitForSDKPageResponse(dynamicXpathLocator(InlineContentWebPerPageElements.CAMPAIGN_SRC, map.get(keyName).toString()),
					System.currentTimeMillis(), apiName);
			CustomAssertions.assertEquals("Image url does not match", map.get(keyName).toString(),
					fetchAttributeValue(InlineContentWebPerPageElements.WEBP_IMAGE.locate(), "src"));
			break;
		case "Width":
			comPage.waitForSDKPageResponse(InlineContentWebPerPageElements.WEBP_IMAGE.locate(),
					System.currentTimeMillis(), apiName);
			Map<String, Integer> dimensionMap = getDimensions(InlineContentWebPerPageElements.WEBP_IMAGE.locate());
			CustomAssertions.assertEquals("width do not match", map.get(keyName).toString(),
					dimensionMap.get("width").toString());
			break;
		case "BackgroundImage":
			comPage.waitForSDKPageResponse(InlineContentWebPerPageElements.BACKGRND_IMG.locate(),
					System.currentTimeMillis(), apiName);
			CustomAssertions.assertEquals("Image url does not match", map.get(keyName).toString(),
					fetchAttributeValue(InlineContentWebPerPageElements.BACKGRND_IMG.locate(), "src"));
			break;
		case "CustomImage":
			comPage.waitForSDKPageResponse(InlineContentWebPerPageElements.CUSTOM_IMG.locate(),
					System.currentTimeMillis(), apiName);
			CustomAssertions.assertEquals("Image url does not match", map.get(keyName).toString(),
					fetchAttributeValue(InlineContentWebPerPageElements.CUSTOM_IMG.locate(), "src"));
			break;
		case "CustomEmoji":
		    comPage.waitForSDKPageResponse(InlineContentWebPerPageElements.CUSTOM_EMOJI.locate(),
                    System.currentTimeMillis(), apiName);
		    CustomAssertions.assertEquals("Emoji does not match", map.get(keyName).toString(),
                    fetchText(InlineContentWebPerPageElements.CUSTOM_EMOJI.locate()));
			break;
		case "MultilingualDataSet":
			comPage.waitForSDKPageResponse(InlineContentWebPerPageElements.MULTILINGUAL.locate(),
					System.currentTimeMillis(), apiName);
			CustomAssertions.assertEquals("Multilingual dataset does not match", map.get(keyName).toString(),
					fetchText(InlineContentWebPerPageElements.MULTILINGUAL.locate()));
			break;
		default:
			Log.error("Expected keyword is not present");
			break;
		}

	}

	public void verifyInlineContentDetails(JSONObject jsonTemplate, String apiName) {
		Map<String, Object> map = rel.jsonObjectToMap((JSONObject) jsonTemplate.get("ExpectedData"));
		for (String keyName : map.keySet()) {
			verifyExpectedJSONData(map, keyName, apiName);
		}
		onspage.switchToDefaultWindow();
	}

	public void verifyCampaignCreationisDisabled() {
		CustomAssertions.assertTrue("Button is not disabled",
				fetchAttributeValue(CampaignsPageElements.CREATE_BTN.locate(), "class").contains("disable"));
		mouseOverElement(CampaignsPageElements.CREATE_BTN.locate());
		CustomAssertions.assertEquals("Tooltip text match failed",
				"You have disabled Web Personalization as a channel to engage users. To create new Web Personalization campaigns, please enable the channel again here",
				fetchText(CampaignsPageElements.TOOLTIP_WEBP.locate()));
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "here"));
	}

	public void fillAndVerifyPropertyDetails(List<List<String>> choices) {
		click(InlineContentWebPerPageElements.PROPERTY_ICON.locate());
		for (List<String> choice : choices) {
			String parameter = choice.get(0);
			String value = choice.get(1);
			sendKeys(dynamicXpathLocator(InlineContentWebPerPageElements.PROPERTY_VALUE, parameter), value);
		}
		waitUntilAttributeContains(CommonPageElements.VERIFY_PROPERTYMODAL.locate(), "class", "modal");
		click(InlineContentWebPerPageElements.CLOSE_PROPERTY.locate());
	}
	
	public void verifyPersonalizedMessage() throws InterruptedException {
		comPage.openDynamicWebPageAndLogin(false);
		comPage.waitForSDKPageResponse(
				dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, "WebSDKUserFirstName"),
				System.currentTimeMillis(), "inlineContent-webP-deactivate");
		onspage.switchToDefaultWindow();
	}
}