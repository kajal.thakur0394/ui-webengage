package com.webengage.ui_automation.pages;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.google.gson.Gson;
import com.webengage.ui_automation.elements.CommonPageElements;
import com.webengage.ui_automation.elements.SegmentsPageElements;
import com.webengage.ui_automation.elements.UserPageElement;
import com.webengage.ui_automation.utils.APIUtility;
import com.webengage.ui_automation.utils.CustomAssertions;
import com.webengage.ui_automation.utils.Log;
import com.webengage.ui_automation.utils.RuntimeUtility;
import com.webengage.ui_automation.utils.SeleniumExtendedUtility;
import com.webengage.ui_automation.utils.SetupUtility;

public class UserPage extends SeleniumExtendedUtility {
    RuntimeUtility runTimeUtility = new RuntimeUtility();
    APIOperationsPage apiOperations = new APIOperationsPage();
    SetupUtility setupUtility = new SetupUtility();
    APIUtility apiUtility = new APIUtility();
    CommonPage commonPage = new CommonPage();
	public void selectUser(String userId) {
		sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Search by email, user ID, phone"),
				userId + Keys.RETURN);
		click(dynamicXpathLocator(UserPageElement.CLICK_USER, userId));
		Log.info("Selected user id " + userId);
	}

	public void verifyDetails(List<List<String>> labels) {
		for (List<String> label : labels) {
			String attribute = label.get(0);
			String expectedValue = label.get(1);
			String actualValue = fetchText(dynamicXpathLocator(UserPageElement.LABEL_VALUE, attribute));
			CustomAssertions.assertEquals("Value for " + attribute + " does not match", expectedValue, actualValue);
		}
	}

	public void verifyRechability(List<List<String>> labels) {
		for (List<String> label : labels) {
			String expectedChannel = label.get(0).toLowerCase();
			String expectedChannelReachability = label.get(1);
			int i = 1;
			for (WebElement el : findElements(UserPageElement.REACHABILITY_HEADER.locate())) {
				if (el.getText().toLowerCase().equals(expectedChannel.toLowerCase())) {
					CustomAssertions.assertTrue("Reachability attribute doesn't match",
							fetchAttributeValue(
									dynamicXpathLocator(UserPageElement.REACHABILITY_VALUE, String.valueOf(i)), "class")
							.contains(expectedChannelReachability));
					break;
				}
				++i;
			}
		}
	}

	public void openSection(String section) {
		click(dynamicXpathLocator(CommonPageElements.ANCHOR_TAG_GENERIC_TEXT, section));
	}

	public void openDevice(String deviceNo) {
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, deviceNo));
	}

	public void checkEvents(String period) {
		waitForElementToBeVisible(UserPageElement.EVENT_PAGE.locate());
		click(CommonPageElements.TIME_PERIOD.locate());
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, period));
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.BUTTON, "Load More"));
		CustomAssertions.assertTrue("Event list is empty for defined period as " + period,
				findElements(UserPageElement.USER_EVENT_LIST.locate()).size() > 0);
	}

	public void verifyTotalUsers(List<List<String>> cellValues) {
		for (List<String> cellVal : cellValues) {
			int expectedValue = Integer.parseInt(cellVal.get(1).replaceAll("\\D+", ""));
			int actualValue = Integer.parseInt(
					fetchText(dynamicXpathLocator(SegmentsPageElements.USERS_SEGMENT_PAGE, cellVal.get(0))).replaceAll("\\D+", ""));
			if (cellVal.get(1).contains(">"))
				CustomAssertions.assertTrue("Expected value condition not staisfied", actualValue > expectedValue);
			else
				CustomAssertions.assertEquals("Values don't match", expectedValue, actualValue);
		}
	}

	public void downloadListOfUsers() {
		click(CommonPageElements.DOWNLOAD_BTN.locate());
		new CommonPage().downloadListOfUsers();
	}
	
    private String fetchEventId(String userName, String eventName) throws IOException, ParseException {
        String luid = "";
        String id = "";
        String category="";
        String name="";
        boolean checkResponse = false;
        APIUtility.getRuntimeValues().put("UserId", userName);
        runTimeUtility.setDynamicRequest("UserId", "URL");
        apiOperations.appendFromAndToDate(false);
        APIUtility.getRuntimeValues().put("cuid", userName);
        runTimeUtility.setDynamicRequest("from", "Query");
        runTimeUtility.setDynamicRequest("to", "Query");
        runTimeUtility.setDynamicRequest("cuid", "Query");
        long startTime = System.currentTimeMillis();
        int dur = 300000;
        int respDur = 1200000;
        outer: do {
            apiOperations.dynamic_get_method("fetchUserEventStream", "UserEvent", "fetchEvent");
            String jpath = "response.data.eventStream";
            List<Object> objectList = apiUtility.fetchObjectList(jpath);
            Log.info("List->" + objectList);
            if (objectList.isEmpty())
                continue;
            Collections.reverse(objectList);
            for (Object obj : objectList) {
                String jsonInString = new Gson().toJson(obj);
                JSONObject jo = apiUtility.getJsonObject(jsonInString);
                long responseTime= commonPage.convertDateToMilliseconds(jo.get("time").toString());
                if (jo.get("name").equals(eventName) && (startTime - responseTime)<respDur) {
                    luid = jo.get("luid").toString();
                    id = jo.get("id").toString();
                    category = jo.get("category").toString();
                    name = jo.get("name").toString();
                    Log.info("Luid->" + luid);
                    Log.info("Id->" + id);
                    checkResponse = true;
                    break outer;
                }
            }
        } while ((System.currentTimeMillis() - startTime) < dur);
        CustomAssertions.assertTrue("Event Stream response not found", checkResponse);
        APIUtility.getRuntimeValues().put("Category", category);
        APIUtility.getRuntimeValues().put("Name", name);
        return luid + " " + id;
    }

    public void verifyUserEventStreamData(String userName, String eventName, Map<String, String> eventMap)
            throws IOException, ParseException {
        String ids[] = fetchEventId(userName, eventName).split(" ");
        String luid = ids[0];
        String id = ids[1];
        String jpath = "";
        List<Object> objectList;
        boolean checkEvent = false;
        try {
            runTimeUtility.setDynamicRequest("Category", "URL");
            runTimeUtility.setDynamicRequest("Name", "URL");
            APIUtility.getRuntimeValues().put("EventId", id);
            runTimeUtility.setDynamicRequest("EventId", "URL");
            APIUtility.getRuntimeValues().put("Luid", luid);
            runTimeUtility.setDynamicRequest("Luid", "URL");
            setupUtility.setApiURI("fetchUserEventDetails");
            String apiURI = runTimeUtility.modifyAPIURL(setupUtility.getApiURI());
            apiURI = apiURI.replace("+", "%2B");
            apiURI = apiURI.replace("\\", "");
            apiURI = apiURI.replace(" ","+");
            apiUtility.getRequest(apiURI);
            if (eventMap.containsKey("systemEvent")) {
                jpath = "response.data.system_data";
                eventMap.remove("systemEvent");
            } else {
                jpath = "response.data.event_data";
                eventMap.remove("customEvent");
            }
            objectList = apiUtility.fetchObjectList(jpath);
            for (Object obj : objectList) {
                String jsonInString = new Gson().toJson(obj);
                JSONObject jo;
                try {
                    jo = apiUtility.getJsonObject(jsonInString);
                    if (eventMap.get("displayName").equals(jo.get("displayName"))) {
                        if (eventMap.get("displayName").equals("Page URL")) {
                            eventMap.put("value", CommonPage.SDKDynamicWebpage);
                        }
                        for (Map.Entry<String, String> entry : eventMap.entrySet()) {
                            String key = entry.getKey();
                            CustomAssertions.assertTrue("Response doesn't matched", jo.get(key).toString().contains(eventMap.get(key)));
                        }
                        checkEvent = true;
                        break;
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            CustomAssertions.assertTrue("Event Data reponse not found", checkEvent);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
