package com.webengage.ui_automation.pages;

import java.io.IOException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import com.google.gson.Gson;
import com.ibm.icu.math.BigDecimal;
import com.webengage.ui_automation.driver.DataFactory;
import com.webengage.ui_automation.pojopackages.user.Attributes;
import com.webengage.ui_automation.utils.APIUtility;
import com.webengage.ui_automation.utils.CustomAssertions;
import com.webengage.ui_automation.utils.ExcelReader;
import com.webengage.ui_automation.utils.Log;
import com.webengage.ui_automation.utils.RestAssuredBuilder;
import com.webengage.ui_automation.utils.RuntimeUtility;
import com.webengage.ui_automation.utils.SeleniumExtendedUtility;
import com.webengage.ui_automation.utils.SetupUtility;

public class APIOperationsPage {
	SetupUtility setupUtility = new SetupUtility();
	APIUtility apiUtility = new APIUtility();
	ExcelReader excelReader = new ExcelReader();
	RuntimeUtility runtimeUtility = new RuntimeUtility();
	SeleniumExtendedUtility extendUtility = new SeleniumExtendedUtility();
	long startTime;

	/**
	 * Method to handle dynamic post requests Runtime values will be replaced at
	 * URL, Body and query parmas level
	 * 
	 * @param apiName
	 * @param sheetName
	 * @param refId
	 * @return
	 * @throws IOException
	 */
	public APIUtility dynamic_post_method(String apiName, String sheetName, String refId) throws IOException {
		setupUtility.setApiURI(apiName);
		XSSFSheet sheetObj = setupUtility.readFromExcelSheet(sheetName);
		HashMap<String, String> apiParams = excelReader.processExcelSheetData(sheetObj, refId);
		APIUtility.setJsonBody(apiParams.get("Body"));
		String apiURI = setupUtility.getApiURI();
		apiURI = runtimeUtility.modifyAPIURL(apiURI);
		APIUtility.setJsonBody(runtimeUtility.modifyBody(APIUtility.getJsonBody()));
		String queryParamsString = apiParams.get("QueryParams");
		queryParamsString = runtimeUtility.modifyQueryParams(queryParamsString);
		List<List<String>> queryParams = apiUtility.generateQueryParams(queryParamsString);
		apiUtility.postRequest(APIUtility.getJsonBody(), queryParams, apiURI);
		return apiUtility;
	}

	public APIUtility generic_post_method(String apiName, String queryParams, String body) throws IOException {
		setupUtility.setApiURI(apiName);
		APIUtility.setJsonBody(body);
		String apiURI = setupUtility.getApiURI();
		apiURI = runtimeUtility.modifyAPIURL(apiURI);
		APIUtility.setJsonBody(runtimeUtility.modifyBody(APIUtility.getJsonBody()));
		String queryParamsString = runtimeUtility.modifyQueryParams(queryParams);
		List<List<String>> queryParameters = apiUtility.generateQueryParams(queryParamsString);
		apiUtility.postRequest(APIUtility.getJsonBody(), queryParameters, apiURI);
		return apiUtility;
	}

	public APIUtility generic_post_method(String apiName) throws IOException {
		setupUtility.setApiURI(apiName);
		String apiURI = setupUtility.getApiURI();
		apiURI = runtimeUtility.modifyAPIURL(apiURI);
		apiUtility.postRequest(apiURI);
		return apiUtility;
	}

	public APIUtility generic_post_method_form_data(String apiName, String queryParams,
			LinkedHashMap<String, String> formData) throws IOException {
		setupUtility.setApiURI(apiName);
		String apiURI = setupUtility.getApiURI();
		apiURI = runtimeUtility.modifyAPIURL(apiURI);
		APIUtility.setJsonBody(runtimeUtility.modifyBody(APIUtility.getJsonBody()));
		String queryParamsString = runtimeUtility.modifyQueryParams(queryParams);
		List<List<String>> queryParameters = apiUtility.generateQueryParams(queryParamsString);
		apiUtility.postRequestFormData(formData, queryParameters, apiURI);
		return apiUtility;
	}

	public APIUtility generic_put_method(String apiName) throws IOException {
		setupUtility.setApiURI(apiName);
		String apiURI = setupUtility.getApiURI();
		apiURI = runtimeUtility.modifyAPIURL(apiURI);
		apiUtility.putRequest(apiURI);
		return apiUtility;
	}

	public APIUtility generic_put_method(String apiName, String queryParams, String body) throws IOException {
		setupUtility.setApiURI(apiName);
		String apiURI = setupUtility.getApiURI();
		apiURI = runtimeUtility.modifyAPIURL(apiURI);
		APIUtility.setJsonBody(runtimeUtility.modifyBody(body));
		String queryParamsString = runtimeUtility.modifyQueryParams(queryParams);
		List<List<String>> queryParameters = apiUtility.generateQueryParams(queryParamsString);
		apiUtility.putRequest(APIUtility.getJsonBody(), queryParameters, apiURI);
		return apiUtility;
	}

	public APIUtility dynamic_get_method(String apiName, String sheetName, String refId) throws IOException {
		setupUtility.setApiURI(apiName);
		XSSFSheet sheetObj = setupUtility.readFromExcelSheet(sheetName);
		HashMap<String, String> apiParams = excelReader.processExcelSheetData(sheetObj, refId);
		APIUtility.setJsonBody(apiParams.get("Body"));
		String apiURI = setupUtility.getApiURI();
		apiURI = runtimeUtility.modifyAPIURL(apiURI);
		APIUtility.setJsonBody(runtimeUtility.modifyBody(APIUtility.getJsonBody()));
		String queryParamsString = apiParams.get("QueryParams");
		queryParamsString = runtimeUtility.modifyQueryParams(queryParamsString);
		List<List<String>> queryParams = apiUtility.generateQueryParams(queryParamsString);
		apiUtility.getRequest(APIUtility.getJsonBody(), queryParams, apiURI);
		return apiUtility;
	}

	public APIUtility getMethod(String apiName) throws IOException {
		setupUtility.setApiURI(apiName);
		String apiURI = setupUtility.getApiURI();
		apiURI = runtimeUtility.modifyAPIURL(apiURI);
		apiUtility.getRequest(apiURI);
		return apiUtility;
	}

	public APIUtility deleteMethod(String apiName) throws IOException {
		setupUtility.setApiURI(apiName);
		String apiURI = setupUtility.getApiURI();
		apiURI = runtimeUtility.modifyAPIURL(apiURI);
		apiUtility.deleteRequest(apiURI);
		return apiUtility;
	}

	public void addURLParams(String keyword, String value) {
		APIUtility.getRuntimeValues().put(keyword, value);
		runtimeUtility.setDynamicRequest(keyword, "URL");
	}

	public void waitForAPIResponse(String definedDuration, String api, String jsonPath, String value)
			throws IOException {
		setupUtility.setApiURI(api);
		String apiURI = runtimeUtility.modifyAPIURL(setupUtility.getApiURI());
		int timeoutDuration = definedDuration.equals("default") ? 300000 : Integer.parseInt(definedDuration) * 60000;
		startTime = System.currentTimeMillis();
		polling(timeoutDuration, apiURI, jsonPath, value);
		BigDecimal duration = new BigDecimal(String.valueOf(System.currentTimeMillis() - startTime));
		Log.info("Time elapsed for expected response - " + duration.divide(new BigDecimal("60000"), 3, 0) + " minutes");
	}

	public void waitForAPIResponse(String definedDuration, String apiName, String sheetName, String refId,
			String jsonPath, String value) throws IOException {
		dynamic_get_method(apiName, sheetName, refId);
		startTime = System.currentTimeMillis();
		int duration = definedDuration.equals("default") ? 300000 : Integer.parseInt(definedDuration) * 60000;
		polling(duration, apiName, sheetName, refId, jsonPath, value);
		BigDecimal elapsedDuration = new BigDecimal(String.valueOf(System.currentTimeMillis() - startTime));
		Log.info("Time elapsed for expected response - " + elapsedDuration.divide(new BigDecimal("60000"), 3, 0)
				+ " minutes");
	}

	private void polling(int duration, String apiURI, String jsonPath, String value) throws IOException {
		if ((System.currentTimeMillis() - startTime) < duration) {
			apiUtility.getRequest(apiURI);
			String responseValue = apiUtility.fetchValue(jsonPath);
			if (!value.equals(responseValue)) {
				polling(duration, apiURI, jsonPath, value);
			}
		} else {
			Log.info("Expected response was not obtained");
			CustomAssertions.assertTrue("Response timed out", false);
		}
	}

	private void polling(int duration, String apiName, String sheetName, String refId, String jsonPath, String value)
			throws IOException {
		if ((System.currentTimeMillis() - startTime) < duration) {
			dynamic_get_method(apiName, sheetName, refId);
			String responseValue = apiUtility.fetchValue(jsonPath);
			if (!value.equals(responseValue)) {
				polling(duration, apiName, sheetName, refId, jsonPath, value);
			}
		} else {
			Log.info("Expected response was not obtained");
			CustomAssertions.assertTrue("Response timed out", false);
		}
	}

	public void create_data_api_operation(String functionName) throws IOException {
		LinkedList<HashMap<String, String>> apiParams = fetchAPIData("TestDataCreation", functionName);
		for (HashMap<String, String> map : apiParams) {
			try {
				runtimeUtility.setDynamicRequest(map.get("RUNTIMEVALUE_API").split(":")[0],
						map.get("RUNTIMEVALUE_API").split(":")[1], "URL");
			} catch (ArrayIndexOutOfBoundsException e) {
			}
			try {
				runtimeUtility.setDynamicRequest(map.get("RUNTIMEVALUE_BODY").split(":")[0],
						map.get("RUNTIMEVALUE_BODY").split(":")[1], "Body");
			} catch (ArrayIndexOutOfBoundsException e) {
			}
			if (!map.get("HEADERS").equals("-")) {
				RestAssuredBuilder.getAdditionalHeaders().add(map.get("HEADERS"));
			}
			switch (map.get("METHOD")) {
			case "POST":
				generic_post_method(map.get("API"), map.get("QUERYPARAMS"), map.get("BODY"));
				break;
			case "PUT":
				generic_put_method(map.get("API"), map.get("QUERYPARAMS"), map.get("BODY"));
				break;
			case "POST-formdata":
				generic_post_method_form_data(map.get("API"), map.get("QUERYPARAMS"),
						generateFormDataBody(map.get("BODY")));
			default:
				break;
			}
			try {
				String[] runTimeJsonValues = map.get("SAVEVALUE").split("\\R");
				for (String path : runTimeJsonValues) {
					valueToSave(path.split(":")[0], path.split(":")[1]);
				}
			} catch (ArrayIndexOutOfBoundsException e) {
			}

		}
	}

	private LinkedHashMap<String, String> generateFormDataBody(String body) {
		LinkedHashMap<String, String> data = new LinkedHashMap<>();
		for (String pair : body.split("\\R")) {
			data.put(pair.split(":")[0], pair.replace(pair.split(":")[0], "").substring(1));
		}
		return data;
	}

	public void user_creation_api_operations(HashMap<String, HashMap<String, String>> userData) {
		String jsonBody;
		try {
			setupUtility.setApiURI("createUser");
			jsonBody = createUserJSON(userData);
			DataFactory.getInstance().setData("jsonBody", jsonBody);
			apiUtility.postRequest(APIUtility.getJsonBody(), setupUtility.getApiURI());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String createUserJSON(HashMap<String, HashMap<String, String>> userData) throws Exception {
		Iterator<String> itr = userData.keySet().iterator();

		String className = itr.next();
		Class<?> class_Name = Class.forName("com.webengage.ui_automation.pojopackages.user." + className);
		Object obj1 = class_Name.getDeclaredConstructor().newInstance();
		obj1 = setupUtility.methodInvocation(userData, className, obj1);

		className = itr.next();
		class_Name = Class.forName("com.webengage.ui_automation.pojopackages.user." + className);
		Object obj2 = class_Name.getDeclaredConstructor().newInstance();
		obj2 = setupUtility.methodInvocation(userData, className, obj2);

		Method m = obj1.getClass().getMethod("setAttributes", Attributes.class);
		m.invoke(obj1, obj2);
		return new Gson().toJson(obj1).replaceAll("_", " ");
	}

	private void valueToSave(String identifier, String jsonPath) {
		APIUtility.getRuntimeValues().put(identifier, apiUtility.fetchValue(jsonPath));
	}

	private LinkedList<HashMap<String, String>> fetchAPIData(String sheet, String function) throws IOException {
		XSSFSheet sheetObj = setupUtility.readFromExcelSheet(sheet);
		return excelReader.filterRowsOnFunctions(sheetObj, function);
	}

	public void create_data_campaigns_api_operation(String channel) throws IOException {
		LinkedList<String> campaignAPIs = new LinkedList<>();
		campaignAPIs.add("createAudience");
		campaignAPIs.add("schedule");
		campaignAPIs.add("createVariation");
		LinkedList<HashMap<String, String>> apiParams = fetchAPIData("CampaignTestDataCreation", channel);
		for (HashMap<String, String> map : apiParams) {
			for (String api : campaignAPIs) {
				try {
					String[] runTimeValues = map.get("RUNTIMEVALUE_API").split("\\R");
					for (String value : runTimeValues) {
						runtimeUtility.setDynamicRequest(value.split(":")[0], value.split(":")[1], "URL");
					}

				} catch (ArrayIndexOutOfBoundsException e) {
				}
				try {
					String[] runTimeValues = map.get("RUNTIMEVALUE_BODY").split("\\R");
					for (String value : runTimeValues) {
						runtimeUtility.setDynamicRequest(value.split(":")[0], value.split(":")[1], "Body");
					}
				} catch (ArrayIndexOutOfBoundsException e) {
				}
				if (api.equals("createVariation"))
					generic_put_method(api + channel, "-", map.get(api));
				else if (!map.get(api).equals("-"))
					generic_post_method(api + channel, "-", map.get(api));
				if (api.equals("createAudience")) {
					try {
						String[] runTimeJsonValues = map.get("SAVEVALUE").split("\\R");
						for (String path : runTimeJsonValues) {
							valueToSave(path.split(":")[0], path.split(":")[1]);
						}
					} catch (ArrayIndexOutOfBoundsException e) {
					}
				}
			}
			generic_put_method("activate" + channel);
		}
	}

    public void appendFromAndToDate(boolean date) {
        String fromDate = date ? "00:00:00.000%2B05:30" : "23:59:59.999%2B05:30";
        String toDate = date ? "23:59:59.999%2B05:30" : "00:00:00.000%2B05:30";
        APIUtility.getRuntimeValues().put("from", createDate().concat(fromDate));
        APIUtility.getRuntimeValues().put("to", createDate().concat(toDate));
    }

	private String createDate() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'");
		Date date = new Date();
		return formatter.format(date).toString();

	}

}
