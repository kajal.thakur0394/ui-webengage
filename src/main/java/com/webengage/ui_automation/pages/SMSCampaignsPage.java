package com.webengage.ui_automation.pages;

import com.webengage.ui_automation.elements.*;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import com.google.gson.Gson;
import com.webengage.ui_automation.utils.APIUtility;
import com.webengage.ui_automation.utils.CustomAssertions;
import com.webengage.ui_automation.utils.Log;
import com.webengage.ui_automation.utils.NotificationToastMessages;
import com.webengage.ui_automation.utils.SeleniumExtendedUtility;
import io.cucumber.datatable.DataTable;

public class SMSCampaignsPage extends SeleniumExtendedUtility {
	private String smsCampaignID;
	Map<String, String> ordinalMapDetails = new HashMap<String, String>();
	Map<String, String> mapDetails = new HashMap<String, String>();
	APIOperationsPage apiOperations = new APIOperationsPage();
	APIUtility apiUtility = new APIUtility();
	CommonPage comPage = new CommonPage();

	public String getSmsCampaignID() {
		return smsCampaignID;
	}

	public void setSmsCampaignID(String smsCampaignID) {
		this.smsCampaignID = smsCampaignID;
	}

	/**
	 * @param{campaignName} This method will locate the 'Search input' field and
	 *                      enters the campaign name inside the input field.
	 */
	public void searchBar(String campaignName) {
		waitForElementToBeVisible(SMSCampaignPageElements.TABLE_HEADER.locate());
		if (checkIfElementisLocated(SMSCampaignPageElements.SEARCH_INPUT_BAR_VALUE.locate())) {
			click(SMSCampaignPageElements.CLEAR_SEARCH.locate());
			waitForElementToBeVisible(SMSCampaignPageElements.TABLE_HEADER.locate());
		}
		sendKeys(SMSCampaignPageElements.SEARCH_INPUT_BAR.locate(), campaignName + Keys.RETURN);
		click(SMSCampaignPageElements.TABLE_HEADER.locate());
	}

	/**
	 * @param{campaignName},{campaignType} This method will verify that
	 *                                     'CampaignName' and 'CampaignType'
	 *                                     searched matches with the expected
	 *                                     'CampaignName' and 'CampaignType'.
	 */
	public void verifyCampaignNameandType(String campaignName, String campaignType) {
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.MODULE_LIST, campaignName));
		List<WebElement> webElements = findElements(CommonPageElements.TABLE_HEADER.locate());
		int index = 1;
		boolean headerfound = false;
		for (WebElement element : webElements) {
			if (element.getText().equalsIgnoreCase("type")) {
				headerfound = true;
				break;
			}
			index++;
		}
		if (!headerfound) {
			index=1;
			for (WebElement elemnt : webElements) {
				if (elemnt.getText().equalsIgnoreCase("layout")) {
					break;
				}
			}
			index++;
		}
		CustomAssertions.assertEquals("CampaignType does not match:", campaignType, fetchText(
				dynamicXpathLocator(CommonPageElements.LISTING_COLUMN_VALUE, campaignName, String.valueOf(index))));
	}

	/**
	 * @param{segmentName},{campaignName} This method will find the recently created
	 *                                    Segment and will verify via ToastMessage
	 *                                    of successful segment creation.
	 */
	public void verifySegmentCreated(String segmentName, String campaignName) {
		clickUsingJSExecutor(dynamicXpathLocator(SMSCampaignPageElements.SMS_CAMPAIGN_NAME, campaignName));
		waitForElementToBePresent(SMSCampaignPageElements.VERIFY_SEGMENT_NAME.locate());
		CustomAssertions.assertEquals("Segment name does not match",
				fetchAttributeValue(SMSCampaignPageElements.VERIFY_SEGMENT_NAME.locate(), "title"), segmentName);
	}

	/*
	 * This method will delete the recently created campaign and will verify via
	 * ToastMessage of successful campaign deletion.
	 */
	public void deleteCampaign(String name) {
		clickUsingJSExecutor(dynamicXpathLocator(SMSCampaignPageElements.POP_OVER_MENU2, name));
		click(CommonPageElements.POP_OVER_DELETE.locate());
		deleteButton();
		verifyToastMessage(NotificationToastMessages.DELETE_CAMPAIGN.message());
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/* @param{eventName} This method will locate the drop down element. */
	public void selectEvent(String eventName) {
		click(CampaignsPageElements.EVENT_DROPDOWN.locate());
		selectEventFromDropdown(eventName);
	}

	/**
	 * This method will select the event from the drop-down.
	 *
	 * @param eventName Name of the event that you wish to choose
	 */
	private void selectEventFromDropdown(String eventName) {
		sendKeys(CommonPageElements.SEARCH.locate(), eventName);
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, eventName));
	}

	/*
	 * @param{name} This method will verify that segment name in the list matches
	 * with the expected segment name.
	 */
	public void verifySegment(String name) {
		waitForElementToBeVisible(SMSCampaignPageElements.TABLE_HEADER.locate());
		sendKeys(CommonPageElements.SEARCHBOX.locate(), name + Keys.RETURN);
		CustomAssertions.assertEquals("Segment name do no match:", name,
				fetchAttributeValue(dynamicXpathLocator(CommonPageElements.MODULE_LIST, name), "title"));
	}

	/*
	 * This method will read the data from the data table and will perform the
	 * actions according to the list of data passed in the dataTable.
	 *
	 */
	public void fillWhenDetails(List<List<String>> choices) {
		CampaignsPage campPage = new CampaignsPage();
		String ordinalStartDateTime, ordinalEndDateTime = null, ordinalDeliveryTime, startDateTime, endDateTime,
				deliveryTime = null;
		for (List<String> choice : choices) {
			String parameter = choice.get(0);
			String value = choice.get(1);
			String monthDropdownOptions = choice.get(2);
			String daysOrDates = choice.get(3);
			try {
				if (("UPON OCCURRENCE OF").equalsIgnoreCase(parameter)) {
					selectEvent(value);
				} else if (("DELIVERY TIME").equalsIgnoreCase(parameter)) {
					click(dynamicXpathLocator(SMSCampaignPageElements.DATEYPE, value));
					click(dynamicXpathLocator(SMSCampaignPageElements.DATEPICKER, parameter));
					startLaterDate();
					ordinalDeliveryTime = campPage.getDateTime(parameter);
					Log.debug("ordinal delivery later==" + ordinalDeliveryTime);
					ordinalMapDetails.put("DELIVERY TIME:", ordinalDeliveryTime);
					deliveryTime = campPage.getNowDateTime(parameter);
					Log.debug("delivery later==" + deliveryTime);
				} else if (("START DATE").equalsIgnoreCase(parameter)) {
					scrollIntoView(dynamicXpathLocator(SMSCampaignPageElements.DATEYPE, value));
					click(dynamicXpathLocator(SMSCampaignPageElements.DATEYPE, value));
					if (("later").equalsIgnoreCase(value)) {
						click(dynamicXpathLocator(SMSCampaignPageElements.DATEPICKER, parameter));
						startLaterDate();
						ordinalStartDateTime = campPage.getDateTime(parameter);
						Log.debug("ordinal start later==" + ordinalStartDateTime);
						startDateTime = campPage.getNowDateTime(parameter);
						Log.debug("start later==" + startDateTime);
					} else {
						TimeUnit.SECONDS.sleep(2);
						click(SMSCampaignPageElements.LATER.locate());
						ordinalStartDateTime = campPage.getDateTime(parameter);
						Log.debug("ordinal start now==" + ordinalStartDateTime);
						startDateTime = campPage.getNowDateTime(parameter);
						Log.debug("start now==" + startDateTime);
						click(dynamicXpathLocator(SMSCampaignPageElements.DATEYPE, value));
					}
					ordinalMapDetails.put("START DATE:", ordinalStartDateTime);
					mapDetails.put("START DATE:", startDateTime);
				} else if (("END DATE").equalsIgnoreCase(parameter)) {
					if (("till").equalsIgnoreCase(value)) {
						scrollIntoView(dynamicXpathLocator(SMSCampaignPageElements.DATEYPE, value));
						click(dynamicXpathLocator(SMSCampaignPageElements.DATEYPE, value));
						click(dynamicXpathLocator(SMSCampaignPageElements.DATEPICKER, parameter));
						endTillDate();
						ordinalEndDateTime = campPage.getDateTime(parameter);
						Log.debug("ordinal end till==" + ordinalEndDateTime);
						endDateTime = campPage.getNowDateTime(parameter);
						Log.debug("end till==" + endDateTime);
						mapDetails.put("END DATE:", endDateTime);
					} else if (value.contains("after") && value.contains("hours")) {
						String[] addHour = value.split(" ");
						String addHours = addHour[1];
						scrollIntoView(dynamicXpathLocator(SMSCampaignPageElements.DATEYPE, "till"));
						click(dynamicXpathLocator(SMSCampaignPageElements.DATEYPE, "till"));
						LocalDateTime currentTime = LocalDateTime.now();
						LocalDateTime newTime = currentTime.plusHours(Integer.parseInt(addHours));
						if (newTime.getDayOfYear() != currentTime.getDayOfYear()) {
							click(dynamicXpathLocator(SMSCampaignPageElements.DATEPICKER, parameter));
							startLaterDate();
						}
						DateTimeFormatter formatter = DateTimeFormatter.ofPattern("h:mm a");
						String formattedNewTime = newTime.format(formatter);
						String[] endDate = formattedNewTime.split(":| ");
						String hour = endDate[0];
						String amPm = endDate[2];
						click(SMSCampaignPageElements.HOUR_SELECTION.locate());
						click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, hour));
						click(SMSCampaignPageElements.AMPM_SELECTION.locate());
						click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, amPm.toLowerCase()));
					} else {
						if (("Never").equalsIgnoreCase(value)) {
							scrollIntoView(dynamicXpathLocator(SMSCampaignPageElements.DATEYPE, value));
							click(dynamicXpathLocator(SMSCampaignPageElements.DATEYPE, value));
							String firstLetter = value.substring(0, 1);
							String remainingLetters = value.substring(1);
							value = firstLetter.toUpperCase().concat(remainingLetters);
						}
						ordinalEndDateTime = value;
						Log.debug("ordinal end never==" + ordinalEndDateTime);
						endDateTime = "-";
						Log.debug("end never==" + endDateTime);
						mapDetails.put("END DATE:", endDateTime);
					}
					ordinalMapDetails.put("END DATE:", ordinalEndDateTime);

				} else if (("DELIVERY SCHEDULE").equalsIgnoreCase(parameter)) {
					click(CommonPageElements.SELECT_CONTROL_DD.locate());
					scrollIntoView(dynamicXpathLocator(SMSCampaignPageElements.DELIVERY_DATE, value));
					click(dynamicXpathLocator(SMSCampaignPageElements.DELIVERY_DATE, value));
					try {
						String[] weekDaysOrDates = daysOrDates.split("\\s+");
						switch (value) {
						case "Week":
							selectWeekdays(weekDaysOrDates);
							Log.debug("Week days selected : " + daysOrDates);
							break;
						case "Month":
							selectMonthDetails(monthDropdownOptions, weekDaysOrDates);
							Log.debug("Dates or Days selected : " + daysOrDates);
							break;
						}
					} catch (NullPointerException e) {
						Log.debug("Selected default schedule provided by system i.e. rounded off to next 5 mins");
					}
					ordinalMapDetails.put("DELIVERY SCHEDULE:", value);
					mapDetails.put("DELIVERY SCHEDULE:", value);
				} else if (("SCHEDULE").equalsIgnoreCase(parameter)) {
					scrollIntoView(dynamicXpathLocator(SMSCampaignPageElements.DATEYPE, value));
					click(dynamicXpathLocator(SMSCampaignPageElements.DATEYPE, value));
					mapDetails.put("FROM DATE", getFromAndToDate(
							findElements(dynamicXpathLocator(InlineContentWebPerPageElements.DEL_SCHEDULE, "From"))));
					mapDetails.put("TO DATE", getFromAndToDate(
							findElements(dynamicXpathLocator(InlineContentWebPerPageElements.DEL_SCHEDULE, "To"))));
				} else if (("SHOW LIMIT").equalsIgnoreCase(parameter)) {
					scrollIntoView(CampaignsPageElements.SHOW_LIMIT.locate());
					click(CampaignsPageElements.SHOW_LIMIT.locate());
					waitUntilAttributeDoesNotContain(CampaignsPageElements.TYPE_NUMBER.locate(), "class", "disabled");
					clearFieldAndSendkeys(CampaignsPageElements.TYPE_NUMBER.locate(), value);
				} else if (("SET TIMEOUT").equalsIgnoreCase(parameter)) {
					clearFieldAndSendkeys(dynamicXpathLocator(CommonPageElements.GENERIC_LABEL_INPUT, "Set Timeout "),
							value);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	private String getFromAndToDate(List<WebElement> element) {
		String date = "";
		for (WebElement el : element) {
			date = date.concat(el.getText());
		}
		return date.substring(0, (date.length() - 4)).concat(":")
				.concat(date.substring((date.length() - 4), date.length()));
	}

	/*
	 * This method helps in selecting Start Date from the datePicker. START
	 * DATE=(Today's date+1)
	 */
	private void startLaterDate() {
		List<WebElement> weblist = findElements(SMSCampaignPageElements.TD_LIST.locate());
		if (weblist.size() == 0) {
			// Today's date=Last date of the month
			selectNextMonth(1);
			dateSelect(0);
		} else if (weblist.size() == 1) {
			// Today's date=Second Last date of the month
			dateSelect(0);
		} else if (weblist.size() > 1) {
			// Today's date=Middle date of the month
			dateSelect(0);
		}
	}

	/*
	 * This method will click on Right navigation arrow in order to select next
	 * month.
	 */
	private void selectNextMonth(int forwardMonthBy) {
		for (int i = 1; i <= forwardMonthBy; i++) {
			scrollIntoView(SMSCampaignPageElements.NEXT_MONTH.locate());
			click(SMSCampaignPageElements.NEXT_MONTH.locate());
		}
	}

	/* This method will select the date from the list of <td>. */
	private void dateSelect(int index) {
		List<WebElement> weblist = findElements(SMSCampaignPageElements.TD_LIST.locate());
		//scrollIntoView(SMSCampaignPageElements.TD_LIST.locate());
		wait.until(ExpectedConditions.visibilityOf(weblist.get(index)));
		weblist.get(index).click();
	}

	/*
	 * This method helps in selecting End Date from the datePicker. END DATE= 15th
	 * of (Current month+2)
	 */
	private void endTillDate() {
		selectNextMonth(2);
		dateSelect(14);
	}

	/*
	 * This method is part of Recurring Campaign type and is used to select days of
	 * the week for 1/Week frequency
	 */
	private void selectWeekdays(String[] weekDays) {
		for (String day : weekDays) {
			click(dynamicXpathLocator(SMSCampaignPageElements.SELECT_DATE_OR_DAY, day));
		}
	}

	/*
	 * This method is part of Recurring Campaign type and is used to select options
	 * like specific dates/days for 1/Month frequency
	 */
	private void selectMonthDetails(String monthDropdownOption, String[] specificDatesOrDays) {
		click(SMSCampaignPageElements.DELIVERY_SCHEDULE_ON.locate());
		waitForElementToBePresent(
				dynamicXpathLocator(SMSCampaignPageElements.MONTH_SCHEDULE_DROPDOWN, monthDropdownOption));
		click(dynamicXpathLocator(SMSCampaignPageElements.MONTH_SCHEDULE_DROPDOWN, monthDropdownOption));

		for (String datesOrDays : specificDatesOrDays) {
			click(dynamicXpathLocator(SMSCampaignPageElements.SELECT_DATE_OR_DAY, datesOrDays));
		}
	}

	/*
	 * This method read the parameters and its corresponding from dataTable and will
	 * fill the details in Audience Page.
	 */
	public void fillAudienceDetails(List<List<String>> choices) {
		for (List<String> choice : choices) {
			String parameter = choice.get(0);
			String value = choice.get(1);
			try {
				if (("Content Type").equalsIgnoreCase(parameter)) {
					click(dynamicXpathLocator(SMSCampaignPageElements.AUDIENCE_OR_CONTENT_TYPE, value));
				} else if (("Audience Type").equalsIgnoreCase(parameter)) {
					click(dynamicXpathLocator(SMSCampaignPageElements.AUDIENCE_OR_CONTENT_TYPE, value));
				} else {
					saveAndContinueButton();
				}
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
		}
	}

	/*
	 * This method will search for passed SegmentName in the search box and will
	 * verify whether the segment is present in the list or not.
	 */
	public void verifySegmentList(List<String> segmentList) {
		for (String segName : segmentList) {
			waitForElementToBeVisible(SMSCampaignPageElements.TABLE_HEADER.locate());
			sendKeys(CommonPageElements.SEARCHBOX.locate(), segName + Keys.RETURN);
			CustomAssertions.assertEquals("Segment name does not match", segName,
					fetchAttributeValue(CommonPageElements.NAME_LINK_TEXT.locate(), "title"));
			clearTextField(CommonPageElements.SEARCHBOX.locate());
		}
	}

	/*
	 * This method will fetch the values from 'Preview and Launch' Page of all the
	 * parameter passed in the data table and verify if the value present in the UI
	 * equals to the expected value that has been stored in a map while selecting
	 * date from date picker.
	 */
	public void previewCampaign(List<String> assertChoices) {
	    CampaignsPage campPage = new CampaignsPage();
	    click(SMSCampaignPageElements.PREVIEWPAGE.locate());
		for (String choices : assertChoices) {
			if (("Delivery Time:").equalsIgnoreCase(choices)) {
				String text = fetchText(SMSCampaignPageElements.ACTUAL_DELIVERY_TIME.locate())
						.replaceAll("[\\\t|\\\n|\\\r]", "");
				String[] arr = text.split(":");
				String actualTime = arr[1].concat(":").concat(arr[2]);
				CustomAssertions.assertEquals("Delivery Time do not match:", ordinalMapDetails.get("DELIVERY TIME:"),
						actualTime);
			} else if (("Start date:").equalsIgnoreCase(choices)) {
				String text = fetchText(dynamicXpathLocator(SMSCampaignPageElements.START_OR_END_DATES, choices))
						.replaceAll("[\\\t|\\\n|\\\r]", "");
				CustomAssertions.assertEquals("Start Date do not match:", campPage.newFormatDate(ordinalMapDetails.get("START DATE:")), campPage.newFormatDate(text));
			} else if (("End date:").equalsIgnoreCase(choices)) {
				String actualTime = fetchText(dynamicXpathLocator(SMSCampaignPageElements.START_OR_END_DATES, choices))
						.replaceAll("[\\\t|\\\n|\\\r]", "");
				CustomAssertions.assertEquals("End Date do not match:", campPage.newFormatDate(ordinalMapDetails.get("END DATE:")), campPage.newFormatDate(actualTime));
			} else if (("Delivery Schedule ").equalsIgnoreCase(choices)) {
				String text = fetchText(dynamicXpathLocator(SMSCampaignPageElements.START_OR_END_DATES, choices))
						.replaceAll("[\\\t|\\\n|\\\r]", "");
				if (choices.contains("Day"))
					CustomAssertions.assertTrue("Day do not match:", text.contains("Day"));
				else if (choices.contains("Week"))
					CustomAssertions.assertTrue("Week do not match:", text.contains("Week"));
				else if (choices.contains("Month"))
					CustomAssertions.assertTrue("Month do not match:", text.contains("Month"));
			} else if (("From date:").equalsIgnoreCase(choices)) {
				click(InlineContentWebPerPageElements.WHEN_TAB.locate());
				CustomAssertions.assertEquals("From Date do not match", mapDetails.get("FROM DATE"), getFromAndToDate(
						findElements(dynamicXpathLocator(InlineContentWebPerPageElements.DEL_SCHEDULE, "From"))));
			} else if (("To date:").equalsIgnoreCase(choices)) {
				CustomAssertions.assertEquals("From Date do not match", mapDetails.get("TO DATE"), getFromAndToDate(
						findElements(dynamicXpathLocator(InlineContentWebPerPageElements.DEL_SCHEDULE, "To"))));
			} else if ((choices).contains("QUEUING")) {
				CustomAssertions.assertEquals("Queuing value doesn't match", choices.split(":")[1].trim(),
						fetchText(dynamicXpathLocator(CampaignsPageElements.LABEL_VALUE, "Queuing:")));
			}
		}
	}

	/*
	 * This method will fetch the status of the campaign and calls the desired
	 * function to perform validations.
	 */
	public void verifyCampaignStatus(List<String> assertChoices, String campaignName) {
		String status = fetchText(SMSCampaignPageElements.CAMP_STATUS.locate());
		comPage.openCampaign(campaignName);
		if (("Running").equalsIgnoreCase(status))
			try {
				verifyDetails(assertChoices);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		else if (("Upcoming").equalsIgnoreCase(status))
			previewCampaign(assertChoices);
	}

	public String getCampaignStatus() {
		return fetchText(SMSCampaignPageElements.CAMP_STATUS.locate());
	}

	/*
	 * This method will fetch the values from 'Stats' page of all the parameter
	 * passed in the data table and verify if the value present in the UI equals to
	 * the expected value that has been stored in a map while selecting date from
	 * date picker.
	 */
	private void verifyDetails(List<String> assertChoices) throws InterruptedException {
		click(SMSCampaignPageElements.SHOW_DETAILS.locate());
		for (String choices : assertChoices) {
			if (("Start date:").equalsIgnoreCase(choices)) {
				String startDate = fetchText(
						dynamicXpathLocator(SMSCampaignPageElements.DETAILS_START_OR_END_DATES, "START DATE"));
				Log.debug("startDate-:" + startDate);
				CustomAssertions.assertEquals("Start Date do not match:", mapDetails.get("START DATE:"), startDate);
			} else if (("End date:").equalsIgnoreCase(choices)) {
				TimeUnit.MILLISECONDS.sleep(500);
				String endDate = fetchText(
						dynamicXpathLocator(SMSCampaignPageElements.DETAILS_START_OR_END_DATES, "END DATE"));
				Log.debug("endDate-:" + endDate);
				CustomAssertions.assertEquals("End Date do not match:", mapDetails.get("END DATE:"), endDate);
			} else if (("Delivery Schedule ").equalsIgnoreCase(choices)) {
				String deliverySchedule = fetchText(
						dynamicXpathLocator(SMSCampaignPageElements.DETAILS_START_OR_END_DATES, "DELIVERY SCHEDULE"));
				if (choices.contains("Day"))
					CustomAssertions.assertTrue("Day do not match:", deliverySchedule.contains("Day"));
				else if (choices.contains("Week")) {
					CustomAssertions.assertTrue("Week do not match:", deliverySchedule.contains("Week"));
				} else if (choices.contains("Month")) {
					CustomAssertions.assertTrue("Month do not match:", deliverySchedule.contains("Month"));
				}
			} else if (("From date:").equalsIgnoreCase(choices)) {
				click(InlineContentWebPerPageElements.WHEN_TAB.locate());
				CustomAssertions.assertEquals("From Date do not match", mapDetails.get("FROM DATE"), getFromAndToDate(
						findElements(dynamicXpathLocator(InlineContentWebPerPageElements.DEL_SCHEDULE, "From"))));
			} else if (("To date:").equalsIgnoreCase(choices)) {
				CustomAssertions.assertEquals("From Date do not match", mapDetails.get("TO DATE"), getFromAndToDate(
						findElements(dynamicXpathLocator(InlineContentWebPerPageElements.DEL_SCHEDULE, "To"))));
			} else if ((choices).contains("QUEUING")) {
				TimeUnit.SECONDS.sleep(1);
				CustomAssertions.assertEquals("Queuing value doesn't match", choices.split(":")[1].trim(),
						fetchText(dynamicXpathLocator(CampaignsPageElements.LABEL_VALUE, "QUEUING")));
			}
		}
	}

	/* This method will call the event api and verifies the expected status. */
	public void triggerEventViaAPI(String apiName, String sheetName, String refId) {

		try {
			TimeUnit.SECONDS.sleep(2);
			String status = apiOperations.dynamic_post_method(apiName, sheetName, refId).fetchValue("response.status");
			CustomAssertions.assertTrue("Status do not match:", status.equals("queued") || status.equals("success"));
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}

	}

	/*
	 * This method helps in fetching the jsonPath required for parameters.
	 */
	public void fetchStatsList(String duration, String apiName, String sheetName, String refId, String path,
			DataTable dataTable) {
		Map<String, String> paraMap = dataTable.asMap(String.class, String.class);
		List<Object> objectList = null;
		waitForJsonObject(paraMap, apiName, sheetName, refId, path, objectList);
		objectList = apiUtility.fetchObjectList(path);
		for (int i = 0; i < objectList.size(); i++) {
			String jsonInString = new Gson().toJson(objectList.get(i));
			JSONObject jo;
			try {
				jo = apiUtility.getJsonObject(jsonInString);
				if (jo.containsKey("name") && jo.containsKey("groupFunction")
						&& jo.get("groupFunction").toString().equalsIgnoreCase("COUNT")
						&& paraMap.keySet().contains(jo.get("name").toString())) {
					Log.debug("Validation for - " + jo.get("name").toString());
					String jsonPath = "response.data[0].dimensions[0].metrics" + "[" + i + "].value";
					apiOperations.waitForAPIResponse(duration, apiName, sheetName, refId, jsonPath,
							paraMap.get(jo.get("name").toString()));
				}
			} catch (ParseException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void waitForJsonObject(Map<String, String> paraMap, String apiName, String sheetName, String refId,
			String path, List<Object> objectList) {
		int keyCount = 0;
		long startTime = System.currentTimeMillis();
		int dur = 300000;
		List<Object> keyList = new ArrayList<>();
		outer: do {
			objectList = apiUtility.fetchObjectList(path);
			for (String key : paraMap.keySet()) {
				if (keyList.contains(key)) {
					continue;
				}
				for (Object i : objectList) {
					String jsonInString = new Gson().toJson(i);
					if (jsonInString.contains(key) && jsonInString.contains("COUNT") && !keyList.contains(key)) {
						keyList.add(key);
						keyCount++;
						break;
					}
				}
				if (keyCount == paraMap.size()) {
					break outer;
				} else {
					try {
						apiOperations.dynamic_get_method(apiName, sheetName, refId);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		} while ((System.currentTimeMillis() - startTime) < dur);
		if ((System.currentTimeMillis() - startTime) >= dur)
			CustomAssertions.assertFalse("Couldn't find the Name in default minutes", true);
	}

	/**
	 * This method will perform the events according to the parameters passed in
	 * feature file.
	 */
	public void fillConversionDetails(List<List<String>> choices) {
		for (List<String> choice : choices) {
			String parameter = choice.get(0);
			String value = choice.get(1);
			if (("Conversion Tracking").equalsIgnoreCase(parameter)) {
				if (!value.equalsIgnoreCase(fetchText(SMSCampaignPageElements.CONVERSION_ON.locate()))) {
					try {
						Thread.sleep(1000);
						click(SMSCampaignPageElements.PERSONALIZATION_TOGGLE.locate());
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			} else if (("Conversion Event").equalsIgnoreCase(parameter)) {
				selectEvent(value);
			} else if (("Conversion Deadline").equalsIgnoreCase(parameter)) {
				String[] arr = value.split("-");
				clearFieldAndSendkeys(SMSCampaignPageElements.CONVERSION_DEADLINE_FIELD.locate(), arr[0]);
				click(SMSCampaignPageElements.CONVERSION_DEADLINE_TIME.locate());
				click(dynamicXpathLocator(SMSCampaignPageElements.SELECT_CONVERSION_DEADLINE, arr[1]));

			}

		}
		saveAndContinueButton();

	}

	/*
	 * This method will read the data from the data table and will perform the
	 * actions according to the list of data passed in the dataTable.
	 *
	 */
	public void fillWhenChannelDetails(String campaignType, String channelType, List<List<String>> choices) {
		String ordinalDeliveryTime = null;
		switch (channelType) {
		case "Push":
			switch (campaignType) {
			case "One-time":
				enterPushDeliveryLaterDetails(ordinalDeliveryTime, choices);
				break;
			default:
			}
			break;
		case "SMS":
			switch (campaignType) {
			case "One-time":
				enterSMSOrEmailDeliveryLaterDetails(ordinalDeliveryTime, choices);
				break;
			default:
			}
			break;
		case "Email":
			switch (campaignType) {
			case "One-time":
				enterSMSOrEmailDeliveryLaterDetails(ordinalDeliveryTime, choices);
				break;
			default:
			}
			break;
		default:
		}
	}

	private void enterPushDeliveryLaterDetails(String ordinalDeliveryTime, List<List<String>> choices) {
		CampaignsPage campPage = new CampaignsPage();
		for (List<String> choice : choices) {
			String parameter = choice.get(0);
			String value = choice.get(1);
			try {
				if (("DELIVERY SCHEDULE").equalsIgnoreCase(parameter)) {
					click(dynamicXpathLocator(SMSCampaignPageElements.DATEYPE, value));
					click(dynamicXpathLocator(SMSCampaignPageElements.DATEPICKER, parameter));
					scrollIntoView(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Campaign Type & Schedule"));
					startLaterDate();
					ordinalDeliveryTime = campPage.getDate(parameter);
					Log.debug("ordinal delivery schedule==" + ordinalDeliveryTime);
				} else if (("DELIVERY TIME").equalsIgnoreCase(parameter)) {
					if (("sendIntelligently").equalsIgnoreCase(value)) {
						ordinalDeliveryTime = campPage.newFormatDate(ordinalDeliveryTime);
						click(dynamicXpathLocator(SMSCampaignPageElements.DATEYPE, value));
						ordinalDeliveryTime = ordinalDeliveryTime.concat(" (Send intelligently)");
						Log.debug("Expected ordinal delivery time intelligently==" + ordinalDeliveryTime);
					} else if (("sendAtSpecificTime").equalsIgnoreCase(value)) {
						ordinalDeliveryTime = campPage.newFormatDate(ordinalDeliveryTime);
						click(dynamicXpathLocator(SMSCampaignPageElements.DATEYPE, value));
						String ptz = fetchText(CampaignsPageElements.SPECIFIC_TIMEZONE.locate());
						ordinalDeliveryTime = ordinalDeliveryTime.concat(", ").concat(campPage.fetchSpecificTime())
								.concat(" ").concat(ptz.replace(ptz.substring(0, 1), "i"));
						Log.debug("Expected ordinal delivery specific time ==" + ordinalDeliveryTime);
					}
					ordinalMapDetails.put("DELIVERY TIME:", ordinalDeliveryTime);
				}

			} catch (NullPointerException e) {
			}
		}
	}

	private void enterSMSOrEmailDeliveryLaterDetails(String ordinalDeliveryTime, List<List<String>> choices) {
		CampaignsPage campPage = new CampaignsPage();
		for (List<String> choice : choices) {
			String parameter = choice.get(0);
			String value = choice.get(1);
			try {
				if (("DELIVERY SCHEDULE").equalsIgnoreCase(parameter)) {
					click(dynamicXpathLocator(SMSCampaignPageElements.DATEYPE, value));
					click(dynamicXpathLocator(SMSCampaignPageElements.DATEPICKER, parameter));
					startLaterDate();
					ordinalDeliveryTime = campPage.getDate(parameter);
					Log.debug("ordinal delivery schedule==" + ordinalDeliveryTime);
				} else if (("DELIVERY TIME").equalsIgnoreCase(parameter)) {
					if (("sendIntelligently").equalsIgnoreCase(value)) {
						ordinalDeliveryTime = campPage.newFormatDate(ordinalDeliveryTime);
						click(dynamicXpathLocator(SMSCampaignPageElements.DATEYPE, value));
						ordinalDeliveryTime = ordinalDeliveryTime.concat(" (Send intelligently)");
						Log.debug("Expected ordinal delivery time intelligently==" + ordinalDeliveryTime);
					} else if (("sendAtSpecificTime").equalsIgnoreCase(value)) {
						ordinalDeliveryTime = campPage.newFormatDate(ordinalDeliveryTime);
						click(dynamicXpathLocator(SMSCampaignPageElements.DATEYPE, value));
						String ptz = fetchText(CampaignsPageElements.SPECIFIC_TIMEZONE.locate());
						ordinalDeliveryTime = ordinalDeliveryTime.concat(", ").concat(campPage.fetchSpecificTime())
								.concat(" ").concat(ptz.replace(ptz.substring(0, 1), "i"));
						Log.debug("Expected ordinal delivery specific time ==" + ordinalDeliveryTime);
					}
					ordinalMapDetails.put("DELIVERY TIME:", ordinalDeliveryTime);
				}

			} catch (NullPointerException e) {
			}
		}
	}

	/*
	 * This method will fetch the status of the campaign and calls the desired
	 * function to perform validations.
	 */
	public void verifyCampaignStatus(String campaignType, String channelType, List<String> assertChoices,
			String campaignName) {
		String status = fetchText(SMSCampaignPageElements.CAMP_STATUS.locate());
		if (("Running").equalsIgnoreCase(status))
			verifyDetails(campaignType, channelType, assertChoices);
		else if (("Upcoming").equalsIgnoreCase(status))
			previewCampaign(campaignType, channelType, assertChoices, campaignName);

	}

	private void verifyDetails(String campaignType, String channelType, List<String> assertChoices) {
		// code will be added in later cases

	}

	/*
	 * This method will fetch the values from 'Preview and Launch' Page of all the
	 * parameter passed in the data table and verify if the value present in the UI
	 * equals to the expected value that has been stored in a map while selecting
	 * date from date picker.
	 */
	private void previewCampaign(String campaignType, String channelType, List<String> assertChoices,
			String campaignName) {
		comPage.openCampaign(campaignName);
		click(SMSCampaignPageElements.PREVIEWPAGE.locate());
		switch (channelType) {
		case "Push":
			switch (campaignType) {
			case "One-time":
				verifyExpectedPushDeliveryTime(assertChoices);
				break;
			default:
			}
			break;
		case "SMS":
			switch (campaignType) {
			case "One-time":
				verifyExpectedSMSOrEmailDeliveryTime(assertChoices);
			}
			break;
		case "Email":
			switch (campaignType) {
			case "One-time":
				verifyExpectedSMSOrEmailDeliveryTime(assertChoices);
				break;
			default:
			}
			break;
		default:
		}
	}

	private void verifyExpectedSMSOrEmailDeliveryTime(List<String> assertChoices) {
		for (String choices : assertChoices) {
			if (("Delivery Time:").equalsIgnoreCase(choices)) {
				String text = fetchText(SMSCampaignPageElements.ACTUAL_DELIVERY_TIME.locate())
						.replaceAll("[\\\t|\\\n|\\\r]", "");
				String[] arr = text.split(":");
				String actualTime = arr[1].concat(":").concat(arr[2]).concat(":").concat(arr[3]);
				Log.debug("Actual Delivery Time: " + actualTime);
				CustomAssertions.assertEquals("Delivery Time do not match:", ordinalMapDetails.get("DELIVERY TIME:"),
						actualTime);
			} else if (("Delivery Time: Send Intelligently").equalsIgnoreCase(choices)) {
				String text = fetchText(SMSCampaignPageElements.ACTUAL_DELIVERY_TIME.locate())
						.replaceAll("[\\\t|\\\n|\\\r]", "");
				String[] arr = text.split(":");
				String actualTime = arr[1];
				Log.debug("Actual Delivery Time: " + actualTime);
				CustomAssertions.assertEquals("Delivery Time do not match:", ordinalMapDetails.get("DELIVERY TIME:"),
						actualTime);
			}
		}
	}

	private void verifyExpectedPushDeliveryTime(List<String> assertChoices) {
		for (String choices : assertChoices) {
			if (("Delivery Time: Send Intelligently").equalsIgnoreCase(choices)) {
				String text = fetchText(SMSCampaignPageElements.ACTUAL_DELIVERY_TIME.locate())
						.replaceAll("[\\\t|\\\n|\\\r]", "");
				String[] arr = text.split(":");
				String actualTime = arr[1];
				Log.debug("Actual Delivery Time: " + actualTime);
				CustomAssertions.assertEquals("Delivery Time do not match:", ordinalMapDetails.get("DELIVERY TIME:"),
						actualTime);
			} else if (("Delivery Time: Send at specific time").equalsIgnoreCase(choices)) {
				String text = fetchText(SMSCampaignPageElements.ACTUAL_DELIVERY_TIME.locate())
						.replaceAll("[\\\t|\\\n|\\\r]", "");
				String[] arr = text.split(":");
				String actualTime = arr[1].concat(":").concat(arr[2]).concat(":" + arr[3]);
				Log.debug("Actual Delivery Time: " + actualTime);
				CustomAssertions.assertEquals("Delivery Time do not match:", ordinalMapDetails.get("DELIVERY TIME:"),
						actualTime);
			}
		}
	}

	public void checkUnicodeCharacPopUp() {
		if (checkIfElementisLocated(
				dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, "Unicode Characters")))
			click(EmailCampaignPageElements.PROCEED_BTN.locate());
	}

	public void fillFCQueueingDetails(List<List<String>> choices) {
		waitForElementToBeVisible(
				dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Frequency Capping, DND, Queuing & Throttling"));
		for (List<String> indivChoice : choices) {
			String type = indivChoice.get(0);
			if (indivChoice.get(1).equals("false")) {
				String radioButtonSelectionText = null;
				switch (type) {
				case "QUEUEING":
					radioButtonSelectionText = "not queue message";
					break;
				case "FC":
					if (checkIfElementisLocated(
							dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Set Frequency Capping Limits")))
						continue;
					else
						radioButtonSelectionText = "Ignore Frequency Capping settings";
					break;
				case "DND":
					radioButtonSelectionText = "Ignore DND settings";
					break;
				case "THROTTLING":
					radioButtonSelectionText = "Do not throttle campaigns";
					break;
				default:
					break;
				}
				click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, radioButtonSelectionText));
			} else {
				switch (type) {
				case "QUEUEING":
					scrollIntoView(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, "Queue message"));
					click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, "Queue message"));
					clearFieldAndSendkeys(dynamicXpathLocator(CampaignsPageElements.QUEUE_THROTTLE_INPUT, "QUEUEING"),
							indivChoice.get(2));
					click(CampaignsPageElements.QUEUING_PERIOD.locate());
					click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, indivChoice.get(3)));
					break;
				default:
					break;
				}
			}
		}
	}
}
