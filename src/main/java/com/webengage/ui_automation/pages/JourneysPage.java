package com.webengage.ui_automation.pages;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.webengage.ui_automation.driver.DataFactory;
import com.webengage.ui_automation.elements.CommonPageElements;
import com.webengage.ui_automation.elements.JourneysPageElements;
import com.webengage.ui_automation.elements.SegmentsPageElements;
import com.webengage.ui_automation.utils.APIUtility;
import com.webengage.ui_automation.utils.ConstantUtils;
import com.webengage.ui_automation.utils.Log;
import com.webengage.ui_automation.utils.NotificationToastMessages;
import com.webengage.ui_automation.utils.ReflectionsUtility;
import com.webengage.ui_automation.utils.SeleniumExtendedUtility;
import com.webengage.ui_automation.utils.SetupUtility;
import com.webengage.ui_automation.utils.CustomAssertions;

public class JourneysPage extends SeleniumExtendedUtility {
	APIOperationsPage apiOperations = new APIOperationsPage();
	final String directoryLoc = SetupUtility.directoryLoc + "JourneyTemplates/";
	CommonPage commPage = new CommonPage();
	CampaignsPage campPage = new CampaignsPage();
	String journeyID;
	String journeyCreationDate;
	String journeyName;
	String expectedStats;
	private JSONObject template;
	static HashMap<String, HashMap<String, String>> journeys = new HashMap<>();
	DateFormat df = new SimpleDateFormat("d MMM ''yy, hh:mma");

	public static void initializeVariables() {
		setJourneyIds(new ArrayList<String>());
	}

	@SuppressWarnings("unchecked")
	public static ArrayList<String> getJourneyIds() {
		return DataFactory.getInstance().getData(ConstantUtils.JOURNEY_IDS, ArrayList.class);
	}

	public static void setJourneyIds(ArrayList<String> ids) {
		DataFactory.getInstance().setData(ConstantUtils.JOURNEY_IDS, ids);
	}

	public static String getJourneyId() {
		return DataFactory.getInstance().getData(ConstantUtils.JOURNEY_ID, String.class);
	}

	public static void setJourneyId(String id) {
		DataFactory.getInstance().setData(ConstantUtils.JOURNEY_ID, id);
	}

	public static String getJourneyExpectedPayload() {
		return DataFactory.getInstance().getData(ConstantUtils.JOURNEY_PAYLOAD, String.class).toString();
	}

	public static void setJourneyExpectedPayload(String JourneyPayload) {
		DataFactory.getInstance().setData(ConstantUtils.JOURNEY_PAYLOAD, JourneyPayload);
	}

	public String getJourneyName() {
		return journeyName;
	}

	public void setJourneyName(String journeyName) {
		this.journeyName = journeyName;
	}

	public String getJourneyID() {
		return journeyID;
	}

	public void setJourneyID() {
		this.journeyID = getCurrentURL().split("/")[getCurrentURL().split("/").length - 1];
		JourneysPage.setJourneyId(this.journeyID);
		Log.info("The journey id is [" + journeyID + "] for Journey - " + getJourneyName());
	}

	public String getJourneyCreationDate() {
		return journeyCreationDate.replace("PM", "pm").replace("AM", "am");
	}

	public void setJourneyCreationDate(Date date) {
		this.journeyCreationDate = df.format(date);
	}

	public void setjourneyTemplate(JSONObject template) {
		this.template = template;
	}

	public JSONObject getjourneyTemplate() {
		return template;
	}

	Actions a = new Actions(driver);
	JavascriptExecutor js = (JavascriptExecutor) driver;

	/**
	 * Open Journeys Page, Lands on Journey Listing page
	 * 
	 * @since V1.0.0
	 * @version 1.0.0
	 */

	/**
	 * Rounds off time to next nearest 5 mins.
	 * 
	 * @param validation Boolean When true is used for validation of dates.
	 * @return Calendar with rounded off time
	 * @since V1.0.0
	 * @version 1.0.0
	 */
	public Calendar getRoundedOffTime(boolean validation) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		int currentMinute = calendar.get(Calendar.MINUTE);
		int minutesToAdd;
		if (validation)
			minutesToAdd = ((currentMinute % 10) % 5 == 0) ? 0 : 5 - ((currentMinute % 10) % 5);
		else
			minutesToAdd = ((currentMinute % 10) % 5 == 0) ? 5 : 5 - ((currentMinute % 10) % 5);
		calendar.set(Calendar.MINUTE, currentMinute + minutesToAdd);
		return calendar;
	}

	/**
	 * Click on create button and wait for Journey canvas page to appear. Save the
	 * journey creation date as system time
	 * 
	 * @since V1.0.0
	 * @version 1.0.0
	 * @throws InterruptedException
	 * 
	 */
	public void createJourney() throws InterruptedException {
		click(JourneysPageElements.CREATE_BTN.locate());
		setJourneyCreationDate(new Date());
		waitForElementToBeVisible(JourneysPageElements.JOURNEY_SIDE_PANEL.locate());
	}

	public JSONObject buildJourney(String scenarioName, String fileName) {
		JSONObject template = new ReflectionsUtility().fetchTemplate(scenarioName, "JourneyTemplates", fileName);
		buildBlocks(template);
		return template;
	}

	/**
	 * Drag and drop blocks mentioned in template from left pane onto the canvas
	 * 
	 * @param template JSONObject having details of which block needs to be used
	 * @since V1.0.0
	 * @version 1.0.0
	 */
	private void buildBlocks(JSONObject template) {
		LinkedList<String> allTheBlocks = new LinkedList<>();
		JSONObject blocks = (JSONObject) template.get("blocks");
		Iterator<?> i = blocks.keySet().iterator();
		while (i.hasNext()) {
			String temp = (String) i.next();
			JSONArray jsonArr = (JSONArray) blocks.get(temp);
			Iterator<?> itr = jsonArr.iterator();
			while (itr.hasNext()) {
				allTheBlocks.add(temp + "." + itr.next());
			}
		}
		try {
			dragAndDropBlocks(template, allTheBlocks);
		} catch (TimeoutException | IndexOutOfBoundsException e) {
			driver.navigate().refresh();
			Log.warn("Journey Page failure due to Platform config. Initialized page reload");
			try {
				dragAndDropBlocks(template, allTheBlocks);
			} catch (StaleElementReferenceException e2) {
				driver.navigate().refresh();
				dragAndDropBlocks(template, allTheBlocks);
				Log.warn("Journey Page failure due to Platform config. Initialized page reload. StaleElementException");
			}
		}
	}

	/**
	 * Drag and drop blocks onto the canvas and save the id of the blocks.
	 * 
	 * @param template
	 * @param allTheBlocks
	 * @since V1.0.0
	 * @version 1.0.0
	 */
	private void dragAndDropBlocks(JSONObject template, LinkedList<String> allTheBlocks) {
		waitForElementToBeVisible(JourneysPageElements.SEARCH_CONTROLS.locate());
		WebElement to = findElement(JourneysPageElements.DROP_TO.locate());
		WebElement from = null;
		for (String s : allTheBlocks) {
			if (!s.equalsIgnoreCase("trigger.BusinessEvent")) {
				from = findElement(selectElement(s));
				a.moveToElement(from);
				try {
					a.dragAndDrop(from, to).build().perform();
				} catch (StaleElementReferenceException e) {
					Log.warn("Drag and drop action failed. Reloading page and retrying...");
					driver.navigate().refresh();
					a.dragAndDrop(from, to).build().perform();
				}
			}
			saveIDofBlock(template, s);
		}
	}

	/**
	 * Save value of the attribute "id" corresponding to each block in JSONObject of
	 * template
	 * 
	 * @param template JSONObject of template being used
	 * @param s        type of block
	 * @since V1.0.0
	 * @version 1.0.0
	 */
	@SuppressWarnings("unchecked")
	private void saveIDofBlock(JSONObject template, String type) {
		JSONObject typeObj = (JSONObject) template.get(type.split("\\.")[1]);
		type = type.replaceAll("\\d", "");
		List<WebElement> getBlocks = findElements(dynamicXpathLocator(JourneysPageElements.GET_BLOCK_ID, type));
		String blockId = getBlocks.get(getBlocks.size() - 1).getAttribute("id");
		typeObj.put("id", blockId);
	}

	/**
	 * Scroll to and select the block that needs to be pulled onto the canvas
	 * 
	 * @param s name of the block and category from which it belongs to
	 * @return By locator of the type
	 * @since V1.0.0
	 * @version 1.6.4
	 */
	private By selectElement(String type) {
		type = type.replaceAll("\\d", "");
		By locator = dynamicXpathLocator(JourneysPageElements.BLOCK_TYPE, type);
		WebElement element = findElement(locator);
		js.executeScript("arguments[0].scrollIntoView();", element);
		return locator;
	}

	/**
	 * Connect paths by the keyword, source and destination that has been mentioned
	 * in the template. Blocks are located by the temp id that have been updated in
	 * template.
	 * 
	 * @param template Journey template
	 * @throws InterruptedException
	 * @since V1.0.0
	 * @version 1.0.0
	 */
	public boolean connectPaths(JSONObject template, Boolean skipSplit) throws InterruptedException {
		boolean splitBlock = makeConnections(template, skipSplit, (JSONArray) template.get("paths"));
		findElement(JourneysPageElements.ALIGN_VERTICALLY.locate()).click();
		findElement(JourneysPageElements.BTN_YES.locate()).click();
		transform(template, false);
		return splitBlock;
	}

	@SuppressWarnings("rawtypes")
	private boolean makeConnections(JSONObject template, Boolean skipSplit, JSONArray jsonArray) {
		Boolean splitBlock = false;
		Iterator i = jsonArray.iterator();
		while (i.hasNext()) {
			JSONObject current = (JSONObject) i.next();
			String connectFrom = (String) current.get("connectFrom");
			if (skipSplit) {
				if (connectFrom.toLowerCase().contains("split")) {
					splitBlock = true;
					continue;
				}
			} else {
				if (!connectFrom.toLowerCase().contains("split")) {
					continue;
				}
			}
			String hover = connectFrom.split("\\.")[1];
			connectFrom = fetchIDOfDesired(template, connectFrom.split("\\.")[0]);
			String connectTo = fetchIDOfDesired(template, (String) current.get("connectTo"));
			WebElement from = findElement(dynamicXpathLocator(JourneysPageElements.LOCATE_BLOCK, connectFrom));
			WebElement to = findElement(dynamicXpathLocator(JourneysPageElements.LOCATE_BLOCK, connectTo));
			a.moveToElement(from).build().perform();
			String value = (String) current.get("connectFrom");
			if (value.contains(".out")) {
				from = findElement(dynamicXpathLocator(JourneysPageElements.HOVER_OVER_CONTAINS, connectFrom, hover));
			} else {
				from = findElement(dynamicXpathLocator(JourneysPageElements.HOVER_OVER, connectFrom, hover));
			}
			a.dragAndDrop(from, to).build().perform();
		}
		return splitBlock;
	}

	/**
	 * Fetch id of the block that has been chosen
	 * 
	 * @param template Journey Template
	 * @param type     Block of whose id needs to be fetched
	 * @return String id of the block
	 * @since V1.0.0
	 * @version 1.0.0
	 */
	private String fetchIDOfDesired(JSONObject template, String type) {
		JSONObject jsonObj = (JSONObject) template.get(type);
		return (String) jsonObj.get("id");
	}

	/**
	 * Place the blocks at desired coordinates mentioned in the template file or
	 * update the coordinates in template post alignment of blocks
	 * 
	 * @param template        Journey template
	 * @param updateAttribute Boolean set as true to update block location on canvas
	 *                        and set as false to update template with current
	 *                        value(post alignment)
	 * @throws InterruptedException
	 * @since V1.0.0
	 * @version 1.0.0
	 */
	public void transform(JSONObject template, boolean updateAttribute) throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		Iterator<?> itr = template.keySet().iterator();
		while (itr.hasNext()) {
			String s = (String) itr.next();
			if (!(s.equalsIgnoreCase("paths") || s.equalsIgnoreCase("blocks") || s.equalsIgnoreCase("stats"))) {
				JSONObject temp = (JSONObject) template.get(s);
				String value = (String) temp.get("transform");
				String id = fetchIDOfDesired(template, s);
				if (updateAttribute)
					js.executeScript("document.getElementById('" + id + "').setAttribute('transform','" + value + "')");
				else
					updateJSONTransformKey(temp, id);
			}
		}
	}

	/**
	 * The value of transform key in template will be updated with current value of
	 * the transform attribute of already laced block on canvas
	 * 
	 * @param temp Journey template
	 * @param id   id of the block
	 * @since V1.0.0
	 * @version 1.0.0
	 */
	@SuppressWarnings("unchecked")
	private void updateJSONTransformKey(JSONObject temp, String id) {
		String updatedTransformValue = fetchAttributeValue(dynamicXpathLocator(JourneysPageElements.LOCATE_BLOCK, id),
				"transform");
		temp.put("transform", updatedTransformValue);
	}

	/**
	 * Save the Journey as Draft. The temp id get updated to state id in this step
	 * 
	 * @since V1.0.0
	 * @version 1.0.0
	 */
	public void saveJourney() {
		click(JourneysPageElements.SAVE_DRAFT.locate());
	}

	/**
	 * Zoom out of canvas by 5 clicks for loop set to value as 5
	 * 
	 * @since V1.0.0
	 * @version 1.0.0
	 */
	public void zoomOut() {
		for (int i = 0; i < 5; i++) {
			click(JourneysPageElements.ZOOM_OUT.locate());
		}
	}

	/**
	 * Fill data for each individual block. The block data is provided in Journey
	 * template
	 * 
	 * @param template Journey Template
	 * @throws InterruptedException
	 * @since V1.0.0
	 * @version 1.6.4
	 */
	public void addBlockData(JSONObject template) throws InterruptedException {
		setjourneyTemplate(template);
		CampaignsPage campPage = new CampaignsPage();
		Iterator<?> itr = template.keySet().iterator();
		boolean isManualBlockPresent = false;
		while (itr.hasNext()) {
			String s = (String) itr.next();
			if (!(s.equalsIgnoreCase("paths") || s.equalsIgnoreCase("blocks") || s.equalsIgnoreCase("stats")
					|| s.equalsIgnoreCase("path_split"))) {
				waitForElementToBeInvisible(CommonPageElements.SAVE_BUTTON.locate());
				TimeUnit.SECONDS.sleep(1);
				JSONObject temp = (JSONObject) template.get(s);
				JSONObject data = (JSONObject) temp.get("data");
				String id = (String) temp.get("id");
				click(dynamicXpathLocator(JourneysPageElements.OPEN_BLOCK, id));
				switch (s.replaceAll("\\d", "")) {
				case "Segment":
					fillSegmentBlock(data);
					break;
				case "IsOnChannel":
					fillIsOnChannelBlock(data);
					break;
				case "SendText":
					campPage.fillSendTextBlock(data);
					break;
				case "SendPush":
					campPage.fillSendPushBlock(data);
					break;
				case "SendEmail":
					campPage.fillSendMailBlock(data);
					break;
				case "Wait":
					fillWaitBlock(data);
					break;
				case "Event":
					fillEventBlock(data);
					break;
				case "BusinessEvent":
					fillBusinessEventBlock(data);
					break;
				case "AttributeChange":
					fillAttributeChangeBlock(data);
					break;
				case "WaitForTimeSlots":
					fillWaitForTimeSlotsBlock(data);
					break;
				case "SetAttribute":
					fillSetAttributeSlotsBlock(data);
					break;
				case "Manual":
					isManualBlockPresent = true;
					click(JourneysPageElements.CLOSE_BTN.locate());
					break;
				case "HasDoneEvent":
					fillHasDoneEvent(data);
					break;
				case "API":
					fillAPIBlock(data);
					break;
				case "CheckAttribute":
					fillCheckAttributeBlock(data);
					break;
				case "GeoFence":
					fillGeofenceBlock(data);
					break;
				case "WaitForDate":
					fillWaitForDateBlock(data);
					break;
				case "SendWhatsApp":
					campPage.fillWABlock(data);
					break;
				case "ShowInApp":
					campPage.fillInAppBlock(data);
					break;
				case "ShowAppPersonalization":
					campPage.fillAppInLineBlock(data);
					break;
				case "SendWebPush":
					campPage.fillWebPushBlock(data);
					break;
				case "ShowWebPersonalization":
					campPage.fillInlineWebBlock(data);
					break;
				case "ShowWeb":
					campPage.fillOnsiteBlock(data);
					break;
				case "IsInSegment":
					fillIsInSegmentBlock(data);
					break;
				case "WaitForEvent":
					fillWaitForEventBlock(data);
					break;
				default:
				}
			}
		}
		if (isManualBlockPresent) {
			JSONObject blockData = (JSONObject) template.get("Manual");
			TimeUnit.SECONDS.sleep(3);
			click(dynamicXpathLocator(JourneysPageElements.OPEN_BLOCK, (String) blockData.get("id")));
			fillManualBlock((JSONObject) blockData.get("data"));
		}
	}

	public void fillSplitBlock(String id, JSONObject data) throws InterruptedException {
		click(dynamicXpathLocator(JourneysPageElements.OPEN_BLOCK, id));
		if (data.get("Intelligent Optimization").equals("true"))
			click(dynamicXpathLocator(CommonPageElements.CHECKBOX_INPUT, "Intelligent Optimization"));
		saveButton();
		TimeUnit.SECONDS.sleep(2);
	}

	private void fillWaitForEventBlock(JSONObject data) {
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Flow Control: Wait for Event"));
		selectValueFromDropDown((String) data.get("Select Event"));
		clearFieldAndSendkeys(JourneysPageElements.INPUT_NUMBER.locate(), (String) data.get("Duration"));
		click(CommonPageElements.DD_SELECT_VALUE.locate());
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, (String) data.get("Period")));
		saveButton();
	}

	private void fillBusinessEventBlock(JSONObject data) {
		waitForElementToBeVisible(
				dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Trigger: When Business Event Occurs"));
		waitUntilAttributeDoesNotContain(JourneysPageElements.DOWNSHIFT_DD_ENABLE.locate(), "class", "disabled");
		click(JourneysPageElements.DOWNSHIFT_DD_OPEN.locate());
		click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, (String) data.get("Select Event")));
		click(dynamicXpathLocator(JourneysPageElements.OPEN_DD, "SEGMENT"));
		sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER_CONTAINS, "Search"),
				(String) data.get("Segment"));
		click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, (String) data.get("Segment")));
		if (data.containsKey("B2S")) {
			click(dynamicXpathLocator(CommonPageElements.MODULE, (String) data.get("B2S")));
			click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "-"));
			findElement(CommonPageElements.SEARCH.locate());
			click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, (String) data.get("B2U")));
			click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Select User Event Attribute"));
			click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, (String) data.get("User Event Attribute")));
		} else {
			click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Select User Profile Attribute"));
			click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, (String) data.get("User Attribute")));
		}
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Select Business Event Attribute"));
		click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, (String) data.get("Business Event Attribute")));
		waitForSaveButtonEnabled();
		saveButton();
	}

	private void fillIsInSegmentBlock(JSONObject data) {
		String checkUser = (String) data.get("Check if user is in");
		if (checkUser == null) {
			click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Create new segment"));
			sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Segment Name"),
					"Create segment through IsInSegment condition");
			campPage.readFromTemplate((JSONObject) data.get("Message"), "Basic");
		} else {
			selectValueFromDropDown(checkUser);
			saveButton();
		}
	}

	private void fillWaitForDateBlock(JSONObject data) {
		waitForElementToBePresent(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Flow Control: Wait for Date"));
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, (String) data.get("Type of Wait")));
		try {
			TimeUnit.MILLISECONDS.sleep(1500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		openDropdown(dynamicXpathLocator(JourneysPageElements.SELECT_OPTION, "Select Attribute"));
		click(dynamicXpathLocator(JourneysPageElements.SELECT_OPTION, (String) data.get("Select Attribute")));
		click(JourneysPageElements.SELECT_VALUE.locate());
		click(dynamicXpathLocator(JourneysPageElements.SELECT_OPTION, (String) data.get("Then")));
		saveButton();
	}

	private void fillGeofenceBlock(JSONObject data) throws InterruptedException {
		waitForElementToBePresent(
				dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Trigger: Enter/Exit Geo-fence"));
		click(JourneysPageElements.SELECT_VALUE.locate());
		click(dynamicXpathLocator(JourneysPageElements.SELECT_OPTION, (String) data.get("action")));
		driver.switchTo().frame(0);
		sendKeys(dynamicXpathLocator(JourneysPageElements.PLACEHOLDER_VALUE, "Search for a location"),
				(String) data.get("Search for a location"));
		TimeUnit.MILLISECONDS.sleep(1500);
		sendKeys(dynamicXpathLocator(JourneysPageElements.PLACEHOLDER_VALUE, "Search for a location"), Keys.ARROW_DOWN);
		sendKeys(dynamicXpathLocator(JourneysPageElements.PLACEHOLDER_VALUE, "Search for a location"), Keys.RETURN);
		driver.switchTo().defaultContent();
		waitForSaveButtonEnabled();
		saveButton();
	}

	private void fillCheckAttributeBlock(JSONObject data) {
		String attribute = (String) data.get("Attribute");
		waitForElementToBePresent(
				dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Condition: Check User Attribute"));
		openDropdown(dynamicXpathLocator(JourneysPageElements.SELECT_OPTION, "Select Attribute"));
		click(dynamicXpathLocator(JourneysPageElements.SELECT_OPTION, (String) data.get("Select Attribute")));
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Select..."));
		click(dynamicXpathLocator(JourneysPageElements.SELECT_OPTION, (String) data.get("Select...")));
		if (attribute != null) {
			click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Select..."));
			click(dynamicXpathLocator(JourneysPageElements.SELECT_OPTION, attribute));
		}
		saveButton();
	}

	private void fillAPIBlock(JSONObject data) {
		waitForElementToBePresent(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Build API Request"));
		click(JourneysPageElements.SELECT_VALUE.locate());
		click(dynamicXpathLocator(JourneysPageElements.SELECT_OPTION, (String) data.get("Method")));
		sendKeys(dynamicXpathLocator(JourneysPageElements.PLACEHOLDER_VALUE,
				"Enter URL starting with http:// or https://"), (String) data.get("URL"));
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "Continue"));
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "Call API Now"));
		verifyAPIResponse(data);
		saveButton();
	}

	private void verifyAPIResponse(JSONObject data) {
		Assert.assertEquals((String) data.get("Method"),
				fetchText(dynamicXpathLocator(JourneysPageElements.FETCH_API_RESULTS, "Method")));
		Assert.assertEquals((String) data.get("URL"),
				fetchText(dynamicXpathLocator(JourneysPageElements.FETCH_API_RESULTS, "URL")));
		Assert.assertEquals((String) data.get("Status"),
				fetchText(dynamicXpathLocator(JourneysPageElements.FETCH_API_RESULTS, "Status")));
		Assert.assertTrue(fetchText(dynamicXpathLocator(JourneysPageElements.FETCH_API_RESULTS, "Body"))
				.contains((String) data.get("Body")));
	}

	private void fillHasDoneEvent(JSONObject data) {
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Condition: Has Done Event"));
		selectValueFromDropDown((String) data.get("Select Event"));
		waitForSaveButtonEnabled();
		saveButton();
	}

	private void fillManualBlock(JSONObject data) {
		waitForElementToBeVisible(
				dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Enter or Upload List of Users"));
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, (String) data.get("trigger journey for")));
		String triggerJourneyFor = findElement(JourneysPageElements.SPECIFIC_USER_TRIGGER.locate()).getText();
		CustomAssertions.assertEquals("Radio button selection mismatch", (String) data.get("trigger journey for"),
				triggerJourneyFor);
		if (triggerJourneyFor.equalsIgnoreCase("Users in CSV file")) {
			uploadFile("user_ids_sample.csv", JourneysPageElements.SELECT_FILE_BUTTON.locate());
			waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "History"));
			verifyToastMessage(NotificationToastMessages.USER_CSV_JOURNEY.message());
		} else if (triggerJourneyFor.equalsIgnoreCase("Users in list below")) {
			sendKeys(JourneysPageElements.TEXTAREA.locate(), (String) data.get("List of Users"));
			try {
				TimeUnit.SECONDS.sleep(3);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			waitForSaveButtonEnabled();
			saveButton();
			verifyToastMessage(NotificationToastMessages.LIST_JOURNEY.message());
		}
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "Return to Journey Canvas"));
	}

	private void fillSetAttributeSlotsBlock(JSONObject data) {
		waitForElementToBePresent(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Action: Set User Attribute"));
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, (String) data.get("action type")));
		click(dynamicXpathLocator(JourneysPageElements.SELECT_OPTION, "Select Attribute"));
		click(dynamicXpathLocator(JourneysPageElements.SELECT_OPTION, (String) data.get("Select Attribute")));
		click(JourneysPageElements.SELECT_VALUE.locate());
		click(dynamicXpathLocator(JourneysPageElements.SELECT_OPTION, (String) data.get("set or increase")));
		waitForElementToBePresent(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS,
				"If the selected user attribute has not been initialized"));
		sendKeys(dynamicXpathLocator(JourneysPageElements.PLACEHOLDER_VALUE, "Enter a value"),
				(String) data.get("Enter a value"));
		waitForSaveButtonEnabled();
		saveButton();
	}

	private void fillWaitForTimeSlotsBlock(JSONObject data) {
		waitForElementToBePresent(
				dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Flow Control: Wait for Time Slots"));
		click(JourneysPageElements.SELECT_DAY.locate());
		click(dynamicXpathLocator(JourneysPageElements.SELECT_OPTION, (String) data.get("day")));
		String from = (String) data.get("from");
		for (int i = 1; i <= 3; i++) {
			click(dynamicXpathLocator(JourneysPageElements.TIMESLOT_VALUE, "from", String.valueOf(i)));
			click(dynamicXpathLocator(JourneysPageElements.SELECT_OPTION, from.split(":")[i - 1]));
		}
		String to = (String) data.get("to");
		for (int i = 1; i <= 3; i++) {
			click(dynamicXpathLocator(JourneysPageElements.TIMESLOT_VALUE, "to", String.valueOf(i)));
			click(dynamicXpathLocator(JourneysPageElements.SELECT_OPTION, to.split(":")[i - 1]));
		}
		waitForSaveButtonEnabled();
		saveButton();
	}

	private void fillEventBlock(JSONObject data) {
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Trigger: Occurrence of Event"));
		selectValueFromDropDown((String) data.get("Select Event"));
		waitForSaveButtonEnabled();
		saveButton();
	}

	private void fillIsOnChannelBlock(JSONObject data) {
		click(JourneysPageElements.SELECT_VALUE.locate());
		click(dynamicXpathLocator(JourneysPageElements.SELECT_OPTION, (String) data.get("reachable on")));
		waitForSaveButtonEnabled();
		saveButton();
	}

	private void fillSegmentBlock(JSONObject data) {
		click(JourneysPageElements.SELECT_VALUE.locate());
		click(dynamicXpathLocator(JourneysPageElements.SELECT_OPTION, (String) data.get("trigger when")));
		selectValueFromDropDown((String) data.get("Select Segment"));
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Total Users:"));
		waitForSaveButtonEnabled();
		saveButton();
	}

	public void fillWaitBlock(JSONObject data) {
		clearFieldAndSendkeys(JourneysPageElements.INPUT_NUMBER.locate(), (String) data.get("duration"));
		click(JourneysPageElements.SELECT_VALUE.locate());
		click(dynamicXpathLocator(JourneysPageElements.SELECT_OPTION, (String) data.get("period")));
		saveButton();
	}

	/**
	 * Post saving the journey, the temp ids get updated as state-id, each block now
	 * has it's unique identifier as STATE-id(STATE-21)
	 * 
	 * @param template Journey Template
	 * @since V1.0.0
	 * @version 1.0.0
	 */
	@SuppressWarnings("unchecked")
	public void saveStateIDs(JSONObject template) {
		Iterator<?> itr = template.keySet().iterator();
		while (itr.hasNext()) {
			String s = (String) itr.next();
			if (!(s.equalsIgnoreCase("paths") || s.equalsIgnoreCase("blocks") || s.equalsIgnoreCase("stats"))) {
				JSONObject temp = (JSONObject) template.get(s);
				String value = (String) temp.get("transform");
				String stateId = fetchAttributeValue(dynamicXpathLocator(JourneysPageElements.TRANSFORM_VALUE, value),
						"model-id");
				temp.put("id", stateId);
			}
		}
	}

	/**
	 * Publish the formulated Journey instantaneously and save the start and end
	 * date.
	 * 
	 * @param journeyDetails HashMap which contains "Start Date" & "End Date"
	 * @since V1.0.0
	 * @version 1.0.0
	 */
	public void publishJourney(String type, HashMap<String, String> journeyDetails) {
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, "Publish"));
		acceptAlert();
		waitForElementToBePresent(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Publishing ..."));
		waitForElementToBeInvisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Publishing ..."));
		verifyToastMessage(type.equalsIgnoreCase("Journey") ? NotificationToastMessages.ACTIVATE_JOURNEY.message()
				: NotificationToastMessages.ACTIVATE_RELAY.message());
		journeyDetails.put("Start Date", "-");
		journeyDetails.put("End Date", "-");
		journeys.put(getJourneyName(), journeyDetails);
		Log.info("Published the " + type + " with start date as [" + journeyDetails.get("Start Date")
				+ "] and end date as [" + journeyDetails.get("End Date") + "]");
	}

	/**
	 * Publish the formulated Journey for later date and save the start and end
	 * date.
	 * 
	 * @param startDate      String time to be added into start date dd:hh:mm
	 * @param endDate        String time to be added into end date dd:hh:mm
	 * @param journeyDetails HashMap which contains "Start Date" & "End Date"
	 * @since V1.0.0
	 * @version 1.0.0
	 */
	public void publishJourneyLater(String startDate, String endDate, HashMap<String, String> journeyDetails) {
		click(JourneysPageElements.DROPDOWN_TOGGLE_ICON.locate());
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Publish Later"));
		DateFormat df = new SimpleDateFormat("MMMM yyyy");
		addIntoStartDate(startDate, df, journeyDetails);
		addIntoEndDate(endDate, df, journeyDetails);
		journeys.put(getJourneyName(), journeyDetails);
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "Schedule Journey"));
		waitForElementToBePresent(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Publishing ..."));
		waitForElementToBeInvisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Publishing ..."));
		verifyToastMessage(NotificationToastMessages.ACTIVATE_JOURNEY.message());
		Log.info("Scheduled the journey with start date as [" + journeyDetails.get("Start Date") + "] and end date as ["
				+ journeyDetails.get("End Date") + "]");
	}

	public void addIntoEndDate(String endDate, DateFormat df, HashMap<String, String> journeyDetails) {
		if (!endDate.equals("-")) {
			Calendar cal = addTime(endDate);
			journeyDetails.put("End Date", this.df.format(cal.getTime()));
			click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Till"));
			click(JourneysPageElements.END_DATE.locate());
			while (!fetchText(JourneysPageElements.MONTH_YEAR.locate()).equals(df.format(cal.getTime()))) {
				click(JourneysPageElements.NEXT_MONTH.locate());
			}
			click(dynamicXpathLocator(JourneysPageElements.SELECT_CAL_DAY,
					df.format(cal.getTime()).split(" ")[0] + " " + String.valueOf(cal.get(Calendar.DAY_OF_MONTH))));
			click(JourneysPageElements.END_INPUT_HOURS.locate());
			click(dynamicXpathLocator(JourneysPageElements.SELECT_OPTION,
					String.valueOf(cal.get(Calendar.HOUR) == 0 ? 12 : cal.get(Calendar.HOUR))));
			click(JourneysPageElements.END_INPUT_MINUTE.locate());
			String mins = String.valueOf(cal.get(Calendar.MINUTE));
			click(dynamicXpathLocator(JourneysPageElements.SELECT_OPTION, mins.length() == 1 ? "0" + mins : mins));
			click(JourneysPageElements.END_INPUT_AM_PM.locate());
			click(dynamicXpathLocator(JourneysPageElements.SELECT_OPTION, cal.get(Calendar.AM_PM) == 1 ? "pm" : "am"));
		} else {
			click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Never"));
			journeyDetails.put("End Date", "-");
		}
		Log.info("For user defined addition of time[" + endDate + "] the end date is now set as "
				+ journeyDetails.get("End Date"));
	}

	public void addIntoStartDate(String startDate, DateFormat df, HashMap<String, String> journeyDetails) {
		Calendar cal = (!startDate.equals("-")) ? addTime(startDate) : getRoundedOffTime(false);
		journeyDetails.put("Start Date", this.df.format(cal.getTime()));
		click(JourneysPageElements.START_INPUT_MINUTE.locate());
		String mins = String.valueOf(cal.get(Calendar.MINUTE));
		click(dynamicXpathLocator(JourneysPageElements.SELECT_OPTION, mins.length() == 1 ? "0" + mins : mins));
		click(JourneysPageElements.START_INPUT_HOURS.locate());
		click(dynamicXpathLocator(JourneysPageElements.SELECT_OPTION,
				String.valueOf(cal.get(Calendar.HOUR) == 0 ? 12 : cal.get(Calendar.HOUR))));
		click(JourneysPageElements.START_INPUT_AM_PM.locate());
		click(dynamicXpathLocator(JourneysPageElements.SELECT_OPTION, cal.get(Calendar.AM_PM) == 1 ? "pm" : "am"));
		click(JourneysPageElements.START_DATE.locate());
		while (!fetchText(JourneysPageElements.MONTH_YEAR.locate()).equals(df.format(cal.getTime()))) {
			click(JourneysPageElements.NEXT_MONTH.locate());
		}
		click(dynamicXpathLocator(JourneysPageElements.SELECT_CAL_DAY,
				df.format(cal.getTime()).split(" ")[0] + " " + String.valueOf(cal.get(Calendar.DAY_OF_MONTH))));
		Log.info("For user defined addition of time[" + startDate + "] the start date is now set as "
				+ journeyDetails.get("Start Date"));
	}

	public void navigateBack(String page) {
		click(JourneysPageElements.NAVIGATE_BACK.locate());
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, page));
	}

	/**
	 * Set the name of Journey via canvas page. Also, save journey name via setter
	 * method
	 * 
	 * @param journeyName Name of the Journey
	 * @since V1.0.0
	 * @throws InterruptedException
	 */
	public void setJourneyNameAs(String journeyName) throws InterruptedException {
		click(JourneysPageElements.JOURNEY_NAME.locate());
		setJourneyName(journeyName);
		clearFieldAndSendkeys(JourneysPageElements.JOURNEY_NAME.locate(), journeyName + Keys.RETURN);
		waitForElementToBePresent(dynamicXpathLocator(JourneysPageElements.GENERIC_TITLE, "Saved"));
		waitForElementToBePresent(dynamicXpathLocator(JourneysPageElements.GENERIC_TITLE, "Click to Edit"));
	}

	public void validateStatus(String status, String journeyName) {
		waitForElementToBePresent(dynamicXpathLocator(JourneysPageElements.JOURNEY_STATUS, journeyName));
		String actualStatus = fetchText(dynamicXpathLocator(JourneysPageElements.JOURNEY_STATUS, journeyName));
		Assert.assertEquals(status, actualStatus);
	}

	public void popOverAction(String journeyName, String action) {
		click(dynamicXpathLocator(JourneysPageElements.JOURNEY_POPOVER, journeyName));
		click(dynamicXpathLocator(CommonPageElements.ANCHOR_TAG_GENERIC_TEXT, action));
	}

	public void validateActionMessages(String action, String page) {
		click(dynamicXpathLocator(CommonPageElements.BUTTON, action));
		String msg;
		if (page.equalsIgnoreCase("Journey")) {
			msg = action.equals("Sunset") ? NotificationToastMessages.SUNSET_JOURNEY.message()
					: NotificationToastMessages.STOP_JOURNEY.message();

		} else {
			msg = action.equals("Sunset") ? NotificationToastMessages.SUNSET_RELAY.message()
					: NotificationToastMessages.STOP_RELAY.message();
		}
		verifyToastMessage(msg);
	}

	private void fillAttributeChangeBlock(JSONObject data) {
		waitForElementToBePresent(
				dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Trigger: Change in User Attribute"));
		openDropdown(JourneysPageElements.SELECT_VALUE.locate());
		click(dynamicXpathLocator(JourneysPageElements.SELECT_OPTION, (String) data.get("trigger journey when")));
		click(dynamicXpathLocator(JourneysPageElements.SELECT_OPTION, "Select Attribute"));
		click(dynamicXpathLocator(JourneysPageElements.SELECT_OPTION, (String) data.get("Select Attribute")));
		waitForSaveButtonEnabled();
		saveButton();
	}

	public void setConversionTracking(String event, Map<String, String> map) {
		click(JourneysPageElements.CONVERSION_TRACKING_BTN.locate());
		click(JourneysPageElements.TOGGLE.locate());
		Assert.assertTrue(fetchAttributeValue(JourneysPageElements.TOGGLE.locate(), "class").contains("checked"));
		commPage.selectSpecificDDValue(map.get("CONVERSION EVENT").split(">")[0],
				map.get("CONVERSION EVENT").split(">")[1]);
		clearFieldAndSendkeys(JourneysPageElements.INPUT_NUMBER.locate(), map.get("number"));
		click(JourneysPageElements.SELECT_VALUE.locate());
		click(dynamicXpathLocator(JourneysPageElements.SELECT_OPTION, map.get("Timeline")));
		saveButton();
		Assert.assertTrue(fetchText(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, "Conversion Event:"))
				.contains(event));
	}

	public void setExitTrigger(Map<String, String> map, String exitTriggerType) throws InterruptedException {
		click(JourneysPageElements.EXIT_TRIGGER_BTN.locate());
		click(CommonPageElements.ADD_BTN.locate());
		click(JourneysPageElements.SELECT_VALUE.locate());
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, exitTriggerType));
		waitForElementToBeVisible(JourneysPageElements.EXIT_CRITERIA.locate());
		TimeUnit.SECONDS.sleep(3);
		switch (exitTriggerType) {
		case "profile attibute changes": {
			click(JourneysPageElements.EXIT_TRIGGER_DD.locate());
			click(dynamicXpathLocator(JourneysPageElements.SELECT_OPTION, map.get("change type")));
			openDropdown(dynamicXpathLocator(JourneysPageElements.SELECT_OPTION, "Select Attribute"));
			click(dynamicXpathLocator(JourneysPageElements.SELECT_OPTION, map.get("attribute")));
			break;
		}
		case "user enters or exits a segment": {
			click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Select..."));
			click(dynamicXpathLocator(JourneysPageElements.SELECT_OPTION, map.get("user action")));
			selectValueFromDropDown(map.get("segment name"));
			waitForElementToBeVisible(SegmentsPageElements.CARD_CONTENT.locate());
			break;
		}
		}
		TimeUnit.SECONDS.sleep(5);
		saveButton();
		TimeUnit.SECONDS.sleep(3);
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, "1 Exit Trigger"));
	}

	public void captureJourney() {
		click(JourneysPageElements.SANPSHOT_BTN.locate());
		waitUntilAttributeContains(JourneysPageElements.SANPSHOT_BTN.locate(), "class",
				"button button--no-style is-processing cursor-not-allowed");
		waitUntilAttributeContains(JourneysPageElements.SANPSHOT_BTN.locate(), "class", "button button--no-style ");
	}

	public void triggerEventviaAPI(String refId) {
		try {
			Assert.assertEquals("queued", apiOperations.dynamic_post_method("performEvent", "PerformEvents", refId)
					.fetchValue("response.status"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void validateStartDate(String journeyName) {
		String actualDate = fetchText(dynamicXpathLocator(JourneysPageElements.JOURNEY_START_DATE, journeyName));
		Assert.assertEquals(journeys.get(journeyName).get("Creation Date").split(",")[0], actualDate);
		clickUsingJSExecutor(dynamicXpathLocator(CommonPageElements.ANCHOR_TAG_GENERIC_TEXT, journeyName));
		verifyInViewDetailsSection(journeyName, "CREATED ON", journeys.get(journeyName).get("Creation Date"));
		verifyInViewDetailsSection(journeyName, "Scheduled Start Date",
				journeys.get(journeyName).get("Start Date").replace("PM", "pm").replace("AM", "am"));
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Journeys"));
	}

	public void validateEndDate(String journeyName) {
		clickUsingJSExecutor(dynamicXpathLocator(CommonPageElements.ANCHOR_TAG_GENERIC_TEXT, journeyName));
		verifyInViewDetailsSection(journeyName, "Scheduled End Date",
				journeys.get(journeyName).get("End Date").replace("PM", "pm").replace("AM", "am"));
		click(dynamicXpathLocator(CommonPageElements.MODULE, "Journeys"));
	}

	public void verifyInViewDetailsSection(String journeyName, String parameter, String expectedValue) {
		click(JourneysPageElements.JOURNEY_DETAILS.locate());
		String actualValue = fetchText(dynamicXpathLocator(JourneysPageElements.JOURNEY_DETAIL_VALUE, parameter));
		Assert.assertEquals(expectedValue, actualValue);
		click(JourneysPageElements.JOURNEY_DETAILS.locate());
	}

	public void clickOnToolTip(String journeyName, String action) {
		clickUsingJSExecutor(dynamicXpathLocator(CommonPageElements.ANCHOR_TAG_GENERIC_TEXT, journeyName));
		click(dynamicXpathLocator(JourneysPageElements.TOOLTIP_BUTTONS, "Edit"));
	}

	public void verifyPublishLater() {
		click(JourneysPageElements.DROPDOWN_TOGGLE_ICON.locate());
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Publish Later"));
		DateFormat df = new SimpleDateFormat("d MMM ''yy");
		verifyJourneyStartDate(df);
		verifyJourneyEndDate(df);
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "cancel"));
	}

	private void verifyJourneyStartDate(DateFormat df) {
		Calendar cal = getRoundedOffTime(true);
		Assert.assertEquals(df.format(cal.getTime()), fetchText(JourneysPageElements.START_INPUT_DATE.locate()));
		Assert.assertEquals(String.valueOf(cal.get(Calendar.HOUR)),
				fetchText(JourneysPageElements.START_INPUT_HOURS.locate()).replace("12", "0"));
		Assert.assertEquals(new SimpleDateFormat("mm").format(cal.getTime()),
				fetchText(JourneysPageElements.START_INPUT_MINUTE.locate()));
		Assert.assertEquals(cal.get(Calendar.AM_PM) == 1 ? "pm" : "am",
				fetchText(JourneysPageElements.START_INPUT_AM_PM.locate()));
		Assert.assertTrue(isSelected(dynamicXpathLocator(JourneysPageElements.GENERIC_VALUE, "NEVER")));
		Assert.assertFalse(isSelected(dynamicXpathLocator(JourneysPageElements.GENERIC_VALUE, "TILL")));
	}

	private void verifyJourneyEndDate(DateFormat df) {
		Calendar cal = getRoundedOffTime(true);
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Till"));
		Assert.assertFalse(isSelected(dynamicXpathLocator(JourneysPageElements.GENERIC_VALUE, "NEVER")));
		Assert.assertTrue(isSelected(dynamicXpathLocator(JourneysPageElements.GENERIC_VALUE, "TILL")));
		cal.set(Calendar.MINUTE, cal.get(Calendar.MINUTE) + 5);
		Assert.assertEquals(df.format(cal.getTime()), fetchText(JourneysPageElements.END_INPUT_DATE.locate()));
		Assert.assertEquals(String.valueOf(cal.get(Calendar.HOUR)),
				fetchText(JourneysPageElements.END_INPUT_HOURS.locate()).replace("12", "0"));
		Assert.assertEquals(new SimpleDateFormat("mm").format(cal.getTime()),
				fetchText(JourneysPageElements.END_INPUT_MINUTE.locate()));
		Assert.assertEquals(cal.get(Calendar.AM_PM) == 1 ? "pm" : "am",
				fetchText(JourneysPageElements.END_INPUT_AM_PM.locate()));
	}

	public void openLiveView(String journeyName) {
		if (fetchText(dynamicXpathLocator(JourneysPageElements.JOURNEY_STATUS, journeyName))
				.equalsIgnoreCase("UPCOMING")) {
			clickUsingJSExecutor(dynamicXpathLocator(CommonPageElements.ANCHOR_TAG_GENERIC_TEXT, journeyName));
		} else {
			clickUsingJSExecutor(dynamicXpathLocator(CommonPageElements.ANCHOR_TAG_GENERIC_TEXT, journeyName));
			click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Live View"));
		}
		waitForElementToBeVisible(dynamicXpathLocator(JourneysPageElements.CARD_LABELS, "Start Date"));
		waitForElementToBeVisible(dynamicXpathLocator(JourneysPageElements.CARD_LABELS, "End Date"));
		waitForElementToBeVisible(dynamicXpathLocator(JourneysPageElements.CARD_LABELS, "Schedule"));
	}

	public Calendar addTime(String timeToAdd) {
		String dayToAdd = timeToAdd.split(":")[0];
		String hoursToAdd = timeToAdd.split(":")[1];
		String minsToAdd = timeToAdd.split(":")[2];
		Calendar calendar = getRoundedOffTime(false);
		calendar.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE) + Integer.parseInt(minsToAdd));
		int minutesToAdd = ((calendar.get(Calendar.MINUTE) % 10) % 5 == 0) ? 0
				: 5 - ((calendar.get(Calendar.MINUTE) % 10) % 5);
		calendar.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE) + minutesToAdd);
		calendar.set(Calendar.HOUR, calendar.get(Calendar.HOUR) + Integer.parseInt(hoursToAdd));
		calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + Integer.parseInt(dayToAdd));
		return calendar;
	}

	/**
	 * Save creation date & time of the Journey
	 * 
	 * @return The creation date of Journey. i.e. the system time at which it got
	 *         created
	 * @since V1.0.0
	 * @version 1.0.0
	 */
	public HashMap<String, String> setJourneyDetails() {
		HashMap<String, String> journeyDetails = new HashMap<>();
		journeyDetails.put("Creation Date", getJourneyCreationDate());
		Log.info("Creating Journey with name as " + getJourneyName() + " with creation date as "
				+ getJourneyCreationDate());
		return journeyDetails;
	}

	public void verifySchedeuledCard(String journeyName) {
		String startDate = fetchText(dynamicXpathLocator(JourneysPageElements.SCHEDULE_DATES, "Start Date"));
		String endDate = fetchText(dynamicXpathLocator(JourneysPageElements.SCHEDULE_DATES, "End Date"));
		Assert.assertEquals(journeys.get(journeyName).get("Start Date").replace("PM", "pm").replace("AM", "am"),
				startDate);
		Assert.assertEquals(journeys.get(journeyName).get("End Date").replace("PM", "pm").replace("AM", "am"), endDate);
	}

	public void modifyScheduleDates(String journeyName, String addStartDate, String addEndDate) {
		String status = fetchText(JourneysPageElements.STATUS.locate());
		String startDate = fetchText(dynamicXpathLocator(JourneysPageElements.SCHEDULE_DATES, "Start Date"));
		String endDate = fetchText(dynamicXpathLocator(JourneysPageElements.SCHEDULE_DATES, "End Date"));
		click(JourneysPageElements.SCHEDULE_BTN.locate());
		DateFormat df = new SimpleDateFormat("MMMM yyyy");
		HashMap<String, String> journeyDetails = journeys.get(journeyName);
		if (status.equalsIgnoreCase("Running") || status.equalsIgnoreCase("Sunset")) {
			if (startDate.equals("-")) {
				waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Not configured"));
			} else {
				Assert.assertEquals(fetchText(JourneysPageElements.START_INPUT_DATE.locate()), startDate.split(",")[0]);
				waitUntilAttributeContains(JourneysPageElements.START_INPUT_DATE.locate(), "class", "disabled");
				String hours = fetchText(JourneysPageElements.START_INPUT_HOURS.locate());
				Assert.assertEquals(hours.length() == 1 ? "0" + hours : hours,
						startDate.split(",")[1].split(":")[0].trim());
				waitUntilAttributeContains(JourneysPageElements.START_INPUT_HOURS.locate(), "class", "disabled");
				Assert.assertEquals(fetchText(JourneysPageElements.START_INPUT_MINUTE.locate()),
						startDate.split(",")[1].split(":")[1].replaceAll("[a-z]", ""));
				waitUntilAttributeContains(JourneysPageElements.START_INPUT_MINUTE.locate(), "class", "disabled");
				Assert.assertEquals(fetchText(JourneysPageElements.START_INPUT_AM_PM.locate()),
						startDate.split(",")[1].split(":")[1].replaceAll("\\d", ""));
				waitUntilAttributeContains(JourneysPageElements.START_INPUT_AM_PM.locate(), "class", "disabled");
			}
		} else {
			Assert.assertEquals(fetchText(JourneysPageElements.START_INPUT_DATE.locate()), startDate.split(",")[0]);
			String hours = fetchText(JourneysPageElements.START_INPUT_HOURS.locate());
			Assert.assertEquals(hours.length() == 1 ? "0" + hours : hours,
					startDate.split(",")[1].split(":")[0].trim());
			Assert.assertEquals(fetchText(JourneysPageElements.START_INPUT_MINUTE.locate()),
					startDate.split(",")[1].split(":")[1].replaceAll("[a-z]", ""));
			Assert.assertEquals(fetchText(JourneysPageElements.START_INPUT_AM_PM.locate()),
					startDate.split(",")[1].split(":")[1].replaceAll("\\d", ""));
			addIntoStartDate(addStartDate, df, journeyDetails);
		}
		if (endDate.equals("-"))
			isSelected(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Never"));
		else
			isSelected(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Till"));
		addIntoEndDate(addEndDate, df, journeyDetails);
		saveButton();
		waitForElementToBeInvisible(CommonPageElements.SAVE_BUTTON.locate());
		waitForElementToBeInvisible(JourneysPageElements.JOURNEY_BLACKOUT.locate());
		try {
			TimeUnit.SECONDS.sleep(3);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		verifySchedeuledCard(journeyName);
	}

	public void verifyAuditLogSection(String journeyName) {
		clickUsingJSExecutor(dynamicXpathLocator(CommonPageElements.ANCHOR_TAG_GENERIC_TEXT, journeyName));
		click(JourneysPageElements.JOURNEY_DETAILS.locate());
		click(JourneysPageElements.AUDIT_LOG_VIEW.locate());
		List<WebElement> logs = findElements(JourneysPageElements.AUDIT_LOG_ROWS.locate());
		int check = 0;
		for (WebElement log : logs) {
			String date = log.findElement(JourneysPageElements.LOG_DATE.locate()).getText();
			String text = log.findElement(JourneysPageElements.LOG_NAME.locate()).getText();
			try {
				if (text.contains("launched")) {
					++check;
					assertDateWithinTolerance(
							journeys.get(journeyName).get("Start Date").replace("PM", "pm").replace("AM", "am"), date);
				}
				if (text.contains("stopped")) {
					++check;
					assertDateWithinTolerance(
							journeys.get(journeyName).get("End Date").replace("PM", "pm").replace("AM", "am"), date);
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Assert.assertTrue(check > 0);
		}
	}

	private void assertDateWithinTolerance(String expectedDate, String actualDate) throws ParseException {
		DateFormat dateFormat = new SimpleDateFormat("d MMM ''yy, hh:mma");
		try {
			long difference = Math
					.abs(dateFormat.parse(expectedDate).getTime() - dateFormat.parse(actualDate).getTime());
			CustomAssertions.assertTrue("Date is not within tolerance of +/- 1 minute", difference <= 60 * 1000);
		} catch (java.text.ParseException e) {
			e.printStackTrace();
		}
	}

	public HashMap<String, String> fetchBlockIds() throws ParseException {
		HashMap<String, String> ids = new HashMap<>();
		APIUtility apiUtil = new APIUtility();
		for (String s : new String[] { "response.data.entryTriggers", "response.data.states" }) {
			List<Object> objList = apiUtil.fetchObjectList(s);
			for (Object obj : objList) {
				JSONObject jo = apiUtil.getJsonObject(new Gson().toJson(obj));
				try {
					String subActivity = ((JSONObject) jo.get("activity")).get("subType").toString();
					if (subActivity.equals("dead_end")) {
						continue;
					}
					if (subActivity.equals("the_end") || subActivity.equals("action_event")) {
						getJourneyIds().add(jo.get("id").toString());
					}
				} catch (Exception e) {
				}
				ids.put(jo.get("label").toString(), jo.get("id").toString());
			}
		}
		return ids;
	}

	public HashMap<String, HashMap<String, String>> extractActualStats() throws ParseException {
		HashMap<String, HashMap<String, String>> ids = new HashMap<>();
		APIUtility apiUtil = new APIUtility();
		List<Object> objList = apiUtil.fetchObjectList("response.data.contents[0].stats.journeyTriggerSets");
		for (Object obj : objList) {
			HashMap<String, String> value = new HashMap<>();
			JSONObject jo = apiUtil.getJsonObject(new Gson().toJson(obj));
			value.put("exited", jo.get("exited").toString());
			ids.put(jo.get("id").toString(), value);
		}
		objList = apiUtil.fetchObjectList("response.data.contents[0].stats.states");
		for (Object obj : objList) {
			HashMap<String, String> value = new HashMap<>();
			JSONObject jo = apiUtil.getJsonObject(new Gson().toJson(obj));
			value.put("entered", jo.get("entered").toString());
			value.put("exited", jo.get("exited").toString());
			ids.put(jo.get("id").toString(), value);
		}
		getJourneyIds().forEach(s -> {
			ids.remove(s);
		});
		return ids;
	}

	public void validateEachBlock(HashMap<String, String> blockIds, String type) {
		HashMap<String, HashMap<String, String>> expectedStats = new HashMap<>();
		Iterator<?> itr = getjourneyTemplate().keySet().iterator();
		while (itr.hasNext()) {
			String s = (String) itr.next();
			if (!(s.equalsIgnoreCase("paths") || s.equalsIgnoreCase("blocks") || s.equalsIgnoreCase("stats"))) {
				HashMap<String, String> stats = new HashMap<>();
				JSONObject temp = (JSONObject) template.get(s);
				JSONObject statsObj = (JSONObject) temp.get("stats");
				try {
					stats.put("entered", statsObj.get("entered").toString());
					if (stats.get("entered").equals("0"))
						continue;
				} catch (NullPointerException e) {
					Log.info("Trigger Block");
				}
				stats.put("exited", statsObj.get("exited").toString());
				String value = (String) temp.get("id");
				expectedStats.put(blockIds.get(value), stats);
			}
		}
		switch (type) {
		case "Polling":
			try {
				apiOperations.getMethod("journeyStats");
				HashMap<String, HashMap<String, String>> actualStats = extractActualStats();
				Assert.assertEquals(expectedStats.size(), actualStats.size());
				Assert.assertEquals(expectedStats, actualStats);
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		case "Distributed":
			JSONObject stats = (JSONObject) getjourneyTemplate().get("stats");
			String overallEnteredCount = stats.get("entered").toString();
			String overallExitedCount = stats.get("exited").toString();
			HashMap<String, String> overallStats = new HashMap<>();
			overallStats.put("OverallEnteredCount", overallEnteredCount);
			overallStats.put("OverallExitCount", overallExitedCount);
			expectedStats.put("OverallStats", overallStats);
			JourneysPage.setJourneyExpectedPayload(hashmapToJsonString(expectedStats));
			break;
		default:
			CustomAssertions.assertFalse("Case can only be Polling or Distributed", true);
		}
	}

	public String hashmapToJsonString(HashMap<String, HashMap<String, String>> map) {
		ObjectMapper objmap = new ObjectMapper();
		try {
			return objmap.writeValueAsString(map);
		} catch (JsonProcessingException e) {
			return null;
		}
	}

	public void configureSplitBlock(JSONObject template) throws InterruptedException {
		String id = (String) ((JSONObject) template.get("path_split")).get("id");
		fillSplitBlock(id, (JSONObject) ((JSONObject) template.get("path_split")).get("data"));
		makeConnections(template, false, (JSONArray) template.get("paths"));
		findElement(JourneysPageElements.ALIGN_VERTICALLY.locate()).click();
		findElement(JourneysPageElements.BTN_YES.locate()).click();
	}

	public void selectValueFromDropDown(String value) {
		waitUntilAttributeDoesNotContain(JourneysPageElements.DOWNSHIFT_DD_ENABLE.locate(), "class", "disabled");
		click(JourneysPageElements.DOWNSHIFT_DD_OPEN.locate());
		commPage.selectInDropdown(JourneysPageElements.DOWNSHIFT_DD_OPEN.locate(), value);
	}

	public void verifyMultiLingualCampaigns(List<List<String>> choices) {
		Map<String, String> map = new HashMap<String, String>();
		verifyCampaignsInJourneyOverviewPage(map, choices);
		verifyCampaignsInListPageViaJourneys(map);
	}

	public void verifyJourneyNameinOverViewPage(String journeyName) {
		CustomAssertions.assertEquals("Journey Name does not match:", journeyName,
				fetchText(JourneysPageElements.JOURNEY_NAME_OVERVIEW.locate()));

	}

	private void verifyCampaignsInJourneyOverviewPage(Map<String, String> map, List<List<String>> choices) {
		Set<String> expectedCampaignNameSet = new HashSet<String>();
		for (List<String> choice : choices) {
			String campaignType = choice.get(0);
			String expectedCampaignName = choice.get(1);
			expectedCampaignNameSet.add(expectedCampaignName);
			map.put(campaignType, expectedCampaignName);
		}
		int size = findElements(JourneysPageElements.CAMPAIGNNAMES.locate()).size();
		for (int i = 0; i < size; i++) {
			String actualCampaignName = findElements(JourneysPageElements.CAMPAIGNNAMES.locate()).get(i).getText();
			CustomAssertions.assertEquals("Campaign Name does not match", true,
					(expectedCampaignNameSet.contains(actualCampaignName)));
		}
	}

	private void verifyCampaignsInListPageViaJourneys(Map<String, String> map) {
		map.entrySet().forEach(entry -> {
			String[] arr = entry.getKey().split("_");
			campPage.openPageOnDashboard(arr[1], arr[0]);
			campPage.searchBar(entry.getValue());
			campPage.verifyCampaignNameOnListPage(entry.getValue());
		});
	}

	public void openJourneySettings(String segmentType, String segmentName) {
		click(JourneysPageElements.JOURNEY_SETTINGS.locate());
		if (segmentType.equals("Existing segment")) {
			selectValueFromDropDown(segmentName);
		} else {
			click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, segmentType));
			sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Segment Name"), segmentName);
		}
	}

	public void changeState(String status) {
		click(JourneysPageElements.CAMPAIGNNAMES.locate());
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Live View"));
		if (status.equalsIgnoreCase("STOPPED")) {
			click(dynamicXpathLocator(CommonPageElements.ANCHOR_TAG_GENERIC_TEXT_CONTAINS, "Edit"));
		}
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, "Publish"));
		acceptAlert();
	}

}