package com.webengage.ui_automation.pages;

import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.webengage.ui_automation.elements.AlertsPageElements;
import com.webengage.ui_automation.elements.CampaignsPageElements;
import com.webengage.ui_automation.elements.CommonPageElements;
import com.webengage.ui_automation.elements.JourneysPageElements;
import com.webengage.ui_automation.elements.OnsiteSurveysPageElements;
import com.webengage.ui_automation.elements.SegmentsPageElements;
import com.webengage.ui_automation.utils.CustomAssertions;
import com.webengage.ui_automation.utils.Log;
import com.webengage.ui_automation.utils.NotificationToastMessages;
import com.webengage.ui_automation.utils.ReflectionsUtility;
import com.webengage.ui_automation.utils.SeleniumExtendedUtility;

public class AlertsPage extends SeleniumExtendedUtility {
	CommonPage commonPage = new CommonPage();
	SegmentsPage segPage = new SegmentsPage();
	AnalyticsPage analyticsPage = new AnalyticsPage();
	ReflectionsUtility refl = new ReflectionsUtility();

	public void customAlertCRUDS(String action, String alertName) {
		selectAlert(alertName);
		clickUsingJSExecutor(dynamicXpathLocator(CommonPageElements.OPEN_POP_OVER_MENU, alertName));
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, action));
		switch (action) {
		case "Edit":
			editCustomAlert(alertName);
			break;
		case "Subscribe":
			commonPage.verifyToastMessage(NotificationToastMessages.SUBSCRIBE_ALERT.message());
			CustomAssertions.assertEquals(action + " failed", segPage.getColumnValue(alertName, "Status", ""),
					action + "d");
			break;
		case "Unsubscribe":
			commonPage.verifyToastMessage(NotificationToastMessages.UNSUBSCRIBE_ALERT.message());
			CustomAssertions.assertEquals(action + " failed", segPage.getColumnValue(alertName, "Status", ""),
					action + "d");
			break;
		case "Pause":
			commonPage.clickButton(action);
			commonPage.verifyToastMessage(NotificationToastMessages.PAUSE_ALERT.message());
			break;
		case "Resume":
			commonPage.verifyToastMessage(NotificationToastMessages.RESUME_ALERT.message());
			break;
		case "Delete":
			commonPage.clickButton(action);
			commonPage.verifyToastMessage(NotificationToastMessages.DELETE_ALERT.message());
			CustomAssertions.assertFalse("Today's Temporary alert not found",
					checkIfElementisLocated(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, alertName)));
			break;
		case "Copy":
			alertName = "Duplicate " + alertName;
			clearFieldAndSendkeys(dynamicXpathLocator(CampaignsPageElements.INPUT_FIELD, "ALERT NAME"), alertName);
			commonPage.clickButton("Create Alert");
			commonPage.verifyToastMessage(NotificationToastMessages.ALERT_CREATED.message());
			CustomAssertions.assertTrue("Duplicate Alert created not found",
					segPage.getColumnValue(alertName, "Created On", "")
							.contains(analyticsPage.getDate("today", "d MMM ''yy")));
			break;
		}
	}

	public void editCustomAlert(String alertName) {
		alertName = "Edited " + alertName;
		clearFieldAndSendkeys(dynamicXpathLocator(CampaignsPageElements.INPUT_FIELD, "ALERT NAME"), alertName);
		click(dynamicXpathLocator(JourneysPageElements.GENERIC_TITLE, "Clear all"));
		click(dynamicXpathLocator(AlertsPageElements.RELATIVE_DROPDOWN, "Notify"));
		click(dynamicXpathLocator(CampaignsPageElements.SELECT_FROM_DD, "Sandip"));
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "Save Changes"));
		CustomAssertions.assertTrue("Edited Alert not found", segPage.getColumnValue(alertName, "Created On", "")
				.contains(analyticsPage.getDate("today", "d MMM ''yy")));
	}

	public void selectAlert(String alertName) {
		clearFieldAndSendkeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER_CONTAINS, "Search"),
				alertName + Keys.RETURN);
		String todayDate = analyticsPage.getDate("today", "d MMM ''yy");
		CustomAssertions.assertTrue("Today's Temporary alert not found",
				segPage.getColumnValue(alertName, "Created On", "").contains(todayDate));
	}

	public void verifyAlertShowDetailsBlock(JSONObject template) {
		Map<String, Object> map = refl.jsonObjectToMap((JSONObject) template.get("ExpectedData"));
		String alertName = map.get("ALERT NAME").toString();
		selectAlert(alertName);
		clickUsingJSExecutor(dynamicXpathLocator(CommonPageElements.OPEN_POP_OVER_MENU, alertName));
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Edit"));
		click(SegmentsPageElements.SHOWDETAILS.locate());
		for (String keyName : map.keySet()) {
			switch (keyName) {
			case "Created date":
			case "Last updated date":
				CustomAssertions.assertTrue("invalid details " + keyName,
						fetchText(dynamicXpathLocator(AlertsPageElements.GET_SHOW_DETAILS_VALUE, keyName))
								.contains(analyticsPage.getDate("today", "d MMM ''yy")));
				break;
			case "ALERT NAME":
				CustomAssertions.assertEquals("invalid details " + keyName, map.get(keyName).toString(),
						fetchAttributeValue((dynamicXpathLocator(CampaignsPageElements.INPUT_FIELD, keyName)),
								"value"));
				break;
			case "Created by":
				CustomAssertions.assertTrue("invalid details " + keyName,
						fetchText(dynamicXpathLocator(AlertsPageElements.GET_SHOW_DETAILS_VALUE, keyName))
								.contains(map.get(keyName).toString()));
				break;
			case "description":
				CustomAssertions.assertEquals("invalid details " + keyName, map.get(keyName).toString(),
						fetchText(OnsiteSurveysPageElements.TEXT_QUESTION.locate()));
				break;
			default:
				CustomAssertions.assertEquals("invalid details " + keyName,
						fetchText(dynamicXpathLocator(AlertsPageElements.GET_SHOW_DETAILS_VALUE, keyName)),
						map.get(keyName).toString());
				break;
			}
		}
	}

	public void verifyAlertList(Map<String, String> filterOptions) {
		for (Map.Entry<String, String> entry : filterOptions.entrySet()) {
			if (entry.getValue().equalsIgnoreCase("All"))
				break;
			else {
				click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, entry.getKey()));
				click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, entry.getValue()));
				commonPage.clickButton("Apply");
			}
		}
		for (Map.Entry<String, String> entry : filterOptions.entrySet()) {
			if (entry.getValue().equalsIgnoreCase("All"))
				break;
			else {
				String filter = entry.getKey();
				String value = entry.getValue();
				List<WebElement> alerts = findElements(AlertsPageElements.ALERT_LIST.locate());
				if (alerts.size() > 0) {
					for (WebElement eachAlert : alerts) {
						String alertName = eachAlert.getAttribute("title");
						CustomAssertions.assertEquals(filter + "failed for alert" + alertName, value,
								segPage.getColumnValue(alertName, filter, value));
					}
				} else
					Log.error("No alerts found for given filter");
			}
		}
	}

}
