package com.webengage.ui_automation.pages;

import com.webengage.ui_automation.elements.*;
import com.webengage.ui_automation.utils.SeleniumExtendedUtility;

import org.openqa.selenium.Keys;

public class ShopifyPage extends SeleniumExtendedUtility {

    public void addToCart(String product) {
        click(dynamicXpathLocator(CommonPageElements.GENERIC_CLASS, "button button--secondary"));
        click(dynamicXpathLocator(CommonPageElements.ANCHOR_TAG_GENERIC_TEXT_CONTAINS, product));
        waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, "Buy it now"));
        click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, "Add to cart"));
        waitForElementToBeVisible(dynamicXpathLocator(ShopifyPageElements.QUANTITY_NUM, "1"));
        click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Cart"));
        click(dynamicXpathLocator(CommonPageElements.GENERIC_TYPE, "submit"));
    }

	public void enterCheckoutInformation(String email) {
		sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Email or mobile phone number"), email);
		sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "First name (optional)"), "TestUser");
		sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Last name"), "Automation");
		sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Address"), "Test Address" + Keys.ENTER);
		sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "City"), "Bangalore");
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Karnataka"));
		sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "PIN code"), "560076" + Keys.TAB);

	}

	public void shopifyPayment() {
		waitForElementToBeVisible(ShopifyPageElements.CARDNUMBER_PLACEHOLDER.locate());
		switchToiFrame(dynamicXpathLocator(JourneysPageElements.GENERIC_TITLE, "Field container for: Card number"));
		sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Card number"), "1");
		switchToDefaultContent();
		switchToiFrame(dynamicXpathLocator(JourneysPageElements.GENERIC_TITLE,
                "Field container for: Expiration date (MM / YY)"));
        sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Expiration date (MM / YY)"),
                "12" + Keys.TAB);
        sendKeys(dynamicXpathLocator(IntegrationsPageElements.FIELDS, "expiry"), "99" + Keys.TAB);
        switchToDefaultContent();
        switchToiFrame(dynamicXpathLocator(JourneysPageElements.GENERIC_TITLE, "Field container for: Security code"));
        sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Security code"), "123");
        switchToDefaultContent();
		switchToiFrame(dynamicXpathLocator(JourneysPageElements.GENERIC_TITLE, "Field container for: Name on card"));
		switchToDefaultContent();
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Pay now"));
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Continue shopping"));
	}
}
