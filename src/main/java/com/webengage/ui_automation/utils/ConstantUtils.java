package com.webengage.ui_automation.utils;

public class ConstantUtils {

	public static final String CAMPAIGNID = "CampaignId";
	public static final String SEGMENTID = "SegmentID";
	public static final String JSONBODY = "jsonBody";
	public static final String RESPONSE = "response";
	public static final String RUNTIMEVALUES = "runtimeValues";
	public static final String RUNTIMEURL = "runtimeURL";
	public static final String RUNTIMEBODY = "runtimeBody";
	public static final String RUNTIMEQUERYPARAMS = "runtimeQueryParams";
	public static final String ADDITIONALHEADERS = "additionalHeaders";
	public static final String CAMPAIGNNAME = "campaignName";
	public static final String CAMPAIGNTYPE = "campaignType";
	public static final String SYSTEM_DATETIME = "systemDateTime";
	public static final String IS_SYSTEM_DATETIME_AVAILABLE = "isSystemDateTimeAvailable";
	public static final String JOURNEY_IDS = "ids";
	public static final String JOURNEY_ID = "id";
	public static final String JOURNEY_PAYLOAD = "JourneyPayload";
	public static final String PRIMARY_NAMESPACE = "primary";
	public static final String SECONDARY_NAMESPACE = "secondary";
	public static final String REACHABLE_USER = "reachableuser";
	public static final String TEMPLATEID = "templateId";
	public static final String JSONTEMPLATE = "jsonTemplate";

	private ConstantUtils() {
	}
}
