package com.webengage.ui_automation.utils;

import java.io.*;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class SetupUtility {
	public static String authApiKey;
	public static String licenseCode;
	public static String shopifyStoreUrl;
	private String apiUri;
	public static final String directoryLoc = System.getProperty("user.dir") + "/src/test/resources/testData/";
	public static final String expectedPayloadsDirectory = directoryLoc + "expectedPayload/";
	public static final String runtimePayloadsDirectory = directoryLoc + "runtimeFiles/";
	public static final String runtimeCampaginStatsDirectory = directoryLoc + "campaignStats/";
	final String testDataexcel = runtimePayloadsDirectory + "testData.xlsx";
	final String apiFilePath = directoryLoc + "api.properties";
	final String credentials = directoryLoc + "AccountData/" + "credentials.json";
	public final String configFilePath = directoryLoc + "config.properties";

	Properties prop = new Properties();
	public static Properties configProp = new Properties();

	public static String getAuthApiKey() {
		return authApiKey;
	}

	public void setAuthApiKey(String authApiKey) throws IOException {
		SetupUtility.authApiKey = authApiKey;
	}
	
	public void setShopifyStoreUrl(String shopifyStoreUrl) throws IOException {
        SetupUtility.shopifyStoreUrl = shopifyStoreUrl;
    }
	
	public static String getShopifyStoreUrl() {
        return shopifyStoreUrl;
    }
	
	public String getLicenseCode() {
		return licenseCode;
	}

	public void setLicenseCode(String licenseCode) throws IOException {
		SetupUtility.licenseCode = licenseCode;
	}

	public String getApiURI() {
		return apiUri;
	}

	public void setApiURI(String apiName) throws IOException {
		FileInputStream apiFile = new FileInputStream(apiFilePath);
		prop.load(apiFile);
		this.apiUri = (prop.getProperty(apiName)).replace("{license}", getLicenseCode());
	}

	public String getBaseURI() throws IOException {
		FileInputStream apiFile = new FileInputStream(apiFilePath);
		prop.load(apiFile);
		switch (System.getProperty("set.Environment")) {
		case "Prod US":
			return prop.getProperty("prodUS");
		case "Prod IN":
			return prop.getProperty("prodIN");
		case "Prod UNL":
			return prop.getProperty("prodUNL");
		case "Prod KSA":
			return prop.getProperty("prodKSA");
		case "Prod Beta":
			return prop.getProperty("prodBeta");
		case "QA":
			return prop.getProperty("QA");
		case "Staging":
			String staging = prop.getProperty("Staging");
			String namespace = System.getProperty("set.Namespace");
			return staging.replace("{namespace}", namespace);
		case "Staging Dev":
			return prop.getProperty("StagingDev");
		default:
			break;
		}
		return null;
	}

	public void loadCredentials(String projectName) throws FileNotFoundException, IOException, ParseException {
	    for (Map.Entry<String, String> indivEntry : readCredFile(projectName).entrySet()) {
			if (indivEntry.getKey().equals("authorizationKey"))
				setAuthApiKey(indivEntry.getValue());
			else
				setLicenseCode(indivEntry.getValue());
		}
	}
	
	public void loadStoreUrl(String projectName) throws FileNotFoundException, IOException, ParseException {
        for (Map.Entry<String, String> indivEntry : readCredFile(projectName).entrySet()) {
            if (indivEntry.getKey().equals("storeUrl"))
                setShopifyStoreUrl(indivEntry.getValue());
        }
    }
	
	private Map<String, String> readCredFile(String projectName) throws FileNotFoundException, IOException, ParseException {
        Object credObj = new JSONParser().parse(new FileReader(credentials));
        JSONObject jsonCredObj = (JSONObject) credObj;
        JSONObject envSpecific = (JSONObject) jsonCredObj.get(System.getProperty("set.Environment"));
        Map<String, String> projectCredentials = ((Map<String, String>) envSpecific.get(projectName));
        return projectCredentials ;
    }

	@SuppressWarnings("resource")
	public XSSFSheet readFromExcelSheet(String sheetName) throws IOException {
		FileInputStream fis = new FileInputStream(testDataexcel);
		XSSFWorkbook workbook = new XSSFWorkbook(fis);
		XSSFSheet sheet = workbook.getSheet(sheetName);
		return sheet;
	}

	public Object methodInvocation(HashMap<String, HashMap<String, String>> data, String className, Object obj)
			throws Exception {
		HashMap<String, String> valueData = data.get(className);
		for (Map.Entry<String, String> me : valueData.entrySet()) {
			String methodName = me.getKey().toString().split("-")[0];
			String parameterDataType = me.getKey().toString().split("-")[1];
			String value = me.getValue().toString();
			Method methodInstance = null;
			switch (parameterDataType) {
			case "String":
				methodInstance = obj.getClass().getMethod(methodName, String.class);
				methodInstance.invoke(obj, value);
				break;
			case "Double":
				methodInstance = obj.getClass().getMethod(methodName, Double.class);
				methodInstance.invoke(obj, Double.parseDouble(value));
				break;
			case "Boolean":
				methodInstance = obj.getClass().getMethod(methodName, Boolean.class);
				methodInstance.invoke(obj, Boolean.parseBoolean(value));
				break;
			case "Integer":
				methodInstance = obj.getClass().getMethod(methodName, Integer.class);
				methodInstance.invoke(obj, (int) Double.parseDouble(value));
				break;
			default:
				break;
			}
		}
		return obj;
	}

	public void loadConfigProp() throws IOException {
		configProp.load(Files.newInputStream(Paths.get(configFilePath)));
	}
}
