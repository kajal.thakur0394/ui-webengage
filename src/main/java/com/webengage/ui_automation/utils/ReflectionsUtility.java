package com.webengage.ui_automation.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;

import com.webengage.ui_automation.initialize.RemoteConnection;
import com.webengage.ui_automation_dao.DatabaseTransaction;

public class ReflectionsUtility {
	Class<?> actionClass;
	JSONObject jsonTemplate;

	public ReflectionsUtility() {
		try {
			actionClass = Class.forName("com.webengage.ui_automation.utils.SeleniumExtendedUtility");
		} catch (ClassNotFoundException e) {
			Log.warn("Exception - " + e);
		}
	}

	private void setJSONTemplate(JSONObject jsonTemplate) {
		this.jsonTemplate = jsonTemplate;
	}

	public JSONObject getJSONTemplate() {
		return jsonTemplate;
	}

	/**
	 * Read the template file, extract and return it as JSONObject
	 * 
	 * @param fileName     Name of the template file
	 * @param templateType Specify the folder name (folders are specific to Campaign
	 *                     Type)
	 * @return JSONObject that contains all the details related to the
	 *         Journey/Campaigns/Personalisation
	 * @since V1.0.0
	 * @version 1.5.3
	 * @throws UnsupportedEncodingException
	 */
	public JSONObject fetchTemplate(String scenarioName, String fileLocation, String fileName) {
		String templateData = null;
		JSONObject jsonObj = null;
		templateData = DatabaseTransaction.getInstance().fetchTemplateFromDb(scenarioName, fileName);
		if (templateData != null) {
			try {
				JSONParser parser = new JSONParser();
				jsonObj = (JSONObject) parser.parse(templateData);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		} else {
			String template = SetupUtility.directoryLoc + fileLocation + "/" + fileName + ".json";
			Log.info("Using the template file [" + fileName + "] from location " + fileLocation);
			try {
				jsonObj = parseToJSONObject(
						new BufferedReader(new InputStreamReader(new FileInputStream(template), "UTF-8")));
			} catch (FileNotFoundException e) {
				Log.warn("File not found in local diretory, searching through connected SFTP server directory.");
				try {
					jsonObj = parseToJSONObject(RemoteConnection.readFilefromRemote(fileLocation, fileName + ".json"));
				} catch (FileNotFoundException e1) {
					Log.error("File not present in either of these locations." + fileLocation + "/" + fileName);
				}
			} catch (UnsupportedEncodingException e) {
				Log.error("Encoding is not supported");
				e.printStackTrace();
			}
		}
		setJSONTemplate(jsonObj);
		return jsonObj;
	}

	/**
	 * Read and parse any JSON file to JSONObject
	 * 
	 * @param file File that needs to be parsed into JSONObject
	 * @return JSONObject of file that is read
	 * @since V1.5.3
	 * @version 1.5.3
	 */
	public JSONObject parseToJSONObject(Reader file) {
		Object templateObj = null;
		try {
			templateObj = new JSONParser().parse(file);
		} catch (Exception e) {
			Log.warn("Exception - " + e);
		}
		JSONObject jsonTemplateObj = (JSONObject) templateObj;
		return jsonTemplateObj;
	}

	/**
	 * Explicitly invoke methods not having any arguments
	 * 
	 * @param methodName
	 */
	private void invokeNoArgMethod(String methodName) {
		try {
			Method actionMethod = actionClass.getMethod(methodName, null);
			actionMethod.invoke(actionClass.getDeclaredConstructor().newInstance());
		} catch (NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| InstantiationException e) {
			Log.warn(e.toString());
		}
	}

	/**
	 * Parse JSONObject to Map
	 * 
	 * @param object JSONObject
	 * @return Map
	 * @since V1.4.4
	 * @version 1.4.4
	 */
	public Map<String, Object> jsonObjectToMap(JSONObject object) {
		Map<String, Object> objectMap = new HashMap<String, Object>();
		object.keySet().forEach(s -> {
			objectMap.put((String) s, object.get(s));
		});
		return objectMap;
	}

	/**
	 * Filter and build a logical Map having locator and action type
	 * 
	 * @param tempMap parsed JSON Map
	 * @return Map logical classification for performing further action
	 * @since V1.4.4
	 * @version 1.4.4
	 */

	private Map<String, String> executeJSONObject(Map<String, Object> tempMap) {
		Map<String, String> objectMap = new HashMap<String, String>();
		String[] arr = null;
		if (tempMap.containsKey("Class")) {
			String classKey = (String) tempMap.get("Class");
			arr = classKey.split("\\.");
			objectMap.put("Class", arr[0]);
			objectMap.put("Method", arr[1]);
			tempMap.remove("Class");
		} else {
			String key = (String) tempMap.get("key");
			arr = key.split("\\.");
			objectMap.put("ENUMClass", arr[0]);
			objectMap.put("ENUM", arr[arr.length - 1]);
			if (key.contains("dynamic"))
				objectMap.put("isDynamic", arr[1]);
			tempMap.remove("key");
		}
		return objectMap;
	}

	/**
	 * Parse and process individual JSONObjects from JSONArray
	 * 
	 * @param template JSONArray
	 * @throws ClassNotFoundException
	 * @since V1.4.4
	 * @version 1.4.4
	 */
	public void readAndFillData(JSONArray template) {
		Iterator<?> objectIterator = template.iterator();
		while (objectIterator.hasNext()) {
			JSONObject object = (JSONObject) objectIterator.next();
			Map<String, Object> tempMap = jsonObjectToMap(object);
			Map<String, String> actualMap;
			try {
				actualMap = executeJSONObject(tempMap);
			} catch (NullPointerException e) {
				Log.info("Invoking method not having any arguments");
				invokeNoArgMethod((String) tempMap.get("action"));
				continue;
			}
			try {
				tempMap.entrySet().forEach(entry -> actualMap.put(entry.getKey(), (String) entry.getValue()));
				performAction(actualMap);
			} catch (ClassCastException | ClassNotFoundException e) {
				Log.info("Working upon the nested JSON required for iFrame");
				try {
					performAction(actualMap);
				} catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				}
				readAndFillData((JSONArray) object.get("iframe"));
				new SeleniumExtendedUtility().switchToDefaultContent();
			}
		}
	}

	public String updateStringWithJson(JSONObject template, String text) throws ClassNotFoundException {
		Map<String, Object> map = jsonObjectToMap(template);
		for (String key : map.keySet()) {
			text = text.replaceAll(key, (String) map.get(key));
		}
		return text;
	}

	/**
	 * Select class at runtime to make instances of them
	 * 
	 * @param Map containing all the data related to action to be performed
	 * @throws ClassNotFoundException
	 * @since V1.4.4
	 * @version 1.4.4
	 */
	@SuppressWarnings("unused")
	private void performAction(Map<String, String> map) throws ClassNotFoundException {

		if (map.get("Class") != null) {
			String className = map.get("Class");
			String methName = map.get("Method");
			Class<?> methodClass = Class.forName("com.webengage.ui_automation.pages." + className);
			try {
				Method runtimeDynamicMethod = methodClass.getMethod(methName, HashMap.class);
				Object runtimeLocator = runtimeDynamicMethod.invoke(methodClass.getDeclaredConstructor().newInstance(), map);
			} catch (Exception e) {
				CustomAssertions.assertTrue("Failure while invoking method->" + map, false);
			}
			return;
		}
		boolean isDynamic = false;
		boolean toReplaceValueExist = false;
		String enumClassName = map.get("ENUMClass");
		String locatorName = map.get("ENUM");
		if (locatorName.contains("##")) {
			String[] arr = locatorName.split("##");
			locatorName = arr[1];
			toReplaceValueExist = true;
		}
		String actionName = map.get("action");
		Class<?> locatorClass = enumClassName.contains("NotificationToastMessages")
				? Class.forName("com.webengage.ui_automation.utils." + enumClassName)
				: Class.forName("com.webengage.ui_automation.elements." + enumClassName);

		isDynamic = map.containsKey("isDynamic") ? true : false;

		if (isDynamic) {
			try {
				String fieldValue = (String) locatorClass.getField(locatorName).get(null);
				String methodName = locatorClass.toString().contains("NotificationToastMessages")
						? "dynamicStringReplace"
						: "dynamicXpathLocator";
				if (toReplaceValueExist) {
					try {
						Method runtimeDynamicMethod = actionClass.getMethod(methodName, String.class, String.class,
								String.class);
						Object runtimeLocator = runtimeDynamicMethod.invoke(actionClass.getDeclaredConstructor().newInstance(), fieldValue,
								map.get("valueToReplace1"), map.get("valueToReplace2"));
						selectActionMethod(null, actionName, map, null, runtimeLocator);
					} catch (InvocationTargetException e) {
						Log.warn(e.toString());
						e.printStackTrace();
					}
				} else {
					Method runtimeDynamicMethod = actionClass.getMethod(methodName, String.class, String.class);
					Object runtimeLocator = runtimeDynamicMethod.invoke(actionClass.getDeclaredConstructor().newInstance(), fieldValue,
							map.get("valueToReplace"));
					selectActionMethod(null, actionName, map, null, runtimeLocator);
				}

			} catch (Exception e) {
				Log.warn(e.toString());
				e.printStackTrace();
			}
		} else {
			for (Object enumObj : locatorClass.getEnumConstants()) {
				if (locatorName.equals(enumObj.toString())) {
					try {
						selectActionMethod(locatorClass, actionName, map, enumObj, null);
					} catch (Exception e) {
						Log.warn(e.toString());
						e.printStackTrace();
					}
					break;
				}
			}
		}
	}

	/**
	 * Invoke methods based on the values defined in template.
	 * 
	 * @param locatorClass   Class instance
	 * @param actionName     String of the selenium method that needs to be invoked
	 * @param map            Map having additional values required for certain
	 *                       actions
	 * @param enumObj        Object of the Enum that needs to be used as a locator
	 * @param runtimeLocator Object that returns locator which are built at runtime
	 * @since V1.4.4
	 * @version 1.4.4
	 */
	private void selectActionMethod(Class<?> locatorClass, String actionName, Map<String, String> map, Object enumObj,
			Object runtimeLocator) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException,
			InstantiationException, NoSuchMethodException, SecurityException {
		Method locatorMethod;
		Object locator;
		try {
			locatorMethod = locatorClass.getMethod("locate", null);
			locator = locatorMethod.invoke(enumObj, null);
		} catch (NullPointerException e) {
			Log.info("Using runtime locators");
			locator = runtimeLocator;
		}
		switch (actionName) {
		case "sendKeys":
		case "sendKeysViaJavaScript":
		case "clearFieldAndSendkeys": {
			Method actionMethod = actionClass.getMethod(actionName, By.class, CharSequence.class);
			actionMethod.invoke(actionClass.getDeclaredConstructor().newInstance(), locator, map.get("value"));
			break;
		}
		case "click":
		case "scrollIntoView":
		case "clearTextField":
		case "switchToiFrame":
		case "waitForElementToBeVisible":
		case "waitForElementToBeClickable":
		case "waitForElementToBeInvisible":{
			Method actionMethod = actionClass.getMethod(actionName, By.class);
			actionMethod.invoke(actionClass.getDeclaredConstructor().newInstance(), locator);
			break;
		}
		case "verifyPersonalisationToastMessage": {
			try {
				locatorMethod = locatorClass.getMethod("message", null);

			} catch (NullPointerException e) {
				Log.warn(e.toString());
			} finally {
				Method actionMethod = actionClass.getMethod(actionName, String.class);
				actionMethod.invoke(actionClass.getDeclaredConstructor().newInstance(), locator);
			}
			break;
		}
		case "uploadFile": {
			Method actionMethod = actionClass.getMethod(actionName, String.class, By.class);
			actionMethod.invoke(actionClass.getDeclaredConstructor().newInstance(), map.get("value"), locator);
			break;
		}
		}
	}

	public void writeExpectedintoFile() {
		JSONObject expected = updateTemplateContent((JSONObject) getJSONTemplate().get("Expected"));
		File directory = new File(SetupUtility.expectedPayloadsDirectory);
		if (!directory.exists())
			directory.mkdir();
		FileWriter file = null;
		try {
			file = new FileWriter(SetupUtility.expectedPayloadsDirectory + expected.get("Name") + ".json");
			file.write(expected.toJSONString());
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			file.flush();
			file.close();
		} catch (IOException e) {
			Log.warn("Exception" + e.toString());
		}

	}

	public JSONObject updateTemplateContent(JSONObject expected) {
		if (expected.containsKey("User")) {
			String expectedStr = expected.toJSONString();
			JSONObject usrDataTemp = fetchTemplate(null, "AccountData", "UsersData");
			JSONObject userData = (JSONObject) usrDataTemp.get(expected.get("User"));

			try {
				expectedStr = updateStringWithJson(userData, expectedStr);
			} catch (ClassNotFoundException exc) {
				exc.printStackTrace();
			}
			JSONParser parser = new JSONParser();
			try {
				expected = (JSONObject) parser.parse(expectedStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return expected;
	}

}
