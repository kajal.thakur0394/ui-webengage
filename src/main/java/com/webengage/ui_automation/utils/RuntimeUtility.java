package com.webengage.ui_automation.utils;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map.Entry;

import com.webengage.ui_automation.driver.DataFactory;

public class RuntimeUtility {

	public static void initializeVariables() {
		setRuntimeBody(new LinkedHashMap<String, String>());
		setRuntimeQueryParams(new LinkedHashMap<String, String>());
		setRuntimeURL(new LinkedHashMap<String, String>());
	}

	@SuppressWarnings("unchecked")
	public static LinkedHashMap<String, String> getRuntimeURL() {
		return DataFactory.getInstance().getData(ConstantUtils.RUNTIMEURL, LinkedHashMap.class);
	}

	public static void setRuntimeURL(LinkedHashMap<String, String> runtimeURL) {
		DataFactory.getInstance().setData(ConstantUtils.RUNTIMEURL, runtimeURL);
	}

	@SuppressWarnings("unchecked")
	public static LinkedHashMap<String, String> getRuntimeBody() {
		return DataFactory.getInstance().getData(ConstantUtils.RUNTIMEBODY, LinkedHashMap.class);
	}

	public static void setRuntimeBody(LinkedHashMap<String, String> runtimeBody) {
		DataFactory.getInstance().setData(ConstantUtils.RUNTIMEBODY, runtimeBody);
	}

	@SuppressWarnings("unchecked")
	public static LinkedHashMap<String, String> getRuntimeQueryParams() {
		return DataFactory.getInstance().getData(ConstantUtils.RUNTIMEQUERYPARAMS, LinkedHashMap.class);
	}

	public static void setRuntimeQueryParams(LinkedHashMap<String, String> runtimeQueryParams) {
		DataFactory.getInstance().setData(ConstantUtils.RUNTIMEQUERYPARAMS, runtimeQueryParams);
	}

	public void setDynamicRequest(String runtimeKey, String type) {
		LinkedList<String> runtimeList = new LinkedList<>();
		String value = APIUtility.getRuntimeValues().get(runtimeKey);
		runtimeList.add("{" + runtimeKey + "}");
		runtimeList.add(value);
		switch (type) {
		case "URL":
			getRuntimeURL().put("{" + runtimeKey + "}", value);
			break;
		case "Body":
			getRuntimeBody().put(runtimeKey, value);
			break;
		case "Query":
			getRuntimeQueryParams().put("{" + runtimeKey + "}", value);
			break;
		default:
			break;
		}
	}

	public void setDynamicRequest(String replacementIdentifier, String runtimeKey, String type) {
		LinkedList<String> runtimeList = new LinkedList<>();
		String value = APIUtility.getRuntimeValues().get(runtimeKey);
		runtimeList.add("{" + replacementIdentifier + "}");
		runtimeList.add(value);
		switch (type) {
		case "URL":
			getRuntimeURL().put("{" + replacementIdentifier + "}", value);
			break;
		case "Body":
			getRuntimeBody().put("{" + replacementIdentifier + "}", value);
			break;
		case "Query":
			getRuntimeQueryParams().put("{" + runtimeKey + "}", value);
			break;
		default:
			break;
		}
	}

	public LinkedHashMap<String, String> getDynamicURL() {
		return getRuntimeURL();
	}

	public LinkedHashMap<String, String> getDynamicBody() {
		return getRuntimeBody();
	}

	public LinkedHashMap<String, String> getDynamicQueryParams() {
		return getRuntimeQueryParams();
	}

	public void setDynamicRequestLicenseCode(String type, String value) {
		LinkedList<String> runtimeList = new LinkedList<>();
		runtimeList.add("{licenseCode}");
		runtimeList.add(value);
		switch (type) {
		case "URL":
			getRuntimeURL().put("{licenseCode}", value);
			break;
		case "Body":
			getRuntimeBody().put("{licenseCode}", value);
			break;
		default:
			break;
		}

	}

	public String modifyAPIURL(String apiURI) {
		if (getDynamicURL() != null) {
			for (Entry<String, String> element : getDynamicURL().entrySet()) {
				String replaceKeyword = element.getKey();
				String value = element.getValue();
				try {
					apiURI = apiURI.replace(replaceKeyword, value);
				} catch (NullPointerException e) {
					Log.info("The key " + replaceKeyword + " does not have any value");
				}
			}
			return apiURI;
		} else {
			return apiURI;
		}
	}

	public String modifyBody(String jsonBody) {
		if (getDynamicBody() != null) {
			for (Entry<String, String> element : getDynamicBody().entrySet()) {
				String replaceKeyword = element.getKey();
				String value = element.getValue();
				try {
					jsonBody = jsonBody.replace(replaceKeyword, value);
				} catch (Exception e) {
					Log.info("There is no replacement value present for " + replaceKeyword);
				}

			}
			return jsonBody;
		} else {
			return jsonBody;
		}
	}

	public String modifyQueryParams(String queryParamsString) {
		if (getDynamicQueryParams() != null) {
			for (Entry<String, String> element : getDynamicQueryParams().entrySet()) {
				String replaceKeyword = element.getKey();
				String value = element.getValue();
				try {
					queryParamsString = queryParamsString.replace(replaceKeyword, value);
				} catch (NullPointerException e) {
					Log.info("The key " + replaceKeyword + " does not have any value");
				}
			}
			return queryParamsString;
		} else {
			return queryParamsString;
		}
	}

}