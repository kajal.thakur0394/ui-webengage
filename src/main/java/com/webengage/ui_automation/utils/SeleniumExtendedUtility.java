package com.webengage.ui_automation.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.webengage.ui_automation.driver.BrowserManager;
import com.webengage.ui_automation.driver.DriverFactory;
import com.webengage.ui_automation.elements.CommonPageElements;
import java.time.Duration;

public class SeleniumExtendedUtility {
	public WebDriver driver;
	public WebDriverWait wait;
	JavascriptExecutor js;
	protected Actions actions;

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public SeleniumExtendedUtility() {
		if (Objects.isNull(DriverFactory.getInstance().getDriver())) {
			new BrowserManager();
		}
		setDriver(DriverFactory.getInstance().getDriver());
		wait = new WebDriverWait(driver, Duration.ofSeconds(45));
		js = (JavascriptExecutor) driver;
		actions = new Actions(driver);
	}

	public void click(By by) {
		wait.until(ExpectedConditions.elementToBeClickable(by));
		try {
			driver.findElement(by).click();
		} catch (Exception ElementClickInterceptedException) {
			WebElement element = driver.findElement(by);
			js.executeScript("arguments[0].click();", element);
		}
	}

	public void openDropdown(By by) {
		wait.until(ExpectedConditions.elementToBeClickable(by));
		wait.until(
				ExpectedConditions.attributeContains(CommonPageElements.DROPDOWN.locate(), "aria-disabled", "false"));
		try {
			driver.findElement(by).click();
		} catch (Exception ElementClickInterceptedException) {
			WebElement element = driver.findElement(by);
			js.executeScript("arguments[0].click();", element);
		}
	}

	public void sendKeys(By by, CharSequence text) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		driver.findElement(by).sendKeys(text);
	}

	public void sendKeysViaJavaScript(By by, CharSequence text) {
		WebElement element = driver.findElement(by);
		String tagName = element.getTagName();
		switch (tagName) {
		case "input":
			js.executeScript(
					"var elm = arguments[0], txt = arguments[1];console.log(elm);console.log(elm.value); elm.setAttribute('value', txt);console.log(elm.value);console.log(elm);elm.dispatchEvent(new Event('keydown', {bubbles: true}));elm.dispatchEvent(new Event('keypress', {bubbles: true})); elm.dispatchEvent(new Event('input', {bubbles: true}));elm.dispatchEvent(new Event('keyup', {bubbles: true}));",
					element, text);
			break;
		case "textarea":
			js.executeScript(
					"var elm = arguments[0], txt = arguments[1];console.log(elm);console.log(elm.value); elm.innerHTML=txt;console.log(elm);elm.dispatchEvent(new Event('keydown', {bubbles: true}));elm.dispatchEvent(new Event('keypress', {bubbles: true})); elm.dispatchEvent(new Event('input', {bubbles: true}));elm.dispatchEvent(new Event('keyup', {bubbles: true}));",
					element, text);
			break;
		case "body":
			js.executeScript(
					"var elm = arguments[0], txt = arguments[1];console.log(elm);console.log(elm.value); elm.innerText=txt;console.log(elm);elm.dispatchEvent(new Event('keydown', {bubbles: true}));elm.dispatchEvent(new Event('keypress', {bubbles: true})); elm.dispatchEvent(new Event('input', {bubbles: true}));elm.dispatchEvent(new Event('keyup', {bubbles: true}));",
					element, text);
			break;
		}
	}

	public void saveButton() {
		wait.until(ExpectedConditions.elementToBeClickable(CommonPageElements.SAVE_BUTTON.locate()));
		driver.findElement(CommonPageElements.SAVE_BUTTON.locate()).click();
	}

	public void saveAndContinueButton() {
		By by = CommonPageElements.SAVE_AND_CONTINUE_BUTTON.locate();
		wait.until(ExpectedConditions.elementToBeClickable(by));
		try {
			driver.findElement(by).click();
		} catch (Exception ElementClickInterceptedException) {
			WebElement element = driver.findElement(by);
			js.executeScript("arguments[0].click();", element);
		}
	}

	public void saveAndCloseButton() {
		wait.until(ExpectedConditions.elementToBeClickable(CommonPageElements.SAVE_AND_CLOSE_BUTTON.locate()));
		driver.findElement(CommonPageElements.SAVE_AND_CLOSE_BUTTON.locate()).click();

	}

	public void skipTestButton() {
		wait.until(ExpectedConditions.elementToBeClickable(CommonPageElements.SKIP_TEST_BUTTON.locate()));
		driver.findElement(CommonPageElements.SKIP_TEST_BUTTON.locate()).click();
	}

	public By dynamicXpathLocator(String element, String value) {
		element = element.replace("%s", value);
		return By.xpath(element);
	}

	public String dynamicStringReplace(String element, String value) {
		element = element.replace("%s", value);
		return element;
	}

	public By dynamicXpathLocator(String element, String value1, String value2) {
		element = element.replace("%s1", value1);
		element = element.replace("%s2", value2);
		return By.xpath(element);
	}

	public String fetchText(By by) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		return driver.findElement(by).getText();
	}

	public void waitForElementToBeVisible(By by) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
	}

	public void waitForElementToBeInvisible(By by) {
		wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
	}

	public void waitForSpinner() {
		List<WebElement> loaders = driver.findElements(CommonPageElements.LOADER.locate());
		wait.until(ExpectedConditions.invisibilityOfAllElements(loaders));
	}

	public void waitForSaveButtonEnabled() {
		WebElement saveBtn = driver.findElement(CommonPageElements.SAVE_BUTTON.locate());
		wait.until(ExpectedConditions.attributeContains(saveBtn, "class", "secondary"));
	}

	public void waitForElementToBePresent(By by) {
		wait.until(ExpectedConditions.presenceOfElementLocated(by));
	}

	public void waitForElementToBeClickable(By by) {
		wait.until(ExpectedConditions.elementToBeClickable(by));
	}

	public void verifyToastMessage(String value) {
		wait.until(ExpectedConditions.presenceOfElementLocated(CommonPageElements.TOAST.locate()));
		if (fetchText(CommonPageElements.TOAST.locate()).contains("Please try again")) {
			clickUsingJSExecutor(CommonPageElements.CLOSE_TOAST.locate());
		}
		CustomAssertions.assertEquals("Incorrect toast message:", value, fetchText(CommonPageElements.TOAST.locate()));
		clickUsingJSExecutor(CommonPageElements.CLOSE_TOAST.locate());
		wait.until(ExpectedConditions.invisibilityOfElementLocated(CommonPageElements.TOAST.locate()));
	}

	public void verifyPersonalisationToastMessage(String value) {
		wait.until(ExpectedConditions.presenceOfElementLocated(CommonPageElements.PERSONALIZATION_TOAST.locate()));
		CustomAssertions.assertEquals("Incorrect toast message:", value,
				fetchText(CommonPageElements.PERSONALIZATION_TOAST.locate()));
	}

	public void clickUsingJSExecutor(By by) {
		wait.until(ExpectedConditions.presenceOfElementLocated(by));
		WebElement loc = driver.findElement(by);
		js.executeScript("arguments[0].click();", loc);
	}

	public void scrollIntoView(By by) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		WebElement element = driver.findElement(by);
		js.executeScript("arguments[0].scrollIntoView();", element);
	}

	public String fetchAttributeValue(By by, String attr) {
		wait.until(ExpectedConditions.presenceOfElementLocated(by));
		return driver.findElement(by).getAttribute(attr);
	}

	public void clearTextField(By by) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		driver.findElement(by).sendKeys(Keys.chord(Keys.CONTROL, "a"));
		driver.findElement(by).sendKeys(Keys.chord(Keys.COMMAND, "a"));
		driver.findElement(by).sendKeys(Keys.BACK_SPACE);
		wait.until(ExpectedConditions.attributeToBe(by, "value", ""));
	}

	public void waitUntilAttributeContains(By by, String attribute, String value) {
		wait.until(ExpectedConditions.attributeContains(by, attribute, value));
	}

	public void waitUntilAttributeDoesNotContain(By by, String attribute, String value) {
		wait.until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				if (!driver.findElement(by).getAttribute(attribute).contains(value))
					return true;
				else
					return false;
			}
		});
	}

	public String getCurrentURL() {
		return driver.getCurrentUrl();
	}

	public void refresh() {
		driver.navigate().refresh();
	}

	public void openURL(String url) {
		driver.get(url);
	}

	public void acceptAlert() {
		Alert alert = driver.switchTo().alert();
		alert.accept();
	}

	public void dismissAlert() {
		Alert alert = driver.switchTo().alert();
		alert.dismiss();
	}

	public WebElement findElement(By by) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		WebElement element = driver.findElement(by);
		return element;
	}

	public List<WebElement> findElements(By by) {
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(by));
			List<WebElement> elements = driver.findElements(by);
			return elements;
		} catch (Exception e) {
			List<WebElement> elements = driver.findElements(by);
			return elements;
		}
	}

	public void waitForTextToBe(By by, String text) {
		wait.until(ExpectedConditions.textToBe(by, text));
	}

	public void switchToiFrame(By by) {
		WebElement element = driver.findElement(by);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
		driver.switchTo().frame(element);

	}

	public void moveToElement(By from, By to) {
		wait.until(ExpectedConditions.presenceOfElementLocated(from));
		actions.moveToElement(driver.findElement(from));
		actions.clickAndHold();
		wait.until(ExpectedConditions.presenceOfElementLocated(from));
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		actions.moveToElement(driver.findElement(to));
		actions.release().perform();
	}

	public void dragAndDropElement(By from, By to) {
		wait.until(ExpectedConditions.presenceOfElementLocated(from));
		wait.until(ExpectedConditions.presenceOfElementLocated(to));
		actions.dragAndDrop(driver.findElement(from), driver.findElement(to)).build().perform();
	}

	public void switchToDefaultContent() {
		driver.switchTo().defaultContent();
	}

	public boolean checkIfElementisLocated(By by) {
		return driver.findElements(by).size() > 0;
	}

	public boolean isSelected(By by) {
		return driver.findElement(by).isSelected();
	}

	public void mouseOverElement(By by) {
		wait.until(ExpectedConditions.presenceOfElementLocated(by));
		actions.moveToElement(driver.findElement(by)).build().perform();
	}

	public void mouseOverElement(WebElement element) {
		wait.until(ExpectedConditions.visibilityOf(element));
		actions.moveToElement(element).build().perform();
	}

	public void clearFieldAndSendkeys(By by, CharSequence text) {
		clearTextField(by);
		sendKeys(by, text);
	}

	public Map<String, Integer> getDimensions(By by) {
		waitForElementToBePresent(by);
		Map<String, Integer> map = new HashMap<String, Integer>();
		WebElement element = driver.findElement(by);
		map.put("width", element.getSize().getWidth());
		map.put("height", element.getSize().getHeight());
		return map;
	}

	public void uploadFile(String fileName, By by) {
		String filePath = SetupUtility.directoryLoc + "AccountData/" + fileName;
		filePath = filePath.replace("/", File.separator);
		waitForElementToBePresent(by);
		driver.findElement(by).sendKeys(filePath);
	}

	public void switchToParentWindow(String parentWindow) {
		driver.close();
		driver.switchTo().window(parentWindow);
	}

	public void switchToChildWindow() {
		String parentWindow = driver.getWindowHandle();
		ArrayList<String> childWindows = new ArrayList<String>(driver.getWindowHandles());
		for (String windowHandle : childWindows) {
			if (!windowHandle.equals(parentWindow)) {
				driver.switchTo().window(windowHandle);
			}
		}
	}

	public void switchToDefaultWindow() {
		ArrayList<String> w = new ArrayList<String>(driver.getWindowHandles());
		driver.close();
		// switch to first tab
		driver.switchTo().window(w.get(0));
		Log.info("Parent tab title: " + driver.getTitle());
		Log.info("Parent tab url: " + getCurrentURL());
	}

	public boolean verifyDownloadedFile(By by, String reportName) throws IOException {
		File dir = new File(BrowserManager.downloadDir.replace("/", File.separator));
		boolean fileDownloaded = false;
		int count = 0;
		try {
			TimeUnit.SECONDS.sleep(5);
			scrollIntoView(by);
			click(by);
			TimeUnit.SECONDS.sleep(10);
			Log.error("Number of files in the directory - " + dir.listFiles().length);
			for (File f : dir.listFiles()) {
				Log.error("File Name" + f.getName());
				if (f.exists()) {
					CustomAssertions.assertTrue("File name does not match", f.getName().contains(reportName));
					count++;
				}
			}
			if (count == 1) {
				fileDownloaded = true;
				FileUtils.cleanDirectory(dir);
			} else
				fileDownloaded = false;
		} catch (Exception e) {
			Log.error("Report download and verification failed");
			e.printStackTrace();
		} finally {
			FileUtils.cleanDirectory(dir);
		}
		return fileDownloaded;
	}

	public boolean verifyElementDisabled(By by) {
		return fetchAttributeValue(by, "class").contains("disabled");
	}

	public void deleteButton() {
		wait.until(ExpectedConditions.elementToBeClickable(CommonPageElements.DELETE_BUTTON.locate()));
		driver.findElement(CommonPageElements.DELETE_BUTTON.locate()).click();
	}

	public void selectDropDown(By dropDownElement, String value) {
		waitForElementToBeClickable(dropDownElement);
		Select select = new Select(driver.findElement(dropDownElement));
		waitForElementToBePresent(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, value));
		select.selectByVisibleText(value);
	}

	public int getSizeOfElements(By element) {
		waitForElementToBeVisible(element);
		return driver.findElements(element).size();
	}

	public void openURLinNewTab(String url) {
		((JavascriptExecutor) driver).executeScript("window.open('');");
		ArrayList<String> windowsList = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(windowsList.get(1));
		driver.get(url);
	}
	
	public By addIndexToLocator(By locator, String index) {
		return By.xpath("(" + locator.toString().replace("By.xpath: ", "") + ")[" + index + "]");
	}

}
