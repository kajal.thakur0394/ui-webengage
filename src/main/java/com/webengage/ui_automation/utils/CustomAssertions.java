package com.webengage.ui_automation.utils;

import org.junit.Assert;

public class CustomAssertions {

	private static void displayAndLogError(String message, AssertionError e) {
		String x = e.toString() + "--" + message;
		Log.error(x);
		Assert.fail(e.toString());
	}

	public static void assertEquals(String message, String expectedValue, String actualValue) {
		try {
			Assert.assertEquals(message, expectedValue, actualValue);
		} catch (AssertionError e) {
			displayAndLogError(message, e);
		}
	}

	public static void assertEquals(String message, int expectedValue, int actualValue) {
		try {
			Assert.assertEquals(message, expectedValue, actualValue);
		} catch (AssertionError e) {
			displayAndLogError(message, e);
		}
	}

	public static void assertEquals(String message, boolean expectedValue, boolean actualValue) {
		try {
			Assert.assertEquals(message, expectedValue, actualValue);
		} catch (AssertionError e) {
			displayAndLogError(message, e);
		}
	}

	public static void assertTrue(String message, boolean flag) {
		try {
			Assert.assertTrue(flag);
		} catch (AssertionError e) {
			displayAndLogError(message, e);
		}
	}

	public static void assertFalse(String message, boolean flag) {
		try {
			Assert.assertFalse(flag);
		} catch (AssertionError e) {
			displayAndLogError(message, e);
		}
	}

	public static void assertEquals(String message, Double expectedValue, Double actualValue) {
		try {
			Assert.assertEquals(message, expectedValue, actualValue);
		} catch (AssertionError e) {
			displayAndLogError(message, e);
		}
	}

	public static void assertEquals(String message, Object expected, Object actual) {
		try {
			Assert.assertEquals(expected, actual);
		} catch (AssertionError e) {
			displayAndLogError(message, e);
		}
	}

	public static void assertNotNull(String message, Object actual){
		try{
			Assert.assertNotNull(actual);
		}
		catch (AssertionError e) {
			displayAndLogError(message, e);
		}
	}
}
