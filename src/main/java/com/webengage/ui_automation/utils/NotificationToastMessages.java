package com.webengage.ui_automation.utils;

public enum NotificationToastMessages {
	WA_TEMPLATE("WhatsApp template has been saved successfully"),
	WA_TEMPLATE_DELETE("Template has been deleted successfully"),
	CREATE_CUSTOM_TEMPLATE("Template Created Successfully"),
	EDIT_CUSTOM_TEMPLATE("Template Updated Successfully"),
	DUPLICATE_CUSTOM_TEMPLATE("Template Duplicated Successfully"),
	CUSTOM_TEMPLATE_DELETE("Template Deleted Successfully"),
	DELETE_PROVIDER(" has been deleted successfully."),
	SEGMENT_UPDATED("Segment updated successfully."),
	SEGMENT_CREATED("Segment created successfully."),
	LIST_CREATED("List created successfully."),
	ALERT_CREATED("Alert has been created successfully"),
	FUNNEL_CREATED("Funnel has been created successfully ."),
	DELETE_CAMPAIGN("Campaign has been deleted successfully"),
	PIN_DASHBOARD("The card has been pinned successfully to the selected dashboard(s)"),
	DELETE_SEGMENT("Segment has been deleted successfully."),
	REFRESH_STATIC_SEGMENT("List refresh is in progress. Please check again in a few minutes."),
	DELETE_STATIC_SEGMENT("List has been deleted successfully."),
	DUPLICATE_SEGMENT("Segment has been duplicated successfully."),
	EVENTS_LIMIT_EXCEED("You have reached the limit of 10 events selection."),
	WSP_CHANGES("Changes to the WSP have been saved successfully"),
	WA_TEMPLATE_CHANGES("Changes to the template have been saved successfully"),
	SP_UPDATE(" has been updated successfully"),
	LIST_JOURNEY(
			"List has been saved successfully. Journey will get triggered for these users when journey is in Running state (if not already running)"),
	USER_CSV_JOURNEY(
			"CSV file has been uploaded successfully. Journey will get triggered for these users when journey is in Running state (if not already running)"),
	ADD__ESP("ESP has been added to your project successfully."),
	ADD__INVALIDESP("The ESP credentials entered are not valid. Please check and try again"),
	ADD_SSP("SSP has been added to your project successfully."),
	ADD_WSP("WSP has been added to your project successfully."),
	ACTIVATE_JOURNEY("Journey activated successfully"),
	ACTIVATE_RELAY("Relay activated successfully"),
	SAVE_ANDROID_CREDS("Android Package details has been saved successfully"),
	SAVE_IOS_CREDS("iOS credential has been saved successfully"),
	SUNSET_JOURNEY("Journey has been sunset successfully"),
	STOP_JOURNEY("Journey has been stopped successfully"),
	ANDROID_DELETE("Android Package details has been deleted successfully"),
	INVALID_ANDROID_CRED("Invalid FCM Credentials."),
	IOS_DELETE("iOS credentials has been deleted successfully"),
	OPTIN_DISABLED("Opt-in prompt will not be displayed on your website anymore"),
	OPTIN_ENABLED("Opt-in prompt will now be displayed on your website"),
	LAUNCH_CAMPAIGN("Campaign has been launched successfully"),
	TEST_CAMPAIGN("Request to send test campaign has been received successfully."),
	DELETE_TEST_SEGMENT("Test segment %s has been deleted"),
	WEBPUSH_CONFIG_UPDATE("Your Web Push settings have been updated successfully"),
	DELETE_FUNNEL("Funnel has been deleted successfully"),
	SUBSCRIBE_ALERT("You have Successfully subscribed to Alert."),
	UNSUBSCRIBE_ALERT("You have Successfully unsubscribed the Alert."),
	PAUSE_ALERT("The Alert is Successfully Paused."),
	RESUME_ALERT("The Alert is Successfully Resumed."),
	DELETE_ALERT("Alert successfully Deleted."),
	CARD_PINNED("The card has been pinned successfully to the selected dashboard(s)"),
	SUNSET_RELAY("Relay has been sunset successfully"),
	STOP_RELAY("Relay has been stopped successfully"),
	CATALOG_CREATION("Catalog file has been uploaded successfully and is now being processed."),
	CATALOG_MAPPING("Event mapping has been updated successfully.");

	String message;

	public static String SURVEY_ACTIVATION = "Survey \"%s\" activation information has been saved. Currently, this survey is active.";
	public static String ONSITE_CREATION = "Notification \"%s\" has been saved";
	public static String ONSITE_ACTIVATION = "Notification \"%s\" has been updated";

	NotificationToastMessages(String message) {
		this.message = message;
	}

	public String message() {
		return this.message;
	}

}