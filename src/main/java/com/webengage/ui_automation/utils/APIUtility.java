package com.webengage.ui_automation.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Assert;
import com.webengage.ui_automation.driver.DataFactory;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;

public class APIUtility {

	public static void initializeVariables() {
		setRuntimeValues(new HashMap<String, String>());
	}

	public static void setResponse(Response response) {
		DataFactory.getInstance().setData(ConstantUtils.RESPONSE, response);
	}

	public static Response getResponse() {
		return DataFactory.getInstance().getData(ConstantUtils.RESPONSE, Response.class);
	}

	public static String getJsonBody() {
		return DataFactory.getInstance().getData(ConstantUtils.JSONBODY, String.class);
	}

	public static void setJsonBody(String jsonBody) {
		DataFactory.getInstance().setData(ConstantUtils.JSONBODY, jsonBody);
	}

	@SuppressWarnings("unchecked")
	public static HashMap<String, String> getRuntimeValues() {
		return DataFactory.getInstance().getData(ConstantUtils.RUNTIMEVALUES, HashMap.class);
	}

	public static void setRuntimeValues(HashMap<String, String> runtimeValues) {
		DataFactory.getInstance().setData(ConstantUtils.RUNTIMEVALUES, runtimeValues);
	}

	@Step("API details")
	public void postRequest(String jsonBody, List<List<String>> queryParams, String apiURI) throws IOException {
		RestAssuredBuilder restAssur = new RestAssuredBuilder(apiURI, "POST");
		Response response = null;
		if (jsonBody.equals("-"))
			response = queryParams.get(0).get(0).equals("-") ? restAssur.execute()
					: restAssur.executeWithQueryParams(queryParams);
		else
			response = queryParams.get(0).get(0).equals("-") ? restAssur.executeWithBody(jsonBody)
					: restAssur.executeWithBodyAndQueryParams(jsonBody, queryParams);
		setResponse(response);
		if (apiURI.contains("survey.html")) {
			ArrayList<String> cookies = new ArrayList<>();
			cookies.add("cookie:" + response.getCookies().toString());
			RestAssuredBuilder.setAdditionalHeaders(cookies);
		}
		recordReportData();
		verifyStatusCode(response.getStatusCode());
	}

	@Step("API details")
	public void postRequest(String jsonBody, String apiURI) throws IOException {
		RestAssuredBuilder restAssur = new RestAssuredBuilder(apiURI, "POST");
		Response response = null;
		response = restAssur.executeWithBody(jsonBody);
		setResponse(response);
		recordReportData();
		verifyStatusCode(response.getStatusCode());
	}

	@Step("API details")
	public void postRequest(String apiURI) throws IOException {
		RestAssuredBuilder restAssur = new RestAssuredBuilder(apiURI, "POST");
		Response response = null;
		response = restAssur.executeWithBody(getJsonBody());
		setResponse(response);
		recordReportData();
		verifyStatusCode(response.getStatusCode());
	}

	@Step("API details")
	public void postRequestFormData(LinkedHashMap<String, String> formData, List<List<String>> queryParams,
			String apiURI) throws IOException {
		RestAssuredBuilder restAssur = new RestAssuredBuilder(apiURI, formData);
		Response response = null;
		response = queryParams.get(0).get(0).equals("-") ? restAssur.execute()
				: restAssur.executeWithQueryParams(queryParams);
		setResponse(response);
		recordReportData();
		verifyStatusCode(response.getStatusCode());
	}

	@Step("API details")
	public void getRequest(String apiURI, List<List<String>> queryParams) throws IOException {
		RestAssuredBuilder restAssur = new RestAssuredBuilder(apiURI, "GET");
		Response response = null;
		response = restAssur.executeWithQueryParams(queryParams);
		setResponse(response);
		recordReportData();
		verifyStatusCode(response.getStatusCode());
	}

	@Step("API details")
	public void getRequest(String jsonBody, List<List<String>> queryParams, String apiURI) throws IOException {
		RestAssuredBuilder restAssur = new RestAssuredBuilder(apiURI, "GET");
		Response response = null;
		if (jsonBody.equals("-"))
			response = restAssur.executeWithQueryParams(queryParams);
		else
			response = queryParams.get(0).get(0).equals("-") ? restAssur.executeWithBody(jsonBody)
					: restAssur.executeWithBodyAndQueryParams(jsonBody, queryParams);
		setResponse(response);
		recordReportData();
		verifyStatusCode(response.getStatusCode());
	}

	@Step("API details")
	public void getRequest(String apiURI) throws IOException {
		RestAssuredBuilder restAssur = new RestAssuredBuilder(apiURI, "GET");
		Response response = restAssur.execute();
		setResponse(response);
		recordReportData();
		verifyStatusCode(getResponse().getStatusCode());
	}

	@Step("API details")
	public void deleteRequest(String apiURI) throws IOException {
		RestAssuredBuilder restAssur = new RestAssuredBuilder(apiURI, "DELETE");
		Response response = restAssur.execute();
		setResponse(response);
		recordReportData();
		verifyStatusCode(response.getStatusCode());
	}

	@Step("API details")
	public void putRequest(String jsonBody, List<List<String>> queryParams, String apiURI) throws IOException {
		RestAssuredBuilder restAssur = new RestAssuredBuilder(apiURI, "PUT");
		Response response = null;
		if (jsonBody.equals("-")) {
			response = restAssur.executeWithQueryParams(queryParams);
		} else {
			response = queryParams.get(0).get(0).equals("-") ? restAssur.executeWithBody(jsonBody)
					: restAssur.executeWithBodyAndQueryParams(jsonBody, queryParams);
		}
		setResponse(response);
		recordReportData();
		verifyStatusCode(response.getStatusCode());
	}

	@Step("API details")
	public void putRequest(String apiURI) throws IOException {
		RestAssuredBuilder restAssur = new RestAssuredBuilder(apiURI, "PUT");
		Response response = restAssur.execute();
		setResponse(response);
		recordReportData();
		verifyStatusCode(response.getStatusCode());
	}

	public List<List<String>> generateQueryParams(String queryParamString) {
		List<List<String>> queryParam = new ArrayList<>();
		List<String> indiv;
		String[] indivParam = queryParamString.split("\\R");
		for (String temp : indivParam) {
			indiv = new ArrayList<>();
			for (String values : temp.split("~")) {
				indiv.add(values);
			}
			queryParam.add(indiv);
		}
		return queryParam;

	}

	private void recordReportData() {
		Serenity.recordReportData().withTitle("API Request")
				.andContents(DataFactory.getInstance().getData("logRequest", String.class));
		Serenity.recordReportData().withTitle("API Response")
				.andContents(DataFactory.getInstance().getData("logResponse", String.class));
	}

	public void fetchStaticBody(String apiNameBody, String refId) throws IOException, ParseException {
		Object obj = new JSONParser().parse(apiNameBody);
		JSONObject jo = (JSONObject) obj;
		String jsonBody = jo.toJSONString();
		setJsonBody(jsonBody);
	}

	public void createJsonBody(String body) throws ParseException {
		Object obj = new JSONParser().parse(body);
		JSONObject jo = (JSONObject) obj;
		String jsonBody = jo.toJSONString();
		setJsonBody(jsonBody);
	}

	public void verifyStatusCode(int actualResponseCode) {
		Assert.assertTrue("Expected Status code: 200 or 201. Actual Status code: " + actualResponseCode + ".",
				actualResponseCode == 200 || actualResponseCode == 201);
	}

	public List<String> fetchList(String path) {
		JsonPath jsonPath = getResponse().jsonPath();
		return jsonPath.getList(path);
	}

	public String fetchValue(String path) {
		JsonPath jsonPath = getResponse().jsonPath();
		return jsonPath.getString(path);
	}

	public List<Object> fetchObjectList(String path) {
		JsonPath jsonPath = getResponse().jsonPath();
		return jsonPath.get(path);
	}

	public JSONObject getJsonObject(String data) throws ParseException {
		Object obj = new JSONParser().parse(data);
		return (JSONObject) obj;

	}
}
