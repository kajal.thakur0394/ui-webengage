package com.webengage.ui_automation.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import com.webengage.ui_automation.driver.DataFactory;
import io.restassured.RestAssured;
import io.restassured.filter.Filter;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class RestAssuredBuilder {
	private RequestSpecification request;
	SetupUtility setupUtility = new SetupUtility();
	private String method;
	private String apiURI;

	@SuppressWarnings("unchecked")
	public static ArrayList<String> getAdditionalHeaders() {
		return DataFactory.getInstance().getData(ConstantUtils.ADDITIONALHEADERS, ArrayList.class);
	}

	public static void setAdditionalHeaders(ArrayList<String> additionalHeaders) {
		DataFactory.getInstance().setData(ConstantUtils.ADDITIONALHEADERS, additionalHeaders);
	}

	public static void initializeVariables() {
		setAdditionalHeaders(new ArrayList<String>());
	}

	public RestAssuredBuilder(String apiURI, String method) throws IOException {
		this.method = method;
		this.apiURI = apiURI;
		RestAssured.baseURI = apiURI.contains("survey.html")
				? System.getProperty("set.Environment").equals("Prod US") ? "https://dashboard.webengage.com"
						: setupUtility.getBaseURI().replace("/api", "")
				: setupUtility.getBaseURI();
		RestAssured.urlEncodingEnabled = false;
		request = RestAssured.given();
		request.header("Content-Type", "application/json").header("Authorization",
				"Bearer " + SetupUtility.getAuthApiKey());
		checkForAdditionalHeaders();
	}

	private void checkForAdditionalHeaders() {
		if (getAdditionalHeaders().size() > 0) {
			for (String s : getAdditionalHeaders()) {
				request.header(s.split(":")[0], s.split(":")[1]);
			}
			getAdditionalHeaders().clear();
		}
	}

	public RestAssuredBuilder(String apiURI, LinkedHashMap<String, String> formData) throws IOException {
		this.method = "POST";
		this.apiURI = apiURI;
		RestAssured.baseURI = setupUtility.getBaseURI();
		RestAssured.urlEncodingEnabled = false;
		request = RestAssured.given();
		request.header("Content-Type", "multipart/form-data").header("Authorization",
				"Bearer " + SetupUtility.getAuthApiKey());
		try {
			Map<Object, Object> filePath = formData.entrySet().stream()
					.filter(a -> a.getKey().toLowerCase().contains("file"))
					.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
			File binaryFile = new File(SetupUtility.directoryLoc + "AccountData/"
					+ filePath.values().iterator().next().toString().replace("/", File.separator));
			request.multiPart(filePath.keySet().iterator().next().toString(), binaryFile);
		} catch (NoSuchElementException e) {
			Log.info("Skipping file attachment to form data as file key is absent");
		}
		for (Map.Entry<String, String> pair : formData.entrySet().stream().filter(a -> !a.getKey().contains("File"))
				.collect(Collectors.toList())) {
			request.multiPart(pair.getKey(), pair.getValue());
		}
	}

	public Response buildAPI() {
		Filter logFilter = new CustomLogFilter();
		switch (this.method) {
		case "POST":
			return request.filter(logFilter).post(apiURI);
		case "GET":
			return request.filter(logFilter).get(apiURI);
		case "PUT":
			return request.filter(logFilter).put(apiURI);
		case "DELETE":
			return request.filter(logFilter).delete(apiURI);
		default:
			break;
		}
		return null;
	}

	public Response executeWithBody(String jsonBody) {
		request.body(jsonBody);
		return buildAPI();
	}

	public Response executeWithQueryParams(List<List<String>> queryParams) {
		for (List<String> s : queryParams) {
			String key = s.get(0);
			String value = s.get(1).contains("\"") ? s.get(1).replaceAll("\"", "") : s.get(1);
			request.queryParam(key, value);
		}
		return buildAPI();
	}

	public Response executeWithBodyAndQueryParams(String jsonBody, List<List<String>> queryParams) {
		request.body(jsonBody);
		for (List<String> s : queryParams) {
			String key = s.get(0);
			String value = s.get(1).contains("\"") ? s.get(1).replaceAll("\"", "") : s.get(1);
			request.queryParam(key, value);
		}
		return buildAPI();
	}

	public Response execute() {
		return buildAPI();
	}

}
