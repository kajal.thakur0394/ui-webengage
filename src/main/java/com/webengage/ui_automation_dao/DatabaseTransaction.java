package com.webengage.ui_automation_dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.webengage.ui_automation.pages.CampaignsPage;
import com.webengage.ui_automation.pages.JourneysPage;
import com.webengage.ui_automation.pages.SegmentsPage;
import com.webengage.ui_automation.utils.DatabaseUtility;
import com.webengage.ui_automation.utils.Log;

public class DatabaseTransaction {

	Connection testExecutionConnection;
	Connection testDataConnection;
	private static DatabaseTransaction instance;
	String buildNumber = System.getProperty("set.BuildNumber");

	private DatabaseTransaction() {
		try {
			testExecutionConnection = DatabaseUtility.getConnection("we_testexecution");
			testDataConnection = DatabaseUtility.getConnection("we_testdata");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static DatabaseTransaction getInstance() {
		if (instance == null) {
			instance = new DatabaseTransaction();
		}
		return instance;
	}

	public void selectTable(String featureName, String scenarioName) {
		switch (featureName) {
		case "LiveSegments":
		case "StaticSegments":
			insertIntoRespectiveTables(featureName, SegmentsPage.getSegmentId(), scenarioName);
			break;
		case "SMSCampaigns":
		case "EmailCampaigns":
		case "WhatsappCampaign":
		case "WebPushCampaign":
		case "PushCampaigns":
			insertIntoRespectiveTables(featureName, CampaignsPage.getCampaignID(), scenarioName);
			break;
		case "Journeys":
		case "Relays":
			insertIntoRespectiveTables(featureName, JourneysPage.getJourneyId(), scenarioName);
			break;
		default:
			Log.info("Not part of e2e suite for Data validation");
			break;
		}
	}

	private void insertIntoRespectiveTables(String feature, String uniqueId, String scenarioName) {
		RuntimeDataDAO dao = new RuntimeDataDAO();
		TransactionRecord tr = new TransactionRecord();
		tr.buildNumber = Integer.parseInt(buildNumber);
		tr.uniqueId = uniqueId;
		tr.scenarioName = scenarioName;
		if (feature.contains("Segments"))
			DatabaseUtility.executeUpdate(dao.insertRecordinRuntimeTable("segment", tr));
		else if(feature.contains("Campaign"))
			DatabaseUtility.executeUpdate(dao.insertRecordinRuntimeTable("campaign", tr));
		else if(feature.contains("Journeys")||feature.contains("Relays")) {
			tr.expectedStats=JourneysPage.getJourneyExpectedPayload();
			DatabaseUtility.executeUpdate(dao.insertJourneyRecordinRuntimeTable("journey", tr));
		}
	}

	public String fetchTemplateFromDb(String scenarioName, String templateName) {
		String template = null;
		ResultSet resultSet = null;
		try {
			if (scenarioName == null)
				resultSet = DatabaseUtility.executeQuery(testDataConnection,
						"select template from templates where template_name='" + templateName + "'");
			else {
				scenarioName = scenarioName.replace("'", "\\'");
				ResultSet resultSet_size_check_query = DatabaseUtility.executeQuery(testDataConnection,
						"select count(template) as num_of_templates from template_data where test_scenario='"
								+ scenarioName + "'");
				resultSet_size_check_query.next();
				int number_of_templates = resultSet_size_check_query.getInt("num_of_templates");
				if (number_of_templates > 1)
					resultSet = DatabaseUtility.executeQuery(testDataConnection,
							"select template from template_data where test_scenario='" + scenarioName
									+ "' and template_name='" + templateName + "'");
				else
					resultSet = DatabaseUtility.executeQuery(testDataConnection,
							"select template from template_data where test_scenario='" + scenarioName + "'");
			}
			if (resultSet.next())
				template = resultSet.getString("template");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return template;
	}
}
